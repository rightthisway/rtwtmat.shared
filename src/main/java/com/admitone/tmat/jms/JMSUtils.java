package com.admitone.tmat.jms;

public class JMSUtils {

	private static JMSMessageSender jmsMessageSender;
	private static JMSMessageReceiver jmsMessageReceiver;
	
	public static JMSMessageSender getJmsMessageSender() {
		return jmsMessageSender;
	}
	public void setJmsMessageSender(JMSMessageSender jmsMessageSender) {
		JMSUtils.jmsMessageSender = jmsMessageSender;
	}
	public static JMSMessageReceiver getJmsMessageReceiver() {
		return jmsMessageReceiver;
	}
	public void setJmsMessageReceiver(JMSMessageReceiver jmsMessageReceiver) {
		JMSUtils.jmsMessageReceiver = jmsMessageReceiver;
	}
	
}
