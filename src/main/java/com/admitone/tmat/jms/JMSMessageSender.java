package com.admitone.tmat.jms;


import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

public class JMSMessageSender {

	private Destination destinationAdmin;
	private Destination destinationAnalytics;
	private Destination destinationBrowse;
	private JmsTemplate jmsTemplate;
	
	public Destination getDestinationAdmin() {
		return destinationAdmin;
	}
	public void setDestinationAdmin(Destination destinationAdmin) {
		this.destinationAdmin = destinationAdmin;
	}
	public Destination getDestinationAnalytics() {
		return destinationAnalytics;
	}
	public void setDestinationAnalytics(Destination destinationAnalytics) {
		this.destinationAnalytics = destinationAnalytics;
	}
	public Destination getDestinationBrowse() {
		return destinationBrowse;
	}
	public void setDestinationBrowse(Destination destinationBrowse) {
		this.destinationBrowse = destinationBrowse;
	}
	public JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}
	
	public void broadCastMessage(final String key,final String value){
		MessageCreator creator = new JMSMessageCreator(key,value);  // Queue
		jmsTemplate.send(destinationAdmin, creator);
		jmsTemplate.send(destinationAnalytics, creator);
		jmsTemplate.send(destinationBrowse, creator);
	}

	public void sendMessageToBrowse(final String key,final String value){
		MessageCreator creator = new JMSMessageCreator(key,value);  // Queue
		jmsTemplate.send(destinationBrowse, creator);
	}
	
	public void sendMessageToAnalytics(final String key,final String value){
		MessageCreator creator = new JMSMessageCreator(key,value);  // Queue
		jmsTemplate.send(destinationAnalytics, creator);
	}
	
	public void sendMessageToAdmin(final String key,final String value){
		MessageCreator creator = new JMSMessageCreator(key,value);  // Queue
		jmsTemplate.send(destinationAdmin, creator);
	}
	public class JMSMessageCreator implements MessageCreator{
		String key;
		String value;
		
		public JMSMessageCreator(String key,String value){
			this.key=key;
			this.value=value;
		}
		
		public Message createMessage(Session session) throws JMSException {
	         TextMessage message = null;
             try {
                 message = session.createTextMessage();
                 message.setStringProperty(key, value);
             }catch (JMSException e) {
                 e.printStackTrace();
             }
             return message;
		}
		
	}


}
