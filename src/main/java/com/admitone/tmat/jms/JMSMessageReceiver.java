package com.admitone.tmat.jms;

//import javax.jms.Destination;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.List;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.crawler.util.CrawlerJMSUtil;
import com.admitone.tmat.crawler.util.LocalDestination;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.utils.SpringUtil;

public class JMSMessageReceiver implements MessageListener{
//	 private JmsTemplate jmsTemplate;
	 private Destination destination;
	 private CrawlerJMSUtil crawlerUtil;
	 /*
	    public void setJmsTemplate(JmsTemplate jmsTemplate) {
	        this.jmsTemplate = jmsTemplate;
	    }
	*/
		public Destination getDestination() {
			return destination;
		}

		public void setDestination(Destination destination) {
			this.destination = destination;
		}
		
		public void onMessage(Message message) {
			TextMessage objMessage = null;
		        if (message instanceof TextMessage){
		            objMessage = (TextMessage)message;
		            try{
		                Enumeration<String> keys = objMessage.getPropertyNames();
		                while(keys.hasMoreElements()){
		                	String key= keys.nextElement();
		                	if(key==null || key.isEmpty()){
		                		continue;
		                	}
		                	if(key.contains("executed")){
		                		String eventId = key.replace("event-executed-", "");
		                		String response = objMessage.getStringProperty(key);
		                		String[] result=response.split("--");
		                		if(result.length>0){
		                			if(result[0].equals("OK")){
		                				crawlerUtil.addExecutedEvents(eventId, Integer.parseInt(result[1]), Integer.parseInt(result[2]));
		                			}
		                		}
		                	}else if(key.contains("crawl")){
		                		manageCrawl(key,objMessage.getStringProperty(key));
		                	}/*else if(key.contains("event")){
		                		if(key.contains("add")){
		                			System.out.println("Add:" + textMessage.getStringProperty(key));
		                		}
		                	}*/
		                	
		                }
		                
		            	
		                
		            }catch (JMSException e){
		                e.printStackTrace();
		            }
		       }
			
		}
		
		
		public void manageCrawl(String key,String value){
			if(key.contains("add")){  // crawl-add
    			String crawls = value;
    			TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
    			for(String crawlId:crawls.split(",")){
    				if(crawlId==null || crawlId.isEmpty()){
    					continue;
    				}
    				TicketListingCrawl  crawl = DAORegistry.getTicketListingCrawlDAO().get(Integer.parseInt(crawlId.trim()));
    				ticketListingCrawler.addTicketListingCrawl(crawl);
    			}
    		}else if(key.contains("force")){ // crawl-force
    			forceCrawl(key,value);
    		}else if(key.contains("remove")){ // crawl-remove
    			String crawls = value;
    			TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
    			for(String crawlId:crawls.split(",")){
    				if(crawlId==null || crawlId.isEmpty()){
    					continue;
    				}
//    				TicketListingCrawl  crawl = DAORegistry.getTicketListingCrawlDAO().get(Integer.parseInt(crawlId.trim()));
    				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(Integer.parseInt(crawlId));
    				ticketListingCrawler.removeTicketListingCrawl(crawl);
    			}
    		}else if(key.contains("disable")){ // crawl-disable-event or crawl-disable-tour 
    			String ids = value;
    			TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
    			if(key.contains("event")){
        			for(String eventId:ids.split(",")){
        				if(eventId==null || eventId.isEmpty()){
        					continue;
        				}
//		                				TicketListingCrawl  crawl = DAORegistry.getTicketListingCrawlDAO().get(Integer.parseInt(crawlId.trim()));
        				ticketListingCrawler.disableCrawlsForEvent(Integer.parseInt(eventId));
        			}
    			}else if(key.contains("artist")){
        			for(String artistId:ids.split(",")){
        				if(artistId==null || artistId.isEmpty()){
        					continue;
        				}
        				ticketListingCrawler.disableCrawlsForArtist(Integer.parseInt(artistId));
        			}
    			}else {
        			for(String crawlId:ids.split(",")){
        				if(crawlId==null || crawlId.isEmpty()){
        					continue;
        				}
        				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(Integer.parseInt(crawlId));
        				ticketListingCrawler.removeTicketListingCrawl(crawl);
        			}
    			}
    		}else if(key.contains("enable")){ // crawl-enable-event or crawl-disable-tour 
    			String ids = value;
    			TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
    			if(key.contains("event")){
        			for(String eventId:ids.split(",")){
        				if(eventId==null || eventId.isEmpty()){
        					continue;
        				}
//		                				TicketListingCrawl  crawl = DAORegistry.getTicketListingCrawlDAO().get(Integer.parseInt(crawlId.trim()));
        				ticketListingCrawler.enableCrawlsForEvent(Integer.parseInt(eventId));
        			}
    			}else if(key.contains("tour")){
        			for(String tourId:ids.split(",")){
        				if(tourId==null || tourId.isEmpty()){
        					continue;
        				}
        				ticketListingCrawler.enableCrawlsForArtist(Integer.parseInt(tourId));
        			}
    			}else{
    				for(String crawlId:ids.split(",")){
        				if(crawlId==null || crawlId.isEmpty()){
        					continue;
        				}
        				TicketListingCrawl  crawl = DAORegistry.getTicketListingCrawlDAO().get(Integer.parseInt(crawlId.trim()));
        				ticketListingCrawler.addTicketListingCrawl(crawl);
        				ticketListingCrawler.enableCrawl(Integer.parseInt(crawlId));
        			}
    				
    			}
    		}
		}
		
		
		public void forceCrawl(String key,String value){
			if(key.contains("event")){   // crawl-force-event-desintation or crawl-force-event-desintation-timestamp
				String eventsString = value;
    			TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
    			List<TicketListingCrawl>  crawls = new ArrayList<TicketListingCrawl>();
    			String dest = key.split("-")[3];
    			LocalDestination destination = null;
    			if(dest.toUpperCase().equals("ADMIN")){
    				destination = LocalDestination.ADMIN;
    			}else if(dest.toUpperCase().equals("ANALYTICS")){
    				destination = LocalDestination.ANALYTICS;
    			}else{
    				destination = LocalDestination.BROWSE;
    			}
    			for(String eventId:eventsString.split(",")){
    				if(eventId==null || eventId.isEmpty()){
    					continue;
    				}
    				Collection<TicketListingCrawl> eventCrawls = ticketListingCrawler.getTicketListingCrawlByEvent(Integer.parseInt(eventId));
    				crawls.addAll(eventCrawls);
    				crawlerUtil.addEventToCheckStatus(eventId, destination, crawls.size());
    			}
    			ticketListingCrawler.forceRecrawlWithPriority(crawls, TicketListingCrawl.PRIORITY_HIGH);
    			
    			
    			
			}else{
    			String crawlsString = value;
    			TicketListingCrawler ticketListingCrawler=  SpringUtil.getTicketListingCrawler();
    			List<TicketListingCrawl>  crawls = new ArrayList<TicketListingCrawl>(); 
    			for(String crawlId:crawlsString.split(",")){
    				if(crawlId==null || crawlId.isEmpty()){
    					continue;
    				}
    				TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(Integer.parseInt(crawlId));
    				crawls.add(crawl);
    				
    			}
    			ticketListingCrawler.forceRecrawlWithPriority(crawls, TicketListingCrawl.PRIORITY_HIGH);
			}
		}

		public CrawlerJMSUtil getCrawlerUtil() {
			return crawlerUtil;
		}

		public void setCrawlerUtil(CrawlerJMSUtil crawlerUtil) {
			this.crawlerUtil = crawlerUtil;
		}
}
