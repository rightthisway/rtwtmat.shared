package com.admitone.tmat.crawler;

import java.io.Serializable;

public class CrawlerDelayStat implements Serializable {
	private long totalDelay = 0;
	private int totalCount = 0;
	private int frequency; // in second
	private long totalTime = 0; 
	private long totalProActiveTime = 0;
	private int proActiveCount = 0;

	public long getTotalProActiveTime() {
		return totalProActiveTime;
	}

	public int getProActiveCount() {
		return proActiveCount;
	}
	
	public Double getProActiveAverageTime() {
		return (double) totalProActiveTime / (double) proActiveCount;
	}

	public CrawlerDelayStat(int frequency) {
		this.frequency = frequency;
	}
	
	public void reset() {
		totalDelay = 0;
		totalCount = 0;
	}
	
	public void increment(long delay) {
		increment(delay, 1);
	}

	public void increment(long delay, int count) {
		increment(delay, count, frequency * 1000);
	}

	public void increment(long delay, int count, long time) {
		if (delay < 0) {
			proActiveCount += count;
			totalProActiveTime -= delay;
		} else {
			totalDelay += delay;			
		}
		this.totalCount+=count;
		totalTime += time;
	}

	public long getTotalDelay() {
		return totalDelay;
	}
	
	public int getTotalCount() {
		return totalCount;
	}

	public long getTotalTime() {
		return totalTime;
	}

	public Double getAverageDelay() {
		return (double)totalDelay / (double)totalCount;
	}

	public Double getAverageDelayInPercent() {
		return (double)(100 * totalDelay) / (double)totalTime;
	}

	public int getFrequency() {
		return frequency;
	}

}
