package com.admitone.tmat.crawler;

/**
 * Exception used when the crawl had timed out.
 */
public class TimedOutTicketListingCrawlException extends RuntimeException {
	private static final long serialVersionUID = -3805684140339661109L;

	public TimedOutTicketListingCrawlException() {
		super();
	}
	
	public TimedOutTicketListingCrawlException(String message) {
		super(message);
	}
}
