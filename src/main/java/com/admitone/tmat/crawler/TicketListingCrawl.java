package com.admitone.tmat.crawler;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.DefaultExchangeCrawlFrequency;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.eventfetcher.EventHit;

/**
 * Ticket listing crawl class.
 */
@Entity
@Table(name="ticket_listing_crawl")
public class TicketListingCrawl implements Serializable, Cloneable, Comparable<TicketListingCrawl>{
	private static final long serialVersionUID = -5219427536191501061L;
	public final static int PRIORITY_LOW = 0;
	public final static int PRIORITY_NORMAL = 5;
	public final static int PRIORITY_HIGH = 10;
	
	/**
	 * Crawl Id.
	 */
	private Integer id;

	/**
	 * Crawl Name.
	 */
	private String name;

	/**
	 * Date when the crawl starts.
	 */
	private Date startCrawl;

	/**
	 * Date when the crawl ends.
	 */
	private Date endCrawl;

	
	/**
	 * Site Id.
	 */
	private String siteId;

	/**
	 * Tour Id.
	 */
//	private Integer tourId;

	/**
	 * Event Id.
	 */
	private Integer eventId;
	
	/**
	 * Extra parameters of the crawl.
	 */	
	private String extraParameters;
	
	/**
	 * Creator username.
	 */	
	private String creator;

	/**
	 * Last updater username.
	 */	
	private String lastUpdater;
	
	/**
	 * Creation date.
	 */	
	private Date created;

	
	/**
	 * Last updated date.
	 */	
	private Date lastUpdated;
	
	/**
	 * Enabled.
	 */	
	private boolean enabled;

	/**
	 * URL of the ticket listing.
	 */	
	private String queryUrl;

	/**
	 * Crawl state:
	 * - STOPPED
	 * - RUNNING
	 * - PAUSED
	 * - ERROR
	 */	
	private CrawlState crawlState = CrawlState.STOPPED;

	/**
	 * Crawl phase:
	 * - STOPPED
	 * - EXTRACTION
	 * - FLUSH
	 * - EXPIRATION
	 */	
	private CrawlPhase crawlPhase = CrawlPhase.STOPPED;
	
	/**
	 * Last successful run
	 */
	private Date lastSuccessfulRun;

	/**
	 * crawl frequency in second
	 * by default, set it to every hour.
	 */
	private int crawlFrequency;
	
	/**
	 * whether frequency is set automatically or not
	 */	
	private boolean automaticCrawlFrequency = true;
	
	/****************************************************************
	 * NOT PERSISTED FIELDS BUT USED BY MASTER AND WORKER
	 ****************************************************************/
	
	/**
	 * Exception associated to this crawl (if error)
	 */
	
	private Exception exception;
	
	/*
	 * Priority associated to the crawl
	 */
	
	private int priority = PRIORITY_NORMAL; // the higher the better
		
	/**
	 * set to true if the crawl is being cancelled
	 */
	private boolean cancelled;
	
	/**
	 * Error message.
	 */	
	private String errorMessage;
	
	/*
	 * Suspended
	 */
	
	private boolean suspended;
	
	/**
	 * whether crawl is force recrawled or notfrequency is set automatically or not
	 */	
	private boolean forceReCrawlFlag = false;

	/****************************************************************
	 * NOT PERSISTED FIELDS ONLY USED BY WORKER (no need to serialize them)
	 ****************************************************************/

	/**
	 * New items indexed during the last crawl.
	 */	
	private Integer newItemIndexed;

	/**
	 * Items indexed during the last crawl.
	 */	
	private Integer itemIndexed;

	/**
	 * Item errors indexed during the last crawl.
	 */	
	private Integer itemErrorIndexed;

	/**
	 * Total time spent on dispatching queue.
	 */
	private Long dispatchingTime = 0L;

	/**
	 * Total time spent on fetching the ticket listing from remote server.
	 */
	private Long fetchingTime = 0L;
	
	/**
	 * Total time spent on extracting the ticket information.
	 */
	private Long extractionTime = 0L;

	/**
	 * Total time spent on indexing the ticket information.
	 */
	private Long indexationTime = 0L;

	/**
	 * Total time spent on flushing the ticket information.
	 */
	private Long flushingTime = 0L;

	/**
	 * Total time spent on expiring the old tickets
	 */
	private Long expirationTime = 0L;

	/**
	 * Total byte count fetched. (measure number of compressed bytes)
	 */
	private int fetchedByteCount = 0;

	/*
	 * node Id and hostname processing the crawl;
	 */
	
	private Integer nodeId;
	private String hostname;
	private Event event;
//	private String postBackUrl;
	private Boolean xmlPostBack;
	
	public TicketListingCrawl() {
		resetStats();
	}

	public TicketListingCrawl(EventHit eventHit) {
		Calendar cal = Calendar.getInstance();
		Date now = cal.getTime();
//		cal.add(Calendar.MINUTE, -15);
 		this.setCreated(now);
		this.setLastUpdated(now);
		this.setCreator(creator);
		this.setCrawlState(crawlState);
		this.setCrawlPhase(crawlPhase);
		//System.out.println("CRAWLER CREATION EVENTHIT SITE: " + eventHit.getSiteId());
		if (eventHit.getSiteId().equals(Site.TICKET_NETWORK_DIRECT)) {
			this.setExtraParameter("queryEventId", eventHit.getEventId());
		} else if (eventHit.getSiteId().equals(Site.EIBOX)) {
			String[] tokens = eventHit.getId().split("-");
			this.setExtraParameter("queryEventId", tokens[1]);			
		 	//System.out.println("CRAWLER CREATION EVENTHIT EVENTID: " + tokens[0]);
			this.setExtraParameter("queryVenueId", tokens[2]);			
			this.setExtraParameter("queryEventDate", tokens[3]);			
		} else if(eventHit.getSiteId().equals(Site.EI_MARKETPLACE)) {
			this.setExtraParameter("queryEventId", eventHit.getEventId());
		}
		//System.out.println("CRAWLER CREATION EVENTHIT ID: " + eventHit.getId());
		this.setSiteId(eventHit.getSiteId());
		this.setEnabled(true);
	//	this.setAutomaticCrawlFrequency(true);
		this.setQueryUrl(eventHit.getUrl());
		if(!eventHit.getSiteId().equals(Site.TICKET_NETWORK_DIRECT) && !eventHit.getSiteId().equals(Site.EI_MARKETPLACE) && !eventHit.getSiteId().equals(Site.EIBOX)){
			this.setExtraParametersString(eventHit.getExtraParameters());		
		}
		
		/* CS 02/11/2011, next change on 01/31/2014
		this.setCrawlFrequency(50*365*24*60*60);  // set to Idle and autoFrequency true
		this.setAutomaticCrawlFrequency(true); */
		
		/*By Ulaganathan - Crawl Frequency based on site id by default. - Begins*/
		DefaultExchangeCrawlFrequency defaultExchangeCrawlFrequency = DAORegistry.getDefaultExchangeCrawlFrequencyDAO().getDefaultExchangeCrawlFrequencyByExchange(eventHit.getSiteId());
		if(null != defaultExchangeCrawlFrequency ){
			this.setCrawlFrequency(defaultExchangeCrawlFrequency.getDefaultCrawlFrequency()); 
			this.setAutomaticCrawlFrequency(defaultExchangeCrawlFrequency.isAutomaticCrawlFrequency());
		}else{
			this.setCrawlFrequency(50*365*24*60*60);  // set to Idle and autoFrequency true
			this.setAutomaticCrawlFrequency(false);
		}
		/*Crawl Frequency based on site id by default. - End*/
		
//		this.setEndCrawl(cal.getTime());
//		this.setStartCrawl(cal.getTime());
//		CS 02/11/2011 ,next change on 01/31/2014*/ 
	}

	public TicketListingCrawl(EventHit eventHit, Event event, String creator) {
		this(eventHit);
//		Tour tour = event.getTour();
		this.setEventId(event.getId());
//		this.setTourId(event.getTourId());
		DateFormat dateFormat = new SimpleDateFormat("MM.dd.yy");
		this.setCreator(creator);
		String date= event.getLocalDate()!=null?dateFormat.format(event.getLocalDate()):"TBD" ;
		this.setName(event.getEventType().toString().toLowerCase() + "." + eventHit.getShortName().replace(" ", "-") + "." + date + "." + eventHit.getSiteId());
	}
	
	public void update(TicketListingCrawl crawl) {
//		this.setId(crawl.getId());
		this.setName(crawl.getName());
		this.setCrawlFrequency(crawl.getCrawlFrequency());
		this.setEventId(crawl.getEventId());
//		this.setTourId(crawl.getTourId());
		this.setSiteId(crawl.getSiteId());
		this.setErrorMessage(crawl.getErrorMessage());
		this.setException(crawl.getException());
		this.setNewItemIndexed(crawl.getNewItemIndexed());
		this.setItemIndexed(crawl.getItemIndexed());
		this.setItemErrorIndexed(crawl.getItemErrorIndexed());
		this.setStartCrawl(crawl.getStartCrawl());
		this.setEndCrawl(crawl.getEndCrawl());
		this.setCrawlState(crawl.getCrawlState());
		this.setCrawlPhase(crawl.getCrawlPhase());
		this.setQueryUrl(crawl.getQueryUrl());
		this.setExtraParametersString(crawl.getExtraParametersString());
		this.setEnabled(crawl.isEnabled());
		this.setFetchingTime(crawl.getFetchingTime());
		this.setExtractionTime(crawl.getExtractionTime());
		this.setIndexationTime(crawl.getIndexationTime());
		this.setExpirationTime(crawl.getExpirationTime());
		this.setCancelled(crawl.isCancelled());	
		this.setNodeId(crawl.getNodeId());
		this.setHostname(crawl.getHostname());
		this.setPriority(crawl.getPriority());
		this.setForceReCrawlFlag(crawl.isForceReCrawlFlag());
	}

	public TicketListingCrawl(Integer id) {
		this();
		this.id = id;
	}

	@Id
	@Column(name="id",  columnDefinition="INTEGER AUTO_INCREMENT")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="start_crawl")
	public Date getStartCrawl() {
		return startCrawl;
	}
	
	public void setStartCrawl(Date startCrawl) {
		this.startCrawl = startCrawl;
	}	

	@Column(name="end_crawl")
	public Date getEndCrawl() {
		return endCrawl;
	}
	
	public void setEndCrawl(Date endCrawl) {
		this.endCrawl = endCrawl;
	}

	@Column(name="crawl_frequency")
	public int getCrawlFrequency() {
		return crawlFrequency;
	}

	public void setCrawlFrequency(int crawlFrequency) {
		this.crawlFrequency = crawlFrequency;
	}

	@Column(name="site_id")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	/*@Column(name="tour_id")
	public Integer getTourId() {
		return tourId;
	}

	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}*/

	@Column(name="enabled")
	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Transient
	public CrawlState getCrawlState() {
		return crawlState;
	}

	public void setCrawlState(CrawlState crawlState) {
		this.crawlState = crawlState;
	}

	@Column(name="last_successful_run")
	public Date getLastSuccessfulRun() {
		return lastSuccessfulRun;
	}

	public void setLastSuccessfulRun(Date lastSuccessfulRun) {
		this.lastSuccessfulRun = lastSuccessfulRun;
	}

	/*@Transient
	public Tour getTour() {
		if (tourId == null) {
			return null;
		}
		return DAORegistry.getTourDAO().get(tourId);
	}*/
	
	
	
	public Object clone() throws CloneNotSupportedException {
		TicketListingCrawl copy = (TicketListingCrawl)super.clone();
		copy.setId(getId());
		copy.setName(getName());
		copy.setCrawlFrequency(getCrawlFrequency());
		copy.setEventId(getEventId());
//		copy.setTourId(getTourId());
		copy.setSiteId(getSiteId());
		copy.setErrorMessage(getErrorMessage());
		copy.setException(getException());
		copy.setNewItemIndexed(getNewItemIndexed());
		copy.setItemIndexed(getItemIndexed());
		copy.setItemErrorIndexed(getItemErrorIndexed());
		copy.setStartCrawl(getStartCrawl());
		copy.setEndCrawl(getEndCrawl());
		copy.setCrawlState(getCrawlState());
		copy.setCrawlPhase(crawlPhase);
		copy.setQueryUrl(getQueryUrl());
		copy.setExtraParametersString(getExtraParametersString());
		copy.setEnabled(isEnabled());
		copy.setFetchingTime(fetchingTime);
		copy.setExtractionTime(extractionTime);
		copy.setIndexationTime(indexationTime);
		copy.setExpirationTime(expirationTime);
		copy.setCancelled(cancelled);
		copy.setLastSuccessfulRun(lastSuccessfulRun);
		copy.setForceReCrawlFlag(isForceReCrawlFlag());
		return copy;
	}
		
	@Column(name="extra_parameters")
	public String getExtraParametersString() {
		return extraParameters;
	}

	public void setExtraParametersString(String extraParameters) {
		this.extraParameters = extraParameters;
	}

	public synchronized void setExtraParameterMap(Map<String, String> map) {
		this.extraParameters = "";
		for (Map.Entry<String, String> entry: map.entrySet()) {
			this.extraParameters += entry.getKey() + "=" + entry.getValue() + "\n";		
		}
	}
	
	@Column(name="creator")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Column(name="last_updater")
	public String getLastUpdater() {
		return lastUpdater;
	}

	public void setLastUpdater(String lastUpdater) {
		this.lastUpdater = lastUpdater;
	}	

	@Column(name="created")
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	@Transient
	public synchronized Map<String, String> getExtraParameterMap() {
		Map<String, String> map = new HashMap<String, String>();
		
		if (extraParameters != null && !extraParameters.isEmpty()) {
			for (String line: extraParameters.split("\n")) {
				String[] tokens = line.split("=", 2);
				if (tokens.length != 2) {
					continue;
				}
				map.put(tokens[0], tokens[1]);
			}
		}
		return map;
	}
	
	@Transient
	public String getExtraParameter(String name) {
		return getExtraParameterMap().get(name);
	}

	public void setExtraParameter(String name, String value) {		
		Map<String, String> map = getExtraParameterMap();
//		System.out.println("CRAWLER CREATION MAP: " + map);
		map.put(name, value);
		setExtraParameterMap(map);
	}

	@Column(name="query_url", length=1024)
	public String getQueryUrl() {
		return queryUrl;
	}

	public void setQueryUrl(String queryUrl) {
		this.queryUrl = queryUrl;
	}

	@Column(name="automatic_crawl_frequency")
	public boolean getAutomaticCrawlFrequency() {
		return automaticCrawlFrequency;
	}

	public void setAutomaticCrawlFrequency(boolean automaticCrawlFrequency) {
		this.automaticCrawlFrequency = automaticCrawlFrequency;
	}

	@Transient
	public int getNextCrawlWaitingTime() {
		if (!enabled) {
			return -1;
		}
		
		if (crawlState.equals(CrawlState.RUNNING)) {
			return 0;
		}
		
		if (endCrawl == null) {
			return 1;
		}
		
		int frequency = crawlFrequency;
		if (crawlState.isError()) {
		//	frequency = 1800;
			frequency = crawlFrequency;
		}
		int waitingTime = (int)(frequency - (new Date().getTime() - endCrawl.getTime()) / 1000);
		if (waitingTime <= 0) {
			return 1;
		}
		return waitingTime;
	}

	@Transient
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	@Transient
	public Integer getNewItemIndexed() {
		return newItemIndexed;
	}

	public void setNewItemIndexed(Integer newItemIndexed) {
		this.newItemIndexed = newItemIndexed;
	}

	@Transient
	public Integer getItemIndexed() {
		return itemIndexed;
	}

	public void setItemIndexed(Integer itemIndexed) {
		this.itemIndexed = itemIndexed;
	}

	@Transient
	public Integer getItemErrorIndexed() {
		return itemErrorIndexed;
	}

	public void setItemErrorIndexed(Integer itemErrorIndexed) {
		this.itemErrorIndexed = itemErrorIndexed;
	}
	
	@Transient
	public int getTotalFetchedActiveTicketCount() {
		return DAORegistry.getTicketDAO().getTicketCountByCrawl(id, TicketStatus.ACTIVE);
	}

	@Transient
	public int getTotalFetchedActiveTicketQuantitySum() {
		return DAORegistry.getTicketDAO().getTicketQuantitySumByCrawl(id, TicketStatus.ACTIVE);
	}

	@Transient
	public int getTotalFetchedExpiredTicketCount() {
		return DAORegistry.getTicketDAO().getTicketCountByCrawl(id, TicketStatus.EXPIRED);
	}

	@Transient
	public int getTotalFetchedExpiredTicketQuantitySum() {
		return DAORegistry.getTicketDAO().getTicketQuantitySumByCrawl(id, TicketStatus.EXPIRED);
	}

	@Transient
	public int getTotalFetchedSoldTicketCount() {
		return DAORegistry.getTicketDAO().getTicketCountByCrawl(id, TicketStatus.SOLD);
	}

	@Transient
	public int getTotalFetchedSoldTicketQuantitySum() {
		return DAORegistry.getTicketDAO().getTicketQuantitySumByCrawl(id, TicketStatus.SOLD);
	}

	@Transient
	public int getTotalFetchedTicketCount() {
		return DAORegistry.getTicketDAO().getTicketCountByCrawl(id);
	}

	@Transient
	public int getTotalFetchedTicketQuantitySum() {
		return DAORegistry.getTicketDAO().getTicketQuantitySumByCrawl(id);
	}
	
	private boolean cmpWithNull(String str1, String str2) {
		if (str1 == null && str2 == null) {
			return true;
		}
		
		if (str1 == null) {
			return false;
		}

		if (str2 == null) {
			return false;
		}
		
		return str1.equals(str2);

	}

	@Transient
	public boolean isEquivalent(TicketListingCrawl crawl) {
		// TODO: check equivalence for site like stubhub where we can have 2 types of url
		// System.out.println(crawl.getSiteId() + " " + this.getSiteId());
		// System.out.println(crawl.getEventId() + " " + this.getEventId());
		// System.out.println(crawl.getQueryUrl() + " " + this.getQueryUrl());
		// System.out.println(crawl.getExtraParametersString() + " " + this.getExtraParametersString());
		if (!cmpWithNull(crawl.getSiteId(), this.getSiteId())
			|| !cmpWithNull(crawl.getQueryUrl(), this.getQueryUrl())
			|| !cmpWithNull(crawl.getExtraParametersString(), this.getExtraParametersString())
			|| !cmpWithNull(crawl.getEventId() + "", this.getEventId() + "")) {
			return false;
		}
		
		return true;
	}

	@Transient
	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	@Transient
	public Long getFetchingTime() {
		return fetchingTime;
	}

	public void setFetchingTime(Long fetchingTime) {
		this.fetchingTime = fetchingTime;
	}

	@Transient
	public Long getExtractionTime() {
		return extractionTime;
	}

	public void setExtractionTime(Long extractionTime) {
		this.extractionTime = extractionTime;
	}

	@Transient
	public Long getIndexationTime() {
		return indexationTime;
	}

	public void setIndexationTime(Long indexationTime) {
		this.indexationTime = indexationTime;
	}

	@Transient
	public Long getExpirationTime() {
		return expirationTime;
	}

	public void setExpirationTime(Long expirationTime) {
		this.expirationTime = expirationTime;
	}

	@Transient
	public CrawlPhase getCrawlPhase() {
		return crawlPhase;
	}

	public void setCrawlPhase(CrawlPhase crawlPhase) {
		this.crawlPhase = crawlPhase;
	}

	@Override
	public String toString() {
		return "Crawl: id=" + id
				+ ", name=" + name 
//				+ ",tourId=" + tourId
				+ ",eventId=" + eventId
				+ ",queryString=" + getExtraParameter("queryString")
				+ ",queryUrl=" + getQueryUrl()
				+ ",state=" + crawlState
				+ ",phase=" + crawlPhase
				+ ",suspended=" + suspended;
				
	}
	
	public void resetStats() {
		errorMessage = null;
		exception = null;
		itemIndexed = 0;
		newItemIndexed = 0;
		itemErrorIndexed = 0;
		extractionTime = 0L;
		indexationTime = 0L;
		fetchingTime = 0L;
		expirationTime = 0L;
		fetchedByteCount = 0;
	}

	@Transient
	public Exception getException() {
		return exception;
	}

	// FIXME: francois: quick fix because some exceptions are not serializable 
	public void setException(Exception exception) {
		if (exception != null) {
			this.exception = new Exception(exception.getMessage());
			this.exception.setStackTrace(exception.getStackTrace());
		}
	}

	@Transient
	public String getShortStatusMessage() {
		if (crawlState.isError()) {
			return (errorMessage != null && errorMessage.equals("No tickets found"))?"NO TICKETS":"ERROR";
		} else if (enabled) {
			return "RUNNING";
		} else {
			return "STOPPED";
		}	
	}

	
	@Transient
	public String getCrawlExceptionStackTrace() {
		if (exception == null || exception.getStackTrace() == null) {
			return null;
		}
		
		String str ="";
		
		for (StackTraceElement element : exception.getStackTrace()) {
			str += element.toString() + "\n";
		}		
		return str;
	}
	
	@Transient
	public int getFetchedByteCount() {
		return fetchedByteCount;
	}

	public void setFetchedByteCount(int fetchedByteCount) {
		this.fetchedByteCount = fetchedByteCount;
	}

	// used by display tag to compare status
	public int compareTo(TicketListingCrawl crawl) {
		Comparable[] values1 = {getShortStatusMessage(), (errorMessage == null)?"":errorMessage, getNextCrawlWaitingTime()};
		Comparable[] values2 = {crawl.getShortStatusMessage(), (crawl.getErrorMessage() == null)?"":errorMessage, crawl.getNextCrawlWaitingTime()};
		
		for(int i = 0 ; i < values1.length ; i++) {
			int r = values1[i].compareTo(values2[i]);
			if (r != 0) {
				return r;
			}
		}		
		return 0;
	}

	@Transient
	public Integer getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	@Transient
	public Integer getNodeId() {
		return nodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	@Transient
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	@Transient
	public Long getDispatchingTime() {
		return dispatchingTime;
	}

	public void setDispatchingTime(Long dispatchingTime) {
		this.dispatchingTime = dispatchingTime;
	}

	@Transient
	public Long getFlushingTime() {
		return flushingTime;
	}

	public void setFlushingTime(Long flushingTime) {
		this.flushingTime = flushingTime;
	}

	@Transient
	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	@Transient
	public boolean isBroken() {
		Date now = new Date();
		return (enabled 
				&& (now.getTime() - lastUpdated.getTime()) > 24L * 60L * 60L * 1000L
				&& (lastSuccessfulRun == null
						|| (now.getTime() - lastSuccessfulRun.getTime()) > 24L * 60L * 60L * 1000L));
	}
	
	public void setBroken(boolean broken) {
		// do nothing
		// define the setter to make the jsp happy
	}
/*
	@Column(name="post_back_url", length=1024)
	public String getPostBackUrl() {
		return postBackUrl;
	}

	public void setPostBackUrl(String postBackUrl) {
		this.postBackUrl = postBackUrl;
	}
*/
	@Column(name="xml_post_back")
	public Boolean getXmlPostBack() {
		return xmlPostBack;
	}

	public void setXmlPostBack(Boolean xmlPostBack) {
		this.xmlPostBack = xmlPostBack;
	}

	@Transient
	public Event getEvent() {
		if(event!=null){
			return event;
		}
		if (eventId == null) {
			return null;
		}
		event = DAORegistry.getEventDAO().get(eventId);
		return event ;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}

	@Transient
	public boolean isForceReCrawlFlag() {
		return forceReCrawlFlag;
	}

	public void setForceReCrawlFlag(boolean forceReCrawlFlag) {
		this.forceReCrawlFlag = forceReCrawlFlag;
	}
	
	
	/*
	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException {
		id = in.readInt();
		name = (String)in.readObject();
		startCrawl = (Date)in.readObject();
		endCrawl = (Date)in.readObject();
		siteId = (String)in.readObject();
		tourId = in.readInt();
		eventId = in.readInt();
		extraParameters = (String)in.readObject();
		creator = (String)in.readObject();
		lastUpdater = (String)in.readObject();
		created = (Date)in.readObject();
		lastUpdated = (Date)in.readObject();
		enabled = in.readBoolean();
		queryUrl = (String)in.readObject();
		crawlState = (CrawlState)in.readObject();
		crawlPhase  = (CrawlPhase)in.readObject();
		crawlFrequency = in.readInt();
		automaticCrawlFrequency = in.readBoolean();
		priority = in.readInt();
		cancelled = in.readBoolean();
		errorMessage = (String)in.readObject();
		exception = (Exception)in.readObject();		
	}
	public void writeExternal(ObjectOutput out) throws IOException {
		out.writeInt(id);
		out.writeObject(name);
		out.writeObject(startCrawl);
		out.writeObject(endCrawl);
		out.writeObject(siteId);
		out.writeInt(tourId);
		out.writeInt(eventId);
		out.writeObject(extraParameters);
		out.writeObject(creator);
		out.writeObject(lastUpdater);
		out.writeObject(created);
		out.writeObject(lastUpdated);
		out.writeBoolean(enabled);
		out.writeObject(queryUrl);
		out.writeObject(crawlState);
		out.writeObject(crawlPhase);
		out.writeInt(crawlFrequency);
		out.writeBoolean(automaticCrawlFrequency);
		out.writeInt(priority);
		out.writeBoolean(cancelled);
		out.writeObject(errorMessage);
		out.writeObject(exception);
	}
	*/
}
