package com.admitone.tmat.crawler;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.admitone.tmat.SystemStat;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.indexer.TicketHitIndexerStat;
import com.admitone.tmat.web.Constants;

/**
 * Contains stats for a thread pool (extraction time, indexation time, ...)
 * as well as system stats (CPU, DB, ...)
 * @author frad
 *
 */
public class TicketListingCrawlStat implements Serializable {
	private long totalTimeToProcessCrawl = 0;
	private long totalProcessedTicketCount = 0;
	private int totalProcessedCrawlCount = 0;
	private int activeThreadCount;
	private SystemStat systemStat = new SystemStat();
	private Map<Integer, CrawlPhase> crawlPhaseMap = new HashMap<Integer, CrawlPhase>();
	private Map<String, SiteStat> siteStatBySiteId = new HashMap<String, SiteStat>();
/*
	private Map<String, Long> totalProcessedTicketCountBySiteId = new HashMap<String, Long>();
	private Map<String, Long> totalTimeToProcessCrawlBySiteId = new HashMap<String, Long>();

	private Map<String, Long> totalExpirationTimeBySiteId = new HashMap<String, Long>();
	private Map<String, Long> totalFetchingTimeBySiteId = new HashMap<String, Long>();
	private Map<String, Long> totalExtractionTimeBySiteId = new HashMap<String, Long>();
	private Map<String, Long> totalIndexationTimeBySiteId = new HashMap<String, Long>();
	private Map<String, Long> totalProcessedCrawlCountBySiteId = new HashMap<String, Long>();
	private Map<String, Long> totalFetchedByteCountBySiteId = new HashMap<String, Long>();
	private Map<String, Integer> totalTicketListingCrawlCountBySiteId = new HashMap<String, Integer>();
	private Map<String, Integer> bandwidthBySiteId = new HashMap<String, Integer>();
*/
	private Map<Integer, CrawlerDelayStat> delayByFrequency = new HashMap<Integer, CrawlerDelayStat>();

	private TicketHitIndexerStat ticketHitIndexerStat = new TicketHitIndexerStat();
	
	private Map<Integer, Date> threadLastUpdatedMap = new HashMap<Integer, Date>();

	private String hostname;
	private Integer nodeId;
	private Date lastUpdate = new Date();
	
	public TicketListingCrawlStat() {
		reset();		
	}
	
	public TicketListingCrawlStat(String hostname, Integer nodeId) {
		this();
		this.hostname = hostname;
		this.nodeId = nodeId;
	}
	
	public String getName() {
		return nodeId + "-" + hostname;
	}
	
	public String getHostname() {
		return hostname;
	}

	public Integer getNodeId() {
		return nodeId;
	}

	
	// aggregate all the stats in one
	public TicketListingCrawlStat(Collection<TicketListingCrawlStat> stats) {
		reset();
		for (TicketListingCrawlStat stat: stats) {
			this.totalTimeToProcessCrawl += stat.getTotalTimeToProcessCrawl();
			this.totalProcessedTicketCount += stat.getTotalProcessedTicketCount();
			this.totalProcessedCrawlCount += stat.getTotalProcessedCrawlCount();
			/*
			addMap(this.totalProcessedTicketCountBySiteId, stat.getTotalProcessedCrawlCountBySiteId());
			addMap(this.totalTimeToProcessCrawlBySiteId, stat.getTotalTimeToProcessCrawlBySiteId());
			addMap(this.totalExpirationTimeBySiteId, stat.getTotalExpirationTimeBySiteId());
			addMap(this.totalFetchingTimeBySiteId, stat.getTotalFetchingTimeBySiteId());
			addMap(this.totalExtractionTimeBySiteId, stat.getTotalExtractionTimeBySiteId());
			addMap(this.totalIndexationTimeBySiteId, stat.getTotalIndexationTimeBySiteId());
			addMap(this.totalProcessedCrawlCountBySiteId, stat.getTotalProcessedCrawlCountBySiteId());
			addMap(this.totalFetchedByteCountBySiteId, stat.getTotalFetchedByteCountBySiteId());
			*/			
		}
	}
	
	// add map1 and map2 and put the result in map1
	private void addMap(Map<String, SiteStat> map1, Map<String, SiteStat> map2) {
		for (String siteId: map1.keySet()) {
			SiteStat stat1 = map1.get(siteId);
			SiteStat stat2 = map2.get(siteId);
			
			stat1.add(stat2);
		}
	}
	
	public double getAverageCrawlProcessingTime() {
		if (totalProcessedCrawlCount == 0) {
			return 0;
		}
		return (double) totalTimeToProcessCrawl / totalProcessedCrawlCount;
	}

	public double getAverageProcessedTicketCount() {
		if (totalProcessedCrawlCount == 0) {
			return 0;
		}
		return (double) totalProcessedTicketCount / totalProcessedCrawlCount;
	}

	/*
	public Map<String, Double> getAverageProcessingTimeBySiteId() {
		Map<String, Double> map = new HashMap<String, Double>();
		for (String siteId : Constants.getInstance().getSiteIds()) {
			if (totalProcessedCrawlCountBySiteId.get(siteId) == 0) {
				map.put(siteId, 0.0);
			} else {
				map.put(siteId, (double) totalTimeToProcessCrawlBySiteId
						.get(siteId)
						/ totalProcessedCrawlCountBySiteId.get(siteId));
			}
		}
		return map;
	}

	public Collection<Entry<String, Double>> getAverageProcessingTimeBySiteIdEntrySet() {
		return getAverageProcessingTimeBySiteId().entrySet();
	}

	public Map<String, Double> getAverageProcessedTicketCountBySiteId() {
		Map<String, Double> map = new HashMap<String, Double>();
		for (String siteId : Constants.getInstance().getSiteIds()) {
			if (totalProcessedCrawlCountBySiteId.get(siteId) == 0) {
				map.put(siteId, 0.0);
			} else {
				map.put(siteId, (double) totalProcessedTicketCountBySiteId
						.get(siteId)
						/ totalProcessedCrawlCountBySiteId.get(siteId));
			}
		}
		return map;
	}

	public Map<String, Double> getAverageFetchingTimeBySiteId() {
		Map<String, Double> map = new HashMap<String, Double>();
		for (String siteId : Constants.getInstance().getSiteIds()) {
			if (totalFetchingTimeBySiteId.get(siteId) == 0) {
				map.put(siteId, 0.0);
			} else {
				map.put(siteId, (double) totalFetchingTimeBySiteId.get(siteId)
						/ totalProcessedCrawlCountBySiteId.get(siteId));
			}
		}
		return map;
	}

	public Map<String, Double> getAverageExtractionTimeBySiteId() {
		Map<String, Double> map = new HashMap<String, Double>();
		for (String siteId : Constants.getInstance().getSiteIds()) {
			if (totalExtractionTimeBySiteId.get(siteId) == 0) {
				map.put(siteId, 0.0);
			} else {
				map.put(siteId, (double) totalExtractionTimeBySiteId
						.get(siteId)
						/ totalProcessedCrawlCountBySiteId.get(siteId));
			}
		}
		return map;
	}

	public Map<String, Double> getAverageIndexationTimeBySiteId() {
		Map<String, Double> map = new HashMap<String, Double>();
		for (String siteId : Constants.getInstance().getSiteIds()) {
			if (totalIndexationTimeBySiteId.get(siteId) == 0) {
				map.put(siteId, 0.0);
			} else {
				map.put(siteId, (double) totalIndexationTimeBySiteId
						.get(siteId)
						/ totalProcessedCrawlCountBySiteId.get(siteId));
			}
		}
		return map;
	}

	public Map<String, Double> getAverageExpirationTimeBySiteId() {
		Map<String, Double> map = new HashMap<String, Double>();
		for (String siteId : Constants.getInstance().getSiteIds()) {
			if (totalExpirationTimeBySiteId.get(siteId) == 0) {
				map.put(siteId, 0.0);
			} else {
				map.put(siteId, (double) totalExpirationTimeBySiteId
						.get(siteId)
						/ totalProcessedCrawlCountBySiteId.get(siteId));
			}
		}
		return map;
	}

	public Map<String, Double> getAverageFetchedByteCountBySiteId() {
		Map<String, Double> map = new HashMap<String, Double>();
		for (String siteId : Constants.getInstance().getSiteIds()) {
			if (totalFetchedByteCountBySiteId.get(siteId) == 0) {
				map.put(siteId, 0.0);
			} else {
				map.put(siteId, (double) totalFetchedByteCountBySiteId.get(siteId) / totalProcessedCrawlCountBySiteId.get(siteId));
			}
		}
		return map;
	}
*/
	

	public void reset() {
		totalProcessedTicketCount = 0;
		totalTimeToProcessCrawl = 0;
		totalProcessedCrawlCount = 0;

		for (String siteId : Constants.getInstance().getExtendedSiteIds()) {
			siteStatBySiteId.put(siteId, new SiteStat());
//			totalProcessedTicketCountBySiteId.put(siteId, 0L);
//			totalProcessedCrawlCountBySiteId.put(siteId, 0L);
//			totalExtractionTimeBySiteId.put(siteId, 0L);
//			totalFetchingTimeBySiteId.put(siteId, 0L);
//			totalExpirationTimeBySiteId.put(siteId, 0L);
//			totalTimeToProcessCrawlBySiteId.put(siteId, 0L);
//			totalIndexationTimeBySiteId.put(siteId, 0L);
//			totalTicketListingCrawlCountBySiteId.put(siteId, 0);
//			totalFetchedByteCountBySiteId.put(siteId, 0L);
//			bandwidthBySiteId.put(siteId, 0);
		}
		
		ticketHitIndexerStat.reset();
		resetCrawlerDelayStats();
	}

	public long getTotalTimeToProcessCrawl() {
		return totalTimeToProcessCrawl;
	}

	public void setTotalTimeToProcessCrawl(long totalTimeToProcessCrawl) {
		this.totalTimeToProcessCrawl = totalTimeToProcessCrawl;
	}

	public void incrementTotalTimeToProcessCrawl(long increment) {
		this.totalTimeToProcessCrawl += increment;
	}
	
	public long getTotalProcessedTicketCount() {
		return totalProcessedTicketCount;
	}

	public void setTotalProcessedTicketCount(long totalProcessedTicketCount) {
		this.totalProcessedTicketCount = totalProcessedTicketCount;
	}
	
	public void incrementTotalProcessedTicketCount(long increment) {
		this.totalProcessedTicketCount += increment;
	}

	public int getTotalProcessedCrawlCount() {
		return totalProcessedCrawlCount;
	}

	public void setTotalProcessedCrawlCount(int totalProcessedCrawlCount) {
		this.totalProcessedCrawlCount = totalProcessedCrawlCount;
	}
	
	public void incrementTotalProcessedCrawlCount(int increment) {
		this.totalProcessedCrawlCount += increment;
	}

//	public Map<String, Long> getTotalProcessedCrawlCountBySiteId() {
//		return totalProcessedCrawlCountBySiteId;
//	}

//	public void setTotalProcessedCrawlCountBySiteId(
//			Map<String, Long> totalProcessedCrawlCountBySiteId) {
//		this.totalProcessedCrawlCountBySiteId = totalProcessedCrawlCountBySiteId;
//	}
//
//	public void incrementTotalTimeToProcessCrawlBySiteId(String siteId, long increment) {
//		Long value = totalTimeToProcessCrawlBySiteId.get(siteId);
//		totalTimeToProcessCrawlBySiteId.put(siteId, value + increment);
//	}
//
//	public void incrementTotalProcessedCrawlCountBySiteId(String siteId, int increment) {
//		Long value = totalProcessedCrawlCountBySiteId.get(siteId);
//		totalProcessedCrawlCountBySiteId.put(siteId, value + increment);
//	}
//
//	public void incrementTotalFetchingTimeBySiteId(String siteId, long increment) {
//		Long value = totalFetchingTimeBySiteId.get(siteId);
//		totalFetchingTimeBySiteId.put(siteId, value + increment);
//	}
//
//	public void incrementTotalFetchedByteCountBySiteId(String siteId, long increment) {
//		Long value = totalFetchedByteCountBySiteId.get(siteId);
//		totalFetchedByteCountBySiteId.put(siteId, value + increment);
//	}
//
//	public void incrementTotalExtractionTimeBySiteId(String siteId, long increment) {
//		Long value = totalExtractionTimeBySiteId.get(siteId);
//		totalExtractionTimeBySiteId.put(siteId, value + increment);
//	}
//
//	public void incrementTotalIndexationTimeBySiteId(String siteId, long increment) {
//		Long value = totalIndexationTimeBySiteId.get(siteId);
//		totalIndexationTimeBySiteId.put(siteId, value + increment);
//	}
//
//	public void incrementTotalExpirationTimeBySiteId(String siteId, long increment) {
//		Long value = totalExpirationTimeBySiteId.get(siteId);
//		totalExpirationTimeBySiteId.put(siteId, value + increment);
//	}
//
//	public void incrementTotalProcessedTicketCountBySiteId(String siteId, long increment) {
//		Long value = totalProcessedTicketCountBySiteId.get(siteId);
//		totalProcessedTicketCountBySiteId.put(siteId, value + increment);
//	}
	
	public void computeCrawlerDelayStat(TicketListingCrawl crawl) {
		Date now = new Date();
		if (crawl.getEndCrawl() != null) {
			long delay = now.getTime()
					- (crawl.getEndCrawl().getTime() + crawl
							.getCrawlFrequency() * 1000);
			CrawlerDelayStat stat = delayByFrequency.get(crawl
					.getCrawlFrequency());
			if (stat == null) {
				stat = new CrawlerDelayStat(crawl.getCrawlFrequency());
				delayByFrequency.put(crawl.getCrawlFrequency(), stat);
			}

			stat.increment(delay);
		}
	}

	public void resetCrawlerDelayStats() {
		delayByFrequency.clear();
	}

	public Map<Integer, CrawlerDelayStat> getDelayByFrequency() {
		return delayByFrequency;
	}

	public Collection<Integer> getDelayByFrequencyKeys() {
		return delayByFrequency.keySet();
	}

	public Collection<CrawlerDelayStat> getDelayByFrequencyValues() {
		if (delayByFrequency == null || delayByFrequency.isEmpty()) {
			return null;
		}

		List<CrawlerDelayStat> list = new ArrayList<CrawlerDelayStat>(
				delayByFrequency.values());
		
		try {			
		list.sort(new Comparator<CrawlerDelayStat>() {			
			@Override
			public int compare(CrawlerDelayStat c1, CrawlerDelayStat c2) {
				if (c1.getFrequency() < c2.getFrequency()) {
					return -1;
				}

				if (c1.getFrequency() > c2.getFrequency()) {
					return 1;
				}

				return 0;
			}

		});
		}catch(Exception ex) {
			System.out.println("Exception in Sorting TicketListingCrawlStat line no 371 " + ex);
		}

		// compute the total average;
		CrawlerDelayStat finalStat = new CrawlerDelayStat(-1);
		for (CrawlerDelayStat stat : list) {
			finalStat.increment(stat.getTotalDelay(), stat.getTotalCount()
					- stat.getProActiveCount(), stat.getTotalTime());
			finalStat.increment(-stat.getTotalProActiveTime(), stat
					.getProActiveCount(), 0);
		}

		list.add(finalStat);

		return list;
	}	

//	public Map<String, Long> getTotalProcessedTicketCountBySiteId() {
//		return totalProcessedTicketCountBySiteId;
//	}
//
//	public void setTotalProcessedTicketCountBySiteId(
//			Map<String, Long> totalProcessedTicketCountBySiteId) {
//		this.totalProcessedTicketCountBySiteId = totalProcessedTicketCountBySiteId;
//	}
//
//	public Map<String, Long> getTotalTimeToProcessCrawlBySiteId() {
//		return totalTimeToProcessCrawlBySiteId;
//	}
//
//	public void setTotalTimeToProcessCrawlBySiteId(
//			Map<String, Long> totalTimeToProcessCrawlBySiteId) {
//		this.totalTimeToProcessCrawlBySiteId = totalTimeToProcessCrawlBySiteId;
//	}
//
//	public Map<String, Long> getTotalExpirationTimeBySiteId() {
//		return totalExpirationTimeBySiteId;
//	}
//
//	public void setTotalExpirationTimeBySiteId(
//			Map<String, Long> totalExpirationTimeBySiteId) {
//		this.totalExpirationTimeBySiteId = totalExpirationTimeBySiteId;
//	}
//
//	public Map<String, Long> getTotalFetchingTimeBySiteId() {
//		return totalFetchingTimeBySiteId;
//	}
//
//	public void setTotalFetchingTimeBySiteId(
//			Map<String, Long> totalFetchingTimeBySiteId) {
//		this.totalFetchingTimeBySiteId = totalFetchingTimeBySiteId;
//	}
//
//	public Map<String, Long> getTotalExtractionTimeBySiteId() {
//		return totalExtractionTimeBySiteId;
//	}

	
//	public void setTotalExtractionTimeBySiteId(
//			Map<String, Long> totalExtractionTimeBySiteId) {
//		this.totalExtractionTimeBySiteId = totalExtractionTimeBySiteId;
//	}
//
//	public Map<String, Long> getTotalIndexationTimeBySiteId() {
//		return totalIndexationTimeBySiteId;
//	}
//
//	public void setTotalIndexationTimeBySiteId(
//			Map<String, Long> totalIndexationTimeBySiteId) {
//		this.totalIndexationTimeBySiteId = totalIndexationTimeBySiteId;
//	}
//
//	public Map<String, Long> getTotalFetchedByteCountBySiteId() {
//		return totalFetchedByteCountBySiteId;
//	}
//
//	public void setTotalFetchedByteCountBySiteId(
//			Map<String, Long> totalFetchedByteCountBySiteId) {
//		this.totalFetchedByteCountBySiteId = totalFetchedByteCountBySiteId;
//	}

	public TicketHitIndexerStat getTicketHitIndexerStat() {
		return ticketHitIndexerStat;
	}

	public void setTicketHitIndexerStat(TicketHitIndexerStat ticketHitIndexerStat) {
		this.ticketHitIndexerStat = ticketHitIndexerStat;
	}

//	public Map<String, Integer> getTotalTicketListingCrawlCountBySiteId() {
//		return totalTicketListingCrawlCountBySiteId;
//	}
//
//	public void setTotalTicketListingCrawlCountBySiteId(
//			Map<String, Integer> totalTicketListingCrawlCountBySiteId) {
//		this.totalTicketListingCrawlCountBySiteId = totalTicketListingCrawlCountBySiteId;
//	}

	public Long getTotalProcessingTime() {
		Long timeToProcessAllCrawls = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			timeToProcessAllCrawls += (long)(stat.getAverageTimeToProcessCrawl() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return timeToProcessAllCrawls;
	}

	public Long getTotalFetchingTime() {
		Long totalFetchingTime = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalFetchingTime += (long)(stat.getAverageFetchingTime() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return totalFetchingTime;
	}

	public Long getTotalExtractionTime() {
		Long totalExtractionTime = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalExtractionTime += (long)(stat.getAverageExtractionTime() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return totalExtractionTime;
	}

	public Long getTotalIndexationTime() {
		Long totalIndexationTime = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalIndexationTime += (long)(stat.getAverageIndexationTime() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return totalIndexationTime;
	}

	public Long getTotalExpirationTime() {
		Long totalExpirationTime = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalExpirationTime += (long)(stat.getAverageExpirationTime() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return totalExpirationTime;
	}

	public Long getTotalFlushingTime() {
		Long totalFlushingTime = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalFlushingTime += (long)(stat.getAverageFlushingTime() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return totalFlushingTime;
	}

	public Long getTotalFetchedByteCount() {
		Long totalFetchedByteCount = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalFetchedByteCount += (long)(stat.getAverageFetchedByteCount() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return totalFetchedByteCount;
	}

	public Long getTotalDispatchingTime() {
		Long totalDispatchingTime = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalDispatchingTime += (long)(stat.getAverageDispatchingTime() * stat.getTotalTicketListingCrawlCount());			
		}
		
		return totalDispatchingTime;
	}

	public Long getTotalBandwidth() {
		Long totalBandwidth = 0L;
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = siteStatBySiteId.get(siteId);
			totalBandwidth += stat.getBandwidth();
		}

		return totalBandwidth;
	}

	public int getActiveThreadCount() {
		return activeThreadCount;
	}

	public void setActiveThreadCount(int activeThreadCount) {
		this.activeThreadCount = activeThreadCount;
	}

//	public Map<String, Integer> getBandwidthBySiteId() {
//		return bandwidthBySiteId;
//	}
//
//	public void setBandwidthBySiteId(Map<String, Integer> bandwidthBySiteId) {
//		this.bandwidthBySiteId = bandwidthBySiteId;
//	}

	public SystemStat getSystemStat() {
		return systemStat;
	}
	
	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Collection<Entry<Integer, Date>> getThreadLastUpdatedEntrySet() {
		return threadLastUpdatedMap.entrySet();
	}

	public Map<Integer, Date> getThreadLastUpdatedMap() {
		return threadLastUpdatedMap;
	}

	public void setThreadLastUpdatedMap(Map<Integer, Date> threadLastUpdatedMap) {
		this.threadLastUpdatedMap = threadLastUpdatedMap;
	}
	
	public Map<Integer, CrawlPhase> getCrawlPhaseMap() {
		return crawlPhaseMap;
	}

	public void setCrawlPhaseMap(Map<Integer, CrawlPhase> crawlPhaseMap) {
		this.crawlPhaseMap = crawlPhaseMap;
	}

	public Map<String, SiteStat> getSiteStatMap() {
		return siteStatBySiteId;
	}

	public SiteStat getSiteStat(String siteId) {
		return siteStatBySiteId.get(siteId);
	}
	
	public static class SiteStat implements Serializable {
		private Long totalProcessedTicketCount = 0L;
		private Long totalTimeToProcessCrawl = 0L;
		private Long totalExpirationTime = 0L;
		private Long totalFetchingTime = 0L;
		private Long totalExtractionTime = 0L;
		private Long totalIndexationTime = 0L;
		private Long totalProcessedCrawlCount = 0L;
		private Long totalFetchedByteCount = 0L;
		private Long totalTicketListingCrawlCount = 0L;
		private Long bandwidth = 0L;
		private Long totalDispatchingTime = 0L;
		private Long totalFlushingTime = 0L;
		private Integer activeTicketListingCrawl = 0;
		
		public void add(SiteStat stat) {
			incrementTotalExpirationTime(stat.getTotalExpirationTime());
			incrementTotalTimeToProcessCrawl(stat.getTotalTimeToProcessCrawl());
			incrementTotalExpirationTime(stat.getTotalExpirationTime());
			incrementTotalFetchingTime(stat.getTotalFetchingTime());
			incrementTotalExtractionTime(stat.getTotalExtractionTime());
			incrementTotalIndexationTime(stat.getTotalIndexationTime());
			incrementTotalProcessedCrawlCount(stat.getTotalProcessedCrawlCount());
			incrementTotalFetchedByteCount(stat.getTotalFetchedByteCount());
			incrementTotalTicketListingCrawlCount(stat.getTotalTicketListingCrawlCount());
			incrementTotalDispatchingTime(stat.getTotalDispatchingTime());
			incrementBandwidth(stat.getBandwidth());
			incrementTotalFlushingTime(stat.getTotalFlushingTime());
		}
		
		public Long getTotalProcessedTicketCount() {
			return totalProcessedTicketCount;
		}
		
		public void setTotalProcessedTicketCount(Long totalProcessedTicketCount) {
			this.totalProcessedTicketCount = totalProcessedTicketCount;
		}
		
		public void incrementTotalProcessedTicketCount(long increment) {
			this.totalProcessedTicketCount += increment;			
		}
		
		public Long getTotalTimeToProcessCrawl() {
			return totalTimeToProcessCrawl;
		}
		
		public void setTotalTimeToProcessCrawl(Long totalTimeToProcessCrawl) {
			this.totalTimeToProcessCrawl = totalTimeToProcessCrawl;
		}
		
		public Double getAverageTimeToProcessCrawl() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalTimeToProcessCrawl / totalProcessedCrawlCount;
		}

		public void incrementTotalTimeToProcessCrawl(long increment) {
			this.totalTimeToProcessCrawl += increment;
		}

		public Long getTotalExpirationTime() {
			return totalExpirationTime;
		}

		public void setTotalExpirationTime(Long totalExpirationTime) {
			this.totalExpirationTime = totalExpirationTime;
		}

		public Double getAverageExpirationTime() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalExpirationTime / totalProcessedCrawlCount;
		}

		public void incrementTotalExpirationTime(long increment) {
			this.totalExpirationTime += increment;
		}

		public Long getTotalFetchingTime() {
			return totalFetchingTime;
		}
		
		public void setTotalFetchingTime(Long totalFetchingTime) {
			this.totalFetchingTime = totalFetchingTime;
		}

		public Double getAverageFetchingTime() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalFetchingTime / totalProcessedCrawlCount;
		}

		public void incrementTotalFetchingTime(long increment) {
			this.totalFetchingTime += increment;
		}
		
		public Long getTotalExtractionTime() {
			return totalExtractionTime;
		}
		
		public void setTotalExtractionTime(Long totalExtractionTime) {
			this.totalExtractionTime = totalExtractionTime;
		}

		public Double getAverageExtractionTime() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalExtractionTime / totalProcessedCrawlCount;
		}

		public void incrementTotalExtractionTime(long increment) {
			this.totalExtractionTime += increment;
		}

		public Long getTotalIndexationTime() {
			return totalIndexationTime;
		}
		
		public void setTotalIndexationTime(Long totalIndexationTime) {
			this.totalIndexationTime = totalIndexationTime;
		}

		public Double getAverageIndexationTime() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalIndexationTime / totalProcessedCrawlCount;
		}

		public void incrementTotalIndexationTime(long increment) {
			this.totalIndexationTime += increment;
		}

		public Long getTotalProcessedCrawlCount() {
			return totalProcessedCrawlCount;
		}
		
		public void setTotalProcessedCrawlCount(Long totalProcessedCrawlCount) {
			this.totalProcessedCrawlCount = totalProcessedCrawlCount;
		}

		public Double getAverageProcessedCrawlCount() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalProcessedCrawlCount / totalProcessedCrawlCount;
		}

		public void incrementTotalProcessedCrawlCount(long increment) {
			this.totalProcessedCrawlCount += increment;
		}

		public Long getTotalFetchedByteCount() {
			return totalFetchedByteCount;
		}
		
		public void setTotalFetchedByteCount(Long count) {
			this.totalFetchedByteCount = count;
		}

		public Double getAverageFetchedByteCount() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalFetchedByteCount / totalProcessedCrawlCount;
		}

		public void incrementTotalFetchedByteCount(long increment) {
			this.totalFetchedByteCount += increment;
		}

		public Long getTotalTicketListingCrawlCount() {
			return totalTicketListingCrawlCount;
		}

		public void setTotalTicketListingCrawlCount(Long totalTicketListingCrawlCount) {
			this.totalTicketListingCrawlCount = totalTicketListingCrawlCount;
		}
		
		public Long getAverageProcessedTicketCount() {
			if (totalProcessedCrawlCount == 0) {
				return 0L;
			}
			return totalProcessedTicketCount / totalProcessedCrawlCount;
		}
		

		public Double getAverageTicketListingCrawlCount() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalFetchedByteCount / totalProcessedCrawlCount;
		}

		public void incrementTotalTicketListingCrawlCount(long increment) {
			this.totalTicketListingCrawlCount += increment;
		}

		public Long getBandwidth() {
			return bandwidth;
		}

		public void setBandwidth(Long bandwidth) {
			this.bandwidth = bandwidth;
		}

		public void incrementBandwidth(long increment) {
			this.bandwidth += increment;
		}

		public Long getTotalDispatchingTime() {
			return totalDispatchingTime;
		}

		public void setTotalDispatchingTime(Long totalDispatchingTime) {
			this.totalDispatchingTime = totalDispatchingTime;
		}

		public Double getAverageDispatchingTime() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalDispatchingTime / totalProcessedCrawlCount;
		}

		public void incrementTotalDispatchingTime(long increment) {
			this.totalDispatchingTime += increment;
		}

		public Long getTotalFlushingTime() {
			return totalFlushingTime;
		}

		public void setTotalFlushingTime(Long totalFlushingTime) {
			this.totalFlushingTime = totalFlushingTime;
		}

		public Double getAverageFlushingTime() {
			if (totalProcessedCrawlCount == 0) {
				return 0D;
			}
			return (double) totalFlushingTime / totalProcessedCrawlCount;
		}

		public void incrementTotalFlushingTime(long increment) {
			this.totalFlushingTime += increment;
		}

		public Integer getActiveTicketListingCrawl() {
			return activeTicketListingCrawl;
		}

		public void setActiveTicketListingCrawl(Integer activeTicketListingCrawl) {
			this.activeTicketListingCrawl = activeTicketListingCrawl;
		}
	}
}