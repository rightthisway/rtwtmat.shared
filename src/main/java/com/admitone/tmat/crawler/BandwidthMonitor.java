package com.admitone.tmat.crawler;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.admitone.tmat.crawler.TicketListingCrawlStat.SiteStat;
import com.admitone.tmat.web.Constants;

/**
 * This class monitor bandwidth of the crawlers. 
 */
public class BandwidthMonitor{
	private final static int BANDWIDTH_UPDATE_FREQUENCY = 10000;
	private final static double SMOOTHNESS_COEFFICIENT = 0.3;
	
	private TicketListingCrawlerThreadPool ticketListingCrawlerThreadPool;
	private Date lastUpdateDate;
	private Map<String, Long> totalFetchedByteCountBySiteId = new HashMap<String, Long>();
	private Map<String, Long> bandwidthBySiteId = new HashMap<String, Long>();
	
	public BandwidthMonitor(TicketListingCrawlerThreadPool ticketListingCrawlerThreadPool) {
		this.ticketListingCrawlerThreadPool = ticketListingCrawlerThreadPool;
		lastUpdateDate = new Date();
		
		TicketListingCrawlStat ticketListingCrawlStat = ticketListingCrawlerThreadPool.getTicketListingCrawlStat(); 
		
		for(String siteId: Constants.getInstance().getSiteIds()) {
			SiteStat stat = ticketListingCrawlStat.getSiteStat(siteId);
			totalFetchedByteCountBySiteId.put(siteId, stat.getTotalFetchedByteCount());
			bandwidthBySiteId.put(siteId, 0L);
		}
		
		Timer timer = new Timer();
		timer.schedule(new BandwidthMonitorTask(), BANDWIDTH_UPDATE_FREQUENCY, BANDWIDTH_UPDATE_FREQUENCY);
	}
	

	class BandwidthMonitorTask extends TimerTask {
		
		public void run() {
			Date now = new Date();
			
			TicketListingCrawlStat ticketListingCrawlStat = ticketListingCrawlerThreadPool.getTicketListingCrawlStat(); 
			Map<String, Long> newTotalFetchedByteCountBySiteId = new HashMap<String, Long>();
			for(String siteId: Constants.getInstance().getSiteIds()) {
				SiteStat stat = ticketListingCrawlStat.getSiteStat(siteId);
				newTotalFetchedByteCountBySiteId.put(siteId, stat.getTotalFetchedByteCount());
			}

			Map<String, Long> newBandwidthBySiteId = new HashMap<String, Long>();

			for(String siteId: Constants.getInstance().getSiteIds()) {
				newBandwidthBySiteId.put(siteId, 
						(long)(SMOOTHNESS_COEFFICIENT * (newTotalFetchedByteCountBySiteId.get(siteId) - totalFetchedByteCountBySiteId.get(siteId)) / ((now.getTime() - lastUpdateDate.getTime()) / 1000) 
							+ (1 - SMOOTHNESS_COEFFICIENT) * bandwidthBySiteId.get(siteId)));
			}
		
			
			totalFetchedByteCountBySiteId = newTotalFetchedByteCountBySiteId;
			bandwidthBySiteId = newBandwidthBySiteId;
			lastUpdateDate = now;
			
			for(String siteId: Constants.getInstance().getSiteIds()) {
				SiteStat stat = ticketListingCrawlStat.getSiteStat(siteId);
				stat.setBandwidth(bandwidthBySiteId.get(siteId));
			}
		}
		
	}
}
