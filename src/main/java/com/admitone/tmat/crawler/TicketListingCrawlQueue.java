package com.admitone.tmat.crawler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.enums.EventStatus;

public class TicketListingCrawlQueue  {
	private Logger logger = LoggerFactory.getLogger(TicketListingCrawlQueue.class); 
	private int sortingTimeSlice = 60; // default is one minute 
	private List<TicketListingCrawl> ticketListingCrawls;
	private CrawlerSchedulerManager crawlerSchedulerManager;
	private Integer crawlDurationThreshold;
	private Map<Integer,Event> eventMap = null;
	public CrawlerSchedulerManager getCrawlerSchedulerManager() {
		return crawlerSchedulerManager;
	}

	public void setCrawlerSchedulerManager(
			CrawlerSchedulerManager crawlerSchedulerManager) {
		this.crawlerSchedulerManager = crawlerSchedulerManager;
	}

	public void setTicketListingCrawls(List<TicketListingCrawl> ticketListingCrawls) {
		this.ticketListingCrawls = Collections.synchronizedList(ticketListingCrawls);
	}

	public TicketListingCrawlQueue(Collection<TicketListingCrawl> crawls) {
		ticketListingCrawls = Collections.synchronizedList(new ArrayList<TicketListingCrawl>(crawls));
		resort();
	}
	
	public void resort(TicketListingCrawl crawl) {
		resort();
	}

	public boolean remove(TicketListingCrawl crawlToRemove) {
		if (crawlToRemove == null) {
			logger.warn("trying to delete a crawl which is null");
			return false;
		}
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getId().equals(crawlToRemove.getId())) {
				synchronized(ticketListingCrawls) {
					ticketListingCrawls.remove(crawl);
				}
				return true;
			}
		}
		resort();
		return false;
	}
	
	public void removeTicketListingCrawlByArtistId(Integer artistId) {
		for (TicketListingCrawl crawl : getTicketListingCrawlByArtistId(artistId)) {
			remove(crawl);
		}
	}
	
	public Collection<TicketListingCrawl> getTicketListingCrawlByArtistId(int artistId) {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		// TODO: use a hashmap instead
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEvent() != null && crawl.getEvent().getArtistId() != null && crawl.getEvent().getArtistId() == artistId) {
				result.add(crawl);
			}
		}
		return result;
	}

	public boolean update(TicketListingCrawl crawlToUpdate) {
		TicketListingCrawl crawl = getTicketListingCrawlById(crawlToUpdate.getId());
		if (crawl == null) {
			return false;
		}
		
		crawl.update(crawlToUpdate);
		
		return true;
	}

	public boolean exists(TicketListingCrawl crawlToAdd) {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.isEquivalent(crawlToAdd)) {
				return true;
			}
		}
		return false;
	}

	public boolean add(TicketListingCrawl crawlToAdd) {
		if (exists(crawlToAdd)) {
			return false;
		}

		synchronized(ticketListingCrawls) {
			ticketListingCrawls.add(crawlToAdd);
		}
//		resort();
		
		return true;
	}
	
	
	private Date parseDate(String date, String format) throws Exception
	{
	   
		SimpleDateFormat formatter = new SimpleDateFormat(format);
	    return formatter.parse(date);
	}
	
	public synchronized void resort() {
		if(eventMap==null){
			eventMap = new HashMap<Integer, Event>();
			for(Event event:DAORegistry.getEventDAO().getAll()){
				eventMap.put(event.getId(), event);
			}
		}
		synchronized(ticketListingCrawls) {
			
			boolean isSorted = true;
			
			try {
				System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
				Collections.sort(ticketListingCrawls, new TicketListCrawlComparator());
			}catch(Exception e) {
				logger.error("Error Occured while sorting TicketListingCrawlsQueue");
				e.printStackTrace();
				isSorted = false;
			}
			
			if(!isSorted) {
				
				try {
					System.setProperty("java.util.Arrays.useLegacyMergeSort", "true");
					ticketListingCrawls.sort((TicketListingCrawl crawl1, TicketListingCrawl crawl2) -> {
						if(crawl1.getPriority()!=null && crawl2.getPriority()!=null && crawl1.getPriority()!= crawl2.getPriority()){
							if(crawl1.getPriority()>crawl2.getPriority()){
								return -1;
							}else if(crawl1.getPriority()<crawl2.getPriority()){
								return 1;
							}
						}else if(crawl1.getPriority()!=null ){
							return 1;
						}else if(crawl2.getPriority()!=null){
							return -1;
						}else if (crawl1.getStartCrawl() == null) {
							if (crawl2.getStartCrawl() == null) {
								Event event1 = eventMap.get(crawl1.getEventId());
								if(event1==null){
									event1=crawl1.getEvent();
									if(event1!=null){
										eventMap.put(event1.getId(), event1);
									}
								}
								Event event2 = eventMap.get(crawl2.getEventId());
								if(event2==null){
									event2=crawl2.getEvent();
									if(event2!=null){
										eventMap.put(event2.getId(), event2);
									}
								}
								
								if (event1 == null && event2 == null) {
									return 0;
								}
								
								if (event1 == null) {
									return -1;
								}
								
								if (event2 == null) {
									return 1;
								}
								if(event1.getDate()==null && (event2.getDate()==null)){
									return 0;
								}
								if(event1.getDate()==null){
									return -1;
								}
								if(event2.getDate()==null){
									return 1;
								}
								return event2.getDate().compareTo(event1.getDate());
								
							}
							return -1;
						} else if (crawl2.getStartCrawl() == null) {
							return 1;
						} else {
							Date now = new Date();
							if (crawl1.getEndCrawl() != null && crawl2.getEndCrawl() != null) {
								int nextTime1 = (int)(crawl1.getCrawlFrequency() - (now.getTime() - crawl1.getEndCrawl().getTime()) / 1000);
								int nextTime2 = (int)(crawl2.getCrawlFrequency() - (now.getTime() - crawl2.getEndCrawl().getTime()) / 1000);
								
								int slice1 = (nextTime1 / sortingTimeSlice);
								int slice2 = (nextTime2 / sortingTimeSlice);
								
								if (slice1 < slice2) {
									return -1;
								} else if (slice1 > slice2) {
									return 1;
								}
							}
							
							// if they belong to the same slice and have same priority, give
							// precedence to the one with the higher priority otherwise they'll
							// never starts
							
							if (crawl1.getCrawlFrequency() > crawl2.getCrawlFrequency()) {
								return -1;
							}

							if (crawl1.getCrawlFrequency() < crawl2.getCrawlFrequency()) {
								return 1;
							}
						
							return 0;
						}
						return 0;
					});
				}catch(Exception e) {
					e.printStackTrace();
				}
			}
			 
			
			
			/*try {
				   final String DATE_FMT = "mm/dd/YYYY HH:mm z";				
				   ticketListingCrawls.sort(new Comparator<TicketListingCrawl>() {
				    @Override
				    public int compare(final TicketListingCrawl model1, final TicketListingCrawl model2) {
				    			    	
				    	
					     int eventId1 = model1.getEventId();
					    int eventId2 = model2.getEventId();
					    Event event1 = (Event) eventMap.get(eventId1);
					    Event event2 = (Event) eventMap.get(eventId2);
					    Date date1 = null;
					    Date date2 =null;
					    
					    				    
					    int compResult = 0;
					    
					   
					    try {
					    	
					    	compResult = model1.compareTo(model2);
					    	 date1 = parseDate(event1.getFormatedDate() , DATE_FMT);
					    	 date2 = parseDate(event2.getFormatedDate() , DATE_FMT);
					    }catch(Exception ex) {
					    	System.out.println("Exception Comparing events date:TicketListigCrawlQueue " + ex );
					    }
					    if(date1 == null) return -1;
					    if(date2 == null ) return 1;					   
					    compResult = date1.compareTo(date2);				   
					    if (compResult == 0)
					    {
					    	if(event1.getName() == null) return -1;
					    	if(event2.getName() == null) return  1;
					    	compResult = event1.getName().compareTo(event2.getName());					    		
					    }					    	
					    return compResult;
				    	
					}
				});
				
				 
			}catch(Exception e) {
				e.printStackTrace();
			}*/
			
			
		}
	}
	
	public int comparedfdfd(TicketListingCrawl crawl1, TicketListingCrawl crawl2) {
		if(crawl1.getPriority()!=null && crawl2.getPriority()!=null && crawl1.getPriority()!= crawl2.getPriority()){
			if(crawl1.getPriority()>crawl2.getPriority()){
				return -1;
			}else if(crawl1.getPriority()<crawl2.getPriority()){
				return 1;
			}
		}else if(crawl1.getPriority()!=null ){
			return 1;
		}else if(crawl2.getPriority()!=null){
			return -1;
		}else if (crawl1.getStartCrawl() == null) {
			if (crawl2.getStartCrawl() == null) {
				Event event1 = eventMap.get(crawl1.getEventId());
				if(event1==null){
					event1=crawl1.getEvent();
					if(event1!=null){
						eventMap.put(event1.getId(), event1);
					}
				}
				Event event2 = eventMap.get(crawl2.getEventId());
				if(event2==null){
					event2=crawl2.getEvent();
					if(event2!=null){
						eventMap.put(event2.getId(), event2);
					}
				}
				
				if (event1 == null && event2 == null) {
					return 0;
				}
				
				if (event1 == null) {
					return -1;
				}
				
				if (event2 == null) {
					return 1;
				}
				if(event1.getDate()==null && (event2.getDate()==null)){
					return 0;
				}
				if(event1.getDate()==null){
					return -1;
				}
				if(event2.getDate()==null){
					return 1;
				}
				return event2.getDate().compareTo(event1.getDate());
				
			}
			return -1;
		} else if (crawl2.getStartCrawl() == null) {
			return 1;
		} else {
			Date now = new Date();
			if (crawl1.getEndCrawl() != null && crawl2.getEndCrawl() != null) {
				int nextTime1 = (int)(crawl1.getCrawlFrequency() - (now.getTime() - crawl1.getEndCrawl().getTime()) / 1000);
				int nextTime2 = (int)(crawl2.getCrawlFrequency() - (now.getTime() - crawl2.getEndCrawl().getTime()) / 1000);
				
				int slice1 = (nextTime1 / sortingTimeSlice);
				int slice2 = (nextTime2 / sortingTimeSlice);
				
				if (slice1 < slice2) {
					return -1;
				} else if (slice1 > slice2) {
					return 1;
				}
			}
			
			// if they belong to the same slice and have same priority, give
			// precedence to the one with the higher priority otherwise they'll
			// never starts
			
			if (crawl1.getCrawlFrequency() > crawl2.getCrawlFrequency()) {
				return -1;
			}

			if (crawl1.getCrawlFrequency() < crawl2.getCrawlFrequency()) {
				return 1;
			}
		
			return 0;
		}
		return 0;
	}

	public int getSortingTimeSlice() {
		return sortingTimeSlice;
	}

	public void setSortingTimeSlice(int sortingTimeSlice) {
		this.sortingTimeSlice = sortingTimeSlice;
	}
	
	public Collection<TicketListingCrawl> getAll() {
		return ticketListingCrawls;
	}
	
	public Collection<TicketListingCrawl> getTicketListingCrawlsByArtistId(int artistId) {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEvent() != null && crawl.getEvent().getArtistId() != null && crawl.getEvent().getArtistId().equals(artistId)) {
				result.add(crawl);
			}
		}
		return result;
	}

	public Collection<TicketListingCrawl> getFailedTicketListingCrawlsByEventId() {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		Collection<TicketListingCrawl> failedCrawls = new ArrayList<TicketListingCrawl>();
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.isEnabled()
					&& crawl.getCrawlState().isError()) {
				failedCrawls.add(crawl);
			}
		}
		return failedCrawls;
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlsByEventId(int eventId) {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEventId() != null && crawl.getEventId() == eventId) {
				result.add(crawl);
			}
		}
		return result;
	}

	public Collection<TicketListingCrawl> getActiveTicketListingCrawlsByEventId(int eventId) {
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		for (TicketListingCrawl crawl : getTicketListingCrawlsByEventId(eventId)) {
			if (crawl.isEnabled()) {
				result.add(crawl);
			}
		}
		return result;
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlByCreator(String username) {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getCreator() == null
					|| !crawl.getCreator().equals(username)) {
				continue;
			}

			Event event = crawl.getEvent();
			if (event != null
					&& event.getEventStatus().equals(EventStatus.ACTIVE)) {
				result.add(crawl);
			}
		}
		return result;
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlByLastUpdater(String username) {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getLastUpdater() == null
					|| !crawl.getLastUpdater().equals(username)) {
				continue;
			}

			Event event = crawl.getEvent();
			if (event != null
					&& event.getEventStatus().equals(EventStatus.ACTIVE)) {
				result.add(crawl);
			}
		}
		return result;
	}

	public TicketListingCrawl getTicketListingCrawlById(int id) {
		// TODO: use a hashmap instead
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getId() != null &&  crawl.getId().equals(id)) {
				return crawl;
			}
		}
		return null;
	}
	
	public TicketListingCrawl getTicketListingCrawlByName(String name) {
		// TODO: use a hashmap instead
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getName().equals(name)) {
				return crawl;
			}
		}
		return null;
	}
	
	public Collection<TicketListingCrawl> getTicketListingCrawls() {
		return new ArrayList<TicketListingCrawl>(ticketListingCrawls);
	}

	public void recomputeCrawlFrequency() {
		logger.info("recompute");
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawls);
		for (TicketListingCrawl crawl : crawls) {
			Event event = crawl.getEvent();
			if (event == null) {
				continue;
			}
			// TBD issue.. Need to discuss with business 
			if (event.getLocalDate() == null) {
				continue;
			}

			if (event.getArtist() == null || event.getArtist().getGrandChildTourCategory()==null || event.getArtist().getGrandChildTourCategory().getChildTourCategory() == null || event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory()==null) {
				continue;
			}
			
			if (crawl.getAutomaticCrawlFrequency()) {
				crawl.setCrawlFrequency(crawlerSchedulerManager.getFrequency(event.getLocalDate(),
						event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName()));
			}
		}
		resort();
	}	

	public Integer getCrawlDurationThreshold() {
		return crawlDurationThreshold;
	}

	public void setCrawlDurationThreshold(Integer crawlDurationThreshold) {
		this.crawlDurationThreshold = crawlDurationThreshold;
	}
	
	class TicketListCrawlComparator implements Comparator<TicketListingCrawl> {
		@Override
		public int compare(final  TicketListingCrawl crawl1, final TicketListingCrawl crawl2) {
			if(crawl1.getPriority()!=null && crawl2.getPriority()!=null && crawl1.getPriority()!= crawl2.getPriority()){
				if(crawl1.getPriority()>crawl2.getPriority()){
					return -1;
				}else if(crawl1.getPriority()<crawl2.getPriority()){
					return 1;
				}
			}else if(crawl1.getPriority()!=null ){
				return 1;
			}else if(crawl2.getPriority()!=null){
				return -1;
			}else if (crawl1.getStartCrawl() == null) {
				if (crawl2.getStartCrawl() == null) {
					Event event1 = eventMap.get(crawl1.getEventId());
					if(event1==null){
						event1=crawl1.getEvent();
						if(event1!=null){
							eventMap.put(event1.getId(), event1);
						}
					}
					Event event2 = eventMap.get(crawl2.getEventId());
					if(event2==null){
						event2=crawl2.getEvent();
						if(event2!=null){
							eventMap.put(event2.getId(), event2);
						}
					}
					
					if (event1 == null && event2 == null) {
						return 0;
					}
					
					if (event1 == null) {
						return -1;
					}
					
					if (event2 == null) {
						return 1;
					}
					if(event1.getDate()==null && (event2.getDate()==null)){
						return 0;
					}
					if(event1.getDate()==null){
						return -1;
					}
					if(event2.getDate()==null){
						return 1;
					}
					return event2.getDate().compareTo(event1.getDate());
					
				}
				return -1;
			} else if (crawl2.getStartCrawl() == null) {
				return 1;
			} else {
				Date now = new Date();
				if (crawl1.getEndCrawl() != null && crawl2.getEndCrawl() != null) {
					int nextTime1 = (int)(crawl1.getCrawlFrequency() - (now.getTime() - crawl1.getEndCrawl().getTime()) / 1000);
					int nextTime2 = (int)(crawl2.getCrawlFrequency() - (now.getTime() - crawl2.getEndCrawl().getTime()) / 1000);
					
					int slice1 = (nextTime1 / sortingTimeSlice);
					int slice2 = (nextTime2 / sortingTimeSlice);
					
					if (slice1 < slice2) {
						return -1;
					} else if (slice1 > slice2) {
						return 1;
					}
				}
				
				// if they belong to the same slice and have same priority, give
				// precedence to the one with the higher priority otherwise they'll
				// never starts
				
				if (crawl1.getCrawlFrequency() > crawl2.getCrawlFrequency()) {
					return -1;
				}

				if (crawl1.getCrawlFrequency() < crawl2.getCrawlFrequency()) {
					return 1;
				}
			
				return 0;
			}
			return 0;
		}
	}

}
