package com.admitone.tmat.crawler;

import org.gridgain.grid.GridConfigurationAdapter;
import org.gridgain.grid.GridFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GridGainCrawlerWorker {
	public static void start() throws Exception {
		System.out.println("==================================================");
		System.out.println("TMAT Crawler");
		System.out.println("");
		System.out.println("(c) 2010 Admit One LLC (GridGain Version)");
		System.out.println("==================================================");
		System.out.println();
		GridConfigurationAdapter adapter = new GridConfigurationAdapter();
		adapter.setPeerClassLoadingEnabled(false);
		GridFactory.start(adapter, new ClassPathXmlApplicationContext(new String[] {
				"spring-dao-database.xml",
				"spring-utils.xml",
				"spring-fetchers.xml",
				"spring-crawler.xml",
				"spring-crawler-pool.xml",
				
				"spring-gridgain.xml",
				"spring-ticketdao-coherence.xml"
		}));
//		new ClassPathXmlApplicationContext(new String[] {
//				"spring-dao-database.xml",
//				"spring-utils.xml",
//				"spring-fetchers.xml",
//				"spring-crawler-pool.xml",
//				"spring-gridgain.xml"
//		});
		System.out.println("Spring context loaded");
	}

	// kill all processes running on this server
	public static void stop() {
		System.out.println("Stopping crawler on this machine...");
		System.out.println("Please wait...");
		new ClassPathXmlApplicationContext(new String[] {
				"spring-dao-database.xml",
				"spring-utils.xml",
				"spring-fetchers.xml",
				"spring-crawler.xml",
				"spring-crawler-pool.xml",
				"spring-stop-crawler.xml",
				"spring-gridgain.xml"
		});		
	}
	
	private static void printUsage() {
		System.out.println("Possible arguments: start or stop");
	}
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			printUsage();
			return;
		}
		
		if (args[0].equalsIgnoreCase("start")) {
			start();
		} else if (args[0].equalsIgnoreCase("stop")) {
			stop();
		}
	}

}
