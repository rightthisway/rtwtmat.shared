package com.admitone.tmat.crawler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import edu.emory.mathcs.backport.java.util.Collections;

public class TicketListingCrawlerTimeOutManager {
	private Logger logger = LoggerFactory.getLogger(TicketListingCrawlerTimeOutManager.class);
	
	private Collection<MonitoredTicketListingCrawlerTask> monitoredTicketListingCrawlerTasks = Collections.synchronizedCollection(new ArrayList<MonitoredTicketListingCrawlerTask>());

	public TicketListingCrawlerTimeOutManager() {
		Timer timer = new Timer();
		
		// check that the crawls do not get timed out every 10s
		timer.schedule(new TimerTask() {
			public void run() {
				logger.info("RUNNING TIMED OUT TASK"+new Date());
				try {
					Long currentTime = new Date().getTime();
					Collection<MonitoredTicketListingCrawlerTask> tasks = new ArrayList<MonitoredTicketListingCrawlerTask>(monitoredTicketListingCrawlerTasks);
					for(MonitoredTicketListingCrawlerTask task: tasks) {
						// to avoid cancelling a task for a different crawl
						if (task.getJobCounter() != task.getTicketListingCrawlerTask().getJobCounter()) {
							continue;
						}
						
						if (currentTime - task.getInsertionDate().getTime() < task.getTimedOut()) {
							continue;
						}
						
						TicketListingCrawl crawl = task.getTicketListingCrawlerTask().getProcessedCrawl();
						logger.info("***************** TIMED OUT for " + crawl);
						// note: do not call cancel, as cancel set running to false making
						// the thread died prematurely
						if (crawl != null) {
							crawl.setCancelled(true);
							logger.info("*** CANCEL TIMEOUT ***");
						}
					
						Thread thread = task.getTicketListingCrawlerTask().getCrawlerThread();
						if (thread != null) {
							logger.info("*** TIMEOUT Thread Intrupted ***"+crawl+" : "+new Date());
							thread.interrupt();
						}
						
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				logger.info("DONE RUNNING TIMED OUT TASK");
			}
		}, 1, 10 * 1000L);

	}
	
	public void monitorTicketListingCrawlerTask(TicketListingCrawlerTask ticketListingCrawlerTask, int jobCounter, long timedOut) {
		monitoredTicketListingCrawlerTasks.add(new MonitoredTicketListingCrawlerTask(ticketListingCrawlerTask, jobCounter, timedOut));
	}

	public void unmonitorTicketListingCrawlerTask(TicketListingCrawlerTask ticketListingCrawlerTask) {
		monitoredTicketListingCrawlerTasks.remove(ticketListingCrawlerTask);
	}

		
	class MonitoredTicketListingCrawlerTask {
		private TicketListingCrawlerTask ticketListingCrawlerTask;
		private int jobCounter;
		private long timedOut;
		private Date insertionDate;

		public MonitoredTicketListingCrawlerTask(TicketListingCrawlerTask ticketListingCrawlerTask, int jobCounter, long timedOut) {
			this.ticketListingCrawlerTask = ticketListingCrawlerTask;
			this.jobCounter = jobCounter;
			this.timedOut = timedOut;
			this.insertionDate = new Date();
		}
		
		public TicketListingCrawlerTask getTicketListingCrawlerTask() {
			return ticketListingCrawlerTask;
		}
		
		public int getJobCounter() {
			return jobCounter;
		}

		public long getTimedOut() {
			return timedOut;
		}

		public Date getInsertionDate() {
			return insertionDate;
		}


	}
}
