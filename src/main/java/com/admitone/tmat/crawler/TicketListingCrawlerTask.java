package com.admitone.tmat.crawler;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.com.TicketListingCrawlReceiver;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.TicketPersistenceTracker;

public class TicketListingCrawlerTask extends TimerTask implements Serializable {
	public static final long EBAY_CRAWL_TIMEOUT = 30L * 60L * 1000L;
	public static final long CRAWL_TIMEOUT = 2L * 60L * 1000L;
	private Logger logger = LoggerFactory.getLogger(TicketListingCrawlerTask.class);
	private Map<String, TicketListingFetcher> ticketListingFetcherBySiteId;
	private TicketHitIndexer ticketHitIndexer;
	private TicketListingCrawlReceiver ticketListingCrawlReceiver;
	private TicketListingCrawlerThreadPool threadPool;

	private Date lastRun;
	private boolean running = true;
	private boolean processingCrawl = false;
	private TicketListingCrawl processedCrawl;
	private TicketUpdater ticketUpdater;
	private Integer id;

//	private static Object startTicketListingCrawlLock = new Object();

	private Thread crawlerThread;
	private int jobCounter = 0;

	public TicketListingCrawlerTask(TicketListingCrawlerThreadPool threadPool, Integer id, TicketListingCrawlReceiver ticketListingCrawlReceiver) {
		super();
		this.threadPool = threadPool;
		this.id = id;
		this.ticketListingCrawlReceiver=ticketListingCrawlReceiver;
	}

	public boolean cancel() {
		super.cancel();
		this.running = false;
		if (processedCrawl != null) {
			processedCrawl.setCancelled(true);
			logger.info("*** CANCEL TIMEOUT *** " + new Date());
		}
		if (crawlerThread != null) {
			crawlerThread.interrupt();
		}
		return true;
	}

	public void cancelCrawlForArtist(Integer artistId) {
		if (processedCrawl != null && processedCrawl.getEvent().getArtistId().equals(artistId)) {
			processedCrawl.setSuspended(true);
			processedCrawl.setCrawlState(CrawlState.STOPPED);
			if (crawlerThread != null) {
				crawlerThread.interrupt();
			}
		}
	}

	public void cancelCrawlForEvent(Integer eventId) {
		if (processedCrawl != null && processedCrawl.getEventId().equals(eventId)) {
			processedCrawl.setSuspended(true);
			processedCrawl.setCrawlState(CrawlState.STOPPED);
			if (crawlerThread != null) {
				crawlerThread.interrupt();
			}
		}
	}

	public void run() {
		crawlerThread = Thread.currentThread();
		Date now = new Date();

		// to prevent two execution at the same time of this method
		synchronized (this) {
			if (lastRun != null
					&& (now.getTime() - lastRun.getTime()) < 60 * 1000) {
				return;
			}
			lastRun = now;
		}

		TicketListingCrawl ticketListingCrawl = null;
		while (running) {
			try {
				threadPool.updateLastUpdated(id);
				if(ticketListingCrawlReceiver.getMasterNodeId().equals(ticketListingCrawlReceiver.getNodeId())){
					Thread.sleep(24 * 60 * 1000L);
					running=false;
					break;
				}
				ticketListingCrawl = threadPool.startTicketListingCrawl();
							
				if (ticketListingCrawl == null) {
					try {
						System.out.println("Inside TCTask Sleep : "+ new Date());
						Thread.sleep(5000L);
					} catch (InterruptedException e) {
						e.printStackTrace();
						logger.error("Crawler Task interrupted", e);
					}
					continue;
				}
				Date startTime = new Date(); 
				//System.out.println("TICKET LISTING CRAWL  START=" + ticketListingCrawl+" : "+new Date());
				logger.info("TICKET LISTING CRAWL  START=" + ticketListingCrawl+" : "+new Date());
				TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(),CrawlState.QUEUED);
				String siteId = ticketListingCrawl.getSiteId();
				TicketListingFetcher fetcher = ticketListingFetcherBySiteId.get(siteId);
				if (fetcher == null) {
					threadPool.stopTicketListingCrawl(ticketListingCrawl, CrawlState.EXCEPTION,
							new Exception("The site id " + siteId + " does not have a fetcher"));
					continue;
				}
				jobCounter++;
				boolean isFlushed =false;
				try {
					processingCrawl = true;
					processedCrawl = ticketListingCrawl;
					// set the timeout to 10 mn

					// run the cancel after 10mn, then once every 10seconds (in
					// case the first cancel don't work out)
					long crawlTimeOut;
					if (processedCrawl.getSiteId().equals(Site.EBAY)) {
						crawlTimeOut = EBAY_CRAWL_TIMEOUT;
					} else {
						crawlTimeOut = CRAWL_TIMEOUT;
					}
					
					threadPool.getTicketListingCrawlerTimeOutManager().monitorTicketListingCrawlerTask(this, jobCounter, crawlTimeOut);

					// timer.schedule(new TicketListingCrawlerTimeOutTask(this),
					// 30 * 1000);

					ticketHitIndexer.clear(ticketListingCrawl);
					ticketListingCrawl.setCrawlPhase(CrawlPhase.EXTRACTION);
					try{
						Boolean finished = fetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
						
						if (null == finished) {
							TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(), CrawlState.EXCEPTION);
							///System.out.println("check exception initiated here===========" +ticketListingCrawl.getId()+":"+ticketListingCrawl.getSiteId()+" TO: "+crawlTimeOut+" start: "+startTime+" end: "+new Date());
							logger.error("check exception initiated here===========" +ticketListingCrawl.getId()+":"+ticketListingCrawl.getSiteId()+" TO: "+crawlTimeOut+" start: "+startTime+" end: "+new Date());
							throw new Exception("00001 : THREAD INTERRUPTED : INTERRUPTED CRAWL ID :"+ticketListingCrawl.getId()+", EXCEPTION :"+ticketListingCrawl.getCrawlExceptionStackTrace());
						}
						
						if (ticketListingCrawl.isCancelled()) {
							TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(), CrawlState.EXCEPTION);
							throw new TimedOutTicketListingCrawlException("00002 : TIMED OUT  CRAWL ID :"+ticketListingCrawl.getId()+", EXCEPTION :"+ticketListingCrawl.getCrawlExceptionStackTrace());
						}

						if (!finished) {
							TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(), CrawlState.EXCEPTION);
							///System.out.println("check exception initiated here===========" +ticketListingCrawl.getId()+":"+ticketListingCrawl.getSiteId()+" TO: "+crawlTimeOut+" start: "+startTime+" end: "+new Date());
							logger.error("check exception initiated here===========" +ticketListingCrawl.getId()+":"+ticketListingCrawl.getSiteId()+" TO: "+crawlTimeOut+" start: "+startTime+" end: "+new Date());
							throw new Exception("00002 : THREAD INTERRUPTED : INTERRUPTED CRAWL ID :"+ticketListingCrawl.getId()+", EXCEPTION :"+ticketListingCrawl.getCrawlExceptionStackTrace());
						}
						
					}catch(Exception e){
						e.printStackTrace();
						TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(), CrawlState.EXCEPTION);
						///System.out.println("check exception initiated here===========" +ticketListingCrawl.getId()+":"+ticketListingCrawl.getSiteId()+" TO: "+crawlTimeOut+" start: "+startTime+" end: "+new Date());
						logger.error("check exception initiated here===========" +ticketListingCrawl.getId()+":"+ticketListingCrawl.getSiteId()+" TO: "+crawlTimeOut+" start: "+startTime+" end: "+new Date());
						throw new Exception("00003 : THREAD INTERRUPTED : INTERRUPTED CRAWL ID :"+ticketListingCrawl.getId()+", EXCEPTION :"+ticketListingCrawl.getCrawlExceptionStackTrace());
					}
					
					
					// cancel the time out
					threadPool.getTicketListingCrawlerTimeOutManager().unmonitorTicketListingCrawlerTask(this);
					String name=ticketListingCrawl.getName();
					//System.out.println(name + " Crawler before flush..ULAGA");
					ticketListingCrawl.setCrawlPhase(CrawlPhase.FLUSH);
					Long startFlushingTime = System.currentTimeMillis();
					
					try{
						isFlushed = ticketHitIndexer.flush(ticketListingCrawl);
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Exception Occured While Flushing Err : " +ticketListingCrawl+" : "+ new Date());
						//System.out.println("ULAGA : Exception Occured While Flushing : ULAGA : EXCEPTION  :"+e.fillInStackTrace());
					}
					System.out.println(name + " Crawler After flush.."+ticketListingCrawl.getSiteId()+" : "+ticketListingCrawl.getId()+" : "+new Date());
					// ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime()
					// + System.currentTimeMillis() - startFlushingTime);
					ticketListingCrawl.setFlushingTime(System
								.currentTimeMillis() - startFlushingTime);
					Long startExpirationTime = System.currentTimeMillis();
					//System.out.println(name + " Crawler after flush.." + isFlushed + ":" + ticketListingCrawl.getId()+" : ULAGA");
					if(!isFlushed){
						ticketUpdater.updateOutDatedExpiredTicketsFromCrawl(ticketListingCrawl);
						//System.out.println(name + " Crawler after updateOutDatedExpiredTicketsFromCrawl.. : ULAGA");
						ticketUpdater.updateTicketsUpdatedBeforeFromCrawl(ticketListingCrawl, ticketListingCrawl.getStartCrawl());
					}
					ticketListingCrawl.setExpirationTime(System.currentTimeMillis() - startExpirationTime);
					//System.out.println(name + " Crawler after updateTicketsUpdatedBeforeFromCrawl.. : ULAGA");
					try{
						threadPool.stopTicketListingCrawl(ticketListingCrawl, CrawlState.STOPPED, null);
					}catch(Exception e){
						e.printStackTrace();
						System.out.println(name + " ERROR OCCURED While STOPPING CRAWLER : ULAGA :  EXCEPTION : "+e.fillInStackTrace());
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.error("Error while crawl is processed : "+ticketListingCrawl.getId()+" : "+new Date());
					//System.out.println("ULAGA - Error while crawl is processed : EXCEPTION : "+ticketListingCrawl.getId()+" : "+new Date());
					
					logger.error(ticketListingCrawl.getId() + " - Error while crawl is processed", e.fillInStackTrace());
					//System.out.println("ULAGA - Error while crawl is processed : EXCEPTION : "+e.fillInStackTrace());
					e.printStackTrace();
					TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(), CrawlState.EXCEPTION);
					threadPool.getTicketListingCrawlerTimeOutManager().unmonitorTicketListingCrawlerTask(this);
					
//					System.out.println(ticketListingCrawl.getName() + " Error 1 while crawl is processed" + e);
					try {
						int ticketTimeToLive = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.ticket.ttl").getValue());
						Calendar calendar = Calendar.getInstance();
						calendar.add(Calendar.MINUTE, -ticketTimeToLive);
						logger.info("EXPIRE ALL TICKET BEFORE "
								+ calendar.getTime());
						Long startExpirationTime = System.currentTimeMillis();
						if(!isFlushed){
							ticketUpdater.updateOutDatedExpiredTicketsFromCrawl(ticketListingCrawl);
							ticketUpdater.updateTicketsUpdatedBeforeFromCrawl(ticketListingCrawl,calendar.getTime());
						}
						ticketListingCrawl.setExpirationTime(System
								.currentTimeMillis()
								- startExpirationTime);
						threadPool.stopTicketListingCrawl(
								ticketListingCrawl, CrawlState.EXCEPTION, e);						
					} catch (Exception e2) {
						e2.printStackTrace();
						logger.error(ticketListingCrawl.getId() + " - Error while crawl is processed: E2", e2.fillInStackTrace());
//						System.out.println(ticketListingCrawl.getName() + " Error while crawl is processed" + e);
					}
				} finally {
					processingCrawl = false;
					processedCrawl = null;
					// note: crawl.getTourId can now be null for the special crawl seatwavefeed (feed containing all the ticket
					// listing of seatwave on multiple tours/events
					
					threadPool.getTicketListingCrawlerTimeOutManager().unmonitorTicketListingCrawlerTask(this);
				}
				long finaltime =new Date().getTime()-startTime.getTime(); 
				logger.info(" TLC Completed : "+ticketListingCrawl.getId()+" : "+finaltime);
			} catch (Exception e) {
				e.printStackTrace();
				try {
					// try to notify the manager of the error
					if (ticketListingCrawl != null) {
						threadPool.stopTicketListingCrawl(
								ticketListingCrawl, CrawlState.EXCEPTION, e);						
					} 
				} catch(Exception ex) {
					ex.printStackTrace();
				}
			}
		}
		running = false;		
		logger.info("*** ONE THREAD WAS STOPPED: running=" + running
				+ ", processing=" + processingCrawl);
		logger.info("was processing " + processedCrawl);
		processingCrawl = false;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherBySiteId) {
		this.ticketListingFetcherBySiteId = ticketListingFetcherBySiteId;
	}

	public int getJobCounter() {
		return jobCounter;
	}

	public boolean isRunning() {
		return running;
	}

	public boolean isProcessingCrawl() {
		return processingCrawl;
	}

	public void setTicketHitIndexer(TicketHitIndexer ticketHitIndexer) {
		this.ticketHitIndexer = ticketHitIndexer;
	}
	
	public void interrupt() {
		Thread thread = getCrawlerThread();
		thread.interrupt();
	}

	public TicketListingCrawl getProcessedCrawl() {
		return processedCrawl;
	}

	public Thread getCrawlerThread() {
		return crawlerThread;
	}
	
	public TicketUpdater getTicketUpdater() {
		return ticketUpdater;
	}

	public void setTicketUpdater(TicketUpdater ticketUpdater) {
		this.ticketUpdater = ticketUpdater;
	}
	
	public Integer getId() {
		return id;
	}	
}