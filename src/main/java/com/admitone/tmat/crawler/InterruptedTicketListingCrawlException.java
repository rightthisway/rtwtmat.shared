package com.admitone.tmat.crawler;

/**
 * Exception used when the crawl was interrupted.
 * For now, it is raised by the PreviewTicketHitIndexer when the number of previewed results is reached. 
 *
 */
public class InterruptedTicketListingCrawlException extends RuntimeException {
	private static final long serialVersionUID = -3805684140339661109L;

	public InterruptedTicketListingCrawlException() {
		super();
	}
	
	public InterruptedTicketListingCrawlException(String message) {
		super(message);
	}
}
