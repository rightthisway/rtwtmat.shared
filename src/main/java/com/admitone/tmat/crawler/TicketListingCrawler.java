package com.admitone.tmat.crawler;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.com.ClusterListener;
import com.admitone.tmat.crawler.com.TicketListingCrawlDispatcher;
import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.CrawlerSchedulerMode;
import com.admitone.tmat.web.Constants;
import com.tangosol.net.CacheFactory;

/*
 * Simple Ticket Listing Crawler.
 */
public class TicketListingCrawler implements InitializingBean, ClusterListener {
	private final Logger logger = LoggerFactory.getLogger(TicketListingCrawler.class);

	private Map<Integer, Boolean> crawlMap = new HashMap<Integer, Boolean>();
	private Boolean idlePeriodEnabled;
	private Integer idlePeriodStartHour;
	private Integer idlePeriodStopHour;
	private Integer crawlDurationThreshold = 600;
	private Integer ebayDurationThreshold = 900;

	private Double proActiveFactor;
	private Integer sortingTimeSlice; // in seconds

	private CrawlerSchedulerMode schedulerMode;
	private CrawlerSchedulerManager crawlerSchedulerManager;
	private TicketListingCrawlHealerThread ticketListingCrawlHealerThread;
	private int crawlerThreadCountPerNode;
	private static List<String> regularSiteIds = Arrays.asList(new String[] {Site.TICKET_EVOLUTION,Site.TICKET_NETWORK_DIRECT,Site.STUBHUB_FEED, "default" });

	public int getCrawlerThreadCountPerNode() {
		return crawlerThreadCountPerNode;
	}

	public void setCrawlerThreadCountPerNode(int crawlerThreadCountPerNode) {
		this.crawlerThreadCountPerNode = crawlerThreadCountPerNode;
	}

	private Boolean running = false;
	private String status = "NONE";
	private Date lastUpdate = new Date();

	/**
	 * Ticket Listing Crawls.
	 */

	private TicketListingCrawlQueue ticketListingCrawlQueue;
	private Integer crawlerArtistId;
	private Map<String, Integer> activeTicketListingCrawlCountBySiteId = new ConcurrentHashMap<String, Integer>();
	private Collection<TicketListingCrawlerTask> ticketListingCrawlerTasks = Collections.synchronizedCollection(new ArrayList<TicketListingCrawlerTask>());

	private TicketListingCrawlDispatcher ticketListingCrawlDispatcher;

	// when doing a forced recrawl, we check that the last crawled wasn't run
	// before minCrawlPeriod
	private Integer minRecrawlPeriod;

	public TicketListingCrawler() {
		for (String siteId : Constants.getInstance().getExtendedSiteIds()) {
			activeTicketListingCrawlCountBySiteId.put(siteId, 0);
		}
	}

	public Map<String, Integer> getActiveTicketListingCrawlCountBySiteId() {
		return activeTicketListingCrawlCountBySiteId;
	}

	public void afterPropertiesSet() throws Exception {
		// ticketListingCrawlerThreadPool.setTicketListingCrawler(this);

		if (crawlerSchedulerManager == null) {
			throw new Exception("Property crawlerSchedulerManager must be set");
		}

		// crawlerSchedulerManager.setTicketListingCrawler(this);

		if (ticketListingCrawlDispatcher == null) {
			throw new Exception(
					"Property ticketListingCrawlDispatcher must be set");
		}

		ticketListingCrawlDispatcher.addClusterListener(this);

		Collection<TicketListingCrawl> ticketListingCrawls = Collections.synchronizedList(new ArrayList<TicketListingCrawl>(DAORegistry.getTicketListingCrawlDAO().getAll()));

		ticketListingCrawlQueue = new TicketListingCrawlQueue(ticketListingCrawls);
		ticketListingCrawlQueue.setCrawlerSchedulerManager(crawlerSchedulerManager);

		reinitProperties();
		resetStats();

		// initialize all the end_crawl of all crawlers to null
		// DAORegistry.getTicketListingCrawlDAO()
		// .resetAllTicketListingCrawlEndCrawl(); // Commented because when ever
		// we restart app. it hits exchanges ...(EIMP Issue)

		crawlerSchedulerManager.reinit();
		// ticketListingCrawlerThreadPool.start();

		// Property crawlStartupEnabledProperty = DAORegistry.getPropertyDAO()
		// .get("crawler.startup.enabled");
		// if (crawlStartupEnabledProperty != null
		// && crawlStartupEnabledProperty.getValue().equals("1")) {
		// start();
		// }
		start();

		ticketListingCrawlHealerThread = new TicketListingCrawlHealerThread(this);
		ticketListingCrawlHealerThread.start();
	}

	public void resetStats() {
		ticketListingCrawlDispatcher.dispatch(new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_RESET_STAT));
	}

	public synchronized void start() {
		if (running) {
			return;
		}

		running = true;
		TicketListingCrawlFeeder ticketListingCrawlFeeder = new TicketListingCrawlFeeder(this, ticketListingCrawlDispatcher);
		Thread feeder = new Thread(ticketListingCrawlFeeder);
		feeder.start();

		TicketListingCrawlConsumer ticketListingCrawlConsumer = new TicketListingCrawlConsumer(this, ticketListingCrawlDispatcher);
		Thread consumer = new Thread(ticketListingCrawlConsumer);
		consumer.start();

		ticketListingCrawlDispatcher.dispatch(new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_START));
	}

	public int getActiveThreadCount() {
		return getTicketListingCrawlDispatcher().getTotalActiveThreadCount();
	}

	public int getProcessingCrawlThreadCount() {
		int processingCrawlCount = 0;
		for (TicketListingCrawlerTask ticketListingCrawlerTask : ticketListingCrawlerTasks) {
			if (ticketListingCrawlerTask.isProcessingCrawl()) {
				processingCrawlCount++;
			}
		}
		return processingCrawlCount;
	}

	public synchronized void stop() {
		if (!running) {
			return;
		}

		running = false;
		ticketListingCrawlDispatcher.dispatch(new TicketListingCrawlerMessage(TicketListingCrawlerMessage.MSG_STOP));

		for (TicketListingCrawlerTask ticketListingCrawlerTask : ticketListingCrawlerTasks) {
			ticketListingCrawlerTask.cancel();
		}

		for (TicketListingCrawl ticketListingCrawl : ticketListingCrawlQueue.getTicketListingCrawls()) {
			if (ticketListingCrawl.getCrawlState().equals(CrawlState.RUNNING)) {
				stopTicketListingCrawl(ticketListingCrawl);
			}
		}

		for (String siteId : Constants.getInstance().getSiteIds()) {
			activeTicketListingCrawlCountBySiteId.put(siteId, 0);
		}

		ticketListingCrawlerTasks.clear();
	}

	public synchronized void forceRecrawl(Collection<TicketListingCrawl> crawls) {
		for (TicketListingCrawl ticketListingCrawl : crawls) {
			_forceRecrawl(ticketListingCrawl, null);
		}
		ticketListingCrawlQueue.resort();
	}

	public synchronized void forceRecrawl(TicketListingCrawl ticketListingCrawl) {
		_forceRecrawl(ticketListingCrawl, null);
		ticketListingCrawlQueue.resort();
	}

	public synchronized void forceRecrawlWithPriority(Collection<TicketListingCrawl> crawls, Integer priority) {
		for (TicketListingCrawl ticketListingCrawl : crawls) {
			_forceRecrawl(ticketListingCrawl, priority);
		}
		ticketListingCrawlQueue.resort();
	}

	public synchronized void forceRecrawlWithPriority(TicketListingCrawl ticketListingCrawl, Integer priority) {
		_forceRecrawl(ticketListingCrawl, priority);
		ticketListingCrawlQueue.resort();
	}

	private synchronized void _forceRecrawl(TicketListingCrawl ticketListingCrawl, Integer priority) {
		if (ticketListingCrawl.getCrawlState().equals(CrawlState.RUNNING)) {
			return;
		}
		// Called..
		if (!ticketListingCrawl.getCrawlPhase().equals(CrawlPhase.STOPPED)) {
			if(!(ticketListingCrawl.getCrawlPhase().equals(CrawlPhase.DISPATCHED) && priority != null && ticketListingCrawl.getPriority()<=priority)){
				return;
			}
		}
		if (priority == null || priority == TicketListingCrawl.PRIORITY_NORMAL) {
			ticketListingCrawl.setPriority(TicketListingCrawl.PRIORITY_NORMAL);
		} else if (priority == TicketListingCrawl.PRIORITY_HIGH) {
			ticketListingCrawl.setPriority(TicketListingCrawl.PRIORITY_HIGH);
		} else {
			ticketListingCrawl.setPriority(TicketListingCrawl.PRIORITY_LOW);
		}
		ticketListingCrawl.setCrawlState(CrawlState.STOPPED);
		ticketListingCrawl.setCrawlPhase(CrawlPhase.STOPPED);
		ticketListingCrawl.setStartCrawl(null);
		ticketListingCrawl.setEndCrawl(null);
		
		//Tamil : for stubhub site if crawl is force recraled then if ticket fetching fails then execute api
		ticketListingCrawl.setForceReCrawlFlag(true);
		
		// ticketListingCrawlQueue.resort();
	}

	public boolean ticketListingCrawlExists(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawlQueue.exists(ticketListingCrawl);
	}

	public boolean existTicketListingCrawl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawlQueue.exists(ticketListingCrawl);
	}

	public boolean addTicketListingCrawl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawlQueue.add(ticketListingCrawl);
	}

	public void removeTicketListingCrawl(TicketListingCrawl ticketListingCrawl) {
		ticketListingCrawlQueue.remove(ticketListingCrawl);
	}

	public void removeTicketListingCrawlByArtist(Integer artistId) {
		ticketListingCrawlQueue.removeTicketListingCrawlByArtistId(artistId);
	}

	public Collection<TicketListingCrawl> getFailedTicketListingCrawlsByEventId() {
		return ticketListingCrawlQueue.getFailedTicketListingCrawlsByEventId();
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlsByEventId(int eventId) {
		return ticketListingCrawlQueue.getTicketListingCrawlsByEventId(eventId);
	}

	public Collection<TicketListingCrawl> getActiveTicketListingCrawlsByEventId(int eventId) {
		return ticketListingCrawlQueue
				.getActiveTicketListingCrawlsByEventId(eventId);
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlsByArtistId(int artistId) {
		return ticketListingCrawlQueue.getTicketListingCrawlsByArtistId(artistId);
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlByCreator(String username) {
		return ticketListingCrawlQueue.getTicketListingCrawlByCreator(username);
	}

	public void updateTicketListingCrawl(TicketListingCrawl crawl) {
		ticketListingCrawlQueue.update(crawl);
		ticketListingCrawlQueue.resort();
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlByLastUpdater(String username) {
		return ticketListingCrawlQueue.getTicketListingCrawlByLastUpdater(username);
	}

	public boolean isRunning() {
		return running;
	}

	public Integer getCrawlerArtistId() {
		return crawlerArtistId;
	}

	public Collection<TicketListingCrawl> getTicketListingCrawls() {
		return ticketListingCrawlQueue.getTicketListingCrawls();
	}

	public TicketListingCrawl getTicketListingCrawlById(int id) {
		return ticketListingCrawlQueue.getTicketListingCrawlById(id);
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlByArtistId(int artistId) {
		return ticketListingCrawlQueue.getTicketListingCrawlByArtistId(artistId);
	}

	public Collection<TicketListingCrawl> getTicketListingCrawlByEvent(int eventId) {
		return ticketListingCrawlQueue.getTicketListingCrawlsByEventId(eventId);
	}

	public TicketListingCrawl getTicketListingCrawlByName(String name) {
		return ticketListingCrawlQueue.getTicketListingCrawlByName(name);
	}

	public Integer getCrawlsExecutedForEventCount(Integer eventId) {
		Integer count = 0;
		for (TicketListingCrawl crawl : getActiveTicketListingCrawlsByEventId(eventId)) {
			if (crawl.getEndCrawl() != null
					&& !crawl.getCrawlState().equals(CrawlState.RUNNING)) {
				count++;
			}
		}

		return count;
	}

	public Date getLeastCrawledDateForEvent(Integer eventId) {
		Date min = null;
		Collection<TicketListingCrawl> crawls = getActiveTicketListingCrawlsByEventId(eventId);
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEndCrawl() == null) {
				continue;
			}

			if (min == null) {
				min = crawl.getEndCrawl();
			} else if (crawl.getEndCrawl().before(min)) {
				min = crawl.getEndCrawl();
			}
		}
		return min;
	}

	public Collection<TicketListingCrawl> getRecentlyCrawledCrawlerForEvent(Integer eventId) {
		Date min = null;
		Date now = new Date();
		Collection<TicketListingCrawl> crawls = getActiveTicketListingCrawlsByEventId(eventId);
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEndCrawl() == null) {
				continue;
			}

			if (min == null) {
				min = crawl.getEndCrawl();
			} else if (crawl.getEndCrawl().before(min)) {
				min = crawl.getEndCrawl();
			}
		}
		if (min != null
				&& now.getTime() - min.getTime() < minRecrawlPeriod * 1000) {
			return null;
		}
		return crawls;
	}

	public Integer forceRecrawlForEvent(Integer eventId,Integer priority) {
		/*
		 * Date now = new Date(); Date min =
		 * getLeastCrawledDateForEvent(eventId); if (min != null &&
		 * now.getTime() - min.getTime() < minRecrawlPeriod * 1000) { return -1;
		 * } Collection<TicketListingCrawl> crawls =
		 * getActiveTicketListingCrawlsByEventId(eventId);
		 */
		Collection<TicketListingCrawl> crawls = getRecentlyCrawledCrawlerForEvent(eventId);
		if (crawls == null || crawls.isEmpty()) {
			return 0;
		}
		forceRecrawlWithPriority(crawls, priority);
		return crawls.size();
	}

	public Integer forceRecrawlByEventId(Integer eventId) {
		Collection<TicketListingCrawl> crawls = getActiveTicketListingCrawlsByEventId(eventId);
		Date now = new Date();
		int count = 0;
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getEndCrawl() != null
					&& now.getTime() - crawl.getEndCrawl().getTime() >= minRecrawlPeriod * 1000) {
				forceRecrawl(crawl);
				count++;
			}
		}
		return count;
	}

	public Integer getIdlePeriodStartHour() {
		return idlePeriodStartHour;
	}

	public Integer getIdlePeriodStopHour() {
		return idlePeriodStopHour;
	}

	public TicketListingCrawlDispatcher getTicketListingCrawlDispatcher() {
		return ticketListingCrawlDispatcher;
	}

	public void setTicketListingCrawlDispatcher(
			TicketListingCrawlDispatcher ticketListingCrawlDispatcher) {
		this.ticketListingCrawlDispatcher = ticketListingCrawlDispatcher;
	}

	public void stopTicketListingCrawl(TicketListingCrawl crawl) {
		crawlMap.remove(crawl.getId());
		if (crawl.getCrawlPhase().equals(CrawlPhase.STOPPED)) {
			logger.info("** " + crawl + " already stopped");
			return;
		}

		synchronized (activeTicketListingCrawlCountBySiteId) {
			activeTicketListingCrawlCountBySiteId.put(crawl.getSiteId(),
							activeTicketListingCrawlCountBySiteId.get(crawl
									.getSiteId()) - 1);
		}

		TicketListingCrawl localCrawl = getTicketListingCrawlById(crawl.getId());
		if (localCrawl == null) {
			logger.error("No crawl found with id=" + crawl + " [" + crawl + "]");
			return;
		}

		localCrawl.setStartCrawl(crawl.getStartCrawl());

		// System.out.println("STOP " + crawl.getId() + " CRAWLSTATE=" +
		// crawl.getCrawlState());

		if (crawl.getCrawlState().equals(CrawlState.STOPPED)) {
			if (crawl.getItemIndexed() > 0) {
				localCrawl.setLastSuccessfulRun(new Date());
			}
			localCrawl.setCrawlState(CrawlState.STOPPED);
			// System.out.println("CHANGE SUCCESSFULL TIME TO " +
			// localCrawl.getLastSuccessfulRun());
		} else if (crawl.getCrawlState().isError()) {
			localCrawl.setCrawlState(crawl.getCrawlState());
			if (crawl.getCrawlState().equals(CrawlState.EXCEPTION)) {
				// System.out.println("++++++++++++++++++++++++++++");
				// System.out.println("EXCEPTION = " + crawl.getException());
				if (crawl.getException() != null) {
					if (crawl.getException() instanceof TimedOutTicketListingCrawlException
						|| (crawl.getException().getMessage() != null && crawl.getException().getMessage().equals("sleep interrupted"))) {
							localCrawl.setCrawlState(CrawlState.TIMED_OUT);
					}
					localCrawl.setException(crawl.getException());
				}
			}
			localCrawl.setErrorMessage(crawl.getErrorMessage());
		} else {
			localCrawl.setCrawlState(CrawlState.STOPPED);
		}

		localCrawl.setEndCrawl(new Date());
		localCrawl.setCrawlPhase(CrawlPhase.STOPPED);
		localCrawl.setNodeId(null);
		localCrawl.setHostname(null);
		localCrawl.setPriority(TicketListingCrawl.PRIORITY_NORMAL);

		localCrawl.setFetchedByteCount(crawl.getFetchedByteCount());
		localCrawl.setFetchingTime(crawl.getFetchingTime());
		localCrawl.setIndexationTime(crawl.getIndexationTime());
		localCrawl.setExtractionTime(crawl.getExtractionTime());
		localCrawl.setExpirationTime(crawl.getExpirationTime());
		localCrawl.setItemIndexed(crawl.getItemIndexed());
		localCrawl.setItemErrorIndexed(crawl.getItemErrorIndexed());
		localCrawl.setNewItemIndexed(crawl.getNewItemIndexed());

		if (localCrawl.getAutomaticCrawlFrequency()) {
			if (localCrawl.getEvent() != null) {
				// System.out.println("The event name is: " +
				// localCrawl.getEvent().getId() + " the event date is: " +
				// localCrawl.getEvent().getLocalDateTime());
				localCrawl.setCrawlFrequency(crawlerSchedulerManager.getFrequency(localCrawl.getEvent().getLocalDateTime(),
						localCrawl.getEvent().getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName()));
			}
		}

		DAORegistry.getTicketListingCrawlDAO().update(localCrawl);

		logger.info("Stopping " + crawl);
	}

	/**
	 * Pick one crawl to process and return it. It chooses one crawl: - which is
	 * not currently running - with a preference to a crawl which was processed
	 * a while ago - for which there is no other crawl running for the same site
	 * 
	 * @return
	 */
	public TicketListingCrawl startTicketListingCrawl() {
		Calendar now = new GregorianCalendar();
		int hour = now.get(Calendar.HOUR_OF_DAY);
		try{
			
		if (idlePeriodEnabled
				&& ((idlePeriodStartHour <= hour && hour < idlePeriodStopHour) || (idlePeriodStopHour < idlePeriodStartHour && (idlePeriodStartHour <= hour || hour < idlePeriodStopHour)))) {
			return null;
		}
		// ProActive logic before eimp issues
		// removed for loop :: reason :: does not fit with requirement..
		// for (Double p = 1D; p >= 1D - proActiveFactor; p -= 0.10D) {
		for (TicketListingCrawl crawl : ticketListingCrawlQueue.getTicketListingCrawls()) {
			if (crawl.isSuspended()) {
				continue;
			}
			Boolean flag = crawlMap.get(crawl.getId());
			if(flag!=null && flag){
				continue;
			}
			// logger.info("ACQUIRING TicketListingCrawler: "
			// + crawl.getArtistId());

			// skip crawls which are being created (not assigned an id yet by
			// the DB)
			if (crawl.getId() == null) {
				continue;
			}
			CrawlPhase crawlPhase = crawl.getCrawlPhase();
			// note: crawl.getArtistId can now be null for the special crawl
			// seatwavefeed (feed containing all the ticket
			// listing of seatwave on multiple tours/events
			if (!crawl.isEnabled()
					|| crawl.getCrawlState().equals(CrawlState.RUNNING)
					|| crawl.getCrawlPhase().equals(CrawlPhase.DISPATCHED)) {
				continue;
			}
			crawl.setCrawlPhase(CrawlPhase.DISPATCHED);
			// if forced crawl, run it now
			if (crawl.getStartCrawl() != null) {
				// if crawl does not need to be reprocessed, skip it

				// if crawl had an error before, pick it up if it has been
				// processed more than 30mn before

				// ProActive logic before eimp issues
				int crawlFrequency;
				// if (crawl.getCrawlState().isError()) {
				// crawlFrequency = (int)(crawl.getCrawlFrequency());
				// } else {
				// crawlFrequency = (int)(crawl.getCrawlFrequency() * p);
				// }

				crawlFrequency = (int) (crawl.getCrawlFrequency()) - (int) (crawl.getCrawlFrequency() * (proActiveFactor));
				// crawlFrequency = (int)(crawl.getCrawlFrequency())-
				// (int)(crawl.getCrawlFrequency());

				if (crawl.getEndCrawl() != null	&& (new Date().getTime() - crawl.getEndCrawl().getTime()) / 1000 < crawlFrequency) {
					crawl.setCrawlPhase(crawlPhase);
					continue;
				}
			}

			Integer count = activeTicketListingCrawlCountBySiteId.get(crawl.getSiteId());

			Site site = DAORegistry.getSiteDAO().get(crawl.getSiteId());

			synchronized (activeTicketListingCrawlCountBySiteId) {
				int maxConcurrentConnection = ticketListingCrawlDispatcher.getNumNodes();
				if (site != null && site.getMaxConcurrentConnections() != null) {
					maxConcurrentConnection = (int) Math.ceil((double) site.getMaxConcurrentConnections() * ticketListingCrawlDispatcher.getNumNodes() * 1.5);
					// we give pro-activeness of 50% (because we want to feed
					// the dispatch queue without having to wait for the crawl
					// to come back))
				}

				// only allow max crawler active by siteId
				if (count >= maxConcurrentConnection) {
					crawl.setCrawlPhase(crawlPhase);
					continue;
				}

				activeTicketListingCrawlCountBySiteId.put(crawl.getSiteId(),activeTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
			}

			// ticketListingCrawlStat.computeCrawlerDelayStat(crawl);

			// crawl.setCrawlState(CrawlState.RUNNING);
			crawl.setStartCrawl(new Date());
			crawl.resetStats();

			try {
				DAORegistry.getTicketListingCrawlDAO().update(crawl);
			} catch (Exception e) {
				crawl.setEndCrawl(new Date());
				crawl.setCrawlState(CrawlState.EXCEPTION);
				crawl.setCrawlPhase(CrawlPhase.STOPPED);
				crawl.setException(e);
				stopTicketListingCrawl(crawl);
				if(crawl != null && crawl.getId() != null) {
					logger.error("Error while taking a crawl from queue crawlID: "+crawl.getId()+" : ", e);
				} else {
					logger.error("Error while taking a crawl from queue", e);
				}
				
				return null;
			}
			
			//ticketListingCrawlQueue.resort();

			// crawlerArtistId = crawl.getArtistId();
			crawlMap.put(crawl.getId(),true);
			//logger.info("Crawl:" + crawlMap.keySet());
			return crawl;
		}
		}catch (Exception e) {
			logger.info("activeTicketListingCrawlCountBySiteId:" + activeTicketListingCrawlCountBySiteId);
			logger.info("ticketListingCrawlDispatcher :" + ticketListingCrawlDispatcher);
			logger.info("maxConcurrentConnection :" + ticketListingCrawlDispatcher.getNumNodes());
		}
		return null;
	}

	public void recomputeCrawlFrequency() {
		ticketListingCrawlQueue.recomputeCrawlFrequency();
	}

	public CrawlerSchedulerManager getCrawlerSchedulerManager() {
		return crawlerSchedulerManager;
	}

	public void setCrawlerSchedulerManager(CrawlerSchedulerManager crawlerSchedulerManager) {
		this.crawlerSchedulerManager = crawlerSchedulerManager;
	}

	public void reinitProperties() {
		try {
			idlePeriodStartHour = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.idle.startHour").getValue());
			idlePeriodStopHour = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.idle.stopHour").getValue());
			sortingTimeSlice = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.scheduler.sortingTimeSlice").getValue());
			proActiveFactor = Double.parseDouble(DAORegistry.getPropertyDAO().get("crawler.scheduler.proActiveFactor").getValue()) / 100D;
			minRecrawlPeriod = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.scheduler.minPeriod").getValue());
			schedulerMode = CrawlerSchedulerMode.valueOf(DAORegistry.getPropertyDAO().get("crawler.scheduler.mode").getValue());
			idlePeriodEnabled = Boolean.valueOf(DAORegistry.getPropertyDAO().get("crawler.idle.enabled").getValue());
			crawlDurationThreshold = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.error.crawlDurationThreshold").getValue());
			crawlerThreadCountPerNode = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.thread.count").getValue());
		} catch (Exception e) {
			logger.info("Property not defined", e);
			idlePeriodStartHour = 0;
			idlePeriodStopHour = 0;
			sortingTimeSlice = 300;
			proActiveFactor = 0.10D;
			crawlDurationThreshold = 600;
			crawlerThreadCountPerNode = 8;
		}
		ticketListingCrawlQueue.setSortingTimeSlice(sortingTimeSlice);
		ticketListingCrawlQueue.setCrawlDurationThreshold(crawlDurationThreshold);
	}

	/*
	 * Return the phase the crawler is in: - SCHEDULED - UNSCHEDULED - IDLE
	 */
	public String getPhase() {
		Calendar cal = new GregorianCalendar();
		int hour = cal.get(Calendar.HOUR_OF_DAY);

		if (idlePeriodEnabled
				&& ((idlePeriodStartHour <= hour && hour < idlePeriodStopHour) || (idlePeriodStopHour < idlePeriodStartHour && (idlePeriodStartHour <= hour || hour < idlePeriodStopHour)))) {
			return "IDLE";
		}

		if (schedulerMode.equals(CrawlerSchedulerMode.ALWAYS_SCHEDULED)) {
			return "SCHEDULED";
		}

		if (schedulerMode.equals(CrawlerSchedulerMode.ALWAYS_UNSCHEDULED)) {
			return "UNSCHEDULED";
		}

		if (crawlerSchedulerManager.isAutomaticScheduledTime()) {
			return "SCHEDULED";
		}

		return "UNSCHEDULED";
	}

	public Integer getMinCrawlPeriod() {
		return minRecrawlPeriod;
	}

	public void pauseCrawlsForArtist(Integer artistId) {
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_PAUSE_CRAWLS_FOR_TOUR);
		msg.setObject(artistId);
		ticketListingCrawlDispatcher.dispatch(msg);
	}

	public void resumeCrawlsForArtist(Integer artistId) {
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_RESUME_CRAWLS_FOR_TOUR);
		msg.setObject(artistId);
		ticketListingCrawlDispatcher.dispatch(msg);
	}

	public void pauseCrawlsForEvent(Integer eventId) {
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_PAUSE_CRAWLS_FOR_EVENT);
		msg.setObject(eventId);
		ticketListingCrawlDispatcher.dispatch(msg);
	}

	public void resumeCrawlsForEvent(Integer eventId) {
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_RESUME_CRAWLS_FOR_EVENT);
		msg.setObject(eventId);
		ticketListingCrawlDispatcher.dispatch(msg);
	}

	public void disableCrawlsForArtist(Integer artistId) {
		for (TicketListingCrawl crawl : getTicketListingCrawlByArtistId(artistId)) {
			crawl.setSuspended(true);
		}
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_DISABLE_CRAWLS_FOR_TOUR);
		msg.setObject(artistId);
		ticketListingCrawlDispatcher.dispatch(msg);
	}

	public void disableCrawl(Integer crawlId) {
		TicketListingCrawl crawl = getTicketListingCrawlById(crawlId);
		crawl.setSuspended(true);
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_DISABLE_CRAWL_ID);
		msg.setObject(crawlId);
		ticketListingCrawlDispatcher.dispatch(msg);
	}

	public void enableCrawlsForArtist(Integer artistId) {
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_ENABLE_CRAWLS_FOR_TOUR);
		msg.setObject(artistId);
		ticketListingCrawlDispatcher.dispatch(msg);
		for (TicketListingCrawl crawl : getTicketListingCrawlByArtistId(artistId)) {
			crawl.setSuspended(false);
		}
	}

	public void enableCrawl(Integer crawlId) {
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_ENABLE_CRAWL_ID);
		msg.setObject(crawlId);
		ticketListingCrawlDispatcher.dispatch(msg);
		TicketListingCrawl crawl = getTicketListingCrawlById(crawlId);
		crawl.setSuspended(false);
	}

	public void disableCrawlsForEvent(Integer eventId) {
		for (TicketListingCrawl crawl : getTicketListingCrawlByEvent(eventId)) {
			crawl.setSuspended(true);
		}
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_DISABLE_CRAWLS_FOR_EVENT);
		msg.setObject(eventId);
		ticketListingCrawlDispatcher.dispatch(msg);
	}

	public void enableCrawlsForEvent(Integer eventId) {
		TicketListingCrawlerMessage msg = new TicketListingCrawlerMessage();
		msg.setMsgCode(TicketListingCrawlerMessage.MSG_ENABLE_CRAWLS_FOR_EVENT);
		msg.setObject(eventId);
		ticketListingCrawlDispatcher.dispatch(msg);
		for (TicketListingCrawl crawl : getTicketListingCrawlByEvent(eventId)) {
			crawl.setSuspended(false);
		}
	}

	public Map<Integer, TicketListingCrawlStat> getStatMap() {
		return ticketListingCrawlDispatcher.getStatMap();
	}

	public void setStat(TicketListingCrawlStat ticketListingCrawlStat) {
	}

	public void memberJoined(Integer nodeId) {
		System.out.println("Member " + nodeId + " has joined the party");
	}

	public void memberLeft(Integer nodeId) {
		System.out.println("Member " + nodeId + " has left the party");
		// stop all its crawls
		stopCrawlsFromNode(nodeId);
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.lastUpdate = new Date();
		this.status = status;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public static class TicketListingCrawlFeeder implements Runnable {
		private TicketListingCrawlDispatcher dispatcher;
		private TicketListingCrawler crawler;
		private Boolean running = false;
		private Logger logger = LoggerFactory
				.getLogger(TicketListingCrawlFeeder.class);
		// private String
		// masterNode=DAORegistry.getPropertyDAO().get("tmat.master.node").value;
		private String masterNode = CacheFactory.ensureCluster().getOldestMember().getAddress().getHostAddress();

		public TicketListingCrawlFeeder(TicketListingCrawler crawler,TicketListingCrawlDispatcher dispatcher) {
			this.dispatcher = dispatcher;
			this.crawler = crawler;
			this.running = true;
		}

		public void run() {
			// FIXME: dirty hack to wait that dao registry is initialized (even
			// though we used spring depends-on)
			while (DAORegistry.getSiteDAO() == null) {
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					logger.warn("waiting for site dao ready interrupted", e);
				}
			}

			while (running) {
				try {
					// FIXME: we can use node id instaed of ips
					String ip = InetAddress.getLocalHost().getHostAddress();
					if (!ip.equals(masterNode)) {
						running = false;
						continue;
					}
				} catch (UnknownHostException e1) {
					e1.printStackTrace();
				}
				try {
					this.crawler.setStatus("BEFORE START");
					int numInQueue = (dispatcher.getDispatchQueueSize()<dispatcher.getProxyDispatchQueueSize())?dispatcher.getDispatchQueueSize():dispatcher.getProxyDispatchQueueSize();
					int maxAvailable = dispatcher.getNumNodes()	* crawler.getCrawlerThreadCountPerNode();
					int toRun = (maxAvailable - numInQueue) * 2;
					this.crawler.setStatus("START (" + toRun + ")");
					Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>();
					Collection<TicketListingCrawl> eiCrawls = new ArrayList<TicketListingCrawl>();
					
					int crawlsCount=0;
					for (int i = 0; i < toRun; i++) {
						TicketListingCrawl ticketListingCrawl = crawler.startTicketListingCrawl();

						if (ticketListingCrawl == null) {
							break;
						}
						ticketListingCrawl.setCrawlPhase(CrawlPhase.DISPATCHED);
						/* EI Test */
						if (regularSiteIds.contains(ticketListingCrawl.getSiteId())) {
							crawls.add(ticketListingCrawl);
						} else {
							eiCrawls.add(ticketListingCrawl);
						}
						crawlsCount++;
					}
					
					if(crawlsCount > 0) {
						crawler.ticketListingCrawlQueue.resort();
					}
					logger.info("Crawl:" + crawler.crawlMap.keySet());
					this.crawler.setStatus("DISPATCH (" + toRun + ")");
					if(!crawls.isEmpty()){
						dispatcher.dispatchAll(crawls);
					}
					if(!eiCrawls.isEmpty()){
						dispatcher.dispatchAllEI(eiCrawls);
					}
					this.crawler.setStatus("SLEEP");
					if (toRun <= 0) {
						Thread.sleep(1000);
					}

				} catch (Exception e) {
					logger.warn("Problem while dispatching a crawl", e);
					this.crawler.setStatus("ERROR");
				}
			}
		}

		public void stop() {
			running = false;
		}
	}

	public static class TicketListingCrawlConsumer implements Runnable {
		private Logger logger = LoggerFactory.getLogger(TicketListingCrawlConsumer.class);
		private TicketListingCrawlDispatcher dispatcher;
		private TicketListingCrawler crawler;
		private Boolean running = false;
		// private String
		// masterNode=DAORegistry.getPropertyDAO().get("tmat.master.node").value;
		private String masterNode = CacheFactory.ensureCluster().getOldestMember().getAddress().getHostAddress();

		public TicketListingCrawlConsumer(TicketListingCrawler crawler,	TicketListingCrawlDispatcher dispatcher) {
			this.dispatcher = dispatcher;
			this.crawler = crawler;
			this.running = true;
		}

		public void run() {
			while (running) {
				// FIXME: we can use node id instaed of ips
				String ip;
				try {
					ip = InetAddress.getLocalHost().getHostAddress();
					if (!ip.equals(masterNode)) {
						running = false;
						continue;
					}
				} catch (UnknownHostException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				Date start = new Date();
				long finltime=0;
				try {
					Collection<TicketListingCrawl> crawls = dispatcher.takeAllFinishedCrawls();
					for (TicketListingCrawl ticketListingCrawl : crawls) {
						if (ticketListingCrawl == null) {
							continue;
						}

						crawler.stopTicketListingCrawl(ticketListingCrawl);
					}

					// take care of the stats
					Collection<TicketListingCrawlStat> stats = new ArrayList<TicketListingCrawlStat>(dispatcher.getStatMap().values());
					for (TicketListingCrawlStat stat : stats) {
						for (Entry<Integer, CrawlPhase> entry : stat.getCrawlPhaseMap().entrySet()) {
							Integer crawlId = entry.getKey();
							CrawlPhase phase = entry.getValue();
							TicketListingCrawl crawl = crawler.getTicketListingCrawlById(crawlId);

							if (crawl == null) {
								// probably has been removed by user
								continue;
							}
							if (crawl.getCrawlPhase().equals(CrawlPhase.STOPPED)) {
								continue;
							}
							crawl.setCrawlPhase(phase);
							crawl.setNodeId(stat.getNodeId());
							crawl.setHostname(stat.getHostname());
						}
					}
					finltime = new Date().getTime()-start.getTime();
					logger.info("Inside CRAWL Consume List Competed : "+finltime);
					if(finltime <= 1000) {
						Thread.sleep(1000);
					}
				} catch (Exception e) {
					logger.warn("Problem in consumer", e);
				}
			}
		}

		public void stop() {
			running = false;
		}
	}

	public void stopCrawlsFromNode(Integer nodeId) {
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawlQueue.getAll());
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getNodeId() != null && crawl.getNodeId().equals(nodeId)) {
				stopTicketListingCrawl(crawl);
			}
		}
	}

	// stop all crawls which have been dispatched/running for more than the
	// timeout
	public void stopOutdatedCrawls() {
		long now = System.currentTimeMillis();
		try {
			Map<Integer,List<TicketListingCrawl>> dupCrawlCountMap = new HashMap<Integer, List<TicketListingCrawl>>();
			int count =0,removeCount=0;
			Map<Integer, Boolean> tempCrawlMap = new HashMap<Integer, Boolean>(crawlMap);
			String missedCrawls="";
			int missedCrlCount=0;
			
			Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawlQueue.getAll());
			for (TicketListingCrawl crawl : crawls) {
				List<TicketListingCrawl> tempCrawls = dupCrawlCountMap.get(crawl.getId());
				if(tempCrawls == null) {
					tempCrawls = new ArrayList<TicketListingCrawl>();
				}
				tempCrawls.add(crawl);
				dupCrawlCountMap.put(crawl.getId(),tempCrawls);
				if(tempCrawls.size() > 1) {
					removeTicketListingCrawl(crawl);
					removeCount++;
					logger.info("Crawl Count Continued : "+crawl.getId()+"_"+crawl.getSiteId());
					continue;
				}
				if (crawl.getCrawlState().equals(CrawlState.RUNNING)
						|| !crawl.getCrawlPhase().equals(CrawlPhase.STOPPED)) {
					if (crawl.getStartCrawl() != null) {
						Integer threshold = crawlDurationThreshold;
						if (crawl.getSiteId().equals("ebay")) {
							threshold = ebayDurationThreshold;
						}
						long duration = (now - crawl.getStartCrawl().getTime()) / 1000;
						if (duration > threshold) {
							// stop it
							stopTicketListingCrawl(crawl);
							logger.info("Forced Stop of crawl " + crawl);
							TicketListingCrawl localCrawl = getTicketListingCrawlById(crawl.getId());
							if (localCrawl != null && !crawl.getCrawlPhase().equals(CrawlPhase.STOPPED)) {
								localCrawl.setCrawlPhase(CrawlPhase.STOPPED);	
								updateTicketListingCrawl(localCrawl);
								logger.info("Forced Stoped If : "+localCrawl);
							}
							count++;
						}
					}
				}
				Boolean falg = tempCrawlMap.get(crawl.getId());
				if(falg != null && falg && crawl.getStartCrawl() != null) {
					if(crawl.getEndCrawl() == null || crawl.getEndCrawl().before(crawl.getStartCrawl())) {
						long duration = (now - crawl.getStartCrawl().getTime()) / 1000;
						if(duration > 900) {
							crawlMap.remove(crawl.getId());
							missedCrawls +=","+crawl.getId();
							missedCrlCount++;
						}
					}
					
				}
			}
			logger.info("Force Stopped CRL Count : "+count+" : "+removeCount);
			logger.info("Missed CRL Count : "+missedCrlCount+" : "+missedCrawls);
			if(missedCrlCount > 0) {
				logger.info("Missed CRAWL Count : "+missedCrlCount+" : "+ new Date());
				System.out.println("Missed CRAWL Count : "+missedCrlCount+" : "+ new Date());
			}
			int dupProceesCount =0,dupAddedCount=0,dupCrawlNotAdded=0;
			for (Integer crawlId : dupCrawlCountMap.keySet()) {
				List<TicketListingCrawl> dupCrawls = dupCrawlCountMap.get(crawlId);
				if(dupCrawls != null && dupCrawls.size() > 1) {
					TicketListingCrawl crawl = dupCrawls.get(0);
					TicketListingCrawl existingCrawl = getTicketListingCrawlById(crawlId);
					if(existingCrawl == null) {
						boolean flag = addTicketListingCrawl(crawl);
						if(flag) {
							dupAddedCount++;	
						} else {
							dupCrawlNotAdded++;	
						}
					}
					dupProceesCount++;
				}
			}
			logger.info("Force CRL Dup Process : "+dupProceesCount+" add: "+dupAddedCount+" nadd: "+dupCrawlNotAdded);
			if(removeCount > 0 || dupProceesCount > 0 ) {
				logger.info("Force CRL Dup Process with count rcount : "+removeCount+" duppros: "+dupProceesCount+" add: "+dupAddedCount+" nadd: "+dupCrawlNotAdded);
				System.out.println("Force CRL Dup Process with count rcount : "+removeCount+" duppros: "+dupProceesCount+" add: "+dupAddedCount+" nadd: "+dupCrawlNotAdded+" : "+ new Date());
			}
			
		} catch (Exception e) {
			logger.warn("Problem while stopping outdated crawls", e);
		}
	}

	public synchronized void recomputeActiveTicketListingCrawlCountBySiteId() {
		synchronized (activeTicketListingCrawlCountBySiteId) {
			Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawlQueue.getAll());
			for (String siteId : Constants.getInstance().getExtendedSiteIds()) {
				activeTicketListingCrawlCountBySiteId.put(siteId, 0);
			}

			Map<Integer,Integer> dupCrawlCountMap = new HashMap<Integer, Integer>(); 
			for (TicketListingCrawl crawl : crawls) {
				if (!crawl.getCrawlPhase().equals(CrawlPhase.STOPPED)) {
					
					Integer count = dupCrawlCountMap.get(crawl.getId());
					if(count == null) {
						count =0;
					}
					count++;
					dupCrawlCountMap.put(crawl.getId(), count);
					if(count > 1) {
						logger.info("Crawl Count Continued : "+crawl.getId()+"_"+crawl.getSiteId());
						continue;
					}
					
					activeTicketListingCrawlCountBySiteId.put(crawl.getSiteId(),activeTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
				}
			}
		}
	}

	public synchronized void stopBuggyCrawls() {
		// stop crawls
		Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(ticketListingCrawlQueue.getAll());
		for (TicketListingCrawl crawl : crawls) {
			if (crawl.getCrawlPhase().equals(CrawlPhase.DISPATCHED)	&& crawl.getStartCrawl() == null) {
				stopTicketListingCrawl(crawl);
				logger.info("Forced Stop of buggy crawl " + crawl);
			}
		}

	}

	public static class TicketListingCrawlHealerThread extends Thread {
		private static Logger logger = LoggerFactory.getLogger(TicketListingCrawlHealerThread.class);
		private TicketListingCrawler crawler;
		public static final long HEAL_PERIOD = 600 * 1000;

		public TicketListingCrawlHealerThread(TicketListingCrawler crawler) {
			this.crawler = crawler;
		}

		public void run() {
			while (true) {
				try {
					crawler.stopOutdatedCrawls();
					crawler.recomputeActiveTicketListingCrawlCountBySiteId();
					crawler.stopBuggyCrawls();
					Thread.sleep(HEAL_PERIOD);
				} catch (Exception e) {
					logger.warn("Problem while healing", e);
				}
			}
		}

	}
}