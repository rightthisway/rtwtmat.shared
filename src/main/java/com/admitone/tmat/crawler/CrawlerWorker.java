package com.admitone.tmat.crawler;

import java.util.Date;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.tangosol.net.CacheFactory;

public final class CrawlerWorker {
	public static void start() {
		System.out.println("==================================================");
		System.out.println("TMAT Crawler");
		System.out.println("");
		System.out.println("(c) 2010 Admit One LLC (Oracle Coherence Version)");
		System.out.println("==================================================");
		System.out.println();

		/*
		 *  check that the dispatcher is running
		 *  not an accurate task but it should do because the program
		 *  will abort anyway if the dispatcher is not running
		 */
		Integer oldestMemberId = CacheFactory.ensureCluster().getOldestMember().getId();
		Integer myMemberId = CacheFactory.ensureCluster().getLocalMember().getId();
		
		if (myMemberId.equals(oldestMemberId)) {
			System.out.println();
			System.out.println("The dispatcher is not running.");
			System.out.println("Exiting...");
			return;
		}

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				System.out.println("Application exiting...");
			}
		});

		System.out.println("Starting Worker at: " + (new Date()));

		new ClassPathXmlApplicationContext(new String[] {
				"spring-dao-database.xml",
				"spring-utils.xml",
				"spring-fetchers.xml",
				"spring-crawler.xml",
				"spring-crawler-pool.xml"
				
		});		
	}
	
	// kill all processes running on this server
	public static void stop() {
		System.out.println("Stopping crawler on this machine...");
		System.out.println("Please wait...");
		new ClassPathXmlApplicationContext(new String[] {
				"spring-dao-database.xml",
				"spring-utils.xml",
				"spring-fetchers.xml",
				"spring-crawler-pool.xml",
				"spring-crawler.xml",
				"spring-stop-crawler.xml"
		});		
	}
	
	private static void printUsage() {
		System.out.println("Possible arguments: start or stop");
	}
	
	public static void main(String[] args) {
		if (args.length != 1) {
			printUsage();
			return;
		}
		
		if (args[0].equalsIgnoreCase("start")) {
			start();
		} else if (args[0].equalsIgnoreCase("stop")) {
			stop();
		}
	}
}