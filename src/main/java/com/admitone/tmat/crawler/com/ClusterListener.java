package com.admitone.tmat.crawler.com;

public interface ClusterListener {
	void memberJoined(Integer nodeId);
	void memberLeft(Integer nodeId);
}
