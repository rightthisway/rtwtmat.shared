package com.admitone.tmat.crawler.com.coherence;

import com.tangosol.net.Cluster;
import com.tangosol.net.Guardable;
import com.tangosol.net.Service;
import com.tangosol.net.ServiceFailurePolicy;

public class DefaultServiceFailurePolicy implements ServiceFailurePolicy{

	public void onServiceFailed(Cluster cluster) {
		// TODO Auto-generated method stub
		System.out.println("-----------************-----------Warning-----------************-----------");
		System.out.println("There was a service failure.... For now ignoring the failed state");
		//cluster.getService("DistributedCache").shutdown();
		//cluster.getService("DistributedCache").start();
		//System.out.println("Started the DistributedCache service again");
		System.out.println("-----------************-----------End Warning-----------************-----------");
	}

	public void onGuardableRecovery(Guardable arg0, Service arg1) {
		// TODO Auto-generated method stub
		
	}

	public void onGuardableTerminate(Guardable arg0, Service arg1) {
		// TODO Auto-generated method stub
		
	}

}
