package com.admitone.tmat.crawler.com;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;

public abstract class TicketListingCrawlDispatcher implements ClusterListener {
	protected Collection<ClusterListener> clusterListeners = new ArrayList<ClusterListener>();
	protected Collection<TicketListingCrawlerListener> statListeners = new ArrayList<TicketListingCrawlerListener>();
	
	public abstract void dispatch(TicketListingCrawl ticketListingCrawl);
	public abstract void dispatchAll(Collection<TicketListingCrawl> ticketListingCrawl);
	/* EI TEST */
	public abstract void dispatchEI(TicketListingCrawl ticketListingCrawl);
	public abstract void dispatchAllEI(Collection<TicketListingCrawl> ticketListingCrawl);
	
	public abstract void dispatch(TicketListingCrawlerMessage message);
	public abstract TicketListingCrawl takeFinishedCrawl();
	public abstract Collection<TicketListingCrawl> takeAllFinishedCrawls();
	public abstract int getTotalActiveThreadCount();
	public abstract Map<Integer, TicketListingCrawlStat> getStatMap();
	public abstract int getNumNodes();
	public abstract int getDispatchQueueSize();
	public abstract int getProxyDispatchQueueSize();
	
	public void addClusterListener(ClusterListener listener) {
		clusterListeners.add(listener);
	}
	
	public void removeClusterListener(ClusterListener listener) {
		clusterListeners.remove(listener);
	}

	public void addStatListener(TicketListingCrawlerListener listener) {
		statListeners.add(listener);
	}
	
	public void removeStatListener(TicketListingCrawlerListener listener) {
		statListeners.remove(listener);
	}

	public void memberJoined(Integer nodeId) {
		for (ClusterListener listener: clusterListeners) {
			listener.memberJoined(nodeId);
		}
	}

	public void memberLeft(Integer nodeId) {
		for (ClusterListener listener: clusterListeners) {
			listener.memberLeft(nodeId);
		}		
	}	
}
