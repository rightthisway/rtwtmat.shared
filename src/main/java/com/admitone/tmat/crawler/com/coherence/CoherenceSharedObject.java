package com.admitone.tmat.crawler.com.coherence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.ConcurrentModificationException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;
import com.admitone.tmat.crawler.com.ClusterListener;
import com.admitone.tmat.crawler.com.SharedObject;
import com.admitone.tmat.crawler.com.TicketListingCrawlerListener;
import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;
import com.admitone.tmat.pojo.TicketData;
import com.admitone.tmat.utils.UniqueLongIDGenerator;
import com.admitone.tmat.web.Constants;
import com.tangosol.net.CacheFactory;
import com.tangosol.net.MemberEvent;
import com.tangosol.net.MemberListener;
import com.tangosol.net.NamedCache;
import com.tangosol.net.Service;
import com.tangosol.util.MapEvent;
import com.tangosol.util.MapListener;
import com.tangosol.util.WrapperException;

public class CoherenceSharedObject implements SharedObject, MapListener, MemberListener{
	private NamedCache dispatchCache;
	private NamedCache finishedCache;
	private NamedCache msgCache;
	private NamedCache statCache;
	private NamedCache ticketCache;
	private NamedCache crawlerPersistenceStatusCache;
	/**
	 * Test for EI and ticketsnow.com
	 * */
	private NamedCache eiDispatchCache;
	private Collection<TicketListingCrawlerListener> msgListeners = new ArrayList<TicketListingCrawlerListener>();
	private Collection<TicketListingCrawlerListener> statListeners = new ArrayList<TicketListingCrawlerListener>();
	private Collection<ClusterListener> clusterListeners = new ArrayList<ClusterListener>();
	private static Long counter = 0L;
	private Timer timer = new Timer();
	private static Logger logger = LoggerFactory.getLogger(CoherenceSharedObject.class);
	public CoherenceSharedObject() {
		dispatchCache = CacheFactory.getCache("dispatchCache");
		finishedCache = CacheFactory.getCache("finishedCache");
		msgCache = CacheFactory.getCache("msgCache");
		statCache = CacheFactory.getCache("statCache");
		ticketCache = CacheFactory.getCache("ticketCache");
		crawlerPersistenceStatusCache = CacheFactory.getCache("crawlerPersistenceStatusCache");
		eiDispatchCache= CacheFactory.getCache("eiDispatchCache");
		UniqueLongIDGenerator.getInstance().reinit(getNodeId());

        Service service = dispatchCache.getCacheService();
        service.addMemberListener(this);
        
        Service eiService = eiDispatchCache.getCacheService();
        eiService.addMemberListener(this);

	}
	
	private synchronized Long incrementCounter() {
		if(counter>=Long.MAX_VALUE-1){
			counter =0L;
		}
//		return counter++;
		return counter;
	}
	
	public void addInDispatchQueue(TicketListingCrawl crawl) {
		synchronized (dispatchCache) {
			dispatchCache.put(crawl.getPriority() + "-" + incrementCounter() + "-" + crawl.getSiteId() + "-" + crawl.getId(), crawl);
		}
	}
	
	public void addAllInDispatchQueue(Collection<TicketListingCrawl> crawls) {
		if(crawls==null || crawls.isEmpty()){
			return;
		}
		synchronized (dispatchCache) {
			Map<String, TicketListingCrawl> crawlMap = new HashMap<String, TicketListingCrawl>();
			Map<Integer,TicketListingCrawl> crawlBoolMap = new HashMap<Integer, TicketListingCrawl>();
			Map<Integer,String> crawlKey = new HashMap<Integer, String>();
			String currentKey ="";
			try{
			for(TicketListingCrawl crawl:crawls){
				crawlBoolMap.put(crawl.getId(),crawl);
				String key = crawl.getPriority() + "-" + incrementCounter() + "-" + crawl.getSiteId() + "-" + crawl.getId();
				crawlMap.put(key , crawl);
				crawlKey.put(crawl.getId(), key);
			}
			List<String> keySet = new ArrayList<String>(dispatchCache.keySet());
			for(String key:keySet){
				currentKey= key;
//				for(Integer crawlId:crawlBoolMap.keySet()){
				Integer crawlId = Integer.parseInt(key.split("-")[3]);
				TicketListingCrawl crawl = crawlBoolMap.get(crawlId);
				if(crawl!=null){
					if(key.contains(""+crawlId)){
						if(Integer.parseInt(key.split("-")[0])>=crawl.getPriority()){
							crawlMap.remove((String)crawlKey.get(key));
						}else{
							dispatchCache.remove(key);
						}
						break;
					}
				}
				
			}
			}catch (Exception e) {
				System.out.println("Error in Dispatch Q.." + currentKey);
			}
			dispatchCache.putAll(crawlMap);
		}
	}

	public void addInFinishedQueue(TicketListingCrawl crawl) {
		logger.info("sending back crawl " + crawl);
		Long key = (long) crawl.getId();
		finishedCache.put(key, crawl);
	}

	public void addMsgListener(TicketListingCrawlerListener listener) {
		if (msgListeners.size() == 0) {
			msgCache.addMapListener(this);
		}
		msgListeners.add(listener);
	}

	public void addStatListener(TicketListingCrawlerListener listener) {
		if (statListeners.size() == 0) {
			statCache.addMapListener(this);
		}
		statListeners.add(listener);
	}
	
	public void removeMsgListener(TicketListingCrawlerListener listener) {
		msgListeners.remove(listener);
		if (msgListeners.size() == 0) {
			msgCache.removeMapListener(this);
		}
	}

	public void removeStatListener(TicketListingCrawlerListener listener) {
		statListeners.remove(listener);
		if (statListeners.size() == 0) {
			statCache.removeMapListener(this);
		}
	}

	public Integer bookNodeCounter() {
		// do nothing and just return the node id assigned by coherence
        return CacheFactory.ensureCluster().getLocalMember().getId();
	}

	// FIXME: need to do some optimization in coherence
	// 2870 invocations of this method takes 563 seconds.
	public int getDispatchQueueSize() {
		return dispatchCache.size();
	}

	public int getProxyDispatchQueueSize() {
		return eiDispatchCache.size();
	}
	public Integer getNodeId() {
        return CacheFactory.ensureCluster().getLocalMember().getId();
	}

	public void putStat(String nodeId, TicketListingCrawlStat stat) {
		msgCache.put(UniqueLongIDGenerator.getInstance().getUID(), stat);
	}

	public void removeListener(TicketListingCrawlerListener listener) {
		msgListeners.remove(listener);
	}

	public void sendMessage(TicketListingCrawlerMessage message) {
		msgCache.put(UniqueLongIDGenerator.getInstance().getUID(), message);
	}
	
	public void saveTicketListToCache(TicketData ticketData){
		ticketCache.put(UniqueLongIDGenerator.getInstance().getUniqueTimestamp(), ticketData);
	}
	
	public void saveCrawlerPersistenceStatusToCache(Integer crawlerId, String status){
		crawlerPersistenceStatusCache.put(crawlerId, status);
	}
	
	private final boolean contains(String[] sites, String site) {
		for (String s: sites) {
			if (s.equals(site)) {
				return true;
			}
		}
		
		return false;
	}

	public synchronized TicketListingCrawl takeFromDispatchQueue(final String[] sites) {
		// add timeout of 30 seconds
		logger.info("Thread " + Thread.currentThread() + ": enter @ " + new Date());
		TimerStop timerStop = new TimerStop(Thread.currentThread());
		timer.schedule(timerStop, 30000);
		
		try {
//			ArrayList<Entry<Long, TicketListingCrawl>> candidates = new ArrayList<Entry<Long, TicketListingCrawl>>();
			// get oldest and with high priority entries first
			
			@SuppressWarnings("unchecked")
			List<String> tempKeys = new ArrayList<String>(dispatchCache.keySet());
			List<String> keys= new ArrayList<String>();
			for(String key:tempKeys){
				String site = key.split("-")[2];
				if(contains(sites, site)){
					keys.add(key);
				}
			}
			System.out.println("[JDK1.8 SORT] - TmatShared.CoherenceSharedObject");		
			keys.sort( new Comparator<String>() {				
				@Override
				public int compare(String key1, String key2) {
					String[] tokens1 = key1.split("-");
					Integer priority1 = Integer.valueOf(tokens1[0]);
					Long id1 = Long.valueOf(tokens1[1]);
					String siteId1 = tokens1[2];
					boolean contains1 = contains(sites, siteId1);

					String[] tokens2 = key2.split("-");
					Integer priority2 = Integer.valueOf(tokens2[0]);
					Long id2 = Long.valueOf(tokens2[1]);
					String siteId2 = tokens2[2];
					boolean contains2 = contains(sites, siteId2);
					
					if (contains1 == contains2) {
						if (priority1 > priority2) {
							return -1;
						} else if (priority1 < priority2) {
							return 1;
						}
						
						return id1.compareTo(id2);
					} else if (contains1) {
						return -1;
					} else {
						return 1;
					}
				}
			});
			
						
			for (String key: keys) {
				TicketListingCrawl crawl =null;
				try{
					boolean flag = dispatchCache.lock(key);
					if(flag){
						crawl = (TicketListingCrawl)dispatchCache.remove(key);
						dispatchCache.unlock(key);
					}
				}catch (WrapperException e) {
					continue;
				}catch (ConcurrentModificationException e) {
					dispatchCache.unlock(key);
				}catch (Exception e) {
					// FIXME : Dirty trick : Crawls drop due to error occured dispatchCache
					System.out.println(key + ".. droped.");
					/*if(crawl==null){
						dispatchCache.put(key,DAORegistry.getTicketListingCrawlDAO().get(Integer.parseInt(key.split("-")[3])));
						dispatchCache.unlock(key);
						System.err.println(key + "added back ..");
					}*/
				}
				if (crawl != null) {
					logger.info("Thread " + Thread.currentThread() + ": return " + crawl.getName() + ":" + crawl.getId() + " With key :" + key + " @ " + new Date());
					return crawl;
				}
			}
			logger.info("Thread " + Thread.currentThread() + ": return @ " + new Date());
			return null;
		} catch(Exception e) {
			logger.info("Reg Log:" + Thread.currentThread() + " has been interrupted", e);
			return null;
		} finally {
			// remove timeout
			timerStop.cancel();
		}
	}
	/* EI TEST */
	
	public synchronized TicketListingCrawl takeFromEIDispatchQueue(final String site[]) {
		// add timeout of 30 seconds
		logger.info("Thread " + Thread.currentThread() + ": enter @ " + new Date());
		TimerStop timerStop = new TimerStop(Thread.currentThread());
		timer.schedule(timerStop, 30000);
		try {
//			ArrayList<Entry<Long, TicketListingCrawl>> candidates = new ArrayList<Entry<Long, TicketListingCrawl>>();
			// get oldest and with high priority entries first
			List<String> keys;
//			if(site.equals(eiSiteId)){
				keys = new ArrayList<String>(eiDispatchCache.keySet());
//			}else{
//				keys = new ArrayList<String>(dispatchCache.keySet());
//			}
		try {
				keys.sort( new Comparator<String>() {
				@Override
				public int compare(String key1, String key2) {
					String[] tokens1 = key1.split("-");
					Integer priority1 = Integer.valueOf(tokens1[0]);
					Long id1 = Long.valueOf(tokens1[1]);
					String siteId1 = tokens1[2];
					boolean contains1 = contains(site,siteId1);

					String[] tokens2 = key2.split("-");
					Integer priority2 = Integer.valueOf(tokens2[0]);
					Long id2 = Long.valueOf(tokens2[1]);
					String siteId2 = tokens2[2];
					boolean contains2 = contains(site,siteId2);
					
					if (contains1 == contains2) {
						if (priority1 < priority2) {
							return 1;
						} else if (priority1 > priority2) {
							return -1;
						}
						
						return id1.compareTo(id2);
					} else if (contains1) {
						return -1;
					} else {
						return 1;
					}
				}
			});
				
		}
		catch(Exception ex) {
			
			System.out.println("Error in Sort CoherenceSharedObject Line 315 " + ex);
		}

//			if(site.equals(eiSiteId)){
				for (String key: keys) {
					TicketListingCrawl crawl =null;
					try{
						boolean flag = eiDispatchCache.lock(key);
						if(flag){
							crawl = (TicketListingCrawl)eiDispatchCache.remove(key);
							eiDispatchCache.unlock(key);
						}
					}catch (Exception e) {
						// FIXME : Dirty trick : Crawls drop due to error occured dispatchCache 
						/*if(crawl==null){
							eiDispatchCache.put(key,DAORegistry.getTicketListingCrawlDAO().get(Integer.parseInt(key.split("-")[3])));
							eiDispatchCache.unlock(key);
							System.err.println(key + "added back..");
						}*/
						System.out.println(key + ".. Droped.");
					}
					if (crawl != null) {
						logger.info("Thread " + Thread.currentThread() + ": return " + crawl.getName() + ":" + crawl.getId() + " With key :" + key + " @ " + new Date());
						return crawl;
					}
				}
//			}
				logger.info("Thread " + Thread.currentThread() + ": return @ " + new Date());
			return null;
		} catch(Exception e) {
			e.printStackTrace();
			logger.info("EI Log:" + Thread.currentThread() + " has been interrupted", e);
			return null;
		} finally {
			// remove timeout
			timerStop.cancel();
		}
	}
	
	// FIXME:  3158 invocations of this methods takes 560s
	public Collection<TicketListingCrawl> takeAllFromDispatchQueue(final String[] sites, int maxSize) {
		// add timeout of 30 seconds
		TimerStop timerStop = new TimerStop(Thread.currentThread());
		timer.schedule(timerStop, 30000);
		
		try {
//			ArrayList<Entry<Long, TicketListingCrawl>> candidates = new ArrayList<Entry<Long, TicketListingCrawl>>();
			// get oldest and with high priority entries first
			
			List<String> keys = new ArrayList<String>(dispatchCache.keySet());
			
			List<String> filteredKeys = new ArrayList<String>();
			for (String key: keys) {
				String[] tokens = key.split("-");
				String site = tokens[1];
				if (contains(sites, site)) {
					filteredKeys.add(site);
				}
			}
			try {
				filteredKeys.sort( new Comparator<String>() {
				@Override
				public int compare(String key1, String key2) {
					String[] tokens1 = key1.split("-");
					Integer priority1 = Integer.valueOf(tokens1[0]);
					Long id1 = Long.valueOf(tokens1[1]);

					String[] tokens2 = key2.split("-");
					Integer priority2 = Integer.valueOf(tokens2[0]);
					Long id2 = Long.valueOf(tokens2[1]);

					if (priority1 < priority2) {
						return 1;
					} else if (priority1 > priority2) {
						return -1;
					}
					
					return id1.compareTo(id2);
				}
			});
			}catch(Exception ex) {
				System.out.println("Exception Sorting CoherenceSharedObject line no 409 " + ex);
			}

			Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>();
			for (String key: filteredKeys) {
				TicketListingCrawl crawl = (TicketListingCrawl)dispatchCache.remove(key);
				if (crawl != null) {
					crawls.add(crawl);
					if (crawls.size() >= maxSize) {
						return crawls;
					}
				}
			}
			
			return null;
		} catch(Exception e) {
			logger.info(Thread.currentThread() + " has been interrupted", e);
			return null;
		} finally {
			// remove timeout
			timerStop.cancel();
		}
	}

	
	public TicketListingCrawl takeFromDispatchQueue() {
		return takeFromDispatchQueue(Constants.getInstance().getSiteIds());
	}

	public Collection<TicketListingCrawl> takeAllFromFinishedQueue() {

//		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
//		TicketListingCrawl crawl = null;
//		do {
//			crawl = takeFromFinishedQueue();
//			if (crawl != null) {
//				result.add(crawl);
//			}
//		} while (crawl != null);

		Collection<Long> keys = new ArrayList<Long>(finishedCache.keySet());
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		for (Long key: keys) {
			TicketListingCrawl crawl = (TicketListingCrawl)finishedCache.remove(key);
			result.add(crawl);
		}
		return result;
	}

	public TicketListingCrawl takeFromFinishedQueue() {
		// return any entry
		Iterator iter = finishedCache.keySet().iterator();
		if (!iter.hasNext()) {
			return null;
		}
		
		Long key = (Long)iter.next();
		
		TicketListingCrawl crawl = (TicketListingCrawl)finishedCache.remove(key);
		logger.info("Finished Took " + key + " " + crawl);
		return crawl;
	}

	public void sendStat(TicketListingCrawlStat stat) {
		logger.info("SEND Updating stat...");
		statCache.put(stat.getNodeId(), stat);
	}

	/**
	 * Cache listeners
	 */
	
	public void entryDeleted(MapEvent mapEvent) {
		// do nothing
	}
	
	public void entryInserted(MapEvent mapEvent) {
		Object object = mapEvent.getNewValue();
		
		if (object instanceof TicketListingCrawlStat) {
			TicketListingCrawlStat stat = (TicketListingCrawlStat)object;
			for (TicketListingCrawlerListener listener: statListeners) {
				listener.onMessage(stat);
			}
		} else if (object instanceof TicketListingCrawlerMessage) {
			TicketListingCrawlerMessage message = (TicketListingCrawlerMessage)object;
			for (TicketListingCrawlerListener listener: msgListeners) {
				listener.onMessage(message);
			}
		}
	}

	public void entryUpdated(MapEvent mapEvent) {
		// same thing than entryInserted
		entryInserted(mapEvent);
	}
	
	public int getNumNodes() {
		return CacheFactory.ensureCluster().getMemberSet().size();		
	}

	public void memberJoined(MemberEvent event) {
		logger.info("Member " + event.getMember().getId() + " just joined the party.");
		for (ClusterListener listener: clusterListeners) {
			listener.memberJoined(event.getMember().getId());
		}
	}

	public void memberLeaving(MemberEvent event) {
		logger.info("Member " + event.getMember().getId() + " is leaving the party.");
		memberLeft(event);
	}

	public void memberLeft(MemberEvent event) {
		Integer leftMemberId = event.getMember().getId();
		logger.info("Member " + leftMemberId + " left the party.");

		for (ClusterListener listener: clusterListeners) {
			listener.memberLeft(event.getMember().getId());
		}
	}

	public void addClusterListener(ClusterListener listener) {
		clusterListeners.add(listener);
	}

	public void removeClusterListener(ClusterListener listener) {
		clusterListeners.remove(listener);
	}

	public Integer getMasterNodeId() {
		return CacheFactory.ensureCluster().getOldestMember().getId();
	}

	public static class TimerStop extends TimerTask {
		private Thread thread;
		private boolean active;
		
		public TimerStop(Thread thread) {
			this.thread = thread;
			active = true;
		}
		
		public synchronized void run() {
			if (!active) {
				return;
			}
			active = false;
			logger.info("Interrupting thread after timeout");
			thread.interrupt();
		}
		
		public boolean cancel() {
			active = false;
			return super.cancel();
		}
	}
	
	/*public int compare(String key1, String key2) {
		String[] tokens1 = key1.split("-");
		Integer priority1 = Integer.valueOf(tokens1[0]);
		Long id1 = Long.valueOf(tokens1[1]);

		String[] tokens2 = key2.split("-");
		Integer priority2 = Integer.valueOf(tokens2[0]);
		Long id2 = Long.valueOf(tokens2[1]);

		if (priority1 < priority2) {
			return 1;
		} else if (priority1 > priority2) {
			return -1;
		}
		
		return id1.compareTo(id2);
	}*/

	public void addInEIDispatchQueue(TicketListingCrawl crawl) {
		synchronized (eiDispatchCache) {
			eiDispatchCache.put(crawl.getPriority() + "-" + incrementCounter() + "-" + crawl.getSiteId() + "-" + crawl.getId(), crawl);
		}
	}
	
	public void addAllInEIDispatchQueue(Collection<TicketListingCrawl> crawls) {
		if(crawls==null || crawls.isEmpty()){
			return;
		}
		synchronized (eiDispatchCache) {
			Map<String, TicketListingCrawl> crawlMap = new HashMap<String, TicketListingCrawl>();
			Map<Integer,TicketListingCrawl> crawlBoolMap = new HashMap<Integer, TicketListingCrawl>();
			Map<Integer,String> crawlKey = new HashMap<Integer, String>();
			String currentKey ="";
			try{
			for(TicketListingCrawl crawl:crawls){
				crawlBoolMap.put(crawl.getId(),crawl);
				String key = crawl.getPriority() + "-" + incrementCounter() + "-" + crawl.getSiteId() + "-" + crawl.getId();
				crawlMap.put(key , crawl);
				crawlKey.put(crawl.getId(), key);
			}
			List<String> keySet = new ArrayList<String>(eiDispatchCache.keySet());
			for(String key:keySet){
				currentKey= key;
//				for(Integer crawlId:crawlBoolMap.keySet()){
				Integer crawlId = Integer.parseInt(key.split("-")[3]);
				TicketListingCrawl crawl = crawlBoolMap.get(crawlId);
				if(crawl!=null){
					if(key.contains(""+crawlId)){
						 
						if(Integer.parseInt(key.split("-")[0])>=crawl.getPriority()){
							crawlMap.remove((String)crawlKey.get(key));
						}else{
							eiDispatchCache.remove(key);
						}
						break;
					}
				}
				
			}
			}catch (Exception e) {
				System.out.println("Error in PDispatch Q.." + currentKey);
			}
			eiDispatchCache.putAll(crawlMap);
		}
		
	}
}
