package com.admitone.tmat.crawler.com;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;
import com.admitone.tmat.data.Site;

public class DefaultTicketListingCrawlReceiver extends TicketListingCrawlReceiver implements InitializingBean, TicketListingCrawlerListener, ClusterListener {
	private Logger logger = LoggerFactory.getLogger(DefaultTicketListingCrawlReceiver.class);
	private SharedObject sharedObject;
	private Collection<TicketListingCrawlerListener> listeners = new ArrayList<TicketListingCrawlerListener>();
	private String hostname;
	private Integer nodeId;
	private Integer masterNodeId;
	private String eiSiteId=Site.AOP;
	public DefaultTicketListingCrawlReceiver() {
		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			hostname = localMachine.getHostName();
			logger.info("In DefaultTicketListingCrawlReceiver constructor, the hostname is: " + hostname);
		} catch (java.net.UnknownHostException uhe) {
			logger.info("Unknown host exception", uhe);
			hostname = "unknown-server";
		}		
	}
	
	public TicketListingCrawl take() {
		return sharedObject.takeFromDispatchQueue();
	}
	
	public TicketListingCrawl take(String[] sites) {
		return sharedObject.takeFromDispatchQueue(sites);
	}
	/* EI TEST */
	public TicketListingCrawl takeEI() {
		return sharedObject.takeFromDispatchQueue();
	}
	public TicketListingCrawl takeEI(String[] sites) {
		return sharedObject.takeFromEIDispatchQueue(sites);
	}
	
	public void sendBack(TicketListingCrawl ticketListingCrawl) {
		sharedObject.addInFinishedQueue(ticketListingCrawl);
	}
	
	public void afterPropertiesSet() throws Exception {
		if (sharedObject == null) {
			throw new RuntimeException("Property sharedObject must be set");
		}

		nodeId = sharedObject.getNodeId();
		masterNodeId = sharedObject.getMasterNodeId();
		sharedObject.addMsgListener(this);
		sharedObject.addClusterListener(this);
		logger.info("In DefaultTicketListingCrawlReceiver afterpropertiesset, the nodeId is: " + nodeId + " the masterNodeId is: " + masterNodeId);
	}
	
	public SharedObject getSharedObject() {
		return sharedObject;
	}

	public void setSharedObject(SharedObject sharedObject) {
		this.sharedObject = sharedObject;
	}
	
	public void updateStat(TicketListingCrawlStat stat) {
		sharedObject.sendStat(stat);
	}
	
	public Integer getNodeId() {
		return nodeId;
	}

	public Integer bookNodeCounter() {
		return sharedObject.bookNodeCounter();
	}
	
	public void addListener(TicketListingCrawlerListener listener) {
		listeners.add(listener);
	}
	
	public void removeListener(TicketListingCrawlerListener listener) {
		listeners.remove(listener);
	}

	public void onMessage(Object message) {
		TicketListingCrawlerMessage msg = (TicketListingCrawlerMessage)message; 
		for (TicketListingCrawlerListener listener: listeners) {
			if (msg.getNodeId() != null) {
				if (!msg.getNodeId().equals(nodeId)) {
					continue;
				}
			}
			
			if (msg.getHostname() != null) {
				if (!msg.getHostname().equals(hostname)) {
					continue;
				}
			}
			listener.onMessage(message);
		}
	}

	public void sendMessage(TicketListingCrawlerMessage message) {
		sharedObject.sendMessage(message);
	}

	public void memberJoined(Integer nodeId) {
	}

	public void memberLeft(Integer nodeId) {
		if (nodeId.equals(masterNodeId)) {
			// shutdown the system
			System.err.println("Master node halted => Exiting...");
			logger.error("The master node shutdown.. Exiting...");
			System.exit(0);
		}
	}

	public Integer getMasterNodeId() {
		return sharedObject.getMasterNodeId();
	}
}