package com.admitone.tmat.crawler.com.local;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;
import com.admitone.tmat.crawler.com.ClusterListener;
import com.admitone.tmat.crawler.com.SharedObject;
import com.admitone.tmat.crawler.com.TicketListingCrawlerListener;
import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;

public class LocalSharedObject implements SharedObject {
	private Queue<TicketListingCrawl> dispatchQueue = new LinkedList<TicketListingCrawl>();
	private Queue<TicketListingCrawl> eiDispatchQueue = new LinkedList<TicketListingCrawl>();
	private Queue<TicketListingCrawl> finishedQueue = new LinkedList<TicketListingCrawl>();
	private Collection<TicketListingCrawlerListener> statListeners = new ArrayList<TicketListingCrawlerListener>();
	private Collection<TicketListingCrawlerListener> msgListeners = new ArrayList<TicketListingCrawlerListener>();
	private Collection<ClusterListener> clusterListeners = new ArrayList<ClusterListener>();
	
	private Integer nodeCounter = 0;
	
	public Integer getNodeId() {
		return nodeCounter;
	}
	
	public Integer bookNodeCounter() {
		return nodeCounter++;
	}
	
	public void sendMessage(TicketListingCrawlerMessage message) {
		for (TicketListingCrawlerListener listener: msgListeners) {
			listener.onMessage(message);
		}
	}
	
	public void addInDispatchQueue(TicketListingCrawl crawl) {
		dispatchQueue.add(crawl);
	}

	public void addInFinishedQueue(TicketListingCrawl crawl) {
		finishedQueue.add(crawl);
	}

	public int getDispatchQueueSize() {
		return dispatchQueue.size();
	}
	
	public int getProxyDispatchQueueSize() {
		return eiDispatchQueue.size();
	}
	
	public TicketListingCrawl takeFromDispatchQueue() {
		try {
			return dispatchQueue.poll();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public TicketListingCrawl takeFromFinishedQueue() {
		try {
			return finishedQueue.poll();
		} catch (NoSuchElementException e) {
			return null;
		}
	}

	public void addMsgListener(TicketListingCrawlerListener listener) {
		msgListeners.add(listener);
	}

	public void addStatListener(TicketListingCrawlerListener listener) {
		statListeners.add(listener);
	}

	public void removeMsgListener(TicketListingCrawlerListener listener) {
		msgListeners.remove(listener);
	}

	public void removeStatListener(TicketListingCrawlerListener listener) {
		statListeners.remove(listener);
	}

	public TicketListingCrawl takeFromDispatchQueue(String[] sites) {
		Collection<TicketListingCrawl> list = new ArrayList<TicketListingCrawl>(dispatchQueue);
		for (TicketListingCrawl crawl: list) {
			for (String site: sites) {
				if (site.equals(crawl.getSiteId())) {
					dispatchQueue.remove(crawl);
					return crawl;
				}
			}
		}
		return null;
	}
	
	public int getNumNodes() {
		return 1;
	}

	public void sendStat(TicketListingCrawlStat stat) {
		for (TicketListingCrawlerListener listener: statListeners) {
			listener.onMessage(stat);
		}
	}

	public void addClusterListener(ClusterListener listener) {
		clusterListeners.add(listener);
	}

	public void removeClusterListener(ClusterListener listener) {
		clusterListeners.remove(listener);
	}
	
	public Integer getMasterNodeId() {
		return 0;
	}

	public Collection<TicketListingCrawl> takeAllFromFinishedQueue() {
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>(finishedQueue);
		finishedQueue.removeAll(result);
		return result;
	}

	public void addAllInDispatchQueue(Collection<TicketListingCrawl> crawls) {
		for (TicketListingCrawl crawl: crawls) {
			addInDispatchQueue(crawl);
		}
	}

	public TicketListingCrawl takeFromEIDispatchQueue(String[] sites) {
		Collection<TicketListingCrawl> list = new ArrayList<TicketListingCrawl>(dispatchQueue);
		for (TicketListingCrawl crawl: list) {
			for (String site: sites) {
				if (site.equals(crawl.getSiteId())) {
					dispatchQueue.remove(crawl);
					return crawl;
				}
			}
		}
		return null;
	}

	public void addInEIDispatchQueue(TicketListingCrawl crawl) {
		eiDispatchQueue.add(crawl);
	}

	public void addAllInEIDispatchQueue(Collection<TicketListingCrawl> crawls) {
			eiDispatchQueue.addAll(crawls);
	}
}