package com.admitone.tmat.crawler.com.gridgain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

public final class GridGainUtil implements Serializable{
	private static Map<String, TicketListingFetcher> ticketListingFetcherBySiteId = new HashMap<String, TicketListingFetcher>();
	private static TicketHitIndexer ticketHitIndexer;

	public static Map<String, TicketListingFetcher> getTicketListingFetcherBySiteId() {
		return ticketListingFetcherBySiteId;
	}

	public void setTicketListingFetcherBySiteId(
			Map<String, TicketListingFetcher> ticketListingFetcherBySiteId) {
		GridGainUtil.ticketListingFetcherBySiteId = ticketListingFetcherBySiteId;
	}

	public static TicketHitIndexer getTicketHitIndexer() {
		return ticketHitIndexer;
	}

	public void setTicketHitIndexer(TicketHitIndexer ticketHitIndexer) {
		GridGainUtil.ticketHitIndexer = ticketHitIndexer;
	}
}