package com.admitone.tmat.crawler.com;

import java.util.Collection;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;

public interface SharedObject {

	Integer getNodeId();
	Integer bookNodeCounter();
	Integer getMasterNodeId();	
	void sendMessage(TicketListingCrawlerMessage message);
	void addAllInDispatchQueue(Collection<TicketListingCrawl> crawls);
	void addInDispatchQueue(TicketListingCrawl crawl);
	void addInFinishedQueue(TicketListingCrawl crawl);
	int getDispatchQueueSize();
	int getProxyDispatchQueueSize();
	TicketListingCrawl takeFromDispatchQueue();
	TicketListingCrawl takeFromDispatchQueue(String[] sites); // take the first crawl belonging to one of the sites
	TicketListingCrawl takeFromEIDispatchQueue(String[] sites);
	TicketListingCrawl takeFromFinishedQueue();
	Collection<TicketListingCrawl> takeAllFromFinishedQueue();
	void sendStat(TicketListingCrawlStat stat);
	void addStatListener(TicketListingCrawlerListener listener);
	void addMsgListener(TicketListingCrawlerListener listener);
	void removeStatListener(TicketListingCrawlerListener listener);
	void removeMsgListener(TicketListingCrawlerListener listener);
	void addClusterListener(ClusterListener listener);
	void removeClusterListener(ClusterListener listener);
	/**
	* EI test 
	*/
	void addInEIDispatchQueue(TicketListingCrawl crawl);
	void addAllInEIDispatchQueue(Collection<TicketListingCrawl> crawls);
	
	int getNumNodes();
}