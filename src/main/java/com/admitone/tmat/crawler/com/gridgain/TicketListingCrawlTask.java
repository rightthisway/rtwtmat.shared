package com.admitone.tmat.crawler.com.gridgain;

import java.io.Serializable;

import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;

public class TicketListingCrawlTask implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer ticketListingCrawlId;
	private CrawlState crawlState;
	private CrawlPhase crawlPhase;
	private Exception exception;
	
	public TicketListingCrawlTask(Integer ticketListingCrawlId) {
		this.ticketListingCrawlId = ticketListingCrawlId;
	}

	public Integer getTicketListingCrawlId() {
		return ticketListingCrawlId;
	}

	public void setTicketListingCrawlId(Integer ticketListingCrawlId) {
		this.ticketListingCrawlId = ticketListingCrawlId;
	}

	public CrawlState getCrawlState() {
		return crawlState;
	}

	public void setCrawlState(CrawlState crawlState) {
		this.crawlState = crawlState;
	}

	public CrawlPhase getCrawlPhase() {
		return crawlPhase;
	}

	public void setCrawlPhase(CrawlPhase crawlPhase) {
		this.crawlPhase = crawlPhase;
	}
	
	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}	
}