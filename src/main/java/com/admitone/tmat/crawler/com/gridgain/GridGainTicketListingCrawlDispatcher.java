package com.admitone.tmat.crawler.com.gridgain;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import org.gridgain.grid.Grid;
import org.gridgain.grid.GridConfigurationAdapter;
import org.gridgain.grid.GridFactory;
import org.gridgain.grid.marshaller.jboss.GridJBossMarshaller;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;
import com.admitone.tmat.crawler.com.TicketListingCrawlDispatcher;
import com.admitone.tmat.crawler.com.TicketListingCrawlerListener;
import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;
import com.admitone.tmat.dao.DAORegistry;

public class GridGainTicketListingCrawlDispatcher extends TicketListingCrawlDispatcher implements InitializingBean, TicketListingCrawlerListener  {
	private Collection<Future<TicketListingCrawlTask>> futures = new ArrayList<Future<TicketListingCrawlTask>>();
	private ExecutorService executorService;
	private Grid grid;
//	private String ip;
	private String eiIP = DAORegistry.getPropertyDAO().get("ei.crawl.machine.ip").getValue();
//	private String otherIP="192.168.0.77";
	
	private final Logger logger = LoggerFactory.getLogger(GridGainTicketListingCrawlDispatcher.class);
	
	public GridGainTicketListingCrawlDispatcher() throws Exception {
		GridConfigurationAdapter adapter = new GridConfigurationAdapter();
		adapter.setPeerClassLoadingEnabled(false);
		
		adapter.setMarshaller(new GridJBossMarshaller());
		logger.info("Starting the grid........");
		this.grid = GridFactory.start(adapter);
		this.executorService = grid.newGridExecutorService();
		logger.info("The executerService is set");
	}
	
	@Override
	public void dispatch(TicketListingCrawl ticketListingCrawl) {
		try {
			if(!InetAddress.getLocalHost().getHostAddress().equals(eiIP)){
				TicketListingCrawlTask ticketListingCrawlTask = new TicketListingCrawlTask(ticketListingCrawl.getId());
				Future<TicketListingCrawlTask> future = executorService.submit(new TicketListingCrawlProcessor(ticketListingCrawlTask));
				
				futures.add(future);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void dispatch(TicketListingCrawlerMessage message) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispatchAll(Collection<TicketListingCrawl> ticketListingCrawls) {
		for (TicketListingCrawl crawl: ticketListingCrawls) {
			dispatch(crawl);
		}
	}

	/* EI TEST */ 
	@Override
	public void dispatchEI(TicketListingCrawl ticketListingCrawl) {
		try {
			if(InetAddress.getLocalHost().getHostAddress().equals(eiIP)){
				TicketListingCrawlTask ticketListingCrawlTask = new TicketListingCrawlTask(ticketListingCrawl.getId());
				Future<TicketListingCrawlTask> future = executorService.submit(new TicketListingCrawlProcessor(ticketListingCrawlTask));
				
				futures.add(future);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void dispatchAllEI(Collection<TicketListingCrawl> ticketListingCrawls) {
		for (TicketListingCrawl crawl: ticketListingCrawls) {
			dispatchEI(crawl);
		}
	}
	
	@Override
	public int getDispatchQueueSize() {
		return futures.size();
	}

	@Override
	public int getProxyDispatchQueueSize() {
		return futures.size()*2;
	}
	
	@Override
	public int getNumNodes() {
		return grid.getAllNodes().size();
	}

	@Override
	public Map<Integer, TicketListingCrawlStat> getStatMap() {
		// TODO Auto-generated method stub
		return new HashMap<Integer, TicketListingCrawlStat>();
	}

	@Override
	public int getTotalActiveThreadCount() {
		return futures.size();
	}

	@Override
	public Collection<TicketListingCrawl> takeAllFinishedCrawls() {
		Collection<TicketListingCrawl> result = new ArrayList<TicketListingCrawl>();
		
		for (Future<TicketListingCrawlTask> future: new ArrayList<Future<TicketListingCrawlTask>>(futures)) {
			if (future.isDone()) {
				try {
					TicketListingCrawlTask task = future.get();
					TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(task.getTicketListingCrawlId());
					crawl.setCrawlPhase(task.getCrawlPhase());
					crawl.setCrawlState(task.getCrawlState());
					result.add(crawl);
					futures.remove(future);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return result;
	}

	@Override
	public TicketListingCrawl takeFinishedCrawl() {
		for (Future<TicketListingCrawlTask> future: new ArrayList<Future<TicketListingCrawlTask>>(futures)) {
			if (future.isDone()) {
				try {
					futures.remove(future);
					TicketListingCrawlTask task = future.get();
					TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(task.getTicketListingCrawlId());
					crawl.setCrawlPhase(task.getCrawlPhase());
					crawl.setCrawlState(task.getCrawlState());
					return crawl;
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (ExecutionException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}

	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public void onMessage(Object message) {
		// TODO Auto-generated method stub
		
	}

}
