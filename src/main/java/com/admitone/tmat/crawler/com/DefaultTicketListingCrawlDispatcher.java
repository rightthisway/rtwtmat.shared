package com.admitone.tmat.crawler.com;

import java.net.InetAddress;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;
import com.admitone.tmat.dao.DAORegistry;

import edu.emory.mathcs.backport.java.util.Arrays;

public class DefaultTicketListingCrawlDispatcher extends TicketListingCrawlDispatcher implements InitializingBean, TicketListingCrawlerListener {
	private SharedObject sharedObject;
	private Map<Integer, TicketListingCrawlStat> statMapByNodeId = new ConcurrentHashMap<Integer, TicketListingCrawlStat>();
//	private String eiIP = DAORegistry.getPropertyDAO().get("ei.crawl.machine.ip").getValue();
	private List<String> eiIP = Arrays.asList(DAORegistry.getPropertyDAO().get("ei.crawl.machine.ip").getValue().split(","));
//	private String eiIP="192.168.0.77";
//	private String otherIP="192.168.0.151";
	
	public void afterPropertiesSet() throws Exception {
		if (sharedObject == null) {
			throw new RuntimeException("Property sharedObject must be set");
		}
		
		sharedObject.addStatListener(this);
		sharedObject.addClusterListener(this);
		
		// add myself
		statMapByNodeId.put(sharedObject.getNodeId(), new TicketListingCrawlStat());
	}

	public void dispatch(TicketListingCrawlerMessage message) {
		if(message.getMsgCode().equals(TicketListingCrawlerMessage.MSG_QUIT)) {
			statMapByNodeId.remove(message.getNodeId());
		}
		sharedObject.sendMessage(message);
	}

	public void dispatchAll(Collection<TicketListingCrawl> ticketListingCrawl) {
		sharedObject.addAllInDispatchQueue(ticketListingCrawl);
	}

	public void dispatch(TicketListingCrawl ticketListingCrawl) {
		sharedObject.addInDispatchQueue(ticketListingCrawl);
	}

	public SharedObject getSharedObject() {
		return sharedObject;
	}

	public void setSharedObject(SharedObject sharedObject) {
		this.sharedObject = sharedObject;
	}
	
	public Map<Integer, TicketListingCrawlStat> getStatMap() {
		return statMapByNodeId;
	}

	public Collection<TicketListingCrawl> takeAllFinishedCrawls() {
		return sharedObject.takeAllFromFinishedQueue();
	}

	public TicketListingCrawl takeFinishedCrawl() {
		return sharedObject.takeFromFinishedQueue();
	}
	
	public int getTotalActiveThreadCount() {
		int count = 0;
		for (TicketListingCrawlStat stat: statMapByNodeId.values()) {
			count += stat.getActiveThreadCount();
		}
		
		return count;
	}
	
	public int getNumNodes() {
		return sharedObject.getNumNodes();
	}

	public void onMessage(Object message) {
		TicketListingCrawlStat stat = (TicketListingCrawlStat) message;
		stat.setLastUpdate(stat.getLastUpdate());
		if (statMapByNodeId.get(stat.getNodeId()) != null) {
			statMapByNodeId.put(stat.getNodeId(), stat);
		}
		
		for (TicketListingCrawlerListener listener: statListeners) {
			listener.onMessage(message);
		}
	}
	
	public int getDispatchQueueSize() {
		return sharedObject.getDispatchQueueSize();
	}
	
	public int getProxyDispatchQueueSize() {
		return sharedObject.getProxyDispatchQueueSize();
	}
	
	public void memberLeft(Integer nodeId) {
		super.memberLeft(nodeId);
		statMapByNodeId.remove(nodeId);
	}
	
	public void memberJoined(Integer nodeId) {
		super.memberJoined(nodeId);
		statMapByNodeId.put(nodeId, new TicketListingCrawlStat());
	}

	/* EI TEST */ 
	@Override
	public void dispatchEI(TicketListingCrawl ticketListingCrawl) {
		try {
			if(eiIP.contains(InetAddress.getLocalHost().getHostAddress())){
				sharedObject.addInEIDispatchQueue(ticketListingCrawl);
			}
		}catch (Exception e) {
			System.out.println("Error while accessing local ip");
		}
	}

	@Override
	public void dispatchAllEI(Collection<TicketListingCrawl> ticketListingCrawls) {
			sharedObject.addAllInEIDispatchQueue(ticketListingCrawls);
	}
	
	
}