package com.admitone.tmat.crawler.com;

import java.io.Serializable;

public class TicketListingCrawlerMessage implements Serializable {
	public final static int MSG_START = 1;
	public final static int MSG_STOP = 2;
	public final static int MSG_RESET_STAT = 3;
	public final static int MSG_QUIT = 4; // tell the node to abort
	public final static int MSG_UPDATE_CATEGORIZATION = 5;
	public final static int MSG_RESET_PROPERTIES = 6;
	public final static int MSG_DISABLE_CRAWLS_FOR_TOUR = 7;
	public final static int MSG_ENABLE_CRAWLS_FOR_TOUR = 8;
	public final static int MSG_PAUSE_CRAWLS_FOR_TOUR = 9;
	public final static int MSG_RESUME_CRAWLS_FOR_TOUR = 10;
	public final static int MSG_PAUSE_CRAWLS_FOR_EVENT = 11;
	public final static int MSG_RESUME_CRAWLS_FOR_EVENT = 12;
	public final static int MSG_DISABLE_CRAWLS_FOR_EVENT = 13;
	public final static int MSG_ENABLE_CRAWLS_FOR_EVENT = 14;
	public final static int MSG_ENABLE_CRAWL_ID = 15;
	public final static int MSG_DISABLE_CRAWL_ID = 16;
	
	private Integer msgCode;
	private String hostname;
	private Integer nodeId;
	private Object object;
	
	public TicketListingCrawlerMessage() {}

	public TicketListingCrawlerMessage(Integer msgCode) {
		this.msgCode = msgCode;
	}

	public TicketListingCrawlerMessage(String hostname, Integer msgCode) {
		this.hostname = hostname;
		this.msgCode = msgCode;
	}

	public TicketListingCrawlerMessage(Integer nodeId, Integer msgCode) {
		this.nodeId = nodeId;
		this.msgCode = msgCode;
	}

	public Integer getMsgCode() {
		return msgCode;
	}
	
	public void setMsgCode(Integer msgCode) {
		this.msgCode = msgCode;
	}
	
	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}	

	public Integer getNodeId() {
		return nodeId;
	}

	public void setNodeId(Integer nodeId) {
		this.nodeId = nodeId;
	}

	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
}
