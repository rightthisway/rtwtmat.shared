package com.admitone.tmat.crawler.com.gridgain;

import java.io.Serializable;
import java.util.Date;
import java.util.concurrent.Callable;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TimedOutTicketListingCrawlException;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;

public class TicketListingCrawlProcessor implements Callable<TicketListingCrawlTask>, Serializable {
	private static final long serialVersionUID = 1L;
	private TicketListingCrawlTask ticketListingCrawlTask;

	public TicketListingCrawlProcessor(TicketListingCrawlTask ticketListingCrawlTask) {
		this.ticketListingCrawlTask = ticketListingCrawlTask;
	}
	
	public TicketListingCrawlProcessor() {}
	
	public TicketListingCrawlTask call() throws Exception {
		Integer crawlId = ticketListingCrawlTask.getTicketListingCrawlId();
		TicketListingCrawl ticketListingCrawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
		Date now = new Date();
		ticketListingCrawl.setStartCrawl(now);

		String siteId = ticketListingCrawl.getSiteId();
		try {
			TicketListingFetcher fetcher = GridGainUtil.getTicketListingFetcherBySiteId().get(siteId);
			if (fetcher == null) {
				ticketListingCrawl.setCrawlPhase(CrawlPhase.REQUEUING);
				return ticketListingCrawlTask;
			}
			
			TicketHitIndexer ticketHitIndexer = GridGainUtil.getTicketHitIndexer();
			boolean finished = false;
			try{
				finished = fetcher.fetchTicketListing(ticketHitIndexer, ticketListingCrawl);
			}catch(Exception e){
				finished = false;
			}
			if (ticketListingCrawl.isCancelled()) {
				throw new TimedOutTicketListingCrawlException("TIMED OUT");
			}
	
			if (!finished) {
				System.out.println("Throwing interrupted exception");
				throw new Exception("Interrupted");
			}
			
			ticketHitIndexer.flush(ticketListingCrawl);
			ticketListingCrawlTask.setCrawlPhase(CrawlPhase.REQUEUING);
			ticketListingCrawlTask.setCrawlState(CrawlState.STOPPED);
		} catch(Exception e) {
			e.printStackTrace();
			ticketListingCrawlTask.setException(e);
			ticketListingCrawlTask.setCrawlPhase(CrawlPhase.REQUEUING);
			ticketListingCrawlTask.setCrawlState(CrawlState.EXCEPTION);
//			ticketListingCrawlTask.setEndCrawl(new Date());
		}
		return ticketListingCrawlTask;
	}

}
