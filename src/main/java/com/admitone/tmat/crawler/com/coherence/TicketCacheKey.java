package com.admitone.tmat.crawler.com.coherence;

import java.io.Serializable;

public class TicketCacheKey implements Serializable, Comparable<TicketCacheKey>{
	private Long timeStamp;
	private Long uuid;
	
	public TicketCacheKey(Long timeStamp, Long uuid){
		this.timeStamp = timeStamp;
		this.uuid = uuid;
	}
	
	public Long getTimeStamp() {
		return timeStamp;
	}

	public Long getUuid() {
		return uuid;
	}
	
	@Override
	public int hashCode(){
		return uuid.hashCode();
	}
	
	@Override
	public String toString(){
		return this.timeStamp.toString() + " : " + this.uuid.toString();
	}
	
	public int compareTo(TicketCacheKey o) {
		int result = this.timeStamp.compareTo(o.timeStamp);
		if(result != 0){
			return result;
		}else{
			return this.uuid.compareTo(o.uuid);
		}
	}
	
	@Override
	public boolean equals(Object o){
		if(o==this){
			return true;
		}
		if(o==null || !(o instanceof TicketCacheKey)){
			return false;
		}
		
		TicketCacheKey copy = (TicketCacheKey)o;
		return this.timeStamp.equals(copy.timeStamp) && this.uuid.equals(copy.uuid);
	}
	
	
	
	
}
