package com.admitone.tmat.crawler.com;

import java.io.Serializable;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawlStat;


public abstract class TicketListingCrawlReceiver implements Serializable{
	public abstract TicketListingCrawl take();
	public abstract TicketListingCrawl take(String[] sites);
	/* EI Test */
	public abstract TicketListingCrawl takeEI();
	public abstract TicketListingCrawl takeEI(String[] sites);
	
	
	public abstract void updateStat(TicketListingCrawlStat stat);
	public abstract void sendBack(TicketListingCrawl ticketListingCrawl);
	public abstract Integer getNodeId();
	public abstract Integer getMasterNodeId();
	public abstract Integer bookNodeCounter();
	public abstract void addListener(TicketListingCrawlerListener listener);
	public abstract void removeListener(TicketListingCrawlerListener listener);
	public abstract void sendMessage(TicketListingCrawlerMessage message);
}
