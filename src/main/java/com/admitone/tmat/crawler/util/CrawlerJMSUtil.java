package com.admitone.tmat.crawler.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.jms.JMSMessageSender;
import com.admitone.tmat.utils.SpringUtil;

public class CrawlerJMSUtil extends TimerTask {
	private Map<String,CrawlStatus> crawlStatusMap = new ConcurrentHashMap<String, CrawlStatus>();
	private JMSMessageSender sender;
	
	public void addEventToCheckStatus(String eventId,LocalDestination destination,Integer totalCount){
		CrawlStatus status = crawlStatusMap.get(eventId);
		List<LocalDestination> destinationList =null;
		if(status==null){
			status = new CrawlStatus();
			destinationList = new ArrayList<LocalDestination>();
			status.setExecutedCrawl(0);
		}else{
			destinationList = status.getDestination();
		}
		destinationList.add(destination);
		status.setEventId(eventId);
		status.setDestination(destinationList);
		status.setTotalCrawl(totalCount);
		crawlStatusMap.put(eventId, status);
	}
	
	public void addExecutedEvents(String eventId,Integer totalCount,Integer executedCount){
		CrawlStatus status = crawlStatusMap.get(eventId);
		if(status==null){
			status = new CrawlStatus();
		}
		status.setLastTimeCrawl(new Date());
		status.setExecutedCrawl(executedCount);
		status.setEventId(eventId);
		status.setTotalCrawl(totalCount);
		crawlStatusMap.put(eventId, status);
	}
	
	public synchronized void checkCrawlStatus(){
		Iterator<Entry<String, CrawlStatus>> itr =  crawlStatusMap.entrySet().iterator();
		while(itr.hasNext()){
			Map.Entry<String, CrawlStatus> entry = itr.next();
			String eventIdStr = entry.getKey();
			Integer eventId = Integer.parseInt(eventIdStr);
			CrawlStatus status = crawlStatusMap.get(eventIdStr);
			try {
				TicketListingCrawler ticketListingCrawler = SpringUtil.getTicketListingCrawler();
//				Integer result = ticketListingCrawler.forceRecrawlForEvent(eventId);
//				if (result < 0) {
					// check if maybe some crawls are running
					Integer count = ticketListingCrawler.getActiveTicketListingCrawlsByEventId(eventId).size();
					Integer runningCount = ticketListingCrawler.getCrawlsExecutedForEventCount(eventId);
					boolean flag=false; 
					if (runningCount != status.getExecutedCrawl()) {
						flag=true;
					}
					status.setTotalCrawl(count);
					status.setExecutedCrawl(runningCount);
					if(flag){
						if(status.getExecutedCrawl()==status.getTotalCrawl()){
							for(LocalDestination dest:status.getDestination()){
								status.setLastTimeCrawl(new Date());
								String msg= "OK--" + status.getTotalCrawl() + "--" + status.getExecutedCrawl();
								if(dest.equals(LocalDestination.ADMIN)){
									sender.sendMessageToAdmin("event-executed-" + eventId,msg);
								}
								if(dest.equals(LocalDestination.ANALYTICS)){
									sender.sendMessageToAnalytics("event-executed-" + eventId,msg);
								}
								if(dest.equals(LocalDestination.BROWSE)){
									sender.sendMessageToBrowse("event-executed-" + eventId,msg);
								}
									
							}
							crawlStatusMap.remove(eventIdStr);
						}
					}
//				} 
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	public Map<String, CrawlStatus> getCrawlStatusMap() {
		return crawlStatusMap;
	}

	@Override
	public void run() {
		checkCrawlStatus();
		
	}
	
	public Date getLeastRecentlyCrawledDate(Integer eventId) {
		return SpringUtil.getTicketListingCrawler().getLeastCrawledDateForEvent(eventId);
	}

	public JMSMessageSender getSender() {
		return sender;
	}

	public void setSender(JMSMessageSender sender) {
		this.sender = sender;
	}

}
