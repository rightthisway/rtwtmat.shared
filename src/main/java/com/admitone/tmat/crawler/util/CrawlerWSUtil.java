package com.admitone.tmat.crawler.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.utils.SpringUtil;

public class CrawlerWSUtil extends TimerTask {
	private Map<String,CrawlStatus> crawlStatusMap = new ConcurrentHashMap<String, CrawlStatus>();
	Logger logger = LoggerFactory.getLogger(CrawlerWSUtil.class);
	public void addEventToCheckStatus(String eventId,String postBackUrl,Integer totalCount){
		CrawlStatus status = crawlStatusMap.get(eventId);
		List<String> postBackUrlList =null;
		if(status==null){
			status = new CrawlStatus();
			postBackUrlList = new ArrayList<String>();
			status.setExecutedCrawl(0);
		}else{
			postBackUrlList = status.getPostBackUrls();
		}
		postBackUrlList.add(postBackUrl);
		status.setEventId(eventId);
		status.setPostBackUrls(postBackUrlList);
		status.setTotalCrawl(totalCount);
		crawlStatusMap.put(eventId, status);
	}
	
	public void addExecutedEvents(String eventId,Integer totalCount,Integer executedCount){
		CrawlStatus status = crawlStatusMap.get(eventId);
		if(status==null){
			status = new CrawlStatus();
		}
		status.setLastTimeCrawl(new Date());
		status.setExecutedCrawl(executedCount);
		status.setEventId(eventId);
		status.setTotalCrawl(totalCount);
		crawlStatusMap.put(eventId, status);
	}
	
	public synchronized void checkCrawlStatus(){
		Iterator<Entry<String, CrawlStatus>> itr =  crawlStatusMap.entrySet().iterator();
		while(itr.hasNext()){
			Map.Entry<String, CrawlStatus> entry = itr.next();
			String eventIdStr = entry.getKey();
			Integer eventId = Integer.parseInt(eventIdStr);
			CrawlStatus status = crawlStatusMap.get(eventIdStr);
			try {
				TicketListingCrawler ticketListingCrawler = SpringUtil.getTicketListingCrawler();
//				Integer result = ticketListingCrawler.forceRecrawlForEvent(eventId);
//				if (result < 0) {
					// check if maybe some crawls are running
					Integer count = ticketListingCrawler.getActiveTicketListingCrawlsByEventId(eventId).size();
					Integer runningCount = ticketListingCrawler.getCrawlsExecutedForEventCount(eventId);
					boolean flag=false; 
					if (runningCount != status.getExecutedCrawl()) {
						flag=true;
					}
					status.setTotalCrawl(count);
					status.setExecutedCrawl(runningCount);
					if(flag){
						status.setLastTimeCrawl(new Date());
						String msg= "OK--" + status.getTotalCrawl() + "--" + status.getExecutedCrawl();
						for(String url:status.getPostBackUrls()){
							try{
								SendRequest sn = new SendRequest(eventIdStr, msg, url);
								sn.setPriority(Thread.MAX_PRIORITY);
								sn.start();
							}catch (Exception e) {
								logger.error(e.fillInStackTrace().getMessage());
							}
						}
						if(status.getExecutedCrawl()==status.getTotalCrawl()){
							crawlStatusMap.remove(eventIdStr);
						}
					}
//				} 
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public class SendRequest extends Thread{
		String eventId;
		String msg;
		String url;
		public SendRequest(String eventId,String msg,String url) {
			this.eventId=eventId;
			this.msg=msg;
			this.url = url;
		}
		@Override
		public void run() {
			try {
				HttpClient hc = new DefaultHttpClient();
				HttpPost hp = new HttpPost(url);
				NameValuePair nameValuePair = new BasicNameValuePair("msg", msg);
				NameValuePair nameValuePair1 = new BasicNameValuePair("eventId", eventId);
				List<NameValuePair> parameters = new ArrayList<NameValuePair>();
				parameters.add(nameValuePair);
				parameters.add(nameValuePair1);
				UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
				hp.setEntity(entity);
				hc.execute(hp);
			} catch (ClientProtocolException e) {
				logger.error(e.fillInStackTrace().getMessage());
			} catch (IOException e) {
				logger.error(e.fillInStackTrace().getMessage());
			} catch (Exception e) {
				logger.error(e.fillInStackTrace().getMessage());
			}

		}
		
	}
	
	public Map<String, CrawlStatus> getCrawlStatusMap() {
		return crawlStatusMap;
	}

	@Override
	public void run() {
		checkCrawlStatus();
		
	}
	
	public Date getLeastRecentlyCrawledDate(Integer eventId) {
		return SpringUtil.getTicketListingCrawler().getLeastCrawledDateForEvent(eventId);
	}

}
