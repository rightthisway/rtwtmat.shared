package com.admitone.tmat.crawler;

import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.enums.CrawlerSchedulerMode;

public class CrawlerSchedulerManager implements InitializingBean {
	private static final Integer DEFAULT_CRAWL_FREQUENCY = 3600;
	private Pattern timeWithUnitPattern = Pattern.compile("(\\d+)\\s*([mhdy])\\s*=\\s*(\\d+)\\s*([mhdy])\\s*");
	private Integer automaticScheduleStartHour;
	private Integer automaticScheduleStopHour;
	private TicketListingCrawler ticketListingCrawler;
	private Timer timer = null;
	private CrawlerSchedulerMode schedulerMode = CrawlerSchedulerMode.ALWAYS_SCHEDULED;

	public void afterPropertiesSet() throws Exception {
		if (ticketListingCrawler == null) {
			throw new RuntimeException("Property ticketListingCrawler has to be set");
		}
	}
	
	/*private Collection<ScheduleProperty> scheduleProperties = new TreeSet<ScheduleProperty>(new Comparator<ScheduleProperty>() {
		public int compare(ScheduleProperty sp1, ScheduleProperty sp2) {
			return sp2.getTime().compareTo(sp1.getTime());
		}
	});*/
	
	private Collection<ScheduleProperty> sportsScheduleProperties = new TreeSet<ScheduleProperty>(new Comparator<ScheduleProperty>() {
		public int compare(ScheduleProperty sp1, ScheduleProperty sp2) {
			return sp2.getTime().compareTo(sp1.getTime());
		}
	});
	
	private Collection<ScheduleProperty> concertsScheduleProperties = new TreeSet<ScheduleProperty>(new Comparator<ScheduleProperty>() {
		public int compare(ScheduleProperty sp1, ScheduleProperty sp2) {
			return sp2.getTime().compareTo(sp1.getTime());
		}
	});
	
	private Collection<ScheduleProperty> theaterScheduleProperties = new TreeSet<ScheduleProperty>(new Comparator<ScheduleProperty>() {
		public int compare(ScheduleProperty sp1, ScheduleProperty sp2) {
			return sp2.getTime().compareTo(sp1.getTime());
		}
	});
	
	private Collection<ScheduleProperty> defaultScheduleProperties = new TreeSet<ScheduleProperty>(new Comparator<ScheduleProperty>() {
		public int compare(ScheduleProperty sp1, ScheduleProperty sp2) {
			return sp2.getTime().compareTo(sp1.getTime());
		}
	});
	
	private Collection<ScheduleProperty> lasVegasScheduleProperties = new TreeSet<ScheduleProperty>(new Comparator<ScheduleProperty>() {
		public int compare(ScheduleProperty sp1, ScheduleProperty sp2) {
			return sp2.getTime().compareTo(sp1.getTime());
		}
	});
	
	private Integer getFactorUnit(String unit) {
		if (unit.equalsIgnoreCase("m")) {
			return 60;
		}

		if (unit.equalsIgnoreCase("h")) {
			return 3600;
		}
		
		if (unit.equalsIgnoreCase("d")) {
			return 24 * 3600;
		}		
/* CS Does not make any sense.. and month can be of either 28 or 29 or 30 or 31 days 
		if (unit.equalsIgnoreCase("M")) {
			return 30 * 24 * 3600;
		}
 CS */
		if (unit.equalsIgnoreCase("y")) {
			return 365 * 24 * 3600;
		}
		
		if (unit.equalsIgnoreCase("IDLE")) {
			return 50 * 365 * 24 * 3600;
		}
		return -1;
	}
	
	private Integer getTimeInSeconds(Integer time, String unit) {
		return time * getFactorUnit(unit);
	}
	
	public void reinit() {
		//scheduleProperties.clear();
		sportsScheduleProperties.clear();
		concertsScheduleProperties.clear();
		theaterScheduleProperties.clear();
		defaultScheduleProperties.clear();
		lasVegasScheduleProperties.clear();
		
		Property schedulerModeProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.mode");
		Property schedulerStartHour = DAORegistry.getPropertyDAO().get("crawler.scheduler.startHour");
		Property schedulerStopHour = DAORegistry.getPropertyDAO().get("crawler.scheduler.stopHour");
		
		//Property schedulerProperty = DAORegistry.getPropertyDAO().get("crawler.scheduler.properties");
		Property sportsSchedulerProperty = DAORegistry.getPropertyDAO().get("crawler.sports.scheduler.properties");
		Property concertsSchedulerProperty = DAORegistry.getPropertyDAO().get("crawler.concerts.scheduler.properties");
		Property theaterSchedulerProperty = DAORegistry.getPropertyDAO().get("crawler.theater.scheduler.properties");
		Property defaultSchedulerProperty = DAORegistry.getPropertyDAO().get("crawler.default.scheduler.properties");
		Property lasVegasSchedulerProperty = DAORegistry.getPropertyDAO().get("crawler.lasvegas.scheduler.properties");

		try {
			if (schedulerStartHour != null) {
				automaticScheduleStartHour = Integer.parseInt(schedulerStartHour.getValue());
			}
	
			if (schedulerStopHour != null) {
				automaticScheduleStopHour = Integer.parseInt(schedulerStopHour.getValue());
			}
			
			if (schedulerModeProperty != null) {
				schedulerMode = CrawlerSchedulerMode.valueOf(schedulerModeProperty.getValue());
			}
			
		} catch(NumberFormatException e) {
			automaticScheduleStartHour = null;
			automaticScheduleStopHour = null;
			schedulerMode = CrawlerSchedulerMode.ALWAYS_SCHEDULED;
		}

		/*if (schedulerProperty == null) {
			return;
		}
		
		for (String prop: schedulerProperty.getValue().split(",")) {
			Matcher matcher = timeWithUnitPattern.matcher(prop);
			if (!matcher.find()) {
				throw new RuntimeException("Incorrect format for property " + prop);
			}
			
			Integer unconvertedTime = Integer.parseInt(matcher.group(1));
			String unit = matcher.group(2);
			
			Integer unconvertedFrequencyTime = Integer.parseInt(matcher.group(3));
			String frequencyUnit = matcher.group(4);
			
			Integer time = getTimeInSeconds(unconvertedTime, unit);
			Integer frequencyTime = getTimeInSeconds(unconvertedFrequencyTime, frequencyUnit);
			
			ScheduleProperty scheduleProperty = new ScheduleProperty(time, frequencyTime);
			scheduleProperties.add(scheduleProperty);
		}*/
		
		
		if (sportsSchedulerProperty == null) {
			return;
		}
		
		for (String prop: sportsSchedulerProperty.getValue().split(",")) {
			Matcher matcher = timeWithUnitPattern.matcher(prop);
			if (!matcher.find()) {
				throw new RuntimeException("Incorrect format for property " + prop);
			}
			
			Integer unconvertedTime = Integer.parseInt(matcher.group(1));
			String unit = matcher.group(2);
			
			Integer unconvertedFrequencyTime = Integer.parseInt(matcher.group(3));
			String frequencyUnit = matcher.group(4);
			
			Integer time = getTimeInSeconds(unconvertedTime, unit);
			Integer frequencyTime = getTimeInSeconds(unconvertedFrequencyTime, frequencyUnit);
			
			ScheduleProperty scheduleProperty = new ScheduleProperty(time, frequencyTime);
			sportsScheduleProperties.add(scheduleProperty);
		}
		
		if (concertsSchedulerProperty == null) {
			return;
		}
		
		for (String prop: concertsSchedulerProperty.getValue().split(",")) {
			Matcher matcher = timeWithUnitPattern.matcher(prop);
			if (!matcher.find()) {
				throw new RuntimeException("Incorrect format for property " + prop);
			}
			
			Integer unconvertedTime = Integer.parseInt(matcher.group(1));
			String unit = matcher.group(2);
			
			Integer unconvertedFrequencyTime = Integer.parseInt(matcher.group(3));
			String frequencyUnit = matcher.group(4);
			
			Integer time = getTimeInSeconds(unconvertedTime, unit);
			Integer frequencyTime = getTimeInSeconds(unconvertedFrequencyTime, frequencyUnit);
			
			ScheduleProperty scheduleProperty = new ScheduleProperty(time, frequencyTime);
			concertsScheduleProperties.add(scheduleProperty);
		}
		
		if (theaterSchedulerProperty == null) {
			return;
		}
		
		for (String prop: theaterSchedulerProperty.getValue().split(",")) {
			Matcher matcher = timeWithUnitPattern.matcher(prop);
			if (!matcher.find()) {
				throw new RuntimeException("Incorrect format for property " + prop);
			}
			
			Integer unconvertedTime = Integer.parseInt(matcher.group(1));
			String unit = matcher.group(2);
			
			Integer unconvertedFrequencyTime = Integer.parseInt(matcher.group(3));
			String frequencyUnit = matcher.group(4);
			
			Integer time = getTimeInSeconds(unconvertedTime, unit);
			Integer frequencyTime = getTimeInSeconds(unconvertedFrequencyTime, frequencyUnit);
			
			ScheduleProperty scheduleProperty = new ScheduleProperty(time, frequencyTime);
			theaterScheduleProperties.add(scheduleProperty);
		}
		
		if (defaultSchedulerProperty == null) {
			return;
		}
		
		for (String prop: defaultSchedulerProperty.getValue().split(",")) {
			Matcher matcher = timeWithUnitPattern.matcher(prop);
			if (!matcher.find()) {
				throw new RuntimeException("Incorrect format for property " + prop);
			}
			
			Integer unconvertedTime = Integer.parseInt(matcher.group(1));
			String unit = matcher.group(2);
			
			Integer unconvertedFrequencyTime = Integer.parseInt(matcher.group(3));
			String frequencyUnit = matcher.group(4);
			
			Integer time = getTimeInSeconds(unconvertedTime, unit);
			Integer frequencyTime = getTimeInSeconds(unconvertedFrequencyTime, frequencyUnit);
			
			ScheduleProperty scheduleProperty = new ScheduleProperty(time, frequencyTime);
			defaultScheduleProperties.add(scheduleProperty);
		}
		
		if (lasVegasSchedulerProperty == null) {
			return;
		}
		
		for (String prop: lasVegasSchedulerProperty.getValue().split(",")) {
			Matcher matcher = timeWithUnitPattern.matcher(prop);
			if (!matcher.find()) {
				throw new RuntimeException("Incorrect format for property " + prop);
			}
			
			Integer unconvertedTime = Integer.parseInt(matcher.group(1));
			String unit = matcher.group(2);
			
			Integer unconvertedFrequencyTime = Integer.parseInt(matcher.group(3));
			String frequencyUnit = matcher.group(4);
			
			Integer time = getTimeInSeconds(unconvertedTime, unit);
			Integer frequencyTime = getTimeInSeconds(unconvertedFrequencyTime, frequencyUnit);
			
			ScheduleProperty scheduleProperty = new ScheduleProperty(time, frequencyTime);
			lasVegasScheduleProperties.add(scheduleProperty);
		}
		

		if (timer != null) {
			timer.cancel();
		}
		timer = new Timer();

		if (automaticScheduleStartHour != null) {
			Date now = new Date();
			Calendar start = new GregorianCalendar();
			
			start.set(Calendar.HOUR_OF_DAY, getAutomaticScheduleStartHour());
			start.set(Calendar.MINUTE, 0);
			start.set(Calendar.SECOND, 0);
			if (start.getTime().getTime() < now.getTime()) {
				start.add(Calendar.DATE, 1);
			}
			
			Calendar stop = new GregorianCalendar();
			stop.set(Calendar.HOUR_OF_DAY, getAutomaticScheduleStopHour());
			stop.set(Calendar.MINUTE, 0);
			stop.set(Calendar.SECOND, 0);
			if (stop.getTime().getTime() < now.getTime()) {
				stop.add(Calendar.DATE, 1);
			}
			timer.scheduleAtFixedRate(new StartStopAutomaticScheduleTask(ticketListingCrawler), start.getTime(), 24 * 3600 * 1000);
			timer.scheduleAtFixedRate(new StartStopAutomaticScheduleTask(ticketListingCrawler), stop.getTime(), 24 * 3600 * 1000);
		}
		timer.schedule(new StartStopAutomaticScheduleTask(ticketListingCrawler), 1000L);
	}
	
	// return frequency in seconds
	public Integer getFrequency(Date date,String parentCategory) {
		if (!isAutomaticScheduledTime()) {
			return DEFAULT_CRAWL_FREQUENCY;
		}
		
		if (date == null) {
			return DEFAULT_CRAWL_FREQUENCY;
		}
		
		Date now = new Date();
		Long diff = (date.getTime() - now.getTime()) / 1000;
		
		if(parentCategory.equalsIgnoreCase("Sports")){
			for (ScheduleProperty property: sportsScheduleProperties) {
				if (diff > property.getTime()) {
					return property.getFrequency();
				}
			}
		}else if(parentCategory.equalsIgnoreCase("Concerts")){
			for (ScheduleProperty property: concertsScheduleProperties) {
				if (diff > property.getTime()) {
					return property.getFrequency();
				}
			}
		}else if(parentCategory.equalsIgnoreCase("Theater")){
			for (ScheduleProperty property: theaterScheduleProperties) {
				if (diff > property.getTime()) {
					return property.getFrequency();
				}
			}
		}else if(parentCategory.equalsIgnoreCase("Las Vegas")){
			for (ScheduleProperty property: lasVegasScheduleProperties) {
				if (diff > property.getTime()) {
					return property.getFrequency();
				}
			}
		}else{// if(parentCategory.equalsIgnoreCase("Default")){  // Changed by CS on 06.15.2015 for default and other parent category
			for (ScheduleProperty property: defaultScheduleProperties) {
				if (diff > property.getTime()) {
					return property.getFrequency();
				}
			}
		}
		return DEFAULT_CRAWL_FREQUENCY;  // No need to use default 
	}
	
	public static class ScheduleProperty {
		private Integer time;
		private Integer frequency;
		
		public ScheduleProperty(Integer time, Integer frequency) {
			this.time = time;
			this.frequency = frequency;
		}
		
		public Integer getTime() {
			return time;
		}
		
		public void setTime(Integer time) {
			this.time = time;
		}
		
		public Integer getFrequency() {
			return frequency;
		}
		
		public void setFrequency(Integer frequency) {
			this.frequency = frequency;
		}
		
		public String toString() {
			return "[ScheduleProperty time=" + getTime() + " frequency=" + getFrequency() + "]";
		}
	}
	
	public static class StartStopAutomaticScheduleTask extends TimerTask {
		private Date lastUpdate = null;
		private TicketListingCrawler ticketListingCrawler;

		public StartStopAutomaticScheduleTask(TicketListingCrawler ticketListingCrawler) {
			this.ticketListingCrawler = ticketListingCrawler;
		}
		
		public void run() {
			Calendar now = new GregorianCalendar();
			if (lastUpdate != null && now.getTime().getTime() - lastUpdate.getTime() < 30000) {
				return;
			}
		
			lastUpdate = now.getTime();
			
			ticketListingCrawler.recomputeCrawlFrequency();
		}
	}
	
	public int getAutomaticScheduleStartHour() {
		return automaticScheduleStartHour;
	}

	public int getAutomaticScheduleStopHour() {
		return automaticScheduleStopHour;
	}

	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}
	
	public boolean isAutomaticScheduledTime() {
		if (schedulerMode.equals(CrawlerSchedulerMode.ALWAYS_SCHEDULED)) {
			return true;
		}

		if (schedulerMode.equals(CrawlerSchedulerMode.ALWAYS_UNSCHEDULED)) {
			return false;
		}

		Calendar cal = new GregorianCalendar();
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		if ((automaticScheduleStartHour < automaticScheduleStopHour && (hour < automaticScheduleStartHour
			|| hour >= automaticScheduleStopHour))
		    || (automaticScheduleStopHour <= hour && hour < automaticScheduleStartHour)) {
			return false;
		}
		return true;
	}
	
	public CrawlerSchedulerMode getSchedulerMode() {
		return schedulerMode;
	}
}