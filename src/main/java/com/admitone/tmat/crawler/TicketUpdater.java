package com.admitone.tmat.crawler;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketItemFetcher;
import com.admitone.tmat.utils.LatestEventCache;

/**
 * Class in charge of updating information (price) of the ticket already in the DB
 * 
 * TODO: maybe rename this class to TicketExpirationProcesser as this is what the class now does
 * There should be one per site Id.
 */
public class TicketUpdater implements InitializingBean {
	private final Logger logger = LoggerFactory.getLogger(TicketUpdater.class);

	private Map<String, TicketItemFetcher> ticketItemFetcherBySiteId = new HashMap<String, TicketItemFetcher>();	
	private TicketHitIndexer ticketHitIndexer;
	private LatestEventCache latestEventCache;
		
	public void afterPropertiesSet() throws Exception {
		if (ticketItemFetcherBySiteId == null) {
			throw new Exception("Property ticketItemFetcherBySiteId must be set");
		}

		if (ticketHitIndexer == null) {
			throw new Exception("Property ticketHitIndexer must be set");
		}
	}
	
	public void setTicketItemFetchers(Map<String, TicketItemFetcher> ticketItemFetchers) {
		this.ticketItemFetcherBySiteId = ticketItemFetchers;
	}

	public TicketHitIndexer getTicketHitIndexer() {
		return ticketHitIndexer;
	}

	public void setTicketHitIndexer(TicketHitIndexer ticketHitIndexer) {
		this.ticketHitIndexer = ticketHitIndexer;
	}

	public void setLatestEventCache(LatestEventCache latestEventCache) {
		this.latestEventCache = latestEventCache;
	}
	
	private void updateTickets(TicketListingCrawl crawl, Collection<Ticket> tickets) throws Exception {
		// keep fetchingTime, extractionTime and indexationTime to restore them after the update
		long fetchingTime = crawl.getFetchingTime();
		long extractionTime = crawl.getExtractionTime();
		long indexationTime = crawl.getIndexationTime();
		
		TicketItemFetcher fetcher = ticketItemFetcherBySiteId.get(crawl.getSiteId());
		
		for (Ticket ticket: tickets) {
			if (crawl.isCancelled()) {
				break;
			}
			
			// if there is no item fetcher, then we assume that if the product was not crawled during the last
			// crawl, then it was deleted
			
			TicketHit ticketHit = null;
			try {
				ticketHit = fetcher.fetchTicketItem(crawl, ticket.getItemId());
			} catch (Exception e) {
				e.printStackTrace();
				/*
				// ebay can throw this exception if the item is no longer accessible
				if (e.getMessage() != null && (e.getMessage().indexOf("No Item Was Found") >= 0 || e.getMessage().indexOf("no longer in our database") >= 0)) {
					TicketHit ticketHit = new TicketHit(ticket);
					ticketHit.setTicketStatus(TicketStatus.SOLD);
					ticketHitIndexer.indexTicketHit(ticketHit);			
				} else {
					e.printStackTrace();
					throw e;
				}
				*/
			}				
			if (ticketHit == null) {
				ticketHit = new TicketHit(ticket);
				ticketHit.setTicketStatus(TicketStatus.SOLD);
			} else {
				if (ticketHit.getSoldQuantity() == null || ticketHit.getSoldQuantity() == 0) {
					ticketHit.setTicketStatus(TicketStatus.EXPIRED);
				} else {
					ticketHit.setTicketStatus(TicketStatus.SOLD);					
				}
			}
				
			ticketHit.setTicketListingCrawl(ticket.getTicketListingCrawl());
			
			int result = ticketHitIndexer.indexTicketHit(ticketHit);

			if (result == TicketHitIndexer.NEW_ITEM) {
				crawl.setNewItemIndexed(crawl.getNewItemIndexed() + 1);
				crawl.setItemIndexed(crawl.getItemIndexed() + 1);
			} else if (result == TicketHitIndexer.EXISTING_ITEM) {
				crawl.setItemIndexed(crawl.getItemIndexed() + 1);
			} else if (result == TicketHitIndexer.ERROR) {
				crawl.setItemErrorIndexed(crawl.getItemErrorIndexed() + 1);					
			}		
		} 

		// restore fetchingTime, extractionTime and indexationTime to restore them after the update
		crawl.setFetchingTime(fetchingTime);
		crawl.setExtractionTime(extractionTime);
		crawl.setIndexationTime(indexationTime);
		
		//refresh the events with active inventory
		latestEventCache.refreshLatestEventsEntry();
	}

	public void updateOutDatedExpiredTicketsFromCrawl(TicketListingCrawl crawl) throws Exception {
		// update expired tickets
		
		TicketItemFetcher fetcher = ticketItemFetcherBySiteId.get(crawl.getSiteId());
		if (fetcher == null) {
			DAORegistry.getTicketDAO().updateStatusOfOutdatedExpiredTicketsFromCrawl(crawl.getId(), TicketStatus.SOLD);
			return;
		}

		Collection<Ticket> expiredTickets = DAORegistry.getTicketDAO().getAllOutDatedExpiredTicketsFromCrawl(crawl.getId());		
		updateTickets(crawl, expiredTickets);
	}

	public void updateTicketsUpdatedBeforeFromCrawl(TicketListingCrawl crawl, Date date) throws Exception {
		// update tickets which were not updated recently
		// if it was skipped by the crawler, that means that the ticket is not sold anymore
		
		if (crawl.getStartCrawl() == null) {
			return;
		}

		TicketItemFetcher fetcher = ticketItemFetcherBySiteId.get(crawl.getSiteId());
		if (fetcher == null) {
			DAORegistry.getTicketDAO().updateStatusOfTicketsUpdatedBeforeFromCrawl(crawl.getId(), date, TicketStatus.SOLD);
			return;
		}
		
		Collection<Ticket> outdatedTickets = DAORegistry.getTicketDAO().getAllActiveTicketsUpdatedBeforeFromCrawl(crawl.getId(), date);
		updateTickets(crawl, outdatedTickets);		
	}

}
