package com.admitone.tmat.crawler;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.hibernate.stat.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.crawler.TicketListingCrawlStat.SiteStat;
import com.admitone.tmat.crawler.com.TicketListingCrawlReceiver;
import com.admitone.tmat.crawler.com.TicketListingCrawlerListener;
import com.admitone.tmat.crawler.com.TicketListingCrawlerMessage;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.web.Constants;
import com.jolbox.bonecp.BoneCPDataSource;

import edu.emory.mathcs.backport.java.util.Arrays;

public class TicketListingCrawlerThreadPool implements TicketListingCrawlerListener, InitializingBean {
	private Logger logger = LoggerFactory.getLogger(TicketListingCrawlerThreadPool.class);
	private boolean running = false;
	private TicketUpdater ticketUpdater;
	private ExecutorService crawlerExecutorService;
	private TicketHitIndexer ticketHitIndexer;
	private Map<String, TicketListingFetcher> ticketListingFetcherBySiteId = new HashMap<String, TicketListingFetcher>();
	private Collection<TicketListingCrawlerTask> ticketListingCrawlerTasks = new ArrayList<TicketListingCrawlerTask>();
	private TicketListingCrawlReceiver ticketListingCrawlReceiver;
	private TicketListingCrawlStat ticketListingCrawlStat;
	private Map<String, Integer> activeTicketListingCrawlCountBySiteId = new ConcurrentHashMap<String, Integer>();
	private DataSource dataSource;
	private Map<Integer, Date> lastUpdatedMap = new ConcurrentHashMap<Integer, Date>();
	private Integer crawlDurationThreshold = 600; // default value. Will be loaded from the DB property crawler.error.crawlDurationThreshold
	private Integer ebayDurationThreshold = 900;
	private SessionFactory sessionFactory;
	private Set<Integer> disabledTours = new  HashSet<Integer>();
	private Set<Integer> disabledEvents = new  HashSet<Integer>();
	private TicketListingCrawlerTimeOutManager ticketListingCrawlerTimeOutManager;
	private Map<Integer, TicketListingCrawlerTask> ticketListingCrawlerTaskById = new ConcurrentHashMap<Integer, TicketListingCrawlerTask>();
	private List<String> eiIP = Arrays.asList(DAORegistry.getPropertyDAO().get("ei.crawl.machine.ip").getValue().split(","));
	Map<String, Site> siteByIdMap = DAORegistry.getSiteDAO().getSiteByIdMap();
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	public TicketListingCrawlerThreadPool() {
		for (String siteId : Constants.getInstance().getExtendedSiteIds()) {
			activeTicketListingCrawlCountBySiteId.put(siteId, 0);
		}
		
	}
	
	public void afterPropertiesSet() throws Exception {
		if (ticketListingFetcherBySiteId == null) {
			throw new Exception(
					"Property ticketListingFetcherBySiteId must be set");
		}

		if (ticketListingCrawlReceiver == null) {
			throw new RuntimeException("Property ticketListingCrawlReceiver must be set");
		}

		if (dataSource == null) {
			throw new RuntimeException("Property dataSource must be set");
		}

		if (sessionFactory == null) {
			throw new RuntimeException("Property sessionFactory must be set");
		}
		
		sessionFactory.getStatistics().setStatisticsEnabled(true);

		ticketListingCrawlReceiver.addListener(this);

		String hostname = "server";
	 
		try {
			java.net.InetAddress localMachine = java.net.InetAddress.getLocalHost();
			hostname = localMachine.getHostName();
		} catch (java.net.UnknownHostException uhe) {
			logger.warn("Unknown host exception", uhe);
		}
		reinitProperties();		
		ticketListingCrawlStat = new TicketListingCrawlStat(hostname, ticketListingCrawlReceiver.getNodeId());

		ticketListingCrawlStat.setTicketHitIndexerStat(ticketHitIndexer.getStat());
		ticketListingCrawlStat.reset();
		ticketListingCrawlReceiver.updateStat(ticketListingCrawlStat);
		
		new BandwidthMonitor(this);

		TicketListingCrawlerPoolHealerThread ticketListingCrawlerPoolHealerThread = new TicketListingCrawlerPoolHealerThread(this);
		ticketListingCrawlerPoolHealerThread.start();
		
		TicketListingCrawlerPoolStatThread ticketListingCrawlerPoolStatThread = new TicketListingCrawlerPoolStatThread(this);
		ticketListingCrawlerPoolStatThread.start();
		start();
	}
	
	public synchronized TicketListingCrawl startTicketListingCrawl() {
		Date now = new Date();
		
		Collection<String> siteList = new ArrayList<String>();
		for (Entry<String, Site> entry: siteByIdMap.entrySet()) {
			Site site = entry.getValue();
			if (activeTicketListingCrawlCountBySiteId.get(site.getId()) < site.getMaxConcurrentConnections()) {
					siteList.add(site.getId());
			}
		}
		
		String[] sites = new String[siteList.size()];
		siteList.toArray(sites);
		TicketListingCrawl crawl =null;
		String ip="";
		try {
			ip=InetAddress.getLocalHost().getHostAddress();
			if(eiIP.contains(ip)){
				crawl = ticketListingCrawlReceiver.takeEI(sites);
			}else{
				crawl = ticketListingCrawlReceiver.take(sites);
			}
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (crawl == null) {
			return null;
		}
		
		synchronized (activeTicketListingCrawlCountBySiteId) {
			activeTicketListingCrawlCountBySiteId.put(
					crawl.getSiteId(),
					activeTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);
//			System.out.println(new Date() + ":" + crawl.getName() + ":" + activeTicketListingCrawlCountBySiteId.get(crawl.getSiteId()));
		}
		ticketListingCrawlStat.computeCrawlerDelayStat(crawl);
		crawl.setDispatchingTime(now.getTime() - crawl.getStartCrawl().getTime());

		if (disabledTours.contains(crawl.getEvent().getArtistId())) {
			crawl.setSuspended(true);
			stopTicketListingCrawl(crawl, CrawlState.STOPPED, null);
			return null;
		}
		crawl.setStartCrawl(new Date());
		return crawl;
	}
	
	public synchronized void updateStat() {
		ticketListingCrawlStat.getCrawlPhaseMap().clear();
		
		for (TicketListingCrawlerTask task: ticketListingCrawlerTasks) {
			TicketListingCrawl crawl = task.getProcessedCrawl();
			if (crawl != null) {
				ticketListingCrawlStat.getCrawlPhaseMap().put(crawl.getId(), crawl.getCrawlPhase());
			}
		}
		
		for (Entry<String, Integer> entry: activeTicketListingCrawlCountBySiteId.entrySet()) {
			SiteStat siteStat = ticketListingCrawlStat.getSiteStat(entry.getKey());
			siteStat.setActiveTicketListingCrawl(activeTicketListingCrawlCountBySiteId.get(entry.getKey()));
		}

		ticketListingCrawlStat.setThreadLastUpdatedMap(lastUpdatedMap);
		ticketListingCrawlStat.getSystemStat().setMaxMemory(Runtime.getRuntime().maxMemory());
		ticketListingCrawlStat.getSystemStat().setTotalMemory(Runtime.getRuntime().totalMemory());
		ticketListingCrawlStat.getSystemStat().setFreeMemory(Runtime.getRuntime().freeMemory());
		ticketListingCrawlStat.getSystemStat().setFreeHttpClientCountBySiteId(HttpClientStore.getFreeHttpClientCountBySiteId());
		ticketListingCrawlStat.getSystemStat().setAllocatedHttpClientCountBySiteId(HttpClientStore.getAllocatedHttpClientCountBySiteId());
		ticketListingCrawlStat.getSystemStat().setAverageHttpClientLifeTimeBySiteId(HttpClientStore.getAverageHttpClientLifeTimeBySiteId());

		try {
			Statistics statistics = sessionFactory.getStatistics();
			ticketListingCrawlStat.getSystemStat().setDBCloseStatementCount(statistics.getCloseStatementCount());
			ticketListingCrawlStat.getSystemStat().setDBFlushCount(statistics.getFlushCount());
			ticketListingCrawlStat.getSystemStat().setDBConnectCount(statistics.getConnectCount());
			ticketListingCrawlStat.getSystemStat().setDBQueryExecutionCount(statistics.getQueryExecutionCount());
			ticketListingCrawlStat.getSystemStat().setDBTransactionCount(statistics.getTransactionCount());
			ticketListingCrawlStat.getSystemStat().setDBSuccessfulTransactionCount(statistics.getSuccessfulTransactionCount());
			ticketListingCrawlStat.getSystemStat().setDBSessionOpenCount(statistics.getSessionOpenCount());
			ticketListingCrawlStat.getSystemStat().setDBSessionCloseCount(statistics.getSessionCloseCount());
			ticketListingCrawlStat.getSystemStat().setDBPrepareStatementCount(statistics.getPrepareStatementCount());
			ticketListingCrawlStat.getSystemStat().setDBOptimisticFailureCount(statistics.getOptimisticFailureCount());
			ticketListingCrawlStat.getSystemStat().setDBQueryExecutionMaxTime(statistics.getQueryExecutionMaxTime());
			ticketListingCrawlStat.getSystemStat().setDBQueryExecutionMaxTimeQueryString(statistics.getQueryExecutionMaxTimeQueryString());
			ticketListingCrawlStat.getSystemStat().setDBConnectionCount((long)((BoneCPDataSource)dataSource).getTotalLeased());
//			for C3P0 database connection pool
//			ticketListingCrawlStat.getSystemStat().setDBConnectionCount((long)dataSource.dataSource.NumConnections());
//			ticketListingCrawlStat.getSystemStat().setDBBusyConnectionCount((long)dataSource.getgetNumBusyConnections());
//			ticketListingCrawlStat.getSystemStat().setDBBusyConnectionCount((long)dataSource.getNumBusyConnections());
//			ticketListingCrawlStat.getSystemStat().setDBIdleConnectionCount((long)dataSource.getNumIdleConnections());
//			ticketListingCrawlStat.getSystemStat().setDBUnclosedOrphanedConnectionCount((long)dataSource.getNumUnclosedOrphanedConnections());
		} catch (Exception e) {
			ticketListingCrawlStat.getSystemStat().setDBConnectionCount(null);
			ticketListingCrawlStat.getSystemStat().setDBBusyConnectionCount(null);
			ticketListingCrawlStat.getSystemStat().setDBIdleConnectionCount(null);
			ticketListingCrawlStat.getSystemStat().setDBUnclosedOrphanedConnectionCount(null);			
		}
		ticketListingCrawlStat.setLastUpdate(new Date());
		ticketListingCrawlReceiver.updateStat(ticketListingCrawlStat);
	}
	
	public void reinitProperties() {
		try{
			crawlDurationThreshold = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.crawlMaxDuration").getValue());
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void stopTicketListingCrawl(TicketListingCrawl ticketListingCrawl,
			CrawlState crawlState, Exception exception) {
			synchronized (activeTicketListingCrawlCountBySiteId) {
				if (ticketListingCrawl.getCrawlPhase().equals(CrawlPhase.REQUEUING)) {
					return;
				}
				activeTicketListingCrawlCountBySiteId.put(
						ticketListingCrawl.getSiteId(),activeTicketListingCrawlCountBySiteId.get(ticketListingCrawl.getSiteId()) - 1);
				if (ticketListingCrawl.getCrawlPhase().equals(CrawlPhase.REQUEUING)) {
					logger.info("** " + ticketListingCrawl + " already stopped");
					return;
				}
				ticketListingCrawl.setCrawlPhase(CrawlPhase.REQUEUING);
			}
			try {
				ticketListingCrawl.setCrawlState(crawlState);
				if (exception != null) {
					if (exception.getMessage() == null
							|| exception.getMessage().length() == 0) {
						ticketListingCrawl.setErrorMessage(exception.getClass()
								.getName());
					} else {
						ticketListingCrawl.setErrorMessage(exception.getMessage());
					}
					ticketListingCrawl.setException(exception);
					exception.printStackTrace();
				} else if (ticketListingCrawl.getItemIndexed() == 0) {
					ticketListingCrawl.setErrorMessage("No tickets found");
					ticketListingCrawl.setCrawlState(CrawlState.NO_TIX);
					crawlState = CrawlState.NO_TIX;
				}

				ticketListingCrawl.setEndCrawl(new Date());
				DAORegistry.getTicketListingCrawlDAO().update(ticketListingCrawl);
				if (!crawlState.isError()) {
					if (!ticketListingCrawl.getSiteId().equals(Site.SEATWAVE_FEED)) {
						// FIXME: remove this
						if (ticketListingCrawl.getStartCrawl() == null) {
							logger.info("*** TMPFIX ERROR " + ticketListingCrawl + " has a start date which is null");
							ticketListingCrawl.setStartCrawl(new Date(new Date().getTime() - 10000));
						}
						
					    ticketListingCrawlStat.incrementTotalTimeToProcessCrawl(ticketListingCrawl.getEndCrawl()
													.getTime() - ticketListingCrawl.getStartCrawl().getTime());
					
					    ticketListingCrawlStat.incrementTotalProcessedTicketCount(ticketListingCrawl.getItemIndexed());
					    ticketListingCrawlStat.incrementTotalProcessedCrawlCount(1);
					    SiteStat siteStat = ticketListingCrawlStat.getSiteStat(ticketListingCrawl.getSiteId());
					    siteStat.incrementTotalProcessedTicketCount(ticketListingCrawl.getItemIndexed());
					    siteStat.incrementTotalTimeToProcessCrawl(ticketListingCrawl.getEndCrawl()
							.getTime() - ticketListingCrawl
							.getStartCrawl().getTime());
					    siteStat.incrementTotalProcessedCrawlCount(1);
					    siteStat.incrementTotalFetchingTime(ticketListingCrawl.getFetchingTime());
					    siteStat.incrementTotalExtractionTime(ticketListingCrawl.getExtractionTime());
					    siteStat.incrementTotalIndexationTime(ticketListingCrawl.getIndexationTime());
					    siteStat.incrementTotalExpirationTime(ticketListingCrawl.getExpirationTime());
					    siteStat.incrementTotalFetchedByteCount(ticketListingCrawl.getFetchedByteCount());
					    siteStat.incrementTotalDispatchingTime(ticketListingCrawl.getDispatchingTime());
					    siteStat.incrementTotalFlushingTime(ticketListingCrawl.getFlushingTime());
					}
					ticketListingCrawlStat.setActiveThreadCount(getActiveThreadCount());
				    ticketListingCrawlReceiver.sendBack(ticketListingCrawl);
					return;
				}
				ticketListingCrawlStat.setActiveThreadCount(getActiveThreadCount());
			    ticketListingCrawlReceiver.sendBack(ticketListingCrawl);
			} catch (Exception e) {
				logger.warn("Error while stopping crawl", e);
//				System.out.println("Error while stopping crawl" + e.fillInStackTrace());
			}
	}
	
	public synchronized void start() {
		if (running) {
			return;
		}
		
		// FIXME: dirty hack to make sure the daoregistry is initialized (even though in spring we make depends-on the daoregistry)
		while (DAORegistry.getSiteDAO() == null) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		running = true;
		int threadCount = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.thread.count").getValue());

		crawlerExecutorService = Executors.newFixedThreadPool(threadCount);

		for (int i = 0; i < threadCount; i++) {
			TicketListingCrawlerTask ticketListingCrawlerTask = new TicketListingCrawlerTask(this, i,ticketListingCrawlReceiver);
			ticketListingCrawlerTask.setTicketListingFetcherMap(ticketListingFetcherBySiteId);
			ticketListingCrawlerTask.setTicketUpdater(ticketUpdater);
			ticketListingCrawlerTask.setTicketHitIndexer(ticketHitIndexer);
			crawlerExecutorService.execute(ticketListingCrawlerTask);
			ticketListingCrawlerTasks.add(ticketListingCrawlerTask);
			ticketListingCrawlerTaskById.put(ticketListingCrawlerTask.getId(), ticketListingCrawlerTask);
		}		
	}
	
	public synchronized void stop() {
		if (!running) {
			return;
		}
		
		for (TicketListingCrawlerTask task: ticketListingCrawlerTasks) {
			task.cancel();
		}
		
		crawlerExecutorService.shutdown();
		try {
			crawlerExecutorService.awaitTermination(30, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		running = false;
	}
	
	public int getActiveThreadCount() {
		int activeThreadCount = 0;
		for (TicketListingCrawlerTask ticketListingCrawlerTask : ticketListingCrawlerTasks) {
			if (ticketListingCrawlerTask.isRunning()) {
				activeThreadCount++;
			}
		}
		return activeThreadCount;
	}
	
	public void reinit() {
		if (running) {
			return;
		}

		running = true;
	}

	public void addTicketListingFetcher(String siteId,
			TicketListingFetcher ticketListingFetcher) {
		ticketListingFetcherBySiteId.put(siteId, ticketListingFetcher);
	}

	public TicketListingFetcher getTicketListingFetcher(String siteId) {
		return ticketListingFetcherBySiteId.get(siteId);
	}

	public Map<String, TicketListingFetcher> getTicketListingFetcherMap() {
		return ticketListingFetcherBySiteId;
	}

	public void setTicketListingFetcherMap(
			Map<String, TicketListingFetcher> ticketListingFetcherBySiteId) {
		this.ticketListingFetcherBySiteId = ticketListingFetcherBySiteId;
	}

	public Set<Map.Entry<String, TicketListingFetcher>> getTicketListingFetcherBySiteIdEntrySet() {
		return ticketListingFetcherBySiteId.entrySet();
	}

	public void setTicketUpdater(TicketUpdater ticketUpdater) {
		this.ticketUpdater = ticketUpdater;
	}

	public TicketHitIndexer getTicketHitIndexer() {
		return ticketHitIndexer;
	}

	public void setTicketHitIndexer(TicketHitIndexer ticketHitIndexer) {
		this.ticketHitIndexer = ticketHitIndexer;
	}

	public Map<String, TicketListingFetcher> getTicketListingFetcherBySiteId() {
		return ticketListingFetcherBySiteId;
	}

	public void setTicketListingFetcherBySiteId(
			Map<String, TicketListingFetcher> ticketListingFetcherBySiteId) {
		this.ticketListingFetcherBySiteId = ticketListingFetcherBySiteId;
	}

	public TicketUpdater getTicketUpdater() {
		return ticketUpdater;
	}

	public TicketListingCrawlReceiver getTicketListingCrawlReceiver() {
		return ticketListingCrawlReceiver;
	}

	public void setTicketListingCrawlReceiver(
			TicketListingCrawlReceiver ticketListingCrawlReceiver) {
		this.ticketListingCrawlReceiver = ticketListingCrawlReceiver;
	}

	public void disableCrawlsForArtist(Integer artistId) {
		disabledTours.add(artistId);
		for (TicketListingCrawlerTask task: ticketListingCrawlerTasks) {
			task.cancelCrawlForArtist(artistId);
		}
	}

	public void disableCrawlsForEvent(Integer eventId) {
		disabledEvents.add(eventId);
		for (TicketListingCrawlerTask task: ticketListingCrawlerTasks) {
			task.cancelCrawlForEvent(eventId);
		}
	}

	public void enableCrawlsForTour(Integer tourId) {
		disabledTours.remove(tourId);
	}
	
	public void enableCrawlsForEvent(Integer eventId) {
		disabledEvents.remove(eventId);
	}

	public boolean isRunning() {
		return running;
	}

	public void onMessage(Object obj) {
		TicketListingCrawlerMessage message = (TicketListingCrawlerMessage) obj;
		switch(message.getMsgCode()) {
			case TicketListingCrawlerMessage.MSG_START:
				start();
				break;
				
			case TicketListingCrawlerMessage.MSG_STOP:
				stop();
				break;				

			case TicketListingCrawlerMessage.MSG_DISABLE_CRAWLS_FOR_TOUR:
				disableCrawlsForArtist((Integer)message.getObject());
				break;				

			case TicketListingCrawlerMessage.MSG_ENABLE_CRAWLS_FOR_TOUR:
				enableCrawlsForTour((Integer)message.getObject());
				break;				

			case TicketListingCrawlerMessage.MSG_QUIT: // quit
				System.err.println("Quit Request... Exiting...");
				// if i'm a worker exit, otherwise let the program leaves (the master)
				if (!ticketListingCrawlReceiver.getMasterNodeId().equals(ticketListingCrawlReceiver.getNodeId())) {
					System.exit(0);
				}
				break;				

			case TicketListingCrawlerMessage.MSG_RESET_STAT:
				ticketListingCrawlStat.reset();
				ticketListingCrawlReceiver.updateStat(ticketListingCrawlStat);
				break;

			case TicketListingCrawlerMessage.MSG_RESET_PROPERTIES:
				reinitProperties();
				break;

			case TicketListingCrawlerMessage.MSG_UPDATE_CATEGORIZATION:
				logger.info("Updating categories for tourId=" + message.getObject());
//				Categorizer.update((Integer)message.getObject());
				break;
		}
	}

	public TicketListingCrawlStat getTicketListingCrawlStat() {
		return ticketListingCrawlStat;
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	
	public void updateLastUpdated(Integer id) {
		lastUpdatedMap.put(id, new Date());
	}

	public synchronized void stopBlockedCrawls() {
		// FIXME: what is it about?
		/*
		for (TicketListingCrawlerTask task: ticketListingCrawlerTasks) {
			TicketListingCrawl crawl =  task.getProcessedCrawl();
			if (crawl != null && !crawl.getCrawlPhase().equals(CrawlPhase.REQUEUING)) {
				
			}
		}
		*/
	}

	public synchronized void recomputeActiveTicketListingCrawlCountBySiteId() {
		synchronized (activeTicketListingCrawlCountBySiteId) {
			for (String siteId : Constants.getInstance().getExtendedSiteIds()) {
				activeTicketListingCrawlCountBySiteId.put(siteId, 0);
			}
			
			for (TicketListingCrawlerTask task: ticketListingCrawlerTasks) {
				TicketListingCrawl crawl =  task.getProcessedCrawl();
				if (crawl != null && !crawl.getCrawlPhase().equals(CrawlPhase.REQUEUING)) {
					activeTicketListingCrawlCountBySiteId.put(crawl.getSiteId(),
							activeTicketListingCrawlCountBySiteId.get(crawl.getSiteId()) + 1);					
				}
			}
		}
	}
	
	public TicketListingCrawlerTask getTicketListingCrawlerTaskById(Integer id) {
		return ticketListingCrawlerTaskById.get(id);
	}
	
	public synchronized void timeOutTaskThreads() {
		Date now = new Date();
		//System.out.println(">>> IN TIME OUT TASK THREADS SIZE=" + lastUpdatedMap.entrySet().size());

		for (Entry<Integer, Date> entry: lastUpdatedMap.entrySet()) {
			Integer id = entry.getKey();
			Date date = entry.getValue();
			TicketListingCrawlerTask task = getTicketListingCrawlerTaskById(id);
			if (task == null || task.getProcessedCrawl() == null) {
				continue;
			}
			Integer threshold = crawlDurationThreshold;
			if(task.getProcessedCrawl().getSiteId().equals("ebay")) {
				threshold = ebayDurationThreshold;
			}
			if (now.getTime() - date.getTime() > threshold  * 1000) {
				task.interrupt();
			}
		}
	}
	
	public static class TicketListingCrawlerPoolHealerThread extends Thread {
		public final static long HEAL_PERIOD = 600 * 1000;
		public TicketListingCrawlerThreadPool threadPool;
		
		public TicketListingCrawlerPoolHealerThread(TicketListingCrawlerThreadPool threadPool) {
			this.threadPool = threadPool;
			//System.out.println(">>> INIT POOL HEALER THREAD");
		}
		
		public void run() {
			//System.out.println(">>> RUNNING POOL HEALER THREAD");
			while(true) {
				try {
//					System.out.println(">>> CALL TIME OUT TASK THREADS");
					threadPool.timeOutTaskThreads();
					threadPool.recomputeActiveTicketListingCrawlCountBySiteId();
				} catch(Exception e) {
					//e.printStackTrace();
				}
				try {
					Thread.sleep(HEAL_PERIOD);
				} catch (InterruptedException e) {
					// e.printStackTrace();
				}
			}
		}
	}
	
	public static class TicketListingCrawlerPoolStatThread extends Thread {
		public final static long STAT_UPDATE_PERIOD = 5 * 1000;
		public TicketListingCrawlerThreadPool threadPool;
		
		public TicketListingCrawlerPoolStatThread(TicketListingCrawlerThreadPool threadPool) {
			this.threadPool = threadPool;
		}
		
		public void run() {
			while(true) {
				try {
					threadPool.updateStat();
					Thread.sleep(STAT_UPDATE_PERIOD);
				} catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	public TicketListingCrawlerTimeOutManager getTicketListingCrawlerTimeOutManager() {
		return ticketListingCrawlerTimeOutManager;
	}

	public void setTicketListingCrawlerTimeOutManager(
			TicketListingCrawlerTimeOutManager ticketListingCrawlerTimeOutManager) {
		this.ticketListingCrawlerTimeOutManager = ticketListingCrawlerTimeOutManager;
	}
	
	
}
