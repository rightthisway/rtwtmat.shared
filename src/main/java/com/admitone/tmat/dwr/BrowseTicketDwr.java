package com.admitone.tmat.dwr;

import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;

import com.admitone.tmat.utils.PreferenceManager;

@RemoteProxy
public class BrowseTicketDwr {
	private static PreferenceManager preferenceManager;
	
	public void setPreferenceManager(PreferenceManager preferenceManager) {
		BrowseTicketDwr.preferenceManager = preferenceManager;
	}

	@RemoteMethod
	public String setTicketMinimizeFilter(Boolean expanded) {
if (expanded==true)
{
	String username = ((UserDetails) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal()).getUsername();
	
	String value ="1";
	String name= "browseTicketFilter";
preferenceManager.updatePreference(username,name,value)	;
}
else
{
	String username = ((UserDetails) SecurityContextHolder.getContext()
		.getAuthentication().getPrincipal()).getUsername();
	
	String value ="0";
	String name= "browseTicketFilter";
preferenceManager.updatePreference(username,name,value)	;
}	
		return "OK";
	}

}
