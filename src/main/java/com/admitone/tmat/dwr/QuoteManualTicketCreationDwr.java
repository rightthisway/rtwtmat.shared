package com.admitone.tmat.dwr;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Quote;
import com.admitone.tmat.data.QuoteManualTicket;

@RemoteProxy
public class QuoteManualTicketCreationDwr {
	@RemoteMethod
	public String saveManualQuote(String[] quoteDetails,String[] ticketDetails) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		QuoteManualTicket quoteManualObj = new QuoteManualTicket();
		Event event = new Event();
		Quote quote = new Quote();
		try{
			Integer eventId =Integer.parseInt(quoteDetails[0].trim());
			Date quoteDate = new java.sql.Date(dateFormat.parse(quoteDetails[2].trim()).getTime());
			Integer quoteId =Integer.parseInt(quoteDetails[3].trim());
			
			event = DAORegistry.getEventDAO().get(eventId);
			quote = DAORegistry.getQuoteDAO().get(quoteId);
				
			quoteManualObj.setCreator(quoteDetails[4]);
			quoteManualObj.setEvent(event);
			quoteManualObj.setCustomer(quote.getCustomer());
			quoteManualObj.setQuote(quote);
			quoteManualObj.setDate(quoteDate);
			quoteManualObj.setQty(Integer.parseInt(ticketDetails[0].trim()));
			quoteManualObj.setSection(ticketDetails[1]);
			quoteManualObj.setRow(ticketDetails[2]);
			quoteManualObj.setPrice(Double.valueOf(ticketDetails[3]));
			quoteManualObj.setMarkedPrice(Double.valueOf(ticketDetails[4]));
			
			if(null != ticketDetails[5] && ticketDetails[5].trim().length() >0){
				quoteManualObj.setId(Integer.parseInt(ticketDetails[5].trim()));
			}
			
			DAORegistry.getQuoteManualTicketDAO().saveOrUpdate(quoteManualObj);
			
			return quoteManualObj.getId().toString();
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	@RemoteMethod
	public String removeManualQuote(String manualQuoteId) {
		try{		
			
			DAORegistry.getQuoteManualTicketDAO().deleteById(Integer.valueOf(manualQuoteId.trim()));
			
			return "OK";
		
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
}
