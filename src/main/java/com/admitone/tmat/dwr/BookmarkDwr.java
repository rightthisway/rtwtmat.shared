package com.admitone.tmat.dwr;

import java.util.List;

import org.acegisecurity.context.SecurityContextHolder;
import org.acegisecurity.userdetails.UserDetails;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Bookmark;
import com.admitone.tmat.enums.BookmarkType;

@RemoteProxy
public class BookmarkDwr {
	@RemoteMethod
	public String toggleBookmarkArtist(Integer artistId) {
		return toggleBookmark(BookmarkType.ARTIST, artistId);
	}

	@RemoteMethod
	public String toggleBookmarkTour(Integer tourId) {
		return toggleBookmark(BookmarkType.TOUR, tourId);
	}

	@RemoteMethod
	public String toggleBookmarkEvent(Integer eventId) {
		return toggleBookmark(BookmarkType.EVENT, eventId);
	}

	@RemoteMethod
	public String toggleBookmarkTicket(Integer ticketId) {
		return toggleBookmark(BookmarkType.TICKET, ticketId);
	}
	
	@RemoteMethod
	public String toggleBookmarkVenue(Integer venueId) {
		return toggleBookmark(BookmarkType.VENUE, venueId);
	}

	@RemoteMethod
	public String toggleOutlierEvent(Integer eventId){
		return toggleOutlier(BookmarkType.EVENT, eventId);
	}
	
	//private String toggleBookmark(BookmarkType type, String objectId) {
	private String toggleBookmark(BookmarkType type, Integer objectId) {
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();

//		if (objectId == null) {
//			return "Off";
//		}
		Bookmark bookmark = new Bookmark();
		bookmark.setObjectId(objectId);
		bookmark.setUsername(username);
		bookmark.setType(type);

		Bookmark b = DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId);
		if (b != null) {
			if(b.getOutlier() != null && b.getOutlier().equals(1)){
				if(b.getType().equals(BookmarkType.EVENT)){
//					updateCrawlsByEvent(objectId); // Disabled this code as per testing purpose.. please talk with chirag before enabling this code.
					/*List<TicketListingCrawl> crawlers = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(objectId);
					if(!crawlers.isEmpty()){
						for(TicketListingCrawl crawl : crawlers){
							crawl.setAutomaticCrawlFrequency(false);
						}
						 we are not maintaining crawler auto frequency history  making it true from here if someone add it to outlier
						DAORegistry.getTicketListingCrawlDAO().updateAll(crawlers);
					}*/
				}
			}
			DAORegistry.getBookmarkDAO().delete(bookmark);
			return "Off";
		} else {
			DAORegistry.getBookmarkDAO().save(bookmark);			
			return "On";
		}
	}

	//private String unbookmark(BookmarkType type, String objectId) {
		private String unbookmark(BookmarkType type, Integer objectId) {
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();
		Bookmark bookmark = new Bookmark();
		bookmark.setObjectId(objectId);
		bookmark.setUsername(username);
		bookmark.setType(type);

		Bookmark b = DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId);
		if (b != null) {
			if(b.getOutlier() != null && b.getOutlier().equals(1)){
				if(b.getType().equals(BookmarkType.EVENT)){
//					updateCrawlsByEvent(objectId); // Disabled this code as per testing purpose.. please talk with chirag before enabling this code.
					/*List<TicketListingCrawl> crawlers = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(objectId);
					if(!crawlers.isEmpty()){
						for(TicketListingCrawl crawl : crawlers){
							crawl.setAutomaticCrawlFrequency(false);
						}
						 we are not maintaining crawler auto frequency history  making it true from here if someone add it to outlier
						DAORegistry.getTicketListingCrawlDAO().updateAll(crawlers);
					}*/
				}
			}
			
			DAORegistry.getBookmarkDAO().delete(bookmark);
		}	
		
		return "Off";
	}
	
	//private String bookmark(BookmarkType type, String objectId) {
		private String bookmark(BookmarkType type, Integer objectId) {
		String username = ((UserDetails) SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal()).getUsername();
		Bookmark bookmark = new Bookmark();
		bookmark.setObjectId(objectId);
		bookmark.setUsername(username);
		bookmark.setType(type);

		try{
			if(DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId) == null) {
				DAORegistry.getBookmarkDAO().save(bookmark);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return "On";
	}
	
		
		private String toggleOutlier(BookmarkType type, Integer objectId) {
			String username = ((UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal()).getUsername();
	
			try{
				Bookmark bookmark = new Bookmark();
				bookmark.setObjectId(objectId);
				bookmark.setUsername(username);
				bookmark.setType(type);
				
	
				Bookmark b = DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId);
				if (b != null) {
					if(b.getOutlier() != null && b.getOutlier().equals(1)){
						if(b.getType().equals(BookmarkType.EVENT)){
//							updateCrawlsByEvent(objectId); // Disabled this code as per testing purpose.. please talk with chirag before enabling this code.
							/*List<TicketListingCrawl> crawlers = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(objectId);
							if(!crawlers.isEmpty()){
								for(TicketListingCrawl crawl : crawlers){
									crawl.setAutomaticCrawlFrequency(false);
								}
								 we are not maintaining crawler auto frequency history  making it true from here if someone add it to outlier
								DAORegistry.getTicketListingCrawlDAO().updateAll(crawlers);
							}*/
							bookmark.setOutlier(null);
							DAORegistry.getBookmarkDAO().update(bookmark);
							return "Off";
						}
						
					}else{
						List<TicketListingCrawl> crawlers = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(objectId);
						if(!crawlers.isEmpty()){
							for(TicketListingCrawl crawl : crawlers){
								crawl.setAutomaticCrawlFrequency(true);
							}
							/* we are not maintaining crawler auto frequency history  making it true from here if someone add it to outlier*/
							DAORegistry.getTicketListingCrawlDAO().updateAll(crawlers);
						}
						bookmark.setOutlier(1);
						DAORegistry.getBookmarkDAO().update(bookmark);
						return "On";
					}
					
					
				} 
				return "Off";
				
			}catch(Exception e){
				e.printStackTrace();
				return "Off";
			}
		}
		
		
		private String makeOutlier(BookmarkType type, Integer objectId){
			String username = ((UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal()).getUsername();
			Bookmark bookmark = new Bookmark();
			bookmark.setObjectId(objectId);
			bookmark.setUsername(username);
			bookmark.setType(type);
			bookmark.setOutlier(1);
			
			try{
				Bookmark b = DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId);
				if (b != null) {
					if(b.getOutlier() == null || "".equals(b.getOutlier())){
						if(b.getType().equals(BookmarkType.EVENT)){
//							updateCrawlsByEvent(objectId); // Disabled this code as per testing purpose.. please talk with chirag before enabling this code.
							/*List<TicketListingCrawl> crawlers = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(objectId);
							if(!crawlers.isEmpty()){
								for(TicketListingCrawl crawl : crawlers){
									crawl.setAutomaticCrawlFrequency(false);
								}
								 we are not maintaining crawler auto frequency history  making it true from here if someone add it to outlier
								DAORegistry.getTicketListingCrawlDAO().updateAll(crawlers);
							}*/
							DAORegistry.getBookmarkDAO().update(bookmark);
						}
					}
					
				}else{
					DAORegistry.getBookmarkDAO().save(bookmark);
				}
					
			}
			catch(Exception e) {
				e.printStackTrace();
			}

			return "On";
		}
		
		private String removeOutlier(BookmarkType type, Integer objectId) {
			String username = ((UserDetails) SecurityContextHolder.getContext()
					.getAuthentication().getPrincipal()).getUsername();
			Bookmark bookmark = new Bookmark();
			bookmark.setObjectId(objectId);
			bookmark.setUsername(username);
			bookmark.setType(type);
			bookmark.setOutlier(null);
			
			
				Bookmark b = DAORegistry.getBookmarkDAO().getBookmark(username, type, objectId);
				if (b != null) {
					if(b.getOutlier() != null && b.getOutlier().equals(1)){
						if(b.getType().equals(BookmarkType.EVENT)){
//							updateCrawlsByEvent(objectId); // Disabled this code as per testing purpose.. please talk with chirag before enabling this code.
							/*List<TicketListingCrawl> crawlers = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(objectId);
							if(!crawlers.isEmpty()){
								for(TicketListingCrawl crawl : crawlers){
									crawl.setAutomaticCrawlFrequency(false);
								}
								 we are not maintaining crawler auto frequency history  making it true from here if someone add it to outlier
								DAORegistry.getTicketListingCrawlDAO().updateAll(crawlers);
							}*/
							DAORegistry.getBookmarkDAO().update(bookmark);
					}
				}
					
			}
			return "Off";
		}
		
		public void updateCrawlsByEvent(Integer eventId){
			// Disabled this code as per testing purpose.. please talk with chirag before enabling this code.
			/*List<TicketListingCrawl> crawlers = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(eventId);
			if(!crawlers.isEmpty()){
				for(TicketListingCrawl crawl : crawlers){
					crawl.setAutomaticCrawlFrequency(false);
				}
				 we are not maintaining crawler auto frequency history  making it true from here if someone add it to outlier
				DAORegistry.getTicketListingCrawlDAO().updateAll(crawlers);
			}*/
		}
}
