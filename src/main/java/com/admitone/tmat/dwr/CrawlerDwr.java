package com.admitone.tmat.dwr;

import java.util.Collection;
import java.util.Date;

import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.TicketPersistenceTracker;

@RemoteProxy
public class CrawlerDwr {
	private Logger logger = LoggerFactory.getLogger(CrawlerDwr.class);
	
	@RemoteMethod
	public Date getLeastRecentlyCrawledDate(Integer eventId) {
		return SpringUtil.getTicketListingCrawler().getLeastCrawledDateForEvent(eventId);
	}
	
	@RemoteMethod
	public Date getLeastRecentlyCrawledDateForTour(String eventIds) {
		Date finalCrawledDateForTour = null;
		String[] eventIdList = eventIds.split(",");
		finalCrawledDateForTour  =  SpringUtil.getTicketListingCrawler().getLeastCrawledDateForEvent(Integer.parseInt(eventIdList[0]));
		
		for(int i = 0; i < eventIdList.length; i++){
			Date currentDate  =  SpringUtil.getTicketListingCrawler().getLeastCrawledDateForEvent(Integer.parseInt(eventIdList[i]));
			if(finalCrawledDateForTour == null){
				finalCrawledDateForTour = currentDate;
			}
			if(currentDate != null){
				if(!(finalCrawledDateForTour.compareTo(currentDate)>0)){
	        		finalCrawledDateForTour = currentDate;
				}
			}
		}
		return finalCrawledDateForTour;
	}
	
	
	@RemoteMethod
	public String forceRecrawlForEvent(Integer eventId) {
		try {
			TicketListingCrawler ticketListingCrawler = SpringUtil.getTicketListingCrawler();
			Integer result = ticketListingCrawler.forceRecrawlForEvent(eventId,TicketListingCrawl.PRIORITY_HIGH);
			if (result == 0) {
				// check if maybe some crawls are running
				Integer count = ticketListingCrawler.getActiveTicketListingCrawlsByEventId(eventId).size();
				Integer runningCount = ticketListingCrawler.getCrawlsExecutedForEventCount(eventId);
				if (runningCount < count) {
					return "OK|" + count + "|" + runningCount + "|" + getLeastRecentlyCrawledDate(eventId).getTime();				
				}
				
				return "ERROR|" + ticketListingCrawler.getMinCrawlPeriod();
			} else {
				Date leastDate = getLeastRecentlyCrawledDate(eventId);
				Long timeInMillis = -1L;
				if (leastDate != null) {
					timeInMillis = leastDate.getTime();
				}
				return "OK|" + result + "|0" + "|" + timeInMillis;
			}
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Error during forced recrawl", e);
			return "ERROR|0";
		}
	}
	
	@RemoteMethod
	public String getCrawlsExecutedForEventCount(Integer eventId) {
		Collection<TicketListingCrawl> crawlerList = SpringUtil.getTicketListingCrawler().getActiveTicketListingCrawlsByEventId(eventId);
		Integer crawlsPersisted = 0;
		Integer crawlsException = 0;
		for(TicketListingCrawl tlc : crawlerList){
//			String status = TicketPersistenceTracker.getInstance().getCrawlPesistenceStatus(tlc.getId());
			CrawlState status = (CrawlState) TicketPersistenceTracker.getInstance().getTicketCrawlStatus(tlc.getId());
			if(status != null && status.equals(CrawlState.PERSISTED)){
				crawlsPersisted++;
			}else if(status != null && status.equals(CrawlState.EXCEPTION)){
				crawlsException++;
			}
		}
		try{
			/*
			 * The idea is to consider persistence as separate crawls. 
			 * (so if you have 2 crawls then there will be 2 persistence threads to persist the tickets from the crawl. 
			 * So you need to make a user wait for the threads to complete the persistence before the auto reload happens).
			 * You could achieve this by Subtracting 2x total crawls with sum of successful crawls and persisted threads.
			 * 
			 * 			 
			*/
			  return crawlsPersisted +":"+ crawlsException; /*Change by Chirag..*/
			//return SpringUtil.getTicketListingCrawler().getCrawlsExecutedForEventCount(eventId)+ crawlsPersisted;
		}catch(Exception e){
			logger.error("Error in getCrawlsExecutedForEventCount", e);
			return SpringUtil.getTicketListingCrawler().getCrawlsExecutedForEventCount(eventId)+":0";
		}
	}
	/**
	 * 
	 * @param eventIds
	 * @return
	 * Still need to work on it..
	 */
	
	@RemoteMethod
	public String getCrawlsExecutedForEventsCount(String eventIds) {
		if(eventIds!=null && eventIds.isEmpty()){
			return ":";
		}
		String[] eventIdList = eventIds.split(",");
		Integer crawlsPersisted = 0;
		
		for(int i = 0; i < eventIdList.length; i++){
			String eventId =eventIdList[i];
			Collection<TicketListingCrawl> crawlerList = SpringUtil.getTicketListingCrawler().getActiveTicketListingCrawlsByEventId(Integer.parseInt(eventId));
			int count=0;
			for(TicketListingCrawl tlc : crawlerList){
				
				CrawlState status = TicketPersistenceTracker.getInstance().getTicketCrawlStatus(tlc.getId());
				if(status != null && (status.equals(CrawlState.PERSISTED) || status.equals(CrawlState.EXCEPTION)) ){
					crawlsPersisted++;
					count++;
				}else{
					
				}
			}
			if(count==crawlerList.size()){
				eventIds=eventIds.replaceAll("/s++", "").replaceAll(eventId, "").replaceAll(",,", ",");
			}
		}
		return crawlsPersisted + ":" + (eventIds.trim().equals(",")?"":eventIds.trim());
		/*try{
			
			 * The idea is to consider persistence as separate crawls. 
			 * (so if you have 2 crawls then there will be 2 persistence threads to persist the tickets from the crawl. 
			 * So you need to make a user wait for the threads to complete the persistence before the auto reload happens).
			 * You could achieve this by Subtracting 2x total crawls with sum of successful crawls and persisted threads.
			 * 
			 * 			 
			
			  return crawlsPersisted +""; Change by Chirag..
			//return SpringUtil.getTicketListingCrawler().getCrawlsExecutedForEventCount(eventId)+ crawlsPersisted;
		}catch(Exception e){
			logger.error("Error in getCrawlsExecutedForEventCount", e);
			int count = 0;
			for(int i = 0; i < eventIdList.length; i++){
				count +=SpringUtil.getTicketListingCrawler().getCrawlsExecutedForEventCount(Integer.parseInt(eventIdList[i]));
			}
			
			return count;
		}*/
	}
	@RemoteMethod
	public String forceRecrawlForEvents(String eventIds) {
		try {
			TicketListingCrawler ticketListingCrawler = SpringUtil.getTicketListingCrawler();
			String[] eventIdList = eventIds.split(",");
			Integer totalCount = 0;
			Integer totalRunningCount = 0;
			Integer totalResult = 0;
//			String val="";
			for(int i = 0; i < eventIdList.length; i++){
				Integer result = ticketListingCrawler.forceRecrawlForEvent(Integer.parseInt(eventIdList[i]),TicketListingCrawl.PRIORITY_LOW);
				totalResult+= result;
				if (result < 0) {
					// check if maybe some crawls are running
					Integer	count = ticketListingCrawler.getActiveTicketListingCrawlsByEventId(Integer.parseInt(eventIdList[i])).size();
					Integer runningCount = ticketListingCrawler.getCrawlsExecutedForEventCount(Integer.parseInt(eventIdList[i]));
					
					totalCount += count;
					totalRunningCount += runningCount;
				}/* else {
					Date leastDate = getLeastRecentlyCrawledDate(Integer.parseInt(eventIdList[i]));
					Long timeInMillis = -1L;
					if (leastDate != null) {
						timeInMillis = leastDate.getTime();
					}
					return "OK|" + result + "|0" + "|" + timeInMillis;
				}*/
		}
		if (totalResult < 0) {
			if (totalRunningCount < totalCount) {
				return "OK|" + totalCount + "|" + totalRunningCount + "|" + getLeastRecentlyCrawledDateForTour(eventIds).getTime();				
			}
			return "ERROR|" + ticketListingCrawler.getMinCrawlPeriod();
		} else {
			Date leastDate = getLeastRecentlyCrawledDateForTour(eventIds);
			Long timeInMillis = -1L;
			if (leastDate != null) {
				timeInMillis = leastDate.getTime();
			}
			return "OK|" + totalResult + "|0" + "|" + timeInMillis;
		}
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Error during forced recrawl", e);
			return "ERROR|0";
		}
	}
	
	public void rtwOpenOrderforceCrawlerByEvent(String eventIds) {
		try {
			TicketListingCrawler ticketListingCrawler = SpringUtil.getTicketListingCrawler();
			String[] eventIdList = eventIds.split(",");
			int totalEvents = eventIdList.length;
			
			for(int i = 0; i < eventIdList.length; i++){
				if( null == eventIdList[i] || eventIdList[i].isEmpty() ){
					continue;
				}
				Integer result = ticketListingCrawler.forceRecrawlForEvent(Integer.parseInt(eventIdList[i]),TicketListingCrawl.PRIORITY_LOW);
				System.out.println("Total Events :"+totalEvents+", Completed Events :"+i+", Event ID :"+eventIdList[i]+", Crawl Count :"+result);
		    }
		
		} catch(Exception e) {
			e.printStackTrace();
			logger.error("Error during rtw open order events forced recrawl", e);
		}
	}
	
}
