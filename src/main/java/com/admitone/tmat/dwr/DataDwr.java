package com.admitone.tmat.dwr;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.acegisecurity.context.SecurityContextHolder;
import org.apache.velocity.app.VelocityEngine;
import org.directwebremoting.annotations.RemoteMethod;
import org.directwebremoting.annotations.RemoteProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.ArtistPriceAdjustment;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.EventSynonym;
import com.admitone.tmat.data.ExcludeTicketMap;
import com.admitone.tmat.data.LongTransaction;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Quote;
import com.admitone.tmat.data.QuoteCustomer;
import com.admitone.tmat.data.ShortTransaction;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.TMATDependentArtistMerge;
import com.admitone.tmat.data.TMATDependentEvent;
import com.admitone.tmat.data.TMATDependentVenueMerge;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.TicketQuote;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.EiMarketPlaceEventPopulator;
import com.admitone.tmat.utils.SeatWaveEventPopulator;
import com.admitone.tmat.utils.SectionRowStripper;
import com.admitone.tmat.utils.SpringUtil;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.eimarketplace.EIMPUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.jira.JiraClient;
import com.admitone.tmat.utils.redmine.WebRedmineClient;
import com.admitone.tmat.web.Constants;

@RemoteProxy
public class DataDwr {
//	private static EventListingManager eventListingManager;
	private static VelocityEngine velocityEngine;
	private static TicketListingCrawler ticketListingCrawler;
	private static JiraClient jiraClient;
	private static WebRedmineClient redmineClient;
	private final Logger logger = LoggerFactory.getLogger(DataDwr.class);

	@RemoteMethod
	public Integer getNumEventsByEventId(Integer artistId) {
		return DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId).size();
	}
	/*public Integer getNumTours(Integer artistId) {
		return DAORegistry.getTourDAO().getAllToursByArtist(artistId).size();
	}*/
	
	@RemoteMethod
	public synchronized String quickCreateEvent(Integer artistId, String artistName, 
			Integer tourId, String tourName, int tourType,int newTourGrandChildType ,
			Integer venueId, String building, String city, String state, String country,
			Integer eventId, String eventName, String eventDateString, String eventTimeString, String timezone,
			Boolean ignoreWarning) {
		
		try {
			// checking parameters
			String warningMessage = "";
	
			//
			// check artist
			//
			if(artistId == null || artistId == 0) {
				// we have to check if artistName is not null
				if (artistName == null) {
					return "ERR|Artist name is empty";				
				}
				// check if there is an artist with the same name
				Artist dbArtist = DAORegistry.getArtistDAO().getArtistByName(artistName);
				if (dbArtist != null) {
					return "ERR|Artist name already exists in the database";								
				}
				// check if there is an artist with a close name
				Collection<Artist> dbArtists = DAORegistry.getArtistDAO().filterByName(artistName);
				if (dbArtists != null && !dbArtists.isEmpty()) {
					warningMessage += "\nThere are artists with a similar name: ";
					for(Artist artist : dbArtists){
						warningMessage += "[" + artist.getName() + "] ";
					}
				}
			} else {
				// check that the artist id exists
				if (DAORegistry.getArtistDAO().get(artistId) == null) {
					return "ERR|Artist with id " + artistId + " does not exist";
				}
			}
			
			//
			// check tour
			//
			/*if(tourId == null || tourId == 0) {
				// we have to check if tourName is not null
				if (tourName == null) {
					return "ERR|Tour name is empty";				
				}
				// check if there is an tour with the same name
				Tour dbTour = DAORegistry.getTourDAO().getTourByName(tourName);
				if (dbTour != null) {
					return "ERR|Tour name already exists in the database";								
				}
				// check if there is an tour with a close name
				Collection<Tour> dbTours = DAORegistry.getTourDAO().filterByName(tourName);
				if (dbTours != null && !dbTours.isEmpty()) {
					warningMessage += "\nThere are tours with a similar name: ";
					for(Tour tour : dbTours){
						warningMessage += "[" + tour.getName() + "] ";
					}
				}
			} else {
				// check that the tour id exists
				if (DAORegistry.getTourDAO().get(tourId) == null) {
					return "ERR|Tour with id " + tourId + " does not exist";
				}
			}*/
	
			//
			// check venue
			//
			if(venueId == null || venueId == 0) {
				// we have to check if venueName is not null
				if (building == null || city == null || country == null) {
					return "ERR|Venue building, city or country is empty";				
				}
				// check if there is an venue with the same name
				Venue dbVenue = DAORegistry.getVenueDAO().getVenue(building);
				if (dbVenue != null) {
					return "ERR|Venue name already exists in the database";								
				}
			} else {
				// check that the venue id exists
				if (DAORegistry.getVenueDAO().get(venueId) == null) {
					return "ERR|Venue with id " + venueId + " does not exist";
				}
			}
			
			// check event
			if (eventId != null && eventId != 0) {
				Event dbEvent = DAORegistry.getEventDAO().get(eventId);
				if (dbEvent != null) {
					return "ERR|Event with id " + eventId + " already exists in the database";					
				}
			}

			if (eventName == null || eventName.isEmpty()) {
				return "ERR|Event name is empty";
			}

			// check event date and time
			Date eventDate = null;
			Time eventTime = null;

			
			if(eventDateString != null){
				try{
					eventDate = parseEventDate(eventDateString);
				}catch (ParseException e) {
					return "ERR|Couldn't parse date or time";
				}
			}
			if(eventTimeString != null){
				try{
					eventTime = parseEventTime(eventTimeString);
				}catch (ParseException e) {
					return "ERR|Couldn't parse date or time";
				}
			}
			
			Collection<Event> dbEvents = null;
			if(eventDate == null & eventTime == null){
				Event tempevent = DAORegistry.getEventDAO().getEventWithSameTourAndVenueAndNameAndDateTBDAndTimeTBD(tourId, venueId, eventName);
				if(tempevent !=  null){
					return "ERR|An event with the same name, date and a conflicting time(" + tempevent.getFormattedEventDate() + ") already exists";	
				}
			}else{
				dbEvents = DAORegistry.getEventDAO().getAllEventsByNameAndDate(eventName, eventDate);
			}
			if(dbEvents != null){
			for(Event event: dbEvents) {
				
					if ((eventDate == null && event.getDate() == null) || (eventDate != null && event.getDate().equals(eventDate))) {
						// if the event is TBD,then we cannot have an existing event on the same date
						// if an existing event with TBD time exists, then ERROR
						if (eventTime == null && event.getTime() == null) { 
							return "ERR|An event with the same name, date and a conflicting time(" + event.getFormattedEventDate() + ") already exists";
						}
						if(event.getTime()!= null && eventTime != null){
						if (event.getTime().equals(eventTime) && event.getTimeZoneId() != null && event.getTimeZoneId().equals(timezone)) {
							return "ERR|An event with the same name, date and time already exists";						
						}
						}else if(event.getTime()== null && eventTime == null){
							return "ERR|An event with the same name, date and time already exists";
						}
						
						if (!event.getVenueId().equals(venueId)) {
							return "ERR|This event cannot happen on the same date than another event with the same name but at a different venue (" + event.getFormattedVenueDescription() + ")";
						}
					}
				
			}

		}
/*	
			Collection<Event> dbEvents = DAORegistry.getEventDAO().getAllEventsByNameExcerpt(eventName, EventStatus.ACTIVE);
			if(dbEvents != null && !dbEvents.isEmpty()){
				warningMessage += "\nThere are events with a similar name: ";
				for(Event event: dbEvents){
					warningMessage += "[" + event.getName() + "] ";
				}
			}
	*/
			
			if (!warningMessage.isEmpty() && !ignoreWarning) {
				return "WARN|" + warningMessage;
			}
	
			//
			// now everything is ok, create artist, venue, tour, event
			//
	
			Artist artist = null;
			Venue venue = null;
//			Tour tour = null;

			if(artistId == null || artistId == 0) {
				artist = new Artist();
				artist.setName(artistName);				
				DAORegistry.getArtistDAO().save(artist);
			} else {
				artist = DAORegistry.getArtistDAO().get(artistId);
			}
			
			/*if(tourId == null || tourId == 0) {
				tour = new Tour();
				tour.setName(tourName);
				if(tourType == 0){
					tour.setTourType(TourType.SPORT);
				} else if(tourType == 1) {
					tour.setTourType(TourType.THEATER);
				} else if(tourType == 2) {
					tour.setTourType(TourType.CONCERT);
				} else if(tourType == 3) {
					tour.setTourType(TourType.OTHER);
				}	
				GrandChildTourCategory grandChildTourCategory = DAORegistry.getGrandChildTourCategoryDAO().get(Integer.valueOf(newTourGrandChildType));						
				tour.setGrandChildTourCategory(grandChildTourCategory);
				tour.setArtist(artist);
				DAORegistry.getTourDAO().save(tour);
			} else {
				tour = DAORegistry.getTourDAO().get(tourId);
			}*/
			
			if(venueId == null || venueId == 0) {
				venue = new Venue();
				venue.setBuilding(building);
				venue.setCity(city);
				venue.setState(state);
				venue.setCountry(country);				
				DAORegistry.getVenueDAO().save(venue);
			} else {
				venue = DAORegistry.getVenueDAO().get(venueId);
			}
			
			Event newEvent = new Event();
			if(eventId == null || eventId == 0){
				newEvent.setId(new Integer(DAORegistry.getEventDAO().getMaxEventId() + 1));
			} else {
				newEvent.setId(eventId);
			}
			newEvent.setName(TextUtil.removeExtraWhitespaces(eventName));
			if(eventDate == null){
				newEvent.setLocalDate(null);	
			}else{
				newEvent.setLocalDate(new java.sql.Date(eventDate.getTime()));	
			}
			
			newEvent.setLocalTime(eventTime);
			newEvent.setTimeZoneId(timezone);			
			newEvent.setEventStatus(EventStatus.ACTIVE);			
			newEvent.setArtistId(artist.getId());
			newEvent.setArtist(artist);
			newEvent.setVenueId(venue.getId());
			logger.info(new Date() + ":" + newEvent.getId() +":" + artist.getId() + ": from quickCreateEvent in DATA DWR");
//			System.out.println(new Date() + ":" + newEvent.getId() +":" + tour.getId() + ": from quickCreateEvent in DATA DWR");
			DAORegistry.getEventDAO().save(newEvent);
			// 
			
			Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
			if(property!=null){
				String dependents = property.getValue();
				if(dependents!= null && !dependents.isEmpty()){
					String dependentList[] = dependents.split(",");
					List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
					for(String dependent:dependentList){
						if(dependent == null || dependent.isEmpty()){
							continue;
						}
						TMATDependentEvent dependentEvent = new TMATDependentEvent();
						dependentEvent.setEventId(newEvent.getId());
						dependentEvent.setDependent(dependent);
						dependentEvent.setAdd(true);
						tmatDependents.add(dependentEvent);
					}
					if(!tmatDependents.isEmpty()){
						DAORegistry.getTmatDependentEventDAO().saveAll(tmatDependents);
					}
				}
			}
			
			// copy category mappings from existing event
			/*Event sameVenueEvent = DAORegistry.getEventDAO().getEventWithSameTourAndVenueButDifferentFromEvent(tour.getId(), venue.getId(), newEvent.getId());
			if (sameVenueEvent != null) {
				DAORegistry.getCategoryMappingDAO().copyCategoryMappings(sameVenueEvent.getId(), newEvent.getId());
			}*/
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERR|" + e.getMessage();
		}
	}
	private Date parseEventDate(String date) throws ParseException {
		DateFormat formatDate = new SimpleDateFormat("M/d/y");		
		return formatDate.parse(date);
	}
	
	private Time parseEventTime(String time) throws ParseException {
		DateFormat format12Time = new SimpleDateFormat("hh:mm aa");
		DateFormat format24Time = new SimpleDateFormat("HH:mm");
		if (time == null || time.isEmpty() || time.equalsIgnoreCase("tbd")) {
			return null;
		} else if (time.toLowerCase().charAt(time.length() - 1) == 'm') {
			return new Time(format12Time.parse(time).getTime());
		} else {
			return new Time(format24Time.parse(time).getTime());
		}		
	}

	@RemoteMethod
	public Integer getNumEvents(Integer artistId) {
		return DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId).size();
	}
	
	@RemoteMethod
	public Integer getNumEventsInVenue(Integer venueId) {
		return DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(venueId, EventStatus.ACTIVE).size();		
	}

	@RemoteMethod
	public Integer getNumTickets(Integer eventId) {
		return DAORegistry.getTicketDAO().getAllTicketsByEvent(eventId).size();
	}

	@RemoteMethod
	public synchronized String deleteUnusedTicketListingCrawl() {
		try {
			DAORegistry.getTicketListingCrawlDAO().deleteUnusedTicketListingCrawl();
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}
		return "OK";
	}
	
	@RemoteMethod
	public synchronized String changePriceAdjustment(Integer eventId, String siteId, double percent) {
		try {
		EventPriceAdjustment priceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(eventId, siteId);
		if (priceAdjustment == null) {
			priceAdjustment = new EventPriceAdjustment();
			priceAdjustment.setEventId(eventId);
			priceAdjustment.setSiteId(siteId);
			priceAdjustment.setPercentAdjustment(percent);
			DAORegistry.getEventPriceAdjustmentDAO().save(priceAdjustment);
		} else {
			priceAdjustment.setPercentAdjustment(percent);
			DAORegistry.getEventPriceAdjustmentDAO().update(priceAdjustment);
		}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return "OK";
	}

	@RemoteMethod
	public int getTicketCount(Integer crawlId, Date startDate, Date endDate) {
//		System.out.println("START DATE=" + startDate);
//		System.out.println("END DATE=" + endDate);
		if (startDate == null) {
			return DAORegistry.getTicketDAO().getTicketCountByCrawl(crawlId);
		}
		
		return DAORegistry.getTicketDAO().getTicketCountByCrawl(crawlId, startDate, endDate);
	}

	@RemoteMethod
	public synchronized boolean deleteTickets(Integer crawlId, Date startDate, Date endDate) {		
		if (startDate == null) {
			//We should update ticket status to DISABLED insted of deleting it.
			//DAORegistry.getTicketDAO().deleteTicketsByCrawlId(crawlId);
			DAORegistry.getTicketDAO().disableTickets(crawlId);
		}
		
		//We should update ticket status to DISABLED insted of deleting it.
		//DAORegistry.getTicketDAO().deleteTicketsByCrawlId(crawlId, startDate, endDate);
		DAORegistry.getTicketDAO().disableTicketsByCrawlId(crawlId, startDate, endDate);
		
		return true;
	}

	@RemoteMethod
	public synchronized String deleteEmptyEvents() {
		try {
			DAORegistry.getEventDAO().deleteEmptyEvents();
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}
	}

	/*@RemoteMethod
	public synchronized String deleteEmptyTours() {
		try {
			DAORegistry.getTourDAO().deleteEmptyTours();
		} catch(Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}
		return "OK";
	}*/

	@RemoteMethod
	public synchronized String deleteEmptyArtists() {
		try {
			DAORegistry.getArtistDAO().deleteEmptyArtists();
		} catch(Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}
		return "OK";
	}

	@RemoteMethod
	public String deleteEmptyVenues() {
		/*try {
			DAORegistry.getVenueDAO().deleteEmptyVenues();
		} catch(Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}*/
		return "OK";
	}	

	@RemoteMethod
	public Collection<Map<String, Object>> getEventsByTour(Integer aritstId) {
		Collection<Map<String, Object>> results = new ArrayList<Map<String,Object>>();
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(aritstId);
		
		for (Event event: events) {
			Map<String, Object> m = new HashMap<String, Object>();
			m.put("id", event.getId());
			m.put("name", event.getName());
			m.put("date", event.getFormattedEventDate());
			if (event.getVenue() != null) {
				m.put("venueBuilding", event.getVenue().getBuilding());
				m.put("venueCity", event.getVenue().getCity());
				m.put("venueState", event.getVenue().getState());
			}
			results.add(m);
		}
		return results;
	}
	
	// FIXME: should be in a dwr which handles only user action so we can define
	// security access with acegi
	@RemoteMethod
	public synchronized String deleteUserPreferences() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();		
		try {
			SpringUtil.getPreferenceManager().removePreferenceForUser(username);
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}
	}
	
	@RemoteMethod
	public synchronized String deletePreferencesForUser(String username) {
		try {
			SpringUtil.getPreferenceManager().removePreferenceForUser(username);
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}
	}

	@RemoteMethod
	public synchronized String deletePreferences() {
		try {
			SpringUtil.getPreferenceManager().removePreferences();
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return "ERROR|" + e.getMessage();
		}
	}

	@RemoteMethod
	public String cleanBookmarks() {
		DAORegistry.getBookmarkDAO().cleanBookmarks();
		return "OK";
	}
	
	/*@RemoteMethod
	public String cleanCategoryMappings() {
		DAORegistry.getCategoryMappingDAO().cleanCategoryMappings();
		return "OK";
	}*/

	@RemoteMethod
	public synchronized void updateEvents() {
		try {
			FileInputStream fis = new FileInputStream("/tmp/toto.csv");
			BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
			String line;
			int lineNum = 0;
			DateFormat formatDate = new SimpleDateFormat("M/d/y");
			DateFormat format12Time = new SimpleDateFormat("hh:mm aa");
			DateFormat format24Time = new SimpleDateFormat("HH:mm");
			
			Map<String, Event> eventMap = new HashMap<String, Event>();
			// hash table to contain eventName + eventDate + venue => event
			for (Event event : DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.ACTIVE)) {
				String key;
				if (event.getName().toLowerCase().indexOf("tbd") > 0) {
					key = (event.getName() + event
							.getVenue().getBuilding()).toLowerCase();					
				}
				else if (event.getLocalTime() == null) {
					key = (formatDate.format(event.getDate()) + " TBD " + event
							.getVenue().getBuilding()).toLowerCase();
				} else {
					key = (formatDate.format(event.getDate()) + " " + 
							format24Time.format(event.getLocalTime()) + " " + event
							.getVenue().getBuilding()).toLowerCase();
				}
//				if (eventMap.get(key) != null) {
//					Event existingEvent = eventMap.get(key); 
//				}
				eventMap.put(key, event);										
			}
			
			while((line = reader.readLine()) != null) {
				lineNum++;
				if (lineNum == 1) {
					continue;
				}
				String[] tokens = line.split(",");
				Integer eventId = Integer.valueOf(tokens[0]);
				String eventName = tokens[1].trim();
				String eventDate = tokens[3].trim().split(" ")[0]; // only get the
				// date, ignore the
				// day name
				String time = tokens[4].trim();
				String building = tokens[5].trim();
				if (eventName.charAt(0) == '\"'
					&& eventName.charAt(eventName.length() - 1) == '\"') {
					eventName = eventName.substring(1, eventName.length() - 2);
				}

				Date date = null;
				Time eventTime = null;
				try {
					date = formatDate.parse(eventDate);

					if (time.equalsIgnoreCase("tbd")) {
						eventTime = null;
					} else if (time.toLowerCase().charAt(time.length() - 1) == 'm') {
						eventTime = new Time(format12Time.parse(time).getTime());
					} else {
						eventTime = new Time(format24Time.parse(time).getTime());
					}

				} catch (Exception e) {
					e.printStackTrace();
				}
				
				String key;
				if (eventName.toLowerCase().indexOf("tbd") > 0) {
					key = (eventName + building).toLowerCase();					
				}
				else if (eventTime == null) {
					key = (formatDate.format(date) + " TBD " + building).toLowerCase();
				} else {
					key = (formatDate.format(date) + " " + format24Time.format(eventTime) + " " + building).toLowerCase();						
				}
				
				Event event = eventMap.get(key);
				if (event == null) {
//					System.out.println("-- event " + eventName + " " +  key);
					continue;
				}
				
				String statement = "-- " + event.getName() + " " + formatDate.format(date) + "\n"
					      + "update bookmark set object_id=DEST where type='EVENT' AND object_id=SRC AND username <> '';\n"
				          + "update category_mapping set event_id=DEST where event_id=SRC AND category_id <> 0;\n"
				          + "update event_price_adjustment set event_id=DEST where event_id=SRC;\n"
				          + "update ticket set event_id=DEST where event_id=SRC;\n"
				          + "update short_transaction set event_id=DEST where event_id=SRC;\n"
				          + "update ticket_listing_crawl set event_id=DEST where event_id=SRC;\n"
				          + "update event set id=DEST where id=SRC;\n\n";
				
//				System.out.println(statement.replaceAll("SRC", event.getId() + "").replaceAll("DEST", eventId + ""));
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RemoteMethod
	public Boolean sendCheckListEasy(String login, String password) throws Exception {
		try {
			return SpringUtil.getListEasyManager().login(login, password);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@RemoteMethod
	public String sendTestEmail(String smtpHost, int smtpPort, Boolean smtpAuth, String smtpUsername, String smtpPassword, String smtpSecureConnection, String emailFrom, String emailTo) throws Exception {
		String result = null;
		try {
			Map<String, Object> map = new HashMap<String, Object>();
			SimpleMailMessage mailMessage = new SimpleMailMessage();
			mailMessage.setFrom(emailFrom);
			
	        InetAddress addr = InetAddress.getLocalHost();
			String hostname = addr.getHostName();
			if (hostname == null) {
				hostname = addr.getHostAddress();
			}
		    
			String subject = "[" + hostname + "] TMAT - Email Settings Test";
			map.put("smtpHost", smtpHost);
			map.put("smtpPort", smtpPort);
			map.put("smtpAuth", smtpAuth);
			map.put("smtpUsername", smtpUsername);
			map.put("smtpPassword", smtpPassword.replaceAll(".", "*"));
			map.put("smtpSecureConnection", smtpSecureConnection);
			map.put("emailFrom", emailFrom);

			SpringUtil.getMailManager().sendMailNow(smtpHost, smtpPort, "smtp", smtpSecureConnection, smtpAuth,
					smtpUsername, smtpPassword,
					"TMAT Mail Tester",
					emailFrom,
					emailTo, subject, "mail-test.txt", map, "text/plain");
		
			result = "Email sent";
		} catch (Exception e) {
			e.printStackTrace();
			result = "ERROR: " + e.getMessage();
		}
				
		return result;
	}

	@RemoteMethod
	public String checkEiMarketPlaceCredentials(String username, String password) throws Exception {
		SimpleHttpClient httpClient = null;
		
//		System.out.println("CHECKING CREDENTIALS");
		
		try {
			httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, username, password);
			return EIMPUtil.login(httpClient, username, password)?"OK":"Cannot log in";
		} catch (Exception e) {
			e.printStackTrace();
			return "OK";
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
//			System.out.println("DONE WITH CHECKING CREDENTIALS");
		}
	}

	/*
	@RemoteMethod
	public boolean recomputeTicketDuplicateKeys() throws Exception {
		try {
			for(Event event: DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.ACTIVE)) {
				Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllTicketsByEvent(event.getId());
				if (tickets == null) {
					continue;
				}
				for(Ticket ticket:tickets) {
					String duplicateKey = TicketUtil.getTicketDuplicateKey(ticket, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);
					ticket.setTicketDuplicateKey(duplicateKey);
					DAORegistry.getTicketDAO().update(ticket);				
				}
			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	*/
	// Did it purpose fully .. please let Chirag know if you need to enable this method . Date: 03/04/2014
	/*@RemoteMethod
	public boolean recomputeTicketRemainingQuantities() throws Exception {
		DAORegistry.getTicketDAO().recomputeTicketRemainingQuantities();
		return true;
	}*/

	/*@RemoteMethod
	public boolean recomputeTourEventCounts() throws Exception {
		Collection<Tour> tours = DAORegistry.getTourDAO().getAll();
		for(Tour tour : tours){
			DAORegistry.getTourDAO().update(tour);
		}
		return true;
	}*/
	@RemoteMethod
	public String mergeEvents(Integer mergeId, Integer goodId) {	
//		boolean acquired1 = false; 
//		boolean acquired2 = false; 
		try {
//			Integer artistId = DAORegistry.getEventDAO().get(mergeId).getArtistId();
//			Integer goodArtistId = DAORegistry.getEventDAO().get(goodId).getArtistId();
			try {

				Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllTicketsByEvent(mergeId);

				if (tickets != null && !tickets.isEmpty()) {
					for(Ticket ticket:tickets) {
						ticket.setEventId(goodId);
						DAORegistry.getTicketDAO().update(ticket);				
					}
				}
				
//		    	DAORegistry.getCategoryMappingDAO().deleteAllByEvent(mergeId);
//				Categorizer.update(tourId);
				
				List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(mergeId);
				
				if (crawls != null && !crawls.isEmpty()) {
					for(TicketListingCrawl crawl: crawls) {
						crawl.setEventId(goodId);
//						crawl.setTourId(goodTourId);
						DAORegistry.getTicketListingCrawlDAO().update(crawl);
						/*ticketListingCrawler.updateTicketListingCrawl(crawl);*/
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
				return e.getMessage();
			}
			
			Collection<EventSynonym> syns = DAORegistry.getEventSynonymDAO().getEventSynonymByExisting(mergeId);
			if (syns != null) {
				for(EventSynonym syn:syns) {
					syn.setEventId(goodId);
					DAORegistry.getEventSynonymDAO().update(syn);				
				}
			}
			
			Map<String, EventPriceAdjustment> adjs = DAORegistry.getEventPriceAdjustmentDAO().getPriceAdjustments(mergeId);
			if (adjs != null) {
				for(EventPriceAdjustment adj: adjs.values()) {
					DAORegistry.getEventPriceAdjustmentDAO().delete(adj);				
				}
			}

			EventSynonym eSyn = new EventSynonym();
			eSyn.setEventId(goodId);
			eSyn.setMergedId(mergeId);
			DAORegistry.getEventSynonymDAO().save(eSyn);
			
			DAORegistry.getEventDAO().deleteById(mergeId);
			
		} catch (Exception e) {
			e.printStackTrace();
			return "Error: " + e.getMessage();
		}

		return "OK";
	}

	/*@RemoteMethod
	public synchronized String mergeMultipleTours(String mergeIds, Integer goodId) {
		List<Integer> mergeIdList = new ArrayList<Integer>();
		
		for (String mergeIdStr: mergeIds.split("[, ]")) {
			Integer tourId = Integer.valueOf(mergeIdStr);
			if (tourId.equals(goodId)) {
				continue;
			}
			mergeIdList.add(tourId);
		}
		
		if (mergeIdList.isEmpty()) {
			return "ERROR";
		}
		
		String username = "AUTO";
		try{
			username = SecurityContextHolder.getContext().getAuthentication().getName();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		if (DAORegistry.getTourDAO().mergeTours(mergeIdList, goodId,username)) {
			
			Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
			for(Integer mergeId:mergeIdList){
				List<TMATDependentTourMerge> list = DAORegistry.getTmatDependentTourMergeDAO().getTMATDependentTourMergeByTourIdAndMergeTourId(mergeId, goodId);
				
				if(property!=null){
					String dependents = property.getValue();
					if(dependents!= null && !dependents.isEmpty()){
						if(list!=null){
							for(TMATDependentTourMerge dependent:list){
								dependents.replaceAll(dependent.getDependent(), "");
							}
						}
						for(String temp:dependents.split(",")){
							if(temp!=null && !temp.isEmpty()){
								TMATDependentTourMerge merge = new TMATDependentTourMerge();
								merge.setDependent(temp);
								merge.setToMergedTourId(goodId);
								merge.setTourId(mergeId);
								DAORegistry.getTmatDependentTourMergeDAO().save(merge);
							}
						}
					}
				
				}
	
			}
			return "OK";
		}
		
		return "Error"; 		
	}*/

	/*@RemoteMethod
	public synchronized String mergeTours(Integer mergeId, Integer goodId) {
		String username = "AUTO";
		try{
			username = SecurityContextHolder.getContext().getAuthentication().getName();
		}catch(Exception e){
			e.printStackTrace();
		}
		if (DAORegistry.getTourDAO().mergeTours(mergeId, goodId,username)) {
			return "OK";
		}
		
		return "Error"; 		
	}*/

	@RemoteMethod
	public String mergeArtists(Integer mergeId, Integer goodId) {	
		try {
			Artist mergeArtist = DAORegistry.getArtistDAO().get(mergeId);
			Artist goodArtist = DAORegistry.getArtistDAO().get(goodId);

			Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(mergeId);

			if (events != null) {
				for(Event event: events) {

					try {
						event.setArtistId(goodId);
						DAORegistry.getEventDAO().update(event);
					} catch (Exception e) {
						e.printStackTrace();
						return e.getMessage();
					}
				}
			}

			Map<String, ArtistPriceAdjustment> adjs = DAORegistry.getArtistPriceAdjustmentDAO().getPriceAdjustments(mergeArtist.getId());
			if (adjs != null) {
				for(ArtistPriceAdjustment adj: adjs.values()) {
					DAORegistry.getArtistPriceAdjustmentDAO().delete(adj);				
				}
			}

			DAORegistry.getArtistDAO().deleteById(mergeId);
			TMATDependentArtistMerge merge = new TMATDependentArtistMerge();
			merge.setDependent("zones");
			merge.setArtistId(goodId);
			merge.setToMergedArtistId(mergeId);
			DAORegistry.getTmatDependentArtistMergeDAO().save(merge);
			DAORegistry.getArtistDAO().update(goodArtist);
			
		} catch (Exception e) {
			e.printStackTrace();
			return "Error: " + e.getMessage();
		}

		return "OK";
	}
	
	@RemoteMethod
	public String mergeVenues(Integer mergeId, Integer goodId) {	
		try {
			//Venue mergeVenue = DAORegistry.getVenueDAO().get(mergeId);
			//Venue goodVenue = DAORegistry.getVenueDAO().get(goodId);
			
			Collection<Event> events = DAORegistry.getEventDAO().getAllEventsActiveandExpiredByVenue(mergeId);
			
			if (events != null) {
				for(Event event: events) {

					try {
						event.setVenueId(goodId);
						event.setVenueCategoryId(null);
						DAORegistry.getEventDAO().update(event);
					} catch (Exception e) {
						e.printStackTrace();
						return e.getMessage();
					}
				}
			}
			List<TMATDependentVenueMerge> list = DAORegistry.getTmatDependentVenueMergeDAO().getTMATDependentVenueMergeByVenueIdAndMergeVenueId(mergeId, goodId);
			Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
			if(property!=null){
				String dependents = property.getValue();
				if(dependents!= null && !dependents.isEmpty()){
					if(list!=null){
						for(TMATDependentVenueMerge dependent:list){
							dependents.replaceAll(dependent.getDependent(), "");
						}
					}
					for(String temp:dependents.split(",")){
						if(temp!=null && !temp.isEmpty()){
							TMATDependentVenueMerge merge = new TMATDependentVenueMerge();
							merge.setDependent(temp);
							merge.setToMergedVenueId(goodId);
							merge.setVenueId(mergeId);
							DAORegistry.getTmatDependentVenueMergeDAO().save(merge);
						}
					}
				}
			
			}
			
			
			DAORegistry.getVenueDAO().deleteVenue(mergeId);
			
		} catch (Exception e) {
			e.printStackTrace();
			return "Error: " + e.getMessage();
		}

		return "OK";
	}


	/*@RemoteMethod
	public EventListingResult searchEvents(String keywords, String siteId, String location, Date fromDate, Date toDate) {
		System.out.println("START SEARCH EVENT");
		try {
		Calendar fromCalendar = new GregorianCalendar();
		fromCalendar.setTime(fromDate);
		fromCalendar.set(Calendar.HOUR_OF_DAY, 0);
		fromCalendar.set(Calendar.MINUTE, 0);
		fromCalendar.set(Calendar.SECOND, 0);
		fromDate = fromCalendar.getTime();

		Calendar toCalendar = new GregorianCalendar();
		toCalendar.setTime(toDate);
		toCalendar.set(Calendar.HOUR_OF_DAY, 23);
		toCalendar.set(Calendar.MINUTE, 59);
		toCalendar.set(Calendar.SECOND, 59);
		toDate = toCalendar.getTime();
		
		Collection<EventListingResult> eventListingResults = eventListingManager.search(keywords, new String[] {siteId}, location, fromDate, toDate,null,false);
		System.out.println("SIZE=" + eventListingResults.size());
		return eventListingResults.iterator().next();
		
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	*/
	@RemoteMethod
	public String sendErrorFeedback(String errorFeedbackProject, String errorFeedbackSummary, Integer errorFeedbackPriority, String errorFeedbackMessage, String errorStackTrace, String currentUrl, String referrerUrl, String cookies, String userAgent, String clientIpAddress) {
		Property errorFeedbackRecipientsProperty = DAORegistry.getPropertyDAO().get("feedback.error.email.to");
		if (errorFeedbackRecipientsProperty == null || errorFeedbackRecipientsProperty.getValue() == null || errorFeedbackRecipientsProperty.getValue().length() == 0) {
			return "ERR - No recipients defined for this error feedback";
		} 
		
//		System.out.println("USER AGENT=" + userAgent);
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		User user = DAORegistry.getUserDAO().getUserByUsername(username);
				
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("submitter", username);
		map.put("submissionDate", new Date().toString());
		map.put("url", currentUrl);
		map.put("referrerUrl", referrerUrl);
		map.put("errorFeedbackPriority", errorFeedbackPriority);
		map.put("errorFeedbackSummary", errorFeedbackSummary);
		map.put("errorFeedbackMessage", errorFeedbackMessage);
		map.put("errorStackTrace", errorStackTrace);
		map.put("cookies", cookies);
		map.put("preferences", DAORegistry.getPreferenceDAO().getAllPreferences(username));
		map.put("userAgent", userAgent);
		map.put("clientIpAddress", clientIpAddress);
		map.put("headerMessage", "");

		String project;
		if (errorFeedbackProject == null || errorFeedbackProject.isEmpty()) {
			if (currentUrl != null && currentUrl.contains("/Ebay")) {
				project = DAORegistry.getPropertyDAO().get("feedback.redmine.ebay.project").getValue();
			} else {
				project = DAORegistry.getPropertyDAO().get("feedback.redmine.project").getValue();
			}
		} else {
			project = errorFeedbackProject;
		}

		Property feedbackJiraEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.jira.enabled");
		if ("1".equals(feedbackJiraEnabledProperty.getValue())) {

			String title = errorFeedbackSummary;
			
			if (title == null || title.length() == 0) {
				if (errorFeedbackMessage.length() > 80) {
					title = errorFeedbackMessage.substring(0, 80) + "...";
				} else {
					title = errorFeedbackMessage;					
				}
			}
		
			String description = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/jira-error-feedback.txt", map);
			
			try {
				String issueKey = jiraClient.postJiraIssue(project, JiraClient.ISSUE_TYPE_BUG, title, description);			
				map.put("headerMessage", "This issue was posted on JIRA in the issue #" + issueKey);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("headerMessage", "This issue cannot be posted to jira" + e.getMessage());
			}
		}

		Property feedbackRedmineEnabledProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.enabled");
		if ("1".equals(feedbackRedmineEnabledProperty.getValue())) {

			String title = errorFeedbackSummary;
			
			if (title == null || title.length() == 0) {
				if (errorFeedbackMessage.length() > 80) {
					title = errorFeedbackMessage.substring(0, 80) + "...";
				} else {
					title = errorFeedbackMessage;					
				}
			}
		
			String description = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/jira-error-feedback.txt", map);
			
			try {
				String issueKey = redmineClient.postRedmineIssue(project, WebRedmineClient.BUG_TRACKER_ID, title, description, null, errorFeedbackPriority);			
				map.put("headerMessage", map.get("headerMessage") + "\nThis issue was posted on Redmine in the issue #" + issueKey);
			} catch (Exception e) {
				e.printStackTrace();
				map.put("headerMessage", map.get("headerMessage") + "\nThis issue cannot be posted to Redmine:" + e.getMessage());				
			}
		}
		
		
		String[] emails = errorFeedbackRecipientsProperty.getValue().split(",");
		for(String email: emails) {
			SpringUtil.getMailManager().sendMail(user.getFirstName() + " " + user.getLastName() + " (" + username + ")", email.trim(), "[" + Constants.getInstance().getHostName() + "] Error feedback", "mail-error-feedback.txt", map, "text/plain");
		}
		
		return "OK";
	}
		
	@RemoteMethod
	public Map<String, Object> getCrawlException(int crawlId) {
		TicketListingCrawl crawl = ticketListingCrawler.getTicketListingCrawlById(crawlId);
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("crawlName", crawl.getName());
		

		if (crawl.getException() != null) {
			map.put("exceptionClassName", crawl.getException().getClass().getName());
			map.put("exceptionStackTrace", crawl.getException().getMessage() + "\n\n" + crawl.getCrawlExceptionStackTrace());
		}
		return map;
	}
	
	/*
	 * the request format is: [ticket_id]_[scope],[ticket_id]_[scope],...
	 * The logic is as follows for a given ticket:
	 * - if we check scope EVENT: delete all maps for cat and transaction
	 * - if we check scope CAT: delete all maps for events and transactions
	 * - if we check scope TICKET: delete all maps for cat and transaction
	 *  
	 */
	@RemoteMethod
	public String updateExcludeTicketMap(Integer transactionId, Integer transactionType, String catScheme, String request,Integer nextPrice) {
		Integer admitoneId;
		Integer categoryId=null;;
		Event event = null;
		ShortTransaction shortTransaction = DAORegistry.getShortTransactionDAO().get(transactionId);
		admitoneId = shortTransaction.getEventId();
		event = DAORegistry.getEventDAO().getEventByAdmitoneId(admitoneId);
		Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
		Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
		Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
		for(Synonym synonym:synonyms){
			if(synonym.getType().equals("Theater")){
				theaterSynonymMap.put(synonym.getName(), synonym);
			}else{
				concertSynonymMap.put(synonym.getName(), synonym);
			}
		}
		Map<String,Synonym> synonymMap = null;
		if(event.getEventType().equals(TourType.THEATER)){
			synonymMap = theaterSynonymMap;
		}else if(event.getEventType().equals(TourType.CONCERT) || event.getEventType().equals(TourType.SPORT)){
			synonymMap = concertSynonymMap;
		}
		else{
			synonymMap =  new HashMap<String, Synonym>();
		}
		try {
			if (transactionType.equals(ExcludeTicketMap.TYPE_SHORT)) {
				
				if(event.getVenueCategory()!=null){
					Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
					Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
					for (Category category : categories) {
						categoryMap.put(category.getId(), category);
					}
					Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
					Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
					for(CategoryMapping mapping:categoryMappingList){
						List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
						if(list==null){
							list = new ArrayList<CategoryMapping>();
						}
						list.add(mapping);
						catMappingMap.put(mapping.getCategoryId(), list);
					}
					String normalizedSection = ""; 
					normalizedSection= SectionRowStripper.strip(event.getEventType(), shortTransaction.getSection(),synonymMap);
					String normalizedRow = shortTransaction.getRow().replaceAll("\\W", "");
					
					Category category = Categorizer.computeCategory(event.getVenueCategory(), normalizedSection, normalizedRow, catScheme,categoryMap,catMappingMap);
					if(category!=null){
						categoryId=category.getId();
					}
				}
				if(categoryId == null){
					Category category = Categorizer.getCategoryByCatSection(shortTransaction.getSection(), shortTransaction.getRow(), event.getVenueCategory());
					categoryId = (category == null)?0: category.getId();
				}
			} else {
				LongTransaction longTransaction = DAORegistry.getLongTransactionDAO().get(transactionId);
				admitoneId = longTransaction.getAdmitoneId();
				event = DAORegistry.getEventDAO().getEventByAdmitoneId(admitoneId);
				if(event.getVenueCategory()!=null){
					Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
					Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
					for (Category category : categories) {
						categoryMap.put(category.getId(), category);
					}
					Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
					Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
					for(CategoryMapping mapping:categoryMappingList){
						List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
						if(list==null){
							list = new ArrayList<CategoryMapping>();
						}
						list.add(mapping);
						catMappingMap.put(mapping.getCategoryId(), list);
					}
					String normalizedSection = ""; 
//					if(event.getEventType().equals(TourType.THEATER)){
//						normalizedSection= SectionRowStripper.strip(event.getEventType(), longTransaction.getSection(),theaterSynonymMap);
//					}else{
						normalizedSection= SectionRowStripper.strip(event.getEventType(), longTransaction.getSection(),synonymMap);
//					}
					String normalizedRow = longTransaction.getRow().replaceAll("\\W", "");
					Category cat = Categorizer.computeCategory(event.getVenueCategory(), normalizedSection, normalizedRow, catScheme,categoryMap,catMappingMap);
					if(cat!=null){
						categoryId = cat.getId();
					}
				}
				
				
				if(categoryId == null){
					Category category = Categorizer.getCategoryByCatSection(longTransaction.getSection(), longTransaction.getRow(), event.getVenueCategory());
					categoryId = (category == null)?0: category.getId();
				}
			}
	
			Collection<ExcludeTicketMap> toDel = new ArrayList<ExcludeTicketMap>();
			for (ExcludeTicketMap excludeTicketMap: DAORegistry.getExcludeTicketMapDAO().getAllMapsByEventId(event.getId())) {
				if ((excludeTicketMap.getScope().equals(ExcludeTicketMap.SCOPE_EVENT) && excludeTicketMap.getEventId().equals(event.getId()))
					|| (excludeTicketMap.getScope().equals(ExcludeTicketMap.SCOPE_CAT) && excludeTicketMap.getCategoryId().equals(categoryId))
					|| (excludeTicketMap.getScope().equals(ExcludeTicketMap.SCOPE_TX) && excludeTicketMap.getTransactionId().equals(transactionId) && excludeTicketMap.getTransactionType().equals(transactionType))) {
					toDel.add(excludeTicketMap);
				}
			}
			DAORegistry.getExcludeTicketMapDAO().deleteAll(toDel);
			
			Set<Integer> usedTicketIds = new HashSet<Integer>();
			if (request != null && !request.isEmpty()) {
				for (String map: request.split(",")) {
					String[] tokens = map.split("-");
					Integer ticketId = Integer.valueOf(tokens[0]);
					int scope = Integer.valueOf(tokens[1]);
					
					usedTicketIds.add(ticketId);
					
//					System.out.println("TIX: " + ticketId + " ==> " + scope);
					
					switch(scope) {
						case ExcludeTicketMap.SCOPE_EVENT:
							DAORegistry.getExcludeTicketMapDAO().deleteAllCatMapsForTicket(ticketId);
							DAORegistry.getExcludeTicketMapDAO().deleteAllTxMapsForTicket(ticketId);
						 	ExcludeTicketMap excludeTicketMap = new ExcludeTicketMap();
						 	excludeTicketMap.setScope(ExcludeTicketMap.SCOPE_EVENT);
						 	excludeTicketMap.setEventId(event.getId());
						 	excludeTicketMap.setTicketId(ticketId);
						 	excludeTicketMap.setTransactionType(transactionType);
						 	DAORegistry.getExcludeTicketMapDAO().save(excludeTicketMap);
							break;
		
						case ExcludeTicketMap.SCOPE_CAT:
							DAORegistry.getExcludeTicketMapDAO().deleteEventMapForTicket(ticketId);
							DAORegistry.getExcludeTicketMapDAO().deleteAllTxMapsForTicket(ticketId, categoryId);
						 	excludeTicketMap = new ExcludeTicketMap();
						 	excludeTicketMap.setScope(ExcludeTicketMap.SCOPE_CAT);
						 	excludeTicketMap.setEventId(event.getId());
						 	excludeTicketMap.setCategoryId(categoryId);
						 	excludeTicketMap.setTicketId(ticketId);
						 	excludeTicketMap.setTransactionType(transactionType);
						 	DAORegistry.getExcludeTicketMapDAO().save(excludeTicketMap);
							break;
		
						case ExcludeTicketMap.SCOPE_TX:
							DAORegistry.getExcludeTicketMapDAO().deleteAllCatMapsForTicket(ticketId, categoryId);
							DAORegistry.getExcludeTicketMapDAO().deleteEventMapForTicket(ticketId);
						 	excludeTicketMap = new ExcludeTicketMap();
						 	excludeTicketMap.setScope(ExcludeTicketMap.SCOPE_TX);
						 	excludeTicketMap.setEventId(event.getId());
						 	excludeTicketMap.setCategoryId(categoryId);
						 	excludeTicketMap.setTransactionId(transactionId);
						 	excludeTicketMap.setTicketId(ticketId);
						 	excludeTicketMap.setTransactionType(transactionType);
						 	DAORegistry.getExcludeTicketMapDAO().save(excludeTicketMap);
							break;
					}
				}
			}
			String response = transactionId+"-"+nextPrice;
			return response;
		} catch(Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	/*@RemoteMethod
	public void updateInventory() {
		try {
			DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			BufferedReader reader = new BufferedReader(new InputStreamReader(new FileInputStream("/a.csv")));
			String line = null;
			while ((line = reader.readLine()) != null) {
				String[] tokens = line.split(",");
				String eventName = tokens[1];
				String building = tokens[2];
				Date eventDate = format.parse(tokens[3]);
				Integer quantity = Integer.valueOf(tokens[5]);
				String category = tokens[6].replaceAll("CAT ", "");
				Integer ticketId = Integer.valueOf(tokens[12]);
				
				Event event = null;
				for (Event e:DAORegistry.getEventDAO().getAllEventsByNameAndDate(eventName, eventDate)) {
					if (e.getVenue().getBuilding().equals(building)) {
						event = e;
						break;
					}
				}
				
				if (event == null) {
//					System.out.println("==============================================>>>> EVENT IS NULL");
					return;
				}
				
				for (EbayInventoryGroup group: DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByEventCategoryQuantity(event.getId(), category, quantity)) {
					group.setTicketId(ticketId);
//					System.out.println("=====================>setting group to ticketId=" + ticketId);
					DAORegistry.getEbayInventoryGroupDAO().update(group);
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
	}*/

	@RemoteMethod
	public String updateEventAdmitoneId(Integer eventId, String aOneId) {
		String returnString = "";
		Event event = DAORegistry.getEventDAO().get(eventId);
		AdmitoneEvent admitoneEvent = DAORegistry.getAdmitoneEventDAO().get(Integer.parseInt(aOneId));
		if(aOneId != null && !"".equals(aOneId)){
			event.setAdmitoneId(Integer.parseInt(aOneId));
			event.setLocalDate(admitoneEvent.getEventDate());
			event.setLocalTime(admitoneEvent.getEventTime());
			returnString ="The event was linked";
		}else{
			event.setAdmitoneId(null);
			returnString ="The event was unlinked";
		}
		try {
			DAORegistry.getEventDAO().update(event);
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		return returnString;
	}
	
	@RemoteMethod
	public String fetchSeatWaveEvents() {
		SeatWaveEventPopulator.start();
		return "OK";
	}

	@RemoteMethod
	public String fetchEiMarketPlaceEvents() {
		EiMarketPlaceEventPopulator.start();
		return "OK";
	}

	@RemoteMethod
	public synchronized String addTicketsToQuotes(String ticketIds, String customerName, String dateStr, Double percentAddition,boolean isNew ,Integer appentToQuote ,String refferal) {
		try {
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.sql.Date date;
			try {
				date = new java.sql.Date(dateFormat.parse(dateStr).getTime());
			} catch (ParseException e) {
				return "Invalid date!";
			} 
			if(customerName == null || customerName.equals("")){
				return "Customer not be null";
			}
			// check if the customer is already in the DB and create it if not
			QuoteCustomer customer = DAORegistry.getQuoteCustomerDAO().getCustomerByName(customerName);
			if (customer == null) {
				customer = new QuoteCustomer(username, customerName);
				DAORegistry.getQuoteCustomerDAO().save(customer);
			}
	
			// create a new quote if there is no active now
			Quote quote = null;
			if(appentToQuote == null){
			quote = DAORegistry.getQuoteDAO().getOpenQuote(customer.getId(), username, date);
			if (quote == null) {
				quote = new Quote(username, customer, date);
				quote.setReferral(refferal);
				DAORegistry.getQuoteDAO().save(quote);
			}else{
				return "Quote already exist";
			}
			}else{
				quote = DAORegistry.getQuoteDAO().get(appentToQuote);
				quote.setSentDate(null);
				DAORegistry.getQuoteDAO().update(quote);
			}
			// create a map so we can check if the ticket is already there
			// date => quote
			Map<Integer, TicketQuote> ticketQuoteMap = new HashMap<Integer, TicketQuote>();
			for (TicketQuote ticketQuote: DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(quote.getId())) {
				ticketQuoteMap.put(ticketQuote.getTicketId(), ticketQuote);
			}
			
			Collection<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
			for (String ticketIdStr: ticketIds.split(",")) {
				Integer ticketId = Integer.valueOf(ticketIdStr);
				if (ticketQuoteMap.containsKey(ticketId)) {
					continue;
				}
				
				TicketQuote ticketQuote = new TicketQuote(quote, date, ticketId);
				ticketQuote.setMarkedPrice((ticketQuote.getTicket().getAdjustedCurrentPrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setQty(ticketQuote.getTicket().getRemainingQuantity());
				ticketQuotes.add(ticketQuote);
			}
			
			DAORegistry.getTicketQuoteDAO().saveOrUpdateAll(ticketQuotes);
		} catch(Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		if(appentToQuote ==null){
			return "Tickets added to quotes";
		}else{
			return "Tickets appended to quotes";	
		}
		
	}
	
	@RemoteMethod
	public synchronized String addTicketsAndZonesToQuotesByBrokerId(String ticketIds,String groupIds, String customerName, String dateStr, Double percentAddition,boolean isNew ,Integer appentToQuote ,String refferal,Integer brokerId) {
		try {
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.sql.Date date;
			try {
				date = new java.sql.Date(dateFormat.parse(dateStr).getTime());
			} catch (ParseException e) {
				return "Invalid date!";
			} 
			if(customerName == null || customerName.equals("")){
				return "Customer can not be null";
			}
			// check if the customer is already in the DB and create it if not
			
			QuoteCustomer customer = null;
			if(brokerId != null && brokerId != 1) {
				customer = DAORegistry.getQuoteCustomerDAO().getCustomerByNameandBrokerId(customerName,brokerId);
			} else {
				customer = DAORegistry.getQuoteCustomerDAO().getCustomerByName(customerName);
			}
			
			if (customer == null) {
				customer = new QuoteCustomer(username, customerName);
				DAORegistry.getQuoteCustomerDAO().save(customer);
			}
	
			// create a new quote if there is no active now
			Quote quote = null;
			if(appentToQuote == null){
				quote = DAORegistry.getQuoteDAO().getOpenQuote(customer.getId(), username, date);
				if (quote == null) {
					quote = new Quote(username, customer, date);
					quote.setReferral(refferal);
					DAORegistry.getQuoteDAO().save(quote);
				}else{
					return "Quote already exist";
				}
			}else{
				quote = DAORegistry.getQuoteDAO().get(appentToQuote);
				quote.setSentDate(null);
				DAORegistry.getQuoteDAO().update(quote);
			}
			// create a map so we can check if the ticket is already there
			// date => quote
			Map<Integer, TicketQuote> ticketQuoteMap = new HashMap<Integer, TicketQuote>();
			for (TicketQuote ticketQuote: DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(quote.getId())) {
				ticketQuoteMap.put(ticketQuote.getTicketId(), ticketQuote);
			}
			
			Collection<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
			if(ticketIds!=null && !"".equalsIgnoreCase(ticketIds)){
			for (String ticketIdStr: ticketIds.split(",")) {
				Integer ticketId = Integer.valueOf(ticketIdStr);
				if (ticketQuoteMap.containsKey(ticketId)) {
					continue;
				}
				
				TicketQuote ticketQuote = new TicketQuote(quote, date, ticketId);
				ticketQuote.setMarkedPrice((ticketQuote.getTicket().getAdjustedCurrentPrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setQty(ticketQuote.getTicket().getRemainingQuantity());
				ticketQuotes.add(ticketQuote);
			}
		}
		if(groupIds!=null && !"".equalsIgnoreCase(groupIds)){
			for (String ticketIdStr: groupIds.split(",")) {
				 if("".equalsIgnoreCase(ticketIdStr)){
					 continue;
				 }
				String[] combinedIds =  ticketIdStr.split("--");
				Integer ticketId = Integer.valueOf(combinedIds[0]);
				String  zone = combinedIds[1];				
				Double zonePrice =Double.valueOf(combinedIds[2]);
				Integer zoneQty =Integer.valueOf(combinedIds[3]);
				
				if (ticketQuoteMap.containsKey(ticketId)) {
					continue;
				}
				
				TicketQuote ticketQuote = new TicketQuote(quote, date, ticketId);
				ticketQuote.setZone(zone);				
				ticketQuote.setZonePrice(zonePrice*zoneQty);
				ticketQuote.setZoneQty(zoneQty);
				ticketQuote.setZoneMarkedPrice((ticketQuote.getZonePrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setMarkedPrice((ticketQuote.getTicket().getAdjustedCurrentPrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setQty(ticketQuote.getTicket().getRemainingQuantity());
				ticketQuotes.add(ticketQuote);
			}
			}
			DAORegistry.getTicketQuoteDAO().saveOrUpdateAll(ticketQuotes);
		} catch(Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		if(appentToQuote ==null){
			return "Tickets added to quotes";
		}else{
			return "Tickets appended to quotes";	
		}
		
	}
	@RemoteMethod
	public synchronized String addTicketsAndZonesToQuotes(String ticketIds,String groupIds, String customerName, String dateStr, Double percentAddition,boolean isNew ,Integer appentToQuote ,String refferal) {
		try {
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.sql.Date date;
			try {
				date = new java.sql.Date(dateFormat.parse(dateStr).getTime());
			} catch (ParseException e) {
				return "Invalid date!";
			} 
			if(customerName == null || customerName.equals("")){
				return "Customer can not be null";
			}
			// check if the customer is already in the DB and create it if not
			QuoteCustomer customer = DAORegistry.getQuoteCustomerDAO().getCustomerByName(customerName);
			if (customer == null) {
				customer = new QuoteCustomer(username, customerName);
				DAORegistry.getQuoteCustomerDAO().save(customer);
			}
	
			// create a new quote if there is no active now
			Quote quote = null;
			if(appentToQuote == null){
				quote = DAORegistry.getQuoteDAO().getOpenQuote(customer.getId(), username, date);
				if (quote == null) {
					quote = new Quote(username, customer, date);
					quote.setReferral(refferal);
					DAORegistry.getQuoteDAO().save(quote);
				}else{
					return "Quote already exist";
				}
			}else{
				quote = DAORegistry.getQuoteDAO().get(appentToQuote);
				quote.setSentDate(null);
				DAORegistry.getQuoteDAO().update(quote);
			}
			// create a map so we can check if the ticket is already there
			// date => quote
			Map<Integer, TicketQuote> ticketQuoteMap = new HashMap<Integer, TicketQuote>();
			for (TicketQuote ticketQuote: DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(quote.getId())) {
				ticketQuoteMap.put(ticketQuote.getTicketId(), ticketQuote);
			}
			
			Collection<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
			if(ticketIds!=null && !"".equalsIgnoreCase(ticketIds)){
			for (String ticketIdStr: ticketIds.split(",")) {
				Integer ticketId = Integer.valueOf(ticketIdStr);
				if (ticketQuoteMap.containsKey(ticketId)) {
					continue;
				}
				
				TicketQuote ticketQuote = new TicketQuote(quote, date, ticketId);
				ticketQuote.setMarkedPrice((ticketQuote.getTicket().getAdjustedCurrentPrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setQty(ticketQuote.getTicket().getRemainingQuantity());
				ticketQuotes.add(ticketQuote);
			}
		}
		if(groupIds!=null && !"".equalsIgnoreCase(groupIds)){
			for (String ticketIdStr: groupIds.split(",")) {
				 if("".equalsIgnoreCase(ticketIdStr)){
					 continue;
				 }
				String[] combinedIds =  ticketIdStr.split("--");
				Integer ticketId = Integer.valueOf(combinedIds[0]);
				String  zone = combinedIds[1];				
				Double zonePrice =Double.valueOf(combinedIds[2]);
				Integer zoneQty =Integer.valueOf(combinedIds[3]);
				
				if (ticketQuoteMap.containsKey(ticketId)) {
					continue;
				}
				
				TicketQuote ticketQuote = new TicketQuote(quote, date, ticketId);
				ticketQuote.setZone(zone);				
				ticketQuote.setZonePrice(zonePrice*zoneQty);
				ticketQuote.setZoneQty(zoneQty);
				ticketQuote.setZoneMarkedPrice((ticketQuote.getZonePrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setMarkedPrice((ticketQuote.getTicket().getAdjustedCurrentPrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setQty(ticketQuote.getTicket().getRemainingQuantity());
				ticketQuotes.add(ticketQuote);
			}
			}
			DAORegistry.getTicketQuoteDAO().saveOrUpdateAll(ticketQuotes);
		} catch(Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		if(appentToQuote ==null){
			return "Tickets added to quotes";
		}else{
			return "Tickets appended to quotes";	
		}
		
	}
	@RemoteMethod
	public synchronized String addZoneTicketsToQuotes(String groupIds, String customerName, String dateStr, Double percentAddition,boolean isNew ,Integer appentToQuote ,String refferal) {
		try {
			String username = SecurityContextHolder.getContext().getAuthentication().getName();
			
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			java.sql.Date date;
			try {
				date = new java.sql.Date(dateFormat.parse(dateStr).getTime());
			} catch (ParseException e) {
				return "Invalid date!";
			} 
			if(customerName == null || customerName.equals("")){
				return "Customer not be null";
			}
			// check if the customer is already in the DB and create it if not
			QuoteCustomer customer = DAORegistry.getQuoteCustomerDAO().getCustomerByName(customerName);
			if (customer == null) {
				customer = new QuoteCustomer(username, customerName);
				DAORegistry.getQuoteCustomerDAO().save(customer);
			}
	
			// create a new quote if there is no active now
			Quote quote = null;
			if(appentToQuote == null){
			quote = DAORegistry.getQuoteDAO().getOpenQuote(customer.getId(), username, date);
			if (quote == null) {
				quote = new Quote(username, customer, date);
				quote.setReferral(refferal);
				quote.setQuoteType("zone");
				DAORegistry.getQuoteDAO().save(quote);
			}else{
				return "Quote already exist";
			}
			}else{
				quote = DAORegistry.getQuoteDAO().get(appentToQuote);
				quote.setSentDate(null);
				DAORegistry.getQuoteDAO().update(quote);
			}
			// create a map so we can check if the ticket is already there
			// date => quote
			Map<Integer, TicketQuote> ticketQuoteMap = new HashMap<Integer, TicketQuote>();
			for (TicketQuote ticketQuote: DAORegistry.getTicketQuoteDAO().getTicketQuotesByQuoteId(quote.getId())) {
				ticketQuoteMap.put(ticketQuote.getTicketId(), ticketQuote);
			}
			
			Collection<TicketQuote> ticketQuotes = new ArrayList<TicketQuote>();
			for (String ticketIdStr: groupIds.split(",")) {
				 if("".equalsIgnoreCase(ticketIdStr)){
					 continue;
				 }
				String[] combinedIds =  ticketIdStr.split("--");
				Integer ticketId = Integer.valueOf(combinedIds[0]);
				String  zone = combinedIds[1];				
				Double zonePrice =Double.valueOf(combinedIds[2]);
				Integer zoneQty =Integer.valueOf(combinedIds[3]);
				
				if (ticketQuoteMap.containsKey(ticketId)) {
					continue;
				}
				
				TicketQuote ticketQuote = new TicketQuote(quote, date, ticketId);
				ticketQuote.setZone(zone);				
				ticketQuote.setZonePrice(zonePrice*zoneQty);
				ticketQuote.setZoneQty(zoneQty);
				ticketQuote.setZoneMarkedPrice((ticketQuote.getZonePrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setMarkedPrice((ticketQuote.getTicket().getAdjustedCurrentPrice() * (100 + percentAddition)) / 100D);
				ticketQuote.setQty(ticketQuote.getTicket().getRemainingQuantity());
				ticketQuotes.add(ticketQuote);
			}
			
			DAORegistry.getTicketQuoteDAO().saveOrUpdateAll(ticketQuotes);
		} catch(Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
		if(appentToQuote ==null){
			return "Tickets added to quotes";
		}else{
			return "Tickets appended to quotes";	
		}
		
	}
	
	@RemoteMethod
	public boolean unLinkEvent(Integer eventId) {
	
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		Event oldEvent=DAORegistry.getEventDAO().get(eventId);
		oldEvent.setUpdatedBy(username);
    	DAORegistry.getEventDAO().update(oldEvent);
    	Date now = new Date();
		EventAudit ea=new EventAudit();
		ea.setEventId(oldEvent.getId());
		ea.setUserName(username);
		ea.setAction("Unlinked");
		ea.setModifiedDate(now);
		//ea.setOldEventName(oldEvent.getName());
		ea.setEventName(oldEvent.getName());
		//ea.setOldTourName(oldEvent.getTour().getName());
		/*ea.setTourName(oldEvent.getTour().getName());
		ea.setArtistId(oldEvent.getTour().getArtist().getId());
		ea.setArtistName(oldEvent.getTour().getArtist().getName());*/
		ea.setArtistId(oldEvent.getArtist().getId());
		ea.setArtistName(oldEvent.getArtist().getName());
		ea.setEventDate(oldEvent.getLocalDate());
		//ea.setNewEventDate(oldEvent.getLocalDate());
	//	ea.setOldEventTime(oldEvent.getLocalTime());
		ea.setEventTime(oldEvent.getLocalTime());
		ea.setVenueName(oldEvent.getVenue().getBuilding());
		//ea.setNewVenueName(oldEvent.getVenue().getBuilding());
		ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(oldEvent.getVenueCategoryId()).toString());
		if(oldEvent.getVenueCategoryId()!= null)
		{
			ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(oldEvent.getVenueCategoryId()).toString());
		}
		else
		{
		ea.setVenueCategory(null);
		}
	
		if(oldEvent.getEventType().name()!=null){
		ea.setEventType(oldEvent.getEventType().name());
		}
		else
		{
			ea.setEventType(null);
		}

		if(oldEvent.getEventType().name()!= null)
		{
		ea.setEventType(oldEvent.getEventType().name());
		}
		else
		{
		ea.setEventType(null);
		}
		if(oldEvent.getNoPrice()!=null){
		ea.setNoPrice("YES");
		}
		else
		{
			ea.setNoPrice("NO");
		}

		if(oldEvent.getNoPrice()!= null){
		ea.setNoPrice("YES");
		}
		else
		{
		ea.setNoPrice("NO");
		}
		if(oldEvent.getAdmitoneId()!=null){
		ea.setAdmitoneId(oldEvent.getAdmitoneId());
		}
		else
		{
			ea.setAdmitoneId(null);
		}	
		ea.setAdmitoneId(null);
		//ea.setTourId(oldEvent.getTourId());
		ea.setVenueId(oldEvent.getVenueId());
		DAORegistry.getEventAuditDAO().save(ea);
		boolean flag = DAORegistry.getEventDAO().unLinkAdmintoneEvent(eventId);
		return flag;
	}
	
//	public void setEventListingManager(EventListingManager eventListingManager) {
//		DataDwr.eventListingManager = eventListingManager;
//	}
	
	public void setVelocityEngine(VelocityEngine velocityEngine) {
		DataDwr.velocityEngine = velocityEngine;
	}	
	

	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(
			TicketListingCrawler ticketListingCrawler) {
		DataDwr.ticketListingCrawler = ticketListingCrawler;
	}

	public void setJiraClient(JiraClient jiraClient) {
		DataDwr.jiraClient = jiraClient;
	}

	public void setRedmineClient(WebRedmineClient redmineClient) {
		DataDwr.redmineClient = redmineClient;
	}


}
