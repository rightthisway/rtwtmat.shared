package com.admitone.tmat.eventfetcher;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.net.URI;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Worker;
import javafx.concurrent.Worker.State;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import com.sun.javafx.application.PlatformImpl;

public class JavaFxWebOpener extends Application {

	/*
	 * static{ System.setProperty("https.proxyHost", "us-s26.netnut.io");
	 * System.setProperty("https.proxyPort", "33128");
	 * System.setProperty("https.proxyUser", "Rightthisway");
	 * System.setProperty("https.proxyPassword", "Qpox9d");
	 * System.setProperty("jdk.http.auth.tunneling.disabledSchemes", ""); }
	 */

	private static int exitCode = 4563456;

	public static void exit(int exitCode) {
		JavaFxWebOpener.exitCode = exitCode;
		Platform.exit();
	}

	@Override
	public void start(final Stage stage) {

		// Platform.setImplicitExit(true);
		Parameters args = getParameters();
		List paranList = args.getUnnamed();
		final String url = (String) paranList.get(0);
		final String filename = (String) paranList.get(1);

		System.out.println(" [JavaFxWebOpener ] url  & filename    recieved to load is  " + filename + " --" + url);
		stage.setWidth(400);
		stage.setHeight(500);
		Scene scene = new Scene(new Group());
		VBox root = new VBox();
		final WebView browser = new WebView();

		final WebEngine webEngine = browser.getEngine();

		// webEngine.setJavaScriptEnabled(true);
		ScrollPane scrollPane = new ScrollPane();
		scrollPane.setContent(browser);

		webEngine.getLoadWorker().stateProperty().addListener(new ChangeListener<State>() {
			@Override
			public void changed(ObservableValue ov, State oldState, State newState) {
				if (newState == Worker.State.SUCCEEDED) {

					System.out.println(" [JavaFxWebOpener.changed() ] url  & filename    recieved to load is  "
							+ filename + " --" + url);

					org.w3c.dom.Document doc = (org.w3c.dom.Document) webEngine.getDocument();
					org.w3c.dom.Node n = doc;
					try {

						Source source = new DOMSource(doc);
						File file = new File(filename);
						if (!file.exists()) {
							System.out.println(" --------- FILE DOES NOT EXIST ... ");
							boolean b = file.createNewFile();
							System.out.println(" --------- FILE DOES NOT EXIST ... created new  " + b);
						}

						Result result = new StreamResult(file);
						Transformer xformer = TransformerFactory.newInstance().newTransformer();
						System.out.println(" [xformer]  ");
						xformer.transform(source, result);
						StringWriter writer = new StringWriter();
						System.out.println(" [writer]  ");
						StreamResult streamResult = new StreamResult(writer);
						TransformerFactory tf = TransformerFactory.newInstance();
						System.out.println(" [streamResult]  ");
						Transformer transformer = tf.newTransformer();
						System.out.println(" [transformer]  ");
						transformer.transform(source, streamResult);

					} catch (Exception ex) {
						ex.printStackTrace();
					} finally {
						System.out.println("  Exiting Platform from Finally Block.. ");

						Platform.exit();

					}

				}
			}
		});

		try {
			Map<String, List<String>> headers = new LinkedHashMap<String, List<String>>();
			URI uri = URI.create(url);
			webEngine.load(url);
			root.getChildren().add(browser);
			scene.setRoot(root);
			stage.setScene(scene);
			stage.show();
			new Thread("Platform Closer Thread when failed to open Url") {
				public void run() {
							System.out.println("Thread: " + getName() + " running");
								try {
									System.out.println("Thread: " + getName() + " Sleep Start");
									Thread.sleep(35000);
									System.out.println("CALLING FORCEFUL JVM EXIT .. SHUTDOWN FROM FXOPENER ....");
									System.out.println("Thread: " + getName() + " Sleep End");
									Runtime.getRuntime().halt(1);
									Platform.exit();
								} catch (Exception ex) {
									
									System.out.println("Thread: " + getName() + " Exception " + ex);
								}
				}
			}.start();

		} catch (Exception ex) {
			System.out.println(ex);
		}

	}

	public static void main(String[] args) throws Exception {

		System.out.println("5 Args recd from batch ----" + args[0] + "-- " + args[1] + "-- " + args[2] + "-- " + args[3]
				+ "-- " + args[4]);
		String jvm = System.getProperty("java.home");
		String fileName = "C:/tmpcrawl/bbb.txt";
		String str = jvm + " 5 Args recd from batch ----" + args[0] + "-- " + args[1] + "-- " + args[2] + "-- "
				+ args[3] + "-- " + args[4];
		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
		writer.write(str);
		writer.close();

		final String userName = args[4];

		System.setProperty("https.proxyHost", args[2]);
		System.setProperty("https.proxyPort", args[3]);
		System.setProperty("https.proxyUser", userName);
		System.setProperty("https.proxyPassword", "Qpox9d");
		System.setProperty("jdk.http.auth.proxying.disabledSchemes", "");
		System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "");
		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(userName, "Qpox9d".toCharArray());
			}
		});

		launch(args);

	}

}
