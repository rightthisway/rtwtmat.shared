package com.admitone.tmat.eventfetcher;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/*EventListing Fetcher for Ticketcity Exchange
@author sandeep Bhatia*/
public class TicketCityEventListingFetcher extends EventListingFetcher {

	private static Logger logger = LoggerFactory.getLogger(TicketCityEventListingFetcher.class);
	
	@Override
	public Collection<EventHit> getEventList(String keywords, String location,
			Date fromDate, Date toDate,List<Event> eventList,
			boolean isVenue,boolean isStubhubFromTicketEvolution) 
			throws Exception {
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		//Replace space to Hyphen for tick pick events
		keywords = keywords.replaceAll("\\s+", "%20");
		SimpleHttpClient httpClient = null;
		
		try{
			httpClient = HttpClientStore.createHttpClient(Site.TICKET_CITY);
			String ticketCityFeedUrl = "https://www.ticketcity.com/data/find/"+keywords+"?cs=999&ps=999&vs=999&es=100&_=1458736475182";
			
			Collection<EventHit> eventResults = getEventsFromTicketCity(ticketCityFeedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
			/*for(EventHit h : eventResults){
				System.out.println("Events :: " +h.getName()+" "+h.getEventId()+" "+h.getSiteId()+" "+h.getUrl()
						+" "+h.getLocation()+" "+h.getTime());
			}*/
			eventHits.addAll(eventResults);
			
		}catch(Exception e) {
			logger.error("SHELF Caught Exception: " + e);
			e.printStackTrace();
		}finally {
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}
		
				return eventHits;
		
	}
	
	/**
	 * Fetch events from ticketCity
	 * exchange
	 * @param feedUrl
	 * @param httpClient
	 * @param keywords
	 * @param location
	 * @param fromDate
	 * @param toDate
	 * @param eventList
	 * @param isVenue
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws ParseException 
	 * @throws JSONException 
	 */
	public Collection<EventHit> getEventsFromTicketCity(String feedUrl, CloseableHttpClient httpClient, String keywords, 
			String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws ClientProtocolException, IOException, ParseException, JSONException{
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");
		
		Map<String, Boolean> locationMap = new HashMap<String, Boolean>();
		Map<String, Boolean> dateMap = new HashMap<String, Boolean>();
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
		
		for(Event event : eventList){
			if(event.getDate() == null){
				dateMap.put("TBD", true);
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}
		
		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		/*while (true) {*/
			HttpGet httpPost = new HttpGet(feedUrl);
			httpPost.setHeader("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Host", "www.ticketcity.com");
			httpPost.addHeader("Accept","application/json, text/javascript, text/html, application/xhtml+xml, application/xml,*/*");
			httpPost.addHeader("Content-Type", "application/json, charset=utf-8");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
            System.out.println("http post details;"+httpPost.toString());
			//Hit the ticketCity  site and get the response
	        System.out.println("http name"+httpClient.toString());
		
	        String content = null;
	        int statusCode = 0;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpPost);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				
				statusCode = response.getStatusLine().getStatusCode();

		        if (statusCode < 200 || statusCode >= 300) {
			        System.out.println("Bad Response from TicketCity:" + statusCode);
			        try {
						throw new Exception("Bad Response from TicketCity Server");
					} catch (Exception e) {
				
						e.printStackTrace();
					}	
		        }
				
				//ensure it is fully consumed
				if(entity != null){
			        content = EntityUtils.toString(entity);	
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}

	        System.out.println("content"+content.toString());
	        if (null == content || content.isEmpty()) {
				System.out.println("Empty Respons from TicketCity:" + statusCode);
				try {
					throw new Exception("Empty Response from TicketCity Server");
				} catch (Exception e) {
				
					e.printStackTrace();
				}	
		    }
			
	        JSONObject eventResult =new JSONObject(content);
	        System.out.println("return1");
	        JSONArray TicketListingEventArray =eventResult.getJSONArray("E");
	        System.out.println(TicketListingEventArray.toString());
		
		    for (int i = 0; i < TicketListingEventArray.length(); i++) {
		    	JSONObject  eventObject=TicketListingEventArray.getJSONObject(i);
		    	System.out.println("i"+i);
		    	System.out.println("event object--"+eventObject.toString());
		    	String eventDateStr = "";
				String eventName = "";
				String eventLoc = "";
				String eventId = "";
				String timeStr = "";
				String ticketFetchUrl = "";
				
				//Event date
			     eventDateStr=eventObject.getString("ISO");
			     System.out.println("eventdate="+eventDateStr);
			     TimeZone timeZone = TimeZone.getDefault();
				 Calendar cal = Calendar.getInstance(timeZone);
				 SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				 sdfd.setCalendar(cal);
				 cal.setTime(sdfd.parse(eventDateStr));
				 Date ticketCityDates = cal.getTime();
				 System.out.println("ticketCitydate-"+ticketCityDates);
				 System.out.println("fromdate-"+fromDate);
				 System.out.println("todate-"+toDate);
				 
				 Date eventDate = format.parse(eventDateStr);
				 java.sql.Date date = new java.sql.Date(eventDate.getTime());
				
				if(ticketCityDates.after(fromDate) && ticketCityDates.before(toDate)){
							System.out.println("Matched with the exact event date " +ticketCityDates);
							System.out.println("eventdate-"+eventDateStr);			
							//Event name for the matched date
							eventName=eventObject.getString("N");
						    System.out.println("eventName-"+eventName);
							//Event location for the matched date
							String venue=eventObject.getString("V");
							String state=eventObject.getString("S");
							String country=eventObject.getString("CT");
							eventLoc=venue+","+state+","+country;
							System.out.println("Eventlocation-"+eventLoc);
							
							//Event ticket fetching url for the matched date
						    ticketFetchUrl=eventObject.getString("PURL");
						    ticketFetchUrl=ticketFetchUrl.replace("//","");
							String append = "https://www.ticketcity.com";
							ticketFetchUrl = append+ticketFetchUrl;
							 System.out.println("Url-"+ticketFetchUrl);
							//Event time
							JSONObject time=eventObject.getJSONObject("WD");
						    timeStr=time.getString("T");
						    System.out.println("Time-"+timeStr);
						    eventId =eventObject.getString("RID");
						    System.out.println("Eventid-"+eventId);
						    
						    //Location filter
							if (location != null) {
								if (eventLoc.toLowerCase().indexOf(location.toLowerCase()) < 0) {
									continue;
								}
							}
										
							SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
							long ms = sdf.parse(timeStr).getTime();
							Time t = new Time(ms);
							if(!eventIds.contains(eventId)){	
								System.out.println("SHELF SPLITWORDS: " + eventName);
								EventHit eventHit = new EventHit(eventId, eventName, date, t, eventLoc, Site.TICKET_CITY, ticketFetchUrl);
								eventHits.add(eventHit);
								eventIds.add(eventId);
							}
						}
				 }
	 	
	System.out.println("return");
	System.out.println("content-"+content);
	return eventHits;
	
	}
	

    //test
	/*public static void main(String[] args) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date fromDate = format.parse("06/11/2016 00:00:00");
		Date toDate = format.parse("06/11/2016 23:59:59");
		Event event = new Event();
		event.setLocalDate(format.parse("06/11/2016 23:59:59"));
		List<Event> eventList = new ArrayList<Event>();
		eventList.add(event);
		Collection<EventHit> hits = new TicketCityEventListingFetcher().getEventList("2016 Bellmont Stakes", null, fromDate, toDate, eventList, false, false);
		System.out.println(hits.toString());
	}*/
}
