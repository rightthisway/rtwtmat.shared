package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * TicketNetwork Event Fetcher.
 */

public class TicketNetworkEventListingFetcher extends EventListingFetcher {
	private Pattern eventIdPattern = Pattern.compile("evtid=(\\d+)&");
//	private Pattern datePattern = Pattern.compile("(\\d{1,2}/\\d{1,2}/\\d{4})");
	
	//                                                                                                                EVENT NAME                      VENUE NAME          CITY                                                                               DATE                                                 TIME                                    URL
	private Pattern eventPattern = Pattern.compile("<tr valign=\"middle\" class=\"tn_results_[a-z]*?_row\">.*?<td.*?<a.*?>(.*?)</a>.*?</td>.*?<td.*?<a.*?>(.*?)</a>.*?<span.*?>(.*?)</span>.*?</td>.*?<td.*?<span.*?</span>.*?<span class=\"tn_results_date_text\">(.*?)</span>.*?<span class=\"tn_results_time_text\">(.*?)</span>.*?</td>.*?<td.*?<a.*?href=\"(.*?)\".*?</td>.*?</tr>", Pattern.DOTALL);
	
	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		if (location == null) {
			location = "";
		}
		
		String normalizedKeywords = QueryUtil.normalizeQuery(keywords).trim();
		if (normalizedKeywords.isEmpty() && !keywords.trim().isEmpty()) {
			return eventHits; 
		}
		
		Collection<String> eventIds = new ArrayList<String>();
		
		for(String keyword : keywords.replaceAll("\\s+", " ").split(" ")) {

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
		String url;
		
		if (keyword == null || keyword.length() == 0) {
			url = "http://zeromarkup.com/ResultsDate.aspx?kwds=&city=&pcatid=&ccatid="
				+ "&sdate=" + dateFormat.format(fromDate) 
				+ "&edate=" + dateFormat.format(toDate)
				+ "&zip=&stprvid=&location=-Any-";
		} else {
			url = "http://zeromarkup.com/ResultsGeneral.aspx?kwds=" + keyword
				     + "&sdate=" + dateFormat.format(fromDate)
				     + "&edate=" + dateFormat.format(toDate)
				     + "&location=" + location.replaceAll("\\s+", "%20");
		}

		HttpGet httpGet = new HttpGet(url);
		SimpleHttpClient httpClient = null;		
		
		try {
			httpClient = HttpClientStore.createHttpClient();
			String content = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpGet);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity);
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
		
			Matcher eventMatcher = eventPattern.matcher(content);
			while(eventMatcher.find()) {
				String eventName = eventMatcher.group(1);
				String venueName = eventMatcher.group(2);
				String venueCity = eventMatcher.group(3);
				String dateString = eventMatcher.group(4);
				String timeString = eventMatcher.group(5);
				String eventUrl = "http://zeromarkup.com/" + eventMatcher.group(6);
				
				java.sql.Date date = new java.sql.Date(dateFormat.parse(dateString).getTime());
				
				if (date.before(fromDate) || date.after(toDate)) {
					continue;
				}
				
				java.sql.Time time = null;
				try {
					time = new java.sql.Time(timeFormat.parse(timeString).getTime());
				} catch(Exception e) {};
				
				if (!QueryUtil.matchAnyTerms(normalizedKeywords, QueryUtil.normalizeQuery(eventName))) {
					continue;
				}
				
				String eventLocation = venueName + " " + venueCity;
				if (!QueryUtil.matchAnyTerms(location, eventLocation)) {
					continue;
				}

				Matcher eventIdMatcher = eventIdPattern.matcher(eventUrl);
				if (!eventIdMatcher.find()) {
					continue;
				}
				String eventId = eventIdMatcher.group(1);				
				
				EventHit eventHit = new EventHit(eventId, eventName, date, time, eventLocation, Site.TICKET_NETWORK, eventUrl);
				if(!eventIds.contains(eventId)) {
					eventHits.add(eventHit);
					eventIds.add(eventId);
				}
			}
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		}
		return eventHits;
	}
}
