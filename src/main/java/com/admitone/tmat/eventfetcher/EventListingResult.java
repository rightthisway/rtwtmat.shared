package com.admitone.tmat.eventfetcher;

import java.util.Collection;

import org.directwebremoting.annotations.DataTransferObject;
import org.directwebremoting.annotations.RemoteProperty;

@DataTransferObject
public class EventListingResult {
	public String siteId;
	public Collection<EventHit> eventHits;
	public Exception exception;
	
	public EventListingResult(String siteId) {
		this.siteId = siteId;
	}

	@RemoteProperty
	public String getSiteId() {
		return siteId;
	}

	@RemoteProperty
	public Collection<EventHit> getEventHits() {
		return eventHits;
	}

	@RemoteProperty
	public Exception getException() {
		return exception;
	}

	public void setEventHits(Collection<EventHit> eventHits) {
		this.eventHits = eventHits;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	@RemoteProperty
	public String getExceptionStackTrace() {
		if (exception == null || exception.getStackTrace() == null) {
			return null;
		}
		
		String str ="";
		
		for (StackTraceElement element : exception.getStackTrace()) {
			str += element.toString() + "\n";
		}		
		return str;
	}

}
