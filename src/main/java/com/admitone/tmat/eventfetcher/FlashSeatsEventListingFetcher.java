package com.admitone.tmat.eventfetcher;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class FlashSeatsEventListingFetcher extends EventListingFetcher {
	private static Logger logger = LoggerFactory.getLogger(FlashSeatsEventListingFetcher.class);
	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {
		
		Date startTime = new Date();
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		keywords = keywords.replaceAll("\\s+", " ");
		SimpleHttpClient httpClient = null;
//		for(String keyword : keywords.split(" ")) {
			try {
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
				String from = format.format(fromDate);
				String to = format.format(toTomorrowDate);
//				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				/*******/
			
				httpClient = HttpClientStore.createHttpClient(Site.FLASH_SEATS);
				String feedUrl = "";
//				String searchTerm="";
//				String dateFrom="";
//				String dateTo="";
//				String url 
		//		feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active+maxPrice&wt=json";
//				if(!isVenue){
					feedUrl = "https://www.flashseats.com/Default.aspx?pid=19&eo=0&eod=s&ss=0";
					/*if (keywords != null && keywords.length() > 0) {
						feedUrl += "%0D%0A%2Bdescription%3A" + keywords.replaceAll("\\s+", "+");			
					}
					
					if (from != null && to != null) {
						feedUrl += "%0D%0A%2Bevent_date%3A[" + from + "T00:00:00Z+TO+" + to + "T23:59:59Z]";
					}
					feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active&rows=50000&wt=json";
					*/
					Collection<EventHit> subResult= getEventsFromExchange(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
					eventHits.addAll(subResult);
				/*}
				else{
					String venueString = keywords;//eventList.get(0).getVenue().getBuilding();
					String venueURL ="http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Avenue%20description%3A";
					venueURL =  venueURL + venueString.replaceAll("\\s+", "+") + "%0D%0A%0D%0A&version=2.2&fl=id+event+description+city+state+active&rows=20&wt=json";
					System.out.println(venueURL);
					HttpPost httpPostVenue = new HttpPost(venueURL);
					
					httpPostVenue.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//					httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
					httpPostVenue.addHeader("Accept", "application/json, text/javascript, * /*");
					httpPostVenue.addHeader("Accept-Encoding", "gzip,deflate");
					HttpResponse venueResponse = httpClient.execute(httpPostVenue);
					HttpEntity venueEntity = HttpEntityHelper.getUncompressedEntity(venueResponse.getEntity());
					JSONObject venueJsonObject = null;
					String venueResult = EntityUtils.toString(venueEntity).trim();
					venueJsonObject = new JSONObject(venueResult);
				
					JSONArray docsJSONVenueArray = venueJsonObject.getJSONObject("response").getJSONArray("docs");
					Set<Integer> venueIdSet= new HashSet<Integer>();
					for(int i=0;i<docsJSONVenueArray.length();i++){
						JSONObject docJSONVenueObject = docsJSONVenueArray.getJSONObject(i);
//						System.out.println(docJSONVenueObject.getString("description").trim().toLowerCase());
						if(keywords.toLowerCase().equals(docJSONVenueObject.getString("description").trim().toLowerCase())){
							venueIdSet.clear();
							venueIdSet.add(docJSONVenueObject.getInt("id"));
							break;
						}
						venueIdSet.add(docJSONVenueObject.getInt("id"));
					}
					for(Integer venueId:venueIdSet){
						feedUrl = "http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent";
						if (keywords != null && keywords.length() > 0) {
							feedUrl += "%0D%0A%2Bgeography_parent%3A" + venueId;			
						}	
						if (from != null && to != null){
							feedUrl += "%0D%0A%2Bevent_date%3A[" + from + "T00:00:00Z+TO+" + to + "T23:59:59Z]";
						}
						feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active&rows=50000&wt=json";
						
						System.out.println("FEED URL=" + feedUrl);
						Collection<EventHit> subResult= getEventsFromExchange(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
						eventHits.addAll(subResult);
					}
					
				}*/
				/*******/
				
				
			} catch (Exception e) {
				System.out.println("SHELF Caught Exception: " + e);
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
			}
//		}
		logger.info("Event SEARCH FLASH : "+(new Date().getTime()-startTime.getTime())+" : key: "+keywords+" : loc: "+location);
		return eventHits;
	}
	
	public Collection<EventHit> getEventsFromExchange(String feedUrl,CloseableHttpClient httpClient,String keywords,String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws ClientProtocolException, IOException, JSONException, ParseException{
		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yy hh:mm aa z");
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
//		Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
//		String from = format.format(fromDate);
//		String to = format.format(toTomorrowDate);

		Date beforeDate = new Date();
		//System.out.println("FLASHEvent inside 1....."+beforeDate);
		
		java.sql.Date sqlFromDate = new java.sql.Date(new java.util.Date(fromDate.getTime() - 24 * 3600 * 1000).getTime());
		java.sql.Date sqlToDate = new java.sql.Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		for(Event event:eventList){
			if(event.getDate()==null){
				dateMap.put("TBD", true);
//				String location = event.getVenue().getBuilding();
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}
		HttpPost httpPost = new HttpPost(feedUrl);
		// System.out.println("stubhub>" + feedUrl);
		httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//		httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
		httpPost.addHeader("Accept", "application/json, text/javascript, */*");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		
//		httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB);

		CloseableHttpResponse response = null;
		String result = null;
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				result = EntityUtils.toString(entity).trim();
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
		JSONObject jsonObject = null;
		
        Document doc = Jsoup.parse(result);
		String viewState = doc.getElementById("__VIEWSTATE").val();

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
		nameValuePairs.add(new BasicNameValuePair("__EVENTTARGET","ctl00$ctl02$fsPager1$viewAllHyperLink"));
		nameValuePairs.add(new BasicNameValuePair("__EVENTARGUMENT",""));
		nameValuePairs.add(new BasicNameValuePair("__VIEWSTATE",viewState));
		httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs)); 
          
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				result = EntityUtils.toString(entity).trim();
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
        doc = Jsoup.parse(result);
	
        Date currentDate = new Date();
		System.out.println("FLASHEVENT fetching time.."+(currentDate.getTime()-beforeDate.getTime())+"...now.."+new Date());
		beforeDate = new Date();
		
		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		 Element tableElement = doc.getElementById("ctl00_ctl02_dgEventList");
         //System.out.println(tableElement);
         Elements trTags = tableElement.select("tbody tr");
         
         int count = 0;
         for(Element tr : trTags){
         	
        	 try {
        		 if(count == 0) {
        			 count++;
        			 continue;
        		 }
	         	count++;
	         	int i = 0;
	         	String eventId = "",name="",venue="",loc="";
	         	String dateStr="",timeStr="";
	         	Double price=0.0;;
	         	Integer qty=0;
	         	String url = "";
	         	Elements tdTags = tr.getElementsByTag("td");
		             
	         	for(Element td : tdTags){
	         		//System.out.println(td);
	         		String[] tdstrArr=null;
	         		switch (i) {
	         			case 0:
	         				name = td.text().trim();
	         				url = "https://www.flashseats.com/"+td.getElementsByTag("a").attr("href");
	         				Pattern eventIdPattern = Pattern.compile(".*&ec=(.*?)&");
	         				Matcher eventIdMatcher = eventIdPattern.matcher(url);
	         				if (eventIdMatcher.find()) {
	         					eventId= eventIdMatcher.group(1);
	         				}
								break;
							case 1:
								//System.out.println(td.text().trim());
								tdstrArr = td.text().trim().split(" ");
								dateStr = tdstrArr[1].trim();
								timeStr = tdstrArr[2].replaceAll(" ", " ");
								if(timeStr.contains("Time")) {
									TimeZone timeZone = TimeZone.getDefault();
									timeStr="00:00 am "+timeZone.getDisplayName(false, TimeZone.SHORT);
								}
								break;
							case 2:
								venue = td.html().replaceAll("<br />", ",").trim();
								break;
						}
	         		i++;
	         	}
	         	
	         	String dateTimeStr = (dateStr + " " + timeStr).replaceAll("\\s+", " ");
	        	java.sql.Date date = new java.sql.Date(format.parse(dateTimeStr).getTime());
	        	Date timeS = timeFormat.parse(timeStr);
				Time time = new Time(timeS.getTime());
				
				if(date.compareTo(sqlFromDate) < 0 || date.compareTo(sqlToDate)> 0) {
					continue;
				}
				
				if(!isVenue){
					if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), name)) {
						continue;
					}
				}else{
					if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), venue)) {
						continue;
					}
				}
				
				String eventCity = null;
				String eventLocation = null;
				String eventState = null;
				String[] temp = venue.split(",");
				try {
					eventLocation = temp[0].trim();
					eventCity = temp[1].trim();
					eventState = temp[2].trim();
					
				}  catch (Exception e) {}
				
				//String eventFullLocation = venue + " " + location;
				
	//			String eventId = docJSONObject.getString("event_id");
	//			String url = "http://www.stubhub.com/?event_id=" + eventId;
				
				String eventFullLocation = eventLocation.trim() + ", " + eventCity.trim() + ", " + eventState.trim();
				
				// location filter
				if (location != null) {
					if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {
				
						continue;
					}
				}
				//System.out.println("date.."+df.format(date));
				boolean flag=false;
				if(!dateMap.containsKey(df.format(date))){
					for(String key:locationMap.keySet()){
						String tokens[] = key.split(":-:");
						String building = tokens[0].trim();
						String cityToken = tokens[1].trim();
						String stateToken = tokens[2].trim();
						
						if( cityToken.equalsIgnoreCase(eventCity.trim())&& stateToken.equalsIgnoreCase(eventState.trim()) && (TextUtil.isSimilarVenue(building, eventLocation, 1))){
							flag=true;
							break;
						}
					}
				}else{
					flag=true;
				}
	//			if(dateMap.containsKey(dateFormat.format(eventDate))){
				// check date - stubhub might return irrelevant dates
				/*if (date.before(fromDate) || date.after(toDate)) {
					continue;
				}*/
				
				if(flag && !eventIds.contains(eventId)){	
					System.out.println("Flash Seats SHELF SPLITWORDS: " + name);
					EventHit eventHit = new EventHit(eventId, name, date, time, eventFullLocation, Site.FLASH_SEATS, url);
					eventHits.add(eventHit);
					eventIds.add(eventId);
				}
        	 }catch (Exception e) {
        		 e.printStackTrace();
        	 }
         }
         currentDate = new Date();
 		System.out.println("FLASHEVENT computation time.."+(currentDate.getTime()-beforeDate.getTime())+"..now.."+new Date()+"..count.."+count);
 		beforeDate = new Date();
 		
		return eventHits;
	}
	/*public static void main(String[] args) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date fromDate = format.parse("03/23/2016");
		Date toDate = format.parse("03/23/2016");
		Event event = new Event();
		event.setLocalDate(format.parse("03/23/2016"));
		List<Event> eventList = new ArrayList<Event>();
		eventList.add(event);
		Collection<EventHit> hits = new FlashSeatsEventListingFetcher().getEventList(" Mormon", null, fromDate, toDate, eventList, false, false);
		
	}*/
}
