package com.admitone.tmat.eventfetcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EiMarketPlaceEvent;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;

public class EiMarketPlaceDBEventListingFetcher extends EventListingFetcher {
	public Collection<EventHit> getEventList(String theKeywords, String location, Date fromDate, Date toDate) throws Exception {
		Collection<EiMarketPlaceEvent> eiMarketPlaceEvents = DAORegistry.getEiMarketPlaceEventDAO().getEiMarketPlaceEvents(fromDate, toDate);
		if (eiMarketPlaceEvents == null) {
			return null;
		}
		String keywords = theKeywords;
		if(keywords != null){
			keywords = QueryUtil.normalizeQuery(theKeywords.replace('+',' '));
		}
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		for (EiMarketPlaceEvent eiMarketPlaceEvent: eiMarketPlaceEvents) {
			
			// check if it keywords matches the event name or the event detail
			if (eiMarketPlaceEvent.getEventName() == null
					|| (
							!QueryUtil.matchAnyTerms(keywords, eiMarketPlaceEvent.getEventName())
							&& (eiMarketPlaceEvent.getEventDetail() == null || !QueryUtil.matchAnyTerms(keywords, eiMarketPlaceEvent.getEventDetail()))
					)
			) {
				continue;
			}

			String eiMarketPlaceEventLocation = eiMarketPlaceEvent.getVenueName() + ", " + eiMarketPlaceEvent.getVenueCity();
			if (!QueryUtil.matchAllTerms(location, eiMarketPlaceEventLocation)) {
				continue;
			}
					
			java.sql.Date sqlEventDate = new java.sql.Date(eiMarketPlaceEvent.getEventDate().getTime()); 
			java.sql.Time sqlEventTime = new java.sql.Time(eiMarketPlaceEvent.getEventDate().getTime());
		
			EventHit eventHit = new EventHit(eiMarketPlaceEvent.getId().toString(), eiMarketPlaceEvent.getEventName() + ((eiMarketPlaceEvent.getEventDetail() != null)?(" - " + eiMarketPlaceEvent.getEventDetail()):""),
					sqlEventDate, sqlEventTime, eiMarketPlaceEventLocation, Site.EI_MARKETPLACE,
					"https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=" + eiMarketPlaceEvent.getId());
			eventHits.add(eventHit);
		}
		
		return eventHits;
	}	
}
