package com.admitone.tmat.eventfetcher;

import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.ChangeProxy;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

// just a placeholder for ticketsnow - working on it now

public class TicketsNowEventListingFetcher extends EventListingFetcher {
	private static Logger logger = LoggerFactory.getLogger(TicketsNowEventListingFetcher.class);
	
//	private EventListingFetcher eiMarketPlaceEventListingFetcher;
	@Override	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {
		Set<String> validURLs= new HashSet<String>();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateTimeFmt = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss a");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationsMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationForVenuePageMap = new HashMap<String, Boolean>();
		Map<String,List<String>> eventDateMap = new HashMap<String, List<String>>();
		
		keywords = keywords.replace('-', '+');
		SimpleHttpClient httpClient = null;
//		Collection<EventHit> eimpEventHits = new ArrayList<EventHit>();
//		Collection<String> eventIds = new ArrayList<String>();
		
		String []keyword=keywords.split(" ");
		
		Date startTime = new Date();
		
		for(Event event:eventList){
			if(event.getDate()==null){
				locationsMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				dateMap.put("TBD", true);
				continue;
			}
			List<String> eventNameList =  eventDateMap.get(df.format(event.getDate()));
			if(eventNameList == null){
				eventNameList = new ArrayList<String>();
			}
			eventNameList.add(event.getName());
			eventDateMap.put(df.format(event.getDate()), eventNameList);
			locationForVenuePageMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
			dateMap.put(df.format(event.getDate()), true);
		}
		if(isVenue){
			String urlKeyword=keywords.replaceAll("-", "").replaceAll(" ", "-");
			String ticketsNowUrl="http://www.ticketsnow.com/"+ urlKeyword + "-venue-tickets";
			HttpGet httpPost= new HttpGet(ticketsNowUrl);
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Host", "www.ticketsnow.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpClient= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
			
			String content = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpPost);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content=EntityUtils.toString(entity).replaceAll("\\s+", " ");
					EntityUtils.consumeQuietly(entity);
					//System.out.println(content);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
				if(null != httpPost){
					httpPost.releaseConnection();
				}
			}
			
			if(content.toLowerCase().contains("page not found")){
				ticketsNowUrl="http://www.ticketsnow.com/Search/SearchResults.aspx?STY=SS&WHAT="+ urlKeyword;
				httpPost= new HttpGet(ticketsNowUrl);
				httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpPost.addHeader("Host", "www.ticketsnow.com");		
				httpPost.addHeader("Accept", "application/json, text/javascript, */*");
				httpPost.addHeader("Accept-Encoding", "gzip,deflate");
				
				try{
					response = httpClient.execute(httpPost);
					HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
					//ensure it is fully consumed
					if(entity != null){
						content=EntityUtils.toString(entity).replaceAll("\\s+", " ");
						EntityUtils.consumeQuietly(entity);
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					//to reuse connection we need to close response stream associated with that
					if(response != null)
						response.close();
					
					if(null != httpPost){
						httpPost.releaseConnection();
					}
					
				}
				
				Pattern pattern = Pattern.compile("searchresultvenuetext'>" +  keywords +"(.*?)>venue");
				Matcher matcher = pattern.matcher(content.toLowerCase());
		        if(matcher.find()){
		        	pattern = Pattern.compile("href='(.*?)'");
		        	Matcher matcher1 = pattern.matcher(matcher.group(1));
		        	if(matcher1.find()){
		        		HttpGet httpGet= new HttpGet(matcher1.group(1).replaceAll(" ","%20"));
		        		httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		        		httpGet.addHeader("Host", "www.ticketsnow.com");		
		        		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		        		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		    			
		        		try{
		    				response = httpClient.execute(httpPost);
		    				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		    				//ensure it is fully consumed
		    				if(entity != null){
		    					content=EntityUtils.toString(entity).replaceAll("\\s+", " ");
		    					EntityUtils.consumeQuietly(entity);
		    				}
		    			}catch(Exception e){
		    				e.printStackTrace();
		    			}finally{
		    				//to reuse connection we need to close response stream associated with that
		    				if(response != null)
		    					response.close();
		    				
		    				if(null != httpGet){
		    					httpGet.releaseConnection();
		    				}
		    			}
		    			
		        	}
		        	
		        }else{
		        	return null;
		        }
			}
//			else{
				Pattern pattern = Pattern.compile("eventsVenueGridData(.*?)</tr>");
				Matcher matcher = pattern.matcher(content);
				while(matcher.find()){
					Pattern urlPattern = Pattern.compile("href='(.*?)'");
					Matcher urlMatcher = urlPattern.matcher(matcher.group());
					
					Pattern eventNamePattern = Pattern.compile("span (.*?)</tr>");
					Matcher eventNameMatcher = eventNamePattern.matcher(matcher.group());
					
					Pattern eventDatePattern = Pattern.compile("eventsVenueGridData(.*?)<");
					Matcher eventDateMatcher = eventDatePattern.matcher(matcher.group());
					
					String url="";
					String eventName="";
					String eventDate="";
					
					if(urlMatcher.find()){
						url=urlMatcher.group();
					}
					if(eventNameMatcher.find()){
						Pattern subEventNamePattern = Pattern.compile(">(.*?)<");
						Matcher subEventNameMatcher = subEventNamePattern.matcher(eventNameMatcher.group());
						if(subEventNameMatcher.find()){
							eventName = subEventNameMatcher.group();
						}
					}
					int i=0;
					while(eventDateMatcher.find()){
						if(i==0){
							i++;
							continue;
						}
						Pattern subEventDatePattern = Pattern.compile(">(.*?)<");
						Matcher subEventDateMatcher = subEventDatePattern.matcher(eventDateMatcher.group());
						if(subEventDateMatcher.find()){
							eventDate = subEventDateMatcher.group();
						}
					}
					url = url.replaceAll("href=", "").replaceAll("'", "");
					//System.out.println(url);
					eventName=eventName.replaceAll("<", "").replaceAll(">", "");
					eventDate=eventDate.replaceAll("<", "").replaceAll(">", "");
					DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
					//System.out.println(eventDate);
					
					Date fromEventDate = null,toEventDate = null;
					if(eventDate ==null || eventDate.trim().equals("")) {
						Pattern fromToDatePattern = Pattern.compile("http(.*?)&fdt=(.*)&tdt=(.*)&");
			        	Matcher dateUrlMatcher = fromToDatePattern.matcher(url);
			        	while(dateUrlMatcher.find()){
			        		fromEventDate = dateTimeFmt.parse(dateUrlMatcher.group(2));
			        		toEventDate = dateTimeFmt.parse(dateUrlMatcher.group(3));
			        	}
					} else {
						String fromEventDateString = eventDate.split("-")[0].trim();
						String toEventDateString = eventDate.split("-")[1].trim();
						fromEventDate = dateFormat.parse(fromEventDateString);
						toEventDate = dateFormat.parse(toEventDateString);
					}
					if(fromEventDate == null || toEventDate == null) {
						continue;
					}
					
					for(Date dt=fromEventDate;dt.before(toEventDate)||dt.equals(toEventDate);){
						List<String> eventNameList = eventDateMap.get(df.format(dt));
						if(eventNameList!=null && !eventNameList.isEmpty()){
							for(String dbEventName:eventNameList){
								if(TextUtil.isKeywordsMatch(dbEventName, eventName)){
									validURLs.add(url);
								}
							}
						}
						Calendar cal= Calendar.getInstance();
						cal.setTime(dt);
						cal.add(Calendar.DATE, 1);
						dt=cal.getTime();
					}
				}
//			}
//			return null;
		}else{
			/*for(String keyword : keywords.split(" ")){
				Collection<EventHit> tempHits = eiMarketPlaceEventListingFetcher.getEventList(keyword, location, fromDate, toDate);
				for(EventHit eHit : tempHits){
					if(!eventIds.contains(eHit.getEventId())) {
						eimpEventHits.add(eHit);
						eventIds.add(eHit.getEventId());
					}
				}
			}
			*/
			
			
			String urlKeyword=keywords.replaceAll(" ", "+");
			//String ticketsNowUrl="http://www.ticketsnow.com/Search/SearchResults.aspx?STY=SS&WHAT=justin+bieber";
			String ticketsNowUrl="http://www.ticketsnow.com/Search/SearchResults.aspx?STY=SS&WHAT="+ urlKeyword;
			//String ticketsNowUrl="http://www.ticketsnow.com/resaleorder/justin-bieber-tickets/event/20510?EID=20510";//+ urlKeyword;
			HttpGet httpPost= new HttpGet(ticketsNowUrl);
			httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPost.addHeader("Host", "www.ticketsnow.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpClient= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
			
			String content = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpPost);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content=EntityUtils.toString(entity);
					//System.out.println(content);
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
				
				if(null != httpPost){
					httpPost.releaseConnection();
				}
			}
			
			/*Pattern patern= Pattern.compile("href='(.*?)'");
			
			Matcher matcher=patern.matcher(content);
	        boolean isValidUrl=true;
	       
	        while(matcher.find()){
	        	isValidUrl=true;
	        	String tempUrl = matcher.group(0);
	        	System.out.println(tempUrl);
	        	if(tempUrl.contains("EventsList")) {
	        		if(tempUrl.contains("//EventsList")) {
	        			tempUrl = tempUrl.replace("//EventsList", "/EventList/EventsList");
	        		}
	        	} else if(!tempUrl.contains("cat=Event")){
	        		continue;
	        	}
	        	for(String temp:keyword){
		        	if(!matcher.group(0).toLowerCase().contains((temp.toLowerCase()))){
		        		isValidUrl=false;
		        		break;
		        	}
	        	}
	        	if(isValidUrl){
	        		validURLs.add(matcher.group(0));
	        	}
	        }*/
			//Pattern patern= Pattern.compile("href='(.*?)'");
			Pattern patern= Pattern.compile("<a  name=.*?href='(.*?)'.*?<Div.*?>(.*?)</div>");
			
			Matcher matcher=patern.matcher(content);
	        boolean isValidUrl=true;
	       
	        while(matcher.find()){
	        	isValidUrl=true;
	        	String tempQry = matcher.group(0);
	        	String tempUrl = matcher.group(1);
	        	String displayName = matcher.group(2).trim();
	        	String tempName = displayName.replaceFirst(":", "-");
	        	//System.out.println(tempName+" "+tempUrl);
	        	if(tempUrl.contains("EventsList")) {
	        		//System.out.println(tempUrl);
	        		if(tempUrl.contains("//EventsList")) {
	        			tempUrl = tempUrl.replace("//EventsList", "/EventList/EventsList");
	
	        			Pattern paternUrl= Pattern.compile("EID=(.*)");
	        			Matcher matcherUrl=paternUrl.matcher(tempUrl);
	        	        if(matcherUrl.find()){
	        	        	String tempId=matcherUrl.group(1);
	        	        	//String tempId1=matcherUrl.group(1);
	        	        	tempUrl = "http://www.ticketsnow.com/resaleorder/"+tempName.toLowerCase().replace(" ", "-")+"-tickets/event/"+tempId+"?EID="+tempId+"&cat=Event&searchpath=1";
	        	        }
	        		}
	        	} else if(!tempUrl.contains("cat=Event")){
	        		continue;
	        	} else if(tempUrl.contains("tickets/?")) {
	        		try {
		        		Pattern paternUrl= Pattern.compile("lid=e(.*)&lpos");
	        			Matcher matcherUrl=paternUrl.matcher(tempQry);
	        	        if(matcherUrl.find()){
	        	        	String tempId=matcherUrl.group(1);
	        	        	//String tempId1=matcherUrl.group(1);
	        	        	Integer tempSearchId = Integer.parseInt(tempId);
	        	        	tempUrl = "http://www.ticketsnow.com/resaleorder/"+tempName.toLowerCase().replace(" ", "-")+"-tickets/event/"+tempSearchId+"?EID="+tempSearchId+"&cat=Event";
	        	        }
	        		}catch (Exception e) {
						// TODO: handle exception
					}
	        	}
	        	for(String temp:keyword){
		        	if(!displayName.toLowerCase().contains((temp.toLowerCase()))){
		        		isValidUrl=false;
		        		break;
		        	}
	        	}
	        	if(isValidUrl){
	        		validURLs.add(tempUrl);
	        	}
	        }
		}
        Iterator<String> it =validURLs.iterator();
        DateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");
       
        String fromDateString= inputFormat.format(fromDate);
        String toDateString= inputFormat.format(toDate);
        HttpClient client=null;
        Collection<EventHit> eventHits = new ArrayList<EventHit>();
//        Pattern eventURLPattern=null;
//		Matcher eventURLMatcher=null;
		
		
		//String eventId="";
		//String date="";
		Collection<Integer> eventIds = new ArrayList<Integer>();
		SimpleDateFormat eventDateDTF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		
        while(it.hasNext()){
        	String url=it.next();
        	//System.out.println(url);
        	
        	Pattern urlPattern=Pattern.compile("http(.*?)&");
        	Matcher urlMatcher=urlPattern.matcher(url);
        	while(urlMatcher.find()){
        		if(!isVenue){
        			url=urlMatcher.group(0)+"fdt="+fromDateString +"&tdt="+toDateString+"&maid=0&searchpath=1";
        		}
        		url=url.replaceAll(" ", "%20");
        		
        		CloseableHttpResponse response= null;//client.execute(post);
        		HttpEntity ticeketsnowEntity = null;
        		String finalContent= null;
        		String finalURL = null;
        		JSONArray jsonArray = null;
        		
        		//client= HttpClientStore.createHttpClient(Site.TICKETS_NOW);
        		ChangeProxy.changeLuminatiProxy(httpClient,false);
				httpClient = new SimpleHttpClient(Site.TICKETS_NOW);
				httpClient.assignHttpClient();
        		int j=0;
    			while(j<3) {
    	        	 urlPattern=Pattern.compile("http(.*?)&");
    	        	 urlMatcher=urlPattern.matcher(url);
    	        	while(urlMatcher.find()){
    	        		if(!isVenue){
    	        			url=urlMatcher.group(0)+"fdt="+fromDateString +"&tdt="+toDateString+"&maid=0&searchpath=1";
    	        		}
    	        		url=url.replaceAll(" ", "%20");
    	        	}
    				HttpPost post= new HttpPost(url);
            		post.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
            		post.addHeader("Host", "www.ticketsnow.com");
            		
            		post.addHeader("Accept", "application/json, text/javascript, */*");
            		post.addHeader("Accept-Encoding", "gzip,deflate");
            		
            		j++;
            		try {
	            		response = httpClient.execute(post);
	        			ticeketsnowEntity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
	        			//ensure it is fully consumed
	        			if(ticeketsnowEntity != null){
	        				EntityUtils.consumeQuietly(ticeketsnowEntity);
	        				
	        				finalContent = EntityUtils.toString(ticeketsnowEntity);
            				//System.out.println(finalContent);
	        				
	        				Header[] headers = response.getHeaders("Location");
	        				if(headers.length > 0) {
	        					url = headers[0].getValue();
	        				}
	        				int len = "tmr.eventlist.completeData  =".length();
	        				int start = finalContent.indexOf("tmr.eventlist.completeData  =");
	        				int end = finalContent.indexOf("}];", (start+len));
	        				
	        				if (start <0 || end < 0) {
	        					continue;
	        				} else {
	        					
	        					String jsonContent = finalContent.substring((start+len), end);
		        				jsonContent = jsonContent +"}]";
		        				//System.out.println(jsonContent);		
		        				jsonArray = new JSONArray(jsonContent);
		        				break;
	        				}
	        			}
            			
            		} catch (Exception e) {
            			e.printStackTrace();
					}
            		finally {
            			response.close();
            			if(null != post){
        					post.releaseConnection();
        				}
            		}
    			}
    			if(jsonArray != null) {
	    			for (int i = 0 ; i < jsonArray.length() ; i++) {
						if  (jsonArray.get(i).equals(JSONObject.NULL)) {
							break;
						}
						JSONObject eventJSONObject = (JSONObject)jsonArray.get(i);
						//System.out.println(eventJSONObject.toString());
						
	
						String timeStr ="";
						String dateStr = "";
						String month = ""; 
						String eventName = "";
						String venueLocation = "";
						String venueName = "";
						String eventCity = "";
						String eventState = "";
						//eventsCount++;
						
						eventName = eventJSONObject.getString("EventName");
						
						Integer eventId = null;
						String eventIdStr = eventJSONObject.getString("ProductionId");
						if(eventIdStr!=null && eventIdStr.trim().length()>0) {
							eventId= Integer.parseInt(eventIdStr);
						} else {
							continue;
						}
						
						venueName = eventJSONObject.getString("VenueName");
						String tempLocation =  eventJSONObject.getString("VenueLocation");
						String[] locationArr = tempLocation.split(",");
						if(locationArr.length == 2) {
							eventCity = locationArr[0];
							eventState = locationArr[1];
						} else {
							eventCity = tempLocation;
						}
						
						url = eventJSONObject.getString("SeoBuyUrl");
						if(url == null || url.equals("")) {
							continue;
						}
						if(!url.toLowerCase().contains("http")) {
							url = "http:"+url;	
						}
						
	
						String eventFullLocation = venueName.trim() + ", " + eventCity.trim() + ", " + eventState.trim();
						
						Date eventDate = null;
						java.sql.Date date = null;
						Time time = null;
						String eventDateTimeStr = eventJSONObject.getString("EventDate");
						if(eventDateTimeStr != null && eventDateTimeStr.trim().length()>0) {
							eventDate = eventDateDTF.parse(eventDateTimeStr);
							date = new java.sql.Date(eventDate.getTime());
							time = new Time(eventDate.getTime());
							dateStr = df.format(date)+" "+time;
						}
						
						/*if(date != null) {
							if(fromDate != null && date.before(fromDate)) {
								System.out.println("Before EventDate : "+eventName +" @ " + eventFullLocation + " on " + dateStr);
								continue;
							}
							if(toDate != null && date.after(toDate)) {
								System.out.println("After EventDate : "+eventName +" @ " + eventFullLocation + " on " + dateStr);
								continue;
							}
						}*/
						
						//System.out.println(eventName +" @ " + eventFullLocation + " on " + dateStr);
						
						// location filter
						if (location != null) {
							if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {
								continue;
							}
						}
						
						boolean flag=false;
						dateStr = df.format(eventDate);
						if(!dateMap.containsKey(dateStr)){
							for(String key:locationsMap.keySet()){
								String tokens[] = key.split(":-:");
								String building = tokens[0].trim();
								String cityToken = tokens[1].trim();
								String stateToken = tokens[2].trim();
								
								if( cityToken.equalsIgnoreCase(eventCity.trim())&& stateToken.equalsIgnoreCase(eventState.trim()) && (TextUtil.isSimilarVenue(building, venueName, 1))){
									flag=true;
									break;
								}
							}
						}else{
							flag=true;
						}
						
						if(flag && !eventIds.contains(eventId)){	
							//System.out.println("TNOW SPLITWORDS: " + eventName);
							EventHit eventHit = new EventHit(eventId+"", eventName, date, time, eventFullLocation, Site.TICKETS_NOW, url);
							eventHits.add(eventHit);
							eventIds.add(eventId);
						}
					
					}
	    			//return eventHits;
    			}
    			
        	}
        }
        logger.info("Event SEARCH TNOW : "+(new Date().getTime()-startTime.getTime())+" : key: "+keywords+" : loc: "+location);
        
        return eventHits;		
	}
	
	/*public void setEiMarketPlaceEventListingFetcher(EventListingFetcher eiMarketPlaceEventListingFetcher) {
		this.eiMarketPlaceEventListingFetcher = eiMarketPlaceEventListingFetcher;
	}*/
	
	public static void main(String[] args) throws Exception {
		TicketsNowEventListingFetcher fetcher = new TicketsNowEventListingFetcher();
		Calendar cal1 = new GregorianCalendar(2018, 2, 01);
		Calendar cal2 = new GregorianCalendar(2018, 11, 28);
		List<Event> eventList = new ArrayList<Event>();
		Event e = new Event();
		e.setLocalDate(new Date(new GregorianCalendar(2016, 9, 4).getTimeInMillis()));
		//eventList.add(e);
		
		fetcher.getEventList("The Donna Summer Musical", "", cal1.getTime(), cal2.getTime(),eventList,false,false);
	}
}
