package com.admitone.tmat.eventfetcher;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.maven.surefire.booter.shade.org.codehaus.plexus.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class StubHubApiEventListingFetcher extends EventListingFetcher{

	private static final Logger LOGGER = LoggerFactory.getLogger(StubHubApiEventListingFetcher.class);
	
	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		SimpleHttpClient httpClient = null;
		String stubHubApiUrl = null;
		try{
			
			httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB_API);
			//https://api.stubhub.com/search/catalog/events/v2?title="Justin Bieber"&date=2016-05-04T00:00 TO 2016-05-04T23:59
			if(keywords == null || keywords.isEmpty()){
				throw new RuntimeException("Keywords should not be null");
			}else{
				//Date range should be in this format date=2014-06-01T00:00 TO 2014-06-05T23:59
				SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
				
				//it will replace all your symbols including spaces -> '+' with proper one for URL
				String strQuery = URLEncoder.encode(keywords, "UTF-8");
				stubHubApiUrl = "https://api.stubhub.com/search/catalog/events/v2?title="+strQuery+"&date="+sdfd.format(fromDate)+"TO"+sdfd.format(toDate); ;
			}
			
			Collection<EventHit> eventResults = getEventsFromStubHubApi(stubHubApiUrl, httpClient, keywords, location,
													fromDate, toDate, eventList, isVenue);
			eventHits.addAll(eventResults);
			
		}catch (Exception e) {
			LOGGER.error("Exception raised: " + e);
			e.printStackTrace();
		}finally{
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}
		
		return eventHits;
	}
	
	/**
	 * Method to fetch the events 
	 * from StubHuB API for the query string
	 * @param feedUrl
	 * @param httpClient
	 * @param keywords
	 * @param location
	 * @param fromDate
	 * @param toDate
	 * @param eventList
	 * @param isVenue
	 * @return
	 * @throws IOException 
	 * @throws ClientProtocolException 
	 * @throws JSONException 
	 * @throws ParseException 
	 */
	public Collection<EventHit> getEventsFromStubHubApi(String apiUrl, CloseableHttpClient httpClient, String keywords, 
			String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws ClientProtocolException, IOException, JSONException, ParseException{
		
		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		Map<String, Boolean> locationMap = new HashMap<String, Boolean>();
		Map<String, Boolean> dateMap = new HashMap<String, Boolean>();
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
		
		for(Event event : eventList){
			if(event.getDate() == null){
				dateMap.put("TBD", true);
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}
		

		String encoded64 = new String(Base64.encodeBase64("PStp62MnpjEm7e57clcTwrCxmeYa:wiEwCdnUQff3ZfHkBWj73n1arSAa".getBytes()));
		HttpPost apiLogin = new HttpPost("https://api.stubhub.com/login");
		apiLogin.setHeader("Authorization", "Basic " + encoded64);
		apiLogin.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "password"));
		params.add(new BasicNameValuePair("username", "amit.raut@rightthisway.com"));
		params.add(new BasicNameValuePair("password", "Admit123"));
		
		//send the request body in HttpPost method that contains
		//grant_type,username,password
		apiLogin.setEntity(new UrlEncodedFormEntity(params));

		CloseableHttpResponse response = null;
		String content = null;
		try{
			response = httpClient.execute(apiLogin);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != apiLogin){
				apiLogin.releaseConnection();
			}
		}	
		
		//Get the json returned after successful login 
		//json response as token_type,access_token,expires_in,refresh_token
		JSONObject jsonAccessObject = new JSONObject(content);
		
		//access token expiration period is six months 
		String accessToken = jsonAccessObject.getString("access_token");
		//once the access token has been expired we can use the refresh token to regenerate
		String refreshToken = jsonAccessObject.getString("refresh_token");
		
		//Hit the api url to look for event info		
		String restUrl = URLEncoder.encode("You url parameter value", "UTF-8");
		HttpGet httpGet = new HttpGet(apiUrl);
		httpGet.setHeader("Authorization", "Bearer " + accessToken);
		httpGet.addHeader("Accept", "application/json");
		
		try{
			response = httpClient.execute(httpGet);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != httpGet){
				httpGet.releaseConnection();
			}
		}	
		
		LOGGER.info("Response content for Search Event API: " +content);
		//System.out.println("Response content :: " +content);
		
		JSONObject jsonObject = new JSONObject(content);
		if(jsonObject.getJSONArray("events") != null){
			System.out.println("Response content :: " +content);
			JSONArray jsonArray = jsonObject.getJSONArray("events");
	
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject docJsonObject = jsonArray.getJSONObject(i);
				
				String eventId = docJsonObject.getString("id");
				//String eventName = docJsonObject.getString("title");
				String queryUrl = docJsonObject.getString("eventInfoUrl");
				String eventDateObj = docJsonObject.getString("dateLocal");
				JSONObject venueObj = docJsonObject.getJSONObject("venue");
				String venueName = venueObj.getString("name");
				String city = venueObj.getString("city");
				String state = venueObj.getString("state");
				String eventLocation = venueName+", "+city+", "+state;		
				System.out.println("=====" + eventLocation);
				String eventName = null;
				String jsonStr = null;
				if(docJsonObject.has("attributes")){
					jsonStr = docJsonObject.getString("attributes");
					if(jsonStr != null){
						JSONArray jsonarray = new JSONArray(jsonStr);
						for (int j = 0; j < jsonarray.length(); j++) {
						    JSONObject jsonobject = jsonarray.getJSONObject(j);
						    String name = jsonobject.getString("name");
						    if(name.equals("act_primary")){
						    	eventName = jsonobject.getString("value");
						    }
						}
					}
				}
				
				SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
				String eventDateStr = sdfd.format(sdfd.parse(eventDateObj));
				
				Date eventDate = sdfd.parse(eventDateStr);
				java.sql.Date date = new java.sql.Date(eventDate.getTime());
				
				String timeStr = eventDateStr.substring(eventDateStr.lastIndexOf("T")+1);
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
				long ms = sdf.parse(timeStr).getTime();
				Time eventTime = new Time(ms);
				
				if(!eventIds.contains(eventId)){	
					System.out.println("SHELF SPLITWORDS: " + eventName);
					EventHit eventHit = new EventHit(eventId, eventName, date, eventTime, eventLocation, Site.STUB_HUB_API, queryUrl);
					eventHits.add(eventHit);
					eventIds.add(eventId);
				}
				
			}
		}
		
		return eventHits;
	}
	
	//Unit testing method
	public static void main(String[] args) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		
		TimeZone timeZone = TimeZone.getDefault();
		Calendar cal = Calendar.getInstance(timeZone);
		SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		sdfd.setCalendar(cal);
		
		Date fromDate = sdfd.parse("2016-05-13T00:00");
		Date toDate = sdfd.parse("2016-05-13T23:59");
		Event event = new Event();
		event.setLocalDate(format.parse("05/04/2016"));
		List<Event> eventList = new ArrayList<Event>();
		eventList.add(event);
		Collection<EventHit> eventHits = new StubHubApiEventListingFetcher().getEventList("Luke Bryan", null, fromDate, toDate, eventList, false, false);
		System.out.println("===Event Hits from stubhub API ::" +eventHits);
		
	}
}
