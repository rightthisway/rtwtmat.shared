package com.admitone.tmat.eventfetcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Event listing fetcher for TickPIck exchanges
 * 
 * @author dthiyagarajan
 *
 */
public class TickPickEventListingFetcher extends EventListingFetcher {

	private static Logger logger = LoggerFactory.getLogger(TickPickEventListingFetcher.class);

	@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,
			List<Event> eventList, boolean isVenue, boolean isStubhubFromTicketEvolution) throws Exception {

		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		// Replace space to Hyphen for tick pick events
		keywords = keywords.replaceAll("\\s+", "-");
		SimpleHttpClient httpClient = null;
		try {
			httpClient = HttpClientStore.createHttpClient(Site.TICK_PICK);
			// String tickPickFeedUrl =
			// "https://www.tickpick.com/search/"+keywords+"/";
			String tickPickFeedUrl = "https://www.tickpick.com/" + keywords + "-tickets/";

			Collection<EventHit> eventResults = getEventsFromTickPick(tickPickFeedUrl, httpClient, keywords, location,
					fromDate, toDate, eventList, isVenue);
			/*
			 * for(EventHit h : eventResults){ System.out.println("Events :: "
			 * +h.getName()+" "+h.getEventId()+" "+h.getSiteId()+" "+h.getUrl()
			 * +" "+h.getLocation()+" "+h.getTime()); }
			 */
			eventHits.addAll(eventResults);

		} catch (Exception e) {
			logger.error("SHELF Caught Exception: " + e);
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}

		return eventHits;
	}

	/**
	 * Fetch events from tick pick exchange
	 * 
	 * @param feedUrl
	 * @param httpClient
	 * @param keywords
	 * @param location
	 * @param fromDate
	 * @param toDate
	 * @param eventList
	 * @param isVenue
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws ParseException
	 */
	public Collection<EventHit> getEventsFromTickPick(String feedUrl, CloseableHttpClient httpClient, String keywords,
			String location, Date fromDate, Date toDate, List<Event> eventList, boolean isVenue)
			throws ClientProtocolException, IOException, ParseException {

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");

		Map<String, Boolean> locationMap = new HashMap<String, Boolean>();
		Map<String, Boolean> dateMap = new HashMap<String, Boolean>();
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();

		for (Event event : eventList) {
			if (event.getDate() == null) {
				dateMap.put("TBD", true);
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:"
						+ event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:"
						+ event.getVenue().getState().replaceAll("\\s+", " "), true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}

		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();		
		HttpGet httpPost = new HttpGet(feedUrl);
		

		/*
		 * httpPost.setHeader("User-Agent",
		 * "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5"
		 * ); httpPost.addHeader("Host", "www.tickpick.com");
		 * httpPost.addHeader("Accept",b
		 * "application/json, text/javascript, text/html, application/xhtml+xml, application/xml,*"
		 * ); httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		 */

		// Hit the tick pick site and get the response
		String tickPickResult = null;
		CloseableHttpResponse response = null;
		try {
			/*response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			
			if (entity != null) {
				tickPickResult = EntityUtils.toString(entity).trim();
				EntityUtils.consumeQuietly(entity);
			}*/
			tickPickResult = getSource(feedUrl);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// to reuse connection we need to close response stream associated
			// with that
			if (response != null)
				response.close();

			if (null != httpPost) {
				httpPost.releaseConnection();
			}
		}

		// For testing puppose
		
		/* File tickPIckRes = new File("C:/Lloyd/tickpickresult.html");
		 FileUtils.writeStringToFile(tickPIckRes, tickPickResult);
		 tickPickResult = FileUtils.readFileToString(tickPIckRes);*/
		 

		Document doc = Jsoup.parse(tickPickResult);
		int i = 0;
		for (Element ticResultDivs : doc.select("div.srItem")) {			
			i++;
			try {
			List<Node> childNodeList = ticResultDivs.childNodes();
			Node nd = childNodeList.get(0);

			Node childNode = nd.childNode(0);
			JSONObject jsonObj = null;

			
				JSONParser parser = new JSONParser();
				jsonObj = (JSONObject) parser.parse(childNode.toString());				
				Map offers = ((Map) jsonObj.get("offers"));				
				String price = (String) offers.get("lowPrice");				
				String ticketFetchUrl = (String) offers.get("url");
				String[] arry = ticketFetchUrl.split("/");
				String eventId = arry[arry.length - 1];
				String eventName = (String) jsonObj.get("name");

				Map locationMapinDoc = ((Map) jsonObj.get("location"));
				Map addressMap = (Map) locationMapinDoc.get("address");
				String addressCountry = (String) addressMap.get("addressCountry");
				String addressRegion = (String) addressMap.get("addressRegion");
				String addressLocality = (String) addressMap.get("addressLocality");
				String locationName = (String) locationMapinDoc.get("name");
				String eventFullLocation = locationName.trim() + ", " + addressLocality.trim() + ", "
						+ addressRegion.trim();

				String eventStartDate = (String) jsonObj.get("startDate");
				TimeZone timeZone = TimeZone.getDefault();
				Calendar cal = Calendar.getInstance(timeZone);
				SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				sdfd.setCalendar(cal);
				cal.setTime(sdfd.parse(eventStartDate));
				Date tickPickDates = cal.getTime();
				Date eventDate = format.parse(eventStartDate);
				java.sql.Date date = new java.sql.Date(eventDate.getTime());				
				Date date1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(eventStartDate);
				String newDateStr = new SimpleDateFormat("yyyy-MM-dd, hh:mm a").format(date1);
				String time[] = newDateStr.split(",");
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
				long ms = sdf.parse(time[1]).getTime();
				Time t = new Time(ms);

				/*System.out.println(" [ PARSED DETAILS FOR  eventId, eventName, date, t, eventFullLocation, ticketFetchUrl "
						+ eventId + ", " + eventName + ", " + date + ", " + t + ", " + eventFullLocation  + ", " + ticketFetchUrl);*/

				if (!(tickPickDates.after(fromDate) && tickPickDates.before(toDate))) {

					/*System.out.println(
							" [ Skipping as  Dates not between From and To ] " + fromDate + "  & "  + toDate);*/
					continue;
				}
				if (location != null) {
					if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {

						/*System.out.println(" [Skipping as Location  ] " + location + "  does not match expected  "
								+ eventFullLocation);*/
						continue;
					}
				}
				if (!eventIds.contains(eventId)) {
					/*System.out.println(" ------ Creating Events --- eventId, eventName, date, t, eventFullLocation, ticketFetchUrl "
							+ eventId + ", " + eventName + ", " + date + ", " + t + ", " + eventFullLocation + ", " + ticketFetchUrl);*/
					EventHit eventHit = new EventHit(eventId, eventName, date, t, eventFullLocation, Site.TICK_PICK,
							ticketFetchUrl);
					eventHits.add(eventHit);
					eventIds.add(eventId);
				}
			} catch (Exception ex) {
				System.out.println(
						"[Exception] [Tick Pick Event Parser ] parsing event details from  " + Site.TICK_PICK + ex);

			}

		}

		return eventHits;

		/*
		 * 
		 * for(Element tableTr : divTable.select("tr")){
		 * 
		 * String eventDateStr = ""; String eventName = ""; String eventLoc =
		 * ""; String eventId = ""; String timeStr = ""; String ticketFetchUrl =
		 * "";
		 * 
		 * //Fetch the tds tags in a tr Elements tds = tableTr.select("td");
		 * 
		 * //Time str timeStr = tds.get(0).select("span.dateDisplay3").text();
		 * 
		 * //Event date Element eventDates =
		 * tds.get(0).select("span[itemprop=startDate]").first(); eventDateStr =
		 * eventDates.attr("content");
		 * 
		 * TimeZone timeZone = TimeZone.getDefault(); Calendar cal =
		 * Calendar.getInstance(timeZone); SimpleDateFormat sdfd = new
		 * SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSSSSS");
		 * sdfd.setCalendar(cal); cal.setTime(sdfd.parse(eventDateStr)); Date
		 * tickPickDates = cal.getTime();
		 * 
		 * Date eventDate = format.parse(eventDateStr); java.sql.Date date = new
		 * java.sql.Date(eventDate.getTime()); System.out.println("from date-"
		 * +fromDate); System.out.println("To date-"+toDate);
		 * 
		 * if(tickPickDates.after(fromDate) && tickPickDates.before(toDate)){
		 * System.out.println("Matched with the exact event date "
		 * +tickPickDates);
		 * 
		 * //Event name for the matched date eventName =
		 * tds.get(1).select("span[itemprop=performer]").text();
		 * 
		 * //Event location for the matched date eventLoc =
		 * tds.get(1).select("a[itemprop=location]").text(); eventLoc =
		 * eventLoc.replace(" - ", ", ");
		 * 
		 * //Event ticket fetching url for the matched date Element ticUrl =
		 * tds.get(1).select("[itemprop=url]").first(); ticketFetchUrl =
		 * ticUrl.attr("href"); String append = "https://www.tickpick.com";
		 * ticketFetchUrl = append+ticketFetchUrl; int
		 * index=ticketFetchUrl.lastIndexOf('/'); eventId =
		 * ticketFetchUrl.substring(0,index); eventId =
		 * eventId.substring(eventId.lastIndexOf("/")+1); //Location filter if
		 * (location != null) { if
		 * (eventLoc.toLowerCase().indexOf(location.toLowerCase()) < 0) {
		 * continue; } } SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
		 * long ms = sdf.parse(timeStr).getTime(); Time t = new Time(ms);
		 * 
		 * if(!eventIds.contains(eventId)){ System.out.println(
		 * "SHELF SPLITWORDS: " + eventName); EventHit eventHit = new
		 * EventHit(eventId, eventName, date, t, eventLoc, Site.TICK_PICK,
		 * ticketFetchUrl); eventHits.add(eventHit); eventIds.add(eventId); } }
		 * }
		 */
		// }

	}
	
	 public static HttpClient wrapClient(HttpClient base) {
	        try {
	            SSLContext ctx = SSLContext.getInstance("TLS");
	            X509TrustManager tm = new X509TrustManager() {

	                public void checkClientTrusted(X509Certificate[] xcs,
	                                               String string) throws CertificateException {
	                }
	                

	                public void checkServerTrusted(X509Certificate[] xcs,
	                                               String string) throws CertificateException {
	                }

	                public X509Certificate[] getAcceptedIssuers() {
	                    return null;
	                }
	            };
	            ctx.init(null, new TrustManager[] { tm }, null);
	            
	            @SuppressWarnings("deprecation")
	            SSLConnectionSocketFactory ssf1 = new SSLConnectionSocketFactory(ctx);
				SSLSocketFactory ssf = new SSLSocketFactory(ctx);
	            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	            ClientConnectionManager ccm = base.getConnectionManager();
	            SchemeRegistry sr = ccm.getSchemeRegistry();
	            sr.register(new Scheme("https", ssf, 443));
	            return new DefaultHttpClient(ccm, base.getParams());
	        } catch (Exception ex) {
	            ex.printStackTrace();
	            return null;
	        }
	    }
	
	
	  public static String getSource(String url) throws Exception {
	        String retValue = "";
	        HttpClient httpclient = new DefaultHttpClient();
	        wrapClient(httpclient);
	        try {
	            HttpGet httpget = new HttpGet(url);	           
	            HttpResponse response = httpclient.execute(httpget);	           	           
	            HttpEntity entity = response.getEntity();
	            if (entity != null) {
	                InputStream instream = entity.getContent();
	                try {
	                    retValue = getStringFromInputStream(instream);

	                } catch (RuntimeException ex) {
	                    httpget.abort();
	                    throw ex;
	                } finally {	                   
	                    try {
	                        instream.close();
	                    } catch (Exception e) {
	                    }
	                }
	            }

	        } finally {
	            httpclient.getConnectionManager().shutdown();
	        }
	        return retValue;
	    }

	    private static String getStringFromInputStream(InputStream is) {

	        BufferedReader br = null;
	        StringBuilder sb = new StringBuilder();
	        String line;
	        try {
	            br = new BufferedReader(new InputStreamReader(is));
	            while ((line = br.readLine()) != null) {
	                sb.append(line);
	            }

	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	        return sb.toString();
	    }
	

	// test
	/*public static void main(String[] args) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date fromDate = format.parse("05/23/2018");
		Date toDate = format.parse("05/24/2019");
		Event event = new Event();
		event.setLocalDate(format.parse("04/25/2018"));
		List<Event> eventList = new ArrayList<Event>();
		eventList.add(event);
		Collection<EventHit> hits = new TickPickEventListingFetcher().getEventList("Madison Square Garden", null, fromDate, toDate,
				eventList, false, false);
		
		System.out.println(hits.toString());
	}*/
}
