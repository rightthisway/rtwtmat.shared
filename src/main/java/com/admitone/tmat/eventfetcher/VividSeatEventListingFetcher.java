package com.admitone.tmat.eventfetcher;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class VividSeatEventListingFetcher extends EventListingFetcher {
	private static Logger logger = LoggerFactory.getLogger(VividSeatEventListingFetcher.class);
	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {
		
		
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		keywords = keywords.replaceAll("\\s+", "+");
		SimpleHttpClient httpClient = null;
//		for(String keyword : keywords.split(" ")) {
		Date start = new Date();
			try {
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
				String from = format.format(fromDate);
				String to = format.format(toTomorrowDate);
//				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				/*******/
			
				
				httpClient = HttpClientStore.createHttpClient(Site.VIVIDSEAT);
				String feedUrl = "";
//				String searchTerm="";
//				String dateFrom="";
//				String dateTo="";
//				String url 
		//		feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active+maxPrice&wt=json";
//				if(!isVenue){
						   //https://www.vividseats.com/widget/Productions.action?showPagedSearchResultsView&event.eventId=&searchTerm=Judas+Priest&dateFrom=03/02/2018&dateThru=04/06/2018&hideRegionFilter=false&useScriptTicketsButtons=false&bRegex=false&bRegex_0=false&bRegex_1=false&bRegex_10=false&bRegex_11=false&bRegex_12=true&bRegex_13=true&bRegex_14=true&bRegex_15=true&bRegex_16=false&bRegex_17=false&bRegex_2=false&bRegex_3=false&bRegex_4=false&bRegex_5=true&bRegex_6=false&bRegex_7=false&bRegex_8=false&bRegex_9=false&bSearchable_0=true&bSearchable_1=true&bSearchable_10=true&bSearchable_11=true&bSearchable_12=true&bSearchable_13=true&bSearchable_14=true&bSearchable_15=true&bSearchable_16=true&bSearchable_17=true&bSearchable_2=true&bSearchable_3=true&bSearchable_4=true&bSearchable_5=true&bSearchable_6=true&bSearchable_7=true&bSearchable_8=true&bSearchable_9=true&bSortable_0=true&bSortable_1=false&bSortable_10=true&bSortable_11=true&bSortable_12=true&bSortable_13=true&bSortable_14=true&bSortable_15=true&bSortable_16=true&bSortable_17=true&bSortable_2=false&bSortable_3=true&bSortable_4=true&bSortable_5=true&bSortable_6=true&bSortable_7=true&bSortable_8=true&bSortable_9=true&iColumns=18&iDisplayLength=400&iDisplayStart=0&iSortCol_0=10&iSortCol_1=0&iSortingCols=2&mDataProp_0=0&mDataProp_1=1&mDataProp_10=10&mDataProp_11=11&mDataProp_12=12&mDataProp_13=13&mDataProp_14=14&mDataProp_15=15&mDataProp_16=16&mDataProp_17=17&mDataProp_2=2&mDataProp_3=3&mDataProp_4=4&mDataProp_5=5&mDataProp_6=6&mDataProp_7=7&mDataProp_8=8&mDataProp_9=9&sColumns=&sEcho=2&sSearch=&sSearch_0=&sSearch_1=&sSearch_10=&sSearch_11=&sSearch_12=&sSearch_13=&sSearch_14=&sSearch_15=&sSearch_16=&sSearch_17=&sSearch_2=&sSearch_3=&sSearch_4=&sSearch_5=&sSearch_6=&sSearch_7=&sSearch_8=&sSearch_9=&sSortDir_0=desc&sSortDir_1=asc
					feedUrl =  "https://www.vividseats.com/widget/Productions.action?showPagedSearchResultsView&event.eventId=&searchTerm="+keywords+"&dateFrom="+from+"&dateThru="+to+"&hideRegionFilter=false&useScriptTicketsButtons=false&bRegex=false&bRegex_0=false&bRegex_1=false&bRegex_10=false&bRegex_11=false&bRegex_12=true&bRegex_13=true&bRegex_14=true&bRegex_15=true&bRegex_16=false&bRegex_17=false&bRegex_2=false&bRegex_3=false&bRegex_4=false&bRegex_5=true&bRegex_6=false&bRegex_7=false&bRegex_8=false&bRegex_9=false&bSearchable_0=true&bSearchable_1=true&bSearchable_10=true&bSearchable_11=true&bSearchable_12=true&bSearchable_13=true&bSearchable_14=true&bSearchable_15=true&bSearchable_16=true&bSearchable_17=true&bSearchable_2=true&bSearchable_3=true&bSearchable_4=true&bSearchable_5=true&bSearchable_6=true&bSearchable_7=true&bSearchable_8=true&bSearchable_9=true&bSortable_0=true&bSortable_1=false&bSortable_10=true&bSortable_11=true&bSortable_12=true&bSortable_13=true&bSortable_14=true&bSortable_15=true&bSortable_16=true&bSortable_17=true&bSortable_2=false&bSortable_3=true&bSortable_4=true&bSortable_5=true&bSortable_6=true&bSortable_7=true&bSortable_8=true&bSortable_9=true&iColumns=18&iDisplayLength=400&iDisplayStart=0&iSortCol_0=10&iSortCol_1=0&iSortingCols=2&mDataProp_0=0&mDataProp_1=1&mDataProp_10=10&mDataProp_11=11&mDataProp_12=12&mDataProp_13=13&mDataProp_14=14&mDataProp_15=15&mDataProp_16=16&mDataProp_17=17&mDataProp_2=2&mDataProp_3=3&mDataProp_4=4&mDataProp_5=5&mDataProp_6=6&mDataProp_7=7&mDataProp_8=8&mDataProp_9=9&sColumns=&sEcho=2&sSearch=&sSearch_0=&sSearch_1=&sSearch_10=&sSearch_11=&sSearch_12=&sSearch_13=&sSearch_14=&sSearch_15=&sSearch_16=&sSearch_17=&sSearch_2=&sSearch_3=&sSearch_4=&sSearch_5=&sSearch_6=&sSearch_7=&sSearch_8=&sSearch_9=&sSortDir_0=desc&sSortDir_1=asc";
					/*if (keywords != null && keywords.length() > 0) {
						feedUrl += "%0D%0A%2Bdescription%3A" + keywords.replaceAll("\\s+", "+");			
					}
					
					if (from != null && to != null) {
						feedUrl += "%0D%0A%2Bevent_date%3A[" + from + "T00:00:00Z+TO+" + to + "T23:59:59Z]";
					}
					feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active&rows=50000&wt=json";
					*/
					Collection<EventHit> subResult= getEventsFromExchange(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
					eventHits.addAll(subResult);
				/*}
				else{
					String venueString = keywords;//eventList.get(0).getVenue().getBuilding();
					String venueURL ="http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Avenue%20description%3A";
					venueURL =  venueURL + venueString.replaceAll("\\s+", "+") + "%0D%0A%0D%0A&version=2.2&fl=id+event+description+city+state+active&rows=20&wt=json";
					System.out.println(venueURL);
					HttpPost httpPostVenue = new HttpPost(venueURL);
					
					httpPostVenue.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//					httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
					httpPostVenue.addHeader("Accept", "application/json, text/javascript, * /*");
					httpPostVenue.addHeader("Accept-Encoding", "gzip,deflate");
					HttpResponse venueResponse = httpClient.execute(httpPostVenue);
					HttpEntity venueEntity = HttpEntityHelper.getUncompressedEntity(venueResponse.getEntity());
					JSONObject venueJsonObject = null;
					String venueResult = EntityUtils.toString(venueEntity).trim();
					venueJsonObject = new JSONObject(venueResult);
				
					JSONArray docsJSONVenueArray = venueJsonObject.getJSONObject("response").getJSONArray("docs");
					Set<Integer> venueIdSet= new HashSet<Integer>();
					for(int i=0;i<docsJSONVenueArray.length();i++){
						JSONObject docJSONVenueObject = docsJSONVenueArray.getJSONObject(i);
//						System.out.println(docJSONVenueObject.getString("description").trim().toLowerCase());
						if(keywords.toLowerCase().equals(docJSONVenueObject.getString("description").trim().toLowerCase())){
							venueIdSet.clear();
							venueIdSet.add(docJSONVenueObject.getInt("id"));
							break;
						}
						venueIdSet.add(docJSONVenueObject.getInt("id"));
					}
					for(Integer venueId:venueIdSet){
						feedUrl = "http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent";
						if (keywords != null && keywords.length() > 0) {
							feedUrl += "%0D%0A%2Bgeography_parent%3A" + venueId;			
						}	
						if (from != null && to != null){
							feedUrl += "%0D%0A%2Bevent_date%3A[" + from + "T00:00:00Z+TO+" + to + "T23:59:59Z]";
						}
						feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active&rows=50000&wt=json";
						
						System.out.println("FEED URL=" + feedUrl);
						Collection<EventHit> subResult= getEventsFromExchange(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
						eventHits.addAll(subResult);
					}
					
				}*/
				/*******/
				
				
			} catch (Exception e) {
				System.out.println("SHELF Caught Exception: " + e);
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
			}
//		}
		logger.info("Event SEARCH VIVID : "+(new Date().getTime()-start.getTime())+" : key: "+keywords+" : loc: "+location);
		return eventHits;
	}
	
	public Collection<EventHit> getEventsFromExchange(String feedUrl,CloseableHttpClient httpClient,String keywords,String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws ClientProtocolException, IOException, JSONException, ParseException{
		SimpleDateFormat format = new SimpleDateFormat("MMM d, yyyy hh:mm aa");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMM d, yyyy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
//		Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
//		String from = format.format(fromDate);
//		String to = format.format(toTomorrowDate);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		for(Event event:eventList){
			if(event.getDate()==null){
				dateMap.put("TBD", true);
//				String location = event.getVenue().getBuilding();
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}
		HttpPost httpPost = new HttpPost(feedUrl);
		// System.out.println("stubhub>" + feedUrl);
		httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//		httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
		httpPost.addHeader("Accept", "application/json, text/javascript, */*");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		
//		httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB);
		String result = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				result = EntityUtils.toString(entity).trim();
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != httpPost){
				httpPost.releaseConnection();
			}
		}
		
		JSONObject jsonObject = null;
		
		//System.out.println(result);
		
		String removeOne = "<a href=\"qsvetudxtvszysezsrqsvet.html\" style=\"display: none;\" rel=\"file\" id=\"bvxxxxusqbrswy\">wzstyzqtqqccddv</a>";
		String removeTow = "<span id=\"bvxxxxusqbrswy\"><a rel=\"file\" style=\"display: none;\" href=\"qsvetudxtvszysezsrqsvet.html\">wzstyzqtqqccddv</a></span>";
		String removeThree = "<div style=\"display: none;\"><a href=\"qsvetudxtvszysezsrqsvet.html\" id=\"bvxxxxusqbrswy\" rel=\"file\">wzstyzqtqqccddv</a></div>";
		
		result = result.replaceAll(removeOne, "").replaceAll(removeTow, "").replaceAll(removeThree, "");
		
		
		jsonObject = new JSONObject(result);
	
		JSONArray docsJSONArray = jsonObject.getJSONArray("aaData");			
		
		//System.out.println("SHELF EVENTLISTSIZE: " + docsJSONArray.length());
		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		for (int i = 0 ; i < docsJSONArray.length(); i++) {
			//System.out.println("SHELF LOOP ITERATION: " + docsJSONArray.getString(i).toString());
//			JSONObject docJSONObject = docsJSONArray.getJSONObject(i);
			String eventContent =  docsJSONArray.getString(i).toString();
			Pattern dayPattern = Pattern.compile("productionsDay'>(.*?)<");
			Pattern datePattern = Pattern.compile("productionsDate'>(.*?)<");
			Pattern timePattern = Pattern.compile("productionsTime'>(.*?)<");
			Pattern namePattern = Pattern.compile("<div class='productionsEvent' itemprop='name'>(.*?)<");
			Pattern venuePattern = Pattern.compile("<div class='productionsVenue' itemprop='location' itemscope itemtype='http://schema.org/Place'>(.*?)<\\\\/d");
			Pattern urlPattern = Pattern.compile("href='(.*?)'>");
			Matcher dayMatcher = dayPattern.matcher(eventContent);
			Matcher dateMatcher = datePattern.matcher(eventContent);
			Matcher timeMatcher = timePattern.matcher(eventContent);
			Matcher nameMatcher = namePattern.matcher(eventContent);
			Matcher venueMatcher = venuePattern.matcher(eventContent);
			Matcher urlMatcher = urlPattern.matcher(eventContent);
			String day="";
			String dateStr="";
			String timeStr="";
			String name="";
			String venue="";
			String url="";
			String eventId="";
			if(dayMatcher.find()){
				day = dayMatcher.group(1);
				//System.out.println("day:" + day);
			}
			if(dateMatcher.find()){
				dateStr = dateMatcher.group(1);
				//System.out.println("date:" + dateStr);
			}
			if(timeMatcher.find()){
				timeStr = timeMatcher.group(1);
				//System.out.println("time:" + timeStr);
			}
			if(nameMatcher.find()){
				name = nameMatcher.group(1);
				//System.out.println("name:" + name);
			}
			//System.out.println(eventContent);
			if(venueMatcher.find()){
				venue = venueMatcher.group(1).replaceAll("<span itemprop='name'>", "").
				replaceAll("<span itemprop='address' itemscope itemtype='http://schema.org/PostalAddress'>", "").
				replaceAll("<span itemprop='addressLocality'>", "").replaceAll("<span itemprop='addressRegion'>", "").
				replaceAll(";", "").replaceAll("/", "").replaceAll("\\\\", "").
				replaceAll("span", "").replaceAll("&nbsp", "").replaceAll("<", "").replaceAll(">", "").replaceAll(";", "").replaceAll("/", "").replaceAll("\\\\", "");
				
				//venue = venueMatcher.group(1).replaceAll("span", "").replaceAll("&nbsp", "").replaceAll("<", "").replaceAll(">", "").replaceAll(";", "").replaceAll("/", "").replaceAll("\\\\", "");
				//System.out.println("venue:" + venue);
			}
			if(urlMatcher.find()){
				url = "www.vividseat.com" + urlMatcher.group(1);
				//System.out.println("url:" + url);
			}
			Pattern eventIdPattern = Pattern.compile(".*-(.*?).h");
			Matcher eventIdMatcher = eventIdPattern.matcher(url);
//			int eventIdPos = url.indexOf("productionId=="); 
			if (eventIdMatcher.find()) {
				eventId= eventIdMatcher.group(1);
//				eventId = url.substring(eventIdPos + 13, url.length());
			}
//			String active = docJSONObject.getString("active");
//			//System.out.println("SHELF ACTIVE: " + active);
//			if (active.equals("0")) {
//				continue;
//			}

//				String maxPrice = docJSONObject.getString("maxPrice");
//				if (maxPrice.equals("0.0")) {
//					continue;
//				}

//			String name = docJSONObject.getString("description");
//			String dateStr = docJSONObject.getString("event_date_local");
//			String timeStr = docJSONObject.getString("event_time_local");
//			String[] timeTokens = timeStr.split(":");
			
			String dateTimeStr = (dateStr + " " + timeStr).replaceAll("\\s+", " ");
			Date eventDate =  null;
			
			java.sql.Date date = null;
			if(timeStr != null && !timeStr.isEmpty()) {
				try{
					eventDate = format.parse(dateTimeStr);
				}catch(Exception e){
					dateTimeStr = (dateStr+", 2018 " + timeStr).replaceAll("\\s+", " ");
				}
				date = new java.sql.Date(format.parse(dateTimeStr).getTime());
			} else {
				try{
					eventDate = dateFormat.parse(dateTimeStr);
				}catch(Exception e){
					dateTimeStr = (dateStr+", 2018 ").replaceAll("\\s+", " ");
				}
				date = new java.sql.Date(dateFormat.parse(dateTimeStr).getTime());
			}
			
			
//			String eventLocation = docJSONObject.getString("eventGeoDescription");
			if(!isVenue){
				if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), name)) {
					continue;
				}
			}else{
				if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), venue)) {
					continue;
				}
			}
			
			Time time = null;
			if(timeStr != null && !timeStr.isEmpty()) {
				Date timeS = timeFormat.parse(timeStr);	
				time = new Time(timeS.getTime());
			}
			
//			if (timeTokens.length == 3) {
//				time = new Time(Integer.valueOf(timeTokens[0]), Integer.valueOf(timeTokens[1]), Integer.valueOf(timeTokens[2]));
//			} else {
//				time = null;
//			}
			
			
			
			String eventCity = null;
			String eventLocation = null;
			String eventState = null;
			String[] temp = venue.split(",");
			try {
				eventState = temp[1].trim();
			}  catch (Exception e) {}
			try {
				String []temp2 = temp[0].split("-");
				if(temp2.length>2){
					eventLocation = temp2[0].trim() + " - " + temp2[1].trim();
					eventCity = temp2[2].trim();
				}else{
					eventLocation = temp2[0].trim();
					eventCity = temp2[1].split("-")[0].trim();
				}
			} catch (Exception e) {}
			
			
			
			
//			String eventId = docJSONObject.getString("event_id");
//			String url = "http://www.stubhub.com/?event_id=" + eventId;
			
			String eventFullLocation = eventLocation.trim() + ", " + eventCity.trim() + ", " + eventState.trim();
			
			// location filter
			if (location != null) {
				if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {
					continue;
				}
			}
			boolean flag=false;
			if(!dateMap.containsKey(df.format(date))){
				for(String key:locationMap.keySet()){
					String tokens[] = key.split(":-:");
					String building = tokens[0].trim();
					String cityToken = tokens[1].trim();
					String stateToken = tokens[2].trim();
					
					if( cityToken.equalsIgnoreCase(eventCity.trim())&& stateToken.equalsIgnoreCase(eventState.trim()) && (TextUtil.isSimilarVenue(building, eventLocation, 1))){
						flag=true;
						break;
					}
				}
			}else{
				flag=true;
			}
//			if(dateMap.containsKey(dateFormat.format(eventDate))){
			// check date - stubhub might return irrelevant dates
			/*if (date.before(fromDate) || date.after(toDate)) {
				continue;
			}*/
			
			if(flag && !eventIds.contains(eventId)){	
				System.out.println("SHELF SPLITWORDS: " + name);
				EventHit eventHit = new EventHit(eventId, name, date, time, eventFullLocation, Site.VIVIDSEAT, url);
				eventHits.add(eventHit);
				eventIds.add(eventId);
			}

		}
		return eventHits;
	}
	public static void main(String[] args) throws Exception {
		VividSeatEventListingFetcher fetcher = new VividSeatEventListingFetcher();
		Calendar cal1 = new GregorianCalendar(2018, 0, 2);
		Calendar cal2 = new GregorianCalendar(2018, 7, 5);
		
		fetcher.getEventList("defending the caveman", "", cal1.getTime(), cal2.getTime(),new ArrayList<Event>(),false,false);
		
		
		/*String eventContent = "\\[<span title='1412553600000'><\\/span><div class='productionsDay'>Sunday" +
				"<\\/div><div class='productionsDate'>Oct 5, 2014<\\/div><div class='productionsTime'>7:00 PM<\\/div>\\" +
		",\"<div class='productionsEvent'>One Direction<\\/div><div class='productionsVenue'>Sun Life Stadium - Miami Gardens, FL<\\/div>\",\"<a class='btn btn-primary' href='/concerts/one-direction-tickets/one-direction-sun-life-stadium-10-5-1514601.html'>Tickets<\\/a>\",\"October\",\"One Direction\",\"Away games\",\"Night\",\"Weekends\",\"Miami Gardens\",\"75\",\"<span style='display:none;'>false<\\/span>National Events\",\"One Direction\",\"Sun Life Stadium\",\"Pop\",\"Concerts\",\"\",\"Oct 05, 2014 12:00 AM\",false,\"0.0\"]";
//		String test = "\\temp";
//		System.out.println(test);
		Pattern dayPattern = Pattern.compile("productionsDay'>(.*?)<");
		Pattern datePattern = Pattern.compile("productionsDate'>(.*?)<");
		Pattern timePattern = Pattern.compile("productionsTime'>(.*?)<");
		Pattern namePattern = Pattern.compile("productionsEvent'>(.*?)<");
		Pattern venuePattern = Pattern.compile("productionsVenue'>(.*?)<");
		Pattern urlPattern = Pattern.compile("href='(.*?)'>");
		Matcher dayMatcher = dayPattern.matcher(eventContent);
		Matcher dateMatcher = datePattern.matcher(eventContent);
		Matcher timeMatcher = timePattern.matcher(eventContent);
		Matcher nameMatcher = namePattern.matcher(eventContent);
		Matcher venueMatcher = venuePattern.matcher(eventContent);
		Matcher urlMatcher = urlPattern.matcher(eventContent);
//		String url = ticketListingCrawl.getQueryUrl();
		
		if(dayMatcher.find()){
			System.out.println("day:" + dayMatcher.group(1));
		}
		if(dateMatcher.find()){
			System.out.println("date:" + dateMatcher.group(1));
		}
		if(timeMatcher.find()){
			System.out.println("time:" + timeMatcher.group(1));
		}
		if(nameMatcher.find()){
			System.out.println("name:" + nameMatcher.group(1));
		}
		if(venueMatcher.find()){
			System.out.println("venue:" + venueMatcher.group(1));
		}
		if(urlMatcher.find()){
			System.out.println("url:" + urlMatcher.group(1));
			Pattern eventIdPattern = Pattern.compile(".*-(.*?).h");
			Matcher eventIdMatcher = eventIdPattern.matcher(urlMatcher.group(1));
			if(eventIdMatcher.find()){
				System.out.println("Event:" + eventIdMatcher.group());
				System.out.println("Event:" + eventIdMatcher.group(1));
			}
		}*/
		
	}
}
