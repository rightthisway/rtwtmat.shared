package com.admitone.tmat.eventfetcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EIBoxEvent;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;

public class EIBoxEventListingFetcher extends EventListingFetcher {
	public Collection<EventHit> getEventList(String theKeywords, String location, Date fromDate, Date toDate) throws Exception {
		//System.out.println("LOOKING BETWEEN DATES: " + fromDate + " TO: " + toDate);
		Collection<EIBoxEvent> eiboxEvents = DAORegistry.getEiBoxEventDAO().getEIBoxEvents(fromDate, toDate);
		ArrayList<String> usedIds = new ArrayList<String>();
		//System.out.println("EI BOX EVENTS=" + eiboxEvents);
		if (eiboxEvents == null) {
			return null;
		}
		String keywords = theKeywords;
		if(keywords != null){
			keywords = QueryUtil.normalizeQuery(theKeywords.replace('+',' '));
		}
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		for (EIBoxEvent eiboxEvent: eiboxEvents) {
			
			for(String keyword : keywords.split(" ")){ 

				//System.out.println("TESTING EIBOX EVENT=" + eiboxEvent.getEventName() + "-" + eiboxEvent.getVenueName() + "-" + eiboxEvent.getEventDate());
				//System.out.println("WITH KEYWORDS=" + keywords + " AND LOCATION=" + location);
			
				// check if it keywords matches event name
				if (eiboxEvent.getEventName() == null || !QueryUtil.matchAnyTerms(keyword, eiboxEvent.getEventName())) {
					continue;
				}

				String eiboxEventLocation = eiboxEvent.getVenueName() + ", " + eiboxEvent.getVenueCity();
				if (!QueryUtil.matchAllTerms(location, eiboxEventLocation)) {
					continue;
				}
						
				java.sql.Date sqlEventDate = new java.sql.Date(eiboxEvent.getEventDate().getTime()); 
				java.sql.Time sqlEventTime = new java.sql.Time(eiboxEvent.getEventDate().getTime());
			
				String id = eiboxEvent.getEventId().toString() + "-" + eiboxEvent.getVenueId() + "-" + eiboxEvent.getEventDate().getTime();
				EventHit eventHit = new EventHit(id, eiboxEvent.getEventName(),
						sqlEventDate, sqlEventTime, eiboxEventLocation, Site.EIBOX, null);
				//System.out.println("ADDING EI BOX EVENT=" + id);
				
				if(!usedIds.contains(id)){
					eventHits.add(eventHit);
					usedIds.add(id);
				}
			}
		}
		
		return eventHits;
	}	
}
