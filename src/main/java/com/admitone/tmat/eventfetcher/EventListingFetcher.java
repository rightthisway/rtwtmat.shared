package com.admitone.tmat.eventfetcher;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.Event;

public abstract class EventListingFetcher {
	private static List<String> ignoreWordList = Arrays.asList(new String[]{"in","an","a","of","on","the"});
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {
		return null;
	}
	
	public String removeIgnoreWordsList(String keywords){
		keywords=keywords.replaceAll("\\+", " ");
		StringBuffer result= new StringBuffer();
		for(String keyword:keywords.split("\\s+")){
			if(!ignoreWordList.contains(keyword)){
				result.append(" "+ keyword);
			}
		}
		return result.toString().trim();
	}
}