package com.admitone.tmat.eventfetcher;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.directwebremoting.annotations.DataTransferObject;

import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.utils.EventInfo;

@DataTransferObject
public class EventHit {
	private String id;
	private String name;
	private Date date;
	private Time time;
	private String dateAndTime;
	private String location;
	private String siteId;
	private String url;
	private String eventId;
	private java.util.Date creationDate;
	private String shortName;
	private CrawlState crawlState;
	private Integer bestMatchEventId;
	/**
	 * Extra parameters of the crawl.
	 */	
	private String extraParameters;
	/**
	 * Used in TMAT Admin Crawl Creation process 
	 * to check existing TicketListingCrawl with this queryUrl.
	 */	
	private Integer existingCrawlId;
	
	public EventHit() {
		
	}
	
	public EventHit(String eventId, String name, Date date, Time time, String location, String siteId, String url) {
		this.name = name.trim();
		this.location = location.trim();
		this.eventId = eventId;
		this.date = date;
		this.time = time;
		this.siteId = siteId;
		this.url = url;
		this.id = siteId + "-" + eventId;
		this.creationDate = new java.util.Date();
		this.shortName = name;
	}
	
	public EventHit(EventInfo eventInfo,String siteId,String url) {
		this.name = eventInfo.getName().trim();
		this.location = eventInfo.getBuilding().trim();
		this.eventId = eventInfo.getEventId().toString();
		this.date = eventInfo.getDate();
		this.time = eventInfo.getTime();
		this.siteId = siteId;
		this.url = url;
		this.id = siteId + "-" + eventId;
		this.creationDate = new java.util.Date();
		this.shortName = name;
	}
	
	public String getShortName() {
		return shortName;
	}

	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	public java.util.Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(java.util.Date creationDate) {
		this.creationDate = creationDate;
	}

	public String getEventId() {
		return eventId;
	}

	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public String getLocation() {
		return location;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public String getSiteId() {
		return siteId;
	}
	
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Time getTime() {
		return time;
	}

	public void setTime(Time time) {
		this.time = time;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getExtraParameters() {
		return extraParameters;
	}

	public void setExtraParameters(String extraParameters) {
		this.extraParameters = extraParameters;
	}

	public CrawlState getCrawlState() {
		return crawlState;
	}

	public void setCrawlState(CrawlState crawlState) {
		this.crawlState = crawlState;
	}
	
	
	public String toString() {
		return "EventHit: "
			+ "id:" + id
			+ ", name:" + name
			+ ", location:" + location
			+ ", date:" + date
			+ ", time:" + time
			+ ", url:" + url;
	}

	public Integer getBestMatchEventId() {
		return bestMatchEventId;
	}

	public void setBestMatchEventId(Integer bestMatchEventId) {
		this.bestMatchEventId = bestMatchEventId;
	}

	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	
	public String getDateAndTime(){
		if(this.dateAndTime==null){
			DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat tf = new SimpleDateFormat("HH:mm");
			String dateStr="TBD";
			String timeStr="TBD";
			if(date==null){
				return "TBD";
			}else{
				dateStr = df.format(date); 
			}
			
			if(time==null){
				return dateStr + " TBD";
			}else{
				timeStr = tf.format(time); 
			}
			this.dateAndTime=dateStr + " " + timeStr;
		}
		return dateAndTime;
		
	}

	public Integer getExistingCrawlId() {
		return existingCrawlId;
	}

	public void setExistingCrawlId(Integer existingCrawlId) {
		this.existingCrawlId = existingCrawlId;
	}

	
}
