package com.admitone.tmat.eventfetcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Authenticator;
import java.net.HttpURLConnection;
import java.net.PasswordAuthentication;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.ChangeProxy;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * Event listing fetcher for TicketMaster 
 * 
 *
 *
 */

public class TicketMasterEventListingFetcher extends EventListingFetcher   {

	private static Logger logger = LoggerFactory.getLogger(TicketMasterEventListingFetcher.class);

	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,
			List<Event> eventList, boolean isVenue, boolean isStubhubFromTicketEvolution) throws Exception {

		
		System.out.println(" [ ------- TEST CHANGE NO 8 ------ keywords ] "  + keywords) ;
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		// Replace space to Hyphen for tick pick events
		keywords = keywords.replaceAll("\\s+", "-");
		SimpleHttpClient httpClient = null;
		try {

			String ticketMasterFeedUrl = "https://www.ticketmaster.com/search?tm_link=tm_header_search&user_input="
					+ keywords + "&q=" + keywords;
			
			
			//String ticketMasterFeedUrl = "https://tickpick.com";			

			Collection<EventHit> eventResults = getEventsFromTicketMaster(ticketMasterFeedUrl, httpClient, keywords,
					location, fromDate, toDate, eventList, isVenue);
			eventHits.addAll(eventResults);
		} catch (Exception e) {
			System.out.println(" [ ------- TEST CHANGE NO 8 ------ ] " + e ) ;
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}

		return eventHits;
	}
	
	public void setWebViewProxySystemSetting(){
		String[] temp = null;
		String proxyHost = null;
		String proxyPort = null;
		
		String netNutProxy = ChangeProxy.getCurrentProxyForTM();
		String userName = ChangeProxy.getNETNUT_USERNAME();
		
		 temp = netNutProxy.split(":");
		
		 proxyHost = temp[0];
		 proxyPort = temp[1];

		System.out.println(" New : - [netNut Proxy ] " + netNutProxy + " [ host ] " + proxyHost + " [ port ] " + proxyPort   + " [userName] " + userName );

				
		final String stPort = proxyPort;
		final String stHost = proxyHost;
		final String stUserName = userName;

		Authenticator.setDefault(new Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(stUserName, "Qpox9d".toCharArray());
			}
		});
	}

	/**
	 * Fetch events from Ticket Master exchange
	 * 
	 * @param feedUrl
	 * @param httpClient
	 * @param keywords
	 * @param location
	 * @param fromDate
	 * @param toDate
	 * @param eventList
	 * @param isVenue
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws ParseException
	 */

	public Collection<EventHit> getEventsFromTicketMaster(String feedUrl, CloseableHttpClient httpClient,
			String keywords, String location, Date fromDate, Date toDate, List<Event> eventList, boolean isVenue)
			throws ClientProtocolException, IOException, ParseException {

		
		 	Date date = new Date();	    
		 	String fileName = null;
		 	
		 	synchronized(this) {
		 		long timeMilli = date.getTime();
		 		fileName = "C:/tmpcrawl/tmstEvnt_" + timeMilli+ ".txt";
		 	}
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");

		Map<String, Boolean> locationMap = new HashMap<String, Boolean>();
		Map<String, Boolean> dateMap = new HashMap<String, Boolean>();
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();

		for (Event event : eventList) {
			if (event.getDate() == null) {
				dateMap.put("TBD", true);
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:"
						+ event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:"
						+ event.getVenue().getState().replaceAll("\\s+", " "), true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}

		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		HttpGet httpPost = new HttpGet();		
		CloseableHttpResponse response = null;
		try {			
			String netNutProxy = ChangeProxy.getCurrentProxyForTM();
			String userName = ChangeProxy.getNETNUT_USERNAME();
			String  temp[] = netNutProxy.split(":");
			
			String proxyHost = temp[0];
			String proxyPort = temp[1];

			System.out.println(" keywords " + keywords + " [netNut Proxy ] " + netNutProxy + " [ host ] " + proxyHost + " [ port ] " + proxyPort + " [userName ] " + userName);

			
			String[] args1 = { feedUrl, fileName, proxyHost, proxyPort, userName};	
			
			//System.out.println( "  [   ]	 " + 	getRandomNetNutHostnUser (proxyHost , userName ) ) ;
			
			String randomProxyDets[] =  getRandomNetNutHostnUser(proxyHost,userName) ;
			
			System.out.println( "  [ Random proxy Host & User Fetched is   ]	 " +  randomProxyDets[0]  + " - "   + randomProxyDets[1] ) ;
			
			//System.out.println("Before Launch OF DOS WITH FXOPENER " + new Date());
		
				try{					
				
					String cmd =  "C:/rtwparserjars/EventParserFXWeb.bat";
	    			String param1 = args1[0];
	    			String param2 = args1[1];
	    			String param3 = randomProxyDets[0];//args1[2];
	    			String param4 = args1[3];
	    			String param5 = randomProxyDets[1];//args1[4];
	    			
	    			param1 = "\"" + param1 + "\"";
	    			param2 = "\"" + param2 + "\"";
	    			param3 = "\"" + param3 + "\"";
	    			param4 = "\"" + param4 + "\"";
	    			param5 = "\"" + param5 + "\"";
	    			//System.out.println( " [  Preparing JAVA Process FxOpener ] LAUNCH  ");    				    			
	    			
	    			
	    				    			
	    			 try
	    		        {           
	    		           
	    		            
	    		            Process proc =   startNewJavaProcess("", "com.admitone.tmat.eventfetcher.JavaFxWebOpener", args1);
	    		            Runtime rt = Runtime.getRuntime();
	    		            //Process proc = rt.exec("C:/Windows/System32/cmd.exe /c start \"\"   " +  cmd + " " +  param1  + " " +  param2 + " " +  param3 + " " +  param4 + " " + param5);
	    		            //Process proc = rt.exec("C:/Program Files/Java/jdk1.7.0_79/bin/java -cp \".;C:/rtwparserjars/;C:/rtwparserjars/json-simple-1.1.jar;C:/rtwparserjars/jsoup-1.11.3.jar;C:/rtwparserjars/xml-apis-1.0.b2.jar;C:/rtwparserjars/javafx-0.2.jar;\" com.admitone.tmat.eventfetcher.JavaFxWebOpener \"https://www.ticketmaster.com/search?tm_link=tm_header_search&user_input=taylor-swift&q=taylor-swift\" \"C:/tmpcrawl/tmstEvnt_1528379452278.txt\" \"us-s15.netnut.io\" \"33128\" \"Rightthisway!a2\"" );
	    		           	    		            
	    		            InputStream stderr = proc.getErrorStream();
	    		            InputStreamReader isr = new InputStreamReader(stderr);
	    		            BufferedReader br = new BufferedReader(isr);
	    		            
	    		            String line = null;
	    		            System.out.println("<ERROR>");
	    		            while ( (line = br.readLine()) != null)
	    		               // System.out.println(line);
	    		            System.out.println("</ERROR>");
	    		            int exitVal = proc.waitFor();
	    		            System.out.println("Process exitValue: " + exitVal);
	    		        } catch (Throwable t)
	    		          {
	    		            t.printStackTrace();
	    		          }
	    			
	    			
	    			//Runtime.getRuntime().exec("C:/Windows/System32/cmd.exe /c start \"\"   " +  cmd + " " +  param1  + " " +  param2 + " " +  param3 + " " +  param4 + " " + param5);
    			
					System.out.println(" FX OPENER CALL ENDED" ) ;
					
				}catch (Exception  e) {
					System.out.println(" EXCEPTION CALLING BAT FILE  CALLED "  ) ;
					e.printStackTrace();
				}
		
			 
			//System.out.println(" Called Sleep FOR....... 10 secs .... " + new Date() ) ;
			Thread.sleep(10000);
			//System.out.println(" woken up .. after ... 12 secs .... start parsing file ... " + new Date() ) ;

			//System.out.println(" After Launch .................................." + new Date());

			String content = new String(Files.readAllBytes(Paths.get(fileName)), "UTF-8");

			eventHits = parseHTMLDoc(content, location, fromDate, toDate, eventList, isVenue);

			//System.out.println("Finished Parsing all Events .................................." + new Date());

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (response != null)
				response.close();

			if (null != httpPost) {
				httpPost.releaseConnection();
			}
		}

		return eventHits;

	}
	
	
	
	void callFxLauncher(final String[] args1) {
		
	    class FxLauncher implements Runnable {	    	
	    		
	      String[] args1;
	       FxLauncher(String[] params) 
	       {
	    	   args1 = params;
	       }
	        
	       public void run() {	        	
	        	
	    	   try{
	    		   
	    			String cmd =  "C:/rtwparserjars/EventParserFXWeb";
	    				    			
	    			/*String param1 = "https://www.ticketmaster.com/search?tm_link=tm_homeA_header_search&user_input=billy+joel&q=billy+joel";
	    			String param2 = 	"C:/Lloyd/xxxxxxANASTBILLY104.txt";*/
	    			String param1 = args1[0];
	    			String param2 = args1[1];
	    			String param3 = args1[2];
	    			String param4 = args1[3];
	    			
	    			param1 = "\"" + param1 + "\"";
	    			param2 = "\"" + param2 + "\"";
	    			param3 = "\"" + param3 + "\"";
	    			param4 = "\"" + param4 + "\"";
	    			
	    			try {
	    				System.out.println( " - Preparing to launch Java command -  "); 
	    				Runtime.getRuntime().exec("cmd /c start \"\"   " +  cmd + " " +  param1  + " " +  param2 + " " +  param3 + " " +  param4 );
	    			}catch(Exception ex){	    				
	    				System.out.println( " ------- exception calling dos command ---- ");	    				
	    			}
	    		   
	    	
	    	   }catch(Exception ilegEx) {	    		
	    		   try {
	    			   //callProxyChange();
	    			   //System.out.println(" ---------  callProxyChange ---- " );	    			  
	    		   }catch(Exception ex) {
	    			   //System.out.println(" ---------     ilegEx  ---- " + ilegEx );
	    		   }
	    		   
	    		  
	    	   }
	          
	        }
	    }
	   // Thread t = new Thread(new FxLauncher(args1));
	   // t.setDaemon(true);
	  //  t.start();
	}
	
	
	public static void runAnotherApp(Class<? extends Application> JavaFxWebOpener) throws Exception {
	    Application app2 = JavaFxWebOpener.newInstance(); 
	    Stage anotherStage = new Stage();
	    app2.start(anotherStage);
	}
	

	private static Collection<EventHit> parseHTMLDoc(String content, String location, Date fromDate, Date toDate,
			List<Event> eventList, boolean isVenue) throws Exception {

		java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");

		/*
		 * File tickPIckRes = new File("C:/Lloyd/parkparkstreet.txt"); content =
		 * FileUtils.readFileToString(tickPIckRes);
		 */

		Collection<EventHit> eventHitsResult = new ArrayList<EventHit>();

		int contentStartIndex = content.indexOf("<DIV id=\"json_ld\"><SCRIPT type=\"application/ld+json\">[{") + 53;

		int contentEndIndex = content.indexOf("]</SCRIPT></DIV>") + 1;

		//System.out.println("************ contentStartIndex ***************** " + contentStartIndex);

		//System.out.println("**************contentEndIndex *************** " + contentEndIndex);

		String contentExtract = content.substring(contentStartIndex, contentEndIndex);

		//System.out.println("************ contentExtract ********************* " + contentExtract);

		JSONParser parser = new JSONParser();
		JSONArray jsonArray = (JSONArray) parser.parse(contentExtract);

		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		List tmp = new ArrayList();

		for (Object jsonEventsMap : jsonArray) {

			try {

				//System.out.println(jsonEventsMap);
				Map eventsMap = (Map) jsonEventsMap;

				//System.out.println(" --eventsMap---- " + eventsMap);

				Map offersMap = (Map) eventsMap.get("offers");
				//System.out.println(" --offersMap---- " + offersMap);
				String eventName = (String) eventsMap.get("name");
				//System.out.println(" --eventName---- " + eventName);
				eventName = eventName.replace("&amp;", "&");
				eventName = eventName.replace("&amp;#39;", "'");

				Map locationMap = (Map) eventsMap.get("location");
				Map addressMap = (Map) locationMap.get("address");
				//System.out.println(" --addressMap---- " + addressMap);
				String countryCode = (String) addressMap.get("addressCountry");
				//System.out.println(" --countryCode---- " + countryCode);
				String addressLocality = (String) addressMap.get("addressLocality");
				//System.out.println(" --addressLocality---- " + addressLocality);
				String addressRegion = (String) addressMap.get("addressRegion");
				//System.out.println(" --addressRegion---- " + addressRegion);
				String locationNametxt = (String) locationMap.get("name");
				//System.out.println(" --locationNametxt---- " + locationNametxt);
				String locationName = locationNametxt.replace("&amp;#39;", "'");
				//System.out.println(" --locationName---- " + locationName);
				String eventFullLocation = locationName.trim() + ", " + addressLocality.trim() + ", "
						+ addressRegion.trim();
				//System.out.println(" --eventFullLocation ---- " + eventFullLocation);
				String eventStartDate = (String) eventsMap.get("startDate");
				String url = (String) eventsMap.get("url");
				System.out.println(" --eventStartDate ---- " + eventStartDate);
				if(eventStartDate.length() > 20)
					eventStartDate = eventStartDate.substring(0,19); 
				
				//System.out.println(" --url ---- " + url);
				url = url.replace("&amp;", "&");
				//System.out.println(" --url ---- " + url);
				int eventIdx = url.indexOf("/event/") + 7;
				if (eventIdx == -1) {
					System.out.println(" -- Not matching /event/ URL ... SKIP ... ");
					continue;
				}
				//System.out.println(" --secondIndex ---- " + eventIdx);
				int questionMarkIndx = url.indexOf('?');
				if (questionMarkIndx == -1) {
					System.out.println(" -- Not matching QUESTION MARK IN URL  URL ... SKIP ... ");
					continue;
				}
				//System.out.println(" --questionMarkIndx ---- " + questionMarkIndx);
				String eventId = url.substring(eventIdx, questionMarkIndx);
				//System.out.println(" --eventId ---- " + eventId);
				TimeZone timeZone = TimeZone.getDefault();
				Calendar cal = Calendar.getInstance(timeZone);
				SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				sdfd.setCalendar(cal);
				try {
					cal.setTime(sdfd.parse(eventStartDate));
				}
				catch(Exception ex) {
					System.out.println("Exception parsing date -- re parsing with new format" + eventStartDate );
					try {
						sdfd.setCalendar(cal);						
						sdfd = new SimpleDateFormat("yyyy-MM-dd'T'ZZZZ");
						cal.setTime(new SimpleDateFormat("yyyy-MM-dd'T'ZZZZ").parse(eventStartDate));
						
						//System.out.println("  -- New date  Format -- " +  cal.getTime() ) ;
					}catch(Exception ex1) {
						
						System.out.println(" Ubanle to Parse DATE in Format  for second attempt " + eventStartDate) ;
						continue;
						
					}
				}
				
				Date tmDates = cal.getTime();
				Date eventDate = format.parse(eventStartDate);
				java.sql.Date date = new java.sql.Date(eventDate.getTime());
				
				
				
				
				Date date1 = sdfd.parse(eventStartDate);
				String newDateStr = new SimpleDateFormat("yyyy-MM-dd, hh:mm a").format(date1);
				String time[] = newDateStr.split(",");
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
				long ms = sdf.parse(time[1]).getTime();
				Time t = new Time(ms);
				//System.out.println(" --date ---- " + date);
				//System.out.println(" --time ---- " + t);

				/**
				 * eXPECTED fORTMAT 
				 * --date ---- 2019-02-26
 				   --time ---- 13:05:00
				 */
				
				if (!(tmDates.after(fromDate) && tmDates.before(toDate))) {

					System.out.println(" [ Skipping as  Dates not between From and To ] " + fromDate + "  & " + toDate);
					//continue;
				}
				if (location != null) {
					if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {

						System.out.println(" [Skipping as Location  ] " + location + "  does not match expected  "
								+ eventFullLocation);
						continue;
					}
				}
				if (!eventIds.contains(eventId)) {
					System.out.println(
							" ------ Creating Events --- eventId, eventName, date, t, eventFullLocation, ticketFetchUrl "
									+ eventId + ", " + eventName + ", " + date + ", " + t + ", " + eventFullLocation
									+ ", " + url);
					EventHit eventHit = new EventHit(eventId, eventName, date, t, eventFullLocation, Site.TICKET_MASTER,
							url);
					eventHits.add(eventHit);
					
					eventIds.add(eventId);
				}

				tmp.add(url);
			} catch (Exception ex) {
				System.out.println("Exception parsing ..args...args.. continie with next event .... " + ex);
			}
		}		

		System.out.println("******** NO OF EVENT HITS IS ********************* " + eventHits.size());
		return eventHits;

	}
	
	
	public Process startNewJavaProcess(final String optionsAsString, final String mainClass, final String[] arguments) 
			throws IOException {
			ProcessBuilder processBuilder = createProcess(optionsAsString, mainClass, arguments);
			Process process = processBuilder.start();	
			return process;
	}
	
	private ProcessBuilder createProcess(final String optionsAsString, final String mainClass, final String[] arguments) {		
		String jvm = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
		String classpath = System.getProperty("java.class.path");
		//classpath +=".;%CLASSPATH%;C:/rtwparserjars;C:/rtwparserjars/json-simple-1.1.jar;C:/rtwparserjars/jsoup-1.11.3.jar;C:/rtwparserjars/xml-apis-1.0.b2.jar;C:/rtwparserjars/jfxrt.jar";
		classpath += ".;%CLASSPATH%;C:/rtwparserjars;C:/rtwparserjars/json-simple-1.1.jar;C:/rtwparserjars/jsoup-1.11.3.jar;C:/rtwparserjars/xml-apis-1.0.b2.jar;C:/rtwparserjars/jfxrt.jar";
		//System.out.println(" classpath " + classpath);
		//System.out.println(" PATH " + System.getProperty("java.path"));
		//System.out.println(" EXECUTING WITH JDK VERSION " + jvm);
		String[] options = optionsAsString.split(" ");
		List < String > command = new ArrayList <String>();
		 command.add(jvm);
		//command.add("C:\\Program Files\\Java\\jdk1.7.0_79\\bin\\java");
		
		//command.add("C:/Program Files/Java/jdk1.7.0_79/jre/bin/java");
		//command.add("C:/Program Files/Java/jdk1.7.0_79/bin/java");
		
		command.addAll(Arrays.asList(options));
		command.add(mainClass);
		command.addAll(Arrays.asList(arguments));
		ProcessBuilder processBuilder = new ProcessBuilder(command);
		Map< String, String > environment = processBuilder.environment();
		environment.put("CLASSPATH", classpath);
		return processBuilder;
	
	}
	
	private static int getRandomNumberInRange(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	private static int getStickyNum (String text) {
		Matcher matcher = Pattern.compile("\\d+").matcher(text);
		matcher.find();
		int i = Integer.valueOf(matcher.group());
		return i;
	}
	
	private static String[] getRandomNetNutHostnUser(String host, String userName) {//us-s50.netnut.io Rightthisway!a25		
	
		int currentHostNum = getStickyNum(host);		
		int currentUserNum = getStickyNum(userName);		
		int hostNumberSuffix = getRandomNumberInRange(1,254);
		//System.out.println( " [__hostNumberSuffix __] " + hostNumberSuffix);		
		int userNumberSuffix = getRandomNumberInRange(1,254);		
		//System.out.println( " [__userNumberSuffix__] " + userNumberSuffix);		
		host = host.replaceFirst(String.valueOf(currentHostNum), String.valueOf(hostNumberSuffix));
		userName = userName.replaceFirst(String.valueOf(currentUserNum), String.valueOf(userNumberSuffix));
		String[] netNutDets = { host,userName };		
		return netNutDets;
	}
		
		
	private static boolean checkSiteProxyConnectivity(String url, String proxyHost,String proxyPort,  String userName) {
		boolean canConnectOverProxy = false;
		
			System.setProperty("https.proxyHost", proxyHost);
			System.setProperty("https.proxyPort", proxyPort);
			System.setProperty("https.proxyUser", userName);
			System.setProperty("https.proxyPassword", "Qpox9d");
			//System.setProperty("jdk.http.auth.proxying.disabledSchemes", "'Basic','Digest','NTLM','Kerberos','Negotiate'");
			//System.setProperty("jdk.http.auth.tunneling.disabledSchemes", "Basic,Digest,NTLM,Kerberos,Negotiate");			
			HttpURLConnection connection = null;
				try {
		            URL siteURL = new URL(url);
		            connection = (HttpURLConnection) siteURL
		                    .openConnection();
		            connection.setRequestMethod("GET");
		            connection.connect();
		 
		            int code = connection.getResponseCode();
		            if (code == 200) {
		            	canConnectOverProxy = true;
		            }
		        } catch (Exception e) {
		        	System.out.println(e);
		        }
				
				System.out.println( "canConnectOverProxy " + canConnectOverProxy) ;
				return canConnectOverProxy;		
		}

	// test
	public static void main(String[] args) throws Exception {
		
			String a[] =  getRandomNetNutHostnUser("us-s50.netnut.io","Rightthisway!a25")  ;
		 
			System.out.println(a[0] +  " --" + a[1]) ; // 
			String keywords = "Taylor-Swift";
			/*String url = "https://www.ticketmaster.com/search?tm_link=tm_header_search&user_input="
					+ keywords + "&q=" + keywords;*/
			String url = "https://www.tickpick.com";
			checkSiteProxyConnectivity(url, a[0], "33128",  a[1]);
			
			//Process proc = rt.exec("C:/Windows/System32/cmd.exe /c start \"\"   " +  cmd + " " +  param1  + " " +  param2 + " " +  param3 + " " +  param4 + " " + param5);
			
			
			
			
		
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		Date fromDate = format.parse("08/31/2018");
		Date toDate = format.parse("10/24/2019");
		Event event = new Event();
		event.setLocalDate(format.parse("04/25/2018"));
		List<Event> eventList = new ArrayList<Event>();
		eventList.add(event);

	/*	Collection<EventHit> hits = new TicketMasterEventListingFetcher().getEventList("taylor swift", null, fromDate,
				toDate, eventList, false, false);*/
		/*Collection<EventHit> hits = new TicketMasterEventListingFetcher().getEventList("New York Mets", null, fromDate,
				toDate, eventList, false, false);*/
		Collection<EventHit> hits = new TicketMasterEventListingFetcher().getEventList("The Lion King", null, fromDate,
				toDate, eventList, false, false);

		System.out.println(hits.toString());
	}
}
