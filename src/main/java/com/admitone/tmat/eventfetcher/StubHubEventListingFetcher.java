package com.admitone.tmat.eventfetcher;

import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.apache.maven.surefire.booter.shade.org.codehaus.plexus.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.StubHubApiTracking;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class StubHubEventListingFetcher extends EventListingFetcher {
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(StubHubEventListingFetcher.class);
	
	private static Integer MAX_HIT_PER_MINUTE=10;
	
	public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {

		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		keywords = keywords.replaceAll("\\s+", "+");
		SimpleHttpClient httpClient = null;
//		for(String keyword : keywords.split(" ")) {
		Date start = new Date();
		long evntFetchtime=0,apifetchTime=0,finalTime=0;
			try {
				SimpleDateFormat monformat = new SimpleDateFormat("yyyy-MM-dd");
				SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
				Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
				String from = monformat.format(fromDate);
				String to = monformat.format(toTomorrowDate);
				String alternateToDate = dateformat.format(toTomorrowDate);
//				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				/*******/
			
				
				httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB);
				String feedUrl = "";
				
				feedUrl = "http://www.stubhub.com/shape/search/catalog/events/v3?tld=1&geoExpansion=true&status=active+%7Ccontingent&start=0&rows=5000&sort=eventDateLocal+asc&spellCheck=true&fieldList=id%2Cname%2CeventDateLocal%2Cvenue%2CeventInfoUrl%2CdisplayAttributes&q="+keywords;
				//feedUrl = "http://www.stubhub.com/search/doSearch?searchStr="+keywords+"&searchMode=event&rows=100&start=0&nS=0&ae=1&sp=Date&sd=1";	
				if (from != null && to != null) {
					//feedUrl += "&dateLocal=2016-10-01T00%3A00+TO+2016-11-01T00%3A00";
					feedUrl += "&dateLocal=" + from + "T00%3A00+TO+" + to + "T00%3A00";
					//feedUrl += "&startDate=" + from + "&endDate="+to ;
					//feedUrl += "&startDate=" + from + "&endDate=" + alternateToDate +";"+to ;
				}
				boolean callApiflag = false;
				try {
					Collection<EventHit> subResult= getEventsFromStubhub(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
					if(subResult != null) {
						eventHits.addAll(subResult);	
					} else {
						callApiflag = true;
					}
						
				}catch (Exception e) {
					callApiflag = true;
					logger.error("SHELF Caught Exception: " + e);
					e.printStackTrace();
				}
				evntFetchtime = new Date().getTime()-start.getTime();
				if(callApiflag) {
					try {
						eventHits = new ArrayList<EventHit>();
						Collection<EventHit> apiEventHits= getStubHubApiEventList(keywords, location, fromDate, toDate, eventList, isVenue, isStubhubFromTicketEvolution);
						if(apiEventHits != null){
							eventHits.addAll(apiEventHits);
						}
					} catch (Exception e) {
						logger.error("SHELF API Caught Exception: " + e);
						e.printStackTrace();
					}
				}
				apifetchTime = new Date().getTime()-(start.getTime()+evntFetchtime);
				/*******/
				
				logger.info("Event SEARCH STUBHUB : "+finalTime+" :evnt: "+evntFetchtime+" :api:"+apifetchTime+" : key: "+keywords+" : loc: "+location);
			} catch (Exception e) {
				logger.error("SHELF Caught Exception: " + e);
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
			}
//		}
		return eventHits;
	}
public Collection<EventHit> getEventsFromStubhubOld(String feedUrl,SimpleHttpClient httpClient,String keywords,String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws ClientProtocolException, IOException, JSONException, ParseException{
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateTimeFormat = new SimpleDateFormat("MMM dd yyyy h:mm a");
		DateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		
		if(null != eventList && !eventList.isEmpty()){
			for(Event event:eventList){
				if(event.getDate()==null){
					dateMap.put("TBD", true);
//					String location = event.getVenue().getBuilding();
					locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
					continue;
				}
				dateMap.put(df.format(event.getDate()), true);
				eventMap.put(event.getId(), event);
			}
		}
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		Integer totalCount = 0;
		Integer pageNumber = 0;
		while(true){
			//http://www.stubhub.com/shape/search/catalog/events/v3?tld=1&geoExpansion=true&status=active+%7Ccontingent&start=0&rows=20&sort=eventDateLocal+asc&spellCheck=true&fieldList=id%2Cname%2CeventDateLocal%2Cvenue%2CeventInfoUrl&q=Mets
			//System.out.println(feedUrl);
			HttpGet httpPost = new HttpGet(feedUrl);
			// System.out.println("stubhub>" + feedUrl);
			httpPost.addHeader("Host", "www.stubhub.com");
			httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
			httpPost.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
			httpPost.addHeader("Accept-Encoding", "gzip, deflate");
			httpPost.addHeader("Accept-Language", "en-us");
			httpPost.addHeader("Referer", "http://www.stubhub.com/an-american-in-paris-tempe-tickets-an-american-in-paris-tempe-asu-gammage-4-19-2017/event/9623006/");
			httpPost.addHeader("Cookie", httpClient.getStubhubCookie());
			
//			httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB);

			String result = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpPost);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					result = EntityUtils.toString(entity).trim();
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
				
				if(null != httpPost ){
					httpPost.releaseConnection();
				}
			}
			
			//System.out.println(result);
			result  = result.replaceAll("\\s++", " ");
			
			Pattern eventTablePattern =Pattern.compile("eventTable(.*?)table");
			Matcher eventTableMatcher = eventTablePattern.matcher(result);
			Collection<Integer> eventIds = new ArrayList<Integer>();
			if(eventTableMatcher.find()){
				String eventTable = eventTableMatcher.group();
//				System.out.println(eventTable);
				Pattern eventTrPattern =Pattern.compile("tr>(.*?)</tr>");
				Matcher eventTrMatcher = eventTrPattern.matcher(eventTable);
				while(eventTrMatcher.find()){
					String tr = eventTrMatcher.group();
					Pattern dayPattern =Pattern.compile("day\"> (.*?)</");
					Matcher dayMatcher = dayPattern.matcher(tr);
					String timeStr ="";
					String dateStr = "";
					String month = ""; 
					String eventName = "";
					String eventLocation = "";
					String eventCity = "";
					String eventState = "";
					if(!dayMatcher.find()){
						continue;
					}
					
					Pattern datePattern =Pattern.compile("ticketEventDate\"> (\\d+)</");
					Matcher dateMatcher = datePattern.matcher(tr);
					if(dateMatcher.find()){
						dateStr  = dateMatcher.group(1);
					}
					Pattern monthPattern =Pattern.compile("ticketEventMonth\"> (\\w+)</");
					Matcher monthMatcher = monthPattern.matcher(tr);
					if(monthMatcher.find()){
						month  = monthMatcher.group(1);
					}
					
					
					Calendar cal = Calendar.getInstance();
					int year = cal.get(Calendar.YEAR);
					
					Pattern yearPattern =Pattern.compile("ticketEventYear\"> (\\w+)</");
					Matcher yearMatcher = yearPattern.matcher(tr);
					if(yearMatcher.find()){
						year  = Integer.parseInt(yearMatcher.group(1));
					}
					
					
					Pattern eventNamePattern =Pattern.compile("eventName(.*?)e=\"(.*?)\"");
					Matcher eventNameMatcher = eventNamePattern.matcher(tr);
					
					if(eventNameMatcher.find()){
						eventName = eventNameMatcher.group(2).replaceAll("Tickets", "");
					}
					
					
					Pattern eventIdPattern =Pattern.compile("(\\d+{1,2})-(\\d+{1,2})-(\\d+{4})-(\\d+{6,10})");
					Matcher eventIdMatcher = eventIdPattern.matcher(tr);
					Integer eventId = null;
					if(eventIdMatcher.find()){
						eventId = Integer.parseInt(eventIdMatcher.group(4));
					}else{
						continue;
					}
					
					//Pattern venueTimePattern =Pattern.compile("ue/(.*?)\"(.*?)<br/>(.*?)<br/>(.*?)</td");
					Pattern venueTimePattern =Pattern.compile("eventLocation\">(.*?)\">(.*?)\">(.*?)</a><br/>(.*?)<br/>(.*?)</td>");
					Matcher venueTimeMatcher = venueTimePattern.matcher(tr);
					if(venueTimeMatcher.find()){
						eventLocation = StringUtils.capitalize(venueTimeMatcher.group(3).replaceAll("-", " "));
						eventCity = venueTimeMatcher.group(4).split(",")[0].trim();
						eventState = venueTimeMatcher.group(4).split(",")[1].trim();					
						timeStr = venueTimeMatcher.group(5).replaceAll("\\.", "").trim();
						if(timeStr.contains("Time TBD")){
							timeStr = null;
						}
						/*eventLocation = StringUtils.capitalize(venueTimeMatcher.group(1).replaceAll("-", " "));
						eventCity = venueTimeMatcher.group(3).split(",")[0].trim();
						eventState = venueTimeMatcher.group(3).split(",")[1].trim();					
						timeStr = venueTimeMatcher.group(4).replaceAll("\\.", "").trim();
						if(timeStr.contains("Time TBD")){
							timeStr = null;
						}*/
//						}
					}
					String url = "http://www.stubhub.com/?event_id=" + eventId;
					String eventFullLocation = eventLocation.trim() + ", " + eventCity.trim() + ", " + eventState.trim();
					
					Date eventDate = null;
					java.sql.Date date = null;
					Time time = null;
					if(timeStr!=null){
						dateStr = month + " " + dateStr + " " + year + " " + ((timeStr.replaceAll("EDT", "").replaceAll("PDT", "").replaceAll("CDT", "")).trim());
						eventDate = dateTimeFormat.parse(dateStr);
						date = new java.sql.Date(eventDate.getTime());
						time = new Time(eventDate.getTime());
					}else{
						dateStr = month + " " + dateStr + " " + year;
						eventDate = dateFormat.parse(dateStr);
						date = new java.sql.Date(eventDate.getTime());
					}
					System.out.println(eventName +" @ " + eventFullLocation + " on " + dateStr);
					
					// location filter
					if (location != null) {
						if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {
							continue;
						}
					}
					
					boolean flag=false;
					dateStr = df.format(eventDate);
					if(!dateMap.containsKey(dateStr)){
						for(String key:locationMap.keySet()){
							String tokens[] = key.split(":-:");
							String building = tokens[0].trim();
							String cityToken = tokens[1].trim();
							String stateToken = tokens[2].trim();
							
							if( cityToken.equalsIgnoreCase(eventCity.trim())&& stateToken.equalsIgnoreCase(eventState.trim()) && (TextUtil.isSimilarVenue(building, eventLocation, 1))){
								flag=true;
								break;
							}
						}
					}else{
						flag=true;
					}
					
					if(flag && !eventIds.contains(eventId)){	
						System.out.println("SHELF SPLITWORDS: " + eventName);
						EventHit eventHit = new EventHit(eventId+"", eventName, date, time, eventFullLocation, Site.STUB_HUB, url);
						eventHits.add(eventHit);
						eventIds.add(eventId);
					}
					
				}
				
			}
			Pattern totalCountPattern = Pattern.compile("Red\">(\\d+)<");
			Matcher totalCountMatcher = totalCountPattern.matcher(result);
			
			if(totalCountMatcher.find()){
				totalCount = Integer.parseInt(totalCountMatcher.group(1));
				if((pageNumber+1)*100<totalCount){
					pageNumber++;
					String temp[] = feedUrl.split("start=");
					String temp1[] = feedUrl.split("&nS=");
					feedUrl = temp[0] + "start=" + (pageNumber*100) + "&nS=" + temp1[1]; 
				}else{
					break;
				}
			}else{
				break;
			}
		}
		
		return eventHits;
	}
	public Collection<EventHit> getEventsFromStubhub(String feedUrl,SimpleHttpClient httpClient,String keywords,String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception{
		
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateTimeFormat = new SimpleDateFormat("MMM dd yyyy h:mm a");
		DateFormat dateFormat = new SimpleDateFormat("MMM dd yyyy");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		
		if(null != eventList && !eventList.isEmpty()){
			for(Event event:eventList){
				if(event.getDate()==null){
					dateMap.put("TBD", true);
//					String location = event.getVenue().getBuilding();
					locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
					continue;
				}
				dateMap.put(df.format(event.getDate()), true);
				eventMap.put(event.getId(), event);
			}
		}
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		while(true){
			//http://www.stubhub.com/shape/search/catalog/events/v3?tld=1&geoExpansion=true&status=active+%7Ccontingent&start=0&rows=20&sort=eventDateLocal+asc&spellCheck=true&fieldList=id%2Cname%2CeventDateLocal%2Cvenue%2CeventInfoUrl&q=Mets
			//System.out.println(feedUrl);
			HttpGet httpPost = new HttpGet(feedUrl);
			// System.out.println("stubhub>" + feedUrl);
			httpPost.addHeader("Host", "www.stubhub.com");
			httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
			httpPost.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
			httpPost.addHeader("Accept-Encoding", "gzip, deflate");
			httpPost.addHeader("Accept-Language", "en-us");
			httpPost.addHeader("Referer", "https://www.stubhub.com/metallica-tickets-metallica-el-paso-don-haskins-center-2-28-2019/event/103491786/");
			httpPost.addHeader("Cookie", httpClient.getStubhubCookie());
			
//			httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB);

			String result = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpPost);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					result = EntityUtils.toString(entity).trim();
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Inside stubhub Exception "+feedUrl);
				throw e;
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
				
				if(null != httpPost){
					httpPost.releaseConnection();
				}
			}
			
			//System.out.println(result);
			//result  = result.replaceAll("\\s++", " ");
			
			JSONObject jsonObject = new JSONObject(result);
			/*if(!content.contains("\"errors\":null")){
				JSONObject errorObject = jsonObject.getJSONObject("errors");
				if (errorObject != null) {
					throw new Exception(errorObject.getJSONArray("error").getJSONObject(0).getString("errorMessage"));
				}
			}*/
			SimpleDateFormat stubhubDTF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Collection<Integer> eventIds = new ArrayList<Integer>();
			
			//To get events proceesed count of previous hit
			//Sampe URL : //http://www.stubhub.com/shape/search/catalog/events/v3?tld=1&geoExpansion=true&status=active+%7Ccontingent&start=0&rows=20&sort=eventDateLocal+asc&spellCheck=true&fieldList=id%2Cname%2CeventDateLocal%2Cvenue%2CeventInfoUrl&q=Mets
			String countPrefix[] = feedUrl.split("start=");
			String countSuffix[] = countPrefix[1].split("&rows=");
			Integer eventsCount = Integer.parseInt(countSuffix[0]);
			if(jsonObject.has("events")){
				JSONArray docsJSONArray = jsonObject.getJSONArray("events");
				//stubHubApiTracking = new StubHubApiTracking();
				for (int i = 0 ; i < docsJSONArray.length() ; i++) {
					String timeStr ="";
					String dateStr = "";
					String month = ""; 
					String eventName = "";
					String eventLocation = "";
					String eventCity = "";
					String eventState = "";
					eventsCount++;
					
					JSONObject eventJSONObject = docsJSONArray.getJSONObject(i);
					eventName = eventJSONObject.getString("name");
					
					Integer eventId = null;
					String eventIdStr = eventJSONObject.getString("id");
					if(eventIdStr!=null && eventIdStr.trim().length()>0) {
						eventId= Integer.parseInt(eventIdStr);
					} else {
						continue;
					}
					
					JSONObject venueJSONObject = eventJSONObject.getJSONObject("venue");
					eventLocation = venueJSONObject.getString("name");
					eventCity =  venueJSONObject.getString("city");
					eventState =  venueJSONObject.getString("state");	
					
					//timeStr = venueTimeMatcher.group(5).replaceAll("\\.", "").trim();
					//if(timeStr.contains("Time TBD")){
					//	timeStr = null;
					//}
					
					String url = "http://www.stubhub.com/?event_id=" + eventId;
					String eventFullLocation = eventLocation.trim() + ", " + eventCity.trim() + ", " + eventState.trim();
					
					Date eventDate = null;
					java.sql.Date date = null;
					Time time = null;
					String eventDateTimeStr = eventJSONObject.getString("eventDateLocal");
					if(eventDateTimeStr != null && eventDateTimeStr.trim().length()>0) {
						eventDate = stubhubDTF.parse(eventDateTimeStr);
						date = new java.sql.Date(eventDate.getTime());
						time = new Time(eventDate.getTime());
						dateStr = df.format(date)+" "+time;
					}
					
					/*if(date != null) {
						if(fromDate != null && date.before(fromDate)) {
							System.out.println("Before EventDate : "+eventName +" @ " + eventFullLocation + " on " + dateStr);
							continue;
						}
						if(toDate != null && date.after(toDate)) {
							System.out.println("After EventDate : "+eventName +" @ " + eventFullLocation + " on " + dateStr);
							continue;
						}
					}*/
					
//					System.out.println(eventName +" @ " + eventFullLocation + " on " + dateStr);
					
					// location filter
					if (location != null) {
						if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {
							continue;
						}
					}
					
					boolean flag=false;
					dateStr = df.format(eventDate);
					if(!dateMap.containsKey(dateStr)){
						for(String key:locationMap.keySet()){
							String tokens[] = key.split(":-:");
							String building = tokens[0].trim();
							String cityToken = tokens[1].trim();
							String stateToken = tokens[2].trim();
							
							if( cityToken.equalsIgnoreCase(eventCity.trim())&& stateToken.equalsIgnoreCase(eventState.trim()) && (TextUtil.isSimilarVenue(building, eventLocation, 1))){
								flag=true;
								break;
							}
						}
					}else{
						flag=true;
					}
					
					if(flag && !eventIds.contains(eventId)){	
						System.out.println("SHELF SPLITWORDS: " + eventName);
						EventHit eventHit = new EventHit(eventId+"", eventName, date, time, eventFullLocation, Site.STUB_HUB, url);
						eventHits.add(eventHit);
						eventIds.add(eventId);
					}
				}
			}
			
			Integer totalCount = null;
			if(jsonObject.has("numFound")) {
				totalCount = Integer.parseInt(jsonObject.getString("numFound"));	
				if(eventsCount<totalCount){
					String temp[] = feedUrl.split("start=");
					String temp1[] = feedUrl.split("&rows=");
					feedUrl = temp[0] + "start=" + (eventsCount) + "&rows=" + temp1[1]; 
				}else{
					break;
				}
			}else{
				break;
			}
		}
		
		return eventHits;
	}
	
	
	/**
	 * StubHub Api Event Listing fetching
	 * @param keywords
	 * @param location
	 * @param fromDate
	 * @param toDate
	 * @param eventList
	 * @param isVenue
	 * @param isStubhubFromTicketEvolution
	 * @return
	 * @throws Exception
	 */
	public Collection<EventHit> getStubHubApiEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue,boolean isStubhubFromTicketEvolution) throws Exception {
		
		
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		SimpleHttpClient httpClient = null;
		String stubHubApiUrl = null;
		try{
			httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB_API);
			//https://api.stubhub.com/search/catalog/events/v2?title="Justin Bieber"&date=2016-05-04T00:00 TO 2016-05-04T23:59
			if(keywords == null || keywords.isEmpty()){
				throw new RuntimeException("Keywords should not be null");
			}else{
				//Date range should be in this format date=2014-06-01T00:00 TO 2014-06-05T23:59
				SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
				
				//it will replace all your symbols including spaces -> '+' with proper one for URL
				String strQuery = URLEncoder.encode(keywords, "UTF-8");
				stubHubApiUrl = "https://api.stubhub.com/search/catalog/events/v2?title="+strQuery+"&date="+sdfd.format(fromDate)+"TO"+sdfd.format(toDate); ;
			}
			
			Collection<EventHit> eventResults = getEventsFromStubHubApi(stubHubApiUrl, httpClient, keywords, location,
													fromDate, toDate, eventList, isVenue);
			eventHits.addAll(eventResults);
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}
		
		return eventHits;
	}
	
	/**
	 * Method to fetch the events 
	 * from StubHuB API for the query string
	 * @param feedUrl
	 * @param httpClient
	 * @param keywords
	 * @param location
	 * @param fromDate
	 * @param toDate
	 * @param eventList
	 * @param isVenue
	 * @return
	 * @throws Exception 
	 */
	public Collection<EventHit> getEventsFromStubHubApi(String apiUrl, CloseableHttpClient httpClient, String keywords, 
			String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception{
		
		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		
		Map<String, Boolean> locationMap = new HashMap<String, Boolean>();
		Map<String, Boolean> dateMap = new HashMap<String, Boolean>();
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
		
		for(Event event : eventList){
			if(event.getDate() == null){
				dateMap.put("TBD", true);
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}
		
		//Tamil : Limiting API hits to 10 per minute
		Property hitTimeProperty = DAORegistry.getPropertyDAO().get("stubhub.hit.minute.start.time");
		Property hitCountProperty =  DAORegistry.getPropertyDAO().get("stubhub.hit.minute.count");
		Integer minuteHitCount =  Integer.parseInt(hitCountProperty.getValue());
		Date minuteTime =  dbDateTimeFormat.parse(hitTimeProperty.getValue());
		Long nowTime = new Date().getTime();
		//If Max minute count reached then stop till remaining time
		if(minuteHitCount >= MAX_HIT_PER_MINUTE) {
			//Long nowTime = new Date().getTime();
			if((nowTime - minuteTime.getTime())<60*1000){
				Thread.sleep((60*1000)-(nowTime - minuteTime.getTime()));
			}
			minuteTime = new Date();
			hitTimeProperty.setValue(dbDateTimeFormat.format(minuteTime));
			hitCountProperty.setValue("1");
			DAORegistry.getPropertyDAO().update(hitTimeProperty);
			DAORegistry.getPropertyDAO().update(hitCountProperty);
			
			//If time exceeds morethan one minute then reset time
		} else if((nowTime - minuteTime.getTime())>60*1000){
				minuteTime = new Date();
				hitTimeProperty.setValue(dbDateTimeFormat.format(minuteTime));
				hitCountProperty.setValue("1");
				
				DAORegistry.getPropertyDAO().update(hitTimeProperty);
				DAORegistry.getPropertyDAO().update(hitCountProperty);
		} else {
			//add stubhum minute count to 1 for each stubhub site hits
			DAORegistry.getPropertyDAO().addStubhubMinuteHitCount(1);
		}
		

		String encoded64 = new String(Base64.encodeBase64("PStp62MnpjEm7e57clcTwrCxmeYa:wiEwCdnUQff3ZfHkBWj73n1arSAa".getBytes()));
		HttpPost apiLogin = new HttpPost("https://api.stubhub.com/login");
		apiLogin.setHeader("Authorization", "Basic " + encoded64);
		apiLogin.addHeader("Content-Type", "application/x-www-form-urlencoded");
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "password"));
		params.add(new BasicNameValuePair("username", "amit.raut@rightthisway.com"));
		params.add(new BasicNameValuePair("password", "Ulaga123$$"));
		
		//send the request body in HttpPost method that contains
		//grant_type,username,password
		apiLogin.setEntity(new UrlEncodedFormEntity(params));

		String content = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(apiLogin);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			logger.error("Inside Stubhub aPI Error 1 : ");
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != apiLogin){
				apiLogin.releaseConnection();
			}
		}			
		
		//System.out.println("api :"+content);
		//Get the json returned after successful login 
		//json response as token_type,access_token,expires_in,refresh_token
		JSONObject jsonAccessObject = new JSONObject(content);
		
		//access token expiration period is six months 
		String accessToken = jsonAccessObject.getString("access_token");
		//once the access token has been expired we can use the refresh token to regenerate
		String refreshToken = jsonAccessObject.getString("refresh_token");
		
		//Hit the api url to look for event info		
		String restUrl = URLEncoder.encode("You url parameter value", "UTF-8");
		HttpGet httpGet = new HttpGet(apiUrl);
		httpGet.setHeader("Authorization", "Bearer " + accessToken);
		httpGet.addHeader("Accept", "application/json");
		
		try{
			response = httpClient.execute(httpGet);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			logger.error("Inside Stubhub aPI Error 2 : ");
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != httpGet){
				httpGet.releaseConnection();
			}
		}	
		
		JSONObject jsonObject = new JSONObject(content);
		StubHubApiTracking stubHubApiTracking = null;
		if(jsonObject.getJSONArray("events") != null){
			stubHubApiTracking = new StubHubApiTracking();
			JSONArray jsonArray = jsonObject.getJSONArray("events");
			for(int i=0; i<jsonArray.length(); i++){
				JSONObject docJsonObject = jsonArray.getJSONObject(i);
				
				String eventId = docJsonObject.getString("id");
				//String eventName = docJsonObject.getString("title");
				String queryUrl = docJsonObject.getString("eventInfoUrl");
				String eventDateObj = docJsonObject.getString("dateLocal");
				JSONObject venueObj = docJsonObject.getJSONObject("venue");
				String venueName = venueObj.getString("name");
				String city = venueObj.getString("city");
				String state = venueObj.getString("state");
				String eventLocation = venueName+", "+city+", "+state;		
				System.out.println("=====" + eventLocation);
				String eventName = null;
				String jsonStr = null;
				if(docJsonObject.has("attributes")){
					jsonStr = docJsonObject.getString("attributes");
					if(jsonStr != null){
						JSONArray jsonarray = new JSONArray(jsonStr);
						for (int j = 0; j < jsonarray.length(); j++) {
						    JSONObject jsonobject = jsonarray.getJSONObject(j);
						    String name = jsonobject.getString("name");
						    if(name.equals("act_primary")){
						    	eventName = jsonobject.getString("value");
						    }
						}
					}
				}
				
				SimpleDateFormat sdfd = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
				String eventDateStr = sdfd.format(sdfd.parse(eventDateObj));
				
				Date eventDate = sdfd.parse(eventDateStr);
				java.sql.Date date = new java.sql.Date(eventDate.getTime());
				
				String timeStr = eventDateStr.substring(eventDateStr.lastIndexOf("T")+1);
				SimpleDateFormat sdf = new SimpleDateFormat("hh:mm");
				long ms = sdf.parse(timeStr).getTime();
				Time eventTime = new Time(ms);
				
				if(!eventIds.contains(eventId)){	
					System.out.println("SHELF SPLITWORDS: " + eventName);
					EventHit eventHit = new EventHit(eventId, eventName, date, eventTime, eventLocation, Site.STUB_HUB, queryUrl);
					eventHits.add(eventHit);
					eventIds.add(eventId);
				}
				
			}
			
			//capture api call hits into db
			stubHubApiTracking.setCrawlId(null);
			stubHubApiTracking.setLastRunRime(new Date());
			stubHubApiTracking.setStatus("ACTIVE");
			stubHubApiTracking.setRunStatus("SUCCESS");
			stubHubApiTracking.setLastUpdated(new Date());
			stubHubApiTracking.setTrackingType("EVENT");
			DAORegistry.getStubHubApiTrackingDAO().save(stubHubApiTracking);
		}
		
		return eventHits;
	}
	
	public static void main(String[] args) throws Exception {
		StubHubEventListingFetcher fetcher = new StubHubEventListingFetcher();
		Calendar cal1 = new GregorianCalendar(2018, 9, 4);
		Calendar cal2 = new GregorianCalendar(2018, 9, 28);
		List<Event> eventList = new ArrayList<Event>();
		Event e = new Event();
		e.setLocalDate(new Date(new GregorianCalendar(2018, 9, 4).getTimeInMillis()));
		eventList.add(e);
		
		fetcher.getEventList("new york rangers", "", cal1.getTime(), cal2.getTime(),eventList,false,false);
	}
}
