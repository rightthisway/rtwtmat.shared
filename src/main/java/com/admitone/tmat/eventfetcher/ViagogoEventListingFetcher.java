package com.admitone.tmat.eventfetcher;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Viagogo Event Fetcher.
 */

public class ViagogoEventListingFetcher extends EventListingFetcher {
	private static Pattern javascriptDatePattern = Pattern.compile("\\((.*)\\)");
	private static Pattern dataPattern = Pattern.compile("[$]create\\(Viagogo.ClientGrid, (.*), null, null, [$]get\\(", Pattern.DOTALL);

	private static Pattern eventIdPattern = Pattern.compile("/E-([0-9]+)$");

	private static Pattern eventRowPattern = Pattern.compile("<tr uid=.*?</tr>", Pattern.DOTALL);
	private static Pattern eventDescPattern = Pattern.compile("<td class=\"go_event_col\">.*?<a href=\"(.*?)\".*?>(.*?)</a>", Pattern.DOTALL);	
	private static Pattern eventDatePattern = Pattern.compile("<td class=\"go_date_col\">.*?<h5>(.*?)</h5>.*?<h6>(.*?)</h6>", Pattern.DOTALL);
	
	//@Override
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate) throws Exception {
		
		Map<String, EventHit> eventHitById = new HashMap<String, EventHit>();
		
		String[] siteExtensions = {"com", "es", "de"};
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		for(String siteExtension: siteExtensions) {
			Collection<EventHit> hits = new ArrayList<EventHit>();
			keywords = keywords.replace('+', ' ');
			for(String keyword : keywords.split(" ")){
				hits.addAll(getEventList(siteExtension, keyword, location, fromDate, toDate));
			}
			if (hits == null) {
				continue;
			}
			for(EventHit hit: hits) {
				if (eventHitById.get(hit.getId()) != null) {
					continue;
				}
				eventHitById.put(hit.getId(), hit);
				eventHits.add(hit);
			}
		}
		return eventHits;
	}
	
	private Collection<EventHit> getEventList(String siteExtension, String keywords, String location, Date fromDate, Date toDate) throws Exception {
		DateFormat df = null;
		
		if (siteExtension.equals("com")) {
			df = new SimpleDateFormat("MMMM d y h:m a");
		} else if (siteExtension.equals("es")) {
			java.util.Locale lc = new java.util.Locale("es", "ES");
			df = new SimpleDateFormat("d MMMM y H:m", lc);
		} else if (siteExtension.equals("de")) {
			java.util.Locale lc = new java.util.Locale("de", "DE");
			df = new SimpleDateFormat("d MMMM y H:m", Locale.GERMAN);
		}
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		
		SimpleHttpClient httpClient = null;
		
		try {
			httpClient = HttpClientStore.createHttpClient();

			// http://www.viagogo.com/searchResults.aspx?SearchText=u2
			HttpGet httpGet = new HttpGet("http://www.viagogo." + siteExtension + "/searchResults.aspx?SearchText=" + keywords);
			
			httpGet.addHeader("Host", "www.viagogo." + siteExtension);		
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5 ");
			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");

			String content = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpGet);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity);
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
				
				if(null != httpGet){
					httpGet.releaseConnection();
				}
			}

			if (content.contains("returned no results")) {
				return null;
			}
	
			Matcher dataMatcher = dataPattern.matcher(content);
			if (!dataMatcher.find()) {
				return null;
			}
			
			String jsonData = dataMatcher.group(1);			
			JSONObject jsonObject = new JSONObject(jsonData);			
			JSONArray recordsJSONArray = jsonObject.getJSONArray("records");
						
			Map<String, EventHit> incompleteEventHitById = new HashMap<String, EventHit>();
			List<EventHit> incompleteEventHits = new ArrayList<EventHit>();

			for(int i = 0 ; i < recordsJSONArray.length() ; i++) {
				// [new Date(1253143800000),300.00000681226533,"Rogers Centre","Toronto",0,"CA","Canada",15,48,"189726"]
				// event date, starting price, venue name, venue city, unknown, country code, country name, unknown, unknown , event id
				JSONArray recordJSONArray = recordsJSONArray.getJSONArray(i);
				String javascriptDateString = recordJSONArray.getString(0);
				Matcher javascriptDateMatcher = javascriptDatePattern.matcher(javascriptDateString);
				javascriptDateMatcher.find();

				String eventLocation = recordJSONArray.getString(2) + ", " + recordJSONArray.getString(3) + ", " + recordJSONArray.getString(6);
				
				String eventId = recordJSONArray.getString(9);
				EventHit eventHit = new EventHit(eventId, "", null, null, eventLocation, Site.VIAGOGO, null);
				incompleteEventHitById.put(eventId, eventHit);
				incompleteEventHits.add(eventHit);
			}

			// we can get the first 20 event name in the html page itself

			Pattern eventGridPattern = Pattern.compile("<table.*?id=\"ctl00_centerContent_eventsPanel_eventsGrid_grid\".*?>(.*?)</table>", Pattern.DOTALL);
			Matcher eventGridMatcher = eventGridPattern.matcher(content); 
			if (!eventGridMatcher.find()) {
				return null;
			}
			
			String eventGrid = eventGridMatcher.group(1);
			completeEventHits(siteExtension, eventHits, incompleteEventHitById, eventGrid, df);
						
			
			// for the events after the 20th, we need to get them by doing an AJAX call
			final int eventPacketSize = 20;
			for (int k = 0 ;  k < incompleteEventHits.size() ; k+= eventPacketSize) {
				String eventIds = null;

				for (int j = k ;  j < incompleteEventHits.size() && j < k + eventPacketSize ; j++) {
					EventHit eventHit = incompleteEventHits.get(j);
					// the event hit is already complete, skip it
					if (eventHit.getUrl() != null) {
						continue;
					}
					if (eventIds == null) {
						eventIds = "\"" + eventHit.getEventId() + "\"";
					} else {
						eventIds += ", " + "\"" + eventHit.getEventId() + "\"";
					}
				}
				
				if (eventIds == null) {
					continue;
				}
				// this html post allows us to get the event href and the event name			
				HttpPost httpPost = new HttpPost("http://www.viagogo." + siteExtension + "/WebServices/SearchWebService.asmx/GetSearchResults");
				httpPost.addHeader("Host", "www.viagogo." + siteExtension);		
				httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
				httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
				httpPost.addHeader("Accept-Encoding", "gzip,deflate");
				httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
				httpPost.addHeader("Content-Type", "application/json; charset=utf-8");			
				httpPost.addHeader("Referer", "http://www.viagogo." + siteExtension + "/searchResults.aspx?SearchText=" + keywords);
	
				String postData = "{\"searchText\":\"" + keywords + "\",\"recordIDs\":[" + eventIds + "]}";
				httpPost.setEntity(new StringEntity(postData));
				
				try{
					response = httpClient.execute(httpPost);
					HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
					//ensure it is fully consumed
					if(entity != null){
						jsonData = EntityUtils.toString(entity);
						EntityUtils.consumeQuietly(entity);
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					//to reuse connection we need to close response stream associated with that
					if(response != null)
						response.close();
				}

				JSONObject jsonHtmlObject = new JSONObject(jsonData);
				String html = jsonHtmlObject.getString("d");
				
				completeEventHits(siteExtension, eventHits, incompleteEventHitById, html, df);
			}			
		} finally {
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}

		
		Collection<EventHit> result = new ArrayList<EventHit>();
		for (EventHit eventHit: eventHits) {
			Date date = new Date(eventHit.getDate().getTime() + 60000);
			if (date.before(fromDate)
				|| date.after(toDate)) {
				continue;
			}
			
			result.add(eventHit);
		}
		
		return result;
	}

	private void completeEventHits(String siteExtension, Collection<EventHit> eventHits, Map<String, EventHit> incompleteEventHitById, String content, DateFormat dateFormat) {
		Matcher eventRowMatcher = eventRowPattern.matcher(content); 

		while(eventRowMatcher.find()) {
			
			String row = eventRowMatcher.group(0);
			
			Matcher eventDescMatcher = eventDescPattern.matcher(row);
			eventDescMatcher.find();
			String eventHref = "http://www.viagogo." + siteExtension + eventDescMatcher.group(1); 
			String eventName = TextUtil.removeExtraWhitespaces(eventDescMatcher.group(2).trim());
	
			Matcher eventMatcher = eventIdPattern.matcher(eventHref);
			if (!eventMatcher.find()) {
				continue;
			}
			String eventId = eventMatcher.group(1);
	
			EventHit eventHit = incompleteEventHitById.get(eventId);
			if (eventHit == null) {
				continue;
			}
			
			Matcher eventDateMatcher = eventDatePattern.matcher(row);
			eventDateMatcher.find();
			String dateStr = eventDateMatcher.group(1);
			String timeStr = eventDateMatcher.group(2);

			try {
				Date date = dateFormat.parse(dateStr + " " + timeStr);
				eventHit.setDate(new java.sql.Date(date.getTime()));
				eventHit.setTime(new java.sql.Time(date.getTime()));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				continue;
			}
			
			eventHit.setName(eventName);
			eventHit.setUrl(eventHref);
	
			eventHits.add(eventHit);
			
		}
	}
	
	public static void main(String[] args) throws Exception {
//		ViagogoEventListingFetcher fetcher = new ViagogoEventListingFetcher();
	//	fetcher.getEventList("es", "andre rieu", "", null, null);

	String str = "\t\t\u003ctd class=\"go_date_col\"\u003e\r\n\t\t\t\t\t\u003ch4\u003eTue-Tue\u003c/h4\u003e\r\n\t\t\t\t\t\u003ch5\u003eFebruary 23-\u003cbr/\u003eFebruary 23 2010\u003c/h5\u003e\r\n\t\t\t\t\t\u003ch6\u003e\u003c/h6\u003e\r\n\t\t\t\t\u003c/td\u003e\u003ctd class=\"go_event_col\"\u003e\r\n\t\t\t\t\t\u003ch4\u003e\u003ca href=\"/Sports-Tickets/Olympic-Winter-Games/Olympic-Ice-Hockey-Tickets/E-182727\" id=\"ctl02_eventGrid_grid_ctl37_TargetLink\" title=\"Ice Hockey - Qualification Playoff - Men at Canada Hockey Place Vancouver on February 23 ";
	Matcher m = eventDatePattern.matcher(str);
	m.find();
	System.out.println(m.group(2));
	}

}
