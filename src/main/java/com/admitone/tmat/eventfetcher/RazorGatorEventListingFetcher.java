package com.admitone.tmat.eventfetcher;

import java.io.InputStream;
import java.io.StringReader;
import java.sql.Time;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.ccil.cowan.tagsoup.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class RazorGatorEventListingFetcher extends EventListingFetcher {
	private Logger logger = LoggerFactory.getLogger(RazorGatorEventListingFetcher.class);
	
	private static final Pattern eventIdPattern = Pattern.compile(".*performance=([\\d]+)|1");
	private static final Pattern htmlContentPattern = Pattern.compile("PerformanceHtm\":\"(.*</[a-z]+>)\"", Pattern.DOTALL);
	private static final Pattern allFieldsPattern = Pattern.compile(">([^<]+)</h1>\\s*<p>\\s*([^<]+)\\s*<br />\\s*([^<]+)\\s*<br />\\s*([^<]+)\\s*<br />");
	
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate) throws Exception{
		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		SimpleDateFormat inputDateFormat = new SimpleDateFormat("MMM dd, yyyy");
		if (location == null) {
			location = "";
		} else {
			location = location.replaceAll(" ", "+");
		}
		
		String data = "{\"filter\":{\"__type\":\"Razorgator.Web.AjaxWebServiceProxies.WSEventFilter\",\"EventId\":\"0\",\"EventBrokerId\""
                    + ":\"0\",\"SortKey\":\"EventDate\",\"SortDirection\":\"Ascending\",\"PageIndex\":\"[INDEX]\",\"EventString\":\"\",\"LocationString\""
                    + ":\"" + location + "\",\"DayString\":\"\",\"TimeSelection\":\"\",\"CategoryString\":\"\",\"StartDate\":\"" + inputDateFormat.format(fromDate) + "\",\"EndDate\":\""
                    + inputDateFormat.format(toDate) + "\"}}";
		keywords = keywords.replaceAll("+"," ");
		for(String keyword : keywords.split(" ")) {
		SimpleHttpClient httpClient = null;
		try {
			// test if it is a redirect URL (directed to the event page directly if only on hit)
			
			String eventUrl = "http://www.razorgator.com/tickets/pages/search/?keyword=" + keyword;
			HttpGet httpGet = new HttpGet(eventUrl);
			httpGet.setHeader("Host", "www.razorgator.com");
			httpGet.setHeader("User-agent", "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.10) Gecko/2009042523 Ubuntu/9.04 (jaunty) Firefox/3.0.10");
			httpGet.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
			httpGet.setHeader("Accept-Language", "en-us,en;q=0.5");
			httpGet.setHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpGet.setHeader("Keep-Alive", "300");
			httpGet.setHeader("Connection", "keep-alive");
			httpGet.getParams().setBooleanParameter("http.protocol.handle-redirects", false);
			httpClient = HttpClientStore.createHttpClient();
			String content = null;
			
			CloseableHttpResponse response = null;
			try{
				int responseCode = 0;
				String redirectedUrl = new String();
				try{
				response = httpClient.execute(httpGet);
				responseCode = response.getStatusLine().getStatusCode();
				if (responseCode == 302) {
					redirectedUrl = response.getHeaders("Location")[0].getValue();
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
				if (responseCode == 302) {
					
					httpGet = new HttpGet("http://www.razorgator.com" + redirectedUrl.replace("|", "%7C"));
					httpGet.setHeader("Host", "www.razorgator.com");
					HttpEntity entity = null;
					try{
						response = httpClient.execute(httpGet);
						entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
						//ensure it is fully consumed
						if(entity != null){
							content = EntityUtils.toString(entity);
							EntityUtils.consumeQuietly(entity);
						}
					}catch(Exception e){
						e.printStackTrace();
					}finally{
						//to reuse connection we need to close response stream associated with that
						if(response != null)
							response.close();
					}
					Matcher matcher = allFieldsPattern.matcher(content);
					if (!matcher.find()) {
						System.out.println("*** NO MATCH");
						return eventHits;
					}
					String name = matcher.group(1);
					String dateStr = matcher.group(2);
					String timeStr = matcher.group(3);
					String eventLocation = matcher.group(4);
					addResult(eventHits, name, redirectedUrl, eventLocation, dateStr, timeStr, location, fromDate, toDate);
					return eventHits;
				}
			}catch(Exception e){
				e.printStackTrace();
			}			

			int numEvents;
			int index = 0;
			
			do {
//				HttpPost post = new HttpPost("http://www.razorgator.com/AjaxWebServiceProxies/PerformanceListing.asmx/GetFilteredPerformances");
				HttpPost post = new HttpPost("http://www.razorgator.com/AjaxWebServiceProxies/SearchListing.asmx/GetSearchData");
				post.setHeader("Host", "www.razorgator.com");
//				post.setHeader("User-Agent", "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.10) Gecko/2009042523 Ubuntu/9.04 (jaunty) Firefox/3.0.10");
//				post.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
//				post.setHeader("Accept-Language", "en-us,en;q=0.5");
				post.setHeader("Accept-Encoding", "gzip,deflate");
//				post.setHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
				post.setHeader("Keep-Alive", "300");
				post.setHeader("Connection", "keep-alive");
				post.setHeader("Content-Type", "application/json; charset=utf-8");
				post.setHeader("Referer", eventUrl);
				post.setEntity(new StringEntity(data.replace("[INDEX]", (index++) + "")));

				Parser parser = new Parser();
				SAX2DOM sax2dom = new SAX2DOM();
				InputStream is = null;
				byte[] b = new byte[10000000];
				try{
					response = httpClient.execute(post);
					parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
					
					HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
					//ensure it is fully consumed
					if(entity != null){
						is = entity.getContent();
						// to define the html: prefix (off by default)
						int len;
						int n = 0;
						while ((len = is.read(b, n, b.length - n)) > 0) {
							n += len;
						}
						EntityUtils.consumeQuietly(entity);
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					//to reuse connection we need to close response stream associated with that
					if(response != null)
						response.close();
				}
				
				content = StringEscapeUtils.unescapeJava(new String(b));
				Matcher matcher = htmlContentPattern.matcher(content);
				if (!matcher.find()) {
					break;
				}
				
				content = matcher.group(1);
				parser.setContentHandler(sax2dom);
				parser.parse(new InputSource(new StringReader(content)));
				Node document = sax2dom.getDOM();
				NodeList eventNodes = XPathAPI.selectNodeList(document, "//html:tr[html:td/@id='tdParticipant']");
				numEvents = eventNodes.getLength();
				System.out.println("************************************** NUM EVENT " + numEvents);
				for (int i = 0; i < numEvents; i++) {
					Node node = eventNodes.item(i);
					String name = TextUtil.removeExtraWhitespaces(XPathAPI.selectSingleNode(node, "html:td[@id='tdParticipant']").getTextContent()).trim();				
					String url = "http://www.razorgator.com" + XPathAPI.selectSingleNode(node, "html:td[@id='tdParticipant']/html:a/@href").getTextContent();
					String eventLocation = XPathAPI.selectSingleNode(node, "html:td[@id='tdVenue']").getTextContent();
					String dateStr = XPathAPI.selectSingleNode(node, "html:td[@id='tdDate']/html:p").getTextContent();
					
					String timeStr = XPathAPI.selectSingleNode(node, "html:td[@id='tdDate']/html:span").getTextContent();
					
					addResult(eventHits, name, url, eventLocation, dateStr, timeStr, location, fromDate, toDate);
				}
			} while(numEvents > 0);
			
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}
		}
		return eventHits;
	}
	
	private void addResult(Collection<EventHit> eventHits, String name, String url,
						   String eventLocation, String dateStr, String timeStr, String location,
						   Date fromDate, Date toDate) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE MMMMM dd yyyy");
		SimpleDateFormat timeFormat = new SimpleDateFormat("h:m a");

		java.sql.Date eventDate = null;
		Time eventTime = null;
		
		try {
			dateStr = dateStr.replaceAll(",", " ").replaceAll("\\s+", " ").trim();
			eventDate = new java.sql.Date(dateFormat.parse(dateStr).getTime());
			
			if (eventDate.before(fromDate) || eventDate.after(toDate)) {
				return;
			}
			
			eventTime = new java.sql.Time(timeFormat.parse(timeStr).getTime());
		} catch (ParseException e) {
			// must be a TBD or empty string
		}
		
		if (location != null) {
			if (eventLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {
				return;
			}
		}
		
		Matcher matcher = eventIdPattern.matcher(url);
		if (!matcher.find()) {
			logger.info("problem obtaining eventId: " + url);
			return;
		}
		
		String eventId = matcher.group(1);
		EventHit eventHit = new EventHit(eventId, name, eventDate, eventTime, eventLocation, Site.RAZOR_GATOR, url);
		eventHits.add(eventHit);
	}
}
