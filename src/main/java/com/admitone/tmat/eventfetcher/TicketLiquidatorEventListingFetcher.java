package com.admitone.tmat.eventfetcher;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * /*EventListing Fetcher for Ticket Liquidator Exchange
 * 
 * @author Nagarajan
 */

public class TicketLiquidatorEventListingFetcher extends EventListingFetcher {

	private static Logger logger = LoggerFactory
			.getLogger(TicketLiquidatorEventListingFetcher.class);
	private Element td;

	@Override
	public Collection<EventHit> getEventList(String keywords, String location,
			Date fromDate, Date toDate, List<Event> eventList, boolean isVenue,
			boolean isStubhubFromTicketEvolution) throws Exception {

		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		// Replace space to Hyphen for tick pick events
		keywords = keywords.replaceAll("\\s+", "+");
		SimpleHttpClient httpClient = null;

		try {
			httpClient = HttpClientStore.createHttpClient(Site.TICKET_LIQUIDATOR);
			String ticketLiquidatorFeedUrl = "http://www.ticketliquidator.com/resultsgeneral.aspx?q="+ keywords + "";
			Collection<EventHit> eventResults = getEventsFromTicketLiquidator(ticketLiquidatorFeedUrl, httpClient, keywords, location,
					fromDate, toDate, eventList, isVenue);
			eventHits.addAll(eventResults);

		} catch (Exception e) {
			logger.error("SHELF Caught Exception: " + e);
			e.printStackTrace();
		} finally {
			if (httpClient != null) {
				HttpClientStore.releaseHttpClient(httpClient);
			}
		}

		return eventHits;
	}

	/**
	 * Fetch events from Ticket Liquidator exchange
	 * 
	 * @param feedUrl
	 * @param httpClient
	 * @param keywords
	 * @param location
	 * @param fromDate
	 * @param toDate
	 * @param eventList
	 * @param isVenue
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws ParseException
	 */

	public Collection<EventHit> getEventsFromTicketLiquidator(String feedUrl,
			CloseableHttpClient httpClient, String keywords, String location,
			Date fromDate, Date toDate, List<Event> eventList, boolean isVenue)
			throws ClientProtocolException, IOException, ParseException {

		SimpleDateFormat format = new SimpleDateFormat("MM-dd-yy hh:mm aa z");
		SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");

		Date beforeDate = new Date();
		java.sql.Date sqlFromDate = new java.sql.Date(new java.util.Date(fromDate.getTime() - 24 * 3600 * 1000).getTime());
		java.sql.Date sqlToDate = new java.sql.Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());

		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");

		Map<String, Boolean> locationMap = new HashMap<String, Boolean>();
		Map<String, Boolean> dateMap = new HashMap<String, Boolean>();
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();

		for (Event event : eventList) {
			if (event.getDate() == null) {
				dateMap.put("TBD", true);
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ")+ ":-:"
								+ event.getVenue().getCity().replaceAll("\\s+", " ")
								+ ":-:"+ event.getVenue().getState().replaceAll("\\s+", " "), true);
				continue;
			}
			dateMap.put(df.format(event.getDate()), true);
			eventMap.put(event.getId(), event);
		}

		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();

		HttpGet httpPost = new HttpGet(feedUrl);
		httpPost.setHeader("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpPost.addHeader("Host", "www.ticketliquidator.com");
		httpPost.addHeader("Accept","application/json, text/javascript, text/html, application/xhtml+xml, application/xml,*/*");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");

		// Hit the ticket liquidator site and get the response
		CloseableHttpResponse response = null;
		String ticketLiquidatorResult = null;
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				ticketLiquidatorResult = EntityUtils.toString(entity).trim();
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != httpPost){
				httpPost.releaseConnection();
			}
		}
		
		Date currentDate = new Date();
		Document doc = Jsoup.parse(ticketLiquidatorResult);
		

		Element tableElement = doc.getElementById("id_events_tbl_color events_tbl_micro");
		Elements trTags = tableElement.select("tbody tr");
		
		int count = 0;
		for (Element tr : trTags) {

			try {
				if (count == 0) {
					count++;
					continue;
				}
				count++;
				int i = 0;
				String eventId = "", eventName="", eventVenue = "";
				String  eventLocation_str = "";
				String  eventLocation= "";
				String dateStr = "", timeStr = "", day = "";
				Double price = 0.0;
				Integer qty = 0;
				String url = "";
				String dtStart ,eventYear="",eventTime="";
				String eventDateStr = "";
				String eventDateStr1 ="";
				String[] res=null;

				Elements tdTags = tr.getElementsByTag("td");
				for (Element td : tdTags) {

					String[] tdstrArr = null;
					switch (i) {
					case 0:
						eventName = td.text().trim();
						url = "https://www.ticketliquidator.com"+ td.getElementsByTag("a").attr("href");
						Pattern eventIdPattern = Pattern.compile(".*evtid=(.*)");
						Matcher eventIdMatcher = eventIdPattern.matcher(url);
						if (eventIdMatcher.find()) {
							eventId = eventIdMatcher.group(1);
						}
						break;
					case 1:

						tdstrArr = td.text().trim().split(",");
						day = tdstrArr[0].trim();
						dtStart = tdstrArr[1].trim();
												
						String dateRes[] = dtStart.split("\\s+");
						String Month = dateRes[0];
						String date = dateRes[1];
					
						timeStr = tdstrArr[2].trim();
						res = timeStr.split("\\s+");
												
						eventYear = res[0].trim();
						eventTime = res[2] + " "+res[3];
					  	for (i = 4; i < res.length; i++) {
							eventLocation_str +=" "+ res[i];
						}

						//eventDateStr1 = day + " " + dtStart + " " + eventYear+ " " + res[2]+" "+res[3];
						eventDateStr1 = day + " " + dtStart + " " + eventYear+ " " + eventTime;
											
						eventVenue = tdstrArr[3].trim();
						eventLocation =eventLocation_str+ ","+eventVenue;
						eventDateStr = day + "," + dtStart + "," + eventYear+ " at " + eventTime;
                        break;

					}
					i++;
				}

				DateFormat readFormat = new SimpleDateFormat( "EEE MMM dd yyyy hh:mm aaa");
			    DateFormat writeFormat = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss");
			    Date date2 = null;
			    try {
			       date2 = readFormat.parse( eventDateStr1 );
			    } catch ( ParseException e ) {
			        e.printStackTrace();
			    }
             			 			    
				java.sql.Date date = new java.sql.Date(date2.getTime());
                int check =0;
				if (date2.after(fromDate)&& date2.before(toDate)) {
				
						// Event name for the matched date
						//System.out.println(" Check the event id :" + eventId);
						// Location filter
						if (location != null) {
							if (eventLocation.toLowerCase().indexOf(
									eventLocation.toLowerCase()) < 0) {
								continue;
							}
						}

						SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
						long ms = sdf.parse(eventTime).getTime();
						Time t = new Time(ms);
												
						if (!eventIds.contains(eventId)) {
							System.out.println("SHELF SPLITWORDS: " + eventName);
							EventHit eventHit = new EventHit(eventId, eventName,date, t, eventLocation, Site.TICKET_LIQUIDATOR,url);
							eventHits.add(eventHit);
							eventIds.add(eventId);
						}
					  } 

					}
			catch (Exception e) {
       		 e.printStackTrace();
       	 }
		}

		return eventHits;
	}

	// test
	
	 /*public static void main(String[] args) throws Exception {
		 SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy"); 
		 Date fromDate = format.parse("04/23/2016");
		 Date toDate =  format.parse("04/23/2016");
		 Event event = new Event();
	  event.setLocalDate(format.parse("04/23/2016"));
	  List<Event> eventList = new ArrayList<Event>(); 
	  eventList.add(event); 
	  Collection<EventHit> hits =  new TicketLiquidatorEventListingFetcher().getEventList("mormon", null, fromDate, toDate, eventList, false, false);
	  System.out.println(hits.toString());
	  
	 }*/
	 
}
