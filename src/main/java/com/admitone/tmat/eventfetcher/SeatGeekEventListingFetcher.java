package com.admitone.tmat.eventfetcher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;


public class SeatGeekEventListingFetcher extends EventListingFetcher {

		
	
public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception {
		
		
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		SimpleHttpClient httpClient = null;
		
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd");
				Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
				
				
				httpClient = HttpClientStore.createHttpClient(Site.SEATGEEK);
				String feedUrl = "";

				feedUrl = "https://seatgeek.com/search?search=";
				String Key = URLEncoder.encode(keywords, "UTF-8");
				feedUrl= feedUrl+Key;
				
					
				System.out.println("FEED URL=" + feedUrl);
				Collection<EventHit> subResult= getEventsFromExchange(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
				eventHits.addAll(subResult);
				 
				System.out.println("Matched Events :" + subResult+ "*****");
							
								
			} catch (Exception e) {
				System.out.println("SHELF Caught Exception: " + e);
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
			}
//		}
		return eventHits;
	}



public Collection<EventHit> getEventsFromExchange(String feedUrl,CloseableHttpClient httpClient,String keywords,String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws ClientProtocolException, IOException, JSONException, ParseException{
	
	
			SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm aa");
			DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");

			Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
			Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
			Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
			Time time;
			
			for(Event event:eventList){
				if(event.getDate()==null){
					dateMap.put("TBD", true);
					locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
					continue;
				}
				//dateMap.put(df.format(event.getDate()), true);
				eventMap.put(event.getId(), event);
			}
	
		
	
	  
	  
			httpClient=new DefaultHttpClient();
			String venueResult1;
			HttpPost httpPostVenue = new HttpPost(feedUrl);
			httpPostVenue.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpPostVenue.addHeader("Accept", "application/json, text/javascript, * /*");
			httpPostVenue.addHeader("Accept-Encoding", "gzip,deflate");
			
			CloseableHttpResponse venueResponse = null;
			CloseableHttpResponse redirectVenueResponse=null;
			String venueResult = null;
			try{
				venueResponse = httpClient.execute(httpPostVenue);
				if (venueResponse.getStatusLine().getStatusCode() == 302) 
		  		{
		  			String redirectURL = venueResponse.getFirstHeader("Location").getValue();
					//System.out.println("redirectURL"+redirectURL);
					
					HttpGet httpPostredirectVenue = new HttpGet(redirectURL);
					redirectVenueResponse = httpClient.execute(httpPostredirectVenue);
					
					HttpEntity entity = HttpEntityHelper.getUncompressedEntity(redirectVenueResponse.getEntity());
					//ensure it is fully consumed
					if(entity != null){
						venueResult = EntityUtils.toString(entity).trim();
						EntityUtils.consumeQuietly(entity);
					}

				}
		  	else
				{
					
					  //System.out.println("--------------");
					  System.out.println("Data not Found");	  
					  //System.out.println("--------------");
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(venueResponse != null)
					venueResponse.close();
				if(redirectVenueResponse != null)
					redirectVenueResponse.close();
			}
			
	   
			File file = new File("SeatGeekxmldata.xml");
	
			// if file doesnt exists, then create it
			
			if (!file.exists()) 
				{
					file.createNewFile();
				}
			if (venueResult==null)
			{
					System.out.println("no value");
			}
			
			
			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);
			bw.write(venueResult);
			System.out.println("------Done Extracting Data----------");
			//bw.close();
	  
	 
	 
	        Pattern pattern = Pattern.compile("(<div class=\"page-event-listing\"|<div class=\"event-listing-time\"| <div class=\"event-listing-details flex-child\"|<div class=\"event-listing-compare flex-child\"|<span style=\"display:none\" class=\"summary\" |<span class=\"subtitle\"|<span class=\"title\")(.*?)(</div>)");


			BufferedReader bf = new BufferedReader(new FileReader("SeatGeekxmldata.xml"));
			StringBuilder sb = new StringBuilder();

			String line;

			while ((line = bf.readLine()) != null)
			 {

					sb.append(line);
			 }


			File file1 = new File("seatGeekxmloutdata.xml");
			Matcher matcher = pattern.matcher(sb);
			

		  // if file doesnt exists, then create it
			if (!file1.exists()) 
			{
				file1.createNewFile();
			}

			FileWriter fw1 = new FileWriter(file1);
			


		     // Find all matches
		    while (matcher.find()) 
		     {
		    	 
		    		    	
		    		String match1 = matcher.group(1);
		    		fw1.write(match1);
		    		
		    		String match2 = matcher.group(2);
		    		fw1.write(match2);
		    		
		    		String match3 = matcher.group(3);
		    		fw1.write(match3);
		    	    		
		    	
		    	}
		    
		    fw1.close();
		    System.out.println("------done----Parsing Data--------");
		   

	
		
		    BufferedReader bf1 = new BufferedReader(new FileReader("seatGeekxmloutdata.xml"));
		    StringBuilder sb1 = new StringBuilder();
        
      
		    String line1;
		    int i=1,j=0,k=0,sub=1,c=0,d=0,e=0,f=0,g=0,h=0,q=0,dd=0;
		    boolean nocost_flag=false;
    
		    while ((line1 = bf1.readLine()) != null)

		    {

		    		sb1.append(line1);
		    }
			
			 Pattern[] a = new Pattern[10] ;
			
			 String[] date_d = new String[2000];
			 String[] time_d =new  String[1000];
			 String[] event_title =new  String[1000];
			 String[] event_venue=new  String[1000];
			 String[] event_location =new  String[1000];
			 String[] event_region =new  String[1000];
			 String[] event_cost_from =new  String[1000];
			 String[] event_price =new  String[1000];
			 String[] url_d = new String[1000];
			 Date[] event_date = new Date[1000];
		
			 
			 
			 
			 if (i==1)
			 
			 { 
			 
		 
				 	a[i]=  Pattern.compile ("(<div class=\"event-listing-date\".*?>)(.*?)(</div>)");
				 	Matcher matcher1= a[i].matcher(sb1);
    	

		
				 	while(matcher1.find())
				 	{
			
				 		String match1= matcher1.group(1);
						String match2= matcher1.group(2);
						String match3= matcher1.group(3);
						//System.out.println(match1);
						//System.out.println(match2);
						date_d[j]=match2;
						//System.out.println(match3);
						j++;
					}
					
				 		i++;
				}		
		 
			 
			 
		 /* Pattern Matching for Time */
			 if (i==2)
			 { 
			 
		 
					 a[i]= Pattern.compile ("(<div class=\"event-listing-time\".*?>)(.*?)(</div>)");
					 Matcher matcher1= a[i].matcher(sb1);
    	
	
		
					while(matcher1.find())
					{
						
						String match1= matcher1.group(1);
						String match2= matcher1.group(2);
						String match3= matcher1.group(3);
						//System.out.println(match1);
						//System.out.println(match2);
						time_d[k]=match2;
						//System.out.println(match3);
						k++;
					}
					
					i++;
			 }		
		 
		 
		
			 if (i==3)
			 { 
			 
		 
					 a[i]= Pattern.compile ("(<a class=\"event-listing-title\".*?<span>|itemprop=\"name\">)(.*?)(</span>)");
					 Matcher matcher1= a[i].matcher(sb1);
    	
	
		
				while(matcher1.find())
				{
					
					String match1= matcher1.group(1);
					String match2= matcher1.group(2);
					String match3= matcher1.group(3);
					//System.out.println(match1);
					//System.out.println(match2);
					//time_d[k]=match2;
					//System.out.println(match3);
					
				
					if (sub==1)
					{
						event_title[c] = match2;
						//System.out.println(event_title[c]);
						sub++;
						c++;
					}
					else if (sub==2)
					{
						event_venue[d]=match2;
						//System.out.println(event_venue[d]);
						sub=1;
						d++;
					}
					
					
				    }		
					 
					i++; 
			      }
	
		 
		 
				 if (i==4)
				 { 
					 
				 
					 a[i]= Pattern.compile ("(<span>|class=\"locality\".*?>|class=\"region\".*?>|class=\"country\".*?>)(.*?)(</span>)");
					 Matcher matcher1= a[i].matcher(sb1);
		    	
			
				
				while(matcher1.find())
				{
					
					String match1= matcher1.group(1);
					String match2= matcher1.group(2);
					String match3= matcher1.group(3);
					//System.out.println(match1);
					//System.out.println(match2);
					//time_d[k]=match2;
					//System.out.println(match3);
					
				
				if (sub==1)
				{
					event_location[e] = match2;
					//System.out.println(event_title[c]);
					sub++;
					e++;
				}
				else if (sub==2)
				{
					event_region[f]=match2;
					//System.out.println(event_venue[d]);
					sub=1;
					f++;
				}
				
			}		
				 
				i++; 
		      }
				 
			
				 if (i==5)
				 { 
					 
					 a[i]=Pattern.compile ("(<span class=\"subtitle\">|class=\"title\">)(.*?)(</span>)");
					 Matcher matcher1= a[i].matcher(sb1);
				     while(matcher1.find())
					{
						
						String match1= matcher1.group(1);
						String match2= matcher1.group(2);
						String match3= matcher1.group(3);
						//System.out.println(match1);
						//System.out.println(match2);
						//System.out.println(match3);
						
						
						
						if (sub==1)
						{
							event_cost_from[g]=match2;
							//System.out.println(match2);
							if (match2.trim().equals("FROM"))
							{
								event_cost_from[g]=match2;
								//System.out.println(event_cost_from[g]);
								sub++;
								g++;
								
							}
							else 
							{
							event_cost_from[g]="---";
							//System.out.println(event_cost_from[g]);
							nocost_flag=true;
							sub=1;
							g++;
							if (nocost_flag)
							{
								
								event_price[h]="BUY NOW";
								
								nocost_flag=false;
								//System.out.println(event_price[h]);
								h++;
							}
							
							}
							//System.out.println(event_cost_from[g]);
							//sub++;
							
							//System.out.println(match2+ "final");
						}
						
						else if (sub==2)
						{
							//event_price[h]=match2;
							//System.out.println(nocost_flag);
							
							
							event_price[h]=match2;
							//System.out.println(event_price[h]);
							h++;
							sub=1;
							
							//System.out.println("------------" +event_price[h]);
							
							
						}
						
						
					}
					
					i++;
						
						
					}
				 
						
		 

				 if (i==6)
				 { 
					 
					 a[i]=Pattern.compile("(<a class=\"event-listing-title\" itemprop=\"url\".*?href=\"/)(.*?)(/\">)");
					 Matcher matcher1= a[i].matcher(sb1);
				     while(matcher1.find())
					{
						
						String match1= matcher1.group(1);
						String match2= matcher1.group(2);
						String match3= matcher1.group(3);
						//System.out.println(match1);
						//System.out.println(match2);
						//System.out.println(match3);
						//System.out.println(match2+"====");
						
						
						url_d[q]=match2;
						q++;
						
						}
				     i++;
						
					}
					
				 

				 if (i==7)
				 { 
					 
					 a[i]=Pattern.compile ("(<div class=\"event-listing-date\" itemprop=\"startDate\".*?content=\")(.*?)(\">)");
					 Matcher matcher1= a[i].matcher(sb1);
				     while(matcher1.find())
					{
						
						String match1= matcher1.group(1);
						String match2= matcher1.group(2);
						// System.out.println(match2);
						Date e_date = df1.parse(match2);
						//System.out.println(e_date);
						//String match21= df.format(e_date);
						String match3= matcher1.group(3);
						//System.out.println(match1);
						//System.out.println(match2);
						//System.out.println(match3);
						
						
						
					 event_date[dd]=e_date;
					 System.out.println(dd);
					 
						dd++;
						
						}
				     i++;
						
					}
					
				

			
		 
			//System.out.println("" + c + " " + d + " " + e+ " " + f + " " + g + " " + h+ " " + j + " " + k + " ");
	
		 
		 int n=0,m=0;
		 for(n=0; n<j && n<k ;n++) // take j and i or length instead
		 {
			 System.out.println("===========================================");
			 System.out.println("Date of Event "+ (m+1) +" is :" + date_d[m]);
			 System.out.println("Time of Event "+ (m+1) +" is :"+ time_d[m]);
			 System.out.println("Event " +  (m+1) + " Title" +" is :"+ event_title[m]);
			 System.out.println("Event " +  (m+1) + " Venue" +" is :"+ event_venue[m]);
			 System.out.println("Event " +  (m+1) + " Location" +" is :"+ event_location[m]);
			 System.out.println("Event " +  (m+1) + " Region" +" is :"+ event_region[m]);
			 System.out.println("Event " +  (m+1) + " Price Starts From " + event_cost_from[m]);
			 System.out.println("Event " +  (m+1) + " Price" +" is :"+ event_price[m]);
			 System.out.println("Event " +  (m+1) + " EVENT Date" +" is :"+ event_date[m]);
			 System.out.println("===========================================");
			 m++;
		 }
		 
		 
		 	Collection<String> eventIds = new ArrayList<String>();
			Collection<EventHit> eventHits = new ArrayList<EventHit>();
			for (int p = 0 ; p <j && p<k; p++) 
			
			{
				
				String day="";
				String dateStr="";
				String timeStr="";
				String name="";
				String venue="";
				String url_s="";
				String eventId="";
				//String [] spl=time_d[p].split("\\s+");
				   
				
				 /* day = spl[1] ;
				  System.out.println("day:" + day);
					   
					
				  timeStr =spl[2]+" "+spl[3];
			      System.out.println("time:" + timeStr);
				
				  dateStr = date_d[p].trim();
				  System.out.println("date:" + dateStr);*/
				
				  time_d[p]=time_d[p].replaceAll("^\\s+|\\s+$", "");
				  String [] spl=time_d[p].trim().split("\\s+");
				   
				  day = spl[1] ;
				  System.out.println("day:" + day);

				  if(spl[1].equals("TBD"))
				  {
					  timeStr =spl[1];
				  }
				  else
				  {
				  timeStr =spl[1]+" "+spl[2];
			      System.out.println("time:" + timeStr);
				  }
				
				  dateStr = date_d[p].trim();
				  System.out.println("date:" + dateStr);
				
					
				
				  name = event_title[p];
				  System.out.println("name:" + name);
				
				   
				  venue = event_venue[p];
				  System.out.println("venue:" + venue);
				
				  
				  url_s= "https://seatgeek.com/" + url_d[p];
				  System.out.println("url:" + url_s);
			
				  
				  String[] stet =url_d[p].split("/");
				  eventId= stet[stet.length-1];
				  System.out.println("EventID :"+ eventId);
					
				
				  
				 String dateTimeStr = (day+ " " +dateStr + " " + timeStr).replaceAll("\\s+", " ");
			
				
				Date date_e =event_date[p];
				System.out.println(date_e);
				
				if(!isVenue){
					if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), name)) {
						continue;
					}
				}else{
					if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), venue)) {
						continue;
					}
				}
				
				if (timeStr.equals("TBD"))
				{
					time=null;
				}
				else
				{
				Date timeS = timeFormat.parse(timeStr);
			    time = new Time(timeS.getTime());
				
				}
				
				
				String eventCity = null;
				String eventLocation = null;

			
				eventLocation = venue;
				eventCity =event_location[p] ; 
				
				String eventState = null;
				
				eventState = event_region[p];
				
				
				String eventFullLocation = eventLocation.trim() + ", " + eventCity.trim() + ", " + eventState.trim();
				System.out.println(eventFullLocation);
				
				if (location != null) {
					if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) { 
						continue;
					}
				}
				/*boolean flag=false;
				if(!dateMap.containsKey(df.format(date))){
					for(String key:locationMap.keySet()){
						String tokens[] = key.split(":-:");
						String building = tokens[0].trim();
						String cityToken = tokens[1].trim();
						String stateToken = tokens[2].trim();
						
						if( cityToken.equalsIgnoreCase(eventCity.trim())&& stateToken.equalsIgnoreCase(eventState.trim()) && (TextUtil.isSimilarVenue(building, eventLocation, 1))){
							flag=true;
							break;
						}
					}
				}else{
					flag=true;
				}*/
//				if(dateMap.containsKey(dateFormat.format(eventDate))){
				// check date - stubhub might return irrelevant dates
				
				System.out.println(fromDate + "====================================================");
				System.out.println(toDate);
				System.out.println(date_e);
			if (date_e.before(fromDate) || date_e.after(toDate)) {
					continue;
				}
			 java.sql.Date sqlDate_e = new java.sql.Date(date_e.getTime());
			
				if(/*flag && */!eventIds.contains(eventId)){	
					System.out.println("SHELF SPLITWORDS: " + name);
					EventHit eventHit = new EventHit(eventId, name,  sqlDate_e, time, eventFullLocation, Site.SEATGEEK, url_s);
					//System.out.println("Inserting data");
					eventHits.add(eventHit);
					eventIds.add(eventId);
					System.out.println("Done data");
				}

				
			}
			
			file.delete();
			file1.delete();
			return eventHits;
		
	}

 	public static void main(String args[])
 	{
 		SeatGeekEventListingFetcher st= new SeatGeekEventListingFetcher();
 		
		Date date1 = new Date("2014/12/10");
		Date date2 = new Date("2014/12/20");
		System.out.println(date1);
 		try {
			//st.getEventList("justin timberlake", "Duluth", date1, date2,false);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
 	}
	
	
		
	}



