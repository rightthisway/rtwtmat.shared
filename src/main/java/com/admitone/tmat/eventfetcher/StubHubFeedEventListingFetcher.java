package com.admitone.tmat.eventfetcher;

import java.io.IOException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.QueryUtil;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class StubHubFeedEventListingFetcher extends EventListingFetcher {
	public Collection<EventHit> getEventList(String keywords, String location, Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws Exception {
		
		
		
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		keywords = keywords.replaceAll("\\s+", " ");
		SimpleHttpClient httpClient = null;
//		for(String keyword : keywords.split(" ")) {
			try {
				SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
				Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
				String from = format.format(fromDate);
				String to = format.format(toTomorrowDate);
//				DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
				/*******/
			
				
				httpClient = HttpClientStore.createHttpClient(Site.STUBHUB_FEED);
				String feedUrl = "";
				
				
		//		feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active+maxPrice&wt=json";
				if(!isVenue){
					feedUrl = "http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent";
					if (keywords != null && keywords.length() > 0) {
						feedUrl += "%0D%0A%2Bdescription%3A" + keywords.replaceAll("\\s+", "+");			
					}
					
					if (from != null && to != null) {
						feedUrl += "%0D%0A%2Bevent_date%3A[" + from + "T00:00:00Z+TO+" + to + "T23:59:59Z]";
					}
					feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active&rows=50000&wt=json";
					System.out.println("FEED URL=" + feedUrl);
					Collection<EventHit> subResult= getEventsFromStubhub(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
					eventHits.addAll(subResult);
				}
				else{
					String venueString = keywords;//eventList.get(0).getVenue().getBuilding();
					String venueURL ="http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Avenue%20description%3A";
					venueURL =  venueURL + venueString.replaceAll("\\s+", "+") + "%0D%0A%0D%0A&version=2.2&fl=id+event+description+city+state+active&rows=20&wt=json";
					System.out.println(venueURL);
					HttpPost httpPostVenue = new HttpPost(venueURL);
					
					httpPostVenue.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//					httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
					httpPostVenue.addHeader("Accept", "application/json, text/javascript, */*");
					httpPostVenue.addHeader("Accept-Encoding", "gzip,deflate");
					
					CloseableHttpResponse response = null;
					String venueResult = null;
					try{
						response = httpClient.execute(httpPostVenue);
						HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
						//ensure it is fully consumed
						if(entity != null){
							venueResult = EntityUtils.toString(entity).trim();
							EntityUtils.consumeQuietly(entity);
						}
					}catch(Exception e){
						e.printStackTrace();
					}finally{
						//to reuse connection we need to close response stream associated with that
						if(response != null)
							response.close();
						
						if(null != httpPostVenue){
							httpPostVenue.releaseConnection();
						}
					}
					JSONObject venueJsonObject = null;
					
					System.out.println(venueResult);
					venueJsonObject = new JSONObject(venueResult);
				
					JSONArray docsJSONVenueArray = venueJsonObject.getJSONObject("response").getJSONArray("docs");
					Set<Integer> venueIdSet= new HashSet<Integer>();
					for(int i=0;i<docsJSONVenueArray.length();i++){
						JSONObject docJSONVenueObject = docsJSONVenueArray.getJSONObject(i);
//						System.out.println(docJSONVenueObject.getString("description").trim().toLowerCase());
						if(keywords.toLowerCase().equals(docJSONVenueObject.getString("description").trim().toLowerCase())){
							venueIdSet.clear();
							venueIdSet.add(docJSONVenueObject.getInt("id"));
							break;
						}
						venueIdSet.add(docJSONVenueObject.getInt("id"));
					}
					for(Integer venueId:venueIdSet){
						feedUrl = "http://www.stubhub.com/listingCatalog/select/?q=%2BstubhubDocumentType%3Aevent";
						if (keywords != null && keywords.length() > 0) {
							feedUrl += "%0D%0A%2Bgeography_parent%3A" + venueId;			
						}	
						if (from != null && to != null){
							feedUrl += "%0D%0A%2Bevent_date%3A[" + from + "T00:00:00Z+TO+" + to + "T23:59:59Z]";
						}
						feedUrl += "%0D%0A%0D%0A&version=2.2&fl=event_id+event_date_local+event_time_local+eventGeoDescription+description+city+state+active&rows=50000&wt=json";
						
						System.out.println("FEED URL=" + feedUrl);
						Collection<EventHit> subResult= getEventsFromStubhub(feedUrl,httpClient,keywords,location,fromDate,toDate,eventList,isVenue);
						eventHits.addAll(subResult);
					}
					
				}
				/*******/
				
				
			} catch (Exception e) {
				System.out.println("SHELF Caught Exception: " + e);
				e.printStackTrace();
			} finally {
				if (httpClient != null) {
					HttpClientStore.releaseHttpClient(httpClient);
				}
			}
//		}
		return eventHits;
	}
	
	public Collection<EventHit> getEventsFromStubhub(String feedUrl,CloseableHttpClient httpClient,String keywords,String location,Date fromDate, Date toDate,List<Event> eventList,boolean isVenue) throws ClientProtocolException, IOException, JSONException, ParseException{
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//		Date toTomorrowDate = new Date(new java.util.Date(toDate.getTime() + 24 * 3600 * 1000).getTime());
//		String from = format.format(fromDate);
//		String to = format.format(toTomorrowDate);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		if(null != eventList && eventList.size() > 0){
			for(Event event:eventList){
				if(event.getDate()==null){
					dateMap.put("TBD", true);
//					String location = event.getVenue().getBuilding();
					locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
					continue;
				}
				dateMap.put(df.format(event.getDate()), true);
				eventMap.put(event.getId(), event);
			}
		}
		
		HttpPost httpPost = new HttpPost(feedUrl);
		// System.out.println("stubhub>" + feedUrl);
		httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
//		httpPost.addHeader("Host", "partnerfeed.stubhub.com");		
		httpPost.addHeader("Accept", "application/json, text/javascript, */*");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		
//		httpClient = HttpClientStore.createHttpClient(Site.STUBHUB_FEED);

		String result = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				result = EntityUtils.toString(entity).trim();
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != httpPost){
				httpPost.releaseConnection();
			}
		}
		JSONObject jsonObject = null;
		
		System.out.println(result);
		jsonObject = new JSONObject(result);
	
		JSONArray docsJSONArray = jsonObject.getJSONObject("response").getJSONArray("docs");			
		
		//System.out.println("SHELF EVENTLISTSIZE: " + docsJSONArray.length());
		Collection<String> eventIds = new ArrayList<String>();
		Collection<EventHit> eventHits = new ArrayList<EventHit>();
		for (int i = 0 ; i < docsJSONArray.length(); i++) {
			//System.out.println("SHELF LOOP ITERATION: " + i);
			JSONObject docJSONObject = docsJSONArray.getJSONObject(i);
			String active = docJSONObject.getString("active");
			//System.out.println("SHELF ACTIVE: " + active);
			if (active.equals("0")) {
				continue;
			}

//				String maxPrice = docJSONObject.getString("maxPrice");
//				if (maxPrice.equals("0.0")) {
//					continue;
//				}

			String name = docJSONObject.getString("description");
			String dateStr = docJSONObject.getString("event_date_local");
			String timeStr = docJSONObject.getString("event_time_local");
			String[] timeTokens = timeStr.split(":");
			java.sql.Date date = new java.sql.Date(format.parse(dateStr).getTime());
			String eventLocation = docJSONObject.getString("eventGeoDescription");
			if(!isVenue){
				if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), name)) {
					continue;
				}
			}else{
				if (!QueryUtil.matchAtleastHalfTerms(keywords.replaceAll("\\s+", " "), eventLocation)) {
					continue;
				}
			}

			Time time;
			if (timeTokens.length == 3) {
				time = new Time(Integer.valueOf(timeTokens[0]), Integer.valueOf(timeTokens[1]), Integer.valueOf(timeTokens[2]));
			} else {
				time = null;
			}
			
			
			
			String eventCity = null;
			try {
				eventCity = docJSONObject.getString("city");
			} catch (Exception e) {}
			
			String eventState = null;
			try {
				eventState = docJSONObject.getString("state");
			}  catch (Exception e) {}
			
			String eventId = docJSONObject.getString("event_id");
			String url = "http://www.stubhub.com/?event_id=" + eventId;
			
			String eventFullLocation = eventLocation.trim() + ", " + eventCity.trim() + ", " + eventState.trim();
			
			// location filter
			if (location != null) {
				if (eventFullLocation.toLowerCase().indexOf(location.toLowerCase()) < 0) {
					continue;
				}
			}
			boolean flag=false;
			if(!dateMap.containsKey(df.format(date))){
				for(String key:locationMap.keySet()){
					String tokens[] = key.split(":-:");
					String building = tokens[0].trim();
					String cityToken = tokens[1].trim();
					String stateToken = tokens[2].trim();
					
					if( cityToken.equalsIgnoreCase(eventCity.trim())&& stateToken.equalsIgnoreCase(eventState.trim()) && (TextUtil.isSimilarVenue(building, eventLocation, 1))){
						flag=true;
						break;
					}
				}
			}else{
				flag=true;
			}
//			if(dateMap.containsKey(dateFormat.format(eventDate))){
			// check date - stubhub might return irrelevant dates
			/*if (date.before(fromDate) || date.after(toDate)) {
				continue;
			}*/
			
			if(flag && !eventIds.contains(eventId)){	
				//System.out.println("SHELF SPLITWORDS: " + name);
				EventHit eventHit = new EventHit(eventId, name, date, time, eventFullLocation, Site.STUBHUB_FEED, url);
				eventHits.add(eventHit);
				eventIds.add(eventId);
			}

		}
		return eventHits;
	}
	public static void main(String[] args) throws Exception {
		StubHubFeedEventListingFetcher fetcher = new StubHubFeedEventListingFetcher();
		Calendar cal1 = new GregorianCalendar(2016, 5, 14);
		Calendar cal2 = new GregorianCalendar(2016, 5, 16);
		
		fetcher.getEventList("Los Angeles Dodgers", "", cal1.getTime(), cal2.getTime(),null,false);
	}
}
