package com.admitone.tmat.ticketfetcher;

import java.util.Arrays;

public class SeatGeek {
	
	private SeatGeekListings[] listings;

	
	public SeatGeekListings[] getListings() {
		return listings;
	}

	public void setListings(SeatGeekListings[] listings) {
		this.listings = listings;
	}

	
	@Override
	public String toString() {
		return "listings = " + Arrays.toString(listings) ;
	}

}
