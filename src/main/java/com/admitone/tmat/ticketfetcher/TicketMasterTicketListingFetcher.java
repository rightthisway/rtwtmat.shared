package com.admitone.tmat.ticketfetcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.ChangeProxy;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * TicketMasterTicketListingFetcher
 * 
 * @author dthiyagarajan
 *
 */
public class TicketMasterTicketListingFetcher extends TicketListingFetcher {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TicketMasterTicketListingFetcher.class);

	public TicketMasterTicketListingFetcher() {

	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer,
			TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_MASTER))) {
			throw new Exception(
					this.getClass().getName() + ".fetchTicketListing parameter must be a Ticket Master crawl");
		}

		String proxy = "";
		if (httpClient.getHost() != null) {
			proxy = httpClient.getHost();
		} else {
			proxy = InetAddress.getLocalHost().getHostName();
		}

		long startProcessingTime = System.currentTimeMillis();

		String eventId;

		String url = ticketListingCrawl.getQueryUrl();
		int eventIdIndex = url.lastIndexOf('/');
		eventId = url.substring(0, eventIdIndex);
		eventId = eventId.substring(eventId.lastIndexOf("/") + 1);
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement();

		long startFetchingTime = System.currentTimeMillis();
		int counter = 1;
		startFetchingTime = System.currentTimeMillis();
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());

		String content = null;
		CloseableHttpResponse response = null;
		String fileName = null;
		try {

			synchronized (this) {
				Date date = new Date();
				long timeMilli = date.getTime();

				fileName = "C:/tmpcrawl/tmstTickt_" + timeMilli + ".txt";

			}
			String netNutProxy = ChangeProxy.getCurrentProxyForTM();
			String userName = ChangeProxy.getNETNUT_USERNAME();
			String temp[] = netNutProxy.split(":");

			String proxyHost = temp[0];
			String proxyPort = temp[1];
			
			
			String randomProxyDets[] =  getRandomNetNutHostnUser(proxyHost,userName) ;
			
			System.out.println( "  [ Random proxy Host & User Fetched is   ]	 " +  randomProxyDets[0]  + " - "   + randomProxyDets[1] ) ;
			

			System.out.println(" [netNut Proxy ] " + netNutProxy + " [ host ] " + randomProxyDets[0] + " [ port ] " + proxyPort
					+ " [userName ] " + randomProxyDets[1]);

			String[] args1 = { url, fileName, proxyHost, proxyPort, userName };

			System.out.println("[ Before Launch of DOS CMD ............." + new Date());

			try {

				String cmd = "C:/rtwparserjars/EventParserFXWeb";
				String param1 = args1[0];
				String param2 = args1[1];
				String param3 = randomProxyDets[0];
				String param4 = args1[3];
				String param5 = randomProxyDets[1];

				param1 = "\"" + param1 + "\"";
				param2 = "\"" + param2 + "\"";
				param3 = "\"" + param3 + "\"";
				param4 = "\"" + param4 + "\"";
				param5 = "\"" + param5 + "\"";
				System.out.println(" - Preparing DOS command - ");

				try {

					Process proc = startNewJavaProcess("", "com.admitone.tmat.eventfetcher.JavaFxWebOpener", args1);
					Runtime rt = Runtime.getRuntime();
					// Process proc = rt.exec("C:/Windows/System32/cmd.exe /c
					// start \"\" " + cmd + " " + param1 + " " + param2 + " " +
					// param3 + " " + param4 + " " + param5);

					InputStream stderr = proc.getErrorStream();
					InputStreamReader isr = new InputStreamReader(stderr);
					BufferedReader br = new BufferedReader(isr);

					String line = null;
					System.out.println("<ERROR>");
					while ((line = br.readLine()) != null)
						System.out.println(line);
					System.out.println("</ERROR>");
					int exitVal = proc.waitFor();
					System.out.println("Process exitValue: " + exitVal);
				} catch (Throwable t) {
					t.printStackTrace();
				}

				// Runtime.getRuntime().exec("cmd /c start \"\" " + cmd + " " +
				// param1 + " " + param2 + " " + param3 + " " + param4 + " " +
				// param5);

				System.out.println(" FX OPENER  CALLED ");

			} catch (Exception e) {

				System.out.println(" EXCEPTION CALLING BAT FILE  CALLED ");
				e.printStackTrace();
			}

			System.out.println(" Called Sleep FOR....... 12 secs .... " + new Date());

			Thread.sleep(15000);

			System.out.println(" woken up .. Sleep ...... after ... 12 secs .... start parsing file ... " + new Date());

			System.out.println(" After Launch .................................." + new Date());

			content = new String(Files.readAllBytes(Paths.get(fileName)), "UTF-8");

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// to reuse connection we need to close response stream associated
			// with that
			if (response != null)
				response.close();
		}

		if (content == null)
			return false;

		/*** Original Starts **/
		try {
			
			
			//Temp Code ::::
			//content = new String(Files.readAllBytes(Paths.get("C:/tmpcrawl/tmstTickt_1533843262170.txt")), "UTF-8");
			
			TicketDeliveryType ticketDeliveryType = null;
			TicketType ticketType = TicketType.REGULAR;

			String ticketId, quantity, row, section = null;
			String eticket, instant, notes = null;
			boolean inHand = true;
			String seat = "";
			int tickPickCount = 0;
			try {
				java.text.SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd");

				int contentStartIndex = content.indexOf("storeUtils['eventOfferJSON']=") + 29;

				int contentEndIndex = content.indexOf("storeUtils['resaleEligibilityJSONData']");

				String contentExtract = content.substring(contentStartIndex, contentEndIndex);

				int lastTxtIndex = contentExtract.indexOf("}];") + 2;
				contentExtract = contentExtract.substring(0, lastTxtIndex);
				JSONParser parser = new JSONParser();
				JSONArray jsonArray = (JSONArray) parser.parse(contentExtract);

				System.out.println(" ******* NO OF TICKETS LISTINGS *****   " + (jsonArray).size());

				for (Object jsonEventsMap : jsonArray) {

					System.out.println(jsonEventsMap);
					Map jsonObj = (Map) jsonEventsMap;
					ticketId = (String) jsonObj.get("offerId");
					System.out.println("***** ticketId *******  " + ticketId);
					section = (String) jsonObj.get("section");
					if (section == null) {
						System.out.println("***** SKIPPING TICKET as section is NULL ******* section = " + section);
						// section = " ";
						continue;
					}

					row = (String) jsonObj.get("row");

					if (row == null) {
						// row = " ";
						System.out.println("***** SKIPPING TICKET as row is NULL ******* row = " + row);
						continue;
					}
					System.out.println("***** section *******  " + section);
					System.out.println("***** row *******  " + row);
					String totalPrice = String.valueOf(jsonObj.get("totalPrice"));
					System.out.println("***** totalPrice *******  " + totalPrice);
					Double price = Double.valueOf(totalPrice);
					JSONArray sellableQtysjsonArray = (JSONArray) jsonObj.get("sellableQuantities");
					String lotSizeStr = String.valueOf(sellableQtysjsonArray.get(sellableQtysjsonArray.size() - 1));
					System.out.println("***** lotSize *******  " + lotSizeStr);

					if (null == lotSizeStr)
						lotSizeStr = "2";
					quantity = "2"; // As we always land on the default page
									// with
									// qty selected as 2 in Ticketmaster.com
					int lotSize = 1;
					int qty = Integer.parseInt(quantity);
					if ((qty % 2) == 0 && qty < 14) {
						lotSize = 2;
					}

					Double buyItNowPrice = price;
					TicketHit ticketHit = new TicketHit(Site.TICKET_MASTER, ticketId, ticketType, qty, lotSize, row,
							section, price, buyItNowPrice, "TicketMaster", TicketStatus.ACTIVE);
					ticketHit.setSoldQuantity(0);
					ticketHit.setSeat(seat);
					ticketHit.setTicketListingCrawl(ticketListingCrawl);
					ticketHit.setTicketDeliveryType(ticketDeliveryType);
					indexTicketHit(ticketHitIndexer, ticketHit);
					ticketHit.setInHand(inHand);
					long startIndexationTime = System.currentTimeMillis();
					ticketListingCrawl.setIndexationTime(
							ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
					tickPickCount++;

					System.out.println(" ticketId " + ticketId + " quantity " + quantity + " row " + row + " section "
							+ section + "price " + price + "ticketType " + ticketType + "lotSize " + lotSize + "inHand "
							+ inHand);

				} // for Loop Ends

			} catch (Exception ex) {
				System.out.println("   Failed to Parse in Regular Format using JSON Data " + ex.getStackTrace());
			}

			// Parse Again for rendered HTML List ..
			try {
				System.out.println("  PARSING WITH RENDERED HTML " ) ;
				List secRowArrayList = parseTicketSectionDetails(fileName);
				List priceList = parseTicketPriceDetails(fileName);

				for (int i = 0; i < secRowArrayList.size(); i++) {
					List tmpList = (List) secRowArrayList.get(i);
					tmpList.add(priceList.get(i));
				}

				System.out.println(Arrays.toString(secRowArrayList.toArray()));

				for (int i = 0; i < secRowArrayList.size(); i++) {

					List secPriceTmpList = (List) secRowArrayList.get(i);
					section = (String) secPriceTmpList.get(0);
					row = (String) secPriceTmpList.get(1);
					Double buyItNowPrice = Double.valueOf((String) secPriceTmpList.get(2));
					int qty = 2; // HardCoded as Default Listing is for 2 only.
					int lotSize = 2; // HardCoded as Default Listing is for 2
										// only.
					Double price = buyItNowPrice;
					ticketId = getSaltStringForTicketId();
					TicketHit ticketHit = new TicketHit(Site.TICKET_MASTER, ticketId, ticketType, qty, lotSize, row,
							section, price, buyItNowPrice, "TicketMaster", TicketStatus.ACTIVE);
					ticketHit.setSoldQuantity(0);
					ticketHit.setSeat(seat);
					ticketHit.setTicketListingCrawl(ticketListingCrawl);
					ticketHit.setTicketDeliveryType(ticketDeliveryType);
					indexTicketHit(ticketHitIndexer, ticketHit);
					ticketHit.setInHand(inHand);

					long startIndexationTime = System.currentTimeMillis();
					ticketListingCrawl.setIndexationTime(
							ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
					tickPickCount++;

					System.out.println(" ******* PICKED FROM RENDERED HTML ****** ticketId " + ticketId + " quantity "
							+ qty + " row " + row + " section " + section + "price " + price + "ticketType "
							+ ticketType + "lotSize " + lotSize + "inHand " + inHand);

				}

			} catch (Exception ex) {
				System.out.println(" Failed to Parse  Ticket details from Rendered HTML  ");
			}

		} catch (Exception ex) {
			System.out.println(" [Exception  TicketMaster Ticket Crawling ]  " + ex);
		}
		saveCounterHistory(ticketListingCrawl, counter, proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime
				- ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());

		return true;
	}

	protected static String getSaltStringForTicketId() {
		String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		StringBuilder salt = new StringBuilder();
		Random rnd = new Random();
		while (salt.length() < 10) { // length of the random string.
			int index = (int) (rnd.nextFloat() * SALTCHARS.length());
			salt.append(SALTCHARS.charAt(index));
		}
		String saltStr = salt.toString();
		return saltStr;

	}

	// content = new String(Files.readAllBytes(Paths.get(fileName)), "UTF-8");
	public static List parseTicketSectionDetails(String filePath) throws Exception {
		File input = new File(filePath);
		Document doc = Jsoup.parse(input, "UTF-8", "");
		List secRowArrayList = new ArrayList();
		String[] secArry = {};
		Elements content = doc.getElementsByClass("quick-picks__item-desc");
		for (Element span : content) {
			List tempList = new ArrayList();
			String spanText = span.text();
			secRowArrayList.add(new ArrayList<String>(Arrays.asList(spanText.split(","))));

		}
		return secRowArrayList;
	}

	public static List<String> parseTicketPriceDetails(String filePath) throws Exception {

		File input = new File(filePath);
		Document doc = Jsoup.parse(input, "UTF-8", "");
		List<String> priceList = new ArrayList();
		Elements content = doc.getElementsByClass("quick-picks__button");
		// Elements links = content.getElementsByTag("a");
		for (Element span : content) {
			String spanText = span.text();
			String[] tmpArray = spanText.split(" ");
			String price = tmpArray[0];
			price = price.substring(1, price.length());
			System.out.println(price);
			priceList.add(price);
		}

		return priceList;
	}

	public Process startNewJavaProcess(final String optionsAsString, final String mainClass, final String[] arguments)
			throws IOException {
		ProcessBuilder processBuilder = createProcess(optionsAsString, mainClass, arguments);
		Process process = processBuilder.start();
		return process;
	}

	private ProcessBuilder createProcess(final String optionsAsString, final String mainClass,
			final String[] arguments) {

		String jvm = System.getProperty("java.home") + File.separator + "bin" + File.separator + "java";
		String classpath = System.getProperty("java.class.path");
		classpath += ".;%CLASSPATH%;C:/rtwparserjars;C:/rtwparserjars/json-simple-1.1.jar;C:/rtwparserjars/jsoup-1.11.3.jar;C:/rtwparserjars/xml-apis-1.0.b2.jar;C:/rtwparserjars/jfxrt.jar";
		System.out.println(" classpath " + classpath);
		System.out.println(" JDK VERSION " + jvm);
		String[] options = optionsAsString.split(" ");
		List<String> command = new ArrayList<String>();
		command.add(jvm);
		// command.add("C:/Program Files/Java/jdk1.7.0_79/jre/bin/java");

		command.addAll(Arrays.asList(options));
		command.add(mainClass);
		command.addAll(Arrays.asList(arguments));
		ProcessBuilder processBuilder = new ProcessBuilder(command);
		Map<String, String> environment = processBuilder.environment();
		environment.put("CLASSPATH", classpath);
		return processBuilder;

	}
	

	private static int getRandomNumberInRange(int min, int max) {
		if (min >= max) {
			throw new IllegalArgumentException("max must be greater than min");
		}

		Random r = new Random();
		return r.nextInt((max - min) + 1) + min;
	}
	
	private static int getStickyNum (String text) {
		Matcher matcher = Pattern.compile("\\d+").matcher(text);
		matcher.find();
		int i = Integer.valueOf(matcher.group());
		return i;
	}
	
	private static String[] getRandomNetNutHostnUser(String host, String userName) {//us-s50.netnut.io Rightthisway!a25		
	
		int currentHostNum = getStickyNum(host);		
		int currentUserNum = getStickyNum(userName);		
		int hostNumberSuffix = getRandomNumberInRange(1,254);
		//System.out.println( " [__hostNumberSuffix __] " + hostNumberSuffix);		
		int userNumberSuffix = getRandomNumberInRange(1,254);		
		//System.out.println( " [__userNumberSuffix__] " + userNumberSuffix);		
		host = host.replaceFirst(String.valueOf(currentHostNum), String.valueOf(hostNumberSuffix));
		userName = userName.replaceFirst(String.valueOf(currentUserNum), String.valueOf(userNumberSuffix));
		String[] netNutDets = { host,userName };		
		return netNutDets;
	}
		

	// Unit testing
	public static void main(String[] args) throws Exception {
		// int i=0;
		// while(i<10){
		// i++;
		TicketMasterTicketListingFetcher feed = new TicketMasterTicketListingFetcher();
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient("ticketmaster", "", "");
		TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setId(123);
		ticketListingCrawl.setQueryUrl(
				"https://www1.ticketmaster.com/taylor-swift-reputation-stadium-tour-indianapolis-indiana-09-15-2018/event/0500537EA32A38B5");///
		ticketListingCrawl.setSiteId(Site.TICKET_MASTER);
		System.out
				.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
		// }
	}

}