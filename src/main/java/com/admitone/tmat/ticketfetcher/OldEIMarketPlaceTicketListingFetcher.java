package com.admitone.tmat.ticketfetcher;

import java.text.NumberFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.eimarketplace.EIMPCredential;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * EiMarketPlace Ticket Listing Fetcher. 
 */
public class OldEIMarketPlaceTicketListingFetcher extends TicketListingFetcher {
	private static final Pattern brokerIdPattern = Pattern.compile("\\?b=([0-9]+)");

	//                                                                                                                                                                    POSITION                                  1-QUANTITY                                                       2-SECTION                                               3-ROW                                                       4-SEATS                                          5-WHOLESALE PRICE                                               6-RETAIL PRICE                                                                BROKER ID         BROKER NAME                    NOTE
//	private static final Pattern trPattern = Pattern.compile("<tr valign=\"CENTER\" align=LEFT bgcolor=\"EEEEEE\">.*?<td.*?>.*?</td>.*?<td.*?>.*?<strong>(.*?)</strong>.*?</td>.*?<td.*?>.*?<span.*?>(.*?)</span>.*?</td>.*?<td.*?><span.*?>(.*?)</span>.*?</td>.*?<td.*?>.*?<span.*?>(.*?)</span>.*?</td>.*?<td.*?>.*?<strong>(.*?)</strong>.*?</td>.*?<td.*?>.*?<strong.*?>(.*?)</strong>.*?</td>.*?.*?/brokers/brokercontact.cfm[?]b=(\\d+).*?\">(.*?)</a>.*?<td.*?>.*?<nobr>(.*?)</nobr>.*?</td>.*?</tr>", Pattern.DOTALL);
	private static final Pattern trPattern = Pattern.compile("<tr valign=\"CENTER\" align=LEFT bgcolor=\".*?\">.*?<td.*?>.*?</td>.*?<td.*?>.*?<strong>(.*?)</strong>.*?</td>.*?<td.*?>.*?<span.*?>(.*?)</span>.*?</td>.*?<td.*?><span.*?>(.*?)</span>.*?</td>.*?<td.*?>.*?<span.*?>(.*?)</span>.*?</td>.*?<td.*?>.*?<strong>(.*?)</strong>.*?</td>.*?<td.*?>.*?<strong.*?>(.*?)</strong>.*?</td>.*?/brokers/brokercontact.cfm[?]b=(\\d+).*?\">(.*?)</a>.*?<td.*?>.*?<nobr>(.*?)</nobr>.*?</td>.*?</tr>", Pattern.DOTALL);
	private static final Pattern seatPattern = Pattern.compile("(\\d+)");

	//private static final Pattern noteTitlePattern = Pattern.compile("title=\"(.*?)\"");
	
	private static final Pattern nextPageButtonPattern = Pattern.compile("(<input type=\"submit\".*?name=\"PaginationButtonDirection\" value=\"Next\">)");

	private EIMPCredential credential;
	
	public OldEIMarketPlaceTicketListingFetcher() {
	}	

	@Override
	protected SimpleHttpClient createHttpClient(TicketListingCrawl crawl) throws Exception {
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.EI_MARKETPLACE, credential.getUsername(), credential.getPassword());
		return httpClient;
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.EI_MARKETPLACE))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an eimarketplace crawl");
		}
		
		long startProcessingTime = System.currentTimeMillis();

		Date d = new Date(new Date().getTime() - 6L * 60L * 60L * 1000L);
				
		httpClient.setRedirectHandler(null);
		
		int page = 0;
		
		Map<String, Boolean> usedItemIds = new HashMap<String, Boolean>(); 
		
		//
		// Get the ticket listing page
		//		

		while(true) {
			String content;
			long startFetchingTime = System.currentTimeMillis();
			UncompressedHttpEntity entity;
			
			//System.out.println("EIMP EVENT ID=" + ticketListingCrawl.getExtraParameter("queryEventId"));
			if (page == 0) {
				HttpGet httpGet = new HttpGet(getTicketListingUrl(ticketListingCrawl));
				//System.out.println("EIMP URL=" + getTicketListingUrl(ticketListingCrawl));
				httpGet.addHeader("Host", "www.eimarketplace.com");
				httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				httpGet.addHeader("Referer", "http://www.eimarketplace.com/brokers/results.cfm");
				HttpResponse response = httpClient.execute(httpGet);		
				entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			} else {
				HttpPost httpPost = new HttpPost("https://www.eimarketplace.com/brokers/results.cfm");
				//System.out.println("EIMP URL=" + "https://www.eimarketplace.com/brokers/results.cfm");
				httpPost.addHeader("Host", "www.eimarketplace.com");
				httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpPost.addHeader("Accept-Encoding", "gzip,deflate");
				httpPost.addHeader("Referer", "https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=" + ticketListingCrawl.getExtraParameter("queryEventId"));
				httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");

				String postData = "showSpeculationTicketsForOtherBrokers=false&showcalc=true&markup=20.0&min=.0000&max=.0000&extmarkup=20.0&extmin=.0000&extmax=.0000&showOnlyActiveTickets=true&p=" + ticketListingCrawl.getExtraParameter("queryEventId") + "&sorttype=pricehightolow&CurrentPageNumber=" + page + "&PaginationButtonDirection=Next";
				httpPost.setEntity(new StringEntity(postData));
				
				HttpResponse response = httpClient.execute(httpPost);				
				entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());				
			}
			
			content = EntityUtils.toString(entity);			
			ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());					
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
		
			Matcher trMatcher = trPattern.matcher(content);			

			while(trMatcher.find()) {
				
				// 1st cell contains the ticket position (skipped)
				// 2nd cell contains the quantity
				int quantity = Integer.parseInt(trMatcher.group(1).trim());
				// System.out.println("QUANTITY=" + quantity);
				
				// 3rd cell contains the section
				String section = trMatcher.group(2).trim();
				// System.out.println("SECTION=" + section);
				
				// 4th cell contains the row
				String row = trMatcher.group(3).trim();
				// System.out.println("ROW=" + row);
	
				// 5th cell contains the seats	
				String seats = trMatcher.group(4).trim();
				// System.out.println("SEATS=" + seats);

				// 6th cell contains the wholesale price
		        NumberFormat numberFormat = NumberFormat.getInstance();
		        double wholesalePrice = numberFormat.parse(trMatcher.group(5).trim().substring(1)).doubleValue();	        
				// System.out.println("WHOLE PRICE=" + wholesalePrice);
		        
				// 7th cell contains the retail price (ignored for now)
				// String retailPriceString = ticketTableCells.item(6).getTextContent().trim().substring(1);
		        // double retailPrice = numberFormat.parse(retailPriceString).doubleValue();	        
				// System.out.println("RETAIL PRICE=" + retailPrice);
		        
		        // make retail price = wholesale price as retail price is an artificial price
		        double retailPrice = wholesalePrice;
				
				// 8th cell contains the broker
				String brokerId = trMatcher.group(7).trim();
				String brokerName = trMatcher.group(8).trim();
				// System.out.println("BROKER NAME=" + brokerName);
				// System.out.println("BROKER ID=" + brokerId);
						
				// 9th cell contains the note
				String note = null;
				String rawNote = trMatcher.group(9).trim();

				// System.out.println("NOTE=" + note);
							
				String itemId = ticketListingCrawl.getExtraParameter("queryEventId") + "-" + brokerId + "-" + section + "-" + row;
				
				int i = 1;
				while (usedItemIds.containsKey(itemId)) {
					itemId = itemId.split("@")[0] + "@" + i;
					i++;
				}
				
				usedItemIds.put(itemId, true);
				
				TicketHit ticketHit = new TicketHit(Site.EI_MARKETPLACE, itemId,
						TicketType.REGULAR,
						quantity,
						quantity,
						row, section, wholesalePrice, retailPrice, brokerName, TicketStatus.ACTIVE);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				
				ticketHit.setSeat((seats == null)?null:seats);

				/*
				ticketHit.setEventDate(venueDate);
				ticketHit.setVenueState(venueState);
				ticketHit.setVenueCity(venueCity);
				ticketHit.setVenueName(venueName);
				*/

				Long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);				
			}
			
			page++;

			// check if we have to fetch the next page or not
			Matcher nextPageButtonMatcher = nextPageButtonPattern.matcher(content);
			if (!nextPageButtonMatcher.find()) {
				break;
			}
		}

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		
		return true;
	}
	
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		String queryEventId = ticketListingCrawl.getExtraParameter("queryEventId");		
		return "https://www.eimarketplace.com/brokers/results.cfm?restart=yes&p=" + queryEventId;
	}
	
	public void setCredential(EIMPCredential credential) {
		this.credential = credential;
	}

	public EIMPCredential getCredential() {
		return credential;
	}
}