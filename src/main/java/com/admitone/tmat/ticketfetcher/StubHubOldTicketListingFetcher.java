package com.admitone.tmat.ticketfetcher;

import java.net.URI;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.xpath.XPathAPI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * StubHub Ticket Listing Fetcher
 * Call httpGet.abort to release connection. (can happen if the content of the response is not consumed)
 */
public class StubHubOldTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(StubHubOldTicketListingFetcher.class);
	
	private Pattern stubHubNotePattern = Pattern.compile("<b>Seller comments:</b>(.*?)</p>");
	private Pattern stubHubTimeLeftPattern = Pattern.compile("Time Left:</div><div class=\"content\">(.*?)</div>");	
	private Pattern xbidPattern = Pattern.compile("qsObj\\['xbid'\\]\\s*=\\s*([0-9]+);");
	private Pattern cobrandIdPattern = Pattern.compile("qsObj\\['cobrand'\\]\\s*=\\s*([0-9]+);");

	public StubHubOldTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals("stubhub"))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a stubhub crawl");
		}		
		
		boolean fetchNote = "1".equals(ticketListingCrawl.getExtraParameter("fetchNoteFlag"));
		
		StubHubRedirectHandler redirectHandler = new StubHubRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		// to get the session in the cookie
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "www.stubhub.com");		
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5 ");
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		// httpGet.addHeader("Keep-Alive", "300");
		// httpGet.addHeader("Connection", "keep-alive");
		
		HttpEntity entity = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpGet);
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			response.close();
		}
		String content = EntityUtils.toString(entity);
		httpGet.abort();
		
		// there can be a  redirection on some url
		// e.g. http://www.stubhub.com/ncaa-tournament-south-regional-tickets/ncaa-tournament-south-regional-vs-fedexforum-3-27-2009-605490/
		String ticketListingUrl;
		if (redirectHandler.getRedirectUrl() != null) {
			ticketListingUrl = redirectHandler.getRedirectUrl();
		} else {
			ticketListingUrl = ticketListingCrawl.getQueryUrl();
		}
		
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/						
		// http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=80&offset=1&sortby=p&orderby=a&eventId=565762&xbid=4981&cobrandId=47

		// Extract EventId
		String url = ticketListingCrawl.getQueryUrl();
		String eventId;
		int eventIdPos = url.indexOf("event_id=");
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}

		// Extract xbid		
		Matcher xbidMatcher = xbidPattern.matcher(content);
		if (!xbidMatcher.find()) {
			if (fetchNote) {
				ticketListingCrawl.setExtraParameter("fetchNoteFlag", "0");
				ticketListingCrawl.setExtraParameter("lastFetchNoteTime", Long.toString(new Date().getTime()));			
			}
			return true;
		}
		String xbid = xbidMatcher.group(1).trim();

		// Extract cobrandId		
		Matcher cobrandIdMatcher = cobrandIdPattern.matcher(content);
		cobrandIdMatcher.find();
		String cobrandId = cobrandIdMatcher.group(1).trim();

		// System.out.println("EVENTID=" + eventId);
		// System.out.println("XBID=" + xbid);
		// System.out.println("COBRANDID" + cobrandId);

		int offset = 1;
		int total = -1;
		
		int itemsPerPage = 200;
		
		do {
			HttpPost httpPost = new HttpPost("http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=" + itemsPerPage + "&offset=" + offset + "&sortby=p&orderby=a&eventId="
					+ eventId + "&xbid=" + xbid + "&cobrandId=" + cobrandId);
			httpPost.addHeader("Host", "www.stubhub.com");		
			httpPost.addHeader("Accept", "application/json, text/javascript, */*");
			httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
			httpPost.addHeader("Accept-Encoding", "gzip,deflate");
			httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpPost.addHeader("Referer", ticketListingCrawl.getQueryUrl());

			CloseableHttpResponse xmlResponse = null;
			try{
				xmlResponse = httpClient.execute(httpPost);
				entity = HttpEntityHelper.getUncompressedEntity(xmlResponse.getEntity());
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				xmlResponse.close();
			}
			
			// content = EntityUtils.toString(entity);
			// System.out.println("CONTENT=" + content);

			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			
			Document xmlDocument = documentBuilder.parse(entity.getContent());
			// Document xmlDocument = documentBuilder.parse(new InputSource(new StringReader(content)));
	
			Node detailsNode = XPathAPI.selectSingleNode(xmlDocument, "/tickets/details");			

			if (total == -1) {
				total = Integer.parseInt(detailsNode.getAttributes().getNamedItem("total").getTextContent());
			}

			// System.out.println("TOTAL=" + total);

			NodeList ticketNodeList = XPathAPI.selectNodeList(xmlDocument, "//ticket");
			for (int i = 0 ; i < ticketNodeList.getLength() ; i++) {
				Node ticketNode = ticketNodeList.item(i);
				
				Date endDate = null;
				String section = "";
				String row = "";
				Double price = 0.0;
				int quantity = 0;
				int quantity_remain = 0;
				int split = 1;
				String id = null;
				
				boolean isAuction = false;
				for (int j = 0 ; j < ticketNode.getChildNodes().getLength() ; j++) {
					Node child = ticketNode.getChildNodes().item(j);
					if (child.getNodeName().equals("section")) {
						section = child.getTextContent().trim();
					} else if (child.getNodeName().equals("row")) {
						row = child.getTextContent().trim();
					} else if (child.getNodeName().equals("price")) {
						price = Double.parseDouble(child.getTextContent());
					} else if (child.getNodeName().equals("quantity")) {
						quantity = Integer.parseInt(child.getTextContent());
					} else if (child.getNodeName().equals("quantity_remain")) {
						quantity_remain = Integer.parseInt(child.getTextContent());
					} else if (child.getNodeName().equals("split")) {
						split = Integer.parseInt(child.getTextContent());
					} else if (child.getNodeName().equals("id")) {
						id = child.getTextContent();
					} else if (child.getNodeName().equals("sale_method")) {
						isAuction = child.getTextContent().equals("0");
					}
				}
				
				String note = null;
				
				/*
			
				Ticket ticket = DAORegistry.getTicketDAO().get("stubhub-" + id);
				if (ticket == null || (isAuction && ticket.getEndDate() == null) || fetchNote) {
					
					// to get the note, we have to go individually to the item page:
					// http://www.stubhub.com/atlanta-hawks-tickets/hawks-vs-pacers-3-13-2009-690602/?ticket_id=178728270
					// listing: http://www.stubhub.com/atlanta-hawks-tickets/hawks-vs-pacers-3-13-2009-690602/
					
					// System.out.println("STUBHUB GET URL=" + ticketListingUrl + "?ticket_id=" + id);
					
					redirectHandler.setRedirectUrl(null);
					
					httpGet = new HttpGet(ticketListingUrl + "?ticket_id=" + id);
					httpGet.addHeader("Host", "www.stubhub.com");		
					httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5 ");
					httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
					httpGet.addHeader("Accept-Encoding", "gzip,deflate");
					httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
					// httpGet.addHeader("Keep-Alive", "300");
					// httpGet.addHeader("Connection", "keep-alive");
					httpGet.addHeader("Referer", ticketListingCrawl.getQueryUrl());
					
					response = httpClient.execute(httpGet);					
					
					// some items has expired and redirect to error page
					if (redirectHandler.getRedirectUrl() != null && redirectHandler.getRedirectUrl().indexOf("error.html") >= 0) {
						httpGet.abort();
						continue;
					}
					
					entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());

					String pageContent = EntityUtils.toString(entity);					
					Matcher noteMatcher = stubHubNotePattern.matcher(pageContent);
					noteMatcher.find();
					note = noteMatcher.group(1);
					
					// auction item url
					// http://www.stubhub.com/new-york-yankees-tickets/yankees-vs-rays-5-7-2009-720475/?ticket_id=186010228
					
					// http://www.stubhub.com/st-johns-red-storm-baseball-tickets/st-johns-red-storm-vs-georgetown-hoyas-3-29-2009-802507/
					// for auction listing, get remaining date
					if (isAuction) {
						long endTime = new Date().getTime();
						Matcher timeLeftMatcher = stubHubTimeLeftPattern.matcher(pageContent);
						timeLeftMatcher.find();
						String timeLeftString = timeLeftMatcher.group(1);
						// System.out.println("URL=" + ticketListingUrl + "?ticket_id=" + id);
						// System.out.println("TILE LEFT STRING=" + timeLeftString);

						// Time Left to Purchase: 1d 1h 38m
						// Time Left to Purchase: 15 min
						
						String[] timeTokens = timeLeftString.split(" ");
						
						if (timeTokens.length == 2 && timeTokens[1].equals("min")) {
							endTime += Integer.parseInt(timeTokens[0]) * 60L * 1000L;							
						} else {						
							for (String timeToken: timeTokens) {
								int timeValue = Integer.parseInt(timeToken.substring(0, timeToken.length() - 1));
								String timeUnit = timeToken.substring(timeToken.length() - 1);
								
								if (timeUnit.equals("d")) {
									endTime += timeValue * 24L * 60L * 60L * 1000L;
								} else if (timeUnit.equals("h")) {
									endTime += timeValue * 60L * 60L * 1000L;								
								} else if (timeUnit.equals("m")) {
									endTime += timeValue * 60L * 1000L;																
								}
							}
						}
						endDate = new Date(endTime);
					}
				} else {
				
				}
				
				*/
				
				Double buyItNowPrice;
				
				if (isAuction) {
					// for auction, there is no buyItNow price
					buyItNowPrice = null;
				} else {
					// otherwise the buyItNowPrice is the current price
					buyItNowPrice = price;
				}
				
				TicketHit ticketHit = new TicketHit(Site.STUB_HUB, id, TicketType.REGULAR,
						quantity, quantity,
						row, section, price, buyItNowPrice, "StubHub", TicketStatus.ACTIVE);
				ticketHit.setSoldQuantity(quantity - quantity_remain);
				ticketHit.setEndDate(endDate);
				ticketHit.setLotSize(split);
				ticketHit.setSeller("StubHub");
				ticketHit.setTicketListingCrawl(ticketListingCrawl);				
				indexTicketHit(ticketHitIndexer, ticketHit);
			}
			offset += itemsPerPage;
		}
	
		while (offset < total);

		if (fetchNote) {
			ticketListingCrawl.setExtraParameter("fetchNoteFlag", "0");
			ticketListingCrawl.setExtraParameter("lastFetchNoteTime", Long.toString(new Date().getTime()));			
		}

		return true;
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		return url.matches("^http://www.stubhub.com/.*[0-9]*/$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	/**
	 * Redirect Handler Class 
	 */
	private class StubHubRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public StubHubRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {	
//			System.out.println("REDIRECT URL=" + redirectUrl);
			if (redirectUrl.startsWith("http://")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create("http://www.stubhub.com" + redirectUrl);				
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

		public void setRedirectUrl(String redirectUrl) {
			this.redirectUrl = redirectUrl;
		}		

		public String getRedirectUrl() {
			return redirectUrl;
		}		
	}	
}
