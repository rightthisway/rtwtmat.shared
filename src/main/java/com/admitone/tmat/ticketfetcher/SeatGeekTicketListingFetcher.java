package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.util.Date;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;
import com.google.gson.Gson;

public class SeatGeekTicketListingFetcher extends TicketListingFetcher  {

	/**
	 * @param args
	 */
	public SeatGeekTicketListingFetcher()
	{
		
	}
	
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.SEATGEEK))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a seat crawl");
		}
		String proxy="";
//		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
//			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId=null;
		
		String ticketUrl = ticketListingCrawl.getQueryUrl();
		String [] ticket_Url_split=ticketUrl.split("/");
		eventId=ticket_Url_split[ticket_Url_split.length-1];
		
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement(); 
		long startFetchingTime = System.currentTimeMillis();
		int counter=1;
		
		startFetchingTime = System.currentTimeMillis();
		
		     Gson gson=new Gson();
		    String url="https://seatgeek.com/listings?client_id=MTY2MnwxMzgzMzIwMTU4&id=" + eventId;
		    
		    HttpGet httpGet = new HttpGet(url);
		    //RequestConfig config = RequestConfig.custom().setCircularRedirectsAllowed(true).build() ;
		    //httpGet.setConfig(config);
			
			counter++;
			crawlerHistoryManagement.setCounter(counter);
			crawlerHistoryManagement.setSuccessfulHit(new Date());

			String content = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpGet);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity);		
					ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
			 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
		
/*
			InputStream in= new URL(url).openStream();
			
			String webSite = IOUtils.toString(in);*/
			
			//ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
	 		//ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
	 		

			SeatGeek j= gson.fromJson(content, SeatGeek.class);
			
			SeatGeekListings[] ticketlisting= j.getListings();
			
			for(SeatGeekListings ticket : ticketlisting)
	 		{
	 			
	 			String note = ticket.getSeller_notes();
	 			String section = ticket.getSection();
	 			String row = ticket.getRow();
 			
				Double price = ticket.getSelling_price();
				Double buyItNowPrice = price;
				Integer quantity = ticket.getQuantity();
				String seller= ticket.getSeller();
				
				
				String seat= "";
				
				String ticketId = ticket.getId();
				
				TicketType ticketType = TicketType.REGULAR;
				if(note !=null){
					if(note.toUpperCase().contains("OBSTRUCT")) {
						ticketType = TicketType.OBSTRUCTEDVIEW;	
					} else if(note.toUpperCase().contains("LIMITED")) {
						ticketType = TicketType.LIMITEDVIEW;
					} else if(note.toUpperCase().contains("WHEELCHAIR")) {
						ticketType = TicketType.WHEELCHAIR;
					} else if(note.toUpperCase().contains("CHILD TICKET")){
						ticketType = TicketType.CHILDTICKET;
					} else if(note.toUpperCase().contains("PARTIAL VIEW")){
						ticketType = TicketType.PARTIALVIEW;
					}
				}
				TicketDeliveryType ticketDeliveryType = null;
//				String instantDownload = ticketJSON.getString("de");
//				String electronics = ticketJSON.getString("et");
				boolean inHand= false;
				if(note!=null && note.contains("Instant Download")){
						ticketDeliveryType = TicketDeliveryType.INSTANT;
						inHand= true;
				}else if(note!=null && (note.toLowerCase().contains("e-ticket") || note.toLowerCase().contains(("eticket")))){
					ticketDeliveryType = TicketDeliveryType.ETICKETS;
					inHand= true;  // As per ZoneII-144
				}
				
				int lotSize = 1;
				if ((quantity % 2) == 0 && quantity < 14) {
					lotSize = 2;
				}
				TicketHit ticketHit = new TicketHit(Site.SEATGEEK, ticketId,
						ticketType,
						quantity,lotSize,
						row, section, price, buyItNowPrice,seller, TicketStatus.ACTIVE);
				ticketHit.setSoldQuantity(0);
				ticketHit.setSeat(seat);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setTicketDeliveryType(ticketDeliveryType);
				ticketHit.setInHand(inHand);
				indexTicketHit(ticketHitIndexer, ticketHit);
				long startIndexationTime = System.currentTimeMillis();
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
	 		}
			
			saveCounterHistory(ticketListingCrawl, counter,proxy);
			ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
			return true;
		
	}
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
//		return "http://www.stubhub.com/largesellers-ticketcenter/";
		return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.seatgeek.com/largesellers-ticketcenter/";
//			if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//				return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//			}
//
//			return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
