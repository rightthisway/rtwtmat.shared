package com.admitone.tmat.ticketfetcher;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.regex.Pattern;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class SeatWaveFeedTicketListingFetcher extends TicketListingFetcher {
	private Collection<SeatWaveFeedEventMapping> eventMappings;
	private static final Pattern seatPattern = Pattern.compile("(\\d+)");	
	
	public Collection<SeatWaveFeedEventMapping> getLastEventMappings() {
		return eventMappings;		
	}

	@Override
	protected SimpleHttpClient createHttpClient(TicketListingCrawl crawl) throws Exception {
		return null;
	}

	
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		Collection<SeatWaveFeedEventMapping> newEventMappings = new ArrayList<SeatWaveFeedEventMapping>();

		/*FTPClient ftpClient = new FTPClient();
		ftpClient.connect("ftp.seatwave.com");

		// After connection attempt, check the reply code
		int reply = ftpClient.getReplyCode();
		if(!FTPReply.isPositiveCompletion(reply)) {
			ftpClient.disconnect();
			throw new Exception("FTP server refused connection.");
		}

		if (!ftpClient.login("admitone", "admitone93246")) {
			throw new Exception("Cannot log in to the FTP server.");			
		}
		
		ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
		
		ZipInputStream zipInputStream = new ZipInputStream(ftpClient.retrieveFileStream("admitone.csv"));
		ZipEntry zipEntry;
		String content = "";
		while((zipEntry = zipInputStream.getNextEntry()) != null) {
			if (zipEntry.getName().equals("admitone.csv")) {
				byte[] bytes = new byte[1024];
				int len;
				while((len = zipInputStream.read(bytes)) > 0) {
					content += new String(bytes, 0, len);
				}
				break;
			}
		}
		zipInputStream.close();
				
		ftpClient.logout();
		ftpClient.disconnect();
		
		// System.out.println("ZIP CONTENT=" + content.length());		
		BufferedReader reader = new BufferedReader(new StringReader(content));
		
		Collection<Event> events = DAORegistry.getEventDAO().getAll();
		Collection<Tour> tours = DAORegistry.getTourDAO().getAll();			
		
		Map<String, Event> eventByKey = new HashMap<String, Event>(); 
		Map<Integer, Collection<Event>> eventstByTourId = new HashMap<Integer, Collection<Event>>();
		
		for(Event event: events) {
			Collection<Event> eventList = eventstByTourId.get(event.getTourId());
			if (eventList == null) {
				eventList = new ArrayList<Event>();
				eventstByTourId.put(event.getTourId(), eventList);
			}
			eventList.add(event);
		}
		
		String line;
		reader.readLine(); // skip first line
		while ((line = reader.readLine()) != null) {			
			// System.out.println("LINE = " + line);
			String[] lineTokens = line.split(",");
			if (lineTokens.length < 13) {
				break;
			}
			// EVENT,EVENTDATE,EVENTTIME,VENUE,NOTES,QUANTITY,ROW,SEATFROM,SEATTHRU,SECTION,COST,SPLITS,TICKETID

			// EVENT = tour name / precision
			String[] eventTokens = lineTokens[0].split("/");
			// remove stop words
			String eventNameQuery = QueryUtil.normalizeQuery(eventTokens[0]);
			String venueQuery = QueryUtil.normalizeQuery(lineTokens[3]);
			
			String eventDateString = lineTokens[1] + " " + lineTokens[2];
			DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

			DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy h:mm");
			Date eventDate = dateTimeFormat.parse(eventDateString);
			
			String eventKey = lineTokens[0] + " " + lineTokens[1] + " " + lineTokens[2];
			
			Event event = null;
						
			if (eventByKey.containsKey(eventKey)) {
				event = eventByKey.get(eventKey);
			} else {
				// first try to match the tour name

				for(Tour tour: tours) {
					int tourMatchingScore = QueryUtil.getMatchingScore(eventNameQuery, tour.getName());						

					// we use that in case the tourMatchingScore is equal
					int artistMatchingScore = QueryUtil.getMatchingScore(eventNameQuery, tour.getArtist().getName());
					
					if (tourMatchingScore < 1 && artistMatchingScore < 1) {
						continue;
					}
					
					// tour
					Collection<Event> eventList = eventstByTourId.get(tour.getId());
					if (eventList == null) {
						continue;
					}
					
					// System.out.println("MATCH TOUR=" + tour.getName());

					for(Event e: eventList) {
						// we use that in case the tourMatchingScore is equal
						int venueMatchingScore = QueryUtil.getMatchingScore(venueQuery, e.getFormattedVenueDescription());
						if (venueMatchingScore < 1) {
							continue;
						}
						// TBD issue.. Need to discuss with business				
						if (e.getLocalDate()!=null && e.getLocalDate().getYear() == eventDate.getYear()
								&& e.getLocalDate().getMonth() == eventDate.getMonth()
								&& e.getLocalDate().getDate() == eventDate.getDate()) {

							event = e;
							// System.out.println("MATCH VENUE=" + e.getFormattedVenueDescription() + lineTokens[1] + "/" + e.getLocalDate());
							break;
						}
					}
				}
			
				
				newEventMappings.add(new SeatWaveFeedEventMapping(lineTokens[0], eventDate, lineTokens[3], event));
				
				eventByKey.put(eventKey, event);					
			}
			
			if (event == null) {
				continue;
			}
																			
			String note = lineTokens[4];
			int quantity = Integer.parseInt(lineTokens[5]);

			String row = lineTokens[6];
			String section = lineTokens[9];
			
			double price = Double.parseDouble(lineTokens[10]);
			// EVENT,EVENTDATE,EVENTTIME,VENUE,NOTES,QUANTITY,ROW,SEATFROM,SEATTHRU,SECTION,COST,SPLITS,TICKETID
			String itemId = lineTokens[12].trim();
			
			int lotSize;
			String splits = lineTokens[11];
			if (splits.equals("Tickets must be purchased in pairs.")) {
				lotSize = 2;
			} else if (splits.equals("Tickets must be all be purchased together.")) {
				lotSize = quantity;
			} else {
				if ((quantity % 2) == 0) {
					lotSize = 2;
				} else {
					lotSize = 1;
				}
			}
			
			String startSeat = null;
			String endSeat = null;
			
			Matcher startSeatMatcher = seatPattern.matcher(lineTokens[7]);
			if (startSeatMatcher.find()) {
				startSeat = startSeatMatcher.group(1);
			}

			Matcher endSeatMatcher = seatPattern.matcher(lineTokens[8]);
			if (endSeatMatcher.find()) {
				endSeat = endSeatMatcher.group(1);
			}			
			
			TicketHit ticketHit = new TicketHit(Site.SEATWAVE, itemId, TicketType.REGULAR, quantity,
					lotSize, row, section, price, price, "SeatWave", TicketStatus.ACTIVE);
			ticketHit.setEventId(event.getId());
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			if(startSeat != null && endSeat != null){
				ticketHit.setSeat(startSeat + "-" + endSeat);
			}
			
			// System.out.println("TICKET HIT=" + ticketHit);

			indexTicketHit(ticketHitIndexer, ticketHit);
		}
		
		eventMappings = newEventMappings;
		*/		
		return true;
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "https://www.seatwave.com/buy/ticketpage.aspx?tsgID=" + ticket.getItemId() + "&TicLumpID=1&receipt=yes&sm=yes";
	}
	
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.seatwave.com";
	}	

	public static class SeatWaveFeedEventMapping {
		private String seatWaveEventName;
		private Date seatWaveEventDate;
		private String seatWaveVenue;
		private Event event;
		
		public SeatWaveFeedEventMapping(String seatWaveEventName, Date seatWaveEventDate, String seatWaveVenue, Event event) {
			this.seatWaveEventName = seatWaveEventName;
			this.seatWaveEventDate = seatWaveEventDate;
			this.seatWaveVenue = seatWaveVenue;
			this.event = event;
		}

		public String getSeatWaveEventName() {
			return seatWaveEventName;
		}

		public Date getSeatWaveEventDate() {
			return seatWaveEventDate;
		}

		public String getSeatWaveVenue() {
			return seatWaveVenue;
		}

		public Event getEvent() {
			return event;
		}
	}

}
