package com.admitone.tmat.ticketfetcher;


import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

	/**
 * StubHub Ticket Listing FetcherdoubleSectionPattern
 * Get tickets from web listing only (don't get end date and notes)
 * 
 */
public class FlashSeatsTicketListingFetcher extends TicketListingFetcher {	
		
	private final Logger logger = LoggerFactory.getLogger(FlashSeatsTicketListingFetcher.class);
	
	public FlashSeatsTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.FLASH_SEATS))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a flashseats crawl");
		}		
		Date beforeDate = new Date();
		//System.out.println("inside FLASHTIX.."+beforeDate+"..url.."+ticketListingCrawl.getQueryUrl());
		
		String proxy="";
//		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
//			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		
		
//		StubHubRedirectHandler redirectHandler = new StubHubRedirectHandler();
//		httpClient.setRedirectHandler(redirectHandler);
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId=null;
		
		String url = ticketListingCrawl.getQueryUrl();
		/*Pattern eventIdPattern = Pattern.compile(".*-(.*?).h");
		Matcher eventIdMatcher = eventIdPattern.matcher(url);
//		int eventIdPos = url.indexOf("productionId=="); 
		if (eventIdMatcher.find()) {
			eventId= eventIdMatcher.group(1);
//			eventId = url.substring(eventIdPos + 13, url.length());
		}*/ /*else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}*/

		// 
		// CHECK IF THE EVENT IS ACTIVE OR NOT
		//
		
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement(); 
		long startFetchingTime = System.currentTimeMillis();
		int counter=1;

		startFetchingTime = System.currentTimeMillis();
		
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpPost.addHeader("Accept", "application/json, text/javascript, */*");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());
		
		String content = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpPost);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
	
		Document doc = Jsoup.parse(content);
		
		Element noTixElement = doc.getElementById("ctl00_ctl02_lbEncourageListing");
        if(noTixElement != null && noTixElement.text().contains("No tickets are available for this event")) {
        	logger.info("FLASSEATS TIX No tickets found........"+ticketListingCrawl.getId());
        	return false;
        }
        if(doc.getElementById("ctl00_ctl02_dgOfferListing") == null) {
        	logger.info("FLASSEATS TIX No tickets found........"+ticketListingCrawl.getId());
        	return false;
        }
        boolean isMultiplePage = true;
        Element totalPage = doc.getElementById("ctl00_ctl02_fsPagerTop_lbTotalPages");
        if(totalPage != null) {
        	try {
        		//System.out.println("total page..."+totalPage.text());
        		if(Integer.parseInt(totalPage.text()) == 1) {
        			isMultiplePage = false;
        		}
        	}catch (Exception e) {
			}
        }

        if(isMultiplePage) {
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("__EVENTTARGET","ctl00$ctl02$fsPagerTop$viewAllHyperLink"));
			nameValuePairs.add(new BasicNameValuePair("__EVENTARGUMENT",""));
			nameValuePairs.add(new BasicNameValuePair("__VIEWSTATE",doc.getElementById("__VIEWSTATE").val()));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs)); 
	        
			try{
				response = httpClient.execute(httpPost);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity).trim();
					ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
			 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}

	        doc = Jsoup.parse(content);
        }

 		Element tableElement = doc.getElementById("ctl00_ctl02_dgOfferListing");
        //System.out.println(tableElement);
        Elements trTags = tableElement.select("tbody tr");
        
        for(Element tr : trTags){
        	int i = 0;
        	String section="",row="",notes="";
        	Double price=0.0;;
        	Integer quantity=0;
        	Elements tdTags = tr.getElementsByTag("td");
        	String ticketId = null;
             
        	for(Element td : tdTags){
        		switch (i) {
        			case 1:
        				section = td.text().trim();
						break;
					case 2:
						row = td.text().trim();
						break;
					case 3:
						String qtyStr = td.text().trim();
						if(qtyStr.contains("sold")) {
							System.out.println(qtyStr);
							i++;
							continue;
						}
						quantity = Integer.parseInt(qtyStr.replaceAll("Up To","").trim());
						break;
					case 4:
						price = Double.parseDouble(td.text().replaceAll("\\$", "").replaceAll(",", "").trim());
						break;
					case 5:
						if(td.getElementsByTag("a") != null) {
							notes = td.getElementsByTag("a").attr("title");
						}
						break;
					case 6:
						if(td.getElementsByTag("a") != null) {
							for (Element element : td.getElementsByTag("a")) {
								String ticketUrl = element.attr("href");
								Pattern ticketIdPattern = Pattern.compile(".*&ol=(.*?)&");
	            				Matcher ticketIdMatcher = ticketIdPattern.matcher(ticketUrl);
	            				if (ticketIdMatcher.find()) {
	            					ticketId= ticketIdMatcher.group(1);
	            					break;
	            				}
							}
						}
						break;
				}
        		i++;
             }
        	if(quantity == 0) {
        		continue;
        	}
        	if(ticketId == null) {
        		logger.info("FLASSEATS TIX ticketId not found..Id :"+ticketListingCrawl.getId()+"..tr : "+tr.html());
        		continue;
        	}
        	Double buyItNowPrice = price;
			String seat= "";
			TicketType ticketType = TicketType.REGULAR;
			if(notes!=null){
				if(notes.toUpperCase().contains("OBSTRUCT")){
					ticketType = TicketType.OBSTRUCTEDVIEW;
				} else if(notes.toUpperCase().contains("LIMITED")){
					ticketType = TicketType.LIMITEDVIEW;
				} else if(notes.toUpperCase().contains("WHEELCHAIR")){
					ticketType = TicketType.WHEELCHAIR;
				} else if(notes.toUpperCase().contains("CHILD TICKET")){
					ticketType = TicketType.CHILDTICKET;
				} else if(notes.toUpperCase().contains("PARTIAL VIEW")){
					ticketType = TicketType.PARTIALVIEW;
				}
			}
			TicketDeliveryType ticketDeliveryType = TicketDeliveryType.EDELIVERY;
			boolean inHand= false;
			/*if(note!=null && note.contains("Instant Download")){
					ticketDeliveryType = TicketDeliveryType.INSTANT;
					inHand= true;
			}else if(note!=null && (note.toLowerCase().contains("e-ticket") || note.toLowerCase().contains(("eticket")))){
				ticketDeliveryType = TicketDeliveryType.ETICKETS;
				inHand= true;  // As per ZoneII-144
			}*/
			int lotSize = 1;
			if ((quantity % 2) == 0 && quantity < 14) {
				lotSize = 2;
			}
			TicketHit ticketHit = new TicketHit(Site.FLASH_SEATS, ticketId,
					ticketType,
					quantity,lotSize,
					row, section, price, buyItNowPrice, "FlashSeats", TicketStatus.ACTIVE);
			ticketHit.setSoldQuantity(0);
			ticketHit.setSeat(seat);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setTicketDeliveryType(ticketDeliveryType);
			//ticketHit.setInHand(inHand);
			indexTicketHit(ticketHitIndexer, ticketHit);
			long startIndexationTime = System.currentTimeMillis();
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
 		}
        logger.info("FLASSEATS TIX TixComputation id :"+ticketListingCrawl.getId()+" time.."+(new Date().getTime()-beforeDate.getTime())+"..now..."+new Date());
		beforeDate = new Date();
 		

		saveCounterHistory(ticketListingCrawl, counter,proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}
	

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^https://www.flashseats.com/Default.aspx?.*") || url.matches("^http://www.flashseats.com/Default.aspx?.*");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
//		return "http://www.stubhub.com/largesellers-ticketcenter/";
		return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return ticketListingCrawl.getQueryUrl();
		//return "https://www.flashseats.com/";
//			if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//				return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//			}
//
//			return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	/**
	 * Redirect Handler Class 
	 */
	
}