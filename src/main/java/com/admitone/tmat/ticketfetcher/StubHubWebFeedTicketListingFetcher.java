package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.net.URI;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.StubHubApiTracking;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.ChangeProxy;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.StubhubCookieFetcher;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

	/**
 * StubHub Ticket Listing FetcherdoubleSectionPattern
 * Get tickets from web listing only (don't get end date and notes)
 * 
 */
public class StubHubWebFeedTicketListingFetcher extends TicketListingFetcher {	
	
	private static final long serialVersionUID = 1L;

	private static final Pattern sectionPattern = Pattern.compile("^\\d+:(\\d+)$");

	private static final Pattern seatPattern = Pattern.compile("(\\d+)");
	
	private static Integer MAX_HIT_PER_MINUTE=50;
	
	public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public StubHubWebFeedTicketListingFetcher() {
	}
	
	public JSONObject getJsonTicketListingsResponse(SimpleHttpClient httpClient, String eventId,int startParam,Boolean reProcessFlag)  throws Exception {
		
		String url = "https://www.stubhub.com/shape/search/inventory/v2/listings/?eventId="+eventId+"&start="+startParam+"&rows=1500&shstore=1";
		//String url = "https://www.stubhub.de/shape/search/inventory/v2/listings/?eventId=9683560&sectionStats=true&zoneStats=true&start=0&allSectionZoneStats=true&eventLevelStats=true&quantitySummary=true&rows=20&sort=price+asc&priceType=nonBundledPrice&tld=3";
		HttpGet httpGet = new HttpGet(url);
		httpGet.addHeader("Host", "www.stubhub.com");
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
		httpGet.addHeader("Accept", "application/json");
		httpGet.addHeader("Accept-Encoding", "gzip, deflate");
		httpGet.addHeader("Accept-Language", "en-US,en;q=0.5");
		httpGet.addHeader("Content-Type", "application/json");
		httpGet.addHeader("Referer", "https://www.stubhub.com/metallica-tickets-metallica-nashville-bridgestone-arena-1-24-2019/event/103491778/?sort=quality+desc");
		httpGet.addHeader("Cookie",httpClient.getStubhubCookie());
		
		JSONObject jsonObject = null;
		try{
			CloseableHttpResponse response = null;
			String content = null;
			try{
				response = httpClient.execute(httpGet,false);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity);
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}

			//System.out.println("stubhub : "+eventId +" : "+content);	
			
			jsonObject= new JSONObject(content);
			return jsonObject;
		}catch(Exception e){
			if(reProcessFlag) {
				return getJsonTicketListingsResponse(httpClient, eventId,startParam,false);
			}
			e.printStackTrace();
			return null;
		}
		
		/*System.out.println("==========================================================================");
		org.apache.http.Header[] responseHeaders = response.getAllHeaders();
    	for (org.apache.http.Header header : responseHeaders) {
    		System.out.println(header.getName() 
    		      + " : " + header.getValue());
    	}
    	System.out.println("==========================================================================");*/
		
		
		/*int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode < 200 || statusCode >= 300) {
	        System.out.println("Bad Respons from SH:" + statusCode);
	        throw new Exception("Bad Response from Stubhub Server");	
        }*/
	}

	

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.STUB_HUB))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a stubhub crawl");
		}		
		
		
		String proxy="";
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		
		
		StubHubRedirectHandler redirectHandler = new StubHubRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId;
		
		String url = ticketListingCrawl.getQueryUrl();
		int eventIdPos = url.indexOf("event_id="); 
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}

		// 
		// CHECK IF THE EVENT IS ACTIVE OR NOT
		//
		
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement(); 
		long startFetchingTime = System.currentTimeMillis();
		int counter=1;

		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());
		
		boolean callApiFlag = false;
		if(ticketListingCrawl.isForceReCrawlFlag()) {
			//callApiFlag = true;
		}
		if(!callApiFlag) {
		try {
			int startParam = 0;
			while (true) {
			
			startFetchingTime = System.currentTimeMillis();
			int j = 0;
			JSONObject jsonObject = null;
			while(j<2){
				try{
					jsonObject = getJsonTicketListingsResponse(httpClient, eventId,startParam,true);
					if (jsonObject != null && jsonObject.length() > 0) {
						// success or other client/website error like 404...
						
						break;
					}else{
						ChangeProxy.changeLuminatiProxy(httpClient,false);
						httpClient = new SimpleHttpClient(ticketListingCrawl.getSiteId());
						httpClient.assignHttpClient();
						if(ticketListingCrawl.getSiteId().equals("stubhub")){
							httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
						}
					}
				}catch (Exception e) {
					e.printStackTrace();
					ChangeProxy.changeLuminatiProxy(httpClient,false);
					try {
						httpClient = new SimpleHttpClient(ticketListingCrawl.getSiteId());
						httpClient.assignHttpClient();
						if(ticketListingCrawl.getSiteId().equals("stubhub")){
							httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
						}
					} catch (Exception e1) {
					}
				}
				j++;
			}
			
			ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
	 		//ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + (int)(content.getBytes().length));
	 		
	 		/*if(jsonObject.has("errors")){
	 			JSONObject errorObject = jsonObject.getJSONObject("errors");
	 			if (errorObject != null) {
	 				throw new Exception(errorObject.getJSONArray("error").getJSONObject(0).getString("errorMessage"));
	 			}
	 		}*/
	 		
	//		System.out.println(content);		
	 		
	 		if(jsonObject.has("listing") && jsonObject.has("totalListings")){
	 			
	 			JSONArray docsJSONArray = jsonObject.getJSONArray("listing");
	 			
	 			for (int i = 0 ; i < docsJSONArray.length() ; i++) {
	 				JSONObject docJSONObject = docsJSONArray.getJSONObject(i);
	
	 				String section = docJSONObject.getString("sectionName");
	 				
	 				// if we have 100:122 => returns 122
	 				Matcher matcher = sectionPattern.matcher(section);
	 				if (matcher.find()) {
	 					section = matcher.group(1);
	 				}
	 				
	 				/*"seats": [
	 				         {
	 				           "seatNumber": "7",
	 				           "listingId": 1307354651,
	 				           "row": "L",
	 				           "isGA": 0
	 				         },
	 				         {
	 				           "seatNumber": "9",
	 				           "listingId": 1307354651,
	 				           "row": "L",
	 				           "isGA": 0
	 				         },
	 				         {
	 				           "seatNumber": "11",
	 				           "listingId": 1307354651,
	 				           "row": "L",
	 				           "isGA": 0
	 				         }
	 				       ],*/
	 				
	 				JSONArray seatsArray = docJSONObject.getJSONArray("seats");
	 				int quantity = 0;
	 				String id = null;
	 				String row = "";
	 				String seats = null;
	 				for (int s = 0 ; s < seatsArray.length() ; s++) {
		 				JSONObject seatObj = seatsArray.getJSONObject(s);
		 				
		 				id = seatObj.getString("listingId");
		 				
		 				if(seatObj.has("seatNumber")){
		 					String seatNumber = seatObj.getString("seatNumber");
			 				if(s == 0){
			 					seats = seatNumber.toString();
			 				}else{
			 					seats = seats+","+seatNumber;
			 				}
		 				}
		 				if(seatObj.has("row")){
		 					row = seatObj.getString("row");
		 				}
		 				quantity++;
	 				}
	 				
	 				// seats can be:
	 				// - 3,4,5,6
	 				// - 15,16
	 				// - ga-5,ga-6,ga-7,ga-8
	 				// - General Admission
	 				String ticketHitSeats = null;
	 				if (seats != null && !seats.isEmpty()) {
	 					Integer startSeat = null;
	 					Integer endSeat = null;		
	 					String[] seatTokens = seats.split(",");
	 					//Tamil : Try block added to skip invalid format seat locations
	 					try {
		 					for(String seatToken: seatTokens) {
		 						Matcher seatMatcher = seatPattern.matcher(seatToken);
		 						if (seatMatcher.find()) {
		 							int seat = Integer.parseInt(seatMatcher.group(1));
		 							if (startSeat == null || seat < startSeat) {
		 								startSeat = seat;
		 							}
		
		 							if (endSeat == null || seat > endSeat) {
		 								endSeat = seat;							
		 							}
		 						}
		 					}
	 					} catch(Exception e) {
		 					
		 				}
	 					
	 					if (startSeat == null) {						
	 					} else if (startSeat == endSeat) {
	 						ticketHitSeats = "" + startSeat;
	 					} else {
	 						ticketHitSeats = startSeat + "-" + endSeat;
	 					}
	 					// System.out.println("SEATS=" + seats + ",start=" + startSeat + ",end=" + endSeat);
	 				}
	
	 				
	 				Double price = docJSONObject.getJSONObject("price").getDouble("amount");
	 				
	 				int split =1;
	 				if ((quantity % 2) == 0 && quantity < 14) {
	 					split = 2;
	 				}
	 							
	
	 				TicketStatus ticketStatus = TicketStatus.ACTIVE;
	 				Date endDate = null;
	 				
	 				// deliveryTypeList:
	 				//1: Electronic. The buyer will be able to download the ticket as a PDF file some time before the event.
	 				//2: Instant download. The buyer can download the ticket (as a PDF file) immediately.
	 				//4: LMS: The buyer can pick up the ticket from a StubHub last minute service center.
	 				//5: UPS: The ticket can be delivered to the buyer by United Parcel Service.
	 				
	 				Double buyItNowPrice;
	 				TicketType ticketType;
	 				TicketDeliveryType ticketDeliveryType = null;
	 				if(docJSONObject.has("deliveryTypeList")) {
	 					String deliveryType = docJSONObject.getString("deliveryTypeList").replaceAll("\\[", "").replaceAll("\\]", "");
		 				if(deliveryType.contains("2")){
		 					ticketDeliveryType = TicketDeliveryType.INSTANT;
		 				}else if("1".equals(deliveryType)){
		 					ticketDeliveryType = TicketDeliveryType.ETICKETS;
		 				}
	 				}
	 				
	 				
	 				buyItNowPrice = price;
	 				ticketType = TicketType.REGULAR;
					
					String ticketAttribute = null;
					if (docJSONObject.has("listingAttributeList")) {
						ticketAttribute = docJSONObject
								.getString("listingAttributeList");
						if (ticketAttribute != null && !ticketAttribute.isEmpty()) {
							ticketAttribute = ticketAttribute.replaceAll("\\[", "").replaceAll("\\]", "");
							String attr[] = ticketAttribute.split(",");
		
							for (String ticket : attr) {
								if (ticket.equals("201")) {
									ticketType = TicketType.OBSTRUCTEDVIEW;
								} else if (ticket.equals("202")
										|| ticket.equals("3306")) {
									ticketType = TicketType.WHEELCHAIR;
								} else if (ticket.equals("3248")) {
									ticketType = TicketType.LIMITEDVIEW;
								} else if (ticket.equals("5662")) {
									ticketType = TicketType.CHILDTICKET;
								}
							}
						}
					}
	 				
	 				
	 				
	 				TicketHit ticketHit = new TicketHit(Site.STUB_HUB, id,
	 						ticketType,
	 						quantity, split,
	 						row, section, price, buyItNowPrice, "StubHub", TicketStatus.ACTIVE);
	
	
	 				ticketHit.setTicketListingCrawl(ticketListingCrawl);
	 				ticketHit.setEndDate(endDate);
	 				ticketHit.setTicketDeliveryType(ticketDeliveryType);
	 				ticketHit.setSoldQuantity(0);
	 				
	 				ticketHit.setSeat(ticketHitSeats);
	 				
	 				System.out.println(ticketHit.toString());
	
	 				long startIndexationTime = System.currentTimeMillis();
	 				indexTicketHit(ticketHitIndexer, ticketHit);
	 				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
	 				
	 			}
	 			
	 		}else {
	 			System.out.println("stubhub No Tix :"+ticketListingCrawl.getId()+" : "+new Date());
	 			
//	 			if(ticketListingCrawl.isForceReCrawlFlag()) {
//	 				callApiFlag = true;
//	 			}
	 			break;
	 		}
	 		if(jsonObject.has("totalListings")) {
				int totalListings = jsonObject.getInt("totalListings");
				//int start = jsonObject.getInt("start");
				int rows = jsonObject.getInt("rows");
				startParam = startParam + rows;
				if(startParam>= totalListings) {
					break;	
				}
			} else {
				break;
			}
			}
		} catch (Exception e) {
			e.printStackTrace();
			
//			if(ticketListingCrawl.isForceReCrawlFlag()) {
// 				callApiFlag = true;
// 			} else {
 				throw e;
// 			}
		}
		
		} else {
			
//		if(callApiFlag) {
			
			SimpleHttpClient httpClientApi = null;
			try {
				httpClientApi = HttpClientStore.createHttpClient(Site.STUB_HUB_API);
				stubHubApiFetchTickets(httpClientApi, ticketHitIndexer, ticketListingCrawl, eventId);
				
				if(httpClientApi.getHost()!=null){
					proxy= httpClientApi.getHost();
				}else {
					proxy=InetAddress.getLocalHost().getHostName();
				}
			} catch (Exception e) {
				//Once crawl executed then change force recrawl flag to False to avoid API calling in auto executions
				ticketListingCrawl.setForceReCrawlFlag(false);
				
				e.printStackTrace();
				throw e;
			} finally {
				releaseHttpClient(httpClientApi);
			}
			
		}// else {
			//Delete apitracking if it runs through auto schedule
//			deleteApitracking(ticketListingCrawl.getId());
//		}
		
 		//Once crawl executed then change force recrawl flag to False to avoid API calling in auto executions
		ticketListingCrawl.setForceReCrawlFlag(false);
			
		
		saveCounterHistory(ticketListingCrawl, counter,proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}
	
	//Delete API Tracking if stubhub works fine. then autopricing will consider subhub tickets
	public void deleteApitracking(Integer crawlId) {
		try {
			DAORegistry.getStubHubApiTrackingDAO().updateStubHubStatus(crawlId);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	
	   

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^http://www.stubhub.com/.*[0-9]*/$") || url.matches("^http://www.stubhub.com/\\?event_id=\\d+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
//			if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//				return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//			}
//
//			return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	/**
	 * Redirect Handler Class 
	 */
	private class StubHubRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public StubHubRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {	
			//System.out.println("REDIRECT URL=" + redirectUrl);
			if (redirectUrl.startsWith("http://")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create("http://www.stubhub.com" + redirectUrl);				
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

	}	
	
	
	/**
	 * Initiate StubHub API call
	 * @param ticketHitIndexer
	 * @param ticketListingCrawl
	 * @return
	 * @throws Exception
	 */
	public boolean stubHubApi(TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl,String eventId) throws Exception{
		
		SimpleHttpClient httpClient = null;
		
		try {
			httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB_API);
			return stubHubApiFetchTickets(httpClient, ticketHitIndexer, ticketListingCrawl, eventId);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}finally{
			releaseHttpClient(httpClient);
		}
	}
	
	/**
	 * Fetch ticket listings from stubhub using the API
	 * @param httpClient
	 * @param ticketHitIndexer
	 * @param ticketListingCrawl
	 * @param eventId
	 * @return
	 * @throws Exception
	 */
	public boolean stubHubApiFetchTickets(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl,String eventId) throws Exception{
		
		StubHubApiTracking stubHubApiTracking = null;
		
		//Tamil : Limiting API hits to 10 per minute
		Property hitTimeProperty = DAORegistry.getPropertyDAO().get("stubhub.hit.minute.start.time");
		Property hitCountProperty =  DAORegistry.getPropertyDAO().get("stubhub.hit.minute.count");
		Integer minuteHitCount =  Integer.parseInt(hitCountProperty.getValue());
		Date minuteTime =  dbDateTimeFormat.parse(hitTimeProperty.getValue());
		Long nowTime = new Date().getTime();
		//If Max minute count reached then stop till remaining time
		if(minuteHitCount >= MAX_HIT_PER_MINUTE) {
			//Long nowTime = new Date().getTime();
			/*if((nowTime - minuteTime.getTime())<60*1000){
				Thread.sleep((60*1000)-(nowTime - minuteTime.getTime()));
			}*/
			System.out.println("Thread Sleep start time : "+new Date());
			while((nowTime - minuteTime.getTime())<60*1000) {
				nowTime = new Date().getTime();
			}
			System.out.println("Thread Sleep stop time : "+new Date());
			minuteTime = new Date();
			hitTimeProperty.setValue(dbDateTimeFormat.format(minuteTime));
			hitCountProperty.setValue("1");
			DAORegistry.getPropertyDAO().update(hitTimeProperty);
			DAORegistry.getPropertyDAO().update(hitCountProperty);
			
			//If time exceeds morethan one minute then reset time
		} else if((nowTime - minuteTime.getTime())>60*1000){
				minuteTime = new Date();
				hitTimeProperty.setValue(dbDateTimeFormat.format(minuteTime));
				hitCountProperty.setValue("1");
				
				DAORegistry.getPropertyDAO().update(hitTimeProperty);
				DAORegistry.getPropertyDAO().update(hitCountProperty);
		} else {
			//add stubhum minute count to 1 for each stubhub site hits
			DAORegistry.getPropertyDAO().addStubhubMinuteHitCount(1);
		}
		
		// check if the event is active or not
		long startFetchingTime = System.currentTimeMillis();
//		String postData = "Username=@@@@&Password=$$$$$";
		String encoded64 = new String(Base64.encodeBase64("7cV2eThY0FZ6_HGHzaQYtOzDeaYa:Y2fBEeMTg73VOWr9RWDLvNPyNEka".getBytes()));
		HttpPost httpLoginPost = new HttpPost("https://api.stubhub.com/login");
		httpLoginPost.setHeader("Authorization", "Basic " + encoded64);

		httpLoginPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
//		httpLoginPost.setEntity(new StringEntity(postData));
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "password"));
		params.add(new BasicNameValuePair("username", "amit.raut@rightthisway.com"));
		params.add(new BasicNameValuePair("password", "Ulaga123$$"));
		
		httpLoginPost.setEntity(new UrlEncodedFormEntity(params));	
		CloseableHttpResponse response = null;
		String content = null;
		try{
			response = httpClient.execute(httpLoginPost);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
				ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);	
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}	
		
		JSONObject jsonAccessObject = new JSONObject(content);
		
		String accessToken = jsonAccessObject.getString("access_token");
		//System.out.println("Access Token :: " +accessToken);
		String refreshToken = jsonAccessObject.getString("refresh_token");
		
		int startParam=0;
		while(true) {
			startFetchingTime = System.currentTimeMillis();
			
		HttpGet httpPost = new HttpGet("https://api.stubhub.com/search/inventory/v2?eventId=" + eventId+"&start="+startParam+"&rows=250"); //4323718
		httpPost.setHeader("Authorization", "Bearer " + accessToken);
		httpPost.addHeader("Accept", "application/json");
		
		try{
			response = httpClient.execute(httpPost);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
				ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}		
		
		JSONObject jsonObject = new JSONObject(content);
		/*if(!content.contains("\"errors\":null")){
			JSONObject errorObject = jsonObject.getJSONObject("errors");
			if (errorObject != null) {
				throw new Exception(errorObject.getJSONArray("error").getJSONObject(0).getString("errorMessage"));
			}
		}*/
	
		if(jsonObject.has("listing") && jsonObject.getJSONArray("listing").length() > 0){
		
			JSONArray docsJSONArray = jsonObject.getJSONArray("listing");
			for (int i = 0 ; i < docsJSONArray.length() ; i++) {
	
				
				JSONObject docJSONObject = docsJSONArray.getJSONObject(i);
	
				String section = docJSONObject.getString("sectionName");
				
				// if we have 100:122 => returns 122
				Matcher matcher = sectionPattern.matcher(section);
				if (matcher.find()) {
					section = matcher.group(1);
				}
	
				String row = docJSONObject.getString("row");
				Double price = docJSONObject.getJSONObject("currentPrice").getDouble("amount");
				int quantity = docJSONObject.getInt("quantity");
	//				int quantity_remain = docJSONObject.getInt("quantity_remain");
				// Using split logic same as it is in StubhubWebFeedTicketListingFetcher.java file.. 
				//int split = docJSONObject.getInt("ticketSplit"); int lotSize = 1;
				int split =1;
				if ((quantity % 2) == 0 && quantity < 14) {
					split = 2;
				}
				String id = docJSONObject.getString("listingId");				
	
				TicketStatus ticketStatus = TicketStatus.ACTIVE;
				Date endDate = null;
				
				// deliveryTypeList:
				//1: Electronic. The buyer will be able to download the ticket as a PDF file some time before the event.
				//2: Instant download. The buyer can download the ticket (as a PDF file) immediately.
				//4: LMS: The buyer can pick up the ticket from a StubHub last minute service center.
				//5: UPS: The ticket can be delivered to the buyer by United Parcel Service.
				
				String deliveryType = docJSONObject.getString("deliveryTypeList").replaceAll("\\[", "").replaceAll("\\]", "");
				Double buyItNowPrice;
				TicketType ticketType;
				TicketDeliveryType ticketDeliveryType = null;
				if(deliveryType.contains("2")){
					ticketDeliveryType = TicketDeliveryType.INSTANT;
				}else if("1".equals(deliveryType)){
					ticketDeliveryType = TicketDeliveryType.ETICKETS;
				}
				
				
				buyItNowPrice = price;
				ticketType = TicketType.REGULAR;
				
				String ticketAttribute = null;
				if (docJSONObject.has("listingAttributeList")) {
					ticketAttribute = docJSONObject
							.getString("listingAttributeList");
					if (ticketAttribute != null && !ticketAttribute.isEmpty()) {
						ticketAttribute = ticketAttribute.replaceAll("\\[", "").replaceAll("\\]", "");
						String attr[] = ticketAttribute.split(",");
	
						for (String ticket : attr) {
							if (ticket.equals("201")) {
								ticketType = TicketType.OBSTRUCTEDVIEW;
							} else if (ticket.equals("202")
									|| ticket.equals("3306")) {
								ticketType = TicketType.WHEELCHAIR;
							} else if (ticket.equals("3248")) {
								ticketType = TicketType.LIMITEDVIEW;
							} else if (ticket.equals("5662")) {
								ticketType = TicketType.CHILDTICKET;
							}
						}
					}
				}
				
				//System.out.println("Ticket Type :: " +ticketType +","+ticketAttribute);
				
				// seats can be:
				// - 3,4,5,6
				// - 15,16
				// - ga-5,ga-6,ga-7,ga-8
				// - General Admission
				String ticketHitSeats = null;
				if (docJSONObject.has("seatNumbers")) {
					Integer startSeat = null;
					Integer endSeat = null;		
					String seats = docJSONObject.getString("seatNumbers");
					String[] seatTokens = seats.split(",");
					
					//Tamil : Try block added to skip invalid format seat locations
					try {
						for(String seatToken: seatTokens) {
							Matcher seatMatcher = seatPattern.matcher(seatToken);
							if (seatMatcher.find()) {
								int seat = Integer.parseInt(seatMatcher.group(1));
								if (startSeat == null || seat < startSeat) {
									startSeat = seat;
								}
		
								if (endSeat == null || seat > endSeat) {
									endSeat = seat;							
								}
							}
						}
					}catch(Exception e) {
						
					}
					
					if (startSeat == null) {						
					} else if (startSeat == endSeat) {
						ticketHitSeats = "" + startSeat;
					} else {
						ticketHitSeats = startSeat + "-" + endSeat;
					}
					// System.out.println("SEATS=" + seats + ",start=" + startSeat + ",end=" + endSeat);
				}
				
				TicketHit ticketHit = new TicketHit(Site.STUB_HUB, id,
						ticketType,
						quantity, split,
						row, section, price, buyItNowPrice, "StubHub", TicketStatus.ACTIVE);
	
	
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setEndDate(endDate);
				ticketHit.setTicketDeliveryType(ticketDeliveryType);
	//				ticketHit.setSoldQuantity(quantity - quantity_remain);
				
				ticketHit.setSeat(ticketHitSeats);
	
				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
			
			}
			
		}else{
			System.out.println("No ticket listings found from api");
			break;
		}
		
		if(jsonObject.has("totalListings")) {
			int totalListings = jsonObject.getInt("totalListings");
			//int start = jsonObject.getInt("start");
			int rows = jsonObject.getInt("rows");
			startParam = startParam + rows;
			if(startParam>= totalListings) {
				break;	
			}
		} else {
			break;
		}
		}
		
		//capture api call hits into db
		stubHubApiTracking = new StubHubApiTracking();
		stubHubApiTracking.setCrawlId(ticketListingCrawl.getId());
		stubHubApiTracking.setLastRunRime(new Date());
		stubHubApiTracking.setStatus("DISABLED");
		stubHubApiTracking.setRunStatus("SUCCESS");
		stubHubApiTracking.setLastUpdated(new Date());
		stubHubApiTracking.setTrackingType("TICKET");
		try {
			//DAORegistry.getStubHubApiTrackingDAO().save(stubHubApiTracking);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return true;
	}
	
	
	public static void main(String[] args) throws Exception {
		StubHubWebFeedTicketListingFetcher feed = new StubHubWebFeedTicketListingFetcher();
		
		for (int j=0;j<1;j++) {
			
		
		List<Integer> eventIds = new ArrayList<Integer>();//LuminatiClientTest.getAllStubhubIds();
		eventIds.add(103491778);
		
		int i = 1;
		for (Integer eventId : eventIds) {
			
			Date start = new Date();
			System.out.println("===================================="+i+"===========start============================"+start);	
			
			SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB);
			StubhubCookieFetcher.getCookies(httpClient, null);
			if(null == httpClient.getStubhubCookie() || httpClient.getStubhubCookie().isEmpty()){
				httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
			}
		
			TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setId(123);
			ticketListingCrawl.setQueryUrl("https://www.stubhub.com?event_id="+eventId);///
			ticketListingCrawl.setSiteId(Site.STUB_HUB);
			ticketListingCrawl.setForceReCrawlFlag(false);
			System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
			System.out.println("===================================="+i+"===============end========================"+(new Date().getTime()-start.getTime()));	
		
			i++;
			if(i == 30){
				break;
			}
		}
		}
		
		System.out.println("Done :"+new Date());
	}
}