package com.admitone.tmat.ticketfetcher;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EIBoxTicket;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * EIBox Ticket Listing Fetcher. 
 */
public class EIBoxTicketListingFetcher extends TicketListingFetcher {

	public EIBoxTicketListingFetcher() {
	}	
	
	@Override
	protected SimpleHttpClient createHttpClient(TicketListingCrawl crawl) throws Exception {
		return null;
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.EIBOX))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an eibox crawl");
		}

		Long startProcessingTime = System.currentTimeMillis();

		
		Integer eiboxEventId = Integer.parseInt(ticketListingCrawl.getExtraParameter("queryEventId"));
		Integer eiboxVenueId = Integer.parseInt(ticketListingCrawl.getExtraParameter("queryVenueId"));
		Date eiboxEventDate = new Date(Long.parseLong(ticketListingCrawl.getExtraParameter("queryEventDate")));
		Collection<EIBoxTicket> eiBoxTickets = DAORegistry.getEiBoxTicketDAO().getEIBoxTicketsByEventId(eiboxEventId, eiboxVenueId, eiboxEventDate);
		if (eiBoxTickets == null) {
			return true;
		}
		for (EIBoxTicket eiBoxTicket: eiBoxTickets) {
			TicketHit ticketHit = new TicketHit(Site.EIBOX, eiBoxTicket.getTicketId().toString(),
					TicketType.REGULAR,
					eiBoxTicket.getQuantity(),
					eiBoxTicket.getQuantity(),
					eiBoxTicket.getRow(), eiBoxTicket.getSection(), eiBoxTicket.getPrice(), eiBoxTicket.getPrice(),
					eiBoxTicket.getCompanyName(), TicketStatus.ACTIVE);
			
			if (eiBoxTicket.getLowSeat() == null || eiBoxTicket.getLowSeat().equals(0)) {
				ticketHit.setSeat(null);
			} else if (eiBoxTicket.getHighSeat() == null || eiBoxTicket.getHighSeat().equals(0)) {
				ticketHit.setSeat(eiBoxTicket.getLowSeat().toString());				
			} else {
				ticketHit.setSeat(eiBoxTicket.getLowSeat().toString() + "-" + eiBoxTicket.getHighSeat().toString());								
			}
			
			// System.out.println("TICKET HIT=" + ticketHit);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);			

			Long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);						
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);				
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		
		return true;
	}
	
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.eimarketplace.com";
	}	
}