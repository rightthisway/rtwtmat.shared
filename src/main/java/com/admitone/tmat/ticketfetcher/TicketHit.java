package com.admitone.tmat.ticketfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;

/**
 * Ticket Hit Class.
 */
public class TicketHit {
	private String siteId;
	private String itemId;
	private TicketType ticketType;
	private String seller;
	private Integer quantity;
	private Integer soldQuantity = 0;

	private String venueState;
	private String venueCity;
	private String venueName;
	private String row;
	private String section;
	private String seat;
	
	private Double currentPrice;	
	private Double buyItNowPrice;
	
	private Date eventDate;
	private Date endDate;
	
	private Integer lotSize;
	
	private TicketStatus ticketStatus;
	
	private TicketListingCrawl ticketListingCrawl;
	
	private Integer eventId;
	
	private TicketDeliveryType ticketDeliveryType;
	private Boolean inHand;
	public TicketHit() {		
	}
			
	public TicketHit(String siteId, String itemId, TicketType ticketType,
			Integer quantity, Integer lotSize, 
			String row, String section, /* String seat, */
			Double currentPrice,
			Double buyItNowPrice,
			String seller,
			TicketStatus ticketStatus) {
		
		this.siteId = siteId;
		this.itemId = itemId;
		this.ticketType = ticketType;
		
		this.quantity = quantity;
		this.lotSize = lotSize;

		this.row = row;
		this.section = section;
		
		this.currentPrice = currentPrice;
		this.buyItNowPrice = buyItNowPrice;
		
		this.seller = seller;
		this.ticketStatus = ticketStatus;
	}
	
	// build ticketHit from ticket
	public TicketHit(Ticket ticket) {
		
		this.siteId = ticket.getSiteId();
		this.itemId = ticket.getItemId();
		this.ticketType = ticket.getTicketType();
		this.seller = ticket.getSeller();
		this.quantity = ticket.getQuantity();
		this.soldQuantity = ticket.getSoldQuantity();
		
		Event event = ticket.getEvent();
		if (event != null) {
			Venue venue = event.getVenue();
			if (venue != null) {
				this.venueName = venue.getBuilding();				
				this.venueState = venue.getState();				
				this.venueCity = venue.getCity();				
			}
			this.eventDate = event.getDate();
		}
		
		this.row = ticket.getRow();
		this.section = ticket.getSection();
		this.seat = ticket.getSeat();
		this.currentPrice = ticket.getCurrentPrice();
		this.buyItNowPrice = ticket.getBuyItNowPrice();
		this.endDate = ticket.getEndDate();
		this.lotSize = ticket.getLotSize();
		this.ticketStatus = ticket.getTicketStatus();
		this.ticketListingCrawl = ticket.getTicketListingCrawl();
		this.ticketDeliveryType = ticket.getTicketDeliveryType();
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemID(String itemId) {
		this.itemId = itemId;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	} 
	
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}	


	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}
	
	public String toString() {

		DateFormat format = new SimpleDateFormat("yyyy/MM/dd");
		format.setTimeZone(TimeZone.getTimeZone("UTC"));

		String formattedEventDate = (eventDate == null)?"n/a":format.format(eventDate);
		String formattedEndDate = (endDate == null)?"n/a":format.format(endDate);
/*
		return "Hit: itemID=" + itemId
			+ ", crawl_id=" + ((ticketListingCrawl == null)?"n/a":ticketListingCrawl.getId())
			+ ", ticketType=" + ticketType
			+ ", quantity=" + quantity
			+ ", soldQuantity=" + soldQuantity
			+ ", row=" + row
			+ ", section=" + section		
			+ ", seat=" + seat
			+ ", seller=" + seller			
			+ ", currentPrice=" + currentPrice			
			+ ", buyItNowPrice=" + buyItNowPrice
			+ ", eventDate=" + formattedEventDate
			+ ", endDate=" + formattedEndDate
			+ ", eventId=" + eventId
			+ ", venueState=" + venueState
			+ ", venueCity=" + venueCity
			+ ", venueName=" + venueName
			+ ", deliveryType=" + ticketDeliveryType
			+ ", lotSize=" + lotSize;
			*/
		return "section=" + section
				+ ", row=" + row
				+ ", quantity=" + quantity
				+ ", currentPrice=" + currentPrice;			
	}

	public String getVenueState() {
		return venueState;
	}

	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}

	public String getVenueCity() {
		return venueCity;
	}

	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Double getBuyItNowPrice() {
		return buyItNowPrice;
	}

	public void setBuyItNowPrice(Double buyItNowPrice) {
		this.buyItNowPrice = buyItNowPrice;
	}

	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public TicketListingCrawl getTicketListingCrawl() {
		return ticketListingCrawl;
	}

	public void setTicketListingCrawl(TicketListingCrawl ticketListingCrawl) {
		this.ticketListingCrawl = ticketListingCrawl;
	}

	public TicketStatus getTicketStatus() {
		return ticketStatus;
	}

	public void setTicketStatus(TicketStatus ticketStatus) {
		this.ticketStatus = ticketStatus;
	}

	public Integer getSoldQuantity() {
		return soldQuantity;
	}

	public void setSoldQuantity(Integer soldQuantity) {
		this.soldQuantity = soldQuantity;
	}

	public String getSeller() {
		return seller;
	}

	public void setSeller(String seller) {
		this.seller = seller;
	}

	public Integer getLotSize() {
		return lotSize;
	}

	public void setLotSize(Integer lotSize) {
		this.lotSize = lotSize;
	}

	public TicketType getTicketType() {
		return ticketType;
	}

	public void setTicketType(TicketType ticketType) {
		this.ticketType = ticketType;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public TicketDeliveryType getTicketDeliveryType() {
		return ticketDeliveryType;
	}

	public void setTicketDeliveryType(TicketDeliveryType ticketDeliveryType) {
		this.ticketDeliveryType = ticketDeliveryType;
	}

	public Boolean isInHand() {
		if(inHand==null){
			return false;
		}
		return inHand;
	}

	public void setInHand(Boolean inHand) {
		this.inHand = inHand;
	}
}
