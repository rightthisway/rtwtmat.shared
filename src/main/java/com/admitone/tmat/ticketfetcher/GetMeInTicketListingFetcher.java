package com.admitone.tmat.ticketfetcher;

import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * GetMeIn Ticket Listing Fetcher. 
 */
public class GetMeInTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(GetMeInTicketListingFetcher.class);
	
	private Pattern ticketPattern = Pattern.compile("<tr class=\"trTicket.*?<td.*?<span.*?>.*?TicketID=(.*?)&.*?<strong>(.*?)</strong>.*?<br.*?>(.*?)</span>.*?<td.*?<td.*?<td.*?>(.*?)<.*?td.*?>(.*?)</td>", Pattern.DOTALL);
	private Pattern rowPattern = Pattern.compile("Row:(.*)");
	private Pattern pricePattern = Pattern.compile("\\D+([0-9.,]+)");
	private Pattern qtyPattern = Pattern.compile("<option.*?=\"(.*?)\"");

	/**
	 * Fetch getmein ticket listing and extract the data.
	 */
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.GET_ME_IN))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a getmein crawl");
		}
		
		long startFetchingTime = System.currentTimeMillis();
		long indexationTime = 0;

		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "www.getmein.com");		
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		
		String content = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpGet);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());					
				ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startFetchingTime);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}

		long startProcessingTime = System.currentTimeMillis();
		
		Matcher ticketMatcher = ticketPattern.matcher(content);
		while(ticketMatcher.find()) {		
			
			String itemId = ticketMatcher.group(1);
			String section = ticketMatcher.group(2);
			String ticketDesc = ticketMatcher.group(3);

			String row;
			Matcher rowMatcher = rowPattern.matcher(ticketDesc);
			if (rowMatcher.find()) {
				row = rowMatcher.group(1).trim();
			} else {
				row = ticketDesc;
			}
			
			String priceString = ticketMatcher.group(4);
			
			Matcher priceMatcher = pricePattern.matcher(priceString);
			priceMatcher.find();
			priceString = priceMatcher.group(1);			

	        NumberFormat numberFormat = NumberFormat.getInstance();
	        Number number = numberFormat.parse(priceString);
			Double price = number.doubleValue();

			int minQuantity = Integer.MAX_VALUE;
			int maxQuantity = Integer.MIN_VALUE;
			
			Matcher qtyMatcher = qtyPattern.matcher(ticketMatcher.group(5));
			while(qtyMatcher.find()) {
				int quantity = Integer.parseInt(qtyMatcher.group(1));
				minQuantity = Math.min(quantity, minQuantity);
				maxQuantity = Math.max(quantity, maxQuantity);
			}
			
			TicketHit ticketHit = new TicketHit(Site.GET_ME_IN, itemId,
					TicketType.REGULAR,					
					maxQuantity, minQuantity,
					row, section, price, price, "GetMeIn", TicketStatus.ACTIVE);
			
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
						
			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);			
			indexationTime += System.currentTimeMillis() - startIndexationTime;
		}
		
		ticketListingCrawl.setIndexationTime(indexationTime);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - indexationTime);
		
		return true;
	}
	
	/**
	 * Return true if the url is a valid GetMeIn ticket listing url.
	 */
	@Override
	public boolean isValidUrl(String url) {
		// valid url:
		// http://www.getmein.com/tickets/beres-hammond-tickets/wolverhampton-113345.html
		// http://www.getmein.com/tickets/dance-nation-tickets/birmingham-107810.html
		// http://www.getmein.com/tickets/tina-turner-tickets/manchester-74825.html
		return url.matches("^http://www.getmein.com/tickets/.*-[0-9]+\\.html$");		
	}

	/**
	 * Return the ticket listing url of the getmein crawl.
	 */
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	
}
