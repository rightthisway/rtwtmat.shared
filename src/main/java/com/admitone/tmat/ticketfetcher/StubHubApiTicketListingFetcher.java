package com.admitone.tmat.ticketfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * StubHub Ticket Listing Fetcher
 * 
 * more info at: http://stubhubapi.nobisi.com/index.php?title=Ticket_Index_Document
 */
public class StubHubApiTicketListingFetcher extends TicketListingFetcher {	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final Logger LOGGER = LoggerFactory.getLogger(StubHubFeedTicketListingFetcher.class);
	
	// for ticket which has a section: 100:112 => 112
	private static final Pattern sectionPattern = Pattern.compile("^\\d+:(\\d+)$");

	private static final Pattern seatPattern = Pattern.compile("(\\d+)");
	private static Integer MAX_HIT_PER_MINUTE=10;
	public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public StubHubApiTicketListingFetcher() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.STUB_HUB_API) || ticketListingCrawl.getSiteId().equals(Site.STUB_HUB_API))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a stubhub api crawl");
		}		
		LOGGER.info("Stub hub Api initiated");	
		//http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// previous feed that we were using
		// http://www.stubhub.com/listingCatalog/getTicketsByEvent?limit=80&offset=1&sortby=p&orderby=a&eventId=565762&xbid=4981&cobrandId=47
		// new feed  for stubhub that we are using
		// http://partnerfeed.stubhub.com/listingCatalog/select/?q=%2B+stubhubDocumentType:ticket%0D%0A%2B+showTicketKeyStr:ticketACTIVE690326&version=2.2&start=0&rows=10000&indent=off&fl=curr_price+event_id+id+quantity+row_desc+section+sale_method+comments+time_left
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId = null;
		
		// Extract EventId
		String url = ticketListingCrawl.getQueryUrl();
		eventId = url.substring(url.lastIndexOf('-') + 1).trim();
		//System.out.println("===" +eventId);
		/*int eventIdPos = url.indexOf("event_id="); 
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}*/
		
		//Tamil : Limiting API hits to 10 per minute
		Property hitTimeProperty = DAORegistry.getPropertyDAO().get("stubhub.hit.minute.start.time");
		Property hitCountProperty =  DAORegistry.getPropertyDAO().get("stubhub.hit.minute.count");
		Integer minuteHitCount =  Integer.parseInt(hitCountProperty.getValue());
		Date minuteTime =  dbDateTimeFormat.parse(hitTimeProperty.getValue());
		Long nowTime = new Date().getTime();
		//If Max minute count reached then stop till remaining time
		if(minuteHitCount >= MAX_HIT_PER_MINUTE) {
			//Long nowTime = new Date().getTime();
			if((nowTime - minuteTime.getTime())<60*1000){
				Thread.sleep((60*1000)-(nowTime - minuteTime.getTime()));
			}
			minuteTime = new Date();
			hitTimeProperty.setValue(dbDateTimeFormat.format(minuteTime));
			hitCountProperty.setValue("1");
			DAORegistry.getPropertyDAO().update(hitTimeProperty);
			DAORegistry.getPropertyDAO().update(hitCountProperty);
			
			//If time exceeds morethan one minute then reset time
		} else if((nowTime - minuteTime.getTime())>60*1000){
				minuteTime = new Date();
				hitTimeProperty.setValue(dbDateTimeFormat.format(minuteTime));
				hitCountProperty.setValue("1");
				
				DAORegistry.getPropertyDAO().update(hitTimeProperty);
				DAORegistry.getPropertyDAO().update(hitCountProperty);
		} else {
			//add stubhum minute count to 1 for each stubhub site hits
			DAORegistry.getPropertyDAO().addStubhubMinuteHitCount(1);
		}
		
		// check if the event is active or not
		long startFetchingTime = System.currentTimeMillis();
//		String postData = "Username=@@@@&Password=$$$$$";
		String encoded64 = new String(Base64.encodeBase64("7cV2eThY0FZ6_HGHzaQYtOzDeaYa:Y2fBEeMTg73VOWr9RWDLvNPyNEka".getBytes()));
		HttpPost httpLoginPost = new HttpPost("https://api.stubhub.com/login");
		httpLoginPost.setHeader("Authorization", "Basic " + encoded64);

		httpLoginPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
//		httpLoginPost.setEntity(new StringEntity(postData));
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("grant_type", "password"));
		params.add(new BasicNameValuePair("username", "amit.raut@rightthisway.com"));
		params.add(new BasicNameValuePair("password", "Admit123"));
		
		httpLoginPost.setEntity(new UrlEncodedFormEntity(params));
		
		String content = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpLoginPost);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
				ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
		JSONObject jsonAccessObject = new JSONObject(content);
		
		String accessToken = jsonAccessObject.getString("access_token");
		System.out.println("Access Token :: " +accessToken);
		String refreshToken = jsonAccessObject.getString("refresh_token");
		
		HttpGet httpPost = new HttpGet("https://api.stubhub.com/search/inventory/v2?eventId=" + eventId+"&rows=10000"); //4323718
		httpPost.setHeader("Authorization", "Bearer " + accessToken);
		httpPost.addHeader("Accept", "application/json");		
		startFetchingTime = System.currentTimeMillis();
		try{
			response = httpClient.execute(httpPost);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				System.out.println("tix api :"+content);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
				ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);	
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}	
		
		JSONObject jsonObject = new JSONObject(content);
		/*if(!content.contains("\"errors\":null")){
			JSONObject errorObject = jsonObject.getJSONObject("errors");
			if (errorObject != null) {
				throw new Exception(errorObject.getJSONArray("error").getJSONObject(0).getString("errorMessage"));
			}
		}*/
	
		JSONArray docsJSONArray = jsonObject.getJSONArray("listing");
		
		for (int i = 0 ; i < docsJSONArray.length() ; i++) {
			JSONObject docJSONObject = docsJSONArray.getJSONObject(i);

			String section = docJSONObject.getString("sectionName");
			
			// if we have 100:122 => returns 122
			Matcher matcher = sectionPattern.matcher(section);
			if (matcher.find()) {
				section = matcher.group(1);
			}

			String row = docJSONObject.getString("row");
			Double price = docJSONObject.getJSONObject("currentPrice").getDouble("amount");
			int quantity = docJSONObject.getInt("quantity");
//				int quantity_remain = docJSONObject.getInt("quantity_remain");
			// Using split logic same as it is in StubhubWebFeedTicketListingFetcher.java file.. 
			//int split = docJSONObject.getInt("ticketSplit"); int lotSize = 1;
			int split =1;
			if ((quantity % 2) == 0 && quantity < 14) {
				split = 2;
			}
			String id = docJSONObject.getString("listingId");				

			TicketStatus ticketStatus = null;
			Date endDate = null;
			
			// deliveryTypeList:
			//1: Electronic. The buyer will be able to download the ticket as a PDF file some time before the event.
			//2: Instant download. The buyer can download the ticket (as a PDF file) immediately.
			//4: LMS: The buyer can pick up the ticket from a StubHub last minute service center.
			//5: UPS: The ticket can be delivered to the buyer by United Parcel Service.
			
			String deliveryType = docJSONObject.getString("deliveryTypeList").replaceAll("\\[", "").replaceAll("\\]", "");
			Double buyItNowPrice;
			TicketType ticketType;
			TicketDeliveryType ticketDeliveryType = null;
			if(deliveryType.contains("2")){
				ticketDeliveryType = TicketDeliveryType.INSTANT;
			}else if("1".equals(deliveryType)){
				ticketDeliveryType = TicketDeliveryType.ETICKETS;
			}
			
			
			buyItNowPrice = price;
			ticketType = TicketType.REGULAR;
			
			String ticketAttribute = null;
			if (docJSONObject.has("listingAttributeList")) {
				ticketAttribute = docJSONObject
						.getString("listingAttributeList");
				if (ticketAttribute != null && !ticketAttribute.isEmpty()) {
					ticketAttribute = ticketAttribute.replaceAll("\\[", "").replaceAll("\\]", "");
					String attr[] = ticketAttribute.split(",");

					for (String ticket : attr) {
						if (ticket.equals("201")) {
							ticketType = TicketType.OBSTRUCTEDVIEW;
						} else if (ticket.equals("202")
								|| ticket.equals("3306")) {
							ticketType = TicketType.WHEELCHAIR;
						} else if (ticket.equals("3248")) {
							ticketType = TicketType.LIMITEDVIEW;
						} else if (ticket.equals("5662")) {
							ticketType = TicketType.CHILDTICKET;
						}
					}
				}
			}
			
			//System.out.println("Ticket Type :: " +ticketType +","+ticketAttribute);
			
			// seats can be:
			// - 3,4,5,6
			// - 15,16
			// - ga-5,ga-6,ga-7,ga-8
			// - General Admission
			String ticketHitSeats = null;
			if (docJSONObject.has("seatNumbers")) {
				Integer startSeat = null;
				Integer endSeat = null;		
				String seats = docJSONObject.getString("seatNumbers");
				String[] seatTokens = seats.split(",");
				
				for(String seatToken: seatTokens) {
					Matcher seatMatcher = seatPattern.matcher(seatToken);
					if (seatMatcher.find()) {
						int seat = Integer.parseInt(seatMatcher.group(1));
						if (startSeat == null || seat < startSeat) {
							startSeat = seat;
						}

						if (endSeat == null || seat > endSeat) {
							endSeat = seat;							
						}
					}
				}
				
				if (startSeat == null) {						
				} else if (startSeat == endSeat) {
					ticketHitSeats = "" + startSeat;
				} else {
					ticketHitSeats = startSeat + "-" + endSeat;
				}
				// System.out.println("SEATS=" + seats + ",start=" + startSeat + ",end=" + endSeat);
			}
			
			TicketHit ticketHit = new TicketHit(Site.STUB_HUB, id,
					ticketType,
					quantity, split,
					row, section, price, buyItNowPrice, "StubHub", ticketStatus);


			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setEndDate(endDate);
			ticketHit.setTicketDeliveryType(ticketDeliveryType);
//				ticketHit.setSoldQuantity(quantity - quantity_remain);
			
			ticketHit.setSeat(ticketHitSeats);

			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
			
			// System.out.println("TICKET HIT=" + ticketHit);
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());

		return true;
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^http://www.stubhub.com/.*[0-9]*/$") || url.matches("^http://www.stubhub.com/\\?event_id=\\d+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
//		if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//			return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//		}
//
//		return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	public static void main(String[] args) throws Exception {
		StubHubFeedTicketListingFetcher feed = new StubHubFeedTicketListingFetcher();
		SimpleHttpClient httpClient = new SimpleHttpClient("Stubhub");
		TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		
		
		/*List<String> eventIds = new ArrayList<String>();
		eventIds.add("http://www.stubhub.com/?event_id=9047200");
		eventIds.add("http://www.stubhub.com/?event_id=9047198");
		eventIds.add("http://www.stubhub.com/?event_id=9047201");
		eventIds.add("http://www.stubhub.com/?event_id=9152810");
		eventIds.add("http://www.stubhub.com/?event_id=9047199");
		//eventIds.add("http://www.stubhub.com/?event_id=9051253");
		eventIds.add("http://www.stubhub.com/?event_id=9047202");
		eventIds.add("http://www.stubhub.com/?event_id=9047199");
		eventIds.add("http://www.stubhub.com/?event_id=9047200");
		eventIds.add("http://www.stubhub.com/?event_id=9047198");
		eventIds.add("http://www.stubhub.com/?event_id=9047201");
		//eventIds.add("http://www.stubhub.com/?event_id=9047197");
		//eventIds.add("http://www.stubhub.com/?event_id=9047196");
		eventIds.add("http://www.stubhub.com/?event_id=9152810");
		eventIds.add("http://www.stubhub.com/?event_id=9152805");
		

		String eventId = "";
		for(Iterator<String> i = eventIds.iterator(); i.hasNext(); ) {
			eventId = i.next();
		    System.out.println(eventId);	  
		    ticketListingCrawl.setQueryUrl(eventId);///
			ticketListingCrawl.setSiteId(Site.STUBHUB_FEED);
			System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
		}*/
		//http://www.stubhub.com/parking-passes-only-luke-bryan-wichita-intrust-bank-arena-parking-lots-5-13-2016-9422155"
		ticketListingCrawl.setQueryUrl("http://www.stubhub.com/parking-passes-only-luke-bryan-wichita-intrust-bank-arena-parking-lots-5-13-2016-9422155");
		ticketListingCrawl.setSiteId(Site.STUB_HUB_API);
		System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl)); 
	}
}