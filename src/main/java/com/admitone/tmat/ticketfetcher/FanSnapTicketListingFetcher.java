package com.admitone.tmat.ticketfetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * FanSnap Ticket Listing Fetcher. 
 */
public class FanSnapTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(FanSnapTicketListingFetcher.class);
	private Pattern queryEventIdPattern = Pattern.compile("-([0-9]+)$");

	private static final Pattern seatPattern = Pattern.compile("(\\d+)");

	public FanSnapTicketListingFetcher() {
	}
		
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.FAN_SNAP))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a fansnap crawl");
		}
		
		long startProcessingTime = System.currentTimeMillis();
		
		// http://www.fansnap.com/san-francisco-giants-tickets/new-york-mets-vs-san-francisco-giants/may-17-2009-68311
		Matcher queryEventIdMatcher = queryEventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
		queryEventIdMatcher.find();
		String queryEventId = queryEventIdMatcher.group(1);
		
		int ticketCount = -1;
		int page = 1;
		
		do {

			Long startFetchingTime = System.currentTimeMillis();						
			HttpGet httpGet = new HttpGet("http://www.fansnap.com/seats/ajax/get_tix_data?order=price&dir=asc&perpage=75&page=" + page + "&event_ids[]=" + queryEventId + "&mode=partial&map_type=interactive&");
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("X-Requested-With", "XMLHttpRequest");
			httpGet.addHeader("Referer", ticketListingCrawl.getQueryUrl());
			
			String content = null;
			CloseableHttpResponse response = null;
			try{
				response = httpClient.execute(httpGet);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity);
					ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());	
					ticketListingCrawl.setFetchingTime(startFetchingTime - System.currentTimeMillis());
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
			
			// System.out.println("CONTENT=" + content);
	
			JSONObject jsonObject = new JSONObject(content);			
			JSONArray setsArray = jsonObject.getJSONArray("sets");
	
			if (ticketCount == -1) {
				ticketCount = jsonObject.getInt("total");
			}
			
			for(int i = 0 ; i < setsArray.length() ; i++) {
				JSONObject ticketObject = setsArray.getJSONObject(i);
		
				String section = ticketObject.getString("section");
				// System.out.println("SECTION=" + section);
	
				String row = ticketObject.getString("row");
				// System.out.println("ROW=" + row);
					
				int quantity = ticketObject.getInt("qty");
	
				// split can be:
				// - 2
				// - 4,2
				String splitString = ticketObject.getString("split");
				String[] tokens = splitString.split(",");
				int lotSize = quantity;
				for (String token: tokens) {
					lotSize = Math.min(Integer.parseInt(token), lotSize);
				}
	
				// System.out.println("NOTES=" + notes);
	
				// seats:
				// ""
				// General Admission
				// 1025,1026
				String seats = ticketObject.getString("seats");
				String[] seatTokens = seats.split(",");
				Integer startSeat = null;
				Integer endSeat = null;				
				
				for(String seatToken: seatTokens) {
					Matcher seatMatcher = seatPattern.matcher(seatToken);
					if (seatMatcher.find()) {
						int seat = Integer.parseInt(seatMatcher.group(1));
						if (startSeat == null || seat < startSeat) {
							startSeat = seat;
						}
	
						if (endSeat == null || seat > endSeat) {
							endSeat = seat;							
						}
					}
				}
				
				List<Object[]> idBrokerSalePriceHandlingFeeList = new ArrayList<Object[]>();
				
				if (ticketObject.has("deals")) {
					JSONArray dealsArray = ticketObject.getJSONArray("deals");
					for(int j = 0 ; j < dealsArray.length() ; j++) {
						JSONObject dealObject = dealsArray.getJSONObject(j);
						
						String itemId = Long.toString(dealObject.getLong("id"));
						String broker = dealObject.getString("broker");
						Double salePrice = dealObject.getDouble("sale_price");
						Double handlingFee = dealObject.getDouble("handling_fee");
						
						idBrokerSalePriceHandlingFeeList.add(new Object[] {
							itemId, broker, salePrice, handlingFee	
						});
					}
				} else {
					String itemId = Long.toString(ticketObject.getLong("id"));
					String broker = ticketObject.getString("broker");						
					Double salePrice = ticketObject.getDouble("sale_price");
					Double handlingFee = ticketObject.getDouble("handling_fee");
					idBrokerSalePriceHandlingFeeList.add(new Object[] {
							itemId, broker, salePrice, handlingFee	
						});					
				}
				
				for (Object[] idBrokerSalePriceHandlingFee: idBrokerSalePriceHandlingFeeList) {
					String itemId = (String)idBrokerSalePriceHandlingFee[0];
					String broker = (String)idBrokerSalePriceHandlingFee[1];						
					Double salePrice = (Double)idBrokerSalePriceHandlingFee[2];
					Double handlingFee = (Double)idBrokerSalePriceHandlingFee[3];
					
					TicketHit ticketHit = new TicketHit(Site.FAN_SNAP, itemId,
							TicketType.REGULAR,								
							quantity, lotSize, 
							row, section, salePrice, salePrice + handlingFee, broker, TicketStatus.ACTIVE);
					ticketHit.setTicketListingCrawl(ticketListingCrawl);
					
					ticketHit.setSeat((seats == null)?null:startSeat + "-" + endSeat);

					Long startIndexationTime = System.currentTimeMillis();
					
					//System.out.println("TICKET HIT=" + ticketHit);
					indexTicketHit(ticketHitIndexer, ticketHit);
					ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);						
		
				}
			}
			
			page++;
		} while ((page - 1) * 75 < ticketCount);
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}
	
	@Override
	public boolean isValidUrl(String url) {
		// valid url: 
		// http://www.fansnap.com/san-francisco-giants-tickets/new-york-mets-vs-san-francisco-giants/may-17-2009-68311
		// http://www.fansnap.com/arizona-diamondbacks-tickets/san-francisco-giants-vs-arizona-diamondbacks/68921/april-26-2009-68922
		return url.matches("^http://www.fansnap.com/.*-[0-9]+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.fansnap.com/checkout/clickout/" + ticket.getItemId() + "?ctx=c%3D300%3Bmt%3Dint%3Btsp%3D0%3Bdt%3D1&quantity=" + ticket.getLotSize() + "&x=23&y=18";
	}	
}