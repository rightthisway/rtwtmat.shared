package com.admitone.tmat.ticketfetcher;


import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public abstract class TicketItemFetcher {
	
	public TicketItemFetcher() {
	}

	public abstract TicketHit runFetchTicketItem(SimpleHttpClient httpClient, TicketListingCrawl crawl, String itemId) throws Exception;
	
	public final TicketHit fetchTicketItem(TicketListingCrawl crawl, String itemId) throws Exception {
		SimpleHttpClient httpClient = null;		
		try {
			httpClient = HttpClientStore.createHttpClient();
			return runFetchTicketItem(httpClient, crawl, itemId);
		} finally {
			HttpClientStore.releaseHttpClient(httpClient);
		}		
	}
}
