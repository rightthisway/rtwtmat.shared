package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.ChangeProxy;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;
import com.admitone.tmat.utils.httpclient.VividSeatCookieFetcher;

	/**
 * StubHub Ticket Listing FetcherdoubleSectionPattern
 * Get tickets from web listing only (don't get end date and notes)
 * 
 */
public class VividSeatTicketListingFetcher extends TicketListingFetcher {	
		
	private final Logger logger = LoggerFactory.getLogger(VividSeatTicketListingFetcher.class);
	
	public VividSeatTicketListingFetcher() {
	}

	public JSONObject getJsonTicketListingsResponse(SimpleHttpClient httpClient, String eventId)  throws Exception {
		
		String url = "https://www.vividseats.com/rest/v2/web/listings/"+eventId;
    	HttpGet httpGet = new HttpGet(url);
    	httpGet.addHeader("Host", "www.vividseats.com");
    	httpGet.addHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
    	httpGet.addHeader(":authority", "www.vividseats.com");
    	httpGet.addHeader(":method", "GET");
    	httpGet.addHeader(":path", "/rest/v2/web/listings/"+eventId);
    	httpGet.addHeader(":scheme", "https");
    	httpGet.addHeader("accept", "application/json, text/plain, */*");
    	httpGet.addHeader("accept-encoding", "gzip, deflate, br");
    	httpGet.addHeader("accept-language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
    	httpGet.addHeader("Content-Type", "application/json");
    	httpGet.addHeader("referer", "www.vividseat.com");
    	httpGet.addHeader("x-distil-ajax", "uurrvyqyyqxrtytrzw");
    	//request11.addHeader("x-requested-with", "XMLHttpRequest");
    	httpGet.addHeader("cookie",httpClient.getVividCookie());
    	httpGet.addHeader("dnt", "1");
		 
		JSONObject jsonObject = null;
		String content = null;
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(httpGet);
			System.out.println("INVENTORY->"+response);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				System.out.println("CONTENT: "+content);
				jsonObject = new JSONObject(content);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		return jsonObject;
	}
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.VIVIDSEAT))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a vividseat crawl");
		}		
		
		try {
		String proxy="";
//		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
//			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		System.out.println("PROXY: "+proxy);
//		StubHubRedirectHandler redirectHandler = new StubHubRedirectHandler();
//		httpClient.setRedirectHandler(redirectHandler);
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId=null;
		
		String url = ticketListingCrawl.getQueryUrl();
		Pattern eventIdPattern = Pattern.compile(".*-(.*?).h");
		Matcher eventIdMatcher = eventIdPattern.matcher(url);
//		int eventIdPos = url.indexOf("productionId=="); 
		if (eventIdMatcher.find()) {
			eventId= eventIdMatcher.group(1);
//			eventId = url.substring(eventIdPos + 13, url.length());
		} /*else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}*/

		// 
		// CHECK IF THE EVENT IS ACTIVE OR NOT
		//
		
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement(); 
		long startFetchingTime = System.currentTimeMillis();
		int counter=1;

		startFetchingTime = System.currentTimeMillis();
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());
		
		int j = 0;
		
		JSONObject jsonObject = null;
		while(j<2){
			try{
				jsonObject = getJsonTicketListingsResponse(httpClient, eventId);
				if (jsonObject != null && jsonObject.length() > 0) {
					// success or other client/website error like 404...
					
					break;
				}else{
					System.out.println("Vivd Exception Response : "+eventId+" : "+j);
					ChangeProxy.changeLuminatiProxy(httpClient,false);
					httpClient = new SimpleHttpClient(ticketListingCrawl.getSiteId());
					httpClient.assignHttpClient();
					if(ticketListingCrawl.getSiteId().equals("vividseat")){
						httpClient.setVividCookie(VividSeatCookieFetcher.getCookies(httpClient, eventId));
						//httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
					}
				}
			}catch (Exception e) {
				System.out.println("Vivd Exception Response : "+eventId+" : "+j);
				ChangeProxy.changeLuminatiProxy(httpClient,false);
				try {
					httpClient = new SimpleHttpClient(ticketListingCrawl.getSiteId());
					httpClient.assignHttpClient();
					if(ticketListingCrawl.getSiteId().equals("vividseat")){
						httpClient.setVividCookie(VividSeatCookieFetcher.getCookies(httpClient, eventId));
						//httpClient.setStubhubCookie(StubhubCookieFetcher.getCookies(httpClient,null));
					}
				} catch (Exception e1) {
				}
			}
			j++;
		}
		
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
 		//ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
 		
		if(jsonObject.has("tickets")){
	// 		String globalStr = eventResult.getString("global");
	// 		JSONObject global = new JSONObject(globalStr.substring(1,globalStr.length()-1));
	 		JSONArray ticketArray = jsonObject.getJSONArray("tickets");
	// 		Pattern piggbackPattern = Pattern.compile("[\\\\|,|/|&]");
	// 		Matcher piggbackMatcher ;
	 		for(int i=0;i<ticketArray.length();i++){
	 			JSONObject ticketJSON = ticketArray.getJSONObject(i);
	// 			String valid = ticketJSON.getString("vi");
	 			/*if(valid==null || valid.equals("null")){
	 				continue;
	 			}*/
	 			String note = ticketJSON.getString("n");
	 			String section = ticketJSON.getString("s");
	 			String row = ticketJSON.getString("r");
	// 			piggbackMatcher = piggbackPattern.matcher(row);
	//			if(piggbackMatcher.find()){
	////					System.out.println(rowActuallyMatcher.group());
	//				continue;
	//			}
	//			JSONObject tcJson = ticketJSON.getJSONObject("tc");
				Double price = ticketJSON.getDouble("p");
				Double buyItNowPrice = price;
				Integer quantity = ticketJSON.getInt("q");
	//			Integer quantity_remain = null;
				String seat= "";
				/*if(seat!=null && !seat.isEmpty()){
					String temp[]=seat.split(",");
					if(temp.length>1){
						seat=temp[0]+"-" + temp[temp.length-1];
					}else{
						seat="";
					}
				}*/
				String ticketId = ticketJSON.getString("i");
				TicketType ticketType = TicketType.REGULAR;
				if(note!=null){
					if(note.toUpperCase().contains("OBSTRUCT")){
						ticketType = TicketType.OBSTRUCTEDVIEW;
					} else if(note.toUpperCase().contains("LIMITED")){
						ticketType = TicketType.LIMITEDVIEW;
					} else if(note.toUpperCase().contains("WHEELCHAIR")){
						ticketType = TicketType.WHEELCHAIR;
					} else if(note.toUpperCase().contains("CHILD TICKET")){
						ticketType = TicketType.CHILDTICKET;
					} else if(note.toUpperCase().contains("PARTIAL VIEW")){
						ticketType = TicketType.PARTIALVIEW;
					}
				}
				TicketDeliveryType ticketDeliveryType = null;
	//			String instantDownload = ticketJSON.getString("de");
	//			String electronics = ticketJSON.getString("et");
				boolean inHand= false;
				if(note!=null && note.contains("Instant Download")){
						ticketDeliveryType = TicketDeliveryType.INSTANT;
						inHand= true;
				}else if(note!=null && (note.toLowerCase().contains("e-ticket") || note.toLowerCase().contains(("eticket")) 
						|| note.toLowerCase().contains("E-tickets"))){
					ticketDeliveryType = TicketDeliveryType.ETICKETS;
					inHand= true;  // As per ZoneII-144
				}
				int lotSize = 1;
				if ((quantity % 2) == 0 && quantity < 14) {
					lotSize = 2;
				}
				TicketHit ticketHit = new TicketHit(Site.VIVIDSEAT, ticketId,
						ticketType,
						quantity,lotSize,
						row, section, price, buyItNowPrice, "VividSeat", TicketStatus.ACTIVE);
				ticketHit.setSoldQuantity(0);
				ticketHit.setSeat(seat);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setTicketDeliveryType(ticketDeliveryType);
				ticketHit.setInHand(inHand);
				indexTicketHit(ticketHitIndexer, ticketHit);
				long startIndexationTime = System.currentTimeMillis();
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
	 		}
 		} else {
 			System.err.println("Vivd No Tix : "+ticketListingCrawl.getId()+" : "+new Date());
 		}
 		

		//saveCounterHistory(ticketListingCrawl, counter,proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		} catch(Exception e) {
			e.printStackTrace();
		}
		return true;
	}
	

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^http://www.vividseat.com/.*[0-9]*/$") || url.matches("^http://www.vividseat.*.com/.*.[0-9]*.html$") || url.matches("^www.vividseat.com/\\?productionId=\\d+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
//		return "http://www.stubhub.com/largesellers-ticketcenter/";
		return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.vividseat.com/";
//			if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//				return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//			}
//
//			return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	/**
	 * Redirect Handler Class 
	 */
	
	public static void main(String[] args) throws Exception {
		VividSeatTicketListingFetcher feed = new VividSeatTicketListingFetcher();
		
		List<Integer> eventIds = new ArrayList<Integer>();//LuminatiClientTest.getAllStubhubIds();
		eventIds.add(2016983);
		for (int i=1;i<=30;i++) {
			
			
			System.out.println("===================================="+i+"=======================================");	
			
			SimpleHttpClient httpClient = HttpClientStore.createHttpClient("vividseat", "", "");
			
			TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
			TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
			ticketListingCrawl.setId(123);
			ticketListingCrawl.setQueryUrl("www.vividseat.com/theatre/once-on-this-island-tickets/once-on-this-island-9-25-2370574.html");///
			ticketListingCrawl.setSiteId(Site.VIVIDSEAT);
			ticketListingCrawl.setForceReCrawlFlag(true);
			System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
			System.out.println("===================================="+i+"=======================================");	
		
			i++;
		}
		
		System.out.println("Done :"+new Date());
	}
}