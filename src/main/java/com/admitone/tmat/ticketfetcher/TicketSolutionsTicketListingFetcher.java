package com.admitone.tmat.ticketfetcher;

import java.net.URI;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * TicketSolutions Ticket Listing Fetcher. 
 */
public class TicketSolutionsTicketListingFetcher extends TicketListingFetcher {
	
	//<span class="bodycopy"><b>Monday, April 05, 2010 - TBA<br>Lucas Oil Stadium (NCAA Basketball) - Indianapolis, IN<br></b></span>
	//                                                                        datetime    venue
	private static Pattern eventPattern = Pattern.compile("<span class=\"bodycopy\"><b>(.*?)<br>(.*?)<br>");
	
	private static Pattern noTicketPattern = Pattern.compile(">Notify When Available</a>");
	// Prudential Center (Britney Spears) - Newark, NJ
	private static Pattern venuePattern1 = Pattern.compile("^(.*)\\((.*)\\)\\s*-\\s*(.*),\\s*(.*)$", Pattern.MULTILINE);

	// Majestic Theatre San Antonio - San Antonio, TX
	private static Pattern venuePattern2 = Pattern.compile("^(.*)\\s*-\\s*(.*),\\s*(.*)$", Pattern.MULTILINE);


	// ticket pattern for rows with colored cell on the left
	// look for NCAA events
	private static Pattern tablePattern = Pattern.compile("<table[^>]*?id=\"LI_dgTickets\".*?>(.*?)<div id=\"LI_showCrumbs\">", Pattern.DOTALL);
	//                                                                                     SECTION                  ROW                          PRICE              QTY SELECTOR              TICKET ID
	private static Pattern ticketPattern1 = Pattern.compile("<tr.*?</table>.*?<td.*?<td.*?<span.*?>(.*?)</span>.*?<span.*?>(.*?)</span>.*?<td.*?<span.*?>(.*?)</span>.*?<td.*?>(.*?)</td>.*?<input.*?'(.*?)'", Pattern.DOTALL);

	// regular ticket rows
	private static Pattern ticketPattern2 = Pattern.compile("<tr.*?class=\"GridBody.*?>.*?<td.*?/td>.*?<td.*?<span.*?>(.*?)</span>.*?/td>.*?<td.*?<span.*?>(.*?)</span>.*?<td.*?<span.*?>(.*?)</span>.*?</td>.*?<td.*?>(.*?)</td>.*?<td.*?'(.*?)'", Pattern.DOTALL);
	private static Pattern optionPattern = Pattern.compile("<option.*?\"(.*?)\".*?</option>");
	
	public TicketSolutionsTicketListingFetcher() {
	}
		
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_SOLUTIONS))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketsolutions crawl");
		}	
		
		TicketSolutionsRedirectHandler redirectHandler = new TicketSolutionsRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		long startProcessingTime = System.currentTimeMillis();
		// http://www.ticketsolutions.com/tickets.aspx?eventid=766296
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "www.ticketsolutions.com");				
		httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		String content = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpGet);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
				ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
		if (redirectHandler.getRedirectUrl() != null && redirectHandler.getRedirectUrl().contains("404_special")) {
			return true;
		}
		
		if (content.contains("This event has expired")) {
			return true;
		}

		//	<span class="bodycopy">
		//		<b>
		//			Friday, March 13, 2009 - 8:00 PM
		//			<br/>
		//			Prudential Center (Britney Spears) - Newark, NJ
		//			<br/>
		//		</b>
		//	</span>
		
		// Friday, March 13, 2009 - 8:00 PM
		// Saturday, July 25, 2009 - 6:30 PM
		Date venueDate = null;
		
		// Saturday, July 25, 2009 - 6:30 PM
		DateFormat dateFormat1 = new SimpleDateFormat("EEEEEEEE, MMMMMMMMM dd, yyyy - hh:mm aa");

		// Sunday, February 07, 2010 - TBA		
		DateFormat dateFormat2 = new SimpleDateFormat("EEEEEEEE, MMMMMMMMM dd, yyyy");

		String venueName = null;
		String artistName = null;
		String venueCity = null;
		String venueState = null;

		Matcher eventMatcher = eventPattern.matcher(content);
		if (eventMatcher.find()) {
			String dateString = eventMatcher.group(1);
			String venueString = eventMatcher.group(2);

			try {
				venueDate = dateFormat1.parse(TextUtil.removeExtraWhitespaces(dateString).trim());
			} catch (Exception e) {};
			
			// Sunday, February 07, 2010 - TBA		
			if (venueDate == null) {
				venueDate = dateFormat2.parse(TextUtil.removeExtraWhitespaces(dateString.split("-")[0]).trim());
			}
	

			String venueContent = TextUtil.removeExtraWhitespaces(venueString).trim();
			Matcher venueMatcher = venuePattern1.matcher(venueContent);
			if (venueMatcher.find()) {
				venueName = venueMatcher.group(1).trim();
				artistName = venueMatcher.group(2).trim();
				venueCity = venueMatcher.group(3).trim();
				venueState = venueMatcher.group(4).trim();
			} else {
				venueMatcher = venuePattern2.matcher(venueContent);	
				venueMatcher.find();			
				venueName = venueMatcher.group(1).trim();
				venueCity = venueMatcher.group(2).trim();
				venueState = venueMatcher.group(3).trim();
			}

			// System.out.println("VENUE NAME = " + venueName);
			// System.out.println("VENUE CITY = " + venueCity);
			// System.out.println("VENUE STATE = " + venueState);
			// System.out.println("VENUE DATE = " + venueDate);

		}
		
		NumberFormat numberFormat = NumberFormat.getInstance();
				
		Matcher noTicketMatcher = noTicketPattern.matcher(content);
		if (noTicketMatcher.find()) {
			return true;
		}

		// there are two possible listing templates
		if (content.contains("LI_dgTickets")) {
			Matcher tableMatcher = tablePattern.matcher(content);
			if (!tableMatcher.find()) {
				return true;
			}				
			
			String tableContent = tableMatcher.group(1);
			
			Matcher ticketMatcher = ticketPattern1.matcher(tableContent);
			while(ticketMatcher.find()) {
				String section = ticketMatcher.group(1).trim();
				String row = ticketMatcher.group(2).trim();
				String priceString = ticketMatcher.group(3).trim();
				String quantitySelector = ticketMatcher.group(4).trim();
				String itemId = ticketMatcher.group(5).trim();

				if (!priceString.contains("$")) {
					continue;
				}
		        Number number = numberFormat.parse(priceString.split("[$]")[1]);
		        Double price = number.doubleValue();

				int minQuantity = Integer.MAX_VALUE;
				int maxQuantity = Integer.MIN_VALUE;
				Matcher optionMatcher = optionPattern.matcher(quantitySelector);
				while(optionMatcher.find()) {
					int quantity = Integer.parseInt(optionMatcher.group(1));
					minQuantity = Math.min(minQuantity, quantity);
					maxQuantity = Math.max(maxQuantity, quantity);					
				}

				TicketHit ticketHit = new TicketHit(Site.TICKET_SOLUTIONS, itemId,
						TicketType.REGULAR,						
						maxQuantity, minQuantity,
						row, section, price, price, "TicketSolutions", TicketStatus.ACTIVE);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);

				ticketHit.setEventDate(venueDate);
				ticketHit.setVenueState(venueState);
				ticketHit.setVenueCity(venueCity);
				ticketHit.setVenueName(venueName);
				
				// System.out.println("TICKET HIT=" + ticketHit);

				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);				
			}
		} else {			
			Matcher ticketMatcher = ticketPattern2.matcher(content);
			while(ticketMatcher.find()) {
				String section = ticketMatcher.group(1).trim();
				String row = ticketMatcher.group(2).trim();
				String priceString = ticketMatcher.group(3).trim();
				String quantitySelector = ticketMatcher.group(4).trim();
				String itemId = ticketMatcher.group(5).trim();

				if (!priceString.contains("$")) {
					continue;
				}
		        Number number = numberFormat.parse(priceString.split("[$]")[1]);
		        Double price = number.doubleValue();

				int minQuantity = Integer.MAX_VALUE;
				int maxQuantity = Integer.MIN_VALUE;
				Matcher optionMatcher = optionPattern.matcher(quantitySelector);
				while(optionMatcher.find()) {
					int quantity = Integer.parseInt(optionMatcher.group(1));
					minQuantity = Math.min(minQuantity, quantity);
					maxQuantity = Math.max(maxQuantity, quantity);
				}
				TicketHit ticketHit = new TicketHit(Site.TICKET_SOLUTIONS, itemId,
						TicketType.REGULAR,						
						maxQuantity, minQuantity,
						row, section, price, price, "TicketSolutions", TicketStatus.ACTIVE);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);

				ticketHit.setEventDate(venueDate);
				ticketHit.setVenueState(venueState);
				ticketHit.setVenueCity(venueCity);
				ticketHit.setVenueName(venueName);

				long startIndexationTime = System.currentTimeMillis();
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);							

			}
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());

		return true;
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.ticketsolutions.com/tickets.aspx?eventid=766296
		// http://go.ticketsolutions.com/tickets.aspx?eventid=818187
		return url.matches("^http://.*.ticketsolutions.com/tickets.aspx\\?eventid=[0-9]+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}
	
	public static class TicketSolutionsRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public TicketSolutionsRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {			
			return URI.create("http://go.ticketsolutions.com" +  redirectUrl);
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

		public String getRedirectUrl() {
			return redirectUrl;
		}		
	}
	
//	public static void main(String[] args) throws Exception {
//		String str = FileUtils.readFileToString(new File("c:\\workspace\\TMATdev\\petty.html"));
//		System.out.println("1" + eventPattern.matcher(str).find());
//		System.out.println("2" + venuePattern1.matcher(str).find());
//		System.out.println("3" + venuePattern2.matcher(str).find());
//		System.out.println("4" + tablePattern.matcher(str).find());
//		System.out.println("5" + eventPattern.matcher(str).find());
//		System.out.println("6" + ticketPattern1.matcher(str).find());
//		System.out.println("7" + ticketPattern2.matcher(str).find());
//		System.out.println("8" + optionPattern.matcher(str).find());
//	}
}