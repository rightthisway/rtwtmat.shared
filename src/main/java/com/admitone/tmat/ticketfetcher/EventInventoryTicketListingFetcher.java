package com.admitone.tmat.ticketfetcher;

import java.io.StringReader;
import java.net.URI;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.ccil.cowan.tagsoup.Parser;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * EventInventory Ticket Listing Fetcher. 
 */
public class EventInventoryTicketListingFetcher extends TicketListingFetcher{
	private Pattern eventIdPattern = Pattern.compile("[&?]e=([0-9]*)&");
	private Pattern ticketPattern = Pattern.compile("&ticket=([0-9]+)&");
	
	public EventInventoryTicketListingFetcher() {
	}
		
	private void fetchTicketListingTickets(TicketHitIndexer ticketHitIndexer, SimpleHttpClient httpClient, TicketListingCrawl ticketListingCrawl, String referer, String href) throws Exception {
		EventInventoryRedirectHandler redirectHandler = new EventInventoryRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		long startFetchingTime = System.currentTimeMillis();				
		HttpGet httpGet = new HttpGet(href);
		httpGet.addHeader("Host", "www.eventinventory.com");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Referer", referer);

		String content = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpGet);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
				ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}

		// http://www.eventinventory.com/search/pubsearch.cfm?cfid=101057332&cftoken=3386104-da576619-7a60-435c-93d1-257bfc4c9d36&cfuser=2B0EC2FC-34B9-423F-94101555EA1A365F&client=1337&e=41&id=10&cfid=101057332&cftoken=3386104-da576619-7a60-435c-93d1-257bfc4c9d36&cfuser=2B0EC2FC-34B9-423F-94101555EA1A365F&RefList=
		Matcher eventIdMatcher = eventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
		eventIdMatcher.find();
		String eventId = eventIdMatcher.group(1);
		
//		System.out.println("QUERY URL=" + ticketListingCrawl.getQueryUrl());

		if (redirectHandler.getRedirectUrl() != null) {
			// if e=eventId is different, the requested event does not exists anymore
			Matcher redirectEventIdMatcher = eventIdPattern.matcher(redirectHandler.getRedirectUrl());
			redirectEventIdMatcher.find();
//			System.out.println("REDIRECT=" + redirectHandler.getRedirectUrl());
			String redirectEventId = redirectEventIdMatcher.group(1);
			if (!redirectEventId.equals(eventId)) {
				return;
			}			
		}

		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);			
		parser.parse(new InputSource(new StringReader(content)));
		Node document = sax2dom.getDOM();
		
		Node eventInfoTrNode = XPathAPI.selectSingleNode(document, "//html:tr[@class='oddrow']");

		Date venueDate = null;
		
		DateFormat format = null;
		String venueBuilding = null;
		String venueCity = null;
		String venueState = null;
		
		
		// try this format: April 4, 2009 Saturday 7:00 PM
		try {
			String venueDateString = XPathAPI.selectSingleNode(eventInfoTrNode, "html:td[2]").getTextContent().trim().replaceAll("\\s+", " ");
			format = new SimpleDateFormat("MMMMMMMMMM dd, yyyy EEEEEEEE hh:mm aa");			
			venueDate = format.parse(venueDateString);
	
			if (venueDate == null) {
				// try with this format:
				// September 12, 2009 Saturday TBA
				format = new SimpleDateFormat("MMMMMMMMMM dd, yyyy EEEEEEEE");			
				venueDate = format.parse(venueDateString);
			}
	
			venueBuilding = XPathAPI.selectSingleNode(eventInfoTrNode, "html:td[3]/html:table/html:tr/html:td/html:strong").getTextContent().trim();
			
			String venueCityState = XPathAPI.selectSingleNode(eventInfoTrNode, "html:td[3]/html:table/html:tr/html:td/html:span[@class='fineprint']").getTextContent().trim();
			
			String tokens[] = venueCityState.split(",");
			
			venueCity = tokens[0].trim();
			venueState = tokens[1].trim();
		} catch (Exception e) {
		}
					
		// f_oddrow and f_evenrow are for featured tickets
		NodeList trNodeList = XPathAPI.selectNodeList(document, "//html:tr[@class='f_oddrow' or @class='f_evenrow' or @class='oddrow' or @class='evenrow']");
		
		for (int i = 1 ; i < trNodeList.getLength() ; i++) {
			Node trNode = trNodeList.item(i);
			NodeList tdNodeList = trNode.getChildNodes();
			// System.out.println("TDs=" + tdNodeList.getLength());
			if (tdNodeList.getLength() != 3) {
				continue;
			}

			String section = XPathAPI.selectSingleNode(tdNodeList.item(1), "html:span/html:strong[1]").getTextContent().trim();
			Node rowNode = XPathAPI.selectSingleNode(tdNodeList.item(1), "html:span/html:strong[2]");
			String row = null;
			if (rowNode != null) {
				row = rowNode.getTextContent().trim();
			}

			String action = XPathAPI.selectSingleNode(tdNodeList.item(2), "html:form/@action").getTextContent();
			Matcher ticketMatcher = ticketPattern.matcher(action);
			ticketMatcher.find();
			String itemId = ticketMatcher.group(1);
			
			NodeList quantityNodeList = XPathAPI.selectNodeList(tdNodeList.item(2), "html:form/html:select/html:option");
			int minQuantity = Integer.MAX_VALUE;
			int maxQuantity = Integer.MIN_VALUE;
			
			for (int j = 0 ; j < quantityNodeList.getLength() ; j++) {
				int quantity = Integer.parseInt(quantityNodeList.item(j).getTextContent().trim());
				minQuantity = Math.min(minQuantity, quantity);
				maxQuantity = Math.max(maxQuantity, quantity);				
			}

	        NumberFormat numberFormat = NumberFormat.getInstance();
	        Number number = numberFormat.parse(XPathAPI.selectSingleNode(tdNodeList.item(2), ".//html:td[@class='fieldInfo']/html:strong").getTextContent().substring(1));

			Double price = number.doubleValue();
			
			TicketHit ticketHit = new TicketHit(Site.EVENT_INVENTORY, itemId,
					TicketType.REGULAR,					
					maxQuantity, minQuantity,
					row, section, price, price, Site.EVENT_INVENTORY, TicketStatus.ACTIVE);
			
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setEventDate(venueDate);
			ticketHit.setVenueState(venueState);
			ticketHit.setVenueCity(venueCity);
			ticketHit.setVenueName(venueBuilding);
			
			
			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);			
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}			
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.EVENT_INVENTORY))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an eventinventory crawl");
		}
		
		long startProcessingTime = System.currentTimeMillis();
		long fetchingTime = 0L;

		Long startFetchingTime = System.currentTimeMillis();		
		HttpGet httpGet = new HttpGet("http://www.eventinventory.com/search/byevent.cfm?restart=yes&client=1337");
		httpGet.addHeader("Host", "www.eventinventory.com");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpGet);
			EntityUtils.consumeQuietly(response.getEntity());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(response != null)
				response.close();
		}
		
		ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
		
		// http://www.eventinventory.com/search/pubsearch.cfm?cfid=101057332&cftoken=3386104-da576619-7a60-435c-93d1-257bfc4c9d36&cfuser=2B0EC2FC-34B9-423F-94101555EA1A365F&client=1337&e=41&id=10&cfid=101057332&cftoken=3386104-da576619-7a60-435c-93d1-257bfc4c9d36&cfuser=2B0EC2FC-34B9-423F-94101555EA1A365F&RefList=
		Matcher eventIdMatcher = eventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
		eventIdMatcher.find();
		String eventId = eventIdMatcher.group(1);

		String cfid = null;
		String cftoken = null;
		String cfuser = null;
		
		/*List<Cookie> cookies = httpClient.getCookieStore().getCookies();
		for (Cookie cookie: cookies) {
			if (cookie.getName().equals("CFID")) {
				cfid = cookie.getValue();
			} else if (cookie.getName().equals("CFTOKEN")) {
				cftoken = cookie.getValue();
			} else if (cookie.getName().equals("CFUSER")) {
				cfuser = cookie.getValue();
			}			
		}*/
			
//		System.out.println("EVENT ID=" + eventId);
//		System.out.println("CFID=" + cfid);
//		System.out.println("CFTOKEN=" + cftoken);
//		System.out.println("CFUSER=" + cfuser);

		if (ticketListingCrawl.getQueryUrl().contains("results.cfm")) {
			// http://www.eventinventory.com/search/results.cfm?restart=yes&client=1337&e=4167&v=1222&s=1&month=11&day=14&year=2009&p=794667&cfid=125924502&cftoken=1b577d5-a6a38e98-2780-4bd6-9fb9-54bbb1e42517&cfuser=C0F508B7-4DBC-4C47-972ED33A42D06ACF&RefList=#search_top

			Pattern eidPattern = Pattern.compile("[&?]e=([0-9]*)&");
			Matcher eidMatcher = eidPattern.matcher(ticketListingCrawl.getQueryUrl());
			eidMatcher.find();
			String eid = eidMatcher.group(1);

			Pattern vidPattern = Pattern.compile("[&?]v=([0-9]*)&");
			Matcher vidMatcher = vidPattern.matcher(ticketListingCrawl.getQueryUrl());
			vidMatcher.find();
			String vid = vidMatcher.group(1);
				
			Pattern pidPattern = Pattern.compile("[&?]p=([0-9]*)&");
			Matcher pidMatcher = pidPattern.matcher(ticketListingCrawl.getQueryUrl());
			pidMatcher.find();
			String pid = pidMatcher.group(1);

			Pattern dayPattern = Pattern.compile("[&?]day=([0-9]*)&");
			Matcher dayMatcher = dayPattern.matcher(ticketListingCrawl.getQueryUrl());
			dayMatcher.find();
			String day = dayMatcher.group(1);

			Pattern monthPattern = Pattern.compile("[&?]month=([0-9]*)&");
			Matcher monthMatcher = monthPattern.matcher(ticketListingCrawl.getQueryUrl());
			monthMatcher.find();
			String month = monthMatcher.group(1);

			Pattern yearPattern = Pattern.compile("[&?]year=([0-9]*)&");
			Matcher yearMatcher = yearPattern.matcher(ticketListingCrawl.getQueryUrl());
			yearMatcher.find();
			String year = yearMatcher.group(1);

			String url = "http://www.eventinventory.com/search/results.cfm?restart=yes&client=1337&e=" + eid + "&v=" + vid + "&s=1&month=" + month + "&day=" + day + "&year=" + year + "&p=" + pid + "&cfid=" + cfid + "&cftoken=" + cftoken.replace("%2D", "-") + "&cfuser=" + cfuser.replace("%2D", "-") + "&RefList=";
			System.out.println("NEW URL=" + url);
			fetchTicketListingTickets(ticketHitIndexer, httpClient, ticketListingCrawl, "http://www.eventinventory.com/search/byevent.cfm?restart=yes&client=1337", url);
			ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
			return true;
		}
		
		startFetchingTime = System.currentTimeMillis();
		String url = "http://www.eventinventory.com/search/byvenue.cfm?cfid=" + cfid + "&cftoken=" + cftoken + "&cfuser=" + cfuser + "&RefList=";
		HttpPost httpPost = new HttpPost(url);
		httpPost.addHeader("Host", "www.eventinventory.com");
		httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
		httpPost.setEntity(new StringEntity("history=byevent.cfm&client=1337&venue=1&datespan=0&startdate=%7Bts+%272009-05-30+15%3A06%3A48%27%7D&e=" + eventId + "&next=byvenue.cfm"));
		
		String content = null;
		try{
			response = httpClient.execute(httpPost);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());		
				ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}

		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);			
		parser.parse(new InputSource(new StringReader(content)));
		Node document = sax2dom.getDOM();
		
		NodeList trNodeList = XPathAPI.selectNodeList(document, "//html:tr[@class='oddrow' or @class='evenrow']");
		
		for (int i = 0 ; i < trNodeList.getLength() ; i++) {
			Node trNode = trNodeList.item(i);			
			String href = "http://www.eventinventory.com" + XPathAPI.selectSingleNode(trNode, "html:td[4]/html:a/@href").getTextContent();
			
			fetchTicketListingTickets(ticketHitIndexer, httpClient, ticketListingCrawl, url, href);			
		}

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());		
		return true;
	}
	
	@Override
	public boolean isValidUrl(String url) {
		// valid url: 
		// http://www.eventinventory.com/search/pubsearch.cfm?cfid=101056146&cftoken=4fda0bf-d30b7501-700c-4052-a85f-44ba0692ab09&cfuser=5514FDF5-08B7-4B40-B909702D85FAED05&client=1337&e=52&id=10&cfid=101056146&cftoken=4fda0bf-d30b7501-700c-4052-a85f-44ba0692ab09&cfuser=5514FDF5-08B7-4B40-B909702D85FAED05&RefList=  
		// http://www.eventinventory.com/search/results.cfm?restart=yes&client=1337&e=594&v=386&s=1&month=2&day=10&year=2009&p=692415&cfid=101288044&cftoken=4a64c19-49694649-a304-486c-ab3a-3eaa3190835e&cfuser=67440E7B-DC11-427D-860CEA70F4B30B5A&RefList=#search_top 
		if (url.matches("^http://www.eventinventory.com/search/pubsearch.cfm.*[&?]e=.*$")) {
			return true;
		}
		return url.matches("^http://www.eventinventory.com/search/results.cfm.*[&?]e=.*$"); 
	}
	
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	

	
	public static class EventInventoryRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public EventInventoryRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {
			if (redirectUrl.startsWith("http")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create("http://www.eventinventory.com" + redirectUrl);
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

		public String getRedirectUrl() {
			return redirectUrl;
		}		
	}

}
