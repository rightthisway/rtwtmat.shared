package com.admitone.tmat.ticketfetcher;

import com.google.gson.annotations.SerializedName;

//import com.google.gson.annotations.SerializedName;

public class SeatGeekListings {

	
		
		@SerializedName("aq")
		private int deal_score;
		private int bi;
		private String bn;
		private float dq;
		private int b;
		@SerializedName("d")
		private String seller_notes;
		private int et;
		private float ev;
		private float f;
		private String id;
		private int dl;
		private int h;
		private int lmo;
		private String mk;
		@SerializedName("m")
		private String seller;
		private int pk;
		private int pu;
		@SerializedName("p")
		private int price;
		@SerializedName("pf")
		private Double selling_price;
		@SerializedName("q")
		private int quantity;
		@SerializedName("r")
		private String row;
		private String rr;
		@SerializedName("s")
		private String section;
		private String sr;
		private float sh;
		private boolean sco;
		private int[] sp;
		private int wc;
		private float w;
		
		
		
		public int getDeal_score() {
			return deal_score;
		}
		public void setDeal_score(int deal_score) {
			this.deal_score = deal_score;
		}
		public int getBi() {
			return bi;
		}
		public void setBi(int bi) {
			this.bi = bi;
		}
		public String getBn() {
			return bn;
		}
		public void setBn(String bn) {
			this.bn = bn;
		}
		public float getDq() {
			return dq;
		}
		public void setDq(float dq) {
			this.dq = dq;
		}
		public int getB() {
			return b;
		}
		public void setB(int b) {
			this.b = b;
		}
		public String getSeller_notes() {
			return seller_notes;
		}
		public void setSeller_notes(String seller_notes) {
			this.seller_notes = seller_notes;
		}
		public int getEt() {
			return et;
		}
		public void setEt(int et) {
			this.et = et;
		}
		public float getEv() {
			return ev;
		}
		public void setEv(float ev) {
			this.ev = ev;
		}
		public float getF() {
			return f;
		}
		public void setF(float f) {
			this.f = f;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public int getDl() {
			return dl;
		}
		public void setDl(int dl) {
			this.dl = dl;
		}
		public int getH() {
			return h;
		}
		public void setH(int h) {
			this.h = h;
		}
		public int getLmo() {
			return lmo;
		}
		public void setLmo(int lmo) {
			this.lmo = lmo;
		}
		public String getMk() {
			return mk;
		}
		public void setMk(String mk) {
			this.mk = mk;
		}
		public String getSeller() {
			return seller;
		}
		public void setSeller(String seller) {
			this.seller = seller;
		}
		public int getPk() {
			return pk;
		}
		public void setPk(int pk) {
			this.pk = pk;
		}
		public int getPu() {
			return pu;
		}
		public void setPu(int pu) {
			this.pu = pu;
		}
		public int getPrice() {
			return price;
		}
		public void setPrice(int price) {
			this.price = price;
		}
		public Double getSelling_price() {
			return selling_price;
		}
		public void setSelling_price(Double selling_price) {
			this.selling_price = selling_price;
		}
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
		public String getRow() {
			return row;
		}
		public void setRow(String row) {
			this.row = row;
		}
		public String getRr() {
			return rr;
		}
		public void setRr(String rr) {
			this.rr = rr;
		}
		public String getSection() {
			return section;
		}
		public void setSection(String section) {
			this.section = section;
		}
		public String getSr() {
			return sr;
		}
		public void setSr(String sr) {
			this.sr = sr;
		}
		public float getSh() {
			return sh;
		}
		public void setSh(float sh) {
			this.sh = sh;
		}
		public boolean isSco() {
			return sco;
		}
		public void setSco(boolean sco) {
			this.sco = sco;
		}
		public int[] getSp() {
			return sp;
		}
		public void setSp(int[] sp) {
			this.sp = sp;
		}
		public int getWc() {
			return wc;
		}
		public void setWc(int wc) {
			this.wc = wc;
		}
		public float getW() {
			return w;
		}
		public void setW(float w) {
			this.w = w;
		}
		
		public String toString()
		{
			StringBuilder sb=new StringBuilder();
			
			sb.append("\n\n************  Seats  **********\n\n");
			sb.append("Section "+getSection()+"\n");
			sb.append("Row "+getRow()+"\n");
			sb.append("Deal Score "+getDeal_score()+"\n");
			sb.append("Quantity "+getQuantity()+"\n");
			sb.append("Seller "+getSeller()+"\n");
			sb.append("Price "+getPrice()+"\n");
			sb.append("Selling Price "+getSelling_price()+"\n");
			sb.append("Seller Notes "+getSeller_notes()+"\n");
			
			return sb.toString();
			
		}
		
		
		
	

}
