package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.ChangeProxy;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Fetcher to extract data from TicketNow website.
 * The ticket information are found in the javascript code in the page and are defined in JSON format. 
 */
public class TicketsNowTicketListingFetcher extends TicketListingFetcher{	
	private final Logger logger = LoggerFactory.getLogger(TicketsNowTicketListingFetcher.class);
//	private static final Pattern seatPattern = Pattern.compile("(\\d+)");	

	public TicketsNowTicketListingFetcher() {
	}

	public JSONArray getJsonTicketListingsResponse(SimpleHttpClient httpClient, TicketListingCrawl ticketListingCrawl,Boolean reProcessFlag)  throws Exception {
		
		// to get the session in the cookie
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "www.ticketsnow.com");				
		//httpGet.addHeader("Accept", "application/json, text/javascript, */*");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		httpGet.addHeader("Accept-Language", "en-US,en;q=0.9");
		//httpGet.addHeader("TNOW3SessionCookie", "TNOW3SessionCookie=1460770220#648ff834-2ac3-4748-8012-0cca6418a91a");
		//httpGet.addHeader("Referer", "http://www.ticketsnow.com/resaleorder/texas-rangers-tickets/event/853?EID=853");
		
		JSONArray jsonArray = null;
		//org.apache.http.Header[] responseHeaders = null;
		try{
			String content = null;
			CloseableHttpResponse response = null;
			
			try{
				//System.out.println("TNOW Inside Before Execute : "+ticketListingCrawl+" : "+new Date());
				response = httpClient.execute(httpGet);
				//responseHeaders = response.getAllHeaders();
				
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity);
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				//System.out.println("TNOW Inside Exception 1 : "+ticketListingCrawl+" : "+new Date());
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
			//System.out.println(content);
			int start = content.indexOf("var ticketData = {\"SeatingData\":") + "var ticketData = {\"SeatingData\":".length();
			int end = content.indexOf("};", start);
			
			/*if (start <0 || end < 0) {
				return true;
			}*/
					
			String jsonContent = content.substring(start, end); 
			//System.out.println(jsonContent);		
			jsonArray = new JSONArray(jsonContent);
			return jsonArray;
			
		} catch(Exception e){
			//System.out.println("TNOW Inside Exception 2 : "+ticketListingCrawl+" : "+new Date()+" : "+ticketListingCrawl);
			if(reProcessFlag) {
				return getJsonTicketListingsResponse(httpClient, ticketListingCrawl,false);
			}
			//System.out.println("Error : "+ new Date());
			e.printStackTrace();
			//return null;
			throw e;
		}
	}
	
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKETS_NOW))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketsnow crawl");
		}
		Date startTime = new Date();
		//System.out.println("TNOW Inside : "+ticketListingCrawl+new Date());
		int countr=0,hits=0;
		long startProcessingTime = System.currentTimeMillis(); 
		int counter=1;
		
		//JSONArray jsonArray = getJsonTicketListingsResponse(httpClient, ticketListingCrawl, true);
		
		JSONArray jsonArray = null;
		int j = 0;
		int maxAttempt = 2;
		
		while(j<maxAttempt){
			j++;
			try{
				jsonArray = getJsonTicketListingsResponse(httpClient, ticketListingCrawl, false);
				if (jsonArray != null && jsonArray.length() > 0) {
					// success or other client/website error like 404...
					
					break;
				}else{
					//System.out.println("Vivd Exception Response : "+eventId+" : "+j);
					ChangeProxy.changeLuminatiProxy(httpClient,false);
					httpClient = new SimpleHttpClient(ticketListingCrawl.getSiteId());
					httpClient.assignHttpClient();
				}
			}catch (Exception e) {
				//System.out.println("Vivd Exception Response : "+eventId+" : "+j);
				ChangeProxy.changeLuminatiProxy(httpClient,false);
				try {
					httpClient = new SimpleHttpClient(ticketListingCrawl.getSiteId());
					httpClient.assignHttpClient();
				} catch (Exception e1) {
				}
				if(j == maxAttempt) {
					throw e;
				}
			}
		}
		
//		System.out.println(content.replaceAll("\\s+", " "));
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);
		String proxy="";
//		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
//			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		saveCounterHistory(ticketListingCrawl, counter,proxy);
			
		if(jsonArray == null) {
			long timeint = new Date().getTime()-startTime.getTime();
			//System.out.println("TNOW Inside if Null : "+ticketListingCrawl+new Date());
			//System.out.println("Zero : "+0+" : "+0+" : "+hits+" : "+timeint+" : "+ticketListingCrawl.getQueryUrl());
			return true;
		}
		
		Pattern deliveryTypePattern = null;
		Matcher deliveryTypeMatcher = null;
		int count = 0;
		
		for (int i = 0 ; i < jsonArray.length() ; i++) {
			if  (jsonArray.get(i).equals(JSONObject.NULL)) {
				break;
			}
			JSONObject item = (JSONObject)jsonArray.get(i);
			
			// {"N":"","P":140,"Q":2,"S":"308","R":"Z","F":false,"T":35645404,"ANE":false,"SQ":2,"PP":0,"B":0,"BN":"","RT":false,"H":false,"LP":130.0000,"AP":123.50,"TS":1,"BSN":"","BPN":"","CP":0,"IS":false,"TN":"","SI":"-","LN":1,"FP":0}
			// seems to use the same format than eimp
			
			//System.out.println(item.toString());	
			String itemId = item.getString("TicketId");
			//Boolean isPDF = Boolean.parseBoolean(item.getString("PDF"));
			//Boolean isID = Boolean.parseBoolean(item.getString("ID"));
			String row = item.getString("Row");
			String note = item.getString("Details");
			String section = item.getString("Section");
			Integer quantity = item.getInt("Quantity");
			Double currentPrice = item.getDouble("TicketPriceForDisplay");
			
			// seats:
			// 1 - 14
			// * - *
			String seats = item.getString("Seats");
			if(seats==null || seats.equalsIgnoreCase("null")){
				seats="";
			}
			String upsOrElectronic = item.getString("UPSOrElectronic");
			String deliveryTypeText = item.getString("Delivery");
			
			String deliveryType = null;
			deliveryTypePattern = Pattern.compile("<strong>(.*)<\\/strong>");
			deliveryTypeMatcher = deliveryTypePattern.matcher(deliveryTypeText);
        	while(deliveryTypeMatcher.find()){
        		deliveryType = deliveryTypeMatcher.group(1);
        	}
        	
			// System.out.println("SEATS=" + seats + ",start=" + startSeat + ",end=" + endSeat);
			
			
			// TS=1 // Active
			// TS=2 // Inactive
			// TS=3 // Pending 
			// TS=4 // Deleted 
			// TS=5 // Exchausted

			// if ticket is not active, skip it
			//int ticketStatus = item.getInt("TS");
			//if (ticketStatus != 1) {
				//continue;
			//}
			
			// lot size depends on quantity:
			// Odd Qty => 1
			// Even Qty => 2
			// Qty >= 14 => 1

			int lotSize = 1;
			if ((quantity % 2) == 0 && quantity < 14) {
				lotSize = 2;
			}
			TicketType ticketType = TicketType.REGULAR;
			if(note !=null){
				if(note.toUpperCase().contains("OBSTRUCT")) {
					ticketType = TicketType.OBSTRUCTEDVIEW;	
				} else if(note.toUpperCase().contains("LIMITED")) {
					ticketType = TicketType.LIMITEDVIEW;
				} else if(note.toUpperCase().contains("WHEELCHAIR")) {
					ticketType = TicketType.WHEELCHAIR;
				} else if(note.toUpperCase().contains("CHILD TICKET")){
					ticketType = TicketType.CHILDTICKET;
				} else if(note.toUpperCase().contains("PARTIAL VIEW")){
					ticketType = TicketType.PARTIALVIEW;
				}
			}
			TicketHit ticketHit = new TicketHit(Site.TICKETS_NOW, itemId, 
					ticketType,
					quantity, lotSize,
					row, section, currentPrice, currentPrice, "TicketsNow", TicketStatus.ACTIVE);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setSeat((seats == null)?null:seats);
			
			if(deliveryType != null) {
				if(deliveryType.contains("Instant")){
					ticketHit.setTicketDeliveryType(TicketDeliveryType.INSTANT);
					ticketHit.setInHand(true);
				} else if(deliveryType.contains("e-Ticket")){
					ticketHit.setTicketDeliveryType(TicketDeliveryType.ETICKETS);
				}
			} else {
				if(upsOrElectronic != null && upsOrElectronic.contains("Electronic")) {
					ticketHit.setTicketDeliveryType(TicketDeliveryType.EDELIVERY);
        		} /*else if(upsOrElectronic == null || !upsOrElectronic.contains("UPS")) {
        			System.out.println(deliveryType);
    				System.out.println(item.toString());
        		}*/
			}
			/*if(ticketHit.getTicketDeliveryType() == null) {
				System.out.println(deliveryType);
				System.out.println(item.toString());
			}
			System.out.println("Type : "+ticketHit.getTicketDeliveryType()+" : Delivary : "+ deliveryType+" : text : "+item.getString("DeliveryFilterText")+" : upsOrElectronic : "+upsOrElectronic+" : id : "+itemId);
			*/count++;
			
			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);							
		}
		//System.out.println("count : "+count+" : "+ticketListingCrawl.getQueryUrl());
		long timeint = new Date().getTime()-startTime.getTime();
		//System.out.println("eventID : "+0+" : "+count+" : "+hits+" : "+timeint+" : "+ticketListingCrawl.getQueryUrl());
		//System.out.println("TNOW Inside End : "+ticketListingCrawl+new Date()+" : "+timeint);

		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.ticketsnow.com/InventoryBrowse/Super-Bowl-Tickets-at-Raymond-James-Stadium-in-Tampa?PID=596701
		return url.matches("^http://www.ticketsnow.com/ResaleOrder/.*/tickets/.*$") ||  url.matches("^http://www.ticketexchangebyticketmaster.com/.*/InventoryBrowse.*\\?PID=.*$");
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.ticketsnow.com/resaleorder/Order?ticketId=" + ticket.getItemId()+ "&quantity=" + ticket.getLotSize();
	}
	
	public static void main(String[] args) throws Exception {
		
		int i=0;
		for (int j=0;j<1;j++) {
		List<String> urls = new ArrayList<String>();
		urls.add("https://www.ticketsnow.com/resaleorder/criss-angel-tickets-las-vegas-nv-11-16-2017/tickets/2102273");
		urls.add("https://www.ticketsnow.com/ResaleOrder/criss-angel-at-luxor-hotel-and-casino-in-las-vegas-11-16-2017-9-30-pm/tickets/2102330");
		urls.add("https://www.ticketsnow.com/resaleorder/chippendales-tickets-las-vegas-nv-12-14-2017/tickets/2049668");
		urls.add("https://www.ticketsnow.com/resaleorder/chippendales-tickets-las-vegas-nv-12-15-2017/tickets/2049667");
		urls.add("https://www.ticketsnow.com/resaleorder/chippendales-tickets-las-vegas-nv-12-15-2017/tickets/2049683");
		urls.add("https://www.ticketsnow.com/resaleorder/chippendales-tickets-las-vegas-nv-12-16-2017/tickets/2049666");
		urls.add("https://www.ticketsnow.com/resaleorder/chippendales-tickets-las-vegas-nv-12-16-2017/tickets/2049682");
		urls.add("https://www.ticketsnow.com/resaleorder/chippendales-tickets-las-vegas-nv-12-17-2017/tickets/2049665");
		
			
			http:https://www.ticketsnow.com/resaleorder/chippendales-tickets-las-vegas-nv-12-15-2017/tickets/2049683
			
			
			
		for (String url : urls) {
			i++;
			Date start = new Date();
			System.out.println("===================================="+i+"===========start============================"+start);	
			
		TicketsNowTicketListingFetcher feed = new TicketsNowTicketListingFetcher();
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.TICKETS_NOW);
		TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setId(123);
		ticketListingCrawl.setQueryUrl(url);///
		ticketListingCrawl.setSiteId(Site.TICKETS_NOW);
		try {
			
			System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("===================================="+i+"===============end========================"+(new Date().getTime()-start.getTime()));
		//System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
		}
		}
	}
}
