package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class TicketCityTicketListingFetcher extends TicketListingFetcher {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(TicketCityTicketListingFetcher.class);

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient,
			TicketHitIndexer ticketHitIndexer,
			TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_CITY))) {
			throw new Exception(
					this.getClass().getName()
							+ ".fetchTicketListing parameter must be a TicketCity crawl");
		}

		String proxy = "";
		if (httpClient.getHost() != null) {
			proxy = httpClient.getHost();

		} else {
			proxy = InetAddress.getLocalHost().getHostName();
		}

		long startProcessingTime = System.currentTimeMillis();

		String eventId = null;
		String url = ticketListingCrawl.getQueryUrl();
		int eventIdPos = url.indexOf("event_id=");
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
			eventId = eventId.replace(".html", "");
		}

		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement();
		long startFetchingTime = System.currentTimeMillis();
		int counter = 1;

		startFetchingTime = System.currentTimeMillis();
		String EventIdTicketUrl = "https://www.ticketcity.com/catalog/public/v1/events/"
				+ eventId + "/ticketBlocks";
		HttpGet httpGet = new HttpGet(EventIdTicketUrl);
		httpGet.setHeader(
				"User-Agent",
				"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpGet.addHeader("Host", "www.ticketcity.com");
		httpGet.addHeader(
				"Accept",
				"application/json, text/javascript, text/html, application/xhtml+xml, application/xml,*/*");
		httpGet.addHeader("Content-Type", "application/json, charset=utf-8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());
		
		CloseableHttpResponse response = null;
		int statusCode = 0;
		String content = null;
		try{
			response = httpClient.execute(httpGet);
			statusCode = response.getStatusLine().getStatusCode();
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime()
						+ System.currentTimeMillis() - startFetchingTime);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl
						.getFetchedByteCount() + (int) (entity.getContentLength()));
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}

		System.out.println("content" + content.toString());

		if (null == content || content.isEmpty()) {
			System.out.println("Empty Response from TicketCity:" + statusCode);
			try {
				throw new Exception("Empty Response from TicketCity Server");
			} catch (Exception e) {

				e.printStackTrace();
			}
		}

		JSONObject eventTicketInformation = new JSONObject(content);
		JSONArray ticketListingObject = eventTicketInformation
				.getJSONArray("B");
		int totalTicketCount=0;
		for (int i = 0; i < ticketListingObject.length(); i++) {
			JSONObject singleTicketObject = ticketListingObject
					.getJSONObject(i);
			totalTicketCount++;
			System.out.println("i"+i);
            System.out.println("Ticket Count-"+totalTicketCount);
			String section = "";
			String row = "";
			String quantity = "";
			String price = "";
			String ticketId = "";
			String notes = "";
			section = singleTicketObject.getString("S");
			System.out.println("section-" + section);
			row = singleTicketObject.getString("R");
			System.out.println("Row-" + row);
			JSONArray quantityarray = singleTicketObject.getJSONArray("Q");
			int j = 0;
			quantity = quantityarray.getString(j);
			int qty = Integer.parseInt(quantity);
			System.out.println("quantity-" + qty);
			price = singleTicketObject.getString("P");
			Double price1 = Double.parseDouble(price);
			System.out.println("Price-" + price1);
			ticketId = singleTicketObject.getString("I");
			System.out.println("TicketID-"+ticketId);
			notes = singleTicketObject.getString("N");
            System.out.println("Notes-"+notes);
			String seat = "";
			TicketType ticketType = TicketType.REGULAR;

			/*if (notes != null) {
				if (notes.toUpperCase().contains("OBSTRUCT")) {
					ticketType = TicketType.OBSTRUCTEDVIEW;
				} else if (notes.toUpperCase().contains("LIMITED")) {
					ticketType = TicketType.LIMITEDVIEW;
				} else if (notes.toUpperCase().contains("WHEELCHAIR")) {
					ticketType = TicketType.WHEELCHAIR;
				} else if (notes.toUpperCase().contains("CHILD TICKET")) {
					ticketType = TicketType.CHILDTICKET;
				} else if (notes.toUpperCase().contains("PARTIAL VIEW")) {
					ticketType = TicketType.PARTIALVIEW;
				}
			}*/
            System.out.println("TicketType-"+ticketType.name());
			TicketDeliveryType ticketDeliveryType = null;
			JSONObject ticketDeliverInfo = singleTicketObject
					.getJSONObject("F");
			String I = ticketDeliverInfo.getString("I");
			String m = ticketDeliverInfo.getString("M");
			String typeOfTicket = I + m;

			boolean inHand = false;
			if (typeOfTicket != null && "falsed".equalsIgnoreCase(typeOfTicket)) {
				ticketDeliveryType = TicketDeliveryType.ETICKETS;
				inHand = true;
			} else if (typeOfTicket != null
					&& "trued".equalsIgnoreCase(typeOfTicket)) {
				ticketDeliveryType = TicketDeliveryType.INSTANT;
				inHand = true;
			}
			System.out.println("TicketType-"+ticketDeliveryType);
				int lotSize = 1;
				if ((qty % 2) == 0 && qty < 14) {
					lotSize = 2;
				}

				Double buyItNowPrice = price1;
				TicketHit ticketHit = new TicketHit(Site.TICKET_CITY, ticketId,
						ticketType, qty, lotSize, row, section, price1,
						buyItNowPrice, "TicketCity", TicketStatus.ACTIVE);
				ticketHit.setSoldQuantity(0);
				ticketHit.setSeat(seat);
				ticketHit.setTicketListingCrawl(ticketListingCrawl);
				ticketHit.setTicketDeliveryType(ticketDeliveryType);
				indexTicketHit(ticketHitIndexer, ticketHit);
				ticketHit.setInHand(inHand);
				long startIndexationTime = System.currentTimeMillis();
				ticketListingCrawl.setIndexationTime(ticketListingCrawl
						.getIndexationTime()
						+ System.currentTimeMillis()
						- startIndexationTime);
				

			}
	     System.out.println("Total Ticket Count-"+totalTicketCount++);
		saveCounterHistory(ticketListingCrawl, counter, proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis()
				- startProcessingTime
				- ticketListingCrawl.getFetchingTime()
				- ticketListingCrawl.getIndexationTime());
		return true;
	}
	
	
	/*public static void main(String[] args) throws Exception {
	
			TicketCityTicketListingFetcher feed1 = new TicketCityTicketListingFetcher();
			SimpleHttpClient httpClient = HttpClientStore.createHttpClient("ticketcity","","");
	
		TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setId(123);
		ticketListingCrawl.setQueryUrl("https://www.ticketcity.com/canada-tickets/rogers-centre-(skydome)-tickets/beyonce-may-25-2016-1457987.html");
		ticketListingCrawl.setSiteId(Site.TICKET_CITY);
	
		System.out.println("result:--: " + feed1.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
	
		
	}*/
}