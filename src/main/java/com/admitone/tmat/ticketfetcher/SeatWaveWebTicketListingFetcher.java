package com.admitone.tmat.ticketfetcher;

import java.io.StringReader;
import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.apache.xalan.xsltc.trax.SAX2DOM;
import org.apache.xpath.XPathAPI;
import org.ccil.cowan.tagsoup.Parser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

/**
 * Seat Wave Ticket Listing Fetcher.
 * (Not in use now as we are using the seatwave API instead.) 
 */
@Deprecated
public class SeatWaveWebTicketListingFetcher extends TicketListingFetcher {	
	private final Logger logger = LoggerFactory.getLogger(SeatWaveWebTicketListingFetcher.class);

	// e.g. £116.94 per ticket, or $11.43 per ticket
	private Pattern pricePattern = Pattern.compile("\\D+([0-9.,]+)");

	// https://www.seatwave.com/buy/ticketpage.aspx?tsgID=1071306&TicLumpID=1&receipt=no&sm=yes
	private Pattern itemIDPattern = Pattern.compile("tsgID=([0-9]+)");
	
	// https://www.seatwave.com/buy/ticketpage.aspx?PrvPstID=1146207&receipt=no&sm=yes
	private Pattern itemIDPattern2 = Pattern.compile("PrvPstID=([0-9]+)");
	private Pattern quantitySelectNodeListPattern = Pattern.compile("<select.*?name=.*?cboQuantity\".*?>(.*?)</select>", Pattern.DOTALL);
	private Pattern quantityOptionNodeListPattern = Pattern.compile("<option value=\"(.*?)\".*?</option>");

	public SeatWaveWebTicketListingFetcher() {
	}
		
	/**
	 * Fetch the SeatWave ticket listing and extract the data.
	 */
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.SEATWAVE))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a seatwave crawl");
		}	

		Long startProcessingTime = System.currentTimeMillis();
		
		// to get the session in the cookie
		HttpGet httpGet = new HttpGet(ticketListingCrawl.getQueryUrl());
		httpGet.addHeader("Host", "www.seatwave.com");		
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		
		String content = null;
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpGet);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());
				ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime);
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
		Parser parser = new Parser();
		parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
		// to define the html: prefix (off by default)
		SAX2DOM sax2dom = new SAX2DOM();
		parser.setContentHandler(sax2dom);					
		parser.parse(new InputSource(new StringReader(content)));
				
		Node htmlDocument = sax2dom.getDOM();
		// attribute name should be written in lowercase
		NodeList trs = XPathAPI.selectNodeList(htmlDocument, "//html:tr[contains(@onmouseover, 'venueListActive')]");
		
		// System.out.println(" -- NUMBER OF ROWS=" + trs.getLength());
		
		for (int i = 0 ; i < trs.getLength() ; i++) {
			if (ticketListingCrawl.isCancelled()) {
				break;
			}
			
			NodeList tds = trs.item(i).getChildNodes();
			
			if (!tds.item(0).getNodeName().equals("td")) {
				continue;
			}
			
			// Block:
			// <strong>207</strong>
			// <div id="ctl00_cpLeftCol_g_g_ctl31_d2">Upper Tier</div>
			// => section=207, note=Upper Tier
			//
			// <div id="ctl00_cpLeftCol_g_g_ctl13_d2">Floor Standing</div>
			// => section=Floor Standing
			//
			// <div id="ctl00_cpLeftCol_g_g_ctl20_d2">Golden Circle B</div>
			// => section=Golden Circle B
			//
			//Block:
			// <strong>211</strong>
			// <div id="ctl00_cpLeftCol_g_g_ctl21_d2">Upper Tier</div>
			// => section=211, note=Upper Tier
			//
			// Block:
			// 	<strong>112</strong>
			// 	, Row: H
			// 	<div id="ctl00_cpLeftCol_g_g_ctl16_d2">Lower Tier</div>
			// => section=112, row=H, note=Lower Tier
			//						
			
			String strongText = null;
			String divText = null;
			String text1 = null;
			String text2 = null;
			
			for (int j = 0 ; j < tds.item(0).getChildNodes().getLength() ; j++) {
				Node node = tds.item(0).getChildNodes().item(j);
				// System.out.println("NODE NAME=" + node.getNodeName());
				if (node.getNodeName().equals("strong")) {
					strongText = node.getTextContent().trim();
				} else if (node.getNodeName().equals("div")) {
					divText = node.getTextContent().trim();
				} else if (node.getNodeName().equals("#text")) {
					if (text1 == null) {
						text1 = node.getTextContent().trim();
					} else if (text2 == null){
						text2 = node.getTextContent().trim();						
					}
				}
			}
			String section = null;
			String row = null;

			/*
			if (text1 != null && text1.equals("Block:")) {
				section = strongText;
			} else {
				section = divText;
			}*/
			if (divText == null) {
				section = strongText;				
			} else {
				section = divText;
			}
			
			if (text2 != null && text2.indexOf("Row:") >= 0) {
				row = text2.substring(text2.indexOf("Row:") + 4).trim();
			}

//			System.out.println("SECTION=" + section);
//			System.out.println("ROW=" + row);

			String rawPriceString = tds.item(2).getTextContent();
			
			Matcher priceMatcher = pricePattern.matcher(rawPriceString);
			priceMatcher.find();
			String priceString = priceMatcher.group(1);			
			
	        NumberFormat numberFormat = NumberFormat.getInstance();
	        
	        Number number = numberFormat.parse(priceString);
			Double price = number.doubleValue();
						
			String href = XPathAPI.selectSingleNode(tds.item(4), "html:a/@href").getTextContent();
			
			// find itemId from href
			// https://www.seatwave.com/buy/ticketpage.aspx?tsgID=1037929&TicLumpID=1&receipt=yes&sm=yes
			
			String itemId;
			Matcher itemIDMatcher = itemIDPattern.matcher(href);
			if (itemIDMatcher.find()) {
				itemId = itemIDMatcher.group(1);				
			} else {
				itemIDMatcher = itemIDPattern2.matcher(href);
				itemIDMatcher.find();
				itemId = "P" + itemIDMatcher.group(1);				
			}
			
			String rawQuantityString = tds.item(1).getTextContent();
			int minQuantity;
			int maxQuantity;
			if (rawQuantityString.contains("left")) {
				// 4 left
				Integer quantity = Integer.parseInt(rawQuantityString.trim().split(" ")[0]);
				maxQuantity = quantity;
				
				if ((quantity % 2) == 1) {
					minQuantity = 1;
				} else {
					minQuantity = 2;					
				}
			} else {
				//
				// go to the item purchase page to get the quantity and lot size
				//			
				
				long startFetchingTime = System.currentTimeMillis();
				httpGet = new HttpGet(href);
				httpGet.addHeader("Host", "www.seatwave.com");		
				httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
				httpGet.addHeader("Accept-Encoding", "gzip,deflate");
				httpGet.addHeader("Referer", ticketListingCrawl.getQueryUrl());
				
				try{
					response = httpClient.execute(httpGet);
					UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
					//ensure it is fully consumed
					if(entity != null){
						content = EntityUtils.toString(entity);
						ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + entity.getReadByteCount());						
						ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
						EntityUtils.consumeQuietly(entity);
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					//to reuse connection we need to close response stream associated with that
					if(response != null)
						response.close();
				}			
	
				Matcher quantitySelectNodeListMatcher = quantitySelectNodeListPattern.matcher(content);
				quantitySelectNodeListMatcher.find();
				String quantitySelect = quantitySelectNodeListMatcher.group(1); 
	
				Matcher quantityOptionNodeListMatcher = quantityOptionNodeListPattern.matcher(quantitySelect);
	
				minQuantity = Integer.MAX_VALUE;
				maxQuantity = Integer.MIN_VALUE;
	
				while(quantityOptionNodeListMatcher.find()) {
					int quantity = Integer.parseInt(quantityOptionNodeListMatcher.group(1));
					minQuantity = Math.min(quantity, minQuantity);
					maxQuantity = Math.max(quantity, maxQuantity);
				}
			}

			TicketHit ticketHit = new TicketHit(Site.SEATWAVE, itemId,
					TicketType.REGULAR,					
					maxQuantity, minQuantity,
					row, section, price, price, "SeatWave", TicketStatus.ACTIVE);
			
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			// System.out.println("TH=" + ticketHit);
			
			long startIndexationTime = System.currentTimeMillis();
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getIndexationTime() - ticketListingCrawl.getFetchingTime());
		return true;
	}
	
	/**
	 * Return true if it's a valid url.
	 */
	@Override
	public boolean isValidUrl(String url) {
		// valid url:
		// http://www.seatwave.com/beyoncetickets/metro-radio-arena-tickets/22-may-2009/perf/198403
		// http://www.seatwave.com/billy-elliot-tickets/victoria-palace-theatre-tickets/19-march-2009/perf/141225?sw_source=mytown
		// http://www.seatwave.com/rock-am-ring-tickets/season
		return url.matches("^http://www.seatwave.[a-z]*?/.*$");		
	}

	/**
	 * Return the ticket listing url from the SeatWave crawl.
	 */
	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}
	
	/**
	 * Return the ticket item url.
	 */
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		if (ticket.getItemId().startsWith("P")) {
			return "https://www.seatwave.com/buy/ticketpage.aspx?PrvPstID=" + ticket.getItemId().substring(1) + "&receipt=yes&sm=yes";			
		} else {
			return "https://www.seatwave.com/buy/ticketpage.aspx?tsgID=" + ticket.getItemId() + "&TicLumpID=1&receipt=yes&sm=yes";
		}
	}		
}
