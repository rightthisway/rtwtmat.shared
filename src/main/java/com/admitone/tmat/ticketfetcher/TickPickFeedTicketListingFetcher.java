package com.admitone.tmat.ticketfetcher;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Map;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * TickPickFeedTicketListingFetcher
 * @author dthiyagarajan
 *
 */
public class TickPickFeedTicketListingFetcher extends TicketListingFetcher{

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory.getLogger(TickPickFeedTicketListingFetcher.class);
	
	public TickPickFeedTicketListingFetcher(){
		
	}
	
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient,
			TicketHitIndexer ticketHitIndexer,
			TicketListingCrawl ticketListingCrawl) throws Exception {
		
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICK_PICK))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a flashseats crawl");
		}
		
		
		String proxy="";
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId;
		
		String url = ticketListingCrawl.getQueryUrl();
		int eventIdIndex=url.lastIndexOf('/');
	    eventId = url.substring(0,eventIdIndex);
	    eventId = eventId.substring(eventId.lastIndexOf("/")+1);	    
	   // System.out.println("[eventId] " + eventId);		
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement();
		
		long startFetchingTime = System.currentTimeMillis();
		int counter=1;
		startFetchingTime = System.currentTimeMillis();	
		String tickpickApiUrl = "https://api.tickpick.com/1.0/listings/internal/event/"+eventId;
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());

		String content = null;
		CloseableHttpResponse response = null;
		try{
			content  = getHttpResponse(tickpickApiUrl);
		
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}		
 		
 		if(content == null) return false;     
 			
 			
 			try {
 				TicketDeliveryType ticketDeliveryType = null;
 				TicketType ticketType = TicketType.REGULAR;

 				String ticketId, quantity, row, section= null;
 				String eticket, instant, notes = null;
 				String seat = "";
 				int tickPickCount = 0;
 				JSONParser parser = new JSONParser();
 				JSONArray jsonarray = (JSONArray) parser.parse(content);
 				int arryCnt = jsonarray.size();

 				for (int i = 0,j=0; i < arryCnt; i++) {
 					Map jsonobjectMap = (Map) jsonarray.get(i);
 					ticketId = (String) jsonobjectMap.get("id");
 					quantity = String.valueOf( jsonobjectMap.get("q"));
 					section = (String) jsonobjectMap.get("sid");
 					row = (String) jsonobjectMap.get("r"); 					
 					Double price = (Double) jsonobjectMap.get("p");
 					eticket = String.valueOf(jsonobjectMap.get("e"));
 					instant = String.valueOf(jsonobjectMap.get("i"));
 					
 					String dateinHandStr = (String) jsonobjectMap.get("ihd");
 					notes = (String) jsonobjectMap.get("n");

 					boolean inHand = false;
 					if (eticket != null && "true".equalsIgnoreCase(eticket)) {
 						ticketDeliveryType = TicketDeliveryType.ETICKETS;
 						inHand = true;
 					} else if (instant != null && "true".equalsIgnoreCase(instant)) {
 						ticketDeliveryType = TicketDeliveryType.INSTANT;
 						inHand = true;
 					}

 					int lotSize = 1;
 					int qty = Integer.parseInt(quantity);
 					if ((qty % 2) == 0 && qty < 14) {
 						lotSize = 2;
 					}

 					if (notes != null) {
 						if (notes.toUpperCase().contains("OBSTRUCT")) {
 							ticketType = TicketType.OBSTRUCTEDVIEW;
 						} else if (notes.toUpperCase().contains("LIMITED")) {
 							ticketType = TicketType.LIMITEDVIEW;
 						} else if (notes.toUpperCase().contains("WHEELCHAIR")) {
 							ticketType = TicketType.WHEELCHAIR;
 						} else if (notes.toUpperCase().contains("ADA")) {
 							ticketType = TicketType.WHEELCHAIR;
 						} else if (notes.toUpperCase().contains("CHILD TICKET")) {
 							ticketType = TicketType.CHILDTICKET;
 						} else if (notes.toUpperCase().contains("PARTIAL VIEW")) {
 							ticketType = TicketType.PARTIALVIEW;
 						} else if (notes.toUpperCase().contains("REAR")) {
 							ticketType = TicketType.PARTIALVIEW;
 						}
 					}	
 					
 					 					
 					if(section.toUpperCase().contains("PARKING")   || section.toUpperCase().contains("VALET") ) {
 						System.out.println( " Skipping Parking  ticketId [ " +  ticketId +   " ] section    = "  + section); 
 						continue;
 					}
 					
 					
 					 Double buyItNowPrice = price; 		 			  
 		 			 TicketHit ticketHit = new TicketHit(Site.TICK_PICK, ticketId, 
 		 					 	ticketType, qty, lotSize, row, section, price, buyItNowPrice,
 		 					 	"TickPick", TicketStatus.ACTIVE);
 		 			 ticketHit.setSoldQuantity(0);
 					 ticketHit.setSeat(seat);
 					 ticketHit.setTicketListingCrawl(ticketListingCrawl);
 					 ticketHit.setTicketDeliveryType(ticketDeliveryType);
 					 indexTicketHit(ticketHitIndexer, ticketHit);
 					 ticketHit.setInHand(inHand);
 					 long startIndexationTime = System.currentTimeMillis();
 					 ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
 					 tickPickCount++;
 					
 					/*System.out.println( " ticketId " +  ticketId +   " quantity "  + quantity +  " row " +  row
 							+ " section " + section  +   "price " +price + "ticketType " + ticketType   + "lotSize " + lotSize  + "inHand "  + inHand);*/

 				}

 			} catch (Exception ex) {
 				System.out.println(" [Exception  TickPick Ticket Crawling ]  " + ex);
 			} 			
 			saveCounterHistory(ticketListingCrawl, counter,proxy);
 			ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
 			
 			
 			
 			/*try {
 	        	DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
 	     	    InputSource is = new InputSource();
 	     	    is.setCharacterStream(new StringReader(content));

 	     	    Document doc = db.parse(is);
 	     	    NodeList nodes = doc.getElementsByTagName("Ask");

 	     	    int tickPickCount = 0;
 	     	    for (int i = 0; i < nodes.getLength(); i++) {
 	     	      Element element = (Element) nodes.item(i); 	     	      
 	     	      
 	     	      NodeList sectionNode = element.getElementsByTagName("sectionID");
 	     	      Element line = (Element) sectionNode.item(0);
 	     	      String section = getCharacterDataFromElement(line);
 	     	      //System.out.println("Section: " +section);
 	     	      
 	     	      NodeList inventoryIdNode = element.getElementsByTagName("inventoryID");
	     	      line = (Element) inventoryIdNode.item(0);
	     	      String ticketId = getCharacterDataFromElement(line);
	     	      //System.out.println("Row: " +ticketId);

 	     	      NodeList rowNode = element.getElementsByTagName("row");
 	     	      line = (Element) rowNode.item(0);
 	     	      String row = getCharacterDataFromElement(line);
	     	      //System.out.println("Row: " +row);
	     	      
	     	      NodeList quantityNode = element.getElementsByTagName("quantity");
	     	      line = (Element) quantityNode.item(0);
	     	      String quantity = getCharacterDataFromElement(line);
	     	      //System.out.println("Quantity: " +quantity);
	     	      
	     	      NodeList priceNode = element.getElementsByTagName("price");
	     	      line = (Element) priceNode.item(0);
	     	      String priceStr = getCharacterDataFromElement(line);
	     	      Double price = Double.parseDouble(priceStr);
	     	      //System.out.println("Price: " +price);
	     	      
	     	      NodeList eventDateNode = element.getElementsByTagName("ihDateString");
	     	      line = (Element) eventDateNode.item(0);
	     	      String eventDate = getCharacterDataFromElement(line);
	     	      //System.out.println("EventDate: " +eventDate);
	     	      
	     	      NodeList notesNode = element.getElementsByTagName("notes");
	     	      line = (Element) notesNode.item(0);
	     	      String notes = getCharacterDataFromElement(line);
	     	      //System.out.println("notes: " +notes);
	     	      
	     	     NodeList isObstructedNode = element.getElementsByTagName("isObstructed");
	     	      line = (Element) isObstructedNode.item(0);
	     	      String isObstructed = getCharacterDataFromElement(line);
	     	      //System.out.println("isObstructed: " +isObstructed);
	     	      
	     	     String seat= "";
	     	     TicketType ticketType = TicketType.REGULAR;
	     	     if(isObstructed !=null && "true".equalsIgnoreCase(isObstructed)){
	     	    	 ticketType = TicketType.OBSTRUCTEDVIEW;
	     	     }
	     	     
	 			 if(notes!=null){
	 				if(notes.toUpperCase().contains("OBSTRUCT")){
	 					ticketType = TicketType.OBSTRUCTEDVIEW;
	 				} else if(notes.toUpperCase().contains("LIMITED")){
	 					ticketType = TicketType.LIMITEDVIEW;
	 				} else if(notes.toUpperCase().contains("WHEELCHAIR")){
	 					ticketType = TicketType.WHEELCHAIR;
	 				} else if(notes.toUpperCase().contains("CHILD TICKET")){
	 					ticketType = TicketType.CHILDTICKET;
	 				} else if(notes.toUpperCase().contains("PARTIAL VIEW")){
	 					ticketType = TicketType.PARTIALVIEW;
	 				}
	 			}
	     	      
	     	      TicketDeliveryType ticketDeliveryType = null;
	     	      NodeList eTicketNode = element.getElementsByTagName("isEticket");
	     	      line = (Element) eTicketNode.item(0);
	     	      String eTicketDelivery = getCharacterDataFromElement(line);
	     	      //System.out.println("IsEticket: " +eTicketDelivery);
	     	      
	     	      NodeList instantTicNode = element.getElementsByTagName("isInstant");
	     	      line = (Element) instantTicNode.item(0);
	     	      String instantTicket = getCharacterDataFromElement(line);
	     	     // System.out.println("IsInstant ticket: " +instantTicket);
	     	      
	     	      boolean inHand= false;
	     	      if(eTicketDelivery != null && "true".equalsIgnoreCase(eTicketDelivery)){
	     	    	  ticketDeliveryType = TicketDeliveryType.ETICKETS;
	     	    	  inHand = true;
	     	      }else if(instantTicket != null && "true".equalsIgnoreCase(instantTicket)){
	     	    	  ticketDeliveryType = TicketDeliveryType.INSTANT;
	     	    	  inHand = true;
	     	      }
	     	      
	     	      int lotSize = 1;
	     	      int qty = Integer.parseInt(quantity);
	 			  if ((qty % 2) == 0 && qty < 14) {
	 				 lotSize = 2;
	 			  }
	 			  
	 			  Double buyItNowPrice = price;
	 			  
	 			 TicketHit ticketHit = new TicketHit(Site.TICK_PICK, ticketId, 
	 					 	ticketType, qty, lotSize, row, section, price, buyItNowPrice,
	 					 	"TickPick", TicketStatus.ACTIVE);
	 			 ticketHit.setSoldQuantity(0);
				 ticketHit.setSeat(seat);
				 ticketHit.setTicketListingCrawl(ticketListingCrawl);
				 ticketHit.setTicketDeliveryType(ticketDeliveryType);
				 indexTicketHit(ticketHitIndexer, ticketHit);
				 ticketHit.setInHand(inHand);
				 long startIndexationTime = System.currentTimeMillis();
				 ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
				 tickPickCount++;
 	     	    }
 	     	    System.out.println("Tick pick tix count :: "+tickPickCount);
 	        } catch (ParserConfigurationException e) {
 	        } catch (SAXException e) {
 	        } catch (IOException e) {
 	        }*/
 		
 		
		return true;
	}
	
	 /**
	  * Method to get the character from appropriate xml element
	  * @param e
	  * @return
	  */
	 public static String getCharacterDataFromElement(Element e) {
		    Node child = e.getFirstChild();
		    if (child instanceof org.w3c.dom.CharacterData) {
		      org.w3c.dom.CharacterData cd = (org.w3c.dom.CharacterData) child;
		      return cd.getData();
		    }
		    return "";
		  }
	 
	 
	 public static HttpClient wrapClient(HttpClient base) {
	        try {
	            SSLContext ctx = SSLContext.getInstance("TLS");
	            X509TrustManager tm = new X509TrustManager() {

	                public void checkClientTrusted(X509Certificate[] xcs,
	                                               String string) throws CertificateException {
	                }
	                

	                public void checkServerTrusted(X509Certificate[] xcs,
	                                               String string) throws CertificateException {
	                }

	                public X509Certificate[] getAcceptedIssuers() {
	                    return null;
	                }
	            };
	            ctx.init(null, new TrustManager[] { tm }, null);
	            
	            @SuppressWarnings("deprecation")
	            SSLConnectionSocketFactory ssf1 = new SSLConnectionSocketFactory(ctx);
				SSLSocketFactory ssf = new SSLSocketFactory(ctx);
	            ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
	            ClientConnectionManager ccm = base.getConnectionManager();
	            SchemeRegistry sr = ccm.getSchemeRegistry();
	            sr.register(new Scheme("https", ssf, 443));
	            return new DefaultHttpClient(ccm, base.getParams());
	        } catch (Exception ex) {
	            ex.printStackTrace();
	            return null;
	        }
	    }
	
	
	  public static String getHttpResponse(String url) throws Exception {
	        String retValue = "";
	        HttpClient httpclient = new DefaultHttpClient();
	        wrapClient(httpclient);
	        try {
	            HttpGet httpget = new HttpGet(url);	           
	            HttpResponse response = httpclient.execute(httpget);
	            System.out.println(response.getStatusLine());	           
	            HttpEntity entity = response.getEntity();
	            if (entity != null) {
	                InputStream instream = entity.getContent();
	                try {
	                    retValue = getStringFromInputStream(instream);

	                } catch (RuntimeException ex) {
	                    httpget.abort();
	                    throw ex;
	                } finally {	                   
	                    try {
	                        instream.close();
	                    } catch (Exception e) {
	                    }
	                }
	            }

	        } finally {
	            httpclient.getConnectionManager().shutdown();
	        }
	        return retValue;
	    }
	    private static String getStringFromInputStream(InputStream is) {

	        BufferedReader br = null;
	        StringBuilder sb = new StringBuilder();
	        String line;
	        try {
	            br = new BufferedReader(new InputStreamReader(is));
	            while ((line = br.readLine()) != null) {
	                sb.append(line);
	            }

	        } catch (IOException e) {
	            e.printStackTrace();
	        } finally {
	            if (br != null) {
	                try {
	                    br.close();
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	        return sb.toString();
	    }

	//Unit testing
public static void main(String[] args) throws Exception {
		//int i=0;
		//while(i<10){
		//	i++;
		TickPickFeedTicketListingFetcher feed = new TickPickFeedTicketListingFetcher();
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient("tickpick", "", "");
		TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setId(123);
		ticketListingCrawl.setQueryUrl("https://www.tickpick.com//buy-billy-joel-tickets-madison-square-garden-5-23-18-8PM/3362545/");///
		ticketListingCrawl.setSiteId(Site.TICK_PICK);
		System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
		//}
	}
	
}