package com.admitone.tmat.ticketfetcher;

import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.security.GeneralSecurityException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;


/**
 * Ticket Evolution Ticket Listing Fetcher. 
 */
public class TicketEvolutionTicketListingFetcher extends TicketListingFetcher {
	
	private  String token="c517a07c3875093fed0c789df1aa0460";
	private String secrete="5nSPDnk4chgthfVsQDNO9vUqL+JFG3rjfqk+G5ME";	

	public TicketEvolutionTicketListingFetcher() {
	}	
	
	/*@Override
	protected SimpleHttpClient createHttpClient(TicketListingCrawl crawl) throws Exception {
		SimpleHttpClient httpClient = new SimpleHttpClient(Site.TICKET_EVOLUTION);
		// Takes two hit one for index page and one for login page
		return httpClient;
	}*/

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {

		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_EVOLUTION))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be an ticketevolution crawl");
		}

		Long startProcessingTime = System.currentTimeMillis();

		httpClient.setRedirectHandler(null);

		int counter=1;  // According to new logic now we dont need to go to index page..
//		FIXME:  Redirect on their side... 
//		counter+=2;  
		//System.out.println(ticketListingCrawl.getQueryUrl().split("/events/")[1]);
		String ticketGroupURL="https://api.ticketevolution.com/v9/ticket_groups?event_id="+ticketListingCrawl.getQueryUrl().split("/events/")[1];
		try{
		//url for ticket group api.sandbox.ticketevolution.com/ticket_groups?event_id=114196
			String proxy="";
//			Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
			if(httpClient.getHost()!=null){
				proxy= httpClient.getHost();
//				proxy=proxy.split(":")[1].replaceAll("//","");
			}else{
				proxy=InetAddress.getLocalHost().getHostName();
			}
			saveCounterHistory(ticketListingCrawl, counter,proxy);
		String ticketGroupConent = 	hitTicketEvolution(httpClient,ticketGroupURL,ticketListingCrawl);
		//System.out.println(ticketGroupConent);
		JSONObject ticketGroupJson = null;
		if(null != ticketGroupConent && !ticketGroupConent.isEmpty() && ticketGroupConent.contains("{")){
			ticketGroupJson = new JSONObject(ticketGroupConent);
		}
			if(null != ticketGroupJson && !ticketGroupJson.isNull("ticket_groups")){
			JSONArray ticketGroupArray =(JSONArray)ticketGroupJson.get("ticket_groups");				
			if(ticketGroupArray!=null && ticketGroupArray.length()>0){
				
				for(int index=0;index<ticketGroupArray.length();index++){
					JSONObject ticketJson= (JSONObject)ticketGroupArray.get(index);					
					
					if(!ticketJson.isNull("quantity") && !"0".equals(ticketJson.getString("quantity"))){
						Integer quantity = Integer.parseInt(ticketJson.getString("quantity"));
						String section = ticketJson.getString("section");
						String inHandString = ticketJson.getString("in_hand");
						boolean inHand = false;
						if(inHandString!=null && !inHandString.isEmpty()){
							inHand = Boolean.parseBoolean(inHandString);
						}
						String row =  ticketJson.getString("row");
						NumberFormat numberFormat = NumberFormat.getInstance();
						int lotSize = 1;
						if ((quantity % 2) == 0 && quantity < 14) {
							lotSize = 2;
						}
						Double wholesalePrice = numberFormat.parse(ticketJson.getString("wholesale_price")).doubleValue();
						//Double retailPrice = numberFormat.parse(ticketJson.getString("retail_price")).doubleValue();
						//retail price should be wholesale price,for browse ticket page
						Double retailPrice = wholesalePrice;
						String brokerId = ticketJson.getJSONObject("office").getJSONObject("brokerage").getString("id");
						String brokerName = ticketJson.getJSONObject("office").getJSONObject("brokerage").getString("name");
						
						//System.out.println("Qty: " + quantity + " Section " + section + " Row " + row + " Seats " + seats + " Wholesale Price " + wholesalePrice + " Retail Price " + retail + " T Now Price" + tNowPrice + " In hand date " + inhanddate + " broker id: " + brokerId + " broker " + brokerName);

						String itemId = "TE" + ticketJson.getString("id");
						String seats = ticketJson.getString("seats");
						if(seats!=null){
							seats=seats.replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\"", "");
							if(!seats.equals("")){
								String[] temp=seats.split(",");
								int high=0;
								int low=0;
								for(String seat:temp){
									int tempSeat =Integer.parseInt(seat) ;
									if(low == 0 || low>tempSeat){
										low=tempSeat;
									}
									if(high==0 || high<tempSeat){
										high=tempSeat;
									}
									
								}
								if(high==low){
									seats=high+"";
								}else{
									seats=low+"-"+high;
								}
							}
						}else{
							seats="";
						}
						boolean isETicket=ticketJson.getBoolean("eticket");
						TicketType ticketType = TicketType.REGULAR;
						String view = ticketJson.getString("public_notes");
						if(view !=null){
							if(view.toUpperCase().contains("OBSTRUCT")) {
								ticketType = TicketType.OBSTRUCTEDVIEW;	
							} else if(view.toUpperCase().contains("LIMITED")) {
								ticketType = TicketType.LIMITEDVIEW;
							} else if(view.toUpperCase().contains("WHEELCHAIR")) {
								ticketType = TicketType.WHEELCHAIR;
							} else if(view.toUpperCase().contains("CHILD SEAT")){
								ticketType = TicketType.CHILDTICKET;
							} else if(view.toUpperCase().contains("PARTIAL VIEW")){
								ticketType = TicketType.PARTIALVIEW;
							}
							
						}
						TicketHit ticketHit = new TicketHit(Site.TICKET_EVOLUTION, itemId,
								ticketType,
								quantity,
								lotSize,
								row, section, wholesalePrice, retailPrice, brokerName, TicketStatus.ACTIVE);
						ticketHit.setInHand(inHand);
						ticketHit.setTicketListingCrawl(ticketListingCrawl);
						ticketHit.setSeat(seats);
						
						if(isETicket){
							ticketHit.setTicketDeliveryType(TicketDeliveryType.ETICKETS);
						}
						
						Long startIndexationTime = System.currentTimeMillis();
						indexTicketHit(ticketHitIndexer, ticketHit);
						ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
					}
					}
					
				} else {
					System.out.println("TICKETEVOLUTION_CRAWL_ERROR :Response 2 : "+ticketListingCrawl.getId()+" : "+ticketGroupJson);
				}
			} else {
				System.out.println("TICKETEVOLUTION_CRAWL_ERROR :Response 3 : "+ticketListingCrawl.getId()+" : "+ticketGroupJson);
			}
			
	     	
	}catch (Exception e) {
			e.printStackTrace();
	}
		
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		
		return true;
		// counter which says how many time we hit ticketevolution web site
		
		
	}
	//common method to hit url using httpget method and return response content
	public String hitTicketEvolution(SimpleHttpClient httpClient, String searchUrl,TicketListingCrawl ticketListingCrawl)
			throws GeneralSecurityException, UnsupportedEncodingException {
			String content = "";
			String toSignIn = coumputeSignInURL(searchUrl);
			String signature = computeSignature(toSignIn,secrete);
			HttpGet httpGet = new HttpGet(searchUrl);
			httpGet.addHeader("Host", "api.ticketevolution.com");
			httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
			httpGet.addHeader("Accept", "application/vnd.ticketevolution.api+json; version=8");				
			httpGet.addHeader("X-Signature", signature);
			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
			httpGet.addHeader("X-Token", token);
			httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
			httpGet.addHeader("Referer", "api.ticketevolution.com");
			try{	
				int j=0;
				HttpEntity entity = null;
				CloseableHttpResponse response = null;
				while(j<2){
					
					try{
						response = httpClient.execute(httpGet);
						if(null != response){
							entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
							if(entity != null){
								content= EntityUtils.toString(entity);
								EntityUtils.consumeQuietly(entity);
							}
								
							if(null != content && !content.isEmpty() && content.contains("{")){
								break;
							} else {
								System.out.println("TICKETEVOLUTION_CRAWL_ERROR :Response : "+ticketListingCrawl.getId()+" : "+content);
							}
							 
						}
					}catch (Exception e) {
						e.printStackTrace();
						//httpClient = new SimpleHttpClient(ticketListingCrawl.getSiteId());
						//httpClient.assignHttpClient();
						if(response != null)
							response.close();
						HttpClientStore.releaseHttpClient(httpClient);
						httpClient =  HttpClientStore.createHttpClient(ticketListingCrawl.getSiteId());
						System.out.println("TICKETEVOLUTION_CRAWL_ERROR : Site_ID :TE, Host :"+httpClient.getHost()+", Port :"+httpClient.getPort());
						System.out.println("TICKETEVOLUTION_CRAWL_ERROR :Error Occured While Excuting TicketevolutionCrawl , Error Is :"+e.fillInStackTrace());
						
					}
					j++;
				}
				
				
			}catch (Exception e) {
				e.printStackTrace();
				System.out.println("TICKETEVOLUTION_CRAWL_ERROR : Site_ID :TE, Host :"+httpClient.getHost()+", Port :"+httpClient.getPort());
				System.out.println("TICKETEVOLUTION_CRAWL_ERROR :Error Occured While Excuting TicketevolutionCrawl , Error Is :"+e.fillInStackTrace());
			}
			return content;
}
	//compute signature for each request 
	public  String computeSignature(String baseString, String keyString) throws GeneralSecurityException, UnsupportedEncodingException {

		SecretKey signingKey = new SecretKeySpec(keyString.getBytes(), "HMACSHA256");  
        Mac mac = Mac.getInstance("HMACSHA256");  
        mac.init(signingKey);  
        byte[] digest = mac.doFinal(baseString.getBytes("UTF-8"));     //output of sha256  
        
        //1  
        String encoded64 = new String(Base64.encodeBase64(digest));   // using it as an input  
        //System.out.println("direct base64 : "+encoded64);
        return encoded64;
	}
	
	public String coumputeSignInURL(String url){
		return url.replace("https://", "GET ");		
	}
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl,
			Ticket ticket) {
		// TODO Auto-generated method stub
		return super.getTicketItemUrl(ticketListingCrawl, ticket);
	}
		
	public static void main(String[] args) throws Exception {
		/*SimpleHttpClient client = new SimpleHttpClient(Site.TICKET_EVOLUTION);
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setSiteId(Site.TICKET_EVOLUTION);
			String ticketGroupConent = 	new TicketEvolutionTicketListingFetcher().hitTicketEvolution(client,"https://api.ticketevolution.com/v9/ticket_groups?event_id=948395",ticketListingCrawl);
			System.out.println(ticketGroupConent);
			JSONObject ticketGroupJson = null;
			if(null != ticketGroupConent && !ticketGroupConent.isEmpty() && ticketGroupConent.contains("{")){
				ticketGroupJson = new JSONObject(ticketGroupConent);
			}
				if(null != ticketGroupJson && !ticketGroupJson.isNull("ticket_groups")){
					
					JSONArray ticketGroupArray =(JSONArray)ticketGroupJson.get("ticket_groups");		
					//System.out.println("True:: "+ticketGroupArray);
				}*/
		
		List<String> eventIds = new ArrayList<String>();//LuminatiClientTest.getAllStubhubIds();
		eventIds.add("https://api.ticketevolution.com/v9/events/929187");
		/*eventIds.add("https://api.ticketevolution.com/v9/events/967444");
		eventIds.add("https://api.ticketevolution.com/v9/events/1014699");
		eventIds.add("https://api.ticketevolution.com/v9/events/910851");
		eventIds.add("https://api.ticketevolution.com/v9/events/908637");
		eventIds.add("https://api.ticketevolution.com/v9/events/909205");
		eventIds.add("https://api.ticketevolution.com/v9/events/941242");*/
		int i = 1;
		for (String url : eventIds) {
		System.out.println("==================st======="+i+"==================================================");	
		
		TicketEvolutionTicketListingFetcher feed = new TicketEvolutionTicketListingFetcher();
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.TICKET_EVOLUTION, "", "");
		
		TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setId(123);
		ticketListingCrawl.setQueryUrl(url);///
		ticketListingCrawl.setSiteId(Site.TICKET_EVOLUTION);
		ticketListingCrawl.setForceReCrawlFlag(true);
		System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
		System.out.println("===================end=============2===========================================");
		i++;
		}
	
	}
	

}