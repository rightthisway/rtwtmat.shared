package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.conn.params.ConnRoutePNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.ticketnetwork.TicketNetworkTicketGroup;
import com.admitone.tmat.utils.ticketnetwork.TicketNetworkWebServiceClient;

/**
 * WSDL: http://tnwebservicespublic-test.ticketsoftware.net/webservices/TNPubServicesXML.asmx?WSDL
 * http://tnwebservicespublic-test.ticketsoftware.net/webservices/TNPubServicesXML.asmx
 * method getEvents
 */

public class TicketNetworkDirectTicketListingFetcher extends TicketListingFetcher {
	
	private Logger logger = LoggerFactory.getLogger(TicketNetworkDirectTicketListingFetcher.class);
	
	// http://zeromarkup.com/ResultsTicket.aspx?evtid=1334135&event=Lady+Gaga
	private static final Pattern eventIdPattern = Pattern.compile("evtid=(\\d+)&");
	
	public TicketNetworkDirectTicketListingFetcher() {
	}
	
	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {		
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_NETWORK_DIRECT))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketnetworkdirect crawl");
		}
		try {
		long startProcessingTime = System.currentTimeMillis();		
		String queryEventId = ticketListingCrawl.getExtraParameter("queryEventId");
		if (queryEventId == null || queryEventId.isEmpty()) {
			// if not found, try to find it in the URL instead
			Matcher eventIdMatcher = eventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
			if (eventIdMatcher.find()) {
				queryEventId = eventIdMatcher.group(1);
			}
		}
		
		if (queryEventId == null || queryEventId.isEmpty()) {
			throw new Exception("Event id is not defined");
		}
		List<TicketNetworkTicketGroup> ticketGroups = null;
//		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement();
		int counter=0;
		for(int retry = 0 ; retry < 2 ; retry++) {
			try {
				
				counter++; 
				ticketGroups = TicketNetworkWebServiceClient.getTicketNetworkTicketGroups(httpClient, queryEventId);
				break;
			} catch (Exception e) {
				//System.out.println("ERROR WHEN DOING TND CALL FOR " + queryEventId + " AT " + new Date());
				if(ticketGroups != null && ticketGroups.size() > 0){
					//logger.error("ERROR WHEN DOING TND CALL FOR " + queryEventId + " AT " + new Date()+" size " +ticketGroups.size());
				}
				//logger.error("ERROR WHEN DOING TND CALL FOR " + queryEventId + " AT " + new Date()+"crawl Id ::" +ticketListingCrawl.getSiteId());
				e.printStackTrace();
			}		
			//Thread.sleep(2000L);				
		}
		String proxy="";
//		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
//			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		saveCounterHistory(ticketListingCrawl, counter,proxy);
		
		if (ticketGroups == null) {
			System.out.println("ticket groups null " + queryEventId + " AT " + new Date());
			logger.error("ticket groups null  " + queryEventId + " AT " + new Date()+"crawl Id ::" +ticketListingCrawl.getSiteId());
			return false;
		}
		
		for (TicketNetworkTicketGroup ticketGroup: ticketGroups) {
			if (ticketGroup.getTicketQuantity() < 0) {
				continue;
			}
			int minQuantity = ticketGroup.getTicketQuantity();
			for (int split: ticketGroup.getValidSplits()) {
				minQuantity = Math.min(minQuantity, split);
			}
	
//			String seller;
//			boolean isMine = ticketGroup.getIsMine();
//			
//			if (isMine){
//				seller = "ADMT1";
//			}else {
//				seller = "TicketNetworkDirect";
//			}
			TicketType ticketType = TicketType.REGULAR;
			if(ticketGroup.getNote()!=null) {
				if(ticketGroup.getNote().toUpperCase().contains("OBSTRUCT")){
					ticketType = TicketType.OBSTRUCTEDVIEW;
				} else if(ticketGroup.getNote().toUpperCase().contains("LIMITED")){
					ticketType = TicketType.LIMITEDVIEW;
				} else if(ticketGroup.getNote().toUpperCase().contains("ADA SEATING")){
					ticketType = TicketType.WHEELCHAIR;
				} else if(ticketGroup.getNote().toUpperCase().contains("CHILD SEAT")){
					ticketType = TicketType.CHILDTICKET;
				} else if(ticketGroup.getNote().toUpperCase().contains("PARTIAL VIEW")){
					ticketType = TicketType.PARTIALVIEW;
				}
			}
			TicketHit ticketHit = new TicketHit(Site.TICKET_NETWORK_DIRECT, ticketGroup.getId(),
					ticketType,
					ticketGroup.getTicketQuantity(), minQuantity,
					ticketGroup.getRow(), ticketGroup.getSection(),
					ticketGroup.getWholesalePrice(), ticketGroup.getWholesalePrice(), 
					"TicketNetworkDirect", TicketStatus.ACTIVE);			
			
			boolean isMercury = ticketGroup.getIsMercury();
			boolean isVisibleInPOS = ticketGroup.getIsVisibleInPOS();
			if(ticketGroup.getTicketDeliveryType()==null){
				if ((isMercury)&&(isVisibleInPOS)){
					ticketHit.setTicketDeliveryType(TicketDeliveryType.MERCURYPOS);
				}else if (isMercury){
					ticketHit.setTicketDeliveryType(TicketDeliveryType.MERCURY);
				}else {
					ticketHit.setTicketDeliveryType(null);
				}
			}else{
				ticketHit.setTicketDeliveryType(ticketGroup.getTicketDeliveryType());
			}
			ticketHit.setInHand(ticketGroup.isInHand());
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			if (ticketGroup.getLoSeat() != null && !ticketGroup.getLoSeat().equals(0)) {
				if (ticketGroup.getHiSeat()!= null && !ticketGroup.getHiSeat().equals(0)){
				ticketHit.setSeat(ticketGroup.getLoSeat().toString() + "-" + ticketGroup.getHiSeat().toString());
			}
			}
			else{
				ticketHit.setSeat(null);
			}
				
			Long startIndexationTime = System.currentTimeMillis();			
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
		}		
		
		// as we cannot get the fetching time, we assume that the extraction time is negligible compared to the extraction time.
		ticketListingCrawl.setFetchingTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getIndexationTime());
		int size =0;
		if(ticketGroups != null && ticketGroups.size() > 0){
			size = ticketGroups.size();
		}
			//System.out.println("end of return in tnd fetcher " + queryEventId + " AT " + new Date()+"..size.."+size);
			//logger.error("end of return in tnd fetcher  " + queryEventId + " AT " + new Date()+"crawl Id ::" +ticketListingCrawl.getSiteId()+"..size.."+size);
		//}
		return true;
		}catch(Exception e) {
			logger.error("Error : "+ticketListingCrawl+" : "+new Date());
			e.printStackTrace();
			throw e;
		}
	}	
	
	
	public StringBuffer runFetchTicketListingTest1(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer,
			TicketListingCrawl ticketListingCrawl) throws Exception {		
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_NETWORK_DIRECT))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a ticketnetworkdirect crawl");
		}
		
		long startProcessingTime = System.currentTimeMillis();		
		String queryEventId = ticketListingCrawl.getExtraParameter("queryEventId");
		if (queryEventId == null || queryEventId.isEmpty()) {
			// if not found, try to find it in the URL instead
			Matcher eventIdMatcher = eventIdPattern.matcher(ticketListingCrawl.getQueryUrl());
			if (eventIdMatcher.find()) {
				queryEventId = eventIdMatcher.group(1);
			}
		}
		
		if (queryEventId == null || queryEventId.isEmpty()) {
			throw new Exception("Event id is not defined");
		}
		List<TicketNetworkTicketGroup> ticketGroups = null;
//		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement();
		int counter=0;
		for(int retry = 0 ; retry < 5 ; retry++) {
			try {
				
				counter++; 
				ticketGroups = TicketNetworkWebServiceClient.getTicketNetworkTicketGroups(httpClient, queryEventId);
				break;
			} catch (Exception e) {
//				System.out.println("ERROR WHEN DOING TND CALL FOR " + queryEventId + " AT " + new Date());
			}		
			Thread.sleep(5000L);				
		}
		String proxy="";
		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(obj!=null){
			proxy= obj.toString();
			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		saveCounterHistory(ticketListingCrawl, counter,proxy);
		
		if (ticketGroups == null) {
			return null;
		}
		StringBuffer stringBuffer = new StringBuffer();
		try{
		
		for (TicketNetworkTicketGroup ticketGroup: ticketGroups) {
			if (ticketGroup.getTicketQuantity() < 0) {
				continue;
			}
			int minQuantity = ticketGroup.getTicketQuantity();
			for (int split: ticketGroup.getValidSplits()) {
				minQuantity = Math.min(minQuantity, split);
			}
	
			stringBuffer.append(ticketGroup.getId() + "," + ticketGroup.getTicketQuantity()
					   +"," + ticketGroup.getRow() + "," + ticketGroup.getSection()
					   + "," + ticketGroup.getWholesalePrice() + "," + ticketGroup.getRetailPrice() 
					   + "," + ticketGroup.getActualPrice()+","+ticketGroup.getIsMercury()
					   +","+ticketGroup.getIsVisibleInPOS()+","+ticketGroup.isInHand()+"\n");
						
			
		}		
		
		}catch(Exception e){
			e.printStackTrace();
		}
		return stringBuffer;
	}	

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return ticketListingCrawl.getQueryUrl();
	}	

	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("zeromarkup") >= 0) {
			// https://tickettransaction2.com/Checkout.aspx?e=YnJva2VyaWQ9OTgyJnNpdGVudW1iZXI9NSZ0Z2lkPTYxNjk3NTA5NiZldnRpZD05NjgzNDYmcHJpY2U9NTkuMDAwMA..!qNUjPcPUd2c.&treq=4&wcid=257&SessionId=TApyV&__utma=1.588180687.1236354578.1236359093.1236365208.3&__utmb=1&__utmc=1&__utmz=1.1236354578.1.1.utmccn=(direct)|utmcsr=(direct)|utmcmd=(none)&ip=10.230.78.114&hc=__utma%3D1.588180687.1236354578.1236359093.1236365208.3%3B%20__utmz%3D1.1236354578.1.1.utmccn%3D(direct)|utmcsr%3D(direct)|utmcmd%3D(none)%3B%20ASP.NET_SessionId%3Deyif5d55vpvqr32waal2jqvv%3B%20__utmb%3D1%3B%20__utmc%3D1&ha=Mozilla/5.0%20(X11%3B%20U%3B%20Linux%20i686%3B%20en-US%3B%20rv%3A1.9.0.6)%20Gecko/2009020911%20Ubuntu/8.10%20(intrepid)%20Firefox/3.0.6
			return "https://tickettransaction2.com/Checkout.aspx?e=" + ticket.getItemId() +"&treq=" + ticket.getLotSize() + "&wcid=257";							
		}						
		return null;
	}			
}
