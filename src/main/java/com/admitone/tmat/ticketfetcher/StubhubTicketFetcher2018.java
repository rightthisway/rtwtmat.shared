package com.admitone.tmat.ticketfetcher;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

	/**
 * StubHub Ticket Listing FetcherdoubleSectionPattern
 * Get tickets from web listing only (don't get end date and notes)
 * 
 */
public class StubhubTicketFetcher2018 {	
	
	private static final long serialVersionUID = 1L;

	private static final Pattern sectionPattern = Pattern.compile("^\\d+:(\\d+)$");

	private static final Pattern seatPattern = Pattern.compile("(\\d+)");
	
	private static Integer MAX_HIT_PER_MINUTE=50;
	
	public static DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public StubhubTicketFetcher2018() {
	}
	
	public void getJsonTicketListingsResponse(SimpleHttpClient httpClient, String eventId,int startParam)  throws Exception {
		
		String url = "https://www.stubhub.com/shape/search/inventory/v2/?eventId="+eventId+"&start="+startParam+"&rows=1500";
		//String url = "https://www.stubhub.de/shape/search/inventory/v2/listings/?eventId=9683560&sectionStats=true&zoneStats=true&start=0&allSectionZoneStats=true&eventLevelStats=true&quantitySummary=true&rows=20&sort=price+asc&priceType=nonBundledPrice&tld=3";
		HttpGet httpGet = new HttpGet(url);
		
		httpGet.addHeader(":authority", "www.stubhub.com");
		httpGet.addHeader(":path", "/shape/search/inventory/v2/listings/?eventId=103559874&sectionStats=true&zoneStats=true&start=0&allSectionZoneStats=true&eventLevelStats=true&quantitySummary=true&rows=20&sort=price+asc&edgeControlEnabled=true&urgencyMessaging=true&priceType=nonBundledPrice&shstore=1");
		httpGet.addHeader(":method", "GET");
		httpGet.addHeader(":scheme", "https");
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
		httpGet.addHeader("Accept", "application/json");
		httpGet.addHeader("Accept-Encoding", "gzip, deflate, br");
		httpGet.addHeader("Accept-Language", "en-US");
		httpGet.addHeader("com-stubhub-dye-path", "a34087189e194acf8a633a2112b40025,df15ee11b11b4fe084bdc50deea3af23");
		httpGet.addHeader("content-type", "application/json");
		httpGet.addHeader("cache-control", "max-age=0");
		httpGet.addHeader("referer", "https://www.stubhub.com/jimmy-eat-world-tickets-jimmy-eat-world-brooklyn-brooklyn-steel-6-14-2018/event/103353552/?sort=price+asc");
		httpGet.addHeader("x-distil-ajax", "yyqvbuqewxtcufrxveb");
		
		httpGet.addHeader("x-requested-with", "XMLHttpRequest");
		
		httpGet.addHeader("Cookie", "" + getCookies());
				/*"DC=lvs01; " +
				"fsr.s={\"f\":1479241326465} " +
				"D_SID=206.71.237.194:/+3v0gg/uLO1sDEXqBRunnAgblopbXhDvLchXiCIHmk; " +
				"D_PID=7E87B955-84EB-3578-A991-B8948732DC33; " +//cc
				//"D_IID=F5DCF7AE-DC0C-3069-B399-19577123EF1A; " +
				"D_UID=17C27F78-CB0F-3838-8C0F-28CE44BC249F; "+
				//"D_HID=RiNWsgCCFOPz99E1d6tjmCo05Pw3S5Js6lyC+3NbFWE; " +
				//"D_ZID=F6C8DF39-CEC9-3C40-8BCC-584479CCBD04; " +
				//"D_ZUID=DE5FCD9E-68BA-3631-8521-0C46A121A937; " +
				"TLTHID=32F68C22AB7110ABB069E7E73FDF5F5F; " +
				"TLTSID=313C4160AB7110AB3BE7A01DC99B9E1D; " +
				"SH_AT=1XNOHCPxzhOmYfQsadRFfX%2FOMRCZ0KKzhzQg%2BzAtaxxW5pfv1Rx11KOIF7aMprwAh7RNWYYcMwhTtmcZsl4mA9nGeX5MWdFfm%2BCJ5TSgwXg%3D; " +
				//"AMCV_1AEC46735278551A0A490D45%40AdobeOrg=1304406280%7CMCIDTS%7C17121%7CMCMID%7C30406199587961530432361014921859770325%7CMCAAMLH-1479840634%7C7%7CMCAAMB-1479840634%7CNRX38WO0n5BH8Th-nqAG_A%7CMCAID%7CNONE; " +
				"S_ACCT=stubhubdeprod; " +
				//"s_pers=%20s_dfa%3Dstubhubdeprod%7C1479237633814%3B; " +
				"mbox=session#1479241328991-880555#1479243189; " +
				"session:loginStatus=0");*/
		
		JSONObject jsonObject = null;
		try{
			CloseableHttpResponse response = null;
			String content = null;
			try{
				response = httpClient.execute(httpGet,false);
				System.out.println("response----->"+response);
				UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					content = EntityUtils.toString(entity);
					System.out.println("content----->"+content);
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
		}catch(Exception e){
			e.printStackTrace();
			return;
		}
		 
	}
	
	
	public static void main(String[] args) throws Exception {
		StubhubTicketFetcher2018 feed = new StubhubTicketFetcher2018();
		
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.TICKET_NETWORK_DIRECT);
		 
		feed.getJsonTicketListingsResponse(httpClient,"103559874", 0);
		
		System.out.println("Done :"+new Date());
	}
	
	public String getCookies(){
		String cookies = "_4c_=fVJdb%2BI4FP0rK0vMy9TgrzgxUlUBLdpKM61UivYR2Y4DWULC2IYsqvrfe5MynWranTw419fnfp7zhNqNq9GYJixTkjEleaou0NadAho%2FIbvvzmN3HHyFxmgT4z6MR6O2bYchHszmYIa22Y3%2BLXe7E3Y64rbxVY5jabcuBvy73%2Fim2Van%2BpcRonMVlpgKzAjNRu7o6jiihPOEJwkbXYXGx8u9L637qoNFF%2Bjx28KFUDb17TV0RCYzPp0wIecTQYmcUzFT1%2FM0vblJpmIuMwiIcXXUvtR1BPzyjlCyWhzWaxfi0q9dbU%2BAWdZlUbp8tYCh%2Fj4YAN51kc7vYBEITGi4Dc7DZbbxzc79JVPwNt3rP2WdwyNcvSuc9z3q%2FzaVlyGW1cqvrN5Hu9HDTdxVV979OEA%2Ft%2FklSU1iFUuw4UxiYYzCGbEE68wQo6giBdVfuuQP51qXAzbflvUabxs4atfiU%2BO3bwx88vRmwEw6b%2FUJxw1Q5B2uT5gSTFVPBeTtyYA%2F0MGUooKCPeDzjpIBv%2B5JGbDpKy2hjA7mfjcrOG2Td06qhnzI8XHIwFeAqFAuOC%2BsTiQRqUilyTPOXJpr4N1wnueAu%2F%2F%2B%2BLCa3kxm93fv96nD%2B31eAXAPAkUCjKqxuurqLRfo%2BQL91%2BtadRUUTyTrlAAizqQg3QcIX%2BZngaPEOZOZNHOOSJYUhbGJdpkxBZeW2F5HfT4uZapSIRhnkKAr3ceLD%2BXIx3KvGvpDzCct7uHao%2BlPdKZSShhNMn5GU3FGPz%2B%2FAA%3D%3D; TLTHID=DA0ADC0C6FA6106F1472BDF1E7A74272; TLTSID=DA0ADC0C6FA6106F1472BDF1E7A74272; SH_AT=1XNOHCPxzhOmYfQsadRFfX%2FOMRCZ0KKzhzQg%2BzAtaxxW5pfv1Rx11KOIF7aMprwAVpWHHbBm4Otsj3UUV7LNHxgspNyxKc8lKVimmuHXtyQ%3D; DC=lvs01; AKA_A2=A; ak_bmsc=AD1FD94A9895E176E181732364C169CAB81A2CE4293E0000001D225BBE5BA74F~plKxgzEqH1g8GIbPfY0MEv3uhJG9+Ez83WX4QI+xAAvRdAsLE/43lPrAvyrh6oGxb3zXYqhoxHnb/a470LtlULRD8Q9JJe1zFSC/iYIVganH1NJvOFBtcMdQxjQ/xB3Zb0gZY0mleWMQCAwN/GuUt219Z0EGge0rupfiDC36h7t353LktuSVbDatp5lfEdScKnrwkNfbI2BQOseBHhLYauzgxyxgQsXYK+0rL/wYWRpvM=; bm_sz=00674BFF58981BACC3A5E921429A706D~QAAQ5CwauGzp8vJjAQAA1UtB/Z1N0oHZGbOi/6nVuvR9X/H4qx5JdLTzfzpCq/+69Kvg6MWyBIWRXJndIAsEFeXiHBVc/UJqs806iMMrlpVlWQ5baNyAImdYzkI1OQww1hBvMojfVw7FVblPixS0NiBskPwLbgvkiluLTbkJlmTVgL6LN+Eo8pcw8+pricMa; optimizelyEndUserId=oeu1528962297163r0.08603562652192953; S_ACCT=stubhub,stubhubglobal; check=true; s_pers=%20s_dfa%3Dstubhub%252Cstubhubglobal%7C1528964097239%3B; AMCVS_1AEC46735278551A0A490D45%40AdobeOrg=1; AMCV_1AEC46735278551A0A490D45%40AdobeOrg=1099438348%7CMCIDTS%7C17697%7CMCMID%7C27244720841430099543993085351480061322%7CMCAAMLH-1529567097%7C7%7CMCAAMB-1529567097%7C6G1ynYcLPuiQxYZrsz_pkqfLG9yMXBpb2zX5dvJdYQJzPXImdj0y%7CMCOPTOUT-1528969497s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C2.1.0; _abck=EA22A02115D62885B0E255C8DBCDA7D5B81A2CE4293E0000001D225BEB55FC25~0~uFDQRcriZ1KGHVbiaLVTFxEaa5gN7dwUFnfwhanUf24=~-1~-1; D_IID=30AB2FE0-0EFA-36AA-B9A1-EF3B3F6F4379; D_UID=EF24AE55-DE7E-3D76-8813-9DB3493CB3EC; D_ZID=F9E169A4-7C53-3590-ACDB-6557E7C80207; D_ZUID=6F08E041-5DEB-3B6A-8DD0-E40AE66B22B1; D_HID=AD6347A5-287C-3C91-BCFA-958EBA9009D5; D_SID=206.71.237.194:/+3v0gg/uLO1sDEXqBRunnAgblopbXhDvLchXiCIHmk; dtfsb_jc=77d5472c8a05f1d3761993599dfa4aad; mbox=session#418b6ae5380e4b4696ed3bfb3aa65f32#1528964159|PC#418b6ae5380e4b4696ed3bfb3aa65f32.17_57#1592207098; bm_sv=F42A0B9519B9C1B3FD6A537694A59937~lh6idSruI9ughOCIV27E/sFEwP/Z3k+E6FIzPK/bKWS175sHfu6Yv3qZsGdECRg+a/nHXS8sOPwnzG6dRjNTP2ZBv0ZlS7LlPQfDYMEmZz9E6W4yayfqnrU6j5TG5FWBjeRgdw5Sgw/F1LZb9lCX4OTy95qtZh+EDXOWNR6GZog=" +
				"";
		return cookies;
	}
}