package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.net.URI;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.BulkTicketHitIndexer;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.httpclient.UncompressedHttpEntity;

	/**
 * StubHub Ticket Listing FetcherdoubleSectionPattern
 * Get tickets from web listing only (don't get end date and notes)
 * 
 */
public class StubHubWebFeedTicketListingFetcherOld extends TicketListingFetcher {	
		
//	private final Logger logger = LoggerFactory.getLogger(StubHubWebTicketListingFetcher.class);
	
	public StubHubWebFeedTicketListingFetcherOld() {
	}

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient, TicketHitIndexer ticketHitIndexer, TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.STUB_HUB))) {
			throw new Exception(this.getClass().getName() + ".fetchTicketListing parameter must be a stubhub crawl");
		}		
		
		
		String proxy="";
//		Object obj=httpClient.getParams().getParameter(ConnRoutePNames.DEFAULT_PROXY);
		if(httpClient.getHost()!=null){
			proxy= httpClient.getHost();
//			proxy=proxy.split(":")[1].replaceAll("//","");
		}else{
			proxy=InetAddress.getLocalHost().getHostName();
		}
		
		
		StubHubRedirectHandler redirectHandler = new StubHubRedirectHandler();
		httpClient.setRedirectHandler(redirectHandler);
		
		long startProcessingTime = System.currentTimeMillis();
		
		String eventId;
		
		String url = ticketListingCrawl.getQueryUrl();
		int eventIdPos = url.indexOf("event_id="); 
		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 9, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
		}

		// 
		// CHECK IF THE EVENT IS ACTIVE OR NOT
		//
		
		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement(); 
		long startFetchingTime = System.currentTimeMillis();
		int counter=1;

		startFetchingTime = System.currentTimeMillis();
		
		HttpGet httpGet = new HttpGet("http://www.stubhub.com/ticketAPI/restSvc/event/" + eventId);
		httpGet.addHeader("Host", "www.stubhub.com");		
		httpGet.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:35.0) Gecko/20100101 Firefox/35.0");
		httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpGet.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
//		httpGet.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
//		httpGet.addHeader("Accept-Length", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());

		CloseableHttpResponse response = null;
		int statusCode = 0;
		String content = null;
		try{
			response = httpClient.execute(httpGet);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			
			statusCode = response.getStatusLine().getStatusCode();

	        if (statusCode < 200 || statusCode >= 300) {
		        System.out.println("Bad Respons from SH:" + statusCode);
		        throw new Exception("Bad Response from Stubhub Server");	
	        }
	        
			//ensure it is fully consumed
			if(entity != null){
		        ticketListingCrawl.setFetchingTime(ticketListingCrawl.getFetchingTime() + System.currentTimeMillis() - startFetchingTime);
		 		ticketListingCrawl.setFetchedByteCount(ticketListingCrawl.getFetchedByteCount() + (int)(entity.getContentLength()));
				content = EntityUtils.toString(entity);	
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}

//		System.out.println("Content length:" + content);
		if (null == content || content.isEmpty()) {
			System.out.println("Empty Respons from SH:" + statusCode);
			throw new Exception("Empty Response from Stubhub Server");	
	    }
		
 		JSONObject eventResult = new JSONObject(content);
 		JSONArray ticketArray = eventResult.getJSONObject("eventTicketListing").getJSONArray("eventTicket");
 		Pattern piggbackPattern = Pattern.compile("[\\\\|,|/|&]");
 		Matcher piggbackMatcher ;
 		for(int i=0;i<ticketArray.length();i++){
 			JSONObject ticketJSON = ticketArray.getJSONObject(i);
// 			String valid = ticketJSON.getString("vi");
 			/*if(valid==null || valid.equals("null")){
 				continue;
 			}*/
 			
 			
 			
 			String obstructView = ticketJSON.getString("ov");
 			String studentTicket = ticketJSON.getString("sf");
 			String childTicket = ticketJSON.getString("sc");
 			String wheelChairAccessible = ticketJSON.getString("wc");
 			String section = ticketJSON.getString("va");
 			String row = ticketJSON.getString("rd");
 			piggbackMatcher = piggbackPattern.matcher(row);
			if(piggbackMatcher.find()){
//					System.out.println(rowActuallyMatcher.group());
				continue;
			}
			JSONObject tcJson = ticketJSON.getJSONObject("tc");
			Double price = tcJson.getDouble("amount");
			Double buyItNowPrice = price;
			Integer quantity = ticketJSON.getInt("qt");
//			Integer quantity_remain = null;
			String seat= ticketJSON.getString("se");
			if(seat!=null && !seat.isEmpty()){
				String temp[]=seat.split(",");
				if(temp.length>1){
					seat=temp[0]+"-" + temp[temp.length-1];
				}else{
					seat="";
				}
			}
			String ticketId = ticketJSON.getString("id");
			TicketType ticketType = TicketType.REGULAR;
			if(studentTicket!=null && "true".equals(studentTicket)){
				ticketType = TicketType.STUDENTTICKET;
			}else if(obstructView!=null && "true".equals(obstructView)){
				ticketType = TicketType.OBSTRUCTEDVIEW;
			} else if(childTicket !=null && "true".equals(childTicket)){
				ticketType = TicketType.CHILDTICKET;
			} else if(wheelChairAccessible != null && "true".equals(wheelChairAccessible)) {
				ticketType = TicketType.WHEELCHAIR;
			}
			TicketDeliveryType ticketDeliveryType = null;
			String instantDownload = ticketJSON.getString("de");
			String electronics = ticketJSON.getString("et");
			boolean inHand= false;
			if(instantDownload!=null && "true".equalsIgnoreCase(instantDownload)){
					ticketDeliveryType = TicketDeliveryType.INSTANT;
					inHand= true;
			}else if(electronics!=null && "true".equalsIgnoreCase(electronics) ){
				ticketDeliveryType = TicketDeliveryType.EDELIVERY;
				inHand= true;  // As per ZoneII-144
			}
			int lotSize = 1;
			if ((quantity % 2) == 0 && quantity < 14) {
				lotSize = 2;
			}
			TicketHit ticketHit = new TicketHit(Site.STUB_HUB, ticketId,
					ticketType,
					quantity,lotSize,
					row, section, price, buyItNowPrice, "StubHub", TicketStatus.ACTIVE);
			ticketHit.setSoldQuantity(0);
			ticketHit.setSeat(seat);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setTicketDeliveryType(ticketDeliveryType);
			ticketHit.setInHand(inHand);
			indexTicketHit(ticketHitIndexer, ticketHit);
			long startIndexationTime = System.currentTimeMillis();
			ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
 		}
 		

		saveCounterHistory(ticketListingCrawl, counter,proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis() - startProcessingTime - ticketListingCrawl.getFetchingTime() - ticketListingCrawl.getIndexationTime());
		return true;
	}
	

	@Override
	public boolean isValidUrl(String url) {
		// valid url: http://www.stubhub.com/mlb-all-star-game-tickets/mlb-all-star-strip-7-11-2009-750829/
		// http://www.stubhub.com/jimmy-buffett-tickets/jimmy-buffett-honolulu-waikiki-shell-2-28-2009-790072/
		// http://www.stubhub.com/super-bowl-tickets/super-bowl-2-1-2009-565762/
		// http://www.stubhub.com?q=eventId=62789
		return url.matches("^http://www.stubhub.com/.*[0-9]*/$") || url.matches("^http://www.stubhub.com/\\?event_id=\\d+$");		
	}

	@Override
	public String getTicketListingUrl(TicketListingCrawl ticketListingCrawl) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
		//return ticketListingCrawl.getQueryUrl();
	}
	
	@Override
	public String getTicketItemUrl(TicketListingCrawl ticketListingCrawl, Ticket ticket) {
		return "http://www.stubhub.com/largesellers-ticketcenter/";
//			if (ticket.getTicketListingCrawl().getQueryUrl().indexOf("?") >= 0) {
//				return ticket.getTicketListingCrawl().getQueryUrl() + "&ticket_id=" + ticket.getItemId();
//			}
//
//			return ticket.getTicketListingCrawl().getQueryUrl() + "?ticket_id=" + ticket.getItemId();
	}
	
	/**
	 * Redirect Handler Class 
	 */
	private class StubHubRedirectHandler implements RedirectHandler{
		private String redirectUrl;
		
		public StubHubRedirectHandler() {
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {	
			//System.out.println("REDIRECT URL=" + redirectUrl);
			if (redirectUrl.startsWith("http://")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create("http://www.stubhub.com" + redirectUrl);				
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for(Header header: headers) {
				// System.out.println("Header=" + header.getName() + "=" + header.getValue());
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			
			return false;
		}

	}	
	
	public static void main(String[] args) throws Exception {
		int i=0;
		StubHubWebFeedTicketListingFetcherOld feed = new StubHubWebFeedTicketListingFetcherOld();
		
		
		while(i<15){
			SimpleHttpClient httpClient = HttpClientStore.createHttpClient("stubhub", "", "");
		i++;
		TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
		TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
		ticketListingCrawl.setId(123);
		ticketListingCrawl.setQueryUrl("https://www.stubhub.com?event_id=9148649");///
		ticketListingCrawl.setSiteId(Site.STUB_HUB);
		System.out.println("result:--: " + feed.runFetchTicketListing(httpClient, ticketHitIndexer, ticketListingCrawl));
		}
	}
}