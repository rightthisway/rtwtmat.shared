package com.admitone.tmat.ticketfetcher;

import java.net.InetAddress;
import java.util.Date;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TicketType;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class TicketLiquidatorTicketListingFetcher extends TicketListingFetcher {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(TicketLiquidatorTicketListingFetcher.class);

	@Override
	public boolean runFetchTicketListing(SimpleHttpClient httpClient,
			TicketHitIndexer ticketHitIndexer,
			TicketListingCrawl ticketListingCrawl) throws Exception {
		if (!(ticketListingCrawl.getSiteId().equals(Site.TICKET_LIQUIDATOR))) {
			throw new Exception(this.getClass().getName()+ ".fetchTicketListing parameter must be a TicketCity crawl");
		}

		String proxy = "";
		if (httpClient.getHost() != null) {
			proxy = httpClient.getHost();

		} else {
			proxy = InetAddress.getLocalHost().getHostName();
		}

		long startProcessingTime = System.currentTimeMillis();

		String eventId = null;
		String url = ticketListingCrawl.getQueryUrl();
		String SubStr1 = new String("evtid=");
		int eventIdPos = url.indexOf(SubStr1);

		if (eventIdPos > 0) {
			eventId = url.substring(eventIdPos + 6, url.length());
		} else {
			String[] tokens = url.split("-");
			eventId = tokens[tokens.length - 1].split("/")[0];
			eventId = eventId.replace(".html", "");
		}

		CrawlerHistoryManagement crawlerHistoryManagement = new CrawlerHistoryManagement();
		long startFetchingTime = System.currentTimeMillis();
		int counter = 1;

		startFetchingTime = System.currentTimeMillis();
		String EventIdTicketUrl = "https://mapwidget2.seatics.com/Api/TicketsByEvent?eventId="+ eventId + "&websiteConfigId=237";
		HttpGet httpGet = new HttpGet(EventIdTicketUrl);
		httpGet.setHeader("User-Agent","Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpGet.addHeader("Accept","application/json, text/javascript, text/html, application/xhtml+xml, application/xml,*/*");
		httpGet.addHeader("Content-Type", "application/json, charset=utf-8");
		httpGet.addHeader("Accept-Encoding", "gzip,deflate");
		counter++;
		crawlerHistoryManagement.setCounter(counter);
		crawlerHistoryManagement.setSuccessfulHit(new Date());

		// Hit the ticketliquidator site and get the response
		CloseableHttpResponse response = null;
		String content = null;
		try{
			response = httpClient.execute(httpGet);
			int statusCode = response.getStatusLine().getStatusCode();
			if (statusCode < 200 || statusCode >= 300) {
				try {
					throw new Exception("Bad Response from TLTLFecther Server");
				} catch (Exception e) {

					e.printStackTrace();
				}
			}
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity).trim();
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}

		if (null == content || content.isEmpty()) {
			try {
				throw new Exception("Empty Response from TicketCity Server");
			} catch (Exception e) {

				e.printStackTrace();
			}
		}

		// removal of parenthesis (,),[ ,] and null from the response
		String jsonpFirst = null;
		jsonpFirst = content.substring(content.indexOf("(") + 1,content.lastIndexOf(")"));
		String jsonpSecond = null;
		jsonpSecond = jsonpFirst.substring(jsonpFirst.indexOf("[") + 1,jsonpFirst.lastIndexOf("]"));
		String jsonpThird = null;
		jsonpThird = jsonpSecond.substring(jsonpSecond.indexOf("[") + 1,jsonpSecond.lastIndexOf("],null"));

		String[] jsonpTokens = jsonpThird.split("\\]");
		
		String jsopElement = "";
		String tempSecond = "";
		String jsopElement1 = "";
		int totalTicketCount = 0;
		for (int i = 0; i < jsonpTokens.length; i++) {
			// take individual array &remove unnecessary [
			totalTicketCount++;
			String temp = jsonpTokens[i];
			jsopElement = jsonpTokens[i];
			String tempStr1 = new String("['',");
			int index = jsopElement.indexOf(tempStr1);

			jsopElement1 = jsopElement.substring(index + 4);

			// After removal  all construct final token..
			String[] finalToken = jsopElement1.split("\\,");
			
			String section = "", row = "", quantity = "", price = "", ticketId = "";
			String notes_0 = "";
			String notes_1 = "";
			String final_notes = "";
			int qty = 0;
			Double price1 = 0.0;

			for (int j = 0; j < finalToken.length; j++) {

				section = finalToken[0];
				row = finalToken[1];
				quantity = finalToken[2];
				qty = Integer.parseInt(quantity);

				price = finalToken[3];
				price1 = Double.parseDouble(price);
				ticketId = finalToken[4];
				

				notes_0 = finalToken[5];
				if (notes_0 != null) {
					notes_1 = finalToken[6];
					final_notes = notes_0 + "," + notes_1;
				}
				final_notes = notes_0;

			}
			String seat = "";
			TicketType ticketType = TicketType.REGULAR;
			TicketDeliveryType ticketDeliveryType = TicketDeliveryType.EDELIVERY;
			boolean inHand = false;
	
			int lotSize = 1;
			if ((qty % 2) == 0 && qty < 14) {
				lotSize = 2;
			}

			Double buyItNowPrice = price1;
			TicketHit ticketHit = new TicketHit(Site.TICKET_LIQUIDATOR,
					ticketId, ticketType, qty, lotSize, row, section, price1,
					buyItNowPrice, "Ticket_Liquidator", TicketStatus.ACTIVE);
			ticketHit.setSoldQuantity(0);
			ticketHit.setSeat(seat);
			ticketHit.setTicketListingCrawl(ticketListingCrawl);
			ticketHit.setTicketDeliveryType(ticketDeliveryType);
			indexTicketHit(ticketHitIndexer, ticketHit);
			ticketHit.setInHand(inHand);
			long startIndexationTime = System.currentTimeMillis();
			ticketListingCrawl.setIndexationTime(ticketListingCrawl
					.getIndexationTime()
					+ System.currentTimeMillis()
					- startIndexationTime);

		}

		System.out.println("Total Ticket Count-" + totalTicketCount++);
		saveCounterHistory(ticketListingCrawl, counter, proxy);
		ticketListingCrawl.setExtractionTime(System.currentTimeMillis()
				- startProcessingTime - ticketListingCrawl.getFetchingTime()
				- ticketListingCrawl.getIndexationTime());
		return true;

	}

	/*
	 * public static void main(String[] args) throws Exception {
	 * 
	 * TicketLiquidatorTicketListingFetcher feed1 = new
	 * TicketLiquidatorTicketListingFetcher(); 
	 * SimpleHttpClient httpClient =
	 * HttpClientStore.createHttpClient("ticketliquidator","","");
	 * 
	 * TicketHitIndexer ticketHitIndexer = new BulkTicketHitIndexer();
	 * TicketListingCrawl ticketListingCrawl = new TicketListingCrawl();
	 * ticketListingCrawl.setId(123); ticketListingCrawl.setQueryUrl(
	 * "http://www.ticketliquidator.com/tix/tickets.aspx?evtid=2607835");
	 * ticketListingCrawl.setSiteId(Site.TICKET_LIQUIDATOR);
	 * 
	 * System.out.println("result:--: " +
	 * feed1.runFetchTicketListing(httpClient, ticketHitIndexer,
	 * ticketListingCrawl));
	 * 
	 * 
	 * }
	 */
}
