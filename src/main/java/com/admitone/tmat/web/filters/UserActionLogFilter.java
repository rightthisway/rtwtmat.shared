package com.admitone.tmat.web.filters;

import java.util.Date;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.acegisecurity.ui.webapp.AuthenticationProcessingFilter;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.User;
import com.admitone.tmat.web.Constants;

public class UserActionLogFilter implements Filter
{
    private FilterConfig filterConfig;
    private String[] ignoreList = new String[]{"GetCrawlsExecutedForEventsCount"};
    public boolean isIgnoreURL(String url){
    	boolean flag= false;
    	for(String ignoreUrL:ignoreList){
    		if(url.contains(ignoreUrL)){
    			flag =true;
    			break;
    		}
    	}
    	return flag;
    }
    public void doFilter(ServletRequest req,ServletResponse res, FilterChain fc)
        throws java.io.IOException, javax.servlet.ServletException {
    	String username = "Test";
    	HttpSession session =  ((HttpServletRequest)req).getSession();
    	String ipAddress = ((HttpServletRequest) req).getHeader("X-FORWARDED-FOR");  
		if (ipAddress == null) {  
			ipAddress = req.getRemoteAddr();  
			session.setAttribute("ipAddress", ipAddress);
		}
    	
    	username = (String) session.getAttribute(AuthenticationProcessingFilter.ACEGI_SECURITY_LAST_USERNAME_KEY);
    	Date timeStamp =  new Date();
    	session.setAttribute("lastTimeStamp",timeStamp);
    	if(username != null && !username.isEmpty()){
    		String path =((HttpServletRequest)req).getPathInfo();
    		if(path != null && !isIgnoreURL(path)){
    			if(path != null && (path.toLowerCase().contains("logout") || path.toLowerCase().contains("logedout"))){
        			// Dirty check for logout 
        		}else  if(((HttpServletRequest)req).getServletPath().contains("/a1")){
        			if(!path.trim().equals("/")){
        				if(!path.contains("/plaincall/")){
        					if(!path.contains(".js")){
        						User user =(User)((HttpServletRequest)req).getSession().getAttribute("userObj");
        						if(user==null){
        							user = DAORegistry.getUserDAO().getUserByUsernameOrEmail(username, username);
        							((HttpServletRequest)req).getSession().setAttribute("userObj",user);
        						}
        						DAORegistry.getUserActionDAO().logUserAction(user, path, ((HttpServletRequest)req).getPathInfo() + " with parameters=" + (Map<String, String>)req.getParameterMap(),ipAddress,timeStamp);
        					}
        				}
        			}else{
        				String serveName =Constants.getInstance().getHostName();
    					User user =(User)((HttpServletRequest)req).getSession().getAttribute("userObj");
    					if(user==null){
    						user = DAORegistry.getUserDAO().getUserByUsernameOrEmail(username, username);
    						((HttpServletRequest)req).getSession().setAttribute("userObj",user);
    						DAORegistry.getUserActionDAO().logUserAction(user, "/Login" , ((HttpServletRequest)req).getPathInfo() + " with parameters=" + (Map<String, String>)req.getParameterMap(),ipAddress,timeStamp);
    						if(null != user){
    							DAORegistry.getLiveUserDAO().updateLiveUserByUserName(user.getUsername(), serveName, 'Y');
    						}
    					}
    				}
        		}	
    		}
    		    	
    	}else{	
	    	String recU = "Test";
	    	recU = req.getParameter("j_password");
	    	if(recU!=null){
	    		session.setAttribute("rec_u", recU);
	    	}
    	}
    	fc.doFilter(req,res); // invoke next item in the chain -- 
                            // either another filter or the
                            // originally requested resource. 
 
    }

    public FilterConfig getFilterConfig()
    {
      // Execute tasks
      return filterConfig;
    }

    public void setFilterConfig(FilterConfig cfg)
    {
      // Execute tasks 
      filterConfig = cfg;
    }

	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
}
