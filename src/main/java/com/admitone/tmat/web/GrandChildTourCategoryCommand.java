package com.admitone.tmat.web;

import com.admitone.tmat.data.GrandChildTourCategory;


public class GrandChildTourCategoryCommand {
	private Integer id;
	private String name;
	private Integer childTourCategory;
	
	public GrandChildTourCategoryCommand(GrandChildTourCategory grandChildTourCategory){
		this.id=grandChildTourCategory.getId();
		this.name=grandChildTourCategory.getName();
		this.childTourCategory=grandChildTourCategory.getChildTourCategory().getId();
	}
	
	public GrandChildTourCategoryCommand() {
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public Integer getChildTourCategory() {
		return childTourCategory;
	}
	public void setChildTourCategory(Integer childTourCategory) {
		this.childTourCategory = childTourCategory;
	}

}
