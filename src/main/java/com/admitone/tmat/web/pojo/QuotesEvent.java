package com.admitone.tmat.web.pojo;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.TicketQuote;

public class QuotesEvent {
	private Event event;
	private Collection<TicketQuote> quotes = new ArrayList<TicketQuote>();
	private boolean boundToImage = false;
	private String imageType;
	private String map;
	private String savedFile;
	private String mapPath;
	

	public String getImageType() {
		return imageType;
	}

	public void setImageType(String imageType) {
		this.imageType = imageType;
	}

	public QuotesEvent(Event event) {
		this.event = event;
	}
	
	public Event getEvent() {
		return event;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	
	public Collection<TicketQuote> getQuotes() {
		return quotes;
	}

	public void setQuotes(Collection<TicketQuote> quotes) {
		this.quotes = quotes;
	}
	
	public void addQuote(TicketQuote quote) {
		this.quotes.add(quote);
	}
	
	public void setBoundToImage(boolean value) {
		this.boundToImage = value;
	}
	
	public Boolean getBoundToImage() {
		return boundToImage;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public String getMap() {
		String filename ="";
		if(null != event.getVenueCategory()){
			filename = "C:\\TMATIMAGESFINAL\\" + event.getVenue().getId()+ "_" + event.getVenueCategory().getCategoryGroup() + ".gif";
			//filename = "\\\\admit1\\Shared\\TMATIMAGESFINAL\\" + event.getVenue().getId()+ "_" + event.getVenueCategory().getCategoryGroup() + ".gif";
		}
		File file = new File(filename);
		
		if (!file.exists()){
		return "";
		}else{
			return filename.replaceAll("/", "\\\\");
		}
		
	}
	
	public String getSavedFile() {
		return savedFile;
	}

	public void setSavedFile(String savedFile) {
		this.savedFile = savedFile;
	}

	public String getMapPath() {
		if(null != event.getVenueCategory() && null != event.getVenueCategory().getCategoryGroup()){
			this.mapPath = "http://zonetickets.com/zones/maps/" + event.getVenue().getId()+ "_" + event.getVenueCategory().getCategoryGroup() + ".gif";
		}else{
			this.mapPath = "http://zonetickets.com/maps/NO_MAP.gif";
		}
		return mapPath;
	}

	public void setMapPath(String mapPath) {
		this.mapPath = mapPath;
	}
	
	
}