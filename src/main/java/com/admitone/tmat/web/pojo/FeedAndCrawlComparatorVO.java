package com.admitone.tmat.web.pojo;

import java.util.List;

import com.admitone.tmat.ticketfetcher.TicketHit;

public class FeedAndCrawlComparatorVO {

	private Integer ticketIndexedFromFeed;
	private Integer ticketIndexedFromWeb;
	
	private List<TicketHit> extraTicketInFeed;
	private List<TicketHit> missingTicketInFeed;
	
	
	public Integer getTicketIndexedFromFeed() {
		return ticketIndexedFromFeed;
	}
	public void setTicketIndexedFromFeed(Integer ticketIndexedFromFeed) {
		this.ticketIndexedFromFeed = ticketIndexedFromFeed;
	}
	public Integer getTicketIndexedFromWeb() {
		return ticketIndexedFromWeb;
	}
	public void setTicketIndexedFromWeb(Integer ticketIndexedFromWeb) {
		this.ticketIndexedFromWeb = ticketIndexedFromWeb;
	}
	public List<TicketHit> getExtraTicketInFeed() {
		return extraTicketInFeed;
	}
	public void setExtraTicketInFeed(List<TicketHit> extraTicketInFeed) {
		this.extraTicketInFeed = extraTicketInFeed;
	}
	public List<TicketHit> getMissingTicketInFeed() {
		return missingTicketInFeed;
	}
	public void setMissingTicketInFeed(List<TicketHit> missingTicketInFeed) {
		this.missingTicketInFeed = missingTicketInFeed;
	}
}
