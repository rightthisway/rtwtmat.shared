package com.admitone.tmat.web.pojo;

public class Setting {

	private String host= "";//resourceBundle.getString("host");
	private String developerTokenId = "";//resourceBundle.getString("developerTokenId");//"8";
	private String customerId = "";//resourceBundle.getString("customerId");//"7391";
	private String developerAuthToken = "";//resourceBundle.getString("developerAuthToken");//"zBasH7q9Pbm1WzYqrPI+rieM3Me5g5rShCzWN4XGyJp6Tt8eivGLlHZv32FqVBQw3JYJRs2BkPVaNGlYI8RLgw==";
	private String customerAuthToken = "";//resourceBundle.getString("customerAuthToken");//"CAMEKVLjjHDsuoOBW0LTqKqfhTDrfWtvL1APJpKHF9vfVUu2Hc34PDcfczZ3ZImMd61EYS/pO5oeLSqq5LFWIw==";
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getDeveloperTokenId() {
		return developerTokenId;
	}
	public void setDeveloperTokenId(String developerTokenId) {
		this.developerTokenId = developerTokenId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getDeveloperAuthToken() {
		return developerAuthToken;
	}
	public void setDeveloperAuthToken(String developerAuthToken) {
		this.developerAuthToken = developerAuthToken;
	}
	public String getCustomerAuthToken() {
		return customerAuthToken;
	}
	public void setCustomerAuthToken(String customerAuthToken) {
		this.customerAuthToken = customerAuthToken;
	}
}
