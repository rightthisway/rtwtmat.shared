package com.admitone.tmat.web;

import com.admitone.tmat.data.Artist;

public class ArtistCommand {
	
	private Integer id;
	private String name;
	private String artistStatus;
	private Integer grandChildTourCategoryId;
	private String grandChildTourCategory;
	
	public ArtistCommand() {
		// TODO Auto-generated constructor stub
	}
	public ArtistCommand(Artist artist){
//		this.id = artistCommand.id;
		this.setGrandChildTourCategoryId(artist.getGrandChildTourCategoryId());
		this.setId(artist.getId());
		this.setName(artist.getName());
		this.setGrandChildTourCategory(artist.getGrandChildTourCategory()==null?"":(artist.getGrandChildTourCategory().getName()+ ", " + artist.getGrandChildTourCategory().getChildTourCategory().getName()+ ", " + artist.getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName()));
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getArtistStatus() {
		return artistStatus;
	}
	public void setArtistStatus(String artistStatus) {
		this.artistStatus = artistStatus;
	}
	public Integer getGrandChildTourCategoryId() {
		return grandChildTourCategoryId;
	}
	public void setGrandChildTourCategoryId(Integer grandChildTourCategoryId) {
		this.grandChildTourCategoryId = grandChildTourCategoryId;
	}
	public String getGrandChildTourCategory() {
		return grandChildTourCategory;
	}
	public void setGrandChildTourCategory(String grandChildTourCategory) {
		this.grandChildTourCategory = grandChildTourCategory;
	}
}
