package com.admitone.tmat.web;

import com.admitone.tmat.data.ChildTourCategory;


public class ChildTourCategoryCommand {
	private Integer id;
	private String name;
	private Integer tourCategory;
	public ChildTourCategoryCommand(ChildTourCategory childTourCategory){
		this.id=childTourCategory.getId();
		this.name=childTourCategory.getName();
		this.tourCategory=childTourCategory.getTourCategory().getId();
	}
	public ChildTourCategoryCommand() {
		// TODO Auto-generated constructor stub
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Integer getTourCategory() {
		return tourCategory;
	}
	public void setTourCategory(Integer tourCategory) {
		this.tourCategory = tourCategory;
	}

}
