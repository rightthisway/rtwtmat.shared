package com.admitone.tmat.web.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import com.admitone.tmat.common.SharedProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.User;


public class SwitchController extends MultiActionController{
	
	private SharedProperty sharedProperty;
	
	public SharedProperty getSharedProperty() {
		return sharedProperty;
	}
	public void setSharedProperty(SharedProperty sharedProperty) {
		this.sharedProperty = sharedProperty;
	}
	
	public String switchToAdmin(HttpServletRequest request,HttpServletResponse response){
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userObj");
		String userName = user.getUsername();
		String recU = (String)session.getAttribute("rec_u");
//		String url = "redirect:"+sharedProperty.getBrowseUrl() + "AllowToBrowse?userName="+userName+"&password="+recU;
		return  "redirect:"+sharedProperty.getAdminUrl() + "AllowToAdmin?userName="+userName+"&password="+recU;
	}
	
	public String allowToAdmin(HttpServletRequest request,HttpServletResponse response){
		return "redirect:"+sharedProperty.getAdminUrl().replaceAll("a1/", "") + "j_acegi_security_check?j_username="+ request.getParameter("userName") +"&j_password="+request.getParameter("password");
	}
	
	public String switchToAnalytics(HttpServletRequest request,HttpServletResponse response){
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userObj");
		String userName = user.getUsername();
		String recU = (String)session.getAttribute("rec_u");
//		String url = "redirect:"+sharedProperty.getBrowseUrl() + "AllowToBrowse?userName="+userName+"&password="+recU;
		return  "redirect:"+sharedProperty.getAnalyticsUrl() + "AllowToAnalytics?userName="+userName+"&password="+recU;
	}
	
	public String allowToAnalytics(HttpServletRequest request,HttpServletResponse response){
		return "redirect:"+sharedProperty.getAnalyticsUrl().replaceAll("a1/", "") + "j_acegi_security_check?j_username="+ request.getParameter("userName") +"&j_password="+request.getParameter("password");
	}
	
	public String switchToBrowse(HttpServletRequest request,HttpServletResponse response){
		HttpSession session = request.getSession();
		User user = (User)session.getAttribute("userObj");
		String userName = user.getUsername();
		String recU = (String)session.getAttribute("rec_u");
//		String url = "redirect:"+sharedProperty.getBrowseUrl() + "AllowToBrowse?userName="+userName+"&password="+recU;
		return  "redirect:"+sharedProperty.getBrowseUrl() + "AllowToBrowse?userName="+userName+"&password=password";//+recU;
	}
	
	public String allowToBrowse(HttpServletRequest request,HttpServletResponse response){
		return "redirect:"+sharedProperty.getBrowseUrl().replaceAll("a1/", "") + "j_acegi_security_check?j_username="+ request.getParameter("userName") +"&j_password="+request.getParameter("password");
	}
	
	public String logout(HttpServletRequest request,HttpServletResponse response) {
		HttpSession httpSession = ((HttpServletRequest)request).getSession();
		User user =(User)httpSession.getAttribute("userObj");
		if(user==null){
			String userName = request.getParameter("username");
			user = DAORegistry.getUserDAO().getUserByUsernameOrEmail(userName, userName);
		}
		String ipAddress = (String)httpSession.getAttribute("ipAddress");
//		Date timeStamp = (Date)httpSession.getAttribute("lastTimeStamp");
		DAORegistry.getUserActionDAO().logUserAction(user, "/Logout", "",ipAddress,new Date());
		String ret = "redirect:" + request.getContextPath().replaceAll("a1/", "") + "/j_acegi_logout";
		return ret;
	}
}
