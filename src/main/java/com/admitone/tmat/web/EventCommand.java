package com.admitone.tmat.web;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourType;


public final class EventCommand {
	
		private String name;
		private String artistName;
		private Integer artistId;
//		private String venueName;
		private Integer venueId;
		private Integer venueCategoryId;
		private TourType eventType;
		private Integer tnPOSEventId;
		private String tnPOSEventName;
		private Date localDate;
		private Time localTime;
		private boolean timeTbd = false;
		private boolean dateTbd = false;
		private Integer id; 
		private EventStatus eventStatus ;
		private String timeZoneId;
		private String updatedBy;
		private Integer tempArtistId;
		private Integer tempVenueId;
		private Venue venue;
		private Artist artist;
		private Integer brokerId;
		protected String brokerStatus; 
		private Boolean presaleEvent;
		public EventCommand() {
			// TODO Auto-generated constructor stub
		}
		
		public EventCommand(Event event){
			if(event!=null){
				this.name = event.getName();
				this.artistId = event.getArtistId();
				this.venueId = event.getVenueId();
				this.venue=event.getVenue();
				this.artist=event.getArtist();
				this.brokerId = event.getBrokerId();
				this.brokerStatus = event.getBrokerStatus();
				this.tempArtistId=event.getTempArtistId();
				this.tempVenueId=event.getTempVenueId();
				this.venueCategoryId = event.getVenueCategoryId();
				this.eventType = event.getEventType();
				this.tnPOSEventId = event.getAdmitoneId();
				this.artistName = (this.artist!=null?artist.getName():"");
				this.localDate = event.getLocalDate();
				this.localTime = event.getLocalTime();
				this.dateTbd = event.getDateTbd();
				this.timeTbd = event.getTimeTbd();
				this.presaleEvent = event.getPresaleEvent();
				AdmitoneEvent tnPOSEvent = null;
				if(event.getAdmitoneId()!=null){
					tnPOSEvent = DAORegistry.getAdmitoneEventDAO().get(event.getAdmitoneId());
				}
				if(tnPOSEvent!=null){
					SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat tf = new SimpleDateFormat("HH:mm");
					String date= df.format(tnPOSEvent.getEventDate());
					String time = tf.format(tnPOSEvent.getEventTime());
					this.tnPOSEventName =tnPOSEvent.getEventName() + "," + date + "," + time + "," + tnPOSEvent.getVenueName() ;
				}
			}
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		/*public String getArtistName() {
			return artistName;
		}
		public void setArtistName(String artistName) {
			this.artistName = artistName;
		}*/
		public Integer getArtistId() {
			return artistId;
		}
		public void setArtistId(Integer artistId) {
			this.artistId = artistId;
		}
		/*public String getVenueName() {
			return venueName;
		}
		public void setVenueName(String venueName) {
			this.venueName = venueName;
		}*/
		public Integer getVenueId() {
			return venueId;
		}
		public void setVenueId(Integer venueId) {
			this.venueId = venueId;
		}
		public Integer getVenueCategoryId() {
			return venueCategoryId;
		}
		public void setVenueCategoryId(Integer venueCategoryId) {
			this.venueCategoryId = venueCategoryId;
		}
		public TourType getEventType() {
			return eventType;
		}
		public void setEventType(TourType eventType) {
			this.eventType = eventType;
		}
		public Integer getTnPOSEventId() {
			return tnPOSEventId;
		}
		public void setTnPOSEventId(Integer tnPOSEventId) {
			this.tnPOSEventId = tnPOSEventId;
		}
		public String getTnPOSEventName() {
			return tnPOSEventName;
		}
		public void setTnPOSEventName(String tnPOSEventName) {
			this.tnPOSEventName = tnPOSEventName;
		}

		public String getArtistName() {
			return artistName;
		}

		public void setArtistName(String artistName) {
			this.artistName = artistName;
		}

		public Date getLocalDate() {
			return localDate;
		}

		public void setLocalDate(Date localDate) {
			this.localDate = localDate;
		}

		public Time getLocalTime() {
			return localTime;
		}

		public void setLocalTime(Time localTime) {
			this.localTime = localTime;
		}

		public boolean isTimeTbd() {
			return timeTbd;
		}

		public void setTimeTbd(boolean timeTbd) {
			this.timeTbd = timeTbd;
		}

		public boolean isDateTbd() {
			return dateTbd;
		}

		public void setDateTbd(boolean dateTbd) {
			this.dateTbd = dateTbd;
		}

		public Integer getId() {
			return id;
		}

		public void setId(Integer id) {
			this.id = id;
		}

		public EventStatus getEventStatus() {
			return eventStatus;
		}

		public void setEventStatus(EventStatus eventStatus) {
			this.eventStatus = eventStatus;
		}

		public String getTimeZoneId() {
			return timeZoneId;
		}

		public void setTimeZoneId(String timeZoneId) {
			this.timeZoneId = timeZoneId;
		}

		public String getUpdatedBy() {
			return updatedBy;
		}

		public void setUpdatedBy(String updatedBy) {
			this.updatedBy = updatedBy;
		}

		public Integer getTempArtistId() {
			return tempArtistId;
		}

		public void setTempArtistId(Integer tempArtistId) {
			this.tempArtistId = tempArtistId;
		}

		public Integer getTempVenueId() {
			return tempVenueId;
		}

		public void setTempVenueId(Integer tempVenueId) {
			this.tempVenueId = tempVenueId;
		}

		public Venue getVenue() {
			/*if(venueId  != null ){
				venue = DAORegistry.getVenueDAO().get(venueId);
			}*/
			return venue;
		}

		public void setVenue(Venue venue) {
			this.venue = venue;
		}

		public Artist getArtist() {
			/*if(artistId  != null ){
				artist = DAORegistry.getArtistDAO().get(artistId);
			}*/
			return artist;
		}

		public void setArtist(Artist artist) {
			this.artist = artist;
		}

		public Integer getBrokerId() {
			return brokerId;
		}

		public void setBrokerId(Integer brokerId) {
			this.brokerId = brokerId;
		}

		public String getBrokerStatus() {
			return brokerStatus;
		}

		public void setBrokerStatus(String brokerStatus) {
			this.brokerStatus = brokerStatus;
		}

		public Boolean getPresaleEvent() {
			return presaleEvent;
		}

		public void setPresaleEvent(Boolean presaleEvent) {
			this.presaleEvent = presaleEvent;
		}
		
		
		
		
}