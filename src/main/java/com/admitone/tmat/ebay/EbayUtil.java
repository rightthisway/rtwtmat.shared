package com.admitone.tmat.ebay;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.data.Ticket;

public final class EbayUtil {
	public static final int MAX_QUANTITY = 12;
	
	private EbayUtil() {}
	
	/**
	 * Tell if a quantity can be a partition of a bigger quantity.
	 * For instance a ticket of quantity 5 can be subdivided in 1, 2, 3, 5
	 * @param quantity
	 * @param totalQuantity
	 * @return
	 */
	public static boolean isPartition(int quantity, int totalQuantity) {
		if (totalQuantity % 2 == 1) {
			totalQuantity = Math.min(totalQuantity, MAX_QUANTITY-1);
		}else{
			totalQuantity = Math.min(totalQuantity, MAX_QUANTITY);
		}
		if (quantity == totalQuantity) {
			return true;
		}
		if(totalQuantity<5){
			if (totalQuantity % 2 == 1) {
				return (quantity % 2 == 1 && quantity <= totalQuantity - 2);
			} else {
				return (quantity <= totalQuantity && quantity % 2 == 0);
			}
		}else if (totalQuantity==6){
			if((quantity <=totalQuantity) &&(quantity%2==0 || quantity==1)){
				return true;
			}else{
				return false;
			}
		}else{
			return (quantity <= totalQuantity - 2);
		}
	}
	
	/**
	 * Return collection of tickets compatible with a quantity
	 * @param tickets
	 * @param quantity
	 * @return
	 */
	public static Collection<Ticket> getTicketsForQuantity(Collection<Ticket> tickets, int quantity) {
		Collection<Ticket> result = new ArrayList<Ticket>();
		for(Ticket ticket: tickets) {
			if(quantity<=ticket.getRemainingQuantity()){
			if (isPartition(quantity, ticket.getRemainingQuantity())) {
				result.add(ticket);
			}}
		}
		
		return result;
	}
	
	/*public static void main(String[] args) { 
		for (int quantity=1;quantity<12;quantity++){
			if(quantity<=4){
			}
		}
	}*/
}
