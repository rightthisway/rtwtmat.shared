package com.admitone.tmat.ebay;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.HashMap;
import java.util.concurrent.Semaphore;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.utils.mail.MailManager;


public class EbayManager {//implements InitializingBean {
	public static final int MIN_TICKET_GROUP_SIZE = 3;
//	private EbayThread ebayThread = null;
	private MailManager mailManager;
	private static Logger logger = LoggerFactory.getLogger(EbayManager.class);
	private static String emailRecipient;
	private static String emailSubject;
	private static String categoryScheme;
	private static Double fanAddition;
	private static Double invalidAddition = 900D;
	private static Integer crawlerFrequency;
	private static Double defaultRptFactor = 25.00;
	private boolean refreshing = false;
	private Semaphore refreshLock = new Semaphore(1); 
	private static HashMap<Integer, Collection<String>> catschemeMap = new HashMap<Integer, Collection<String>>();
	
	public boolean isRefreshing() {
		return refreshing;
	}

	public static Double getEbayFees(Double price) {
		Double fees = 0D;

		if (price <= 50) {
			fees = .12 * price;
		} else if (price <= 1000) {
			fees = (.12 * 50) + (price - 50) * .06;
		} else {
			fees = (.12 * 50) + 950 * .06 + (price - 1000) * .02;
		}

		fees *= .80; // -20% as powerseller

		return fees;
	}

	public static Double getPaypalFees(Double price) {
		return price * .025 + .30;
	}

	/*public void saveEbayInventoryFromEventId(Integer eventId, String username) {
		boolean skip = false;
		EbayInventoryEvent ebayInventoryEvent = DAORegistry
				.getEbayInventoryEventDAO().get(eventId);

		Event event = DAORegistry.getEventDAO().get(eventId);
		if (ebayInventoryEvent == null) {
			ebayInventoryEvent = new EbayInventoryEvent(eventId, username,
					new Date());
			ebayInventoryEvent.setRptFactor(defaultRptFactor);
			DAORegistry.getEbayInventoryEventDAO().save(ebayInventoryEvent);

			// change the crawlers to crawl more frequently
			updateEbayCrawlersFrequency(eventId);
		} else {
			skip = true;
		}

		// check if the ebay inventory tour exists
		EbayInventoryTour ebayInventoryTour = DAORegistry
				.getEbayInventoryTourDAO().get(event.getTourId());
		if (ebayInventoryTour == null) {
			ebayInventoryTour = new EbayInventoryTour(event.getTourId(), null);
			DAORegistry.getEbayInventoryTourDAO().save(ebayInventoryTour);
		}

		if (skip) {
			return;
		}

		saveEbayInventoryFromTickets(eventId);
	}

	public void saveEbayInventoryFromEventIds(
			Collection<Integer> eventIds, String username) {
//		Collection<Ticket> tickets = new ArrayList<Ticket>();
		for (Integer eventId : eventIds) {
			Event event = DAORegistry.getEventDAO().get(eventId);
			if (event == null) {
				continue;
			}

			EbayInventoryEvent ebayInventoryEvent = DAORegistry.getEbayInventoryEventDAO().get(eventId);
			if (ebayInventoryEvent == null) {
				ebayInventoryEvent = new EbayInventoryEvent(eventId, username,
						new Date());
				ebayInventoryEvent.setRptFactor(defaultRptFactor);

				EbayInventoryTour ebayInventoryTour = DAORegistry
						.getEbayInventoryTourDAO().get(event.getTourId());
				if (ebayInventoryTour == null) {
					ebayInventoryTour = new EbayInventoryTour(
							event.getTourId(), null);
					DAORegistry.getEbayInventoryTourDAO().save(
							ebayInventoryTour);
				} else {
					ebayInventoryEvent.setExposure(ebayInventoryTour.getExposure());
					ebayInventoryEvent.setEnableZoneMaps(ebayInventoryTour.getEnableZoneMaps());
					ebayInventoryEvent.setNotes(ebayInventoryTour.getNotes());
				}
				
				DAORegistry.getEbayInventoryEventDAO().save(ebayInventoryEvent);

			} else {
				continue;
			}

			saveEbayInventoryFromTickets(eventId);
		}
	}*/
	
	/*private void recomputeTicketIds(Collection<EbayInventoryGroup> groups) {
		Date now = new Date();
		Long numDays = Long.valueOf(DAORegistry.getPropertyDAO().get("ebay.ticketId.ttl").getValue());

		// first expire not valid tickets
		for (EbayInventoryGroup group: groups) {
			if (group.getTicketId() == null) {
				continue;
			}
			
			if (group.isValid()) {
				continue;
			}
			// this is a ticket which have been created but
			// not uploaded to ebay yet or a ticket which is
			// older than 30 days
			// if disabled or invalid => null 
			// if deleted => null the ticket id
			// if valid => try to re-use the existing ticket id of
			// an invalid or deleted group
			if (group.getFirstTimeUsed() == null
				|| now.getTime() - group.getFirstTimeUsed().getTime() > numDays * 24L * 3600L * 1000L) {								
				if (group.isDeleted()) {
					DAORegistry.getEbayInventoryGroupDAO().delete(group);
					group.setTicketId(null);
					group.setFirstTimeUsed(null);
				} else {
					group.setTicketId(null);
					group.setFirstTimeUsed(null);
					DAORegistry.getEbayInventoryGroupDAO().update(group);
				}
			}
		}
		
		for (EbayInventoryGroup group: groups) {
			if (!group.isValid()) {
				continue;	
			}

			// this is a ticket which have been created but
			// not uploaded to ebay yet or a ticket which is
			// older than 30 days
			// if disabled or invalid => null 
			// if deleted => null the ticket id
			// if valid => try to re-use the existing ticket id of
			// an invalid or deleted group
			if (group.getTicketId() == null || group.getFirstTimeUsed() == null
				|| now.getTime() - group.getFirstTimeUsed().getTime() > numDays * 24L * 3600L * 1000L) {
				Object[] availableTicket = bookFirstGroupWithAvailableTicketId();			

				if (availableTicket[1] == null) {
					group.setFirstTimeUsed(new Date());
					if (group.getTicketId() == null) {
						group.setTicketId((Integer)availableTicket[0]);
					}
				} else {
					group.setTicketId((Integer)availableTicket[0]);
					group.setFirstTimeUsed((Date)availableTicket[1]);
				}
				DAORegistry.getEbayInventoryGroupDAO().update(group);
			}
			
			
//			if (now.getTime() - group.getFirstTimeUsed().getTime() > numDays * 24L * 3600L * 1000L) {								
//				Object[] availableTicket = bookFirstGroupWithAvailableTicketId();
//
//				if (group.getTicketId() != null) {
//					group.setFirstTimeUsed(now);
//				} else {  
//					group.setTicketId((Integer)availableTicket[0]);
//					group.setFirstTimeUsed((Date)availableTicket[1]);
//					logger.info("1- availgroup values " + group.getTicketId() + " " + group.getFirstTimeUsed());
//				}
//				DAORegistry.getEbayInventoryGroupDAO().update(group);
//			}
//					
		}
	}

	public EbayInventoryGroup findEbayInventoryGroupbyId(Integer groupId, Collection<EbayInventoryGroup> groups) {
		for (EbayInventoryGroup group: groups) {
			if (group.getId().equals(groupId)) {
				return group;
			}
		}
		return null;
	}
	
	private String getInvalidRow(Integer ticketId) {
		return "Y,"
			+ "TBD vs. TBD,"
			+ "TBD,"
			+ "1/1/2015,"
			+ ","
			+ "2,"
			+ "TBD,"
			+ "TBD,"
			+ "901,," // skip seat from and seat thru
			+ "<br /><b>Click map for larger view</b><br/>,"
			+ "4000," + ticketId + "\n";		
	}
	
	private String getRocketInvalidRow(Integer ticketId) {
		return "Y,"
			+ "TBD vs. TBD,"
			+ "TBD,"
			+ "1/1/2012,"
			+ ","
			+ "2,"
			+ "TBD,"
			+ "TBD,"
			+ "901,," // skip seat from and seat thru
			+ "<br /><b>Click map for larger view</b><br/>,"
			+ "4000," + ticketId + "\n";		
	}
	*/
	public synchronized void downloadCsvEbayFile(Writer writer,
			boolean onlyValid) throws IOException {
	/*	String noteTemplate = DAORegistry.getPropertyDAO().get("ebay.listeasy.notes").getValue();
		
		try {
			DateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");
			DateFormat timeFormat = new SimpleDateFormat("hh:mm aa");

			writer
					.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n");

			Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			Map<Integer, Venue> venueMap = new HashMap<Integer, Venue>();

			Object[] statuses;
			if (onlyValid) {
				statuses = new Object[] { EbayInventoryStatus.ACTIVE };
			} else {
				statuses = new Object[] { EbayInventoryStatus.ACTIVE,
						EbayInventoryStatus.INVALID,
						EbayInventoryStatus.DISABLED,
						EbayInventoryStatus.REPLACEMENT,
						EbayInventoryStatus.DELETED };
			}

			Collection<EbayInventoryGroup> groups = DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroups(statuses);
			recomputeTicketIds(groups);

			for (EbayInventoryGroup group : groups) {
				if (!group.isValid() || group.hasIncompatibleTicketQuantities()) {
					if (group.getTicketId() != null) {
						String row = getInvalidRow(group.getTicketId());
						writer.write(row);
					}
					continue;
				}
				
				EbayInventory inventory1 = group.getEbayInventory1();
				
				Collection<String> catschemelist = catschemeMap.get(group.getEventId());
				
				String catschemtouse = categoryScheme;
				for(String catscheme : catschemelist){
					if(!catscheme.equals(categoryScheme)){
						catschemtouse = catscheme;
					}
				}
				
				//Category category = inventory1.getTicket().getCategory(categoryScheme);
				Category category = inventory1.getTicket().getCategory(catschemtouse);
				if (category == null) {
					String row = getInvalidRow(group.getTicketId());
					writer.write(row);
					continue;
				}
				
				Event event = eventMap.get(group.getEventId());
				if (event == null) {
					event = group.getEvent();
					eventMap.put(group.getEventId(), event);
				}

				Venue venue = venueMap.get(event.getVenueId());
				if (venue == null) {
					venue = DAORegistry.getVenueDAO().get(event.getVenueId());
					venueMap.put(event.getVenueId(), venue);
				}

				String timeStr = "";

				if (event.getLocalTime() == null) {
					timeStr = "";
				} else {
					timeStr = timeFormat.format(event.getLocalTime());
				}

				String img = group.getEbayInventoryEvent().getNotes();
				String notes = "";
				if (img == null) { // take the one from the tour
					img = group.getEbayInventoryEvent().getEbayInventoryTour()
							.getNotes();
				}

				if (img != null) {
					if (group.getEbayInventoryEvent().getEnableZoneMaps()) {
						String symbol = img + group.getCategory();
						if (symbol.length() > 8) {
							symbol = symbol.substring(0, 8);
						}
						notes = noteTemplate.replaceAll("\\[MAP\\]", symbol) + ".gif";						
					} else {
						notes = noteTemplate.replaceAll("\\[MAP\\]", img) + ".gif";
					}
				}

				String categoryStr = "";
				Double rpt = inventory1.getRpt();

				if (group.isValid()) {
					categoryStr = "Zone "
							+ category.getSymbol();
				} else {
					categoryStr = "TBD";
					rpt = (rpt * (100 + invalidAddition)) / 100;
				}

				String row = "Y," + event.getName().replaceAll(",", "-").trim() + ","
						+ venue.getBuilding() + ","
						+ dateFormat.format(event.getLocalDate()) + ","
						+ timeStr + "," + group.getQuantity() + "," + categoryStr
						+ "," + "TBD,"
						+ "901,," // skip seat from and seat thru
						+ notes.trim() + "," + rpt + "," + group.getTicketId()
						+ "\n";
				writer.write(row);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
	}

	/*public synchronized void downloadCsvEbayRocketFile(Writer writer,
			boolean onlyValid) throws IOException {
		String noteTemplateRocket = DAORegistry.getPropertyDAO().get("ebay.rocket.notes").getValue();
		
		try {
			DateFormat dateFormat = new SimpleDateFormat("M/d/yyyy");
			DateFormat timeFormat = new SimpleDateFormat("hh:mm aa");

			writer
					.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n");

			Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
			Map<Integer, Venue> venueMap = new HashMap<Integer, Venue>();

			Object[] statuses;
			if (onlyValid) {
				statuses = new Object[] { EbayInventoryStatus.ACTIVE };
			} else {
				statuses = new Object[] { EbayInventoryStatus.ACTIVE,
						EbayInventoryStatus.INVALID,
						EbayInventoryStatus.DISABLED,
						EbayInventoryStatus.REPLACEMENT,
						EbayInventoryStatus.DELETED };
			}

//			Collection<EbayInventoryGroup> groups = DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroups(statuses);
//			recomputeTicketIds(groups);
//
//			for (EbayInventoryGroup group : groups) {
//				if (!group.isValid() || group.hasIncompatibleTicketQuantities()) {
//					if (group.getTicketId() != null) {
//						String row = getRocketInvalidRow(group.getTicketId());
//						writer.write(row);
//					}
//					continue;
//				}
//				
//				EbayInventory inventory1 = group.getEbayInventory1();
//				Category category = null;
//				try{
//					
//					Collection<String> catschemelist = catschemeMap.get(group.getEventId());
//					//DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(group.getEventId());
//					
//					String catschemtouse = categoryScheme;
//					for(String catscheme : catschemelist){
//						if(!catscheme.equals(categoryScheme)){
//							catschemtouse = catscheme;
//						}
//					}
//					
//					//category = inventory1.getTicket().getCategory(categoryScheme);
//					category = inventory1.getTicket().getCategory(catschemtouse);
//				}catch(Exception e){
//					e.printStackTrace();
//					logger.error("Exception when trying to get category");
//				}
//				if (category == null) {
//					String row = getRocketInvalidRow(group.getTicketId());
//					writer.write(row);
//					continue;
//				}
//				
//				Event event = eventMap.get(group.getEventId());
//				if (event == null) {
//					event = group.getEvent();
//					eventMap.put(group.getEventId(), event);
//				}
//
//				Venue venue = venueMap.get(event.getVenueId());
//				if (venue == null) {
//					venue = DAORegistry.getVenueDAO().get(event.getVenueId());
//					venueMap.put(event.getVenueId(), venue);
//				}
//
//				String timeStr = "";
//
//				if (event.getLocalTime() == null) {
//					timeStr = "";
//				} else {
//					timeStr = timeFormat.format(event.getLocalTime());
//				}
//			 
//				String img = group.getEbayInventoryEvent().getNotes();
//				String notes = "";
//				if (img == null) { // take the one from the tour
//					img = group.getEbayInventoryEvent().getEbayInventoryTour()
//							.getNotes();
//				}
//
//				if (img != null) {
//					if (group.getEbayInventoryEvent().getEnableZoneMaps()) {
//						String symbol = img + group.getCategory();
//						if (symbol.length() > 8) {
//							symbol = symbol.substring(0, 8);
//						}
//						notes = noteTemplateRocket.replaceAll("\\[MAP\\]", symbol) + ".gif";						
//					} else {
//						notes = noteTemplateRocket.replaceAll("\\[MAP\\]", img) + ".gif";
//					}
//				}
//
//				String categoryStr = "";
//				Double rpt = inventory1.getRpt();
//
//				if (group.isValid()) {
//					categoryStr = "Zone "
//							+ category.getSymbol();
//				} else {
//					categoryStr = "TBD";
//					rpt = (rpt * (100 + invalidAddition)) / 100;
//				}
//
//				String row = "Y," + event.getName().replaceAll(",", "-").trim() + ","
//						+ venue.getBuilding() + ","
//						+ dateFormat.format(event.getLocalDate()) + ","
//						+ timeStr + "," + group.getQuantity() + "," + categoryStr
//						+ "," + "TBD,"
//						+ "901,," // skip seat from and seat thru
//						+ notes.trim() + "," + rpt + "," + group.getTicketId()
//						+ "\n";
//				writer.write(row);
//				
//			}
			
		} catch (Exception e) {
			//mailManager.sendMail(fromName, toAddress, subject, resource, map, mimeType)
			
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("message", e.getMessage());
			map.put("username", "pratap");
			getMailManager().sendMail("Exception", "ppalthady@rightthisway.com",
					"Admitone Ebay Notification",
					"mail-ebay-notification.txt", map, "text/plain");
			
			e.printStackTrace();
		}
	}

	
	public Collection<EbayInventoryGroup> computeEbayInventoryFromTickets(Integer eventId) {
		Double netProfitFilter = 0.00;
		try {
			netProfitFilter = Double.valueOf(DAORegistry.getPropertyDAO().get(
					"ebay.filter.netProfit").getValue());
		} catch (Exception e) {
			logger.info("Error while reading property ebay.filter.netProfit",	e);
		}
		return computeEbayInventoryFromTickets(eventId, netProfitFilter);
	}*/

	/*public Collection<EbayInventoryGroup> computeEbayInventoryFromTickets (
			Integer eventId, Double netProfitFilter) {
		//System.out.println("EBAY=> computing groups for event=" + eventId);
		Event event = DAORegistry.getEventDAO().get(eventId);
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		
		
//		Collection<String> catschemelist = catschemeMap.get(event.getId());
//		//DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(event.getId());
//		
//		String catschemtouse = categoryScheme;
//		for(String catscheme : catschemelist){
//			if(!catscheme.equals(categoryScheme)){
//				catschemtouse = catscheme;
//			}
//		}
		
		
		//Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), categoryScheme);		
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), catschemtouse);
		Collection<Category> categories = null;
		if(event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		}else{
//			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catschemtouse);
			categories = new ArrayList<Category>();
		}
		//TicketUtil.preAssignCategoriesToTickets(tickets, categories, categoryScheme);
		TicketUtil.preAssignCategoriesToTickets(tickets, categories);
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		EbayInventoryEvent ebayEvent = DAORegistry.getEbayInventoryEventDAO().get(eventId);
		
		Collection<EbayInventoryGroup> result = new ArrayList<EbayInventoryGroup>();
		
		for (int quantity = 1; quantity <= EbayUtil.MAX_QUANTITY; quantity++) {
			 Collection<Ticket> subset = EbayUtil.getTicketsForQuantity(tickets, quantity);
			 Collection<EbayInventoryGroup> groups = computeEbayInventoryFromTickets(subset, netProfitFilter, quantity, ebayEvent,event.getVenueCategory());
			 result.addAll(groups);
//			 Seems unnecessary code..  - Chirag 09.26.2013
			 for(EbayInventoryGroup group: result){
				 for(Ticket ticket:subset){
					 String key = group.getEventId() + "-" + group.getCategory() + "-" + group.getQuantity()+ "-" +group.getId();
					 if (group.getQuantity()>ticket.getRemainingQuantity()){
						 System.out.println("The ebaygroup is incorrect as the group quantity cannot be greater"  +key);
					}
				 }
			 }
		}
		return result;
	}

	public Collection<EbayInventoryGroup> computeEbayInventoryFromTickets (
			Collection<Ticket> tickets, Double netProfitFilter, Integer quantity, EbayInventoryEvent ebayInventoryEvent,VenueCategory venueCategory) {
		
		if (tickets == null) {
			return new ArrayList<EbayInventoryGroup>();
		}
		Date startDate = new Date();

		// eventId-categoryId-remaining quantity => numOccurences
		Map<String, Integer> catNumTicketsMap = new HashMap<String, Integer>();

		Collection<Ticket> tmpFilteredTickets = new ArrayList<Ticket>();
		Collection<Category> catList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Category cat:catList){
			catMap.put(cat.getId(), cat);
		}
		List<CategoryMapping> CategoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer,List<CategoryMapping>>();
		for(CategoryMapping cm:CategoryMappingList){
			List<CategoryMapping> list = catMappingMap.get(cm.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(cm);
			catMappingMap.put(cm.getCategoryId(), list);
		}

		for (Ticket ticket : tickets) {
	
			Collection<String> catschemelist = catschemeMap.get(ebayInventoryEvent.getEventId());
			//DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(ebayInventoryEvent.getEventId());
			
			String catschemtouse = categoryScheme;
			for(String catscheme : catschemelist){
				if(!catscheme.equals(categoryScheme)){
					catschemtouse = catscheme;
				}
			}
			Integer catId = ticket.getCategoryId(venueCategory, catschemtouse,catMap,catMappingMap);		
			if (catId == null) {
				continue;
			}
//			Category cat = ticket.getCategory();
//			if (ticket.getCategoryId(categoryScheme) == null) {
//				continue;
//			}
			if (ticket.getTicketType().equals(TicketType.AUCTION)) {
				continue;
			}

//			String key = ticket.getEventId() + "-" + ticket.getCategoryId(categoryScheme);
			String key = ticket.getEventId() + "-" + catId;
			if (catNumTicketsMap.get(key) == null) {
				catNumTicketsMap.put(key, 1);
			} else {
				catNumTicketsMap.put(key, catNumTicketsMap.get(key) + 1);
			}
			tmpFilteredTickets.add(ticket);
		}

		Map<String, Set<Ticket>> cheapestTicketsMap = new HashMap<String, Set<Ticket>>();

		// second pass to remove groups with less than exposure level ticket entries
		for (Ticket ticket : tmpFilteredTickets) {
			
			Collection<String> catschemelist = catschemeMap.get(ebayInventoryEvent.getEventId());
			//DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(ebayInventoryEvent.getEventId());
			
			String catschemtouse = categoryScheme;
			for(String catscheme : catschemelist){
				if(!catscheme.equals(categoryScheme)){
					catschemtouse = catscheme;
				}
			}
			
			//String key = ticket.getEventId() + "-"	+ ticket.getCategoryId(categoryScheme);
			Category catgory = ticket.getCategory();
			String key = ticket.getEventId() + "-"	+ catgory.getId();
			
			
			int numEntries = catNumTicketsMap.get(key);
			if (numEntries < ebayInventoryEvent.getExposure()) {
				continue;
			}

			Set<Ticket> cheapestTickets = cheapestTicketsMap.get(key);

			if (cheapestTickets == null) {
				cheapestTickets = new TreeSet<Ticket>(new Comparator<Ticket>() {
					public int compare(Ticket ticket1, Ticket ticket2) {
						int cmp = ticket1.getCurrentPrice().compareTo(
								ticket2.getCurrentPrice());
						if (cmp < 0) {
							return -1;
						}

						if (cmp > 0) {
							return 1;
						}
						
						// if same price get the one with less quantity first
						int cmp2 = ticket1.getRemainingQuantity().compareTo(
								ticket2.getRemainingQuantity());
						if (cmp2 < 0) {
							return -1;
						}

						if (cmp2 > 0) {
							return 1;
						}

						return ticket1.getId().compareTo(ticket2.getId());
					}

				});
				cheapestTicketsMap.put(key, cheapestTickets);
			}

			if (ticket.getSiteId().equalsIgnoreCase("eimarketplace")) {
				if (ticket.getSeller() != null && ticket.getSeller().equalsIgnoreCase("FAN")) {
					ticket.setCurrentPrice(ticket.getCurrentPrice()
							* (100 + fanAddition) / 100);
				}
			}

			Double binPrice = (quantity * ticket
					.getCurrentPrice())
					* (100 + ebayInventoryEvent.getRptFactor()) / 100;
			Double rpt = binPrice / quantity;
			Double netProfit = binPrice
					- (quantity * ticket.getCurrentPrice())
					- getEbayFees(binPrice) - getPaypalFees(binPrice);

			
			 * TicketWithProfit ticketWithProfit = new TicketWithProfit(ticket);
			 * ticketWithProfit.setBinPrice(binPrice);
			 * ticketWithProfit.setRpt(rpt);
			 * ticketWithProfit.setNetProfit(netProfit);
			 
			cheapestTickets.add(ticket);
		}

		List<EbayInventoryGroup> result = new ArrayList<EbayInventoryGroup>();
		for (Set<Ticket> ticketSet : cheapestTicketsMap.values()) {
			Iterator<Ticket> iterator = ticketSet.iterator();
			Collection<EbayInventory> inventories = new ArrayList<EbayInventory>();
			int count = 0;
			Ticket ticket = calculatePricePt(iterator,netProfitFilter,quantity,ebayInventoryEvent,inventories);
			if(ticket == null && count ==1)
			{
				continue;
			}
			else if(ticket == null && count ==0)
			{
				count=count+1;
				iterator = ticketSet.iterator();
				inventories = new ArrayList<EbayInventory>();
				if(iterator.hasNext())
				{
					iterator.next();
					iterator.remove();
				}
				ticket = calculatePricePt(iterator,netProfitFilter,quantity,ebayInventoryEvent,inventories);
				if(ticket == null && count ==1)
				{
					continue;
				}
			}
			
//			Collection<String> catschemelist = catschemeMap.get(ebayInventoryEvent.getEventId());
//			//DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(ebayInventoryEvent.getEventId());
//			
//			String catschemtouse = categoryScheme;
//			for(String catscheme : catschemelist){
//				if(!catscheme.equals(categoryScheme)){
//					catschemtouse = catscheme;
//				}
//			}
			
			
			//EbayInventoryGroup ebayInventoryGroup = new EbayInventoryGroup(ebayInventoryEvent, ticket.getCategory(categoryScheme).getSymbol(), quantity);
			EbayInventoryGroup ebayInventoryGroup = new EbayInventoryGroup(ebayInventoryEvent, ticket.getCategory().getSymbol(), quantity);

			ebayInventoryGroup.setEbayInventories(inventories);
			ebayInventoryGroup.computeInternalValues();
			result.add(ebayInventoryGroup);
		}
		logger.info("computeEbayInventoryFromTickets: "
				+ (new Date().getTime() - startDate.getTime() + " ms"));
		return result;
	}*/

	/*public void saveEbayInventoryFromTickets(
			Integer eventId) {
//		Collection<EbayInventoryGroup> allGroups = DAORegistry
//				.getEbayInventoryGroupDAO().getAll();
		for (EbayInventoryGroup group : computeEbayInventoryFromTickets(eventId)) {
			group.setTicketId(null);
			group.setFirstTimeUsed(null);
			DAORegistry.getEbayInventoryGroupDAO().save(group);
		}

		// EbayInventory ebayInventory1 = new EbayInventory(ticket1,
		// ebayInventoryGroup.getId(), 1);
		// EbayInventory ebayInventory2 = new EbayInventory(ticket2,
		// ebayInventoryGroup.getId(), 2);
		// EbayInventory ebayInventory3 = new EbayInventory(ticket3,
		// ebayInventoryGroup.getId(), 3);
		//			
		// DAORegistry.getEbayInventoryDAO().save(ebayInventory1);
		// DAORegistry.getEbayInventoryDAO().save(ebayInventory2);
		// DAORegistry.getEbayInventoryDAO().save(ebayInventory3);
		//			
		// }
	}*/

	/*public void exportTickets(Writer writer, Integer eventId) {
		DecimalFormat decimalFormat = new DecimalFormat("0.00");

		Collection<Ticket> tickets = DAORegistry.getTicketDAO()
				.getAllActiveTicketsByEvent(eventId);
		Event event = DAORegistry.getEventDAO().get(eventId);
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Category cat:DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId())){
			catMap.put(cat.getId(), cat);
		}
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getCategoryId(), list);
		}
		for (Ticket ticket : tickets) {
			String row = ticket.getRemainingQuantity() + ", ";
			
			
			Collection<String> catschemelist = catschemeMap.get(eventId);
			//DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(eventId);
			
			String catschemtouse = categoryScheme;
			for(String catscheme : catschemelist){
				if(!catscheme.equals(categoryScheme)){
					catschemtouse = catscheme;
				}
			}			
			
//			if (ticket.getCategory(categoryScheme) == null) {
//				row += ", ";
//			} else {
//				row += ticket.getCategory(categoryScheme).getSymbol() + ", ";
//			}
			Category category = ticket.getCategory(event.getVenueCategory(),catschemtouse,catMap,catMappingMap);
			if (category == null) {
				row += ", ";
			} else {
				row += category.getSymbol() + ", ";
			}

			row += ticket.getSection() + ", ";
			row += ticket.getRow() + ", ";
			row += "$" + decimalFormat.format(ticket.getAdjustedCurrentPrice())
					+ ", ";
			row += "$" + decimalFormat.format(ticket.getCurrentPrice()) + ", ";
			// row += ticket.getTitle() + ", ";
			row += ticket.getSeller();
			row += "\r\n";
			try {
				writer.write(row);
			} catch (IOException e) {
				logger.error("Ticket not exportable " + ticket, e);
			}
		}
	}

	public void exportTickets(Writer writer, Collection<Integer> eventIds) {
		for (Integer eventId : eventIds) {
			exportTickets(writer, eventId);
		}
	}*/

	/*private EbayInventoryGroup findFirstGroupWithAvailableTicketId() {
		EbayInventoryGroup group = DAORegistry.getEbayInventoryGroupDAO().findFirstGroupWithAvailableTicketId();
		if (group == null) {
			logger.info("findFirstGroupWithAvailableTicketId: NOT FOUND");
			return null;
		}
		logger.info("findFirstGroupWithAvailableTicketId: FOUND groupId=" + group.getId() + " ticketId=" + group.getTicketId());
		return group;
		
//		List<EbayInventoryGroup> list = new ArrayList<EbayInventoryGroup>(groups);
//		Collections.sort(list, new Comparator<EbayInventoryGroup>() {
//			public int compare(EbayInventoryGroup g1, EbayInventoryGroup g2) {
//				if (g1.getFirstTimeUsed() == null) {
//					if (g2.getFirstTimeUsed() == null) {
//						return 0;
//					}
//					
//					return 1;
//				}
//				
//				if (g2.getFirstTimeUsed() == null) {
//					return -1;
//				}
//				
//				if (g1.getFirstTimeUsed().before(g2.getFirstTimeUsed())) {
//					return 1;
//				}
//
//				if (g1.getFirstTimeUsed().after(g2.getFirstTimeUsed())) {
//					return -1;
//				}
//				
//				return 0;
//			}
//		});
//		
//		
//		// find in priority groups which have been created recently
//		for (EbayInventoryGroup group : list) {
//			if (!group.isValid() && group.getTicketId() != null) {
//				return group;
//			}
//		}

//		
//		for (EbayInventoryGroup group : groups) {
//			if (!group.isValid() && group.getTicketId() != null) {
//				return group;
//			}
//		}
		

//		return null;
	}*/

	/*public String diffEbayInventoryGroups(
			Collection<EbayInventoryGroup> existingGroups,
			Collection<EbayInventoryGroup> newGroups) {
		Date startDate = new Date();
		String diffText = "";

		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");

		Map<String, EbayInventoryGroup> existingGroupMap = new HashMap<String, EbayInventoryGroup>();
		Map<String, EbayInventoryGroup> existingDisabledGroupMap = new HashMap<String, EbayInventoryGroup>();

		Map<String, EbayInventoryGroup> newGroupMap = new HashMap<String, EbayInventoryGroup>();

		for (EbayInventoryGroup group : existingGroups) {
			String key = group.getCategory() + "-" + group.getQuantity();

			if (group.getStatus().equals(EbayInventoryStatus.DELETED)) {
				continue;
			}

			if (group.getStatus().equals(EbayInventoryStatus.DISABLED)) {
				existingDisabledGroupMap.put(key, group);
			} else {
				// put in existing group:
				// - VALID
				// - SUGGESTED
				// - REPLACEMENT (priority over INVALID)
				// - INVALID
				if (group.isInvalid()) {
					EbayInventoryGroup existingGroup = existingGroupMap
							.get(key);
					if (existingGroup == null) {
						existingGroupMap.put(key, group);
					}
				} else {
					existingGroupMap.put(key, group);
				}
			}
		}

		for (EbayInventoryGroup group : newGroups) {
			newGroupMap.put(group.getCategory() + "-" + group.getQuantity(),
					group);
		}

		for (EbayInventoryGroup group : existingGroups) {

			// if the group is deleted, disabled or invalid skip it
			if (group.getStatus().equals(EbayInventoryStatus.DELETED)
				|| group.getStatus().equals(EbayInventoryStatus.DISABLED)
				|| group.getStatus().equals(EbayInventoryStatus.INVALID)) {
				continue;
			}

			String key = group.getCategory() + "-" + group.getQuantity();
			EbayInventoryGroup newGroup = newGroupMap.get(key);

			// the group has disappeared and is active or replacement => group
			// is now invalid
			// if they are suggested then remove it
			if (newGroup == null) {
				if (group.isSuggested()) {
					DAORegistry.getEbayInventoryGroupDAO().delete(group);
				} else {
					// if the group is a replacement then make it invalid and
					// remove the group
					// it replaces
					if (group.isReplacement()) {
						EbayInventoryGroup replacedGroup = group
								.getReplacedEbayInventoryGroup();
						if (replacedGroup == null) {
							logger.warn("replaced group is null "
									+ group.getId());
						} else {
							DAORegistry.getEbayInventoryGroupDAO().delete(
									replacedGroup);
						}
					}
					group.setStatus(EbayInventoryStatus.INVALID);
					group.setReplacedEbayInventoryGroupId(null);
					DAORegistry.getEbayInventoryGroupDAO().update(group);
					diffText += "Group Category " + group.getId() + ": "
							+ group.getCategory() + " Quantity: "
							+ group.getQuantity() + " is now invalid.\n\r";
				}
				continue;
			}

			// the group is different
			// if the group is cheaper then:
			// - if group is ACTIVE then invalid the other one and create a new
			// entry
			// - if group is SUGGESTED/REPLACEMENT then remove entry and create
			// a new entry
			try{
				if (group.hasInvalidTickets() || group.hasIncompatibleTicketQuantities() || newGroup.getEbayInventory1().getBinPrice().compareTo(
						group.getEbayInventory1().getBinPrice()) < 0) {
					if (group.getStatus().equals(EbayInventoryStatus.ACTIVE)) {
						diffText += "Found a replacement for group "
								+ group.getId() + " Category: "
								+ group.getCategory() + " quantity: "
								+ group.getQuantity() + ".\n\r";
						group.setStatus(EbayInventoryStatus.INVALID);
						group.setReplacedEbayInventoryGroupId(null);
	
						newGroup.setTicketId(group.getTicketId());
						newGroup.setFirstTimeUsed(group.getFirstTimeUsed());
						newGroup.setStatus(EbayInventoryStatus.REPLACEMENT);
						newGroup.setReplacedEbayInventoryGroupId(group.getId());
						DAORegistry.getEbayInventoryGroupDAO().save(newGroup);
						group.setTicketId(null);
						group.setFirstTimeUsed(null);
						DAORegistry.getEbayInventoryGroupDAO().update(group);
					} else {
						DAORegistry.getEbayInventoryGroupDAO().delete(group);
						existingGroupMap.remove(key);
						newGroup.setStatus(group.getStatus());
						newGroup.setTicketId(group.getTicketId());
						newGroup.setFirstTimeUsed(group.getFirstTimeUsed());
						newGroup.setReplacedEbayInventoryGroupId(group
								.getReplacedEbayInventoryGroupId());
						DAORegistry.getEbayInventoryGroupDAO().save(newGroup);
					}
					existingGroupMap.put(key, newGroup);
	
					// EbayInventoryGroup existingGroup =
					// DAORegistry.getEbayInventoryGroupDAO().getByEventCategoryQuantity(group.getEventId(),
					// group.getCategory(), group.getQuantity());
					// if (existingGroup != null) {
					// if (existingGroup.isDisabled()) {
					// if (!isEquals(existingGroup, group)) {
					// DAORegistry.getEbayInventoryGroupDAO().save(newGroup);
					// DAORegistry.getEbayInventoryDAO().saveAll(newGroup.getEbayInventories());
					// }
					// }
					// } else {
					// DAORegistry.getEbayInventoryGroupDAO().save(newGroup);
					// DAORegistry.getEbayInventoryDAO().saveAll(newGroup.getEbayInventories());
					// }
				} 
			}catch(Exception e){
				logger.error(e.fillInStackTrace().getMessage());
			}
//			else {
//				// same price - if it's a different group then replace the
//				// current one implicitly
//				if (!isEquals(group, newGroup)) {
//					DAORegistry.getEbayInventoryDAO().deleteByGroupId(
//							group.getId());
//					for (EbayInventory inventory : newGroup.getEbayInventories()) {
//						inventory.setGroupId(group.getId());
//						DAORegistry.getEbayInventoryDAO().save(inventory);
//					}
//				}
//			}
		}

		// iterate over all the new found groups and compare with the existing
		// ones
		for (EbayInventoryGroup group : newGroups) {
			String key = group.getCategory() + "-" + group.getQuantity();
			EbayInventoryGroup existingGroup = existingGroupMap.get(key);
			EbayInventoryGroup existingDisabledGroup = existingDisabledGroupMap
					.get(key);

			// if it does not exist then create a new SUGGESTED entry
			// if it exists then:
			// - if it is an INVALID group and there is no REPLACEMENT, offer a
			// replacement
			if (existingGroup == null) {
				if (existingDisabledGroup == null) {
					// ||
					// existingDisabledGroup.getEbayInventory1().getOnlinePrice().compareTo(group.getEbayInventory1().getOnlinePrice())
					// > 0) {
					group.setStatus(EbayInventoryStatus.SUGGESTED);
					if (existingDisabledGroup != null) {
						group.setTicketId(existingDisabledGroup.getTicketId());
						group.setFirstTimeUsed(existingDisabledGroup.getFirstTimeUsed());
					} else {
						group.setTicketId(null);
					}
					DAORegistry.getEbayInventoryGroupDAO().save(group);
					diffText += "Found a new group Category " + group.getId()
							+ ": " + group.getCategory() + " quantity: "
							+ group.getQuantity() + ".\n\r";
				}
			} else if (existingGroup.isInvalid()) {
				group.setStatus(EbayInventoryStatus.REPLACEMENT);
				group.setReplacedEbayInventoryGroupId(existingGroup.getId());
				group.setTicketId(existingGroup.getTicketId());
				group.setFirstTimeUsed(existingGroup.getFirstTimeUsed());
				DAORegistry.getEbayInventoryGroupDAO().save(group);
				diffText += "Found a replacement " + group.getId()
						+ " for group Category: " + group.getCategory()
						+ " quantity: " + group.getQuantity() + ".\n\r";

				existingGroup.setReplacedEbayInventoryGroupId(null);
				existingGroup.setTicketId(null);
				existingGroup.setFirstTimeUsed(null);
				DAORegistry.getEbayInventoryGroupDAO().update(existingGroup);
			}
		}

		logger.info("diffEbayInventoryGroups: "
				+ (new Date().getTime() - startDate.getTime()) + " ms");
		return diffText;
	}*/

	/*private void deleteOthersThanGroup(EbayInventoryGroup group,
			EbayInventoryStatus status) {
		Collection<EbayInventoryGroup> groups = DAORegistry
				.getEbayInventoryGroupDAO().getByEventCategoryQuantity(
						group.getEventId(), group.getCategory(),
						group.getQuantity(), status);
		for (EbayInventoryGroup curGroup : groups) {
			if (curGroup.getId().equals(group.getId())) {
				continue;
			}

			DAORegistry.getEbayInventoryGroupDAO().delete(curGroup);
		}
	}

	public boolean isEquals(EbayInventoryGroup group1, EbayInventoryGroup group2) {
		if (!group1.getEventId().equals(group2.getEventId())) {
			return false;
		}

		if (!group1.getCategory().equals(group2.getCategory())) {
			return false;
		}

		if (!group1.getQuantity().equals(group2.getQuantity())) {
			return false;
		}

		if (!group1.getEbayInventory1().getNetProfit().equals(
				group2.getEbayInventory1().getNetProfit())) {
			return false;
		}

		if ((group1.getEbayInventory2() == null
			&& group2.getEbayInventory2() != null)
			|| (group1.getEbayInventory2() != null
					&& group2.getEbayInventory2() == null)) {
			return false;
		}
		
		if (group1.getEbayInventory2() != null
			&& group2.getEbayInventory2() != null
			&& !group1.getEbayInventory2().getNetProfit().equals(
				group2.getEbayInventory2().getNetProfit())) {
			return false;
		}

		if ((group1.getEbayInventory3() == null
				&& group2.getEbayInventory3() != null)
				|| (group1.getEbayInventory3() != null
						&& group2.getEbayInventory3() == null)) {
				return false;
			}

		if (group1.getEbayInventory3() != null
				&& group2.getEbayInventory3() != null
		        && !group1.getEbayInventory3().getNetProfit().equals(
				group2.getEbayInventory3().getNetProfit())) {
			return false;
		}

		return true;
	}

	public synchronized Object[] bookFirstGroupWithAvailableTicketId() {
		return DAORegistry.getEbayInventoryGroupDAO().bookFirstGroupWithAvailableTicketId();
	}
	*/
	/*public boolean addGroups(Collection<Integer> groupIds) {
//		Collection<EbayInventoryGroup> allGroups = DAORegistry
//				.getEbayInventoryGroupDAO().getAll();
		// check if we add the same group twice

		Map<String, EbayInventoryGroup> existingEbayInventoryMap = new HashMap<String, EbayInventoryGroup>();
		for (EbayInventoryGroup group : DAORegistry.getEbayInventoryGroupDAO().getAllActiveEbayInventoryGroups()) {
			String key = group.getEventId() + "-" + group.getCategory() + "-"
					+ group.getQuantity();
			existingEbayInventoryMap.put(key, group);
		}

		Set<String> excludeGroups = new HashSet<String>();
		
		Map<String, EbayInventoryGroup> ebayInventoryMap = new HashMap<String, EbayInventoryGroup>();
		for (Integer groupId : groupIds) {
			EbayInventoryGroup group = DAORegistry.getEbayInventoryGroupDAO().get(groupId);
			if (group == null || group.getStatus().equals(EbayInventoryStatus.ACTIVE)) {
				continue;
			}

			String key = group.getEventId() + "-" + group.getCategory() + "-"
					+ group.getQuantity();

			if (ebayInventoryMap.get(key) != null) {
				excludeGroups.add(key);
			}

			if (existingEbayInventoryMap.get(key) != null) {
				excludeGroups.add(key);
			}

			ebayInventoryMap.put(key, group);
		}

		for (EbayInventoryGroup group : ebayInventoryMap.values()) {
			String key = group.getEventId() + "-" + group.getCategory() + "-"
						+ group.getQuantity();
			
			if (excludeGroups.contains(key)) {
				continue;
			}

			if (group.getStatus().equals(EbayInventoryStatus.INVALID)) {
				continue;
			}

			if (group.getStatus().equals(EbayInventoryStatus.REPLACEMENT)) {
				DAORegistry.getEbayInventoryGroupDAO().deleteById(
						group.getReplacedEbayInventoryGroupId());
			}

			group.setStatus(EbayInventoryStatus.ACTIVE);
			group.setReplacedEbayInventoryGroupId(null);
			DAORegistry.getEbayInventoryGroupDAO().update(group);
		}
		
		return excludeGroups.isEmpty();
	}*/

	/*public void removeGroups(Collection<Integer> groupIds) {
		for (Integer groupId : groupIds) {
			EbayInventoryGroup group = DAORegistry.getEbayInventoryGroupDAO()
					.get(groupId);

			// FIXME: not logical here - either become DELETED OR DISABLED
			// but not depending on the status
			if (group.isInvalid()) {
				group.setStatus(EbayInventoryStatus.DELETED);
				group.setEventId(null);
			} else {
				group.setStatus(EbayInventoryStatus.DISABLED);				
			}
			
			group.setReplacedEbayInventoryGroupId(null);
			DAORegistry.getEbayInventoryGroupDAO().update(group);

			for (EbayInventoryGroup existingGroup : DAORegistry
					.getEbayInventoryGroupDAO().getByEventCategoryQuantity(
							group.getEventId(), group.getCategory(),
							group.getQuantity(), null)) {
				if (!existingGroup.getId().equals(group.getId())) {
					DAORegistry.getEbayInventoryGroupDAO()
							.delete(existingGroup);
				}
			}
		}
	}*/

	/*public void deleteExpiredEbayInventoryEvents() {
		Calendar calendar = new GregorianCalendar();
		for (EbayInventoryEvent ebayInventoryEvent : DAORegistry
				.getEbayInventoryEventDAO().getAll()) {
			Integer eventId = ebayInventoryEvent.getEventId();
			Event event = DAORegistry.getEventDAO().get(eventId);

			Calendar eventCalendar = new GregorianCalendar();
			eventCalendar.setTime(event.getLocalDate());
			if (eventCalendar.get(Calendar.YEAR) < calendar.get(Calendar.YEAR)) {
				DAORegistry.getEbayInventoryEventDAO().delete(
						ebayInventoryEvent);
			} else if (eventCalendar.get(Calendar.YEAR) == calendar
					.get(Calendar.YEAR)) {
				if (eventCalendar.get(Calendar.MONTH) < calendar
						.get(Calendar.MONTH)) {
					DAORegistry.getEbayInventoryEventDAO().delete(
							ebayInventoryEvent);
				} else if (eventCalendar.get(Calendar.MONTH) == calendar
						.get(Calendar.MONTH)) {
					if (eventCalendar.get(Calendar.DATE) < calendar
							.get(Calendar.DATE)) {
						DAORegistry.getEbayInventoryEventDAO().delete(
								ebayInventoryEvent);
					}
				}
			}

			if (event.getEventStatus().equals(EventStatus.EXPIRED)) {
				DAORegistry.getEbayInventoryEventDAO().delete(
						ebayInventoryEvent);
			}
		}
	}

	public String refreshEvent(Integer eventId) {
		try {
			refreshLock.acquire();
			
			Event event = DAORegistry.getEventDAO().get(eventId);
			DateFormat format = new SimpleDateFormat("MM/dd/yyyy");
			Collection<EbayInventoryGroup> existingGroups = DAORegistry
					.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByEventId(
							eventId);
			Date loadTickets = new Date();

			Collection<EbayInventoryGroup> newGroups = computeEbayInventoryFromTickets(eventId);
			Date loadDate = new Date();
			logger.info("refreshEvent.load : " + eventId + "=>"
					+ (loadDate.getTime() - loadTickets.getTime()) + " numGroups="
					+ newGroups.size());
	
			String result = diffEbayInventoryGroups(existingGroups, newGroups);
			if (result != null && !result.isEmpty()) {
				result = "==================================================\n"
						+ event.getName() + " "
						+ format.format(event.getLocalDate()) + "\n"
						+ "==================================================\n\n"
						+ result + "\n";
			}
			logger.info("refreshEvent.diff: " + eventId + "=>"
					+ (new Date().getTime() - loadDate.getTime()));
			return result;
		} catch(InterruptedException e) {
			logger.info(e.getMessage());
			e.printStackTrace();
		} finally {
			refreshLock.release();
		}
		
		return null;
	}

	public Collection<EbayInventoryGroup> removeReplacedGroup(
			Collection<EbayInventoryGroup> groups) {
		Collection<EbayInventoryGroup> result = new ArrayList<EbayInventoryGroup>();

		Map<Integer, Boolean> replacedGroupMap = new HashMap<Integer, Boolean>();
		for (EbayInventoryGroup group : groups) {
			if (group.getReplacedEbayInventoryGroupId() != null) {
				replacedGroupMap.put(group.getReplacedEbayInventoryGroupId(),
						true);
			}
		}

		for (EbayInventoryGroup group : groups) {
			if (replacedGroupMap.get(group.getId()) == null) {
				result.add(group);
			}
		}

		return result;
	}

	public void updateEbayCrawlersFrequency(Integer eventId) {
		for (TicketListingCrawl crawl : DAORegistry.getTicketListingCrawlDAO()
				.getTicketListingCrawlByEvent(eventId)) {
			crawl.setAutomaticCrawlFrequency(false);
			crawl.setCrawlFrequency(crawlerFrequency);
			DAORegistry.getTicketListingCrawlDAO().update(crawl);
		}
	}

	public void updateEbayCrawlersFrequency() {
		for (EbayInventoryEvent ebayInventoryEvent : DAORegistry
				.getEbayInventoryEventDAO().getAll()) {
			updateEbayCrawlersFrequency(ebayInventoryEvent.getEventId());
		}
	}

	public void updateEbayCrawlersWithAutoFrequency(Integer eventId) {
		for (TicketListingCrawl crawl : DAORegistry.getTicketListingCrawlDAO()
				.getTicketListingCrawlByEvent(eventId)) {
			crawl.setAutomaticCrawlFrequency(true);
			DAORegistry.getTicketListingCrawlDAO().update(crawl);
		}
	}

	public void updateEbayCrawlersWithAutoFrequency() {
		for (EbayInventoryEvent ebayInventoryEvent : DAORegistry
				.getEbayInventoryEventDAO().getAll()) {
			updateEbayCrawlersWithAutoFrequency(ebayInventoryEvent.getEventId());
		}
	}

	public Collection<EbayInventoryGroup> sort(
			Collection<EbayInventoryGroup> groups) {
		final Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
		final Map<Integer, Tour> tourMap = new HashMap<Integer, Tour>();
		for (EbayInventoryGroup group : groups) {
			if (eventMap.get(group.getEventId()) == null) {
				Event event = DAORegistry.getEventDAO().get(group.getEventId());
				eventMap.put(group.getEventId(), event);

				if (tourMap.get(event.getTourId()) == null) {
					Tour tour = DAORegistry.getTourDAO().get(event.getTourId());
					tourMap.put(event.getTourId(), tour);
				}
			}
		}

		List<EbayInventoryGroup> list = new ArrayList<EbayInventoryGroup>(
				groups);
		Collections.sort(list, new Comparator<EbayInventoryGroup>() {
			public int compare(EbayInventoryGroup ebayInventoryGroup1,
					EbayInventoryGroup ebayInventoryGroup2) {
				Event event1 = eventMap.get(ebayInventoryGroup1.getEventId());
				Event event2 = eventMap.get(ebayInventoryGroup2.getEventId());

				Tour tour1 = tourMap.get(event1.getTourId());
				Tour tour2 = tourMap.get(event1.getTourId());

				if (tour1.getName().compareTo(tour2.getName()) < 0) {
					return -1;
				} else if (tour1.getName().compareTo(tour2.getName()) > 0) {
					return 1;
				}

				if (event1.getLocalDate().before(event2.getLocalDate())) {
					return -1;
				} else if (event1.getLocalDate().after(event2.getLocalDate())) {
					return 1;
				}

				if (event1.getLocalTime() == null) {
					if (event2.getLocalTime() != null) {
						return 1;
					}
				} else if (event2.getLocalTime() == null) {
					return -1;
				} else {
					if (event1.getLocalTime().before(event2.getLocalTime())) {
						return -1;
					} else if (event1.getLocalTime().after(
							event2.getLocalTime())) {
						return 1;
					}
				}

				if (ebayInventoryGroup1.getCategory().compareTo(
						ebayInventoryGroup2.getCategory()) < 0) {					
					return -1;
				} else if (ebayInventoryGroup1.getCategory().compareTo(
						ebayInventoryGroup2.getCategory()) > 0) {
					return 1;
				}

				if (ebayInventoryGroup1.getQuantity() > ebayInventoryGroup2
						.getQuantity()) {
					return -1;
				} else if (ebayInventoryGroup1.getQuantity() < ebayInventoryGroup2
						.getQuantity()) {
					return 1;
				}

				return ebayInventoryGroup1.getId().compareTo(
						ebayInventoryGroup2.getId());

			}
		});
		return list;
	}

	public String refreshAllEvents() {
		try {
			refreshing = true;
			Map<String, String> textMap = new HashMap<String, String>();
			String fullText = "";
			
			deleteExpiredEbayInventoryEvents();

			for (EbayInventoryEvent ebayInventoryEvent : DAORegistry
					.getEbayInventoryEventDAO().getAll()) {
				String output = refreshEvent(ebayInventoryEvent.getEventId());
				if (output.isEmpty()) {
					continue;
				}
				fullText += output;
				if (textMap.get(ebayInventoryEvent.getUsername()) == null) {
					textMap.put(ebayInventoryEvent.getUsername(), output);
				} else {
					textMap.put(ebayInventoryEvent.getUsername(), textMap
							.get(ebayInventoryEvent.getUsername())
							+ output);
				}
			}

			if (!fullText.trim().isEmpty()) {
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("message", fullText);
				map.put("username", "ALL");
				getMailManager().sendMail(emailSubject, emailRecipient,
						"Admitone Ebay Notification",
						"mail-ebay-notification.txt", map, "text/plain");
			}

			for (Entry<String, String> entry : textMap.entrySet()) {
				User user = DAORegistry.getUserDAO().getUserByUsername(entry.getKey());
				Map<String, Object> map = new HashMap<String, Object>();
				map.put("message", fullText);
				map.put("username", user.getUsername());
				getMailManager().sendMail(emailSubject, user.getEmail(),
						"Admitone Ebay Notification",
						"mail-ebay-notification.txt", map, "text/plain");
			}

			DateFormat format = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
			Property lastUpdateProperty = new Property(
					"ebay.check.lastRefresh", format.format(new Date()));
			DAORegistry.getPropertyDAO().saveOrUpdate(lastUpdateProperty);
			return fullText;
		} catch (Exception e) {
			logger.info("Error in refresh all events", e);
			e.printStackTrace();
		} finally {
			refreshing = false;
		}
		return "*** ERROR ***";
	}*/

	/*public static void reinitProperties() {
		try {
			emailSubject = DAORegistry.getPropertyDAO().get(
					"ebay.email.subject").getValue();
			emailRecipient = DAORegistry.getPropertyDAO().get(
					"ebay.email.recipient").getValue();
			categoryScheme = DAORegistry.getPropertyDAO().get(
					"ebay.category.scheme").getValue();
			fanAddition = Double.valueOf(DAORegistry.getPropertyDAO().get(
					"ebay.eimp.fan.addition").getValue());
			crawlerFrequency = Integer.valueOf(DAORegistry.getPropertyDAO()
					.get("ebay.crawler.frequency").getValue()) * 60;
			defaultRptFactor = Double.valueOf(DAORegistry.getPropertyDAO().get(
					"ebay.rptFactor").getValue());
			invalidAddition = Double.valueOf(DAORegistry.getPropertyDAO().get(
					"ebay.invalid.addition").getValue());
			
			Collection<Event> activeEvents = DAORegistry.getEventDAO().getAllActiveEvents();
			for(Event event: activeEvents){ // TODO Check if any utility is not working anymore.. // Chirag
				Collection<String> distinctSchemes = Categorizer.getCategoryGroupsByVenueId(event.getVenueId()) ;//DAORegistry.getCategoryDAO().getAllCategoryGroupsByEvent(event.getId());
				catschemeMap.put(event.getId(), distinctSchemes);
			}
			
			
		} catch (Exception e) {
			categoryScheme = "DEFAULT";
			fanAddition = 15.00;
			crawlerFrequency = 30 * 60;
			invalidAddition = 900D;
		}
	}

	public static class EbayThread extends Thread {
		private static final int DEFAULT_PERIOD = 10;
		private boolean running = true;
		private int checkPeriod = DEFAULT_PERIOD;
		private EbayManager ebayManager;

		public EbayThread(EbayManager ebayManager) {
			this.ebayManager = ebayManager;
			try {
				checkPeriod = Integer.valueOf(DAORegistry.getPropertyDAO().get(
						"ebay.check.period").getValue());
			} catch (Exception e) {
			}
		}

		public void run() {
			running = true;
			while (running) {
				try {
					if (!ebayManager.isRefreshing()) {
						
						//Refresh All events
						ebayManager.refreshAllEvents();
						
						//Code below adds suggested groups and replacement groups.
						//The below lines were added on 10/08/2010
						Collection<EbayInventoryGroup> groups = new ArrayList<EbayInventoryGroup>();
						groups = DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByTourId(null, new Object[]{EbayInventoryStatus.SUGGESTED,EbayInventoryStatus.REPLACEMENT});
						Collection<Integer> groupIds = new ArrayList<Integer>();
						for(EbayInventoryGroup ebayInventoryGroup : groups){
							groupIds.add(ebayInventoryGroup.getId());
						}
						if(groupIds.size() > 0){
							try{
								//TODO Add alerts in the future for errors.
								if(ebayManager.addGroups(groupIds)){
									System.out.println("The ticket groups have been added successfully");
								}else{
									System.out.println("Ticket group(s) not added because you were trying to make 2 groups of the same event, category, quantity active.");
								}
							}catch(Exception e){
								System.out.println("Error occured while adding suggested and replacement group");
								e.printStackTrace();
							}
						}
						System.out.println("The end time of refresh all events is " + new Date() +  " refreshing all the events");
						
					}
					Thread.sleep(checkPeriod * 60000);
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				} catch (Exception e) {
					Map<String, Object> map = new HashMap<String, Object>();
					map.put("date", new Date());
					map.put("hostName", Constants.getInstance().getHostName());
					map.put("hostAddress", Constants.getInstance().getIpAddress());				
					map.put("exception", e);
					
					String[] emailTo = DAORegistry.getPropertyDAO().get("ebay.email.recipient").getValue().split(",");
					ebayManager.getMailManager().sendMail("TMAT Ebay Checker", emailTo, "[" + Constants.getInstance().getHostName() + "] Ebay error", "mail-ebay-error.txt", map, "text/plain");

					logger.error("Error in ebay thread", e);
					try {
						Thread.sleep(60000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
			}
		}

		public void cancel() {
			running = false;
		}
	}*/

	/*public synchronized void start() {
		if (ebayThread == null) {
			ebayThread = new EbayThread(this);
			ebayThread.start();
		}
	}

	public synchronized void stop() {
		if (ebayThread != null) {
			ebayThread.cancel();
			ebayThread.interrupt();
			ebayThread = null;
		}
	}

	public synchronized void restart() {
		stop();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		start();
	}

	
	private Ticket calculatePricePt(Iterator<Ticket> iterator,Double netProfitFilter, Integer quantity,
				EbayInventoryEvent ebayInventoryEvent,Collection<EbayInventory> inventories)
	{
		boolean flag = true;
		Ticket ticket1 = iterator.next();

		Double binPrice = (double) (((double) quantity * ticket1.getCurrentPrice()) * (100D + ebayInventoryEvent
				.getRptFactor())) / 100.00;
		Double rpt = binPrice / (double) quantity;
		Double netProfit = binPrice
				- ((double) quantity * ticket1
						.getCurrentPrice()) - getEbayFees(binPrice)
				- getPaypalFees(binPrice);

		if (netProfit.compareTo(netProfitFilter) < 0) {
			return null;
		}

		inventories.add(new EbayInventory(ticket1, 1));

		// if exposure is more than 1 then
		// check if the profit for the first backup (xp2)
		// is positive
		if (ebayInventoryEvent.getExposure() > 1 && iterator != null) {
			Ticket ticket2 = null;
			try{
				ticket2 = iterator.next();
			}catch(NoSuchElementException e){
				return null;
			}
			inventories.add(new EbayInventory(ticket2, 2));
			Double tg2 = netProfit
				- (ticket2.getCurrentPrice() - ticket1.getCurrentPrice())
				* quantity;				
			if (tg2 < 0) {
				return null;
			}

			// if exposure is more than 2 then
			// check if the profit for the second backup (xp3)
			// is positive
			
			if (ebayInventoryEvent.getExposure() > 2) {
				Ticket ticket3 = null;
				try{
					ticket3 = iterator.next();
				}catch(NoSuchElementException e){
					return null;
				}
				inventories.add(new EbayInventory(ticket3, 3));
				Double tg3 = netProfit
					- (ticket3.getCurrentPrice() - ticket1.getCurrentPrice())
					* quantity;
				if (tg3 < 0) {
					return null;
				}
			}
		}
		
		
		return ticket1;
	}*/
	
	
	/*public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	public void afterPropertiesSet() throws Exception {
		reinitProperties();
		DAORegistry.getEbayInventoryGroupDAO().cleanGroups();
		start();
	}*/
	
	
	
	public static void main(String[] args)  {
				String notes = "http://www.admitone.com/Maps/sfga.gif";
				System.out.println( "<A HREF =\""    + notes +  "\" target = \"_blank\" ><img src=\""   +notes+ "\"  width=\"850\"></img></a>");
			}
	}
