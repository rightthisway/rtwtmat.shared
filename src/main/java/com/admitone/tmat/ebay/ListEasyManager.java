package com.admitone.tmat.ebay;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.data.Site;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class ListEasyManager {
	public static final String LIST_EASY_URL_LOGIN = "http://www.listeasy.net/login.php"; 
	public static final String PATTERN_LOGIN_SUCCESS = "http://www.listeasy.net/uploadtickets";

	public boolean login(String username, String password) {
		return login(new DefaultHttpClient(), username, password);
	}

	public boolean login(CloseableHttpClient httpClient, String username, String password) {
		HttpPost httpPost = new HttpPost(LIST_EASY_URL_LOGIN);
		httpPost.addHeader("Host", "www.listeasy.net");		
		httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");			
		
		String postData = "amember_login=" + username
						+ "&amember_pass=" + password;

		System.out.println("[" + postData + "]");
		try {
			httpPost.setEntity(new StringEntity(postData, "UTF-8"));
			HttpEntity entity = null;
			CloseableHttpResponse response = null;
			String text = new String();
			try{
				response = httpClient.execute(httpPost);
				entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				text = IOUtils.toString(entity.getContent());
				//ensure it is fully consumed
				if(entity != null)
					EntityUtils.consumeQuietly(entity);
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
		/*	if (response.matches("<h1>Welcome ")) {
				return true;
			}
			*/
			
			System.out.println(text);
			if (text.contains(PATTERN_LOGIN_SUCCESS)) {
				return true;
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
	
	public boolean upload(String username, String password, File file) throws Exception {
  	    SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.LIST_EASY);
/*  	    if (!login(httpClient, username, password)) {
  	    	return false;
  	    }
  	    
  	    */
  	    
	    httpClient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
	    
		HttpPost httppost = new HttpPost("http://localhost/upload.php");
	    MultipartEntity mpEntity = new MultipartEntity();
	    
	    httppost.setEntity(mpEntity);
	    CloseableHttpResponse response = null;
		try {
		    ContentBody cbFile = new FileBody(file, "text/csv");
		    mpEntity.addPart("upload", cbFile);
			response = httpClient.execute(httppost);
		    HttpEntity resEntity = response.getEntity();
		    System.out.println(response.getStatusLine());
		    if (resEntity != null) {
		      System.out.println(EntityUtils.toString(resEntity));
		      EntityUtils.consumeQuietly(resEntity);
		    }
			return true;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			response.close();
		    httpClient.getConnectionManager().shutdown();			
		}		
	}
}
