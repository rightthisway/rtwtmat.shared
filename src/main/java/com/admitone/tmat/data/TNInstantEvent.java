package com.admitone.tmat.data;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.admitone.tmat.dao.DAORegistry;

@SuppressWarnings("serial")
@Entity
@Table(name="tninstant_event")
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class TNInstantEvent implements Serializable{

	private Integer id;
	private Event event;
	private String name;
	private Date localDate;
	private Time localTime;
//	private Venue venueBuilding;
	private Double minThreshold;
	private Double maxThreshold;
	private Integer expiryTime;
	private Double markupPercent;
	private Double salesPercent;
	private Double shippingFee;
	private Integer venueId;
	private Venue venue;
//	private Tour tour;
	
	public TNInstantEvent(Event event) {
		this.event = event;
		//this.eventName = eventName;
//		this.minThreshold = 100.00;
//		this.maxThreshold = 20000.00;
//		this.expiryTime = 60;
//		this.markupPercent=20.00;
//		this.salesPercent=5.0;
//		this.shippingFee=5.0;
	}
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@OneToOne
	@JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	
	public TNInstantEvent() {}

	
	@Transient
	public Integer getEventId() {
		return this.event.getId();
	}
	
	@Transient
	public String getName(){
		return name;
	}
	
	public void setName(String eventName) {
		this.name = eventName;
	}

	@Transient
	public Date getLocalDate() {
		return localDate;
	}
	
	public void setLocalDate(Date date) {
		this.localDate = date;
	}

	
	@Transient
	public Time getLocalTime() {
		return localTime;
	}

	public void setLocalTime(Time localTime) {
		this.localTime = localTime;
	}

//	@Transient
//	public Integer getTourId() {
//		return tourId;
//	}
//	
//	public void setTourId(Integer tourId) {
//		this.tourId = tourId;
//	}
	
//	@Column(name="venue_building")
//	public Venue getVenueBuilding() {
//		return venueBuilding;
//	}
//
//	public void setVenueBuilding(Venue venueBuilding) {
//		this.venueBuilding = venueBuilding;
//	}
//	
	
	@Column(name="min_threshold")
	public Double getMinThreshold() {
		return minThreshold;
	}

	public void setMinThreshold(Double minThreshold) {
		this.minThreshold = minThreshold;
	}

	@Column(name="max_threshold")
	public Double getMaxThreshold() {
		return maxThreshold;
	}

	public void setMaxThreshold(Double maxThreshold) {
		this.maxThreshold = maxThreshold;
	}

	
	@Column(name="time_expiry")
	public Integer getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Integer expiryTime) {
		this.expiryTime = expiryTime;
	}

	@Column(name="markup_percent")
	public Double getMarkupPercent() {
		return markupPercent;
	}

	public void setMarkupPercent(Double markupPercent) {
		this.markupPercent = markupPercent;
	}

	@Column(name="sales_percent")
	public Double getSalesPercent() {
		return salesPercent;
	}

	public void setSalesPercent(Double salesPercent) {
		this.salesPercent = salesPercent;
	}

	@Column(name="shipping_fee")
	public Double getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(Double shippingFee) {
		this.shippingFee = shippingFee;
	}

	/*@Transient
	public Event getEvent() {
		return DAORegistry.getEventDAO().get(id);
	}
	
	@Transient
	public TNInstantTour getInstantTour() {
		return DAORegistry.getTninstantTourDAO().get(getEvent().getTourId());
	}

	*/
	
	@Transient
	public Venue getVenue() {
		if (venueId == null) {
			return null;
		}
		
		if (venue == null) {
			venue = DAORegistry.getVenueDAO().get(venueId); 
		}
		return venue;
	}
	
	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	
	/*@OneToOne
	@JoinColumn(name="tour_id")
	public Tour getTour() {
		return tour;
	}

	public void setTour(Tour tour) {
		this.tour = tour;
	}*/
	
}
