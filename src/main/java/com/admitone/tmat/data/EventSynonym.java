package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@DataTransferObject
@Table(name="event_synonym")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class EventSynonym implements Serializable {
	private Integer id;
	private Integer eventId;
	private Integer mergedId;

	public EventSynonym(Integer id, Integer eventId, Integer mergedId) {
		this.id = id;
		this.eventId = eventId;
		this.mergedId = mergedId;
	}

	public EventSynonym() {	}

	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="merged_id")
	public Integer getMergedId() {
		return mergedId;
	}
	
	public void setMergedId(Integer mergedId) {
		this.mergedId = mergedId;
	}

	
	@Transient
	public Event getEvent(){
		return DAORegistry.getEventDAO().get(eventId);
	}
	
	public String toString() {
		
		String thisObject = "EventSynonym[ "
			+ "id:" + id
			+ " eventId: " + eventId
			+ " mergedId: " + mergedId + "]";
	
		return thisObject;
	}

}
