package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ebay_inventory_tour")
public class EbayInventoryTour implements Serializable {
	private Integer tourId;
	private String notes;
	private Integer exposure;
	private Boolean enableZoneMaps;
	
	public EbayInventoryTour(Integer tourId, String notes) {
		this.tourId = tourId;
		this.notes = notes;
		this.exposure = 3;
		this.enableZoneMaps = false;
	}
	
	public EbayInventoryTour() {}
	
	@Id
	@Column(name="tour_id")
	public Integer getTourId() {
		return tourId;
	}
	
	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}

	@Column(name="notes")
	public String getNotes() {
		return notes;
	}
	
	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name="exposure")
	public Integer getExposure() {
		return exposure;
	}

	public void setExposure(Integer exposure) {
		this.exposure = exposure;
	}	

	@Column(name="enable_zone_maps")
	public Boolean getEnableZoneMaps() {
		return enableZoneMaps;
	}

	public void setEnableZoneMaps(Boolean enableZoneMaps) {
		this.enableZoneMaps = enableZoneMaps;
	}
}