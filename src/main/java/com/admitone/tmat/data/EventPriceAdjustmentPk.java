package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Event Price Adjustment Primary Key Class.
 */
@Embeddable
public class EventPriceAdjustmentPk implements Serializable {
	private static final long serialVersionUID = 1085777955297911709L;
	private int eventId;
	private String siteId;
	
	public EventPriceAdjustmentPk() {}
	
	public EventPriceAdjustmentPk(int eventId, String siteId) {
		super();
		this.eventId = eventId;
		this.siteId = siteId;
	}

	@Column(name="event_id")
	public int getEventId() {
		return eventId;
	}
	
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	@Column(name="site_id")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	
}
