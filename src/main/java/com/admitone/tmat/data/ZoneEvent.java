package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.pojo.EventDTO;

@Entity
@Table(name="zone_event")

public final class ZoneEvent implements Serializable{
	
	private Integer id;
	private Integer eventId;
	private String eventName;
	private Date eventDate;
	private Date eventTime;
	private String eventStatus;
	private Integer venueId;
	private String venueName;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private Integer admitoneId;
	private Integer venueCategoryId;
	private String venueCategoryName;
	private Integer noOfCrawls;
	private Integer artistId;
	private String artistName;
	private Integer grandChildCategoryId;
	private String grandChildCategoryName;
	private Integer childCategoryId;
	private String childCategoryName;
	private Integer parentCategoryId;
	private String parentCategoryName;
	private Date creationDate;
	private Date updateDate;
	private String process;
	
	public ZoneEvent(){
		
	}
     
	public ZoneEvent(EventDTO eventDTO){
		setEventId(eventDTO.getId());
		setEventName(eventDTO.getName());
		setEventDate(eventDTO.getEventDate());
		setEventTime(eventDTO.getEventTime());
		setEventStatus(eventDTO.getEventStatus());
		setVenueId(eventDTO.getVenueId());
		setVenueName(eventDTO.getVenueName());
		setCity(eventDTO.getCity());
		setState(eventDTO.getState());
		setCountry(eventDTO.getCountry());
		setVenueCategoryId(eventDTO.getVenueCategoryId());
		setVenueCategoryName(eventDTO.getVenueCategoryName());
		setNoOfCrawls(eventDTO.getNoOfCrawls());
		setAdmitoneId(eventDTO.getAdmitoneId());
		setArtistId(eventDTO.getArtistId());
		setArtistName(eventDTO.getArtistName());
		setGrandChildCategoryId(eventDTO.getGrandChildCategoryId());
		setGrandChildCategoryName(eventDTO.getGrandChildCategoryName());
		setChildCategoryId(eventDTO.getChildCategoryId());
		setChildCategoryName(eventDTO.getChildCategoryName());
		setParentCategoryId(eventDTO.getParentCategoryId());
		setParentCategoryName(eventDTO.getParentCategoryName());
		setCreationDate(new Date());
		setUpdateDate(new Date());
		
	}
	public void setPropertyValues(EventDTO eventDTO){
		setEventId(eventDTO.getId());
		setEventName(eventDTO.getName());
		setEventDate(eventDTO.getEventDate());
		setEventTime(eventDTO.getEventTime());
		setEventStatus(eventDTO.getEventStatus());
		setVenueId(eventDTO.getVenueId());
		setVenueName(eventDTO.getVenueName());
		setCity(eventDTO.getCity());
		setState(eventDTO.getState());
		setCountry(eventDTO.getCountry());
		setVenueCategoryId(eventDTO.getVenueCategoryId());
		setVenueCategoryName(eventDTO.getVenueCategoryName());
		setNoOfCrawls(eventDTO.getNoOfCrawls());
		setAdmitoneId(eventDTO.getAdmitoneId());
		setArtistId(eventDTO.getArtistId());
		setArtistName(eventDTO.getArtistName());
		setGrandChildCategoryId(eventDTO.getGrandChildCategoryId());
		setGrandChildCategoryName(eventDTO.getGrandChildCategoryName());
		setChildCategoryId(eventDTO.getChildCategoryId());
		setChildCategoryName(eventDTO.getChildCategoryName());
		setParentCategoryId(eventDTO.getParentCategoryId());
		setParentCategoryName(eventDTO.getParentCategoryName());
		setCreationDate(new Date());
		setUpdateDate(new Date());
	}
	
    @Id
   	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date", columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_time", columnDefinition="DATE")
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="event_status")
	public String getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="venue_city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="venue_state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="venue_country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="admitone_id")
	public Integer getAdmitoneId() {
		return admitoneId;
	}
	public void setAdmitoneId(Integer admitoneId) {
		this.admitoneId = admitoneId;
	}
	
	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	
	@Column(name="venue_category_name")
	public String getVenueCategoryName() {
		return venueCategoryName;
	}
	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}
	
	@Column(name="no_of_crawls")
	public Integer getNoOfCrawls() {
		return noOfCrawls;
	}
	public void setNoOfCrawls(Integer noOfCrawls) {
		this.noOfCrawls = noOfCrawls;
	}
	
	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
    
	@Column(name="artist_name")
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	@Column(name="grand_child_category_id")
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}
	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}
	
	@Column(name="grand_child_category_name")
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}
	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}
	
	@Column(name="child_category_id")
	public Integer getChildCategoryId() {
		return childCategoryId;
	}
	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}
	
	@Column(name="child_category_name")
	public String getChildCategoryName() {
		return childCategoryName;
	}
	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	
	@Column(name="parent_category_id")
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	
	@Column(name="parent_category_name")
	public String getParentCategoryName() {
		return parentCategoryName;
	}
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="last_update")
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	@Transient
	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}
	@Column(name="postal_code")
	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	
	
	
	
	
}