package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import com.admitone.tmat.enums.BookmarkType;

@Entity
@Table(name="bookmark")
@IdClass(value=BookmarkPk.class)
@DiscriminatorColumn(name="type")
public class Bookmark implements Serializable {
	private String username;
	private BookmarkType type;
	//private String id; 
	protected Integer id;
	protected Integer outlier;
	protected Integer crawlFrequencyBefore;
	
	
	@Id
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Id
	@Column(name="type")
	@Enumerated(EnumType.STRING)
	public BookmarkType getType() {
		return type;
	}
	
	public void setType(BookmarkType type) {
		this.type = type;
	}
	
	@Id
	@Column(name="object_id")
/*	public String getObjectId() {
		return id;
	}
	
	public void setObjectId(String id) {
		this.id = id; */
	
	public Integer getObjectId() {
		return id;
	}
	
	public void setObjectId(Integer id) {
		this.id = id; 
	}

	@Column(name="outlier")
	public Integer getOutlier() {
		return outlier;
	}

	public void setOutlier(Integer outlier) {
		this.outlier = outlier;
	}

	@Column(name="auto_frequency_before")
	public Integer getCrawlFrequencyBefore() {
		return crawlFrequencyBefore;
	}

	public void setCrawlFrequencyBefore(Integer crawlFrequencyBefore) {
		this.crawlFrequencyBefore = crawlFrequencyBefore;
	}
	
	
	
}