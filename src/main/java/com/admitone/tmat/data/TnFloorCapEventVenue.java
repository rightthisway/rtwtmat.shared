package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OptimisticLockType;


@Entity
@Table(name="tn_exchange_event_venue")
@org.hibernate.annotations.Entity(
		dynamicUpdate = true,optimisticLock=OptimisticLockType.NONE)
	@DataTransferObject
	@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class TnFloorCapEventVenue implements Serializable {
	private Integer id; 
	private String venue;
	private Integer eventVenueId; 
	
    @Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="venue")
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	@Column(name="venue_id")
	public Integer getEventVenueId() {
		return eventVenueId;
	}
	public void setEventVenueId(Integer eventVenueId) {
		this.eventVenueId = eventVenueId;
	}
	

	
}
