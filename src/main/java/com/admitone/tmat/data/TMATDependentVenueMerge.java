package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tmat_dependent_venue_merge")
public class TMATDependentVenueMerge implements Serializable{
	private Integer id;
	private Integer venueId;
	private Integer toMergedVenueId;
	private String dependent;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="to_merged_venue_id")
	public Integer getToMergedVenueId() {
		return toMergedVenueId;
	}
	public void setToMergedVenueId(Integer toMergedVenueId) {
		this.toMergedVenueId = toMergedVenueId;
	}
	
	@Column(name="dependent")
	public String getDependent() {
		return dependent;
	}
	public void setDependent(String dependent) {
		this.dependent = dependent;
	}

}
