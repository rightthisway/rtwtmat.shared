package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ebay_inventory")
public class EbayInventoryOld implements Serializable {
	private Integer ticketId;
	private Integer eventId;
	private String username;
	
	private Double currentPrice;
	private Double adjustedCurrentPrice;
	private Double rpt;
	private Double binPrice;
	private Double netProfit;
	private Double xp2;
	private Double xp3;
	
	@Id
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="current_price")
	public Double getCurrentPrice() {
		return currentPrice;
	}
	
	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = currentPrice;
	}
	
	@Column(name="adjusted_current_price")
	public Double getAdjustedCurrentPrice() {
		return adjustedCurrentPrice;
	}
	
	public void setAdjustedCurrentPrice(Double adjustedCurrentPrice) {
		this.adjustedCurrentPrice = adjustedCurrentPrice;
	}

	@Column(name="rpt")
	public Double getRpt() {
		return rpt;
	}
	
	public void setRpt(Double rpt) {
		this.rpt = rpt;
	}
	
	@Column(name="bin_price")
	public Double getBinPrice() {
		return binPrice;
	}
	
	public void setBinPrice(Double binPrice) {
		this.binPrice = binPrice;
	}
	
	@Column(name="net_profit")
	public Double getNetProfit() {
		return netProfit;
	}
	
	public void setNetProfit(Double netProfit) {
		this.netProfit = netProfit;
	}
	
	@Column(name="xp2")
	public Double getXp2() {
		return xp2;
	}
	
	public void setXp2(Double xp2) {
		this.xp2 = xp2;
	}
	
	@Column(name="xp3")
	public Double getXp3() {
		return xp3;
	}
	
	public void setXp3(Double xp3) {
		this.xp3 = xp3;
	}	
}
