package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="crawler_history_management")

public class CrawlerHistoryManagement implements Serializable{
 private Integer id;
 private Date successfulHit ;
 private String siteId;
 private Integer crawlId;
 private Integer counter;
 private String system;
 
@Id
@Column(name="id")
@GeneratedValue(strategy=GenerationType.AUTO)
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}

@Column(name="successful_hit")
public Date getSuccessfulHit() {
	return successfulHit;
}
public void setSuccessfulHit(Date successfulHit) {
	this.successfulHit = successfulHit;
}

@Column(name="site_id")
public String getSiteId() {
	return siteId;
}
public void setSiteId(String siteId) {
	this.siteId = siteId;
}

@Column(name="crawl_id")
public Integer getCrawlId() {
	return crawlId;
}
public void setCrawlId(Integer crawlId) {
	this.crawlId = crawlId;
}

@Column(name="counter")
public Integer getCounter() {
	return counter;
}
public void setCounter(Integer counter) {
	this.counter = counter;
}

@Column(name="system")
public String getSystem() {
	return system;
}
public void setSystem(String system) {
	this.system = system;
}
 

}
