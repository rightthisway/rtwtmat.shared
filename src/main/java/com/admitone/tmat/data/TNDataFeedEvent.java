package com.admitone.tmat.data;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.enums.TourType;


@Entity
@Table(name="tn_data_feed_event")
public class TNDataFeedEvent implements Serializable{
	
	public Integer id;
	public Integer exchangePerformerId;
	public String exchangePerformerName;
	public Integer exchangeEventId;
	public String exchangeEventName;
	public TourType exchangeEventType;
	public Date exchangeEventDate;
	public Time exchangeEventTime;
	public Integer parentCategoryId;
	public String parentCategory;
	public Integer childCategoryId;
	public String childCategory;
	public Integer grandChildCategoryId;
	public String grandChildCategory;
	public Integer exchangeVenueId;
	public String exchangeVenueName;
	public String city;
	public String state;
	public String country;
	public String zipcode;
	public String status;
	public java.util.Date lastUpdateDate;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	/*@Column(name="exchange_tour_id")
	public Integer getExchangeTourId() {
		return exchangeTourId;
	}
	public void setExchangeTourId(Integer exchangeTourId) {
		this.exchangeTourId = exchangeTourId;
	}
	
	@Column(name="exchange_tour_name")
	public String getExchangeTourName() {
		return exchangeTourName;
	}
	public void setExchangeTourName(String exchangeTourName) {
		this.exchangeTourName = exchangeTourName;
	}*/
	
	@Column(name="exchange_performer_id")
	public Integer getExchangePerformerId() {
		return exchangePerformerId;
	}
	public void setExchangePerformerId(Integer exchangePerformerId) {
		this.exchangePerformerId = exchangePerformerId;
	}
	
	@Column(name="exchange_performer_name")
	public String getExchangePerformerName() {
		return exchangePerformerName;
	}
	public void setExchangePerformerName(String exchangePerformerName) {
		this.exchangePerformerName = exchangePerformerName;
	}
	
	@Column(name="exchange_event_id")
	public Integer getExchangeEventId() {
		return exchangeEventId;
	}
	public void setExchangeEventId(Integer exchangeEventId) {
		this.exchangeEventId = exchangeEventId;
	}
	
	@Column(name="exchange_event_name")
	public String getExchangeEventName() {
		return exchangeEventName;
	}
	public void setExchangeEventName(String exchangeEventName) {
		this.exchangeEventName = exchangeEventName;
	}
	
	@Column(name="exchange_event_date")
	public Date getExchangeEventDate() {
		return exchangeEventDate;
	}
	public void setExchangeEventDate(Date exchangeEventDate) {
		this.exchangeEventDate = exchangeEventDate;
	}
	
	@Column(name="exchange_event_time")
	public Time getExchangeEventTime() {
		return exchangeEventTime;
	}
	public void setExchangeEventTime(Time exchangeEventTime) {
		this.exchangeEventTime = exchangeEventTime;
	}
	
	@Column(name="exchange_venue_id")
	public Integer getExchangeVenueId() {
		return exchangeVenueId;
	}
	public void setExchangeVenueId(Integer exchangeVenueId) {
		this.exchangeVenueId = exchangeVenueId;
	}
	
	@Column(name="exchange_venue_name")
	public String getExchangeVenueName() {
		return exchangeVenueName;
	}
	public void setExchangeVenueName(String exchangeVenueName) {
		this.exchangeVenueName = exchangeVenueName;
	}
	
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="postal_code")
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	@Column(name="last_update_date")
	public java.util.Date getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(java.util.Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	
	@Column(name="parent_category_id")
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	
	@Column(name="parent_category_name")
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	
	@Column(name="child_category_id")
	public Integer getChildCategoryId() {
		return childCategoryId;
	}
	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}
	
	@Column(name="child_category_name")
	public String getChildCategory() {
		return childCategory;
	}
	public void setChildCategory(String childCategory) {
		this.childCategory = childCategory;
	}
	
	@Column(name="grand_child_category_id")
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}
	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}
	
	@Column(name="grand_child_category_name")
	public String getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	
	@Transient
	public TourType getExchangeEventType() {
		return exchangeEventType;
	}
	public void setExchangeEventType(TourType exchangeEventType) {
		this.exchangeEventType = exchangeEventType;
	}
	
	
	
}
