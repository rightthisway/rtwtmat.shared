package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

/**
 * Class to store seatwave event(performance).
 */

@Entity
@Table(name="seatwave_event")
public class SeatWaveEvent implements Serializable {
	
	private Integer id;
	private Integer seasonId;
	private String name;
	private String comments;
	private Date eventDate;
	private String url;
	private Integer venueId;
	private String venueName;
	private String venueTown;
	private String venueCountry;
	private String venueCountryCode;
	private Date updatedDate;
	
	public SeatWaveEvent() {
	}
	
	public SeatWaveEvent(Integer id) {
		this.id = id;
	}
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="season_id")
	public Integer getSeasonId() {
		return seasonId;
	}

	public void setSeasonId(Integer seasonId) {
		this.seasonId = seasonId;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="comments")
	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	@Column(name="url")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	@Column(name="venue_town")
	public String getVenueTown() {
		return venueTown;
	}

	public void setVenueTown(String venueTown) {
		this.venueTown = venueTown;
	}

	@Column(name="venue_country")
	public String getVenueCountry() {
		return venueCountry;
	}

	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}

	@Column(name="venue_country_code")
	public String getVenueCountryCode() {
		return venueCountryCode;
	}

	public void setVenueCountryCode(String venueCountryCode) {
		this.venueCountryCode = venueCountryCode;
	}
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Transient
	public SeatWaveSeason getSeason() {
		return DAORegistry.getSeatWaveSeasonDAO().get(seasonId);
	}
	
	public String toString() {
		return "SeatWaveEvent: " 
			+ "id=" + id
			+ ", name=" + name 
			+ ", url=" + url 
			+ ", url=" + venueName; 
	}
}
