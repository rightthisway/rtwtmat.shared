package com.admitone.tmat.data;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="tm_event_artist")
public class TicketMasterEventsArtist implements Serializable {

	protected Integer id;
	protected String eventId;
	protected String artistId;
	protected String artistName;
	protected String type;
	protected String url;
	protected Boolean isTest;
	protected String segmentId;
	protected String segmentName;
	protected String genreId;
	protected String genreName;
	protected String subGenreId;
	protected String subGenreName;
	protected String typeId;
	protected String typeName;
	protected String subTypeId;
	protected String subTypeName;
	protected Date creationDate;
	protected Date lastUpdate;
	
	
	@Transient
	public String getCompareString() {
		String str = getEventId()+":"+getArtistId()+":"+getArtistName()+":"+getType()+":"+getUrl()+":"+getIsTest()+
		getSegmentId()+":"+getSegmentName()+":"+getGenreId()+":"+getGenreName()+":"+getSubGenreId()+":"+getSubGenreName()+":"+
		getTypeId()+":"+getTypeName()+":"+getSubTypeId()+":"+getSubTypeName();
		
		return str;
	}
	
	@Transient
	public void updateObjectValues(TicketMasterEventsArtist tmEventArtist) {
		this.eventId=tmEventArtist.getEventId();
		this.artistId=tmEventArtist.getArtistId();
		this.artistName=tmEventArtist.getArtistName();
		this.type=tmEventArtist.getType();
		this.url=tmEventArtist.getUrl();
		this.isTest=tmEventArtist.getIsTest();
		this.segmentId=tmEventArtist.getSegmentId();
		this.segmentName=tmEventArtist.getSegmentName();
		this.genreId=tmEventArtist.getGenreId();
		this.genreName=tmEventArtist.getGenreName();
		this.subGenreId=tmEventArtist.getSubGenreId();
		this.subGenreName=tmEventArtist.getSubGenreName();
		this.typeId=tmEventArtist.getTypeId();
		this.typeName=tmEventArtist.getTypeName();
		this.subTypeId=tmEventArtist.getSubTypeId();
		this.subTypeName=tmEventArtist.getSubTypeName();
	}
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="artist_id")
	public String getArtistId() {
		return artistId;
	}
	public void setArtistId(String artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="artist_name")
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	@Column(name="type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(name="is_test")
	public Boolean getIsTest() {
		return isTest;
	}
	public void setIsTest(Boolean isTest) {
		this.isTest = isTest;
	}
	
	@Column(name="segment_id")
	public String getSegmentId() {
		return segmentId;
	}
	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}
	
	@Column(name="segment_name")
	public String getSegmentName() {
		return segmentName;
	}
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	
	@Column(name="genre_id")
	public String getGenreId() {
		return genreId;
	}
	public void setGenreId(String genreId) {
		this.genreId = genreId;
	}
	
	@Column(name="genre_name")
	public String getGenreName() {
		return genreName;
	}
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}
	
	@Column(name="sub_genre_id")
	public String getSubGenreId() {
		return subGenreId;
	}
	public void setSubGenreId(String subGenreId) {
		this.subGenreId = subGenreId;
	}
	
	@Column(name="sub_genre_name")
	public String getSubGenreName() {
		return subGenreName;
	}
	public void setSubGenreName(String subGenreName) {
		this.subGenreName = subGenreName;
	}
	
	@Column(name="type_id")
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	
	@Column(name="type_name")
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	@Column(name="sub_type_id")
	public String getSubTypeId() {
		return subTypeId;
	}
	public void setSubTypeId(String subTypeId) {
		this.subTypeId = subTypeId;
	}
	
	@Column(name="sub_type_name")
	public String getSubTypeName() {
		return subTypeName;
	}
	public void setSubTypeName(String subTypeName) {
		this.subTypeName = subTypeName;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
		
}
