package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

public class Stat implements Serializable {
	private Double average;
	private Double median;
	private Double variance;
	private Double deviation;
	private Double minPrice;
	private Double maxPrice;
	private Double minPrice1;
	private Double minPrice2;
	private Double marketClosed;
	private Integer minQuantity;
	private Integer maxQuantity;
	private Date date;
	private String siteId;
	private String section;
	private Integer ticketId;
	private Integer inTicketCount;
	private Integer outTicketCount;

	private Integer startTicketCount;
	private Integer endTicketCount;
	private Integer totalTicketCount;

	public Stat(Stat stat) {
		this.average = stat.average;
		this.median = stat.median;
		this.variance = stat.variance;
		this.deviation = stat.deviation;
		this.minPrice = stat.minPrice;
		this.maxPrice = stat.maxPrice;
		this.minPrice1 = stat.minPrice1;
		this.minPrice2 = stat.minPrice2;
		this.marketClosed = stat.marketClosed;
		this.minQuantity = stat.minQuantity;
		this.maxQuantity = stat.maxQuantity;
		this.date = stat.date;
		this.siteId = stat.siteId;
		this.ticketId = stat.ticketId;
		this.inTicketCount = stat.inTicketCount;
		this.outTicketCount = stat.outTicketCount;
		this.startTicketCount = stat.startTicketCount;
		this.endTicketCount = stat.endTicketCount;
		this.totalTicketCount = stat.totalTicketCount;
	}
	
	public Stat() {}
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Double getAverage() {
		return average;
	}
	public void setAverage(Double average) {
		this.average = average;
	}
	public Double getMedian() {
		return median;
	}
	public void setMedian(Double median) {
		this.median = median;
	}
	
	@Deprecated
	// either use getStartTicketCount or getEndTicketCount
	public Integer getCount() {
		return getEndTicketCount();
	}

	@Deprecated
	// either use setStartTicketCount or setEndTicketCount
	public void setCount(Integer count) {
		setEndTicketCount(count);
	}
	public Double getDeviation() {
		return deviation;
	}
	public void setDeviation(Double deviation) {
		this.deviation = deviation;
	}
	public Double getMinPrice() {
		return minPrice;
	}
	public void setMinPrice(Double minPrice) {
		this.minPrice = minPrice;
	}
	public Double getMaxPrice() {
		return maxPrice;
	}
	public void setMaxPrice(Double maxPrice) {
		this.maxPrice = maxPrice;
	}
	
	public Integer getMinQuantity() {
		return minQuantity;
	}
	public void setMinQuantity(Integer minQuantity) {
		this.minQuantity = minQuantity;
	}
	public Integer getMaxQuantity() {
		return maxQuantity;
	}
	public void setMaxQuantity(Integer maxQuantity) {
		this.maxQuantity = maxQuantity;
	}
	public Double getVariance() {
		return variance;
	}
	public void setVariance(Double variance) {
		this.variance = variance;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public Integer getInTicketCount() {
		return inTicketCount;
	}
	public void setInTicketCount(Integer inTicketCount) {
		this.inTicketCount = inTicketCount;
	}
	public Integer getOutTicketCount() {
		return outTicketCount;
	}
	public void setOutTicketCount(Integer outTicketCount) {
		this.outTicketCount = outTicketCount;
	}
	public Integer getTotalTicketCount() {
		return totalTicketCount;
	}
	public void setTotalTicketCount(Integer totalTicketCount) {
		this.totalTicketCount = totalTicketCount;
	}
	
	public Double getLiquidityRatio() {
		return (double)(outTicketCount - inTicketCount) * 100.0 / startTicketCount;
	}

	public Double getMarketPercent() {
		return (double)outTicketCount * 100.0 / startTicketCount;
	}

	public Integer getStartTicketCount() {
		return startTicketCount;
	}
	
	public void setStartTicketCount(Integer startTicketCount) {
		this.startTicketCount = startTicketCount;
	}
	
	public Integer getEndTicketCount() {
		return endTicketCount;
	}
	
	public void setEndTicketCount(Integer endTicketCount) {
		this.endTicketCount = endTicketCount;
	}

	public Double getMinPrice1() {
		return minPrice1;
	}

	public void setMinPrice1(Double minPrice1) {
		this.minPrice1 = minPrice1;
	}

	public Double getMinPrice2() {
		return minPrice2;
	}

	public void setMinPrice2(Double minPrice2) {
		this.minPrice2 = minPrice2;
	}

	public Double getMarketClosed() {
		return marketClosed;
	}

	public void setMarketClosed(Double marketClosed) {
		this.marketClosed = marketClosed;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}
	
}
