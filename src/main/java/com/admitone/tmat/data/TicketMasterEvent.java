package com.admitone.tmat.data;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="ticket_master_event")
public class TicketMasterEvent implements Serializable {

	protected Integer id;
	protected String eventId;
	protected String eventName;
	protected String type;
	protected String url;
	protected Boolean isTest;
	protected Date eventDate;
	protected Date eventTime;
	protected Date eventDateTime;
	protected String timeZone;
	protected String eventStatus;
	protected String statusCode;
	protected String segmentId;
	protected String segmentName;
	protected String genreId;
	protected String genreName;
	protected String subGenreId;
	protected String subGenreName;
	protected String typeId;
	protected String typeName;
	protected String subTypeId;
	protected String subTypeName;
	protected String venueId;
	protected String venueName;
	protected Boolean isTestVenue;
	protected String postalCode;
	protected String venueTimeZone;
	protected String city;
	protected String state;
	protected String stateCode;
	protected String country;
	protected String countryCode;
	protected String venueAddress1;
	protected String spanMultipleDays;
	protected Date creationDate;
	protected Date lastUpdate;
	
	@Transient
	public String getCompareString() {
		
		String str = getEventId()+":"+getEventName()+":"+getType()+":"+getUrl()+":"+getIsTest()+":"+getEventDate()+":"+getEventTime()+":"+getEventDateTime()+":"+getTimeZone()+":"+
		getStatusCode()+":"+getSegmentId()+":"+getSegmentName()+":"+getGenreId()+":"+getGenreName()+":"+getSubGenreId()+":"+getSubGenreName()+":"+
		getTypeId()+":"+getTypeName()+":"+getSubTypeId()+":"+getSubTypeName()+":"+getVenueId()+":"+getVenueName()+":"+getIsTestVenue()+":"+getPostalCode()+":"+getVenueTimeZone()+":"+
		getCity()+":"+getState()+":"+getStateCode()+":"+getCountry()+":"+getCountryCode()+":"+getVenueAddress1()+":"+getSpanMultipleDays()+":"+getEventStatus();
		
		return str;
	}
	
	@Transient
	public void updateObjectValues(TicketMasterEvent tmEvent) {
		this.eventId=tmEvent.getEventId();
		this.eventName=tmEvent.getEventName();
		this.type=tmEvent.getType();
		this.url=tmEvent.getUrl();
		this.isTest=tmEvent.getIsTest();
		this.eventDate=tmEvent.getEventDate();
		this.eventTime=tmEvent.getEventTime();
		this.eventDateTime=tmEvent.getEventDateTime();
		this.timeZone=tmEvent.getTimeZone();
		this.statusCode=tmEvent.getStatusCode();
		this.segmentId=tmEvent.getSegmentId();
		this.segmentName=tmEvent.getSegmentName();
		this.genreId=tmEvent.getGenreId();
		this.genreName=tmEvent.getGenreName();
		this.subGenreId=tmEvent.getSubGenreId();
		this.subGenreName=tmEvent.getSubGenreName();
		this.typeId=tmEvent.getTypeId();
		this.typeName=tmEvent.getTypeName();
		this.subTypeId=tmEvent.getSubTypeId();
		this.subTypeName=tmEvent.getSubTypeName();
		this.venueId=tmEvent.getVenueId();
		this.venueName=tmEvent.getVenueName();
		this.isTestVenue=tmEvent.getIsTestVenue();
		this.postalCode=tmEvent.getPostalCode();
		this.venueTimeZone=tmEvent.getVenueTimeZone();
		this.city=tmEvent.getCity();
		this.state=tmEvent.getState();
		this.stateCode=tmEvent.getStateCode();
		this.country=tmEvent.getCountry();
		this.countryCode=tmEvent.getCountryCode();
		this.venueAddress1=tmEvent.getVenueAddress1();
		this.spanMultipleDays=tmEvent.getSpanMultipleDays();
		this.eventStatus=tmEvent.getEventStatus();
	}
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="type")
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Column(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(name="is_test")
	public Boolean getIsTest() {
		return isTest;
	}
	public void setIsTest(Boolean isTest) {
		this.isTest = isTest;
	}
	
	@Column(name="event_date", columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_time")
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="event_date_time")
	public Date getEventDateTime() {
		return eventDateTime;
	}
	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	
	@Column(name="time_zone")
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	
	@Column(name="event_status")
	public String getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	
	@Column(name="status_code")
	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
	
	@Column(name="segment_id")
	public String getSegmentId() {
		return segmentId;
	}
	public void setSegmentId(String segmentId) {
		this.segmentId = segmentId;
	}
	
	@Column(name="segment_name")
	public String getSegmentName() {
		return segmentName;
	}
	public void setSegmentName(String segmentName) {
		this.segmentName = segmentName;
	}
	
	@Column(name="genre_id")
	public String getGenreId() {
		return genreId;
	}
	public void setGenreId(String genreId) {
		this.genreId = genreId;
	}
	
	@Column(name="genre_name")
	public String getGenreName() {
		return genreName;
	}
	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}
	
	@Column(name="sub_genre_id")
	public String getSubGenreId() {
		return subGenreId;
	}
	public void setSubGenreId(String subGenreId) {
		this.subGenreId = subGenreId;
	}
	
	@Column(name="sub_genre_name")
	public String getSubGenreName() {
		return subGenreName;
	}
	public void setSubGenreName(String subGenreName) {
		this.subGenreName = subGenreName;
	}
	
	@Column(name="type_id")
	public String getTypeId() {
		return typeId;
	}
	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
	
	@Column(name="type_name")
	public String getTypeName() {
		return typeName;
	}
	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}
	
	@Column(name="sub_type_id")
	public String getSubTypeId() {
		return subTypeId;
	}
	public void setSubTypeId(String subTypeId) {
		this.subTypeId = subTypeId;
	}
	
	@Column(name="sub_type_name")
	public String getSubTypeName() {
		return subTypeName;
	}
	public void setSubTypeName(String subTypeName) {
		this.subTypeName = subTypeName;
	}
	
	@Column(name="venue_id")
	public String getVenueId() {
		return venueId;
	}
	public void setVenueId(String venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="is_test_venue")
	public Boolean getIsTestVenue() {
		return isTestVenue;
	}
	public void setIsTestVenue(Boolean isTestVenue) {
		this.isTestVenue = isTestVenue;
	}
	
	@Column(name="postal_code")
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	@Column(name="venue_time_zone")
	public String getVenueTimeZone() {
		return venueTimeZone;
	}
	public void setVenueTimeZone(String venueTimeZone) {
		this.venueTimeZone = venueTimeZone;
	}
	
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	
	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	
	@Column(name="state_code")
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="country_code")
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	
	@Column(name="venue_address1")
	public String getVenueAddress1() {
		return venueAddress1;
	}
	public void setVenueAddress1(String venueAddress1) {
		this.venueAddress1 = venueAddress1;
	}
	
	@Column(name="span_multiple_days")
	public String getSpanMultipleDays() {
		return spanMultipleDays;
	}
	public void setSpanMultipleDays(String spanMultipleDays) {
		this.spanMultipleDays = spanMultipleDays;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	
}
