package com.admitone.tmat.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="auto_vividseats_ticket_inventory")
public class VividSeatsTicketInventory implements Serializable{

	protected static DateFormat dateTimeFormat = new SimpleDateFormat("MM/dd/yyyy hh:mmaa");
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="vendorID")
	private Integer vendorID;
	
	@Column(name="eventId")
	private Integer eventId;
	
	@Column(name="eventName")
	private String eventName;
	
	@Column(name="venueName")
	private String venueName;
	
	@Column(name="eventDate")
	private String eventDate;
	
	@Column(name="eventTime")
	private String eventTime;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="section")
	private String section;
	
	@Column(name="row")
	private String row;
	
	@Column(name="lowSeat")
	private String lowSeat;
	
	@Column(name="publicNotes")
	private String publicNotes;	
	
	@Column(name="internalNotes")
	private String internalNotes;	
	
	@Column(name="groupCost")
	private Double groupCost;
	
	@Column(name="unitListPrice")
	private Double unitListPrice;	
	
	@Column(name="expectedValue")
	private String expectedValue;
	
	@Column(name="stockType")
	private String stockType;	
	
	@Column(name="electronicTransfer")
	private String electronicTransfer;
	
	@Column(name="inHandDate")
	private String inHandDate;
	
	@Column(name="broadcast")
	private String broadcast;
	
	@Column(name="hideSeatNumbers")
	private String hideSeatNumbers;	
	
	@Column(name="splitType")
	private String splitType;
	
	@Column(name="splitValue")
	private String splitValue;	
	
	@Column(name="seatType")
	private String seatType;
	
	@Column(name="zoneListing")
	private String zoneListing;	
	
	@Column(name="tags")
	private String tags;
	
	@Column(name="created_date")
	private Date createdDate;
	
	@Column(name="last_updated")
	private Date lastUpdated;
	
	@Column(name="status")
	private String status;
	
	@Transient
	private Date eventDateTime;
	
	@Transient
	private String action;
	
	
	@Transient
	private String artistName;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVendorID() {
		return vendorID;
	}

	public void setVendorID(Integer vendorID) {
		this.vendorID = vendorID;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public String getEventDate() {
		return eventDate;
	}

	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}

	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getLowSeat() {
		return lowSeat;
	}

	public void setLowSeat(String lowSeat) {
		this.lowSeat = lowSeat;
	}

	public String getPublicNotes() {
		return publicNotes;
	}

	public void setPublicNotes(String publicNotes) {
		this.publicNotes = publicNotes;
	}

	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	public Double getGroupCost() {
		return groupCost;
	}

	public void setGroupCost(Double groupCost) {
		this.groupCost = groupCost;
	}

	public Double getUnitListPrice() {
		return unitListPrice;
	}

	public void setUnitListPrice(Double unitListPrice) {
		this.unitListPrice = unitListPrice;
	}

	public String getExpectedValue() {
		return expectedValue;
	}

	public void setExpectedValue(String expectedValue) {
		this.expectedValue = expectedValue;
	}

	public String getStockType() {
		return stockType;
	}

	public void setStockType(String stockType) {
		this.stockType = stockType;
	}

	public String getElectronicTransfer() {
		return electronicTransfer;
	}

	public void setElectronicTransfer(String electronicTransfer) {
		this.electronicTransfer = electronicTransfer;
	}

	public String getInHandDate() {
		return inHandDate;
	}

	public void setInHandDate(String inHandDate) {
		this.inHandDate = inHandDate;
	}

	public String getBroadcast() {
		return broadcast;
	}

	public void setBroadcast(String broadcast) {
		this.broadcast = broadcast;
	}

	public String getHideSeatNumbers() {
		return hideSeatNumbers;
	}

	public void setHideSeatNumbers(String hideSeatNumbers) {
		this.hideSeatNumbers = hideSeatNumbers;
	}

	public String getSplitType() {
		return splitType;
	}

	public void setSplitType(String splitType) {
		this.splitType = splitType;
	}

	public String getSplitValue() {
		return splitValue;
	}

	public void setSplitValue(String splitValue) {
		this.splitValue = splitValue;
	}

	public String getSeatType() {
		return seatType;
	}

	public void setSeatType(String seatType) {
		this.seatType = seatType;
	}

	public String getZoneListing() {
		return zoneListing;
	}

	public void setZoneListing(String zoneListing) {
		this.zoneListing = zoneListing;
	}

	public String getTags() {
		return tags;
	}

	public void setTags(String tags) {
		this.tags = tags;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getEventDateTime() {
		String tempDate = eventDate;
		if(eventTime.equals("TBD")) {
			tempDate = tempDate + " 00:05AM";
		}else {
			tempDate = tempDate + " "+eventTime;
		}
		//System.out.println("Event Date: "+eventDate+", Event Time: "+eventTime+", EventDateTime: "+tempDate);
		try {
			eventDateTime = dateTimeFormat.parse(tempDate);
		}catch(Exception e) {
			try {
				eventDateTime = dateFormat.parse(eventDate);
			}catch(Exception e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		}
		return eventDateTime;
	} 

	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	 
	
	public static void main(String[] args) {
		System.out.println(dateTimeFormat.format(new Date()));
	}

	public String getAction() {
		if(null == action) {
			action ="ADD";
		}
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	
	
}
