package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="quote_manual_ticket")
public class QuoteManualTicket implements Serializable {
	private Integer id;
	private QuoteCustomer customer;
	private Quote quote;
	private java.util.Date date;
	private Double markedPrice;
	private Double price;
	private Integer qty;
	private String section;
	private String row;
	private Event event;
	private String creator;
	
	
	public QuoteManualTicket(Event event,java.sql.Date date,QuoteCustomer customer,Quote quote) {
		this.event = event;
		this.date = date;
		this.customer = customer;
		this.quote = quote;
	}
	
	public QuoteManualTicket() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="quote_date")
	public java.util.Date getDate() {
		return date;
	}
	
	public void setDate(java.util.Date date) {
		this.date = date;
	}
	

	@Column(name="marked_price")
	public Double getMarkedPrice() {
		return markedPrice;
	}
	public void setMarkedPrice(Double markedPrice) {
		this.markedPrice = markedPrice;
	}
	
	
	
	
	@Column(name="qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}

	@Column(name="price")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Column(name="section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name="row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}
	
	
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id")
	public QuoteCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(QuoteCustomer customer) {
		this.customer = customer;
	}
	
	
	
	
	@Column(name="creator")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="quote_id")
	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	
	
	
}
