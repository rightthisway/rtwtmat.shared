package com.admitone.tmat.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@Table(name="tg_cats_category_event")
public class TNTGCatsCategoryEvent implements Serializable {
	private Integer id;
	private com.admitone.tmat.data.Event event;
	private Double markup;
	private Date expectedArrivalDate;
	private Integer shippingMethod;
	private Integer nearTermDisplayOption;
	private Boolean frequentEvent; 
	private Integer eventId;
	private String status;
	private static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Transient
	public com.admitone.tmat.data.Event getEvent() {
		if(null != eventId) {
			return DAORegistry.getEventDAO().get(eventId);
		} 
		return null;
	}
	
	@Column(name="markup")
	public Double getMarkup() {
		return markup;
	}
	public void setMarkup(Double markup) {
		this.markup = markup;
	}
	
	@Column(name="expected_arrival_date")
	public Date getExpectedArrivalDate(){
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}
	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	@Column(name="near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}
	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Transient
	public String getStringExpectedArrivalDate() {
		if(expectedArrivalDate ==null){
			return null;
		}
//		return stringExpectedArrivalDate;
		return dateFormat.format(expectedArrivalDate);
	}
	
	/*@Transient
	public int compareTo(TNTGCatsCategoryEvent obj) {
		return this.getEvent().getId()- obj.getEvent().getId();
	}*/
	
	@Column(name="is_frequent")
	public Boolean getFrequentEvent() {
		return frequentEvent;
	}
	public void setFrequentEvent(Boolean frequentEvent) {
		this.frequentEvent = frequentEvent;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
