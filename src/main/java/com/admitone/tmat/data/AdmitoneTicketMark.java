package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;

import com.admitone.tmat.enums.AdmitoneTicketMarkType;

@Entity
@Table(name="admitone_ticket_mark")
public class AdmitoneTicketMark implements Serializable {
	private Integer eventId;
	private Integer ticketId;
	private AdmitoneTicketMarkType type;
	
	public AdmitoneTicketMark() {}
	
	public AdmitoneTicketMark(Integer eventId, Integer ticketId, AdmitoneTicketMarkType type) {
		this.eventId = eventId;
		this.ticketId = ticketId;
		this.type = type;
	}

	@Id
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="admitone_type")
	@Enumerated(EnumType.ORDINAL)
	public AdmitoneTicketMarkType getType() {
		return type;
	}
	
	public void setType(AdmitoneTicketMarkType type) {
		this.type = type;
	}
}