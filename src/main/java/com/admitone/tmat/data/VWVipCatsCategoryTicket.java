package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="vw_vipcat_category_tickets")
public class VWVipCatsCategoryTicket implements Serializable {
   private Integer id;
   private String eventName;
   private String eventDate;
   private String eventTime;
   private String venueName;
   private Integer quantity;
   private String section;
   private String alternateRow;
   private Double actualPrice;
   private Double tnPrice;
   private Double vividPrice;
   private Double tickPickPrice;
   private Double scoreBigPrice;
   private String expectedArrivalDate;
   private Integer shippingMethodId;
   private Integer nearTermDisplayOptionId;
   private Integer eventId;
   private Integer tnExchangeEventId;
   private Long tnCategoryTicketGroupId;
   private Integer categoryId;
   private Date createdDate;
   private String status;
   private String eventType;
   @Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="Section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	@Column(name="Qty")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="ActualPrice")
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualPrice) {
		this.actualPrice = actualPrice;
	}
	
	@Column(name="TNPrice")
	public Double getTnPrice() {
		return tnPrice;
	}
	public void setTnPrice(Double tnPrice) {
		this.tnPrice = tnPrice;
	}
	@Column(name="VividPrice")
	public Double getVividPrice() {
		return vividPrice;
	}
	public void setVividPrice(Double vividPrice) {
		this.vividPrice = vividPrice;
	}
	
	@Column(name="ExpectedArrivalDate")
	public String getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(String expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="shipping_method_id")
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	
	@Column(name="near_term_display_option_id")
	public Integer getNearTermDisplayOptionId() {
		return nearTermDisplayOptionId;
	}
	public void setNearTermDisplayOptionId(Integer nearTermDisplayOptionId) {
		this.nearTermDisplayOptionId = nearTermDisplayOptionId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="TnEXeventId")
	public Integer getTnExchangeEventId() {
		return tnExchangeEventId;
	}
	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		this.tnExchangeEventId = tnExchangeEventId;
	}
	
	@Column(name="tn_category_ticket_group_id")
	public Long getTnCategoryTicketGroupId() {
		return tnCategoryTicketGroupId;
	}
	public void setTnCategoryTicketGroupId(Long tnCategoryTicketGroupId) {
		this.tnCategoryTicketGroupId = tnCategoryTicketGroupId;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	@Column(name="EventDate")
	public String getEventDate() {
		return eventDate;
	}
	
	public void setEventDate(String date) {
		this.eventDate = date;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="EventTime")
	public String getEventTime() {
		return eventTime;
	}

	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}

	@Column(name="EventName")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="Venue")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name="eventType")
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	
	@Column(name="AlternateRow")
	public String getAlternateRow() {
		return alternateRow;
	}
	public void setAlternateRow(String alternateRow) {
		this.alternateRow = alternateRow;
	}
	
	@Column(name="tickPickPrice")
	public Double getTickPickPrice() {
		return tickPickPrice;
	}
	public void setTickPickPrice(Double tickPickPrice) {
		this.tickPickPrice = tickPickPrice;
	}
	
	@Column(name="scoreBigPrice")
	public Double getScoreBigPrice() {
		return scoreBigPrice;
	}
	public void setScoreBigPrice(Double scoreBigPrice) {
		this.scoreBigPrice = scoreBigPrice;
	}
	
}
