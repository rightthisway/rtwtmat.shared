package com.admitone.tmat.data;

import java.io.Serializable;

public class EventPK implements Serializable{
	Integer eventId;

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result=1;
		result=prime * result + ((eventId==null)?0:eventId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventPK other=(EventPK) obj;
		return this.eventId==other.eventId;
	}
}
