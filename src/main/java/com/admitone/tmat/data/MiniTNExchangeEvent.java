package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="mini_tn_exchange_event")
public class MiniTNExchangeEvent implements Serializable {
	 private Integer id;
	 private Event event;
	 private Double lowerMarkup;
	 private Double upperMarkup;
	 private Double lowerShippingFees;
	 private Double upperShippingFees;
	 private String exposure;
	 private Double rptFactor;
	 private Boolean isFrequentEvent;
	 private String status;
	 private Double priceBreakup;
	 private Integer shippingMethod;
	 private Integer nearTermDisplayOption;
	 private Boolean allowSectionRange;
	 private Integer shippingDays;
	 
	 @Transient
	 public void setAutoPricingValues(String exposure,Double rptFactor,Double priceBreakup,
				Double lMarkup,Double uMarkup,Double lShippingFees,Double uShippingFees, Boolean isFrequent,
				Integer shippingMethod,Integer nearTermDisplayOption,Boolean allowSectionRange,Integer shippingDays) {
			
		this.exposure = exposure;
		this.shippingMethod = shippingMethod;
		this.nearTermDisplayOption = nearTermDisplayOption;
		this.rptFactor = rptFactor;
		this.priceBreakup = priceBreakup;
		this.lowerMarkup = lMarkup;
		this.upperMarkup = uMarkup;
		this.lowerShippingFees = lShippingFees;
		this.upperShippingFees = uShippingFees;
		this.isFrequentEvent = isFrequent;
		this.allowSectionRange = allowSectionRange;
		this.shippingDays=shippingDays;
	}
	 
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	/*@Transient
	public int compareTo(TNTGCatsCategoryEvent obj) {
		return this.getEvent().getId()- obj.getEvent().getId();
	}*/
	
	
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	@Column(name="rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	@Column(name="exposure")
	public String getExposure() {
		return exposure;
	}
	public void setExposure(String exposure) {
		this.exposure = exposure;
	}
	@Column(name="lower_markup")
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	
	@Column(name="upper_markup")
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	
	@Column(name="lower_shipping_fees")
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	
	@Column(name="upper_shipping_fees")
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	
	@Column(name="is_frequent_event")
	public Boolean getIsFrequentEvent() {
		return isFrequentEvent;
	}
	public void setIsFrequentEvent(Boolean isFrequentEvent) {
		this.isFrequentEvent = isFrequentEvent;
	}
	
	@Column(name="price_breakup")
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}
	
	@Column(name="shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	@Column(name="near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}

	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Column(name="allow_section_range")
	public Boolean getAllowSectionRange() {
		return allowSectionRange;
	}


	public void setAllowSectionRange(Boolean allowSectionRange) {
		this.allowSectionRange = allowSectionRange;
	}
	
	@Column(name="shipping_days")
	public Integer getShippingDays() {
		return shippingDays;
	}

	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}
	
}
