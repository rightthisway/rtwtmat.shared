package com.admitone.tmat.data;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.admitone.tmat.dao.DAORegistry;

@SuppressWarnings("serial")
//@IdClass(EventPK.class)
@Entity
@Table(name="instant_event")
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class InstantEvent implements Serializable{
	private Integer id;
	private Event event;
//	private Integer eventId;
	private String name;
	private Date localDate;
	private Time localTime;
//	private Venue venueBuilding;
	private Double minThreshold;
	private Double maxThreshold;
	private Integer expiryTime;
	private Double markupPercent;
	private Double salesPercent;
	private Double shippingFee;
//	private Integer tourId;
	private Integer venueId;
	private Venue venue;
//	private Tour tour;
	

	public InstantEvent(Event event) {
		this.event = event;
		//this.eventName = eventName;
//		this.minThreshold = 100.00;
//		this.maxThreshold = 20000.00;
//		this.expiryTime = 60;
//		this.markupPercent=20.00;
//		this.salesPercent=5.0;
//		this.shippingFee=5.0;
//		this.minThreshold = minThreshold;
//		this.maxThreshold = maxThreshold;
//		this.expiryTime = expiryTime;
//		this.markupPercent=markupPercent;
//		this.salesPercent=salesPercent;
//		this.shippingFee=shippingFee;
	}
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	//	@Id
//	@Column(name="event_id")
//	public Integer getEventId() {
//		return eventId;
//	}
//
//	public void setEventId(Integer eventId) {
//		this.eventId = eventId;
//	}
//	

	
	@JoinColumn(name="event_id")
	@OneToOne(fetch=FetchType.EAGER)
	public Event getEvent() {
		return event;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	
	
	
	public InstantEvent() {}

	
//	@Transient
//	public Integer getEventId() {
//		return id;
//	}
	
@Transient
	public String getName(){
		return name;
	}
	
	public void setName(String eventName) {
		this.name = eventName;
	}

	@Transient
	public Date getLocalDate() {
		return localDate;
	}
	
	public void setLocalDate(Date date) {
		this.localDate = date;
	}

	
	@Transient
	public Time getLocalTime() {
		return localTime;
	}

	public void setLocalTime(Time localTime) {
		this.localTime = localTime;
	}

//	@Transient
//	public Integer getTourId() {
//		return tourId;
//	}
//	
//	public void setTourId(Integer tourId) {
//		this.tourId = tourId;
//	}
	
//	@Column(name="venue_building")
//	public Venue getVenueBuilding() {
//		return venueBuilding;
//	}
//
//	public void setVenueBuilding(Venue venueBuilding) {
//		this.venueBuilding = venueBuilding;
//	}
//	
	
	
	
	@Column(name="min_threshold")
	public Double getMinThreshold() {
		return minThreshold;
	}

	public void setMinThreshold(Double minThreshold) {
		this.minThreshold = minThreshold;
	}

	@Column(name="max_threshold")
	public Double getMaxThreshold() {
		return maxThreshold;
	}

	public void setMaxThreshold(Double maxThreshold) {
		this.maxThreshold = maxThreshold;
	}

	
	@Column(name="time_expiry")
	public Integer getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Integer expiryTime) {
		this.expiryTime = expiryTime;
	}

	@Column(name="markup_percent")
	public Double getMarkupPercent() {
		return markupPercent;
	}

	public void setMarkupPercent(Double markupPercent) {
		this.markupPercent = markupPercent;
	}

	@Column(name="sales_percent")
	public Double getSalesPercent() {
		return salesPercent;
	}

	public void setSalesPercent(Double salesPercent) {
		this.salesPercent = salesPercent;
	}

	@Column(name="shipping_fee")
	public Double getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(Double shippingFee) {
		this.shippingFee = shippingFee;
	}

	/*@Transient
	public Event getEvent() {
		return DAORegistry.getEventDAO().get(id);
	}
	
	@Transient
	public InstantTour getInstantTour() {
		return DAORegistry.getInstantTourDAO().get(getEvent().getTourId());
	}

	*/
	
	@Transient
	public Venue getVenue() {
		if (venueId == null) {
			return null;
		}
		
		if (venue == null) {
			venue = DAORegistry.getVenueDAO().get(venueId); 
		}
		return venue;
	}
	
	@Transient
	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	/*
	@Override
	public int hashCode() {
		final int prime = 31;
		int result=1;
		result=prime * result + ((eventId==null)?0:eventId.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventPK other=(EventPK) obj;
		return this.eventId==other.eventId;
	}

*/
	/*@OneToOne
	@JoinColumn(name="tour_id")
	public Tour getTour() {
		return tour;
	}
	public void setTour(Tour tour) {
		this.tour = tour;
	}*/
}
