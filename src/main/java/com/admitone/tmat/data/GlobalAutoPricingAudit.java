package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="global_autopricing_audit")
public class GlobalAutoPricingAudit implements Serializable {
	 private Integer id;
	 private Double lowerMarkup;
	 private Double upperMarkup;
	 private Double lowerShippingFees;
	 private Double upperShippingFees;
	 private String autoExposure;
	 private String miniExposure;
	 private String vipMiniExposure;
	 private String vipAutoExposure;
	 private Double rptFactor;
	 private Double priceBreakup;
	 private String eventTypes;
	 private Integer shippingMethod;
	 private Integer nearTermDisplayOption;
	 private String lastRowMiniExposure;
	 private String lastFiveRowMiniExposure;
	 private String zonesPricingExposure;
	 private String zonedLastRowMiniExposure;
 	 
	 private String userName;
	 private Date createdDate;
	 private String action;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="lower_markup")
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	
	@Column(name="upper_markup")
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	
	@Column(name="lower_shipping_fees")
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	
	@Column(name="upper_shipping_fees")
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	
	
	@Column(name="rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	
	@Column(name="price_breakup")
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}
	
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}

	@Column(name="auto_exposure")
	public String getAutoExposure() {
		return autoExposure;
	}


	public void setAutoExposure(String autoExposure) {
		this.autoExposure = autoExposure;
	}

	@Column(name="mini_exposure")
	public String getMiniExposure() {
		return miniExposure;
	}


	public void setMiniExposure(String miniExposure) {
		this.miniExposure = miniExposure;
	}

	@Column(name="vip_mini_exposure")
	public String getVipMiniExposure() {
		return vipMiniExposure;
	}


	public void setVipMiniExposure(String vipMiniExposure) {
		this.vipMiniExposure = vipMiniExposure;
	}

	@Column(name="vip_auto_exposure")
	public String getVipAutoExposure() {
		return vipAutoExposure;
	}

	public void setVipAutoExposure(String vipAutoExposure) {
		this.vipAutoExposure = vipAutoExposure;
	}
	
	@Column(name="event_types")
	public String getEventTypes() {
		return eventTypes;
	}
	public void setEventTypes(String eventTypes) {
		this.eventTypes = eventTypes;
	}
	
	@Column(name="shipping_method")
	public Integer getShippingMethod() {
		return shippingMethod;
	}

	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}

	@Column(name="near_term_display_option")
	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}

	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	@Column(name="lastrow_mini_exposure")
	public String getLastRowMiniExposure() {
		return lastRowMiniExposure;
	}
	public void setLastRowMiniExposure(String lastRowMiniExposure) {
		this.lastRowMiniExposure = lastRowMiniExposure;
	}
	@Column(name="lastfiverow_mini_exposure")
	public String getLastFiveRowMiniExposure() {
		return lastFiveRowMiniExposure;
	}
	public void setLastFiveRowMiniExposure(String lastFiveRowMiniExposure) {
		this.lastFiveRowMiniExposure = lastFiveRowMiniExposure;
	}
	
	@Column(name="zones_pricing_exposure")
	public String getZonesPricingExposure() {
		return zonesPricingExposure;
	}
	public void setZonesPricingExposure(String zonesPricingExposure) {
		this.zonesPricingExposure = zonesPricingExposure;
	}
	@Column(name="zoned_lastrowmini_exposure")
	public String getZonedLastRowMiniExposure() {
		return zonedLastRowMiniExposure;
	}
	public void setZonedLastRowMiniExposure(String zonedLastRowMiniExposure) {
		this.zonedLastRowMiniExposure = zonedLastRowMiniExposure;
	}
	
	
}
