package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.utils.SectionRowStripper;


@Entity
@Table(name="short_broadcast")
public final class ShortBroadcast implements Serializable {
	private Integer id;
	private Double price;
	private Integer quantity;
	private Integer admitoneEventId;
	private String section;
	private String row;
	private Event event;
	
	public ShortBroadcast(Double price,
			Integer quantity, Integer admitoneEventId, 
			String section, String row) {
		this.price = price;
		this.quantity = quantity;
		this.admitoneEventId = admitoneEventId;
		this.section = section;
		this.row = row;
	}

	public ShortBroadcast() { }
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name="admitone_event_id")
	public Integer getAdmitoneEventId() {
		return admitoneEventId;
	}

	public void setAdmitoneEventId(Integer admitoneEventId) {
		this.admitoneEventId = admitoneEventId;
	}

	@Column(name="section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Transient
	public String getNormalizedSection(Map<String,Synonym> synonymsMap) {
		return SectionRowStripper.strip(event.getEventType(), section,synonymsMap);
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Transient
	public String getNormalizedRow() {
		return row.replaceAll("\\W", "");
	}

	@Transient
	public Event getEvent() {
		return event;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}

	@Transient
	public AdmitoneEvent getAdmitoneEvent() {
		return DAORegistry.getAdmitoneEventDAO().get(admitoneEventId);
	}
	
	@Transient
	public Collection<AdmitoneInventory> getAdmitoneInventory(){
		return DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByAdmitOneEventId(admitoneEventId);
	}
	
	
	public String toString() {
		
		String thisObject = "ShortBroadcast[ "
				+ " price: " + price
				+ " quantity: " + quantity
				+ " a1EventId: " + admitoneEventId
				+ " section: " + section
				+ " row: " + row + "]";
		
		return thisObject;
	}
}
