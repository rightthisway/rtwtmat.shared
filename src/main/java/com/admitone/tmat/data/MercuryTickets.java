package com.admitone.tmat.data;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.utils.Categorizer;


	@SuppressWarnings("serial")
	public class MercuryTickets extends Ticket{
		private Integer ticketId;
		private Integer eventId;
		private String eventName;
		private String venue;
		private Date eventDate;
		private Date eventTime;
		private Integer quantity;
		private String section;
		private String row;
		private String seat;
		private Double currentPrice;
		private TicketStatus ticketStatus;
		private TicketDeliveryType ticketDeliveryType;


		public Integer getTicketId() {
			return ticketId;
		}
		
		public void setTicketId(Integer ticketId) {
			this.ticketId = ticketId;
		}
		
		
		public Integer getEventId() {
			return eventId;
		}

		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}

		
		public String getEventName() {
			return eventName;
		}

		public void setEventName(String eventName) {
			this.eventName = eventName;
		}

		
		public String getVenue() {
			return venue;
		}

		public void setVenue(String venue) {
			this.venue = venue;
		}

		
		public Date getEventDate() {
			return eventDate;
		}

		public void setEventDate(Date eventDate) {
			this.eventDate = eventDate;
		}


		public Date getEventTime() {
			return eventTime;
		}

		public void setEventTime(Date eventTime) {
			this.eventTime = eventTime;
		}

		
		public String getSection() {
			return section;
		}

		public void setSection(String section) {
			this.section = section;
		}

		
		public String getRow() {
			return row;
		}

		public void setRow(String row) {
			this.row = row;
		}

		
		public String getSeat() {
			return seat;
		}

		public void setSeat(String seat) {
			this.seat = seat;
		}

		
		public Double getCurrentPrice() {
			return currentPrice;
		}

		public void setCurrentPrice(Double currentPrice) {
			this.currentPrice = currentPrice;
		}

		
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}

		public Integer getQuantity() {
			return quantity;
		}
		
		public TicketStatus getTicketStatus() {
			return ticketStatus;
		}

		public void setTicketStatus(TicketStatus ticketStatus) {
			this.ticketStatus = ticketStatus;
		}

		public TicketDeliveryType getTicketDeliveryType() {
			return ticketDeliveryType;
		}

		public void setTicketDeliveryType(TicketDeliveryType ticketDeliveryType) {
			this.ticketDeliveryType = ticketDeliveryType;
		}

		@Override
		public String getNormalizedSection() {
			return section;
		}	
		
		public Integer getCategoryId(VenueCategory venueCategory, String groupName,Map<Integer,Category> catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
			if(category != null && category.getGroupName().equals(groupName)){
				return category.getId();
			} else {
				category = Categorizer.computeCategory(venueCategory,this.getNormalizedSection(),this.getRow(),this.getSeat(),catMap,catMappingMap);
				if(category == null) {
					uncategorized = true;				
				}
			}
			if(category == null){
				return null;
			}
			return category.getId();
		}
		
		public void setCategory(Category category) {
			this.category = category;
			if (category == null) {
				uncategorized = true;
			}
		}
		
	
}
