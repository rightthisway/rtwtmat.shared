package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="eimp_events")
public class EIBoxEvent implements Serializable {
	private EIBoxEventPK eiBoxEventPK = new EIBoxEventPK();
	private String eventName;
	
	private String venueName;
	private String venueCity;
	
	public EIBoxEvent() {		
	}

	@Id
	public EIBoxEventPK getEiBoxEventPK() {    	
		return eiBoxEventPK;
	}

	public void setEiBoxEventPK(EIBoxEventPK eiBoxEventPK) {
		this.eiBoxEventPK = eiBoxEventPK;
	}

    @Column(name="EventID",insertable=false,updatable=false)	
	public Integer getEventId() {    	
		return eiBoxEventPK.getEventId();
	}

	public void setEventId(Integer eventId) {
		eiBoxEventPK.setEventId(eventId);
	}

    @Column(name="VenueID",insertable=false,updatable=false)	
	public Integer getVenueId() {
		return eiBoxEventPK.getVenueId();
	}

	public void setVenueId(Integer venueId) {
		eiBoxEventPK.setVenueId(venueId);
	}

    @Column(name="EventDateTime",insertable=false,updatable=false)	
	public Date getEventDate() {
		return eiBoxEventPK.getEventDate();
	}

	public void setEventDate(Date eventDate) {
		eiBoxEventPK.setEventDate(eventDate);
	}

	@Column(name="EventName")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="VenueName")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	@Column(name="City")
	public String getVenueCity() {
		return venueCity;
	}

	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	
	@Embeddable
	public static class EIBoxEventPK implements Serializable{
		private Integer eventId;
		private Integer venueId;
		private Date eventDate;		

		@Column(name="EventID")
		public Integer getEventId() {
			return eventId;
		}
		
		public void setEventId(Integer eventId) {
			this.eventId = eventId;
		}
		
		@Column(name="VenueID")
		public Integer getVenueId() {
			return venueId;
		}
		
		public void setVenueId(Integer venueId) {
			this.venueId = venueId;
		}
		
		@Column(name="EventDateTime")
		public Date getEventDate() {
			return eventDate;
		}
		
		public void setEventDate(Date eventDate) {
			this.eventDate = eventDate;
		}
	}
}
