package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="short_transaction")
public final class ShortTransaction implements Serializable {
	private Integer id;
	private Double price;
	private Integer quantity;
	private Integer coverId;
	private Integer admitoneId;
	private String section;
	private String row;
	private Integer invoice;
	private String customer;
	private Integer groupId;
	private Double selling_price;
	private Date invoiceDate;
	private Date poDate;
	private Double buyingPrice;
	
	public ShortTransaction(Double price,
			Integer quantity, Integer coverId,
			Integer admitoneId, String section, String row, Integer invoice, 
			String customer, Integer groupId) {
		this.price = price;
		this.quantity = quantity;
		this.coverId = coverId;
		this.admitoneId = admitoneId;
		this.section = section;
		this.row = row;
		this.invoice = invoice;
		this.customer = customer;
		this.groupId = groupId;
	}

	public ShortTransaction() { }
	
	public ShortTransaction(AdmitoneInventory inv){
		this.setId(inv.getTicketId());
		this.setPrice(inv.getCost());
		this.setQuantity(inv.getQuantity());
		this.setAdmitoneId(inv.getEventId());
		this.setSection(inv.getSection());
		this.setRow(inv.getRow());
		String type = inv.getInventoryType();
		if(type != null && !type.equals("REGULAR")) {
			this.setCustomer("ADMT1 - cat");
		} else {
			this.setCustomer("ADMT1");
		}
	}	
	public ShortTransaction(AdmitoneInventoryLocal inv){
		this.setId(inv.getTicketId());
		this.setPrice(inv.getCost());
		this.setQuantity(inv.getQuantity());
		this.setAdmitoneId(inv.getEventId());
		this.setSection(inv.getSection());
		this.setRow(inv.getRow());
		String type = inv.getInventoryType();
		if(type != null && !type.equals("REGULAR")) {
			this.setCustomer("ADMT1 - cat");
		} else {
			this.setCustomer("ADMT1");
		}
	}
	
	public ShortTransaction(LongTransaction longTransaction){
		this.setId(longTransaction.getId());
		this.setPrice(longTransaction.getCost());
		this.setQuantity(longTransaction.getQuantity());
		this.setAdmitoneId(longTransaction.getAdmitoneId());
		this.setSection(longTransaction.getSection());
		this.setRow(longTransaction.getRow());
		this.setCustomer("ADMT1");
		this.setInvoiceDate(longTransaction.getInvoiceDate());
		this.setPoDate(longTransaction.getPoDate());
		this.setSelling_price(longTransaction.getSelling_price());
		this.setBuyingPrice(longTransaction.getBuyingPrice());
	}		

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="price")
	public Double getPrice() {
		return price;
	}
	
	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name="cover_id")
	public Integer getCoverId() {
		return coverId;
	}
	
	public void setCoverId(Integer coverId) {
		this.coverId = coverId;
	}

	@Column(name="admitone_id")
	public Integer getAdmitoneId() {
		return admitoneId;
	}

	public void setAdmitoneId(Integer admitoneId) {
		this.admitoneId = admitoneId;
	}

	@Column(name="section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name="row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Transient
	public Integer getEventId() {
		return admitoneId;
	}

	@Column(name="invoice")
	public Integer getInvoice() {
		return invoice;
	}

	public void setInvoice(Integer invoice) {
		this.invoice = invoice;
	}

	@Column(name="customer")
	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	@Column(name="group_id")
	public Integer getGroupId() {
		return groupId;
	}

	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	public String toString() {
		
		String thisObject = "ShortTransaction[ "
				+ " price: " + price
				+ " quantity: " + quantity
				+ " coverId: " + coverId
				+ " admitoneId: " + admitoneId
				+ " section: " + section
				+ " groupId: " + groupId
				+ " row: " + row + "]";
		
		return thisObject;
	}
	@Column(name = "selling_price")
	public Double getSelling_price() {
		return selling_price;
	}

	public void setSelling_price(Double selling_price) {
		this.selling_price = selling_price;
	}
	@Column(name = "invoice_date")
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	@Transient
	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}
	@Transient
	public Double getBuyingPrice() {
		return buyingPrice;
	}

	public void setBuyingPrice(Double buyingPrice) {
		this.buyingPrice = buyingPrice;
	}
	
	
	
}
