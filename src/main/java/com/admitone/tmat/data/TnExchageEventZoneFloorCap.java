package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="tn_exchange_event_floor_cap")
public class TnExchageEventZoneFloorCap implements Serializable {
	private Integer id; 
	private AdmitoneEvent exchangeEvent; 
	private TnExchangeTheatreEventZones tnExchangeTheatreEventZones; 
	private Float cap;
	private Date createdDate;
	private Date lastupdate;
	private String dayOfWeek;
	private Integer brokerId;
	private Boolean isZonePremium;
	
	public TnExchageEventZoneFloorCap(TnExchageEventZoneFloorCap zapt){
		this.id =zapt.id;	
		this.cap=zapt.cap;
		this.brokerId=zapt.brokerId;
		this.exchangeEvent=zapt.exchangeEvent;
		this.tnExchangeTheatreEventZones=zapt.tnExchangeTheatreEventZones;
		this.lastupdate=zapt.lastupdate;
		this.createdDate=zapt.createdDate;
		this.dayOfWeek=zapt.dayOfWeek;
	}
	
	
	public TnExchageEventZoneFloorCap(){
		
	}
    @Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	
	@Column(name="zone_price_cap")
	public Float getCap() {
		return cap;
	}
	public void setCap(Float cap) {
		this.cap = cap;
	}
	
	
	
	@OneToOne
	@JoinColumn(name="event_id")
	public AdmitoneEvent getExchangeEvent() {
		return exchangeEvent;
	}

	public void setExchangeEvent(AdmitoneEvent exchangeEvent) {
		this.exchangeEvent = exchangeEvent;
	}

	@OneToOne
	@JoinColumn(name="zone_id")
	public TnExchangeTheatreEventZones getTnExchangeTheatreEventZones() {
		return tnExchangeTheatreEventZones;
	}
	public void setTnExchangeTheatreEventZones(
			TnExchangeTheatreEventZones tnExchangeTheatreEventZones) {
		this.tnExchangeTheatreEventZones = tnExchangeTheatreEventZones;
	}


	@Column(name="last_update")
	public Date getLastupdate() {
		return lastupdate;
	}
	public void setLastupdate(Date lastupdate) {
		this.lastupdate = lastupdate;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}


	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}


	@Transient
	public String getDayOfWeek() {
		return dayOfWeek;
	}

	@Transient
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	@Column(name="is_premium_zone")
	public Boolean getIsZonePremium() {
		return isZonePremium;
	}


	public void setIsZonePremium(Boolean isZonePremium) {
		this.isZonePremium = isZonePremium;
	}


	
}
