package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="tninstant_tour")
public class TNInstantTour implements Serializable {
	private Integer tourId;
	private Double minThreshold;
	private Double maxThreshold;
	private Integer expiryTime;
	private Double markupPercent;
	private Double salesPercent;
	private Double shippingFee;
	
	public TNInstantTour(Integer tourId) {
		this.tourId = tourId;
//		this.minThreshold = 100.00;
//		this.maxThreshold = 20000.00;
//		this.expiryTime = 60;
//		this.markupPercent=20.00;
//		this.salesPercent=5.00;
//		this.shippingFee=5.00;
	}
	
	public TNInstantTour() {}
	
	@Id
	@Column(name="tour_id")
	public Integer getTourId() {
		return tourId;
	}
	
	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}

	@Column(name="min_threshold")
	public Double getMinThreshold() {
		return minThreshold;
	}

	public void setMinThreshold(Double minThreshold) {
		this.minThreshold = minThreshold;
	}

	@Column(name="max_threshold")
	public Double getMaxThreshold() {
		return maxThreshold;
	}

	public void setMaxThreshold(Double maxThreshold) {
		this.maxThreshold = maxThreshold;
	}

	@Column(name="time_expiry")
	public Integer getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Integer expiryTime) {
		this.expiryTime = expiryTime;
	}

	@Column(name="markup_percent")
	public Double getMarkupPercent() {
		return markupPercent;
	}

	public void setMarkupPercent(Double markupPercent) {
		this.markupPercent = markupPercent;
	}

	@Column(name="sales_percent")
	public Double getSalesPercent() {
		return salesPercent;
	}

	public void setSalesPercent(Double salesPercent) {
		this.salesPercent = salesPercent;
	}

	@Column(name="shipping_fee")
	public Double getShippingFee() {
		return shippingFee;
	}

	public void setShippingFee(Double shippingFee) {
		this.shippingFee = shippingFee;
	}
}
