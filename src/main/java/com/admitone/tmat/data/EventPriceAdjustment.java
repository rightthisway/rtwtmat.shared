package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Event Price Adjustment Class.
 */
@Entity
@Table(name="event_price_adjustment")
@IdClass(value=EventPriceAdjustmentPk.class)
public class EventPriceAdjustment implements Serializable {
	private static final long serialVersionUID = 6663072661991502603L;
	private int eventId;
	private String siteId;
	
	private Double percentAdjustment;
	private Double shortPercent;
	private Double shortFlatFee;

	@Id
	@Column(name="event_id")
	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	@Id
	@Column(name="site_id")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	@Column(name="percentage")
	public Double getPercentAdjustment() {
		return percentAdjustment;
	}

	public void setPercentAdjustment(Double percentAdjustment) {
		this.percentAdjustment = percentAdjustment;
	}
	
	@Column(name="short_percent")
	public Double getShortPercent() {
		return shortPercent;
	}

	public void setShortPercent(Double shortPercent) {
		this.shortPercent = shortPercent;
	}
	
	@Column(name="short_flat_fee")
	public Double getShortFlatFee() {
		return shortFlatFee;
	}

	public void setShortFlatFee(Double shortFlatFee) {
		this.shortFlatFee = shortFlatFee;
	}
	
}
