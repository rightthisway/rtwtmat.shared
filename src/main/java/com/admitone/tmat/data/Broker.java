package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "broker")
public class Broker implements Serializable {

	private Integer id;
	private String name;
	private String emailId;
	private String phoneNo;
	private String status;
	private Boolean isAutopricingBroker;
	private String emailSuffix;
	private Integer ssAccountOfficeId;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "email_id")
	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	@Column(name = "phone_no")
	public String getPhoneNo() {
		return phoneNo;
	}

	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	@Column(name = "status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	@Column(name = "is_autopricing_broker")
	public Boolean getisAutopricingBroker() {
		return isAutopricingBroker;
	}
	public void setisAutopricingBroker(Boolean isAutopricingBroker) {
		this.isAutopricingBroker = isAutopricingBroker;
	}

	@Column(name = "email_suffix")
	public String getEmailSuffix() {
		return emailSuffix;
	}

	public void setEmailSuffix(String emailSuffix) {
		this.emailSuffix = emailSuffix;
	}
	@Column(name = "ss_account_office_id")
	public Integer getSsAccountOfficeId() {
		return ssAccountOfficeId;
	}

	public void setSsAccountOfficeId(Integer ssAccountOfficeId) {
		this.ssAccountOfficeId = ssAccountOfficeId;
	}

}
