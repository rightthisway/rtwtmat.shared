package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="valuation_factor")
public class ValuationFactor implements Serializable {
	private Integer id;
	private Integer eventId;
	private String startSection;
	private String endSection;
	private String startRow;
	private String endRow;
	private Integer value;
	
	public ValuationFactor(Integer eventId, String startSection, String endSection,
			String startRow, String endRow, Integer value) {
		this.eventId = eventId;
		this.startSection = startSection;
		this.endSection = endSection;
		this.startRow = startRow;
		this.endRow = endRow;
		this.value = value;
	}
	
	public ValuationFactor() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}	
	
	@Column(name="start_row")
	public String getStartRow() {
		return startRow;
	}
	
	public void setStartRow(String startRow) {
		this.startRow = startRow;
	}
	
	@Column(name="end_row")
	public String getEndRow() {
		return endRow;
	}
	
	public void setEndRow(String endRow) {
		this.endRow = endRow;
	}
	
	@Column(name="start_section")
	public String getStartSection() {
		return startSection;
	}
	
	public void setStartSection(String startSection) {
		this.startSection = startSection;
	}
	
	@Column(name="end_section")
	public String getEndSection() {
		return endSection;
	}
	
	public void setEndSection(String endSection) {
		this.endSection = endSection;
	}
	
	@Column(name="value")
	public Integer getValue() {
		return value;
	}
	
	public void setValue(Integer value) {
		this.value = value;
	}
}