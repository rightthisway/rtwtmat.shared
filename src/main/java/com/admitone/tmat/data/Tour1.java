package com.admitone.tmat.data;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name="tour")
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public final class Tour1 extends BaseTour {
	private Artist artist;

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="artist_id")
	public Artist getArtist() {
		return artist;
//		return DAORegistry.getArtistDAO().get(artistId);
	}
	
	public void setArtist(Artist artist) {
		this.artist = artist;
	}
	
	@Transient
	public Integer getArtistId() {
		return artist.getId();
	}
	
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		Tour1 tour =(Tour1)obj;
		return this.getId().equals(tour.getId());
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return this.getId();
	}

}
