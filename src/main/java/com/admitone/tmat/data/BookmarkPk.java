package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

import com.admitone.tmat.enums.BookmarkType;

@Embeddable
public class BookmarkPk implements Serializable {
	private BookmarkType type;
	private Integer objectId;
	private String username;
	
	public BookmarkPk() {}
	
	public BookmarkPk(Integer objectId, BookmarkType type, String username) {
		super();
		this.objectId = objectId;
		this.type = type;
		this.username = username;
	}



	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	@Column(name="type")
	@Enumerated(EnumType.STRING)
	public BookmarkType getType() {
		return type;
	}
	
	public void setType(BookmarkType type) {
		this.type = type;
	}
	
	@Column(name="object_id")
	public Integer getObjectId() {
		return objectId;
	}
	
	public void setObjectId(Integer objectId) {
		this.objectId = objectId;
	}
}
