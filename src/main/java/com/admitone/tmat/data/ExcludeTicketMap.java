package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;

@Entity
@Table(name="exclude_ticket_map")
@DataTransferObject
public class ExcludeTicketMap implements Serializable {
	private Integer id;
	private Integer ticketId;
	private Integer categoryId;
	private Integer eventId;
	private Integer transactionType;
	private Integer scope;
	private Integer transactionId;
	
	public final static int TYPE_SHORT = 0;
	public final static int TYPE_LONG = 1;
	
	public final static int SCOPE_EVENT = 0;
	public final static int SCOPE_CAT = 1;
	public final static int SCOPE_TX = 2;
	
	public ExcludeTicketMap() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name="transaction_id")
	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer shortOrLongId) {
		this.transactionId = shortOrLongId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}	

	@Column(name="transaction_type")
	public Integer getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(Integer transactionType) {
		this.transactionType = transactionType;
	}

	@Column(name="scope")
	public Integer getScope() {
		return scope;
	}

	public void setScope(Integer scope) {
		this.scope = scope;
	}

}