package com.admitone.tmat.data;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


@Entity
@Table(name="expired_tickets")
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public final class HarvestTicket extends BaseTicket {
}
