package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="inventory_ticket_exclusion")
public class InventoryTicketExclusion implements Serializable {
	public static final int TYPE_LONG = 0;
	public static final int TYPE_SHORT = 1;

	public static final int SCOPE_EVENT = 0;
	public static final int SCOPE_CAT = 1;
	public static final int SCOPE_TICKET = 2;

	private Integer id;
	private Integer scope;
	private Integer eventId;
	private Integer categoryId;
	private Integer type;
	private Integer entryId;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="scope")
	public Integer getScope() {
		return scope;
	}
	
	public void setScope(Integer scope) {
		this.scope = scope;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	@Column(name="inventory_type")
	public Integer getType() {
		return type;
	}
	
	public void setType(Integer type) {
		this.type = type;
	}

	@Column(name="entry_id")
	public Integer getEntryId() {
		return entryId;
	}
	
	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}
}