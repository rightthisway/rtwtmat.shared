package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@Table(name="ticket_quote")
public class TicketQuote implements Serializable {
	private Integer id;
	private Quote quote;
	private java.sql.Date date;
	private Integer ticketId;
	private Ticket ticket;
	private Double markedPrice;
	private Double zoneMarkedPrice;
	private Integer qty;
	private String fileName;
	private Integer zoneQty;
	private String zone;
	private Double zonePrice;
	private Character isManQuoteTic='N';
	public TicketQuote(Quote quote,
					   java.sql.Date date,
					   Integer ticketId) {
		this.quote = quote;
		this.date = date;
		this.ticketId = ticketId;
	}
	
	public TicketQuote() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="quote_id")
	public Quote getQuote() {
		return quote;
	}

	public void setQuote(Quote quote) {
		this.quote = quote;
	}

	@Column(name="quote_date")
	public java.sql.Date getDate() {
		return date;
	}
	
	public void setDate(java.sql.Date date) {
		this.date = date;
	}
	
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name="marked_price")
	public Double getMarkedPrice() {
		if (markedPrice == null) {
			markedPrice = getTicket().getBuyItNowPrice();
		}
		return markedPrice;
	}

	public void setMarkedPrice(Double markedPrice) {
		this.markedPrice = markedPrice;
	}
	@Column(name="zone_marked_price")
	public Double getZoneMarkedPrice() {
		
		return zoneMarkedPrice;
	}

	public void setZoneMarkedPrice(Double zoneMarkedPrice) {
		this.zoneMarkedPrice = zoneMarkedPrice;
	}
	
	@Column(name="zone_price")
	public Double getZonePrice() {		
		return zonePrice;
	}

	public void setZonePrice(Double zonePrice) {
		this.zonePrice = zonePrice;
	}

	@Transient
	public Ticket getTicket() {
		if (ticket == null) {
			ticket = DAORegistry.getTicketDAO().get(ticketId);
		}
		
		return ticket;
	}
	
	@Column(name="qty")
	public Integer getQty() {
		return qty;
	}
	public void setQty(Integer qty) {
		this.qty = qty;
	}
	
	@Column(name="file_name")
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	@Column(name="zone_qty")
	public Integer getZoneQty() {
		return zoneQty;
	}
	public void setZoneQty(Integer zoneQty) {
		this.zoneQty = zoneQty;
	}
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}

	public Character getIsManQuoteTic() {
		return isManQuoteTic;
	}

	public void setIsManQuoteTic(Character isManQuoteTic) {
		this.isManQuoteTic = isManQuoteTic;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	
}
