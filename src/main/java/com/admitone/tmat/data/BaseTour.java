package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.admitone.tmat.enums.TourStatus;
import com.admitone.tmat.enums.TourType;

@MappedSuperclass
public abstract class BaseTour implements Serializable {
	private Integer id;
	private String name;
	private String location;
	private java.util.Date startDate;
	private java.util.Date endDate;
	private TourType tourType;
	private Integer stubhubId;
	private Integer seatwaveId;
	private Integer eventCount;
	private TourStatus tourStatus;
	private GrandChildTourCategory grandChildTourCategory;	
	public BaseTour() {
		eventCount = 0;
		tourStatus = TourStatus.ACTIVE;
	}

	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="location")
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	@Column(name="start_date")
	public java.util.Date getStartDate() {
		return startDate;
	}

	public void setStartDate(java.util.Date start_date) {
		this.startDate = start_date;
	}

	@Column(name="end_date")
	public java.util.Date getEndDate() {
		return endDate;
	}

	public void setEndDate(java.util.Date end_date) {
		this.endDate = end_date;
	}

	/*@Transient
	public Collection<Category> getCategories() {
		return DAORegistry.getCategoryDAO().getAllCategories(id);
	}*/

	
	/*@Transient
	public Collection<Event> getEvents() {
		return DAORegistry.getEventDAO().getAllEventsByTour(id);
	}

	@Transient
	public Collection<Event> getOrderedEvents() {
		return DAORegistry.getEventDAO().getAllEventsByTourOrderdByEventName(id);
	}*/
	
	/*@Transient
	public Collection<Event> getActiveEvents() {
		return DAORegistry.getEventDAO().getAllEventsByTour(id, EventStatus.ACTIVE);
	}

	@Transient
	public int getCrawlCount() {
		return DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlCountByTour(id);
	}*/

	@Column(name="type")
	@Enumerated(EnumType.STRING)
	public TourType getTourType() {
		return tourType;
	}

	public void setTourType(TourType tourType) {
		this.tourType = tourType;
	}

	@Column(name="stubhub_id")
	public Integer getStubhubId() {
		return stubhubId;
	}

	public void setStubhubId(Integer stubhubId) {
		this.stubhubId = stubhubId;
	}

	@Column(name="event_count")
	public int getEventCount() {
		return eventCount;
	}
	
	public void setEventCount(Integer eventCount) {
		this.eventCount = eventCount;
	}

	@Column(name="seatwave_id")
	public Integer getSeatwaveId() {
		return seatwaveId;
	}

	public void setSeatwaveId(Integer seatwaveId) {
		this.seatwaveId = seatwaveId;
	}

	@Column(name="tour_status")
	@Enumerated(EnumType.ORDINAL)	
	public TourStatus getTourStatus() {
		return tourStatus;
	}

	public void setTourStatus(TourStatus tourStatus) {
		this.tourStatus = tourStatus;
	}
	

	/*@Transient
	public int getCategoryCount() {
		return DAORegistry.getCategoryDAO().getCategoryCount(id);
	}*/
	
	@Transient
	public String getTourLabel() {
		if (tourType.equals(TourType.SPORT)) {
			return "Season";
		}
		return "Tour";
	}

	@Transient
	public String getArtistLabel() {
		if (tourType.equals(TourType.SPORT)) {
			return "Team";
		}
		return "Artist";
	}

	@ManyToOne
	@JoinColumn(name="grand_child_category_id")
	public GrandChildTourCategory getGrandChildTourCategory() {
		return grandChildTourCategory;
	}

	public void setGrandChildTourCategory(
			GrandChildTourCategory grandChildTourCategory) {
		this.grandChildTourCategory = grandChildTourCategory;
	}

}
