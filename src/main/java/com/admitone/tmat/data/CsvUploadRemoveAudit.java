package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;

/**
 * Property Class.
 * This class is used to store properties such as smtp settings and crawler settings.
 */
@Entity
@Table(name="csv_upload_remove_audit")
@DataTransferObject
public class CsvUploadRemoveAudit implements Serializable {
	private static final long serialVersionUID = 1957532231871350178L;
	private Integer id;
	private String username;
	private String action;
	private String building;
	private String zonesgroup;
	private Date createdDate;
	private Integer linecount;
	private Integer initialLineCount;

	public CsvUploadRemoveAudit(String username, String action) {
		this.username = username;
		this.action = action;
	}
	
	public CsvUploadRemoveAudit() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="username")
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	@Column(name="action")
	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}
	@Column(name="venue_name")
	public String getBuilding() {
		return building;
	}

	public void setBuilding(String building) {
		this.building = building;
	}
	@Column(name="zonesgroup")
	public String getZonesgroup() {
		return zonesgroup;
	}

	public void setZonesgroup(String zonesgroup) {
		this.zonesgroup = zonesgroup;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="line_count")
	public Integer getLinecount() {
		return linecount;
	}

	public void setLinecount(Integer linecount) {
		this.linecount = linecount;
	}

	@Column(name="initial_line_count")
	public Integer getInitialLineCount() {
		return initialLineCount;
	}

	public void setInitialLineCount(Integer initialLineCount) {
		this.initialLineCount = initialLineCount;
	}
}
