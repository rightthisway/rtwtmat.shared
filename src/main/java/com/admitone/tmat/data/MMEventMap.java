package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@Table(name="mm_event_map")
public class MMEventMap implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String username;
	private Integer eventId;
	private Double coverage;
	private java.util.Date startDate;
	private java.util.Date endDate;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	@Column(name="coverage")
	public Double getCoverage() {
		return coverage;
	}
	public void setCoverage(Double coverage) {
		this.coverage = coverage;
	}
	@Column(name="start_date")
	public java.util.Date getStartDate() {
		return startDate;
	}
	public void setStartDate(java.util.Date startDate) {
		this.startDate = startDate;
	}
	@Column(name="end_date")
	public java.util.Date getEndDate() {
		return endDate;
	}
	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	@Transient
	public User getUser() {
		if (username == null) {
			return null;
		}
		if (username == "") {
			return null;
		}
		return DAORegistry.getUserDAO().getUserByUsername(username);
	}

	@Transient
	public Event getEvent() {
		if (eventId == null) {
			return null;
		}
		return DAORegistry.getEventDAO().get(eventId);
	}
	
}