package com.admitone.tmat.data;

import java.io.Serializable;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.security.SessionListener;


@Entity
@Table(name="admitone_users")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String username;
	private String password;
	private Set<Role> roles;
	private String email;
	private String firstName;
	private String lastName;
	private String phone;
	private Boolean locked;
	private Boolean emailCrawlErrorCreatorEnabled;
	private Boolean emailCrawlErrorLastUpdaterEnabled;
	private String code;
	private String applicationName;
	private Logger logger = LoggerFactory.getLogger(User.class);
	private Boolean isLiveUser=Boolean.FALSE;
	private Broker broker;
	private Set<UserBrokerDetails> userBrokerDetails;
//	private Broker multiBrokerAccess;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="password", columnDefinition="CHAR(40)")
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void setAndEncryptPassword(String clearPassword) {
	     MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("SHA-1");
		} catch (NoSuchAlgorithmException e) {
			logger.error("SHA-1 algorithm not found!", e);
			throw new RuntimeException("SHA-1 algorithm not found!");
		}
	    byte [] digest = algorithm.digest(clearPassword.getBytes());
	    setPassword(new String(Hex.encodeHex(digest)));
	}

	@ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name="user_roles",
        joinColumns=
            @JoinColumn(name="user_id", referencedColumnName="id"),
        inverseJoinColumns=
            @JoinColumn(name="role_id", referencedColumnName="id")
        )
	public Set<Role> getRoles() {
		return roles;
	}
		
	
	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Column(name="email")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Column(name="first_name")
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	@Column(name="last_name")
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	@Column(name="phone")
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Column(name="locked")
	public Boolean getLocked() {
		return locked;
	}
	
	public void setLocked(Boolean locked) {
		this.locked = locked;
	}
	
	@Transient
	public Boolean isLocked() {
		return locked;
	}
	
	@Column(name="email_crawl_error_creator_enabled")
	public Boolean getEmailCrawlErrorCreatorEnabled() {
		return emailCrawlErrorCreatorEnabled;
	}
	
	public void setEmailCrawlErrorCreatorEnabled(
			Boolean emailCrawlErrorCreatorEnabled) {
		this.emailCrawlErrorCreatorEnabled = emailCrawlErrorCreatorEnabled;
	}
	
	@Column(name="email_crawl_error_last_updater_enabled")
	public Boolean getEmailCrawlErrorLastUpdaterEnabled() {
		return emailCrawlErrorLastUpdaterEnabled;
	}
	
	public void setEmailCrawlErrorLastUpdaterEnabled(
			Boolean emailCrawlErrorLastUpdaterEnabled) {
		this.emailCrawlErrorLastUpdaterEnabled = emailCrawlErrorLastUpdaterEnabled;
	}
	
	@Column(name="code")
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	@Transient
	public boolean hasRole(String roleId) {
		for (Role role:roles) {
			if (role.getId().equals(roleId)) {
				return true;
			}
		}
		return false;
	}

	@Transient
	public boolean hasPrivilege(String privilegeId) {
		for (Role role:roles) {
			Collection<Privilege> privileges = role.getPrivileges();
			if (privileges == null) {
				continue;
			}
			for (Privilege privilege: privileges) {
				if (privilegeId.equals(privilege.getId())) {
					return true;
				}
			}
		}
		return false;
	}

	@Transient
	public boolean isLoggedIn() {
		return SessionListener.isLoggedIn(username);
	}
	
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public Boolean getIsLiveUser() {
		return isLiveUser;
	}
	public void setIsLiveUser(Boolean isLiveUser) {
		this.isLiveUser = isLiveUser;
	}
	@OneToOne
	@JoinColumn(name = "broker_id")
	public Broker getBroker() {
		return broker;
	}
	public void setBroker(Broker broker) {
		this.broker = broker;
	}
	
	/*@Column(name = "multibroker_access")
	public Integer getMultiBrokerAccess() {
		return multiBrokerAccess;
	}
	public void setMultiBrokerAccess(Integer multiBrokerAccess) {
		this.multiBrokerAccess = multiBrokerAccess;
	
	}*/

	@OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL, mappedBy="user")
	public Set<UserBrokerDetails> getUserBrokerDetails() {
		return userBrokerDetails;
	}
	public void setUserBrokerDetails(Set<UserBrokerDetails> userBrokerDetails) {
		this.userBrokerDetails = userBrokerDetails;
	}
	
	
}