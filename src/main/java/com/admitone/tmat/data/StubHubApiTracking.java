package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stubhub_api_tracking")
public class StubHubApiTracking implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name = "crawl_id")
	private Integer crawlId;
	
	@Column(name = "created_time")
	private Date lastRunRime;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "run_status")
	private String runStatus;
	
	@Column(name = "last_updated")
	private Date lastUpdated;
	
	@Column(name = "tracking_type")
	private String trackingType;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCrawlId() {
		return crawlId;
	}

	public void setCrawlId(Integer crawlId) {
		this.crawlId = crawlId;
	}

	public Date getLastRunRime() {
		return lastRunRime;
	}

	public void setLastRunRime(Date lastRunRime) {
		this.lastRunRime = lastRunRime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRunStatus() {
		return runStatus;
	}

	public void setRunStatus(String runStatus) {
		this.runStatus = runStatus;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getTrackingType() {
		return trackingType;
	}

	public void setTrackingType(String trackingType) {
		this.trackingType = trackingType;
	}
}