package com.admitone.tmat.data;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tg_cats_category_event_audit")
public class TNTGCatsCategoryEventAudit implements Serializable {
	private Integer id;
	private Integer eventId;
	private String eventName;
	private Date eventDate;
	private Time eventTime;
	private String venueName;
	private Double newMarkup;
	private Date newExpectedArrivalDate;
	private Integer newShippingMethod;
	private Integer newNearTermDisplayOption;
	private Boolean newFrequentEvent;
	private Double oldMarkup;
	private Date oldExpectedArrivalDate;
	private Integer oldShippingMethod;
	private Integer oldNearTermDisplayOption;
	private Boolean oldFrequentEvent;
	private String userName;
	private Date createdDate;
	private String action;
	
	private static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	
	public TNTGCatsCategoryEventAudit() {
		
	}
	public TNTGCatsCategoryEventAudit(Event tmatEvent) {
		
		this.eventId = tmatEvent.getId();
		this.eventName = tmatEvent.getName();
		this.eventDate = tmatEvent.getLocalDate();
		this.eventTime = tmatEvent.getLocalTime();
		
	}
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="event_time")
	public Time getEventTime() {
		return eventTime;
	}
	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	@Column(name="new_markup")
	public Double getNewMarkup() {
		return newMarkup;
	}
	public void setNewMarkup(Double newMarkup) {
		this.newMarkup = newMarkup;
	}
	
	@Column(name="new_expected_arrival_date")
	public Date getNewExpectedArrivalDate() {
		return newExpectedArrivalDate;
	}
	public void setNewExpectedArrivalDate(Date newExpectedArrivalDate) {
		this.newExpectedArrivalDate = newExpectedArrivalDate;
	}
	
	@Column(name="new_shipping_method")
	public Integer getNewShippingMethod() {
		return newShippingMethod;
	}
	public void setNewShippingMethod(Integer newShippingMethod) {
		this.newShippingMethod = newShippingMethod;
	}
	
	@Column(name="new_near_term_display_option")
	public Integer getNewNearTermDisplayOption() {
		return newNearTermDisplayOption;
	}
	public void setNewNearTermDisplayOption(Integer newNearTermDisplayOption) {
		this.newNearTermDisplayOption = newNearTermDisplayOption;
	}
	
	@Column(name="new_is_frequent")
	public Boolean getNewFrequentEvent() {
		return newFrequentEvent;
	}
	public void setNewFrequentEvent(Boolean newFrequentEvent) {
		this.newFrequentEvent = newFrequentEvent;
	}
	
	@Column(name="old_markup")
	public Double getOldMarkup() {
		return oldMarkup;
	}
	public void setOldMarkup(Double oldMarkup) {
		this.oldMarkup = oldMarkup;
	}
	
	@Column(name="old_expected_arrival_date")
	public Date getOldExpectedArrivalDate() {
		return oldExpectedArrivalDate;
	}
	public void setOldExpectedArrivalDate(Date oldExpectedArrivalDate) {
		this.oldExpectedArrivalDate = oldExpectedArrivalDate;
	}
	
	@Column(name="old_shipping_method")
	public Integer getOldShippingMethod() {
		return oldShippingMethod;
	}
	public void setOldShippingMethod(Integer oldShippingMethod) {
		this.oldShippingMethod = oldShippingMethod;
	}
	
	@Column(name="old_near_term_display_option")
	public Integer getOldNearTermDisplayOption() {
		return oldNearTermDisplayOption;
	}
	public void setOldNearTermDisplayOption(Integer oldNearTermDisplayOption) {
		this.oldNearTermDisplayOption = oldNearTermDisplayOption;
	}
	
	@Column(name="old_is_frequent")
	public Boolean getOldFrequentEvent() {
		return oldFrequentEvent;
	}
	public void setOldFrequentEvent(Boolean oldFrequentEvent) {
		this.oldFrequentEvent = oldFrequentEvent;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	

}
