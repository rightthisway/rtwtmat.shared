package com.admitone.tmat.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@DiscriminatorValue(value="ARTIST")
public class ArtistBookmark extends Bookmark {
	
	
	@Transient
	public Artist getArtist() {
		return DAORegistry.getArtistDAO().get(getObjectId());
	}
}
