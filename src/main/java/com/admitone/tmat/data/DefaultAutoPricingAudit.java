package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="default_autopricing_properties_audit")
public class DefaultAutoPricingAudit implements Serializable {
	 private Integer id;
	 private String userName;
	 private Date createdDate;
	 private String action;
	 private String category;
	 private String oldMiniExposure;
	 private String newMiniExposure;
	 private String oldAutoExposure;
	 private String newAutoExposure;
	 private String oldVipMiniExposure;
	 private String newVipMiniExposure;
	 private String oldVipAutoExposure;
	 private String newVipAutoExposure;
	 private Integer oldShippingMethod;
	 private Integer newShippingMethod;
	 private Integer oldNearTermDisplayOption;
	 private Integer newNearTermDisplayOption;
	 private Double oldRptFactor;
	 private Double newRptFactor;
	 private Double oldPriceBreakup;
	 private Double newPriceBreakup;
	 private Double oldUpperMarkup;
	 private Double newUpperMarkup;
	 private Double oldLowerMarkup;
	 private Double newLowerMarkup;
	 private Double oldUpperShippingFees;
	 private Double newUpperShippingFees;
	 private Double oldLowerShippingFees;
	 private Double newLowerShippingFees;
	 private Integer oldSectionCountTicket;
	 private Integer newSectionCountTicket;
	 private String oldLastRowMiniExposure;
	 private String newLastRowMiniExposure;
	 private String oldLastFiveRowMiniExposure;
	 private String newLastFiveRowMiniExposure;
	 
	 private String oldZonesPricingExposure;
	 private String newZonesPricingExposure;
	 private String oldZonedLastRowMiniExposure;
	 private String newZonedLastRowMiniExposure;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@Column(name="old_lower_markup")
	public Double getOldLowerMarkup() {
		return oldLowerMarkup;
	}
	public void setOldLowerMarkup(Double oldLowerMarkup) {
		this.oldLowerMarkup = oldLowerMarkup;
	}
	
	@Column(name="old_upper_markup")
	public Double getOldUpperMarkup() {
		return oldUpperMarkup;
	}
	public void setOldUpperMarkup(Double oldUpperMarkup) {
		this.oldUpperMarkup = oldUpperMarkup;
	}
	
	@Column(name="old_lower_shipping_fees")
	public Double getOldLowerShippingFees() {
		return oldLowerShippingFees;
	}
	public void setOldLowerShippingFees(Double oldLowerShippingFees) {
		this.oldLowerShippingFees = oldLowerShippingFees;
	}
	
	@Column(name="old_upper_shipping_fees")
	public Double getOldUpperShippingFees() {
		return oldUpperShippingFees;
	}
	public void setOldUpperShippingFees(Double oldUpperShippingFees) {
		this.oldUpperShippingFees = oldUpperShippingFees;
	}
	
	@Column(name="old_rpt_factor")
	public Double getOldRptFactor() {
		return oldRptFactor;
	}
	public void setOldRptFactor(Double oldRptFactor) {
		this.oldRptFactor = oldRptFactor;
	}
	
	
	@Column(name="new_lower_markup")
	public Double getNewLowerMarkup() {
		return newLowerMarkup;
	}
	public void setNewLowerMarkup(Double newLowerMarkup) {
		this.newLowerMarkup = newLowerMarkup;
	}
	
	@Column(name="new_upper_markup")
	public Double getNewUpperMarkup() {
		return newUpperMarkup;
	}
	public void setNewUpperMarkup(Double newUpperMarkup) {
		this.newUpperMarkup = newUpperMarkup;
	}
	
	@Column(name="new_lower_shipping_fees")
	public Double getNewLowerShippingFees() {
		return newLowerShippingFees;
	}
	public void setNewLowerShippingFees(Double newLowerShippingFees) {
		this.newLowerShippingFees = newLowerShippingFees;
	}
	
	@Column(name="new_upper_shipping_fees")
	public Double getNewUpperShippingFees() {
		return newUpperShippingFees;
	}
	public void setNewUpperShippingFees(Double newUpperShippingFees) {
		this.newUpperShippingFees = newUpperShippingFees;
	}
	
	
	@Column(name="new_rpt_factor")
	public Double getNewRptFactor() {
		return newRptFactor;
	}
	public void setNewRptFactor(Double newRptFactor) {
		this.newRptFactor = newRptFactor;
	}
	
	@Column(name="old_price_breakup")
	public Double getOldPriceBreakup() {
		return oldPriceBreakup;
	}
	public void setOldPriceBreakup(Double oldPriceBreakup) {
		this.oldPriceBreakup = oldPriceBreakup;
	}
	
	@Column(name="new_price_breakup")
	public Double getNewPriceBreakup() {
		return newPriceBreakup;
	}
	public void setNewPriceBreakup(Double newPriceBreakup) {
		this.newPriceBreakup = newPriceBreakup;
	}
	
	@Column(name="modified_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="old_section_count_ticket")
	public Integer getOldSectionCountTicket() {
		return oldSectionCountTicket;
	}
	public void setOldSectionCountTicket(Integer oldSectionCountTicket) {
		this.oldSectionCountTicket = oldSectionCountTicket;
	}
	
	@Column(name="new_section_count_ticket")
	public Integer getNewSectionCountTicket() {
		return newSectionCountTicket;
	}
	public void setNewSectionCountTicket(Integer newSectionCountTicket) {
		this.newSectionCountTicket = newSectionCountTicket;
	}
	/*
	@Column(name="old_exposure")
	public String getOldExposure() {
		return oldExposure;
	}
	public void setOldExposure(String oldExposure) {
		this.oldExposure = oldExposure;
	}
	
	@Column(name="new_exposure")
	public String getNewExposure() {
		return newExposure;
	}
	public void setNewExposure(String newExposure) {
		this.newExposure = newExposure;
	}*/
	
	@Column(name="old_mini_exposure")
	public String getOldMiniExposure() {
		return oldMiniExposure;
	}
	public void setOldMiniExposure(String oldMiniExposure) {
		this.oldMiniExposure = oldMiniExposure;
	}
	@Column(name="new_mini_exposure")
	public String getNewMiniExposure() {
		return newMiniExposure;
	}
	public void setNewMiniExposure(String newMiniExposure) {
		this.newMiniExposure = newMiniExposure;
	}
	
	@Column(name="old_auto_exposure")
	public String getOldAutoExposure() {
		return oldAutoExposure;
	}
	public void setOldAutoExposure(String oldAutoExposure) {
		this.oldAutoExposure = oldAutoExposure;
	}
	
	@Column(name="new_auto_exposure")
	public String getNewAutoExposure() {
		return newAutoExposure;
	}
	public void setNewAutoExposure(String newAutoExposure) {
		this.newAutoExposure = newAutoExposure;
	}
	
	@Column(name="old_vip_mini_exposure")
	public String getOldVipMiniExposure() {
		return oldVipMiniExposure;
	}
	public void setOldVipMiniExposure(String oldVipMiniExposure) {
		this.oldVipMiniExposure = oldVipMiniExposure;
	}
	@Column(name="new_vip_mini_exposure")
	public String getNewVipMiniExposure() {
		return newVipMiniExposure;
	}
	public void setNewVipMiniExposure(String newVipMiniExposure) {
		this.newVipMiniExposure = newVipMiniExposure;
	}
	
	@Column(name="old_vip_auto_exposure")
	public String getOldVipAutoExposure() {
		return oldVipAutoExposure;
	}
	public void setOldVipAutoExposure(String oldVipAutoExposure) {
		this.oldVipAutoExposure = oldVipAutoExposure;
	}
	@Column(name="new_vip_auto_exposure")
	public String getNewVipAutoExposure() {
		return newVipAutoExposure;
	}
	public void setNewVipAutoExposure(String newVipAutoExposure) {
		this.newVipAutoExposure = newVipAutoExposure;
	}
	
	@Column(name="old_shipping_methd")
	public Integer getOldShippingMethod() {
		return oldShippingMethod;
	}
	public void setOldShippingMethod(Integer oldShippingMethod) {
		this.oldShippingMethod = oldShippingMethod;
	}
	@Column(name="new_shipping_methd")
	public Integer getNewShippingMethod() {
		return newShippingMethod;
	}
	public void setNewShippingMethod(Integer newShippingMethod) {
		this.newShippingMethod = newShippingMethod;
	}
	
	@Column(name="old_near_term_display_option")
	public Integer getOldNearTermDisplayOption() {
		return oldNearTermDisplayOption;
	}
	public void setOldNearTermDisplayOption(Integer oldNearTermDisplayOption) {
		this.oldNearTermDisplayOption = oldNearTermDisplayOption;
	}
	
	@Column(name="new_near_term_display_option")
	public Integer getNewNearTermDisplayOption() {
		return newNearTermDisplayOption;
	}
	public void setNewNearTermDisplayOption(Integer newNearTermDisplayOption) {
		this.newNearTermDisplayOption = newNearTermDisplayOption;
	}
	
	@Column(name="old_lastrow_mini_exposure")
	public String getOldLastRowMiniExposure() {
		return oldLastRowMiniExposure;
	}
	public void setOldLastRowMiniExposure(String oldLastRowMiniExposure) {
		this.oldLastRowMiniExposure = oldLastRowMiniExposure;
	}
	
	@Column(name="new_lastrow_mini_exposure")
	public String getNewLastRowMiniExposure() {
		return newLastRowMiniExposure;
	}
	public void setNewLastRowMiniExposure(String newLastRowMiniExposure) {
		this.newLastRowMiniExposure = newLastRowMiniExposure;
	}
	
	@Column(name="old_lastfiverow_mini_exposure")
	public String getOldLastFiveRowMiniExposure() {
		return oldLastFiveRowMiniExposure;
	}
	public void setOldLastFiveRowMiniExposure(String oldLastFiveRowMiniExposure) {
		this.oldLastFiveRowMiniExposure = oldLastFiveRowMiniExposure;
	}
	
	@Column(name="new_lastfiverow_mini_exposure")
	public String getNewLastFiveRowMiniExposure() {
		return newLastFiveRowMiniExposure;
	}
	public void setNewLastFiveRowMiniExposure(String newLastFiveRowMiniExposure) {
		this.newLastFiveRowMiniExposure = newLastFiveRowMiniExposure;
	}
	
	@Column(name="old_zones_pricing_exposure")
	public String getOldZonesPricingExposure() {
		return oldZonesPricingExposure;
	}
	public void setOldZonesPricingExposure(String oldZonesPricingExposure) {
		this.oldZonesPricingExposure = oldZonesPricingExposure;
	}
	
	@Column(name="new_zones_pricing_exposure")
	public String getNewZonesPricingExposure() {
		return newZonesPricingExposure;
	}
	public void setNewZonesPricingExposure(String newZonesPricingExposure) {
		this.newZonesPricingExposure = newZonesPricingExposure;
	}
	@Column(name="old_zoned_lastrowmini_exposure")
	public String getOldZonedLastRowMiniExposure() {
		return oldZonedLastRowMiniExposure;
	}
	public void setOldZonedLastRowMiniExposure(String oldZonedLastRowMiniExposure) {
		this.oldZonedLastRowMiniExposure = oldZonedLastRowMiniExposure;
	}
	@Column(name="new_zoned_lastrowmini_exposure")
	public String getNewZonedLastRowMiniExposure() {
		return newZonedLastRowMiniExposure;
	}
	public void setNewZonedLastRowMiniExposure(String newZonedLastRowMiniExposure) {
		this.newZonedLastRowMiniExposure = newZonedLastRowMiniExposure;
	}
	
	
	
	
	
}
