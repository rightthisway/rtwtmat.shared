package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.ebay.EbayUtil;
import com.admitone.tmat.enums.EbayInventoryStatus;
import com.admitone.tmat.enums.TicketStatus;

@Entity
@Table(name="ebay_inventory_group")
public class EbayInventoryGroup implements Serializable {
	private Integer id;
	private Integer eventId;
	private String category;
	private Integer quantity;
	private Date creationDate;
	private Date lastUpdate;
	private Collection<EbayInventory> inventories;
	private Double xp2;
	private Double xp3;
	private EbayInventoryStatus status;
	private Integer replacedEbayInventoryGroupId;
	private Integer ticketId;
	private Date firstTimeUsed;
	private Event event;
	private String tourLevel= "N";
	private String eventLevel= "N";
	private String allLevel= "Y";
	private Double markup;
	private Double shippingFees;
	private Double ebayInventoryPrice;
	private Double zonePrice;
	private Integer categoryId;
	
	
	public EbayInventoryGroup(EbayInventoryEvent ebayInventoryEvent, String category, Integer quantity) {
		this.eventId = ebayInventoryEvent.getEventId();
		this.category = category;
		this.quantity = quantity;
		
		this.creationDate = new Date();
		this.lastUpdate = creationDate;
		this.status = EbayInventoryStatus.ACTIVE;
	}
	
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public EbayInventoryGroup() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
/*
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	*/
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}
	
	@Column(name="last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}
	
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	@Column(name="tour_level")
	public String getTourLevel() {
		return tourLevel;
	}

	public void setTourLevel(String tourLevel) {
		this.tourLevel = tourLevel;
	}

	@Column(name="event_level")
	public String getEventLevel() {
		return eventLevel;
	}

	public void setEventLevel(String eventLevel) {
		this.eventLevel = eventLevel;
	}

	@Column(name="all_level")
	public String getAllLevel() {
		return allLevel;
	}

	public void setAllLevel(String allLevel) {
		this.allLevel = allLevel;
	}

	@Transient
	public Collection<EbayInventory> getEbayInventories() {
		return inventories;
	}

	public void setEbayInventories(Collection<EbayInventory> inventories) {
		this.inventories = inventories;
	}
	@Transient
	public Double getShippingFees() {
		return shippingFees;
	}
	public void setShippingFees(Double shippingFees) {
		this.shippingFees = shippingFees;
	}
	@Transient
	public Double getEbayInventoryPrice(){
		Iterator<EbayInventory> itr = inventories.iterator();
		if(!itr.hasNext())
			return null;
		Double price = itr.next().getOnlinePrice();
		if(markup ==null)
			markup=0.0;
		if(shippingFees==null)
			shippingFees=0.0;
		price = price + (markup *price)/100 + (shippingFees/quantity);
		ebayInventoryPrice=price;
		return price;
	}
	public void setEbayInventoryPrice(Double ebayInventoryPrice) {
		this.ebayInventoryPrice = ebayInventoryPrice;
	}
	@Transient
	public Double getZonePrice() {
		zonePrice=getEbayInventoryPrice()*getQuantity();
		return zonePrice;
	}
	public void setZonePrice(Double zonePrice) {
		this.zonePrice = zonePrice;
	}
	@Transient
	public EbayInventory getEbayInventory1() {
		Iterator<EbayInventory> iterator = inventories.iterator();
		if (!iterator.hasNext()) {
			return null;
		}
		return iterator.next();
	}

	@Transient
	public EbayInventory getEbayInventory2() {
		Iterator<EbayInventory> iterator = inventories.iterator();
		if (!iterator.hasNext()) {
			return null;
		}
		iterator.next();
		if (!iterator.hasNext()) {
			return null;
		}
		return iterator.next();
	}

	@Transient
	public EbayInventory getEbayInventory3() {
		Iterator<EbayInventory> iterator = inventories.iterator();
		if (!iterator.hasNext()) {
			return null;
		}

		iterator.next();
		if (!iterator.hasNext()) {
			return null;
		}

		iterator.next();
		if (!iterator.hasNext()) {
			return null;
		}
		return iterator.next();
	}

	@Transient
	public Double getXp2() {
		return xp2;
	}

	public void setXp2(Double xp2) {
		this.xp2 = xp2;
	}

	@Transient
	public Double getXp3() {
		return xp3;
	}

	public void setXp3(Double xp3) {
		this.xp3 = xp3;
	}

	/*public void computeInternalValuesFromDB() {
		if (status.equals(EbayInventoryStatus.DELETED)) {
			return;
		}
		if (inventories == null) {
			inventories = DAORegistry.getEbayInventoryDAO().getEbayInventoriesByGroupId(id);
		}
		computeInternalValues();
	}
		
	public void computeInternalValues() {
		for (EbayInventory inventory: inventories) {
			Double binPrice = (quantity * inventory.getOnlinePrice()) * (100 + getEbayInventoryEvent().getRptFactor()) / 100;
			Double rpt = binPrice / quantity;
			Double netProfit = binPrice - (quantity * inventory.getOnlinePrice()) - EbayManager.getEbayFees(binPrice) - EbayManager.getPaypalFees(binPrice);
			inventory.setBinPrice(binPrice);
			inventory.setRpt(rpt);
			inventory.setNetProfit(netProfit);
		}
		
		EbayInventory inventory1 = getEbayInventory1();

		if (getExposure() > 1) {
			EbayInventory inventory2 = getEbayInventory2();
			Double tg2 = inventory1.getNetProfit() - (inventory2.getOnlinePrice() - inventory1.getOnlinePrice()) * quantity;
			setXp2(tg2);

			if (getExposure() > 2) {
				EbayInventory inventory3 = getEbayInventory3();
				Double tg3 = inventory1.getNetProfit() - (inventory3.getOnlinePrice() - inventory1.getOnlinePrice()) * quantity;
				setXp3(tg3);		
			}
		}
	}*/
	
	@Column(name="status")
	@Enumerated(EnumType.STRING)
	public EbayInventoryStatus getStatus() {
		return status;
	}

	public void setStatus(EbayInventoryStatus status) {
		this.status = status;
	}

	@Column(name="replaced_inventory_group_id")
	public Integer getReplacedEbayInventoryGroupId() {
		return replacedEbayInventoryGroupId;
	}

	public void setReplacedEbayInventoryGroupId(Integer replacedEbayInventoryGroupId) {
		this.replacedEbayInventoryGroupId = replacedEbayInventoryGroupId;
	}

	@Column(name="first_time_used")
	public Date getFirstTimeUsed() {
		return firstTimeUsed;
	}

	public void setFirstTimeUsed(Date firstTimeUsed) {
		this.firstTimeUsed = firstTimeUsed;
	}

//	@ManyToOne(fetch=FetchType.EAGER)
//	@JoinColumn(name="event_id")
	/*@Transient
	public EbayInventoryEvent getEbayInventoryEvent() {
		if (eventId == null) {
			return null;
		}
		return DAORegistry.getEbayInventoryEventDAO().get(eventId);
//		return ebayInventoryEvent;
	}*/

//	public void setEbayInventoryEvent(EbayInventoryEvent ebayInventoryEvent) {
//		this.ebayInventoryEvent = ebayInventoryEvent;
//	}

	@Transient
	public boolean isDisabled() {
		return status.equals(EbayInventoryStatus.DISABLED);
	}
	
	/*@Transient
	public EbayInventoryGroup getReplacedEbayInventoryGroup() {
		if (replacedEbayInventoryGroupId == null) {
			return null;
		}
		return DAORegistry.getEbayInventoryGroupDAO().get(replacedEbayInventoryGroupId);
	}*/
	
	@Transient
	public boolean isReplacement() {
		return status.equals(EbayInventoryStatus.REPLACEMENT);
	}

	@Transient
	public boolean isValid() {
		return status.equals(EbayInventoryStatus.ACTIVE);
	}

	@Transient
	public boolean isInvalid() {
		return status.equals(EbayInventoryStatus.INVALID);
	}

	@Transient
	public boolean isSuggested() {
		return status.equals(EbayInventoryStatus.SUGGESTED);
	}

	@Transient
	public boolean isDeleted() {
		return status.equals(EbayInventoryStatus.DELETED);
	}

	@Transient
	public Event getEvent() {
		if (this.event == null) {
			this.event = DAORegistry.getEventDAO().get(eventId);
		}
		
		return this.event;
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	
/*
	@Transient
	public EbayInventoryEvent getEbayInventoryEvent() {
		return DAORegistry.getEbayInventoryEventDAO().get(eventId);
	}
	*/
	
	@Transient
	public Long getTicketIdTime() {
		if (firstTimeUsed == null) {
			return 0L;
		}
		
		Date now = new Date();
		return now.getTime() - firstTimeUsed.getTime();
	}
	
	@Transient
	public int getExposure() {
		return inventories.size();
	}
	
	@Transient
	public boolean hasInvalidTickets() {
		if (isDeleted() || isInvalid()) {
			return false;
		}
		
		for (EbayInventory inventory: inventories) {
			Ticket ticket = inventory.getTicket();
			if (ticket == null) {
				return true;
			}

			if (!ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
				return true;
			}

		}
		
		return false;
	}
	
	@Transient
	public boolean hasIncompatibleTicketQuantities() {
		for (EbayInventory inventory: inventories) {
			if (!EbayUtil.isPartition(quantity, inventory.getTicket().getRemainingQuantity())) {
				return true;
			}
		}
		return false;
	}
	@Transient
	public Double getMarkup() {
		return markup;
	}
	public void setMarkup(Double markup) {
		this.markup = markup;
	}
	@Transient
	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
}