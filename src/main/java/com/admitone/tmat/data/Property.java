package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;

/**
 * Property Class.
 * This class is used to store properties such as smtp settings and crawler settings.
 */
@Entity
@Table(name="tmat_property")
@DataTransferObject
public class Property implements Serializable {
	private static final long serialVersionUID = 1957532231871350178L;
	public String name;
	public String value;

	public Property(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	public Property() {}
	
	@Id
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@Column(name="value")
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
