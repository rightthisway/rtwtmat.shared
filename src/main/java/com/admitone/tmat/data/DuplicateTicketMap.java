package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="duplicate_ticket_map")
public class DuplicateTicketMap implements Serializable {
	private Integer ticketId;
	private Integer eventId;
	private Long groupId;
	
	public DuplicateTicketMap() {
		super();
	}
	
	public DuplicateTicketMap(Integer ticketId, Integer eventId, Long groupId) {
		this.ticketId = ticketId;
		this.eventId = eventId;
		this.groupId = groupId;
	}
	
	
	@Id
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="group_id")
	public Long getGroupId() {
		return groupId;
	}
	
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
}
