package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;

//import com.admitone.tmat.dao.DAORegistry;
//import com.admitone.tmat.utils.SectionRowStripper;
//import com.admitone.tmat.utils.TicketUtil;

@Entity
@Table(name="admitone_inventory_local")
@DataTransferObject
public class AdmitoneInventoryLocal implements Serializable {
	
	/* the identifier that uniquely defines a ticket for admitone */
	protected Integer ticketId;

	protected Integer eventId;
	
	protected Integer quantity;
	protected Double currentPrice;
	protected Double cost;
	
	protected String section;
	protected String row;
	protected String seat;

	protected String broker;
	protected String broadcastSeller;
	protected String inventoryType;
	protected Boolean categoryTicket;

	
	@Id
	@Column(name="TicketId")
	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	@Column(name="EventId")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="Quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name="Wholesale")
	public Double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(Double currentPrice) {
		this.currentPrice = currentPrice;
	}

	@Column(name="Cost")
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost= cost;
	}

	@Column(name="Section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name="Row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Column(name="SEAT")
	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	@Column(name="Broker")
	public String getBroker() {
		return broker;
	}

	public void setBroker(String broker) {
		this.broker = broker;
	}

	@Column(name="Code")
	public String getBroadcastSeller() {
		return broadcastSeller;
	}

	public void setBroadcastSeller(String broadcastSeller) {
		this.broadcastSeller = broadcastSeller;
	}

	@Column(name="InventoryType")
	public String getInventoryType() {
		return inventoryType;
	}

	public void setInventoryType(String inventoryType) {
		this.inventoryType = inventoryType;
	}

	@Column(name="category_ticket")
	public Boolean getCategoryTicket() {
		if(categoryTicket == null) {
			return false;
		}
		return categoryTicket;
	}

	public void setCategoryTicket(Boolean categoryTicket) {
		if(categoryTicket == null) {
			categoryTicket = false;
		}
		this.categoryTicket = categoryTicket;
	}


}
