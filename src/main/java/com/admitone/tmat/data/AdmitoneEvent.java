package com.admitone.tmat.data;

import java.io.Serializable;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
//import org.hibernate.annotations.Index;

//import com.admitone.tmat.dao.DAORegistry;

@Entity
@Table(name="admitone_event")
@DataTransferObject
public class AdmitoneEvent implements Serializable {
	
	/* the identifier that uniquely defines an event for admitone */
	protected Integer eventId;
	private String formatedDate;
	protected String eventName;
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	protected static DateFormat timeFormat = new SimpleDateFormat("HH:mm zzz");
	
	protected Date eventDate;
	protected Time eventTime;
	
	protected String venueName;
	private String city;
	private String state;
	private String stateShortDesc;
	private String country;
	private String parentCategory;
	private String childCategory;
	private String grandChildCategory;
	private Date createdDate;
	private String performanceName;
	private Integer performanceId;
	private Integer venueId;
	private Integer zoneEvent;
	private String zipcode;
	public AdmitoneEvent() {
	}
	
	public AdmitoneEvent(AdmitoneEventLocal admitoneEventLocal) {
		this.eventId = -1;
		this.eventDate = admitoneEventLocal.getEventDate();
		this.eventName = admitoneEventLocal.getEventName();
		this.eventTime = admitoneEventLocal.getEventTime();
		this.venueName = admitoneEventLocal.getVenueName();
	}

	@Id
	@Column(name="EventId")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="EventName")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="EventDate")
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	@Column(name="EventTime")
	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	@Column(name="VenueName")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	@Transient
	public String getFormatedDate() {
		// TODO Auto-generated method stub	
		if(getEventDate() == null){
			formatedDate = "TBD";	
		}else{
		formatedDate= dateFormat.format(getEventDate());
		}
		if(getEventTime()!=null){
			formatedDate = formatedDate+" "+timeFormat.format(getEventTime());
		}else{
			formatedDate = formatedDate+" TBD";
		}
		
		return formatedDate;
	}
	@Column(name="city")
	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}
	@Column(name="state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	@Column(name="parent_category")
	public String getParentCategory() {
		return parentCategory;
	}

	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	@Column(name="child_category")
	public String getChildCategory() {
		return childCategory;
	}

	public void setChildCategory(String childCategory) {
		this.childCategory = childCategory;
	}
	@Column(name="grandchild_category")
	public String getGrandChildCategory() {
		return grandChildCategory;
	}

	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="performance_name")
	public String getPerformanceName() {
		return performanceName;
	}

	public void setPerformanceName(String performanceName) {
		this.performanceName = performanceName;
	}

	@Column(name="performance_id")
	public Integer getPerformanceId() {
		return performanceId;
	}

	public void setPerformanceId(Integer performanceId) {
		this.performanceId = performanceId;
	}

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	
	@Column(name="state_short_desc")
	public String getStateShortDesc() {
		return stateShortDesc;
	}

	public void setStateShortDesc(String stateShortDesc) {
		this.stateShortDesc = stateShortDesc;
	}
	
	
	@Column(name="country")
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	@Column(name="is_zone_event")
	public Integer getZoneEvent() {
		return zoneEvent;
	}

	public void setZoneEvent(Integer zoneEvent) {
		this.zoneEvent = zoneEvent;
	}

	@Column(name="postal_code")
	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
	
}
