package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tmat_active_events_crawls")
public class ActiveEventAndActiveCrawls implements Serializable{
    int id;   
    Date statisticDate;
    int activeEventSports;
    int activeEventConcert;
    int activeEventTheater;
    int activeEventOther;
    int tndExchangeCrawlsSports;
    int tndExchangeCrawlsConcert;
    int tndExchangeCrawlsTheater;
    int tndExchangeCrawlsOther;
    int shExchangeCrawlsSports;
    int shExchangeCrawlsConcert;
    int shExchangeCrawlsTheater;
    int shExchangeCrawlsOther;
    int teExchangeCrawlsSports;
    int teExchangeCrawlsConcert;
    int teExchangeCrawlsTheater;
    int teExchangeCrawlsOther;
    int tnowExchangeCrawlsSports;
    int tnowExchangeCrawlsConcert;
    int tnowExchangeCrawlsTheater;
    int tnowExchangeCrawlsOther;
    int fsExchangeCrawlsSports;
    int fsExchangeCrawlsConcert;
   // Integer fsExchangeCrawlsConcert;
    int fsExchangeCrawlsTheater;
    int fsExchangeCrawlsOther;
    int vsExchangeCrawlsSports;
    int vsExchangeCrawlsConcert;
    int vsExchangeCrawlsTheater;
    int vsExchangeCrawlsOther;
    
    @Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	@Column(name="stats_date") 
	public Date getStatisticDate() {
		return statisticDate;
	}
	public void setStatisticDate(Date statisticDate) {
		this.statisticDate = statisticDate;
	}
	@Column(name="active_events_sports")
	public int getActiveEventSports() {
		return activeEventSports;
	}
	public void setActiveEventSports(int activeEventSports) {
		this.activeEventSports = activeEventSports;
	}
	@Column(name="active_events_concerts")
	public int getActiveEventConcert() {
		return activeEventConcert;
	}
	public void setActiveEventConcert(int activeEventConcert) {
		this.activeEventConcert = activeEventConcert;
	}
	@Column(name="active_events_theater")
	public int getActiveEventTheater() {
		return activeEventTheater;
	}
	public void setActiveEventTheater(int activeEventTheater) {
		this.activeEventTheater = activeEventTheater;
	}
	@Column(name="active_events_other")
	public int getActiveEventOther() {
		return activeEventOther;
	}
	public void setActiveEventOther(int activeEventOther) {
		this.activeEventOther = activeEventOther;
	}
	@Column(name="tnd_exchanges_crawls_sports")
	public int getTndExchangeCrawlsSports() {
		return tndExchangeCrawlsSports;
	}
	public void setTndExchangeCrawlsSports(int tndExchangeCrawlsSports) {
		this.tndExchangeCrawlsSports = tndExchangeCrawlsSports;
	}
	@Column(name="tnd_exchanges_crawls_concerts")
	public int getTndExchangeCrawlsConcert() {
		return tndExchangeCrawlsConcert;
	}
	public void setTndExchangeCrawlsConcert(int tndExchangeCrawlsConcert) {
		this.tndExchangeCrawlsConcert = tndExchangeCrawlsConcert;
	}
	@Column(name="tnd_exchanges_crawls_theater")
	public int getTndExchangeCrawlsTheater() {
		return tndExchangeCrawlsTheater;
	}
	public void setTndExchangeCrawlsTheater(int tndExchangeCrawlsTheater) {
		this.tndExchangeCrawlsTheater = tndExchangeCrawlsTheater;
	}
	@Column(name="tnd_exchanges_crawls_other")
	public int getTndExchangeCrawlsOther() {
		return tndExchangeCrawlsOther;
	}
	public void setTndExchangeCrawlsOther(int tndExchangeCrawlsOther) {
		this.tndExchangeCrawlsOther = tndExchangeCrawlsOther;
	}
	@Column(name="sh_exchanges_crawls_sports")
	public int getShExchangeCrawlsSports() {
		return shExchangeCrawlsSports;
	}
	public void setShExchangeCrawlsSports(int shExchangeCrawlsSports) {
		this.shExchangeCrawlsSports = shExchangeCrawlsSports;
	}
	@Column(name="sh_exchanges_crawls_concerts")
	public int getShExchangeCrawlsConcert() {
		return shExchangeCrawlsConcert;
	}
	public void setShExchangeCrawlsConcert(int shExchangeCrawlsConcert) {
		this.shExchangeCrawlsConcert = shExchangeCrawlsConcert;
	}
	@Column(name="sh_exchanges_crawls_theater")
	public int getShExchangeCrawlsTheater() {
		return shExchangeCrawlsTheater;
	}
	public void setShExchangeCrawlsTheater(int shExchangeCrawlsTheater) {
		this.shExchangeCrawlsTheater = shExchangeCrawlsTheater;
	}

    @Column(name="sh_exchanges_crawls_other")
	public int getShExchangeCrawlsOther() {
		return shExchangeCrawlsOther;
	}
	public void setShExchangeCrawlsOther(int shExchangeCrawlsOther) {
		this.shExchangeCrawlsOther = shExchangeCrawlsOther;
	}
	@Column(name="te_exchanges_crawls_sports")
	public int getTeExchangeCrawlsSports() {
		return teExchangeCrawlsSports;
	}
	public void setTeExchangeCrawlsSports(int teExchangeCrawlsSports) {
		this.teExchangeCrawlsSports = teExchangeCrawlsSports;
	}
	@Column(name="te_exchanges_crawls_concerts")
	public int getTeExchangeCrawlsConcert() {
		return teExchangeCrawlsConcert;
	}
	public void setTeExchangeCrawlsConcert(int teExchangeCrawlsConcert) {
		this.teExchangeCrawlsConcert = teExchangeCrawlsConcert;
	}
	@Column(name="te_exchanges_crawls_theater")
	public int getTeExchangeCrawlsTheater() {
		return teExchangeCrawlsTheater;
	}
	public void setTeExchangeCrawlsTheater(int teExchangeCrawlsTheater) {
		this.teExchangeCrawlsTheater = teExchangeCrawlsTheater;
	}
	@Column(name="te_exchanges_crawls_other")
	public int getTeExchangeCrawlsOther() {
		return teExchangeCrawlsOther;
	}
	public void setTeExchangeCrawlsOther(int teExchangeCrawlsOther) {
		this.teExchangeCrawlsOther = teExchangeCrawlsOther;
	}
	@Column(name="tn_exchanges_crawls_sports")
	public int getTnowExchangeCrawlsSports() {
		return tnowExchangeCrawlsSports;
	}
	public void setTnowExchangeCrawlsSports(int tnExchangeCrawlsSports) {
		this.tnowExchangeCrawlsSports = tnExchangeCrawlsSports;
	}
	@Column(name="tn_exchanges_crawls_concerts")
	public int getTnowExchangeCrawlsConcert() {
		return tnowExchangeCrawlsConcert;
	}
	public void setTnowExchangeCrawlsConcert(int tnExchangeCrawlsConcert) {
		this.tnowExchangeCrawlsConcert = tnExchangeCrawlsConcert;
	}
	@Column(name="tn_exchanges_crawls_theater")
	public int getTnowExchangeCrawlsTheater() {
		return tnowExchangeCrawlsTheater;
	}
	public void setTnowExchangeCrawlsTheater(int tnExchangeCrawlsTheater) {
		this.tnowExchangeCrawlsTheater = tnExchangeCrawlsTheater;
	}
	@Column(name="tn_exchanges_crawls_other")
	public int getTnowExchangeCrawlsOther() {
		return tnowExchangeCrawlsOther;
	}
	public void setTnowExchangeCrawlsOther(int tnExchangeCrawlsOther) {
		this.tnowExchangeCrawlsOther = tnExchangeCrawlsOther;
	}

	@Column(name="fs_exchanges_crawls_sports")
	public int getFsExchangeCrawlsSports() {
		return fsExchangeCrawlsSports;
	}
	public void setFsExchangeCrawlsSports(int flseExchangeCrawlsSports) {
		this.fsExchangeCrawlsSports = flseExchangeCrawlsSports;
	}
	@Column(name="fs_exchanges_crawls_concerts")
	public int getFsExchangeCrawlsConcert() {
		return fsExchangeCrawlsConcert;
	}
	/*public void setFsExchangeCrawlsConcert(Integer flseExchangeCrawlsConcert) {
		this.fsExchangeCrawlsConcert = flseExchangeCrawlsConcert;
	}*/
	
	public void setFsExchangeCrawlsConcert(int flseExchangeCrawlsConcert) {
		this.fsExchangeCrawlsConcert = flseExchangeCrawlsConcert;
	}
		
	@Column(name="fs_exchanges_crawls_theater")
	public int getFsExchangeCrawlsTheater() {
		return fsExchangeCrawlsTheater;
	}
	public void setFsExchangeCrawlsTheater(int flseExchangeCrawlsTheater) {
		this.fsExchangeCrawlsTheater = flseExchangeCrawlsTheater;
	}
	@Column(name="fs_exchanges_crawls_other")
	public int getFsExchangeCrawlsOther() {
		return fsExchangeCrawlsOther;
	}
	public void setFsExchangeCrawlsOther(int flseExchangeCrawlsOther) {
		this.fsExchangeCrawlsOther = flseExchangeCrawlsOther;
	}

    @Column(name="vs_exchanges_crawls_sports")
	public int getVsExchangeCrawlsSports() {
		return vsExchangeCrawlsSports;
	}
	public void setVsExchangeCrawlsSports(int vvseExchangeCrawlsSports) {
		this.vsExchangeCrawlsSports = vvseExchangeCrawlsSports;
	}
	@Column(name="vs_exchanges_crawls_concerts")
	public int getVsExchangeCrawlsConcert() {
		return vsExchangeCrawlsConcert;
	}
	public void setVsExchangeCrawlsConcert(int vvseExchangeCrawlsConcert) {
		this.vsExchangeCrawlsConcert = vvseExchangeCrawlsConcert;
	}
	@Column(name="vs_exchanges_crawls_theater")
	public int getVsExchangeCrawlsTheater() {
		return vsExchangeCrawlsTheater;
	}
	public void setVsExchangeCrawlsTheater(int vvseExchangeCrawlsTheater) {
		this.vsExchangeCrawlsTheater = vvseExchangeCrawlsTheater;
	}
	@Column(name="vs_exchanges_crawls_other")
	public int getVsExchangeCrawlsOther() {
		return vsExchangeCrawlsOther;
	}
	public void setVsExchangeCrawlsOther(int vvseExchangeCrawlsOther) {
		this.vsExchangeCrawlsOther = vvseExchangeCrawlsOther;
	}

}
