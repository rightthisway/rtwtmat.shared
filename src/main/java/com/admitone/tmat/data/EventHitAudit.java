package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.admitone.tmat.eventfetcher.EventHit;

@Entity
@Table(name="event_hit_audit")
public class EventHitAudit implements Serializable {
	 private Integer id;
	 private Integer eventId;
	 private Integer artistId;
	 private Integer venueId;
	 private String searchType;
	 private String searchKey;
	 private String resultValue; 
	 private String eventHitId;
	 private String name;
	 private String dateAndTime;
	 private String location;
	 private String siteId;
	 private String url;
	 private String shortName;
	 private Date modified_date;
	 
	 public EventHitAudit() {
		// TODO Auto-generated constructor stub
	 }
	 
	 public EventHitAudit(EventHit eventHit) {
		this.eventHitId = eventHit.getId();
		this.name = eventHit.getName();
		this.dateAndTime = eventHit.getDateAndTime();
		this.location = eventHit.getLocation();
		this.siteId = eventHit.getSiteId();
		this.url = eventHit.getUrl();
		this.shortName = eventHit.getShortName();
		this.resultValue = eventHit.getDateAndTime().trim() + "-" + eventHit.getLocation().toLowerCase().trim();
	 }
	 
	 
	@Id
	@Column(name="id",  columnDefinition="INTEGER AUTO_INCREMENT")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="searchType")
	public String getSearchType() {
		return searchType;
	}
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	
	 
	
	@Column(name="eventHitId")
	public String getEventHitId() {
		return eventHitId;
	}
	public void setEventHitId(String eventHitId) {
		this.eventHitId = eventHitId;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="dateAndTime")
	public String getDateAndTime() {
		return dateAndTime;
	}
	public void setDateAndTime(String dateAndTime) {
		this.dateAndTime = dateAndTime;
	}
	
	@Column(name="location")
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Column(name="siteId")
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	@Column(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	@Column(name="shortName")
	public String getShortName() {
		return shortName;
	}
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}
	
	@Column(name="modified_date")
	public Date getModified_date() {
		return modified_date;
	}
	public void setModified_date(Date modified_date) {
		this.modified_date = modified_date;
	}

	
	@Column(name="searchKey")
	public String getSearchKey() {
		return searchKey;
	}

	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}

	@Column(name="resultValue")
	public String getResultValue() {
		return resultValue;
	}

	public void setResultValue(String resultValue) {
		this.resultValue = resultValue;
	}
	
	@Column(name="eventId")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="artistId")
	public Integer getArtistId() {
		return artistId;
	}

	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}

	@Column(name="venueId")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	
}
