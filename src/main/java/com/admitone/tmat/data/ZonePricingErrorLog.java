package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OptimisticLockType;


@Entity
@Table(name="zone_pricing_error_log")
@org.hibernate.annotations.Entity(
	dynamicUpdate = true,optimisticLock=OptimisticLockType.NONE)
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class ZonePricingErrorLog implements Serializable {
	private Integer id; 
	private String eventName;
	private String venueName;
	private Date eventDateTime;
	private Integer noOfTickets;
	private String zone;
	private Float tgOldPrice;
	private Float cap;
	private Date createdDateTime;
	private String errorType;

	@Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="event_date")
	public Date getEventDateTime() {
		return eventDateTime;
	}
	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	
	@Column(name="ticket_qty")
	public Integer getNoOfTickets() {
		return noOfTickets;
	}
	public void setNoOfTickets(Integer noOfTickets) {
		this.noOfTickets = noOfTickets;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="pos_price")
	public Float getTgOldPrice() {
		return tgOldPrice;
	}
	public void setTgOldPrice(Float tgOldPrice) {
		this.tgOldPrice = tgOldPrice;
	}
	@Column(name="zone_price_cap")
	public Float getCap() {
		return cap;
	}
	public void setCap(Float cap) {
		this.cap = cap;
	}
	
	@Column(name="create_datetime")
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Column(name="error_type")
	public String getErrorType() {
		return errorType;
	}
	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	
	
	
	
}
