package com.admitone.tmat.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@DiscriminatorValue(value="VENUE")
public class VenueBookmark extends Bookmark{

	@Transient
	public Venue getVenue(){
		Venue ven = DAORegistry.getVenueDAO().get(getObjectId());
		return ven;
	}
}
