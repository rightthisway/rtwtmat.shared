package com.admitone.tmat.data;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;

@Entity
@Table(name="admitone_event_local")
@DataTransferObject
public class AdmitoneEventLocal implements Serializable {
	
	/* the identifier that uniquely defines an event for admitone */
	protected Integer eventId;
	private String formatedDate;
	protected String eventName;
	protected static DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	protected static DateFormat timeFormat = new SimpleDateFormat("HH:mm zzz");
	
	protected Date eventDate;
	protected Time eventTime;
	protected String venueName;
	protected String cityName;
	protected String stateName;
	
	
	@Id
	@Column(name="EventId")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	@Column(name="EventName")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Column(name="EventDate")
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	@Column(name="EventTime")
	public Time getEventTime() {
		return eventTime;
	}

	public void setEventTime(Time eventTime) {
		this.eventTime = eventTime;
	}

	@Column(name="VenueName")
	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="StateName")
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}
	
	@Column(name="CityName")
	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	
	@Transient
	public String getFormatedDate() {
		// TODO Auto-generated method stub	
		if(getEventDate() == null){
			formatedDate = "TBD";	
		}else{
		formatedDate= dateFormat.format(getEventDate());
		}
		if(getEventTime()!=null){
			formatedDate = formatedDate+" "+timeFormat.format(getEventTime());
		}else{
			formatedDate = formatedDate+" TBD";
		}
		
		return formatedDate;
	}
}
