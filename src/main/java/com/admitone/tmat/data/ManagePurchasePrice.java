package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="manage_purchase_price")
public class ManagePurchasePrice implements Serializable{
	private Integer id;
	private Event event;
	private String exchange;
	private String ticketType;
	private Double serviceFee;
	private Integer currencyType;
	private Double shipping;
	
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@OneToOne
	@JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}

	@Column(name="exchange")
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	
	@Column(name="ticket_type")
	public String getTicketType() {
		return ticketType;
	}
	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}
	
	@Column(name="service_fee")
	public Double getServiceFee() {
		return serviceFee;
	}
	public void setServiceFee(Double serviceFee) {
		this.serviceFee = serviceFee;
	}
	
	@Column(name="shipping")
	public Double getShipping() {
		return shipping;
	}
	public void setShipping(Double shipping) {
		this.shipping = shipping;
	}
	
	@Column(name="currency_type")
	public Integer getCurrencyType() {
		return currencyType;
	}
	public void setCurrencyType(Integer currencyType) {
		this.currencyType = currencyType;
	}
	
	

}
