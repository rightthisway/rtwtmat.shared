package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="tn_exchange_event_floor_cap_audit")

public class TnExchangeEventZoneFloorCapAudit implements Serializable {
	private Integer id;
	private Integer eventId;
	private Integer zoneId;
	private Integer brokerId;
	private String eventName;
	private Date eventDate;
	private String venue;
	private Float oldPriceCap;
	private Float newPriceCap;
	private String userName;
	private Date createdDate;
	private String action;
	private Boolean oldPremiumZoneOption;
	private Boolean newPremiumZoneOption;
	
	
	public TnExchangeEventZoneFloorCapAudit(TnExchageEventZoneFloorCap zoneFloorCap) {
	this.eventId=zoneFloorCap.getExchangeEvent().getEventId();
	this.zoneId = zoneFloorCap.getTnExchangeTheatreEventZones().getId();
	this.eventName = zoneFloorCap.getExchangeEvent().getEventName();
	this.eventDate = zoneFloorCap.getExchangeEvent().getEventDate();
	this.venue = zoneFloorCap.getExchangeEvent().getVenueName();
	this.brokerId=zoneFloorCap.getBrokerId();
		
	}
	
	public TnExchangeEventZoneFloorCapAudit() {
		
	}
	
    @Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="venue")
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="zone_id")
	public Integer getZoneId() {
		return zoneId;
	}
	public void setZoneId(Integer zoneId) {
		this.zoneId = zoneId;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="old_price_cap")
	public Float getOldPriceCap() {
		return oldPriceCap;
	}
	public void setOldPriceCap(Float oldPriceCap) {
		this.oldPriceCap = oldPriceCap;
	}
	
	@Column(name="new_price_cap")
	public Float getNewPriceCap() {
		return newPriceCap;
	}
	public void setNewPriceCap(Float newPriceCap) {
		this.newPriceCap = newPriceCap;
	}

	@Column(name="old_premium_zone_option")
	public Boolean getOldPremiumZoneOption() {
		return oldPremiumZoneOption;
	}

	public void setOldPremiumZoneOption(Boolean oldPremiumZoneOption) {
		this.oldPremiumZoneOption = oldPremiumZoneOption;
	}

	@Column(name="new_premium_zone_option")
	public Boolean getNewPremiumZoneOption() {
		return newPremiumZoneOption;
	}

	public void setNewPremiumZoneOption(Boolean newPremiumZoneOption) {
		this.newPremiumZoneOption = newPremiumZoneOption;
	}

	@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}

	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	
	

	
}
