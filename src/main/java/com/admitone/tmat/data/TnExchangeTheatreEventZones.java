package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OptimisticLockType;


@Entity
@Table(name="tn_exchange_theatre_zones")
@org.hibernate.annotations.Entity(
		dynamicUpdate = true,optimisticLock=OptimisticLockType.NONE)
	@DataTransferObject
	@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class TnExchangeTheatreEventZones implements Serializable {
	private Integer id; 
	private String zone;
	private String symbol;
	
    @Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="symbol")
	public String getSymbol() {
		return symbol;
	}
	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}	
}
