package com.admitone.tmat.data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@Table(name="event")
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_ONLY)
public final class HarvestEvent extends BaseEvent {
	private Venue venue;

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Transient
	public Venue getVenue() {
		if (venueId == null) {
			return null;
		}
		
		if (venue == null) {
			venue = DAORegistry.getVenueDAO().get(venueId); 
		}
		return venue;
	}

	/**
	 * Get a well formatted venue description.
	 * @return
	 */
	@Transient
	public String getFormattedVenueDescription() {
		Venue venue = getVenue();
		if (venue == null) {
			return "n/a";
		}
		
		String[] tokens = {
			venue.getBuilding(),
			venue.getCity(),
			venue.getState(),
			venue.getCountry()				
		};
		
		String v = null;
		for (String token: tokens) {
			if (token == null || token.equals("null")) {
				continue;
			}
			
			if (v == null) {
				v = token;
			} else {
				v += ", " + token;
			}
		}
		return v;
	}
	
	@Transient
	public String toString() {
		String thisEvent = "Event[EventId:" + id 
		+ ", EventName: " + name 
		+ ", Venue: " + this.getFormattedVenueDescription() + "]";
		
		return thisEvent;
	} 
}