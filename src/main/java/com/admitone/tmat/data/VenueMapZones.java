package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="venue_map_zones")
public class VenueMapZones implements Serializable{

	private Integer id;
	private Integer venueId;
	private Integer venueCategoryId;
	private String zone;
	
	//private Boolean mapExistInTmat;
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	
}
