package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="long_transaction")
public final class LongTransaction implements Serializable {
	private Integer id;
	private Integer admitoneId;
	private Integer quantity;
	private String section;
	private String row;
	private String seat;
	private Double wholesale;
	private Double cost;
	private Double selling_price;
	private Date invoiceDate;
	private Date poDate;
	private Double buyingPrice;
	
	public LongTransaction() {
	}

	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="admitone_id")
	public Integer getAdmitoneId() {
		return admitoneId;
	}

	public void setAdmitoneId(Integer admitoneId) {
		this.admitoneId = admitoneId;
	}

	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}

	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name="section")
	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	@Column(name="row")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	@Column(name="seat")
	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	@Column(name="wholesale")
	public Double getWholesale() {
		return wholesale;
	}

	public void setWholesale(Double wholesale) {
		this.wholesale = wholesale;
	}

	@Column(name="cost")
	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public String toString() {		
		return "ShortTransaction[ "
				+ " id: " + id
				+ " admitoneId: " + admitoneId
				+ " quantity: " + quantity
				+ " section: " + section
				+ " row: " + row
				+ " seat: " + seat
				+ " wholesale: " + wholesale
				+ " cost: " + cost;
	}
	@Column(name = "selling_price")
	public Double getSelling_price() {
		return selling_price;
	}

	public void setSelling_price(Double selling_price) {
		this.selling_price = selling_price;
	}
	@Column(name = "invoice_date")
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	@Column(name = "po_date")
	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}
	@Column(name = "po_cost")
	public Double getBuyingPrice() {
		return buyingPrice;
	}

	public void setBuyingPrice(Double buyingPrice) {
		this.buyingPrice = buyingPrice;
	}
	
}
