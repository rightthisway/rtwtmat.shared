package com.admitone.tmat.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@DiscriminatorValue(value="TICKET")
public class TicketBookmark extends Bookmark {
	
	@Transient
	public Ticket getTicket() {
		//return DAORegistry.getTicketDAO().get(Integer.valueOf(getObjectId()));
		return DAORegistry.getTicketDAO().get(getObjectId());
	}
}
