package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "audit_tn_api_call_limit")
public class AuditTnApiCallLimit implements Serializable{

	private Integer id;
	private String project;
	private String createCallDay;
	private String createCallMinute;
	private String deleteCallMinute;
	private String updateCallMinute;
	private String userName;
	private Date createDate;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "project")
	public String getProject() {
		return project;
	}
	public void setProject(String project) {
		this.project = project;
	}
	@Column(name = "create_call_day")
	public String getCreateCallDay() {
		return createCallDay;
	}
	public void setCreateCallDay(String createCallDay) {
		this.createCallDay = createCallDay;
	}
	@Column(name = "create_call_minute")
	public String getCreateCallMinute() {
		return createCallMinute;
	}
	public void setCreateCallMinute(String createCallMinute) {
		this.createCallMinute = createCallMinute;
	}
	@Column(name = "delete_call_minute")
	public String getDeleteCallMinute() {
		return deleteCallMinute;
	}
	public void setDeleteCallMinute(String deleteCallMinute) {
		this.deleteCallMinute = deleteCallMinute;
	}
	@Column(name = "update_call_minute")
	public String getUpdateCallMinute() {
		return updateCallMinute;
	}
	public void setUpdateCallMinute(String updateCallMinute) {
		this.updateCallMinute = updateCallMinute;
	}
	@Column(name = "user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(name = "create_date")
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
	
	
}
