package com.admitone.tmat.data;

public class TnnApiCallLimitTemp {

	private Integer id;
	private String projects;
	private Integer createCallDay;
	private Integer createCallMinute;
	private Integer deleteCallMinute;
	private Integer updateCallMinute;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getProjects() {
		return projects;
	}
	public void setProjects(String projects) {
		this.projects = projects;
	}
	public Integer getCreateCallDay() {
		return createCallDay;
	}
	public void setCreateCallDay(Integer createCallDay) {
		this.createCallDay = createCallDay;
	}
	public Integer getCreateCallMinute() {
		return createCallMinute;
	}
	public void setCreateCallMinute(Integer createCallMinute) {
		this.createCallMinute = createCallMinute;
	}
	public Integer getDeleteCallMinute() {
		return deleteCallMinute;
	}
	public void setDeleteCallMinute(Integer deleteCallMinute) {
		this.deleteCallMinute = deleteCallMinute;
	}
	public Integer getUpdateCallMinute() {
		return updateCallMinute;
	}
	public void setUpdateCallMinute(Integer updateCallMinute) {
		this.updateCallMinute = updateCallMinute;
	}
	
	
	
}
