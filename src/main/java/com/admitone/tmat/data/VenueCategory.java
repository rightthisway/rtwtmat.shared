package com.admitone.tmat.data;

import java.io.File;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="venue_category")
public class VenueCategory implements Serializable{

	private Integer id;
	private String categoryGroup;
	private Venue venue;
	private Integer venueCapacity;
	private Date lastUpdatedDate;
	private String lastUpdatedBy;
	private Boolean isVenuMap;
	
	//private Boolean mapExistInTmat;
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="category_group")
	public String getCategoryGroup() {
		return categoryGroup;
	}
	public void setCategoryGroup(String categoryGroup) {
		this.categoryGroup = categoryGroup;
	}
	
	@ManyToOne
	@JoinColumn(name="venue_id")
	public Venue getVenue() {
		return venue;
	}
	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	
	@Column(name="venue_capacity")
	public Integer getVenueCapacity() {
		return venueCapacity;
	}
	public void setVenueCapacity(Integer venueCapacity) {
		this.venueCapacity = venueCapacity;
	}
	
	@Column(name="last_updated_date")
	public Date getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(Date lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	
	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	
	@Column(name="venue_map")
	public Boolean getIsVenuMap() {
		return isVenuMap;
	}
	public void setIsVenuMap(Boolean isVenuMap) {
		if(null != isVenuMap && isVenuMap){
			isVenuMap = true;
		}else{
			isVenuMap = false;
		}
		this.isVenuMap = isVenuMap;
	}
	
	@Transient
	public Boolean getMapExistInTmat() {
		//mapExistInTmat = false;
		File file = new File("C:/TMATIMAGESFINAL/SvgMaps/" + venue.getId() + "_" +getCategoryGroup()+ ".gif");		
		if (file.exists()) {
			return true;
		}
		return false;
	}	
}
