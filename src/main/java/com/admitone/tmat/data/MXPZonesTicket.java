package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="mxp_zones_tickets")
public class MXPZonesTicket implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Integer id;
	
	@Column(name="tn_event_id")
	private Integer tnEventId;
	
	@Column(name="category_ticket_group_id")
	private Integer categoryTicketGroupId;
	
	@Column(name="section_low")
	private String sectionLow;
	
	@Column(name="row_low")
	private String rowLow;
	
	@Column(name="row_high")
	private String rowHigh;
	
	@Column(name="seat_high")
	private String seatHigh;
	
	@Column(name="seat_low")
	private String seatLow;
	
	@Column(name="item_id")
	private String itemId;
	
	@Column(name="quantity")
	private Integer quantity;
	
	@Column(name="wholesale_price")
	private Double wholesalePrice;
	
	@Column(name="retail_price")
	private Double retailPrice;
	
	@OneToOne
	@JoinColumn(name="event_id")
	private Event event;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getTnEventId() {
		return tnEventId;
	}
	public void setTnEventId(Integer tnEventId) {
		this.tnEventId = tnEventId;
	}
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	public String getSectionLow() {
		return sectionLow;
	}
	public void setSectionLow(String sectionLow) {
		this.sectionLow = sectionLow;
	}
	public String getRowLow() {
		return rowLow;
	}
	public void setRowLow(String rowLow) {
		this.rowLow = rowLow;
	}
	public String getRowHigh() {
		return rowHigh;
	}
	public void setRowHigh(String rowHigh) {
		this.rowHigh = rowHigh;
	}
	public String getSeatHigh() {
		return seatHigh;
	}
	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}
	public String getSeatLow() {
		return seatLow;
	}
	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}
}
