package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="default_exchange_markup_audit")
public class DefaultExchangeMarkupAudit implements Serializable {
	 private Integer id;
	 private String userName;
	 private Date createdDate;
	 private String action;
	// private Double newUpperShippingFees;
	 private Double oldVividSeats;
	 private Double newVividSeats;
	 private Double oldTicketNetwork;
	 private Double newTicketNetwork;
	 private Double oldEbay;
	 private Double newEbay;
	 private Double oldOtherExchange;
	 private Double newOtherExchange;
	 private Double oldScorebig;
	 private Double newScorebig;
	 private Double oldStubhub;
	 private Double newStubhub;
	 private Double oldExchange3;
	 private Double newExchange3;
	 private Double oldExchange4;
	 private Double newExchange4;
	 private Double oldExchange5;
	 private Double newExchange5;
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
		
	@Column(name="modified_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Column(name="old_vivid_seats")
	public Double getOldVividSeats() {
		return oldVividSeats;
	}
	public void setOldVividSeats(Double oldVividSeats) {
		this.oldVividSeats = oldVividSeats;
	}
	@Column(name="new_vivid_seats")
	public Double getNewVividSeats() {
		return newVividSeats;
	}
	public void setNewVividSeats(Double newVividSeats) {
		this.newVividSeats = newVividSeats;
	}
	@Column(name="old_ticket_network")
	public Double getOldTicketNetwork() {
		return oldTicketNetwork;
	}
	public void setOldTicketNetwork(Double oldTicketNetwork) {
		this.oldTicketNetwork = oldTicketNetwork;
	}
	@Column(name="new_ticket_network")
	public Double getNewTicketNetwork() {
		return newTicketNetwork;
	}
	public void setNewTicketNetwork(Double newTicketNetwork) {
		this.newTicketNetwork = newTicketNetwork;
	}
	@Column(name="old_ebay")
	public Double getOldEbay() {
		return oldEbay;
	}
	public void setOldEbay(Double oldEbay) {
		this.oldEbay = oldEbay;
	}
	@Column(name="new_ebay")
	public Double getNewEbay() {
		return newEbay;
	}
	public void setNewEbay(Double newEbay) {
		this.newEbay = newEbay;
	}
	@Column(name="old_tickpick")
	public Double getOldOtherExchange() {
		return oldOtherExchange;
	}
	public void setOldOtherExchange(Double oldOtherExchange) {
		this.oldOtherExchange = oldOtherExchange;
	}
	@Column(name="new_tickpick")
	public Double getNewOtherExchange() {
		return newOtherExchange;
	}
	public void setNewOtherExchange(Double newOtherExchange) {
		this.newOtherExchange = newOtherExchange;
	}
	
	@Column(name="old_scorebig")
	public Double getOldScorebig() {
		return oldScorebig;
	}
	public void setOldScorebig(Double oldScorebig) {
		this.oldScorebig = oldScorebig;
	}
	
	@Column(name="new_scorebig")
	public Double getNewScorebig() {
		return newScorebig;
	}
	public void setNewScorebig(Double newScorebig) {
		this.newScorebig = newScorebig;
	}
	
	@Column(name="old_stubhub")
	public Double getOldStubhub() {
		return oldStubhub;
	}
	public void setOldStubhub(Double oldStubhub) {
		this.oldStubhub = oldStubhub;
	}
	
	@Column(name="new_stubhub")
	public Double getNewStubhub() {
		return newStubhub;
	}
	public void setNewStubhub(Double newStubhub) {
		this.newStubhub = newStubhub;
	}
	
	@Column(name="old_exchange3")
	public Double getOldExchange3() {
		return oldExchange3;
	}
	public void setOldExchange3(Double oldExchange3) {
		this.oldExchange3 = oldExchange3;
	}
	@Column(name="new_exchange3")
	public Double getNewExchange3() {
		return newExchange3;
	}
	public void setNewExchange3(Double newExchange3) {
		this.newExchange3 = newExchange3;
	}
	@Column(name="old_exchange4")
	public Double getOldExchange4() {
		return oldExchange4;
	}
	public void setOldExchange4(Double oldExchange4) {
		this.oldExchange4 = oldExchange4;
	}
	@Column(name="new_exchange4")
	public Double getNewExchange4() {
		return newExchange4;
	}
	public void setNewExchange4(Double newExchange4) {
		this.newExchange4 = newExchange4;
	}
	@Column(name="old_exchange5")
	public Double getOldExchange5() {
		return oldExchange5;
	}
	public void setOldExchange5(Double oldExchange5) {
		this.oldExchange5 = oldExchange5;
	}
	@Column(name="new_exchange5")
	public Double getNewExchange5() {
		return newExchange5;
	}
	public void setNewExchange5(Double newExchange5) {
		this.newExchange5 = newExchange5;
	}
	
}
