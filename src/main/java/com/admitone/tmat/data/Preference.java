package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name="preference")
@IdClass(value=PreferencePK.class)
public class Preference implements Serializable {
	private String username;
	private String name;
	private String value;

	public Preference(String username, String name, String value) {
		this.username = username;
		this.name = name;
		this.value = value;
	}
	
	public Preference() {}
	
	@Id
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Id
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="value")
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
