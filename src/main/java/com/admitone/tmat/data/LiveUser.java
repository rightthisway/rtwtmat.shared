package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="live_user_details")
public class LiveUser implements Serializable {
	private static final long serialVersionUID = 1L;
	private Long id;
	private String username;
	private String appName;
	private Character loggedIn='N';
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="application_name")
	public String getAppName() {
		return appName;
	}
	public void setAppName(String appName) {
		this.appName = appName;
	}
	
	@Column(name="isLoggedIn")
	public Character getLoggedIn() {
		return loggedIn;
	}
	public void setLoggedIn(Character loggedIn) {
		this.loggedIn = loggedIn;
	}
	
}