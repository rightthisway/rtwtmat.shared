package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Tour Price Adjustment Primary Key Class.
 */
@Embeddable
public class TourPriceAdjustmentPk implements Serializable {
	private static final long serialVersionUID = -4312533738477639297L;
	
	private int tourId;
	private String siteId;
	
	public TourPriceAdjustmentPk() {}
	
	public TourPriceAdjustmentPk(int tourId, String siteId) {
		super();
		this.tourId = tourId;
		this.siteId = siteId;
	}

	@Column(name="tour_id")
	public int getTourId() {
		return tourId;
	}
	
	public void setTourId(int tourId) {
		this.tourId = tourId;
	}

	@Column(name="site_id")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	
}
