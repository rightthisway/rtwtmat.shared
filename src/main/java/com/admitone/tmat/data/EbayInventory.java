package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="ebay_inventory")
public class EbayInventory implements Serializable {
	private Integer id;
	private Ticket ticket;
	private Integer groupId;
	private Double wholesalePrice;
	private Double onlinePrice;
	private Double binPrice;
	private Double rpt;
	private Double netProfit;
	private Integer rank;
	
	public EbayInventory() {}

	public EbayInventory(Ticket ticket, Integer rank) {
		this(ticket, null, rank);
	}

	public EbayInventory(Ticket ticket, Integer groupId, Integer rank) {
		this.ticket = ticket;
		this.wholesalePrice = ticket.getAdjustedCurrentPrice();
		this.onlinePrice = ticket.getCurrentPrice();
		this.rank = rank;
		this.groupId = groupId;
	}

	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="group_id")
	public Integer getGroupId() {
		return groupId;
	}
	
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}

	@Column(name="wholesale_price")
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name="online_price")
	public Double getOnlinePrice() {
		return onlinePrice;
	}
	
	public void setOnlinePrice(Double onlinePrice) {
		this.onlinePrice = onlinePrice;
	}

	@Column(name="rank")
	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	@Transient
	public Double getBinPrice() {
		return binPrice;
	}

	public void setBinPrice(Double binPrice) {
		this.binPrice = binPrice;
	}

	@Transient
	public Double getRpt() {
		return rpt;
	}

	public void setRpt(Double rpt) {
		this.rpt = rpt;
	}

	@Transient
	public Double getNetProfit() {
		return netProfit;
	}

	public void setNetProfit(Double netProfit) {
		this.netProfit = netProfit;
	}

	/*
	@Transient
	public Ticket getTicket() {
		Ticket ticket = DAORegistry.getTicketDAO().get(ticketId);
		if (ticket != null) {
			return ticket;
		}
		
		HistoricalTicket hticket = DAORegistry.getHistoricalTicketDAO().get(ticketId);
		if (hticket == null) {
			return null;
		}
		
		return new Ticket(hticket);
	}
	*/

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="ticket_id")
	public Ticket getTicket() {
		return ticket;
	}

	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
/*
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="group_id")
	public EbayInventoryGroup getGroup() {
		return group;
	}

	public void setGroup(EbayInventoryGroup group) {
		this.group = group;
	}
	*/
	

	@Transient
	public Integer getTicketId() {
		if (ticket == null) { 
			return null;
		}
		
		return ticket.getId();
	}

	/*@Transient
	public EbayInventoryGroup getEbayInventoryGroup() {
		return DAORegistry.getEbayInventoryGroupDAO().get(groupId);
	}*/
}
