package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tmat_dependent_artist_merge")
public class TMATDependentArtistMerge implements Serializable{
	private Integer id;
	private Integer artistId;
	private Integer toMergedArtistId;
	private String dependent;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="aritst_id")
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="to_merged_artist_id")
	public Integer getToMergedArtistId() {
		return toMergedArtistId;
	}
	public void setToMergedArtistId(Integer toMergedArtistId) {
		this.toMergedArtistId = toMergedArtistId;
	}
	
	@Column(name="dependent")
	public String getDependent() {
		return dependent;
	}
	public void setDependent(String dependent) {
		this.dependent = dependent;
	}

}
