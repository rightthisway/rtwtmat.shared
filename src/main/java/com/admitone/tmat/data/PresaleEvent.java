package com.admitone.tmat.data;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="presale_event")
public class PresaleEvent implements Serializable {

	private Integer id;
	private Date applyDate;
	private Date createdDate;
	private String createdBy;
	private String lastUpdatedBy;
	private Date lastUpdated;
	private Integer applyDateHour;
	private Integer applyDateMinute;
	private Date applyTime;
	private VenueCategory assignToVenueCategory;
	private Event event;
	private String status;
	private List<VenueCategory> venueCategoryList;
	private SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm:ss");
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="apply_date")
	public Date getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(Date applyDate) {
		this.applyDate = applyDate;
	}

	@OneToOne
	@JoinColumn(name="assign_to_venue_category_id")
	public VenueCategory getAssignToVenueCategory() {
		return assignToVenueCategory;
	}

	public void setAssignToVenueCategory(VenueCategory assignToVenueCategory) {
		this.assignToVenueCategory = assignToVenueCategory;
	}

	@OneToOne
	@JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}

	@Transient
	public List<VenueCategory> getVenueCategoryList() {
		return venueCategoryList;
	}

	public void setVenueCategoryList(List<VenueCategory> venueCategoryList) {
		this.venueCategoryList = venueCategoryList;
	}

	@Column(name="status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Transient
	public Integer getApplyDateHour() {
		if(null != applyTime){
			String temp = sdfTime.format(applyTime);
			applyDateHour = Integer.parseInt(temp.split(":")[0]);
		}else{
			applyDateHour = 0;	
		}
		return applyDateHour;
	}

	public void setApplyDateHour(Integer applyDateHour) {
		this.applyDateHour = applyDateHour;
	}

	@Transient
	public Integer getApplyDateMinute() {
		if(null != applyTime){
			String temp = sdfTime.format(applyTime);
			applyDateMinute = Integer.parseInt(temp.split(":")[1]);
		}else{
			applyDateMinute = 0;	
		}
		return applyDateMinute;
	}

	public void setApplyDateMinute(Integer applyDateMinute) {
		this.applyDateMinute = applyDateMinute;
	}
	
	@Column(name="apply_time")
	public Date getApplyTime() {
		return applyTime;
	}

	public void setApplyTime(Date applyTime) {
		this.applyTime = applyTime;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name="created_by")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name="last_updated_by")
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	
	
}


