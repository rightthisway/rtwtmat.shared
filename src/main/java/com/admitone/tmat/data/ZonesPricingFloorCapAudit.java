package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="zones_pricing_brokers_floor_cap_audit")

public class ZonesPricingFloorCapAudit implements Serializable {
	private Integer id;
	private Integer eventId;
	private String zone;
	private String eventName;
	private Date eventDate;
	private String venue;
	private Float oldPriceCap;
	private Float newPriceCap;
	private String userName;
	private Date createdDate;
	private String action;
	
	
	public ZonesPricingFloorCapAudit(ZonesPricingFloorCap zoneFloorCap) {
	this.eventId=zoneFloorCap.getExchangeEvent().getEventId();
	this.zone = zoneFloorCap.getZone();
	this.eventName = zoneFloorCap.getExchangeEvent().getEventName();
	this.eventDate = zoneFloorCap.getExchangeEvent().getEventDate();
	this.venue = zoneFloorCap.getExchangeEvent().getVenueName();
		
	}
	
	public ZonesPricingFloorCapAudit() {
		
	}
	
    @Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="venue")
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	@Column(name="tn_exchnage_event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="old_floor_cap")
	public Float getOldPriceCap() {
		return oldPriceCap;
	}
	public void setOldPriceCap(Float oldPriceCap) {
		this.oldPriceCap = oldPriceCap;
	}
	
	@Column(name="new_floor_cap")
	public Float getNewPriceCap() {
		return newPriceCap;
	}
	public void setNewPriceCap(Float newPriceCap) {
		this.newPriceCap = newPriceCap;
	}
	
	

	
}
