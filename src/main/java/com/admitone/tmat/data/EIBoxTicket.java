package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="eimp_tickets")
public class EIBoxTicket implements Serializable {
	private Integer ticketId;
	
	// the eibox event is the equivalent of a TOUR
	private Integer eventId;
	private Integer venueId;
	private Date eventDate;
	
	private String section;
	private String row;
	private Integer quantity;
	private Double price;
	
	private Integer lowSeat;
	private Integer highSeat;
	private String note;
	private String companyName;
	
	public EIBoxTicket() {
		
	}
	
	@Id
	@Column(name="TicketID")
	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	@Column(name="PrimaryEventID")
	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="SeatSection")
	public String getSection() {
		return section;
	}
	
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="SeatRow")
	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}
	
	@Column(name="Quantity")
	public Integer getQuantity() {
		return quantity;
	}
	
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@Column(name="TicketPrice")
	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
	@Column(name="StartingSeat")
	public Integer getLowSeat() {
		return lowSeat;
	}

	public void setLowSeat(Integer lowSeat) {
		this.lowSeat = lowSeat;
	}
	
	@Column(name="EndingSeat")
	public Integer getHighSeat() {
		return highSeat;
	}
	
	public void setHighSeat(Integer highSeat) {
		this.highSeat = highSeat;
	}
	
	@Column(name="Note")
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	@Column(name="CompanyName")
	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	@Column(name="VenueID")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="EventDatetime")
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}


}
