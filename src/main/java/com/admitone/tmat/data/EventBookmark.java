package com.admitone.tmat.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@DiscriminatorValue(value="EVENT")
public class EventBookmark extends Bookmark {
	
	@Transient
	public Event getEvent() {
		//return DAORegistry.getEventDAO().get(Integer.parseInt(getObjectId()));
		return DAORegistry.getEventDAO().get(getObjectId());
	}
}
