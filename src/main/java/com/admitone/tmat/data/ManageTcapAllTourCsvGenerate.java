package com.admitone.tmat.data;

public class ManageTcapAllTourCsvGenerate {

	private Integer id;
	private Integer venueId;
	private String eventName;
	private String eventDateTime;
	private String venueName;
	private String tourName;
	private String tourCategoryName;
	private String childCategoryName;
	private String grandChildCategory;
	private Integer crawlCount;
	private String categoryGroupName;
	private String autoCorrect;
	private String ignoreTbdTime;
	private String map;
	private String link;
	private Integer venueCategoryId;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateTime() {
		return eventDateTime;
	}
	public void setEventDateTime(String eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	public String getTourName() {
		return tourName;
	}
	public void setTourName(String tourName) {
		this.tourName = tourName;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getTourCategoryName() {
		return tourCategoryName;
	}
	public void setTourCategoryName(String tourCategoryName) {
		this.tourCategoryName = tourCategoryName;
	}
	public String getChildCategoryName() {
		return childCategoryName;
	}
	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	public String getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	public Integer getCrawlCount() {
		return crawlCount;
	}
	public void setCrawlCount(Integer crawlCount) {
		this.crawlCount = crawlCount;
	}
	public String getCategoryGroupName() {
		return categoryGroupName;
	}
	public void setCategoryGroupName(String categoryGroupName) {
		this.categoryGroupName = categoryGroupName;
	}
	public String getAutoCorrect() {
		return autoCorrect;
	}
	public void setAutoCorrect(String autoCorrect) {
		this.autoCorrect = autoCorrect;
	}
	public String getIgnoreTbdTime() {
		return ignoreTbdTime;
	}
	public void setIgnoreTbdTime(String ignoreTbdTime) {
		this.ignoreTbdTime = ignoreTbdTime;
	}
	public String getMap() {
		return map;
	}
	public void setMap(String map) {
		this.map = map;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
		
	
}
