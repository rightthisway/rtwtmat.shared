package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="manage_broker_audit")
public class ManageBrokerAudit implements Serializable {
	 private Integer id;
//	 private Event event;
//	 private Broker broker;
	 
	 private Integer eventId;
 	 
	 private String userName;
	 private Date createdDate;
	 private String action;
	 private Integer brokerId;
	 //private String broker_name;
	 private String brokerStatus;
	 
	

	public ManageBrokerAudit() {
		
	}


	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	/*@ManyToOne
	@JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}*/
	
	
	

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId =eventId;
	}
	
	
	
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}


	@Column (name="broker_id")
	public Integer getBrokerId() {
		return brokerId; 
	}
	public void setBrokerId(Integer brokerId) {
		this.brokerId=brokerId;
	}

	/*@Column (name="broker_name")
	public String  getBroker_Name() {
		return broker_name; 
	}
	public void setBroker_Name(String broker_name) {
		this.broker_name=broker_name;
	}*/
	
	@Column (name="broker_status")
	public String getBrokerStatus() {
		return brokerStatus; 
	}
	public void setBrokerStatus(String  brokerStatus) {
		this.brokerStatus=brokerStatus;
	}
	
}
