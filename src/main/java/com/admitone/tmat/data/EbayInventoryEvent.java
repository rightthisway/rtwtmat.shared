package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@Table(name="ebay_inventory_event")
public class EbayInventoryEvent implements Serializable {
	private String username;
	private Date creationDate;
	private String notes;
	private Double rptFactor;
	private Integer id;
	private Long numActiveGroups;
	private Long numPendingGroups;
	private Integer exposure;
	private Boolean enableZoneMaps;	
//	private Integer tourId;
	
	public EbayInventoryEvent(Integer eventId, String username, Date creationDate) {
		this.id = eventId;
		this.username = username;
		this.creationDate = creationDate;
		this.rptFactor = 25.00;
		this.exposure = 3;
		this.enableZoneMaps = false;
	}
	
	@Column(name="exposure")
	public Integer getExposure() {
		return exposure;
	}

	public void setExposure(Integer exposure) {
		this.exposure = exposure;
	}

	@Id
	@Column(name="event_id")
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public EbayInventoryEvent() {}

	@Transient
	public Integer getEventId() {
		return id;
	}
	
	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}

	@Column
	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}
	
	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	@Column(name="rpt_factor")
	public Double getRptFactor() {
		return rptFactor;
	}

	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}

	@Column(name="enable_zone_maps")
	public Boolean getEnableZoneMaps() {
		return enableZoneMaps;
	}

	public void setEnableZoneMaps(Boolean enableZoneMaps) {
		this.enableZoneMaps = enableZoneMaps;
	}
	
	/*@Transient
	public Collection<EbayInventoryGroup> getEbayInventoryGroups() {
		return DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByEventId(id);
	}*/
	
	@Transient
	public Event getEvent() {
		return DAORegistry.getEventDAO().get(id);
	}
	
	/*@Transient
	public EbayInventoryTour getEbayInventoryTour() {
		return DAORegistry.getEbayInventoryTourDAO().get(getEvent().getTourId());
	}*/

	/*@Transient
	public Long getNumActiveGroups() {
		if (numActiveGroups == null)  {
			numActiveGroups = DAORegistry.getEbayInventoryGroupDAO().getNumInventoryGroups(id, EbayInventoryStatus.ACTIVE);
		}
		return numActiveGroups;
	}*/

	/*@Transient
	public Long getNumPendingGroups() {
		if (numPendingGroups == null) {
			numPendingGroups = DAORegistry.getEbayInventoryGroupDAO().getNumPendingInventoryGroups(id);
		}
		return numPendingGroups;
	}*/

/*
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="event_id", insertable=false, updatable=false)
	public Event getEvent() {
		return DAORegistry.getEventDAO().get(event.getId());
	}
	
	public void setEvent(Event event) {
		this.event = event;
	}
	@Column(name="tour_id")
	public Integer getTourId() {
		return tourId;
	}

	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}
	*/

}
