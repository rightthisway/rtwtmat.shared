package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="venue_probable_synonyms")
public class ProbableVenue implements Serializable {
	private Integer id;
	private Integer venueId;
	private String siteId;
	private String probleName;
	private Boolean markedAsSynonym;
	private Date createdDate;
	private Date updatedDate;
	private String creator;
	
	@Id
	@Column(name="id",  columnDefinition="INTEGER AUTO_INCREMENT")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="probable_name")
	public String getProbleName() {
		return probleName;
	}
	public void setProbleName(String probleName) {
		this.probleName = probleName;
	}
	
	
	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	@Column(name="site_id")
	public String getSiteId() {
		if(null != siteId && !siteId.isEmpty()){
			siteId = siteId.toUpperCase();
		}
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	@Column(name="marked_as_synonym")
	public Boolean getMarkedAsSynonym() {
		return markedAsSynonym;
	}
	public void setMarkedAsSynonym(Boolean markedAsSynonym) {
		this.markedAsSynonym = markedAsSynonym;
	}
	@Column(name="creator")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}
	
}
