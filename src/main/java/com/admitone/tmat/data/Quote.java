package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="quote")
public class Quote implements Serializable {
	private Integer id;
	private QuoteCustomer customer;
	private String username;
	private java.sql.Date quoteDate;
	private Date creationDate;
	private Date sentDate;
	private String sentTo;
	private String referral;
	private String quoteType;
    public Quote(String username, QuoteCustomer customer, java.sql.Date quoteDate) {
		this.username = username;
		this.customer = customer;
		this.quoteDate = quoteDate;
		this.creationDate = new Date();
	}
	
	public Quote() {}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="customer_id")
	public QuoteCustomer getCustomer() {
		return customer;
	}

	public void setCustomer(QuoteCustomer customer) {
		this.customer = customer;
	}

	@Column(name="username")
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	@Column(name="quote_date")
	public java.sql.Date getQuoteDate() {
		return quoteDate;
	}
	
	public void setQuoteDate(java.sql.Date quoteDate) {
		this.quoteDate = quoteDate;
	}
	
	@Column(name="sent_date")
	public Date getSentDate() {
		return sentDate;
	}
	
	public void setSentDate(Date sentDate) {
		this.sentDate = sentDate;
	}
	
	@Column(name="creation_date")
	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}	
	
	@Column(name="sent_to")
	public String getSentTo() {
		return sentTo;
	}
	public void setSentTo(String sentTo) {
		this.sentTo = sentTo;
	}
	@Column(name="type")
	public String getQuoteType() {
		return quoteType;
	}
	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}
	@Column(name="referral")
	public String getReferral() {
		return referral;
	}
	public void setReferral(String referral) {
		this.referral = referral;
	}

	
		
}