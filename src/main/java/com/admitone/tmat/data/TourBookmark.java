package com.admitone.tmat.data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="TOUR")
public class TourBookmark extends Bookmark {
	
	/*@Transient
	public Tour getTour() {
		return DAORegistry.getTourDAO().get(getObjectId());
	}*/
}
