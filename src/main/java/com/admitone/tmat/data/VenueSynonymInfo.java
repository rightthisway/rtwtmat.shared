package com.admitone.tmat.data;

import java.util.List;

 
public class VenueSynonymInfo {

	private Integer venueId;
	private List<VenueSynonym> addedSynonyms;
	private List<ProbableVenue> probableVenues;
	private Venue venue;
	private String creator;
	private String createdTime;
	
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	public List<VenueSynonym> getAddedSynonyms() {
		return addedSynonyms;
	}
	public void setAddedSynonyms(List<VenueSynonym> addedSynonyms) {
		this.addedSynonyms = addedSynonyms;
	}
	public List<ProbableVenue> getProbableVenues() {
		return probableVenues;
	}
	public void setProbableVenues(List<ProbableVenue> probableVenues) {
		this.probableVenues = probableVenues;
	}
	
	public Venue getVenue() {
		/*if(venueId != null){
			venue = DAORegistry.getVenueDAO().get(venueId);
		}*/
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	public String getCreator() {
		return creator;
	}
	public void setCreator(String creator) {
		this.creator = creator;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
	}

	
}
