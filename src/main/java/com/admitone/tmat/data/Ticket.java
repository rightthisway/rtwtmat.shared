package com.admitone.tmat.data;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OptimisticLockType;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.admitone.tmat.utils.Categorizer;
import com.thoughtworks.xstream.annotations.XStreamOmitField;


@Entity
@Table(name="ticket")
@org.hibernate.annotations.Entity(
	dynamicUpdate = true,optimisticLock=OptimisticLockType.NONE)
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
@Transactional(isolation=Isolation.READ_UNCOMMITTED)
public class Ticket extends BaseTicket {
	private Integer dupRefTicketId; // reference ticket which replaces me (if I'm a dupe) 
	private String eventName;
	private String venue;
	private Date eventDate;
	private Date eventTime;
	private Integer categoryId;
	private Integer categoryMappingId;
	private CategoryMapping categoryMapping;
	private String filename;
	private ManageTourPrice manageTourPrice;
	private Integer catId;
	private String venueCategoryGroup;
	@XStreamOmitField
	private Double purchasePrice=0.0;
	@XStreamOmitField
	private String priceByQuantity;
	
	public Ticket() {
	}

	public Ticket(BaseTicket baseTicket) {
		setDupRefTicketId(null);
		//setAdjustedCurrentPrice(baseTicket.getAdjustedCurrentPrice());
		setBuyItNowPrice(baseTicket.getBuyItNowPrice());
		setCurrentPrice(baseTicket.getAdjustedCurrentPrice());
		setEndDate(baseTicket.getEndDate());
		setEventId(baseTicket.getEventId());
		setId(baseTicket.getId());
		setInsertionDate(baseTicket.getInsertionDate());
		setItemId(baseTicket.getItemId());
		setLastUpdate(baseTicket.getLastUpdate());
		setLotSize(baseTicket.getLotSize());
//		setNormalizedRow(baseTicket.getNormalizedRow());
		setNormalizedSection(baseTicket.getNormalizedSection());
//		setNormalizedSeat(baseTicket.getNormalizedSeat());
		setPriceUpdateCount(baseTicket.getPriceUpdateCount());
		setQuantity(baseTicket.getQuantity());
		setRemainingQuantity(baseTicket.getRemainingQuantity());
		setRow(baseTicket.getRow());
		setSection(baseTicket.getSection());
		setSeller(baseTicket.getSeller());
		//setShortPrice(baseTicket.getShortPrice());
		setSiteId(baseTicket.getSiteId());
		setSoldQuantity(baseTicket.getSoldQuantity());
		setSeat(baseTicket.getNormalizedSeat());
		setTicketDeliveryType(baseTicket.getTicketDeliveryType());
		setTicketListingCrawlId(baseTicket.getTicketListingCrawlId());
		setTicketStatus(baseTicket.getTicketStatus());
		setTicketType(baseTicket.getTicketType());
		setInHand(baseTicket.isInHand());
		//setTitle(baseTicket.getTitle());
	}

	public Ticket(String siteId, String itemId) {
		setSiteId(siteId);
		setItemId(itemId);
	}
	
	public String toString() {
		return super.toString();
	}

	@Transient
	public Integer getDupRefTicketId() {
		return dupRefTicketId;
	}

	public void setDupRefTicketId(Integer dupRefTicketId) {
		this.dupRefTicketId = dupRefTicketId;
	}
	
	@Transient
	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	@Transient
	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	@Transient
	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	@Transient
	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	@Transient
	public Integer getCategoryId(VenueCategory venueCategory,String groupName,Map<Integer,Category>catMap,Map<Integer,List<CategoryMapping>> catMappingMap) {
		if(category != null && category.getGroupName().equals(groupName)){
			return category.getId();
		} else {
			category = Categorizer.computeCategory(venueCategory,this.getNormalizedSection(),this.getRow(),this.getSeat(),catMap,catMappingMap);
			if(category == null) {
				uncategorized = true;				
			}
		}
		if(category == null){
			return null;
		}
		return category.getId();
	}
	
	
	@Transient
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	@Transient
	public Integer getCatId() {
		return catId ;
	}
	
	@Transient
	public void setCatId(Integer catId) {
		this.catId = catId;
	}
	
	@Transient
	public Integer getCategoryMappingId() {
		return categoryMappingId;
	}

	public void setCategoryMappingId(Integer categoryMappingId) {
		this.categoryMappingId = categoryMappingId;
	}
	
	@Transient
	public CategoryMapping getCategoryMapping() {
		return categoryMapping;
	}

	public void setCategoryMapping(CategoryMapping categoryMapping) {
		this.categoryMapping = categoryMapping;
	}
	
	@Transient
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}

	@Transient
	public ManageTourPrice getManageTourPrice(Integer tourId) {
		if(manageTourPrice==null){
			
		}
		return manageTourPrice;
	}

	public void setManageTourPrice(ManageTourPrice manageTourPrice) {
		this.manageTourPrice = manageTourPrice;
	}
	
	@Transient
	public Double getPurchasePrice() {		
		return purchasePrice;
	}
	public void setPurchasePrice(Double purchasePrice) {		
		this.purchasePrice = purchasePrice;
	}
	
	@Transient
	public Category getCategory() {
		return category;
	}
	
	public void setCategory(Category category) {
		this.category = category;
		if (category == null) {
			uncategorized = true;
		}
	}
	
	@Transient
	public String getVenueCategoryGroup() {
		return venueCategoryGroup;
	}
	
	public void setVenueCategoryGroup(String venueCategoryGroup) {
		this.venueCategoryGroup = venueCategoryGroup;
	}

	@Transient
	public String getPriceByQuantity() {
		return priceByQuantity;
	}

	public void setPriceByQuantity(String priceByQuantity) {
		this.priceByQuantity = priceByQuantity;
	}
	
/*	public void setCategory(Category category) {
		this.category = category;
		if (category == null) {
			uncategorized = true;
		}
	}
	*/
	
}
