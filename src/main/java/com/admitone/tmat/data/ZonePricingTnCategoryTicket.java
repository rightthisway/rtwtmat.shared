package com.admitone.tmat.data;

import java.io.Serializable;

public class ZonePricingTnCategoryTicket implements Serializable{

	   private Integer id;
	   private Integer quantity;
	   private String section;
	   private String row;
	   private String zone;
	   private Double price;
	   private String expectedArrivalDate;
	   private Integer shippingMethodId;
	   private Integer nearTermDisplayOptionId;
	   private String eventName;
	   private String eventDate;
	   private String eventTime;
	   private String venue;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(String expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	public Integer getNearTermDisplayOptionId() {
		return nearTermDisplayOptionId;
	}
	public void setNearTermDisplayOptionId(Integer nearTermDisplayOptionId) {
		this.nearTermDisplayOptionId = nearTermDisplayOptionId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	} 	
	
	
	
	
	
	
	
	
	
	
}
