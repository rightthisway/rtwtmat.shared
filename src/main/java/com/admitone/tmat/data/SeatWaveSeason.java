package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class to store seatwave season(tour).
 */

@Entity
@Table(name="seatwave_season")
public class SeatWaveSeason implements Serializable {
	
	private Integer id;
	
	public static final int GENRE_CONCERTS_ID = 1;
	public static final int GENRE_SPORT_ID = 2;
	public static final int GENRE_THEATRE_ID = 3;
	
	private String name;
	private String url;
	private Integer genreId;
	
	private Date updatedDate;
	
	public SeatWaveSeason() {
	}
	
	public SeatWaveSeason(Integer id) {
		this.id = id;
	}
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name="name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="url")
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Column(name="genre_id")
	public Integer getGenreId() {
		return genreId;
	}

	public void setGenreId(Integer genreId) {
		this.genreId = genreId;
	}

	@Column(name="updated_date")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	
}
