package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="venue_synonym")
public class VenueSynonym implements Serializable {
	private Integer id;
	private Integer venueId;
	private Integer probableVenueId;
	private String value;
	private String creator;
	private Date createdDate;
	private Venue venue;
	
	public VenueSynonym() {
		
	}
	
	public VenueSynonym(Integer venueId, String value) {
		this.venueId = venueId;
		this.value = value;
	}
	@Id
	@Column(name="id",  columnDefinition="INTEGER AUTO_INCREMENT")
    @GeneratedValue(strategy=GenerationType.AUTO)	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="value")
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	@Column(name="creator")
	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Transient
	public Venue getVenue() {
		/*if(venueId != null){
			venue = DAORegistry.getVenueDAO().get(venueId);
		}*/
		return venue;
	}

	public void setVenue(Venue venue) {
		this.venue = venue;
	}
	@Column(name="probable_venue_id")
	public Integer getProbableVenueId() {
		return probableVenueId;
	}

	public void setProbableVenueId(Integer probableVenueId) {
		this.probableVenueId = probableVenueId;
	}

	
	
}
