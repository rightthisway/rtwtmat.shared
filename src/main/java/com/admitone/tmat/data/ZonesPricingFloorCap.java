package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="zones_pricing_brokers_floor_cap")
public class ZonesPricingFloorCap implements Serializable {
	private Integer id; 
	private AdmitoneEvent exchangeEvent; 
	private String zone; 
	private String uiZone; 
	private Float cap;
	private Date createdDate;
	private Date lastupdate;
	private String dayOfWeek;
	
	public ZonesPricingFloorCap(ZonesPricingFloorCap zapt){
		this.id =zapt.id;	
		this.cap=zapt.cap;
		this.zone=zapt.zone;
		this.exchangeEvent=zapt.exchangeEvent;
		this.lastupdate=zapt.lastupdate;
		this.createdDate=zapt.createdDate;
		this.dayOfWeek=zapt.dayOfWeek;
	}
	
	
	public ZonesPricingFloorCap(){
		
	}
	
	
    @Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	
	
	
	@Column(name="zone_floor_cap")
	public Float getCap() {
		return cap;
	}
	public void setCap(Float cap) {
		this.cap = cap;
	}
	
	
	
	@OneToOne
	@JoinColumn(name="tn_exchnage_event_id")
	public AdmitoneEvent getExchangeEvent() {
		return exchangeEvent;
	}

	public void setExchangeEvent(AdmitoneEvent exchangeEvent) {
		this.exchangeEvent = exchangeEvent;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}

	public void setZone(String zone) {
		this.zone = zone;
	}


	@Column(name="last_update")
	public Date getLastupdate() {
		return lastupdate;
	}
	public void setLastupdate(Date lastupdate) {
		this.lastupdate = lastupdate;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Transient
	public String getDayOfWeek() {
		return dayOfWeek;
	}

	@Transient
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}

	@Transient
	public String getUiZone() {
		return uiZone;
	}


	public void setUiZone(String uiZone) {
		this.uiZone = uiZone;
	}
	
	
	
}
