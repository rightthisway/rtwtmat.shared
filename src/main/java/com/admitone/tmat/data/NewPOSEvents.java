package com.admitone.tmat.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="new_pos_events")
public class NewPOSEvents implements Serializable{
	static DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
	static DateFormat tf = new SimpleDateFormat("hh:mm aa");
	static DateFormat dateTimeformat = new SimpleDateFormat("MM/dd/yyyy HH:mm");
	
	private Integer id;
	private String name;
	private Date date;
	private Date time;
	private String venue;
	private String city;
	private String state;
	private String parentCategory;
	private String childCategory;
	private String grandChildCategory;
	private Date createdDate;
	private String dateStr;
	private String timeStr;
	private String createdDateStr;
	private String eventExistinTMAT;
	private String tmatParentCategory;
	private String isParentTypeMismatch;
	private String categoryGroup;
	private String tmatArtistName;
	
	@Id
	@Column(name="id")
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	@Column(name="date")
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
	@Column(name="time")
	public Date getTime() {
		return time;
	}
	public void setTime(Date time) {
		this.time = time;
	}
	
	@Column(name="venue")
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	@Column(name="city")
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	@Column(name="state")
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	@Column(name="parent_category")
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	@Column(name="child_category")
	public String getChildCategory() {
		return childCategory;
	}
	public void setChildCategory(String childCategory) {
		this.childCategory = childCategory;
	}
	@Column(name="grandchild_category")
	public String getGrandChildCategory() {
		return grandChildCategory;
	}
	public void setGrandChildCategory(String grandChildCategory) {
		this.grandChildCategory = grandChildCategory;
	}
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Transient
	public String getTmatArtistName() {
		return tmatArtistName;
	}
	@Transient
	public void setTmatArtistName(String tmatArtistName) {
		this.tmatArtistName = tmatArtistName;
	}
	
	@Transient
	public String getDateStr() {
		if(date != null) {
			dateStr = df.format(date);
		}
		return dateStr;
	}
	
	@Transient
	public void setDateStr(String dateStr) {
		this.dateStr = dateStr;
	}
	@Transient
	public String getTimeStr() {
		if(time != null) {
			timeStr = tf.format(time);
		}
		return timeStr;
	}
	@Transient
	public void setTimeStr(String timeStr) {
		this.timeStr = timeStr;
	}
	@Transient
	public String getCreatedDateStr() {
		if(createdDate != null) {
			createdDateStr = dateTimeformat.format(createdDate);
		}
		return createdDateStr;
	}
	@Transient
	public void setCreatedDateStr(String createdDateStr) {
		this.createdDateStr = createdDateStr;
	}
	@Transient
	public String getEventExistinTMAT() {
		return eventExistinTMAT;
	}
	@Transient
	public void setEventExistinTMAT(String eventExistinTMAT) {
		this.eventExistinTMAT = eventExistinTMAT;
	}
	
	@Transient
	public String getTmatParentCategory() {
		return tmatParentCategory;
	}
	
	@Transient
	public void setTmatParentCategory(String tmatParentCategory) {
		this.tmatParentCategory = tmatParentCategory;
	}
	
	@Transient
	public String getIsParentTypeMismatch() {
		return isParentTypeMismatch;
	}
	
	@Transient
	public void setIsParentTypeMismatch(String isParentTypeMismatch) {
		this.isParentTypeMismatch = isParentTypeMismatch;
	}
	
	@Transient
	public String getCategoryGroup() {
		return categoryGroup;
	}
	public void setCategoryGroup(String categoryGroup) {
		this.categoryGroup = categoryGroup;
	}
	
}
