package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

/**
 * Artist Price Adjustment Class.
 */
@Entity
@Table(name="artist_price_adjustment")
@IdClass(value=ArtistPriceAdjustmentPk.class)
public class ArtistPriceAdjustment implements Serializable {
	private static final long serialVersionUID = -5462515495303661754L;

	private int artistId;
	private String siteId;
	
	private Double percentAdjustment;

	@Id
	@Column(name="artist_id")
	public int getArtistId() {
		return artistId;
	}

	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}

	@Id
	@Column(name="site_id")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	@Column(name="percentage")
	public Double getPercentAdjustment() {
		return percentAdjustment;
	}

	public void setPercentAdjustment(Double percentAdjustment) {
		this.percentAdjustment = percentAdjustment;
	}
	
}
