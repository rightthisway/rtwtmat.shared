package com.admitone.tmat.data;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.web.ArtistCommand;

@Entity
@Table(name="artist")
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class Artist extends BaseArtist {
	List<Event> events ;
	public Artist() {
		//this.setArtistStatus(ArtistStatus.ACTIVE);
		super();
	}
	public Artist(ArtistCommand artistCommand){
//		this.id = artistCommand.id;
		this.setGrandChildTourCategoryId(artistCommand.getGrandChildTourCategoryId());
		this.setId(artistCommand.getId());
		this.setName(artistCommand.getName());
	}
	
	@Transient
	public List<Event> getEvents(){
		if(events==null || events.isEmpty()){
			events = (List<Event>)DAORegistry.getEventDAO().getAllActiveEventsByArtistId(this.getId());
		}
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}
}
