package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tixcity_unfilled_order_summary")
public class TixcityUnfilledOrderSummary implements Serializable {
	 private Integer id;
	 private Double totalAmount;
	 private Double grossAmount;
	 private Double grossPercentage;
	 private Date createdDate;
	 private Date summaryDate;
	 
	 @Id
	 @Column(name="id")
	 @GeneratedValue(strategy=GenerationType.AUTO)
	 public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="total_amount")
	public Double getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(Double totalAmount) {
		this.totalAmount = totalAmount;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	@Column(name="gross_amount")
	public Double getGrossAmount() {
		return grossAmount;
	}
	public void setGrossAmount(Double grossAmount) {
		this.grossAmount = grossAmount;
	}
	@Column(name="gross_percentage")
	public Double getGrossPercentage() {
		return grossPercentage;
	}
	public void setGrossPercentage(Double grossPercentage) {
		this.grossPercentage = grossPercentage;
	}
	
	@Column(name="summary_date")
	public Date getSummaryDate() {
		return summaryDate;
	}
	public void setSummaryDate(Date summaryDate) {
		this.summaryDate = summaryDate;
	}
	 
	 
}
