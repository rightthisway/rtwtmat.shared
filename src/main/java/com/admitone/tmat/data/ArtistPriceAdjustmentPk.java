package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Artist Price Adjustment Primary Key Class.
 */
@Embeddable
public class ArtistPriceAdjustmentPk implements Serializable {
	private static final long serialVersionUID = -4265283705716861987L;
	private int artistId;
	private String siteId;
	
	public ArtistPriceAdjustmentPk() {}
	
	public ArtistPriceAdjustmentPk(int artistId, String siteId) {
		super();
		this.artistId = artistId;
		this.siteId = siteId;
	}

	@Column(name="artist_id")
	public int getArtistId() {
		return artistId;
	}
	
	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}

	@Column(name="site_id")
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	
}
