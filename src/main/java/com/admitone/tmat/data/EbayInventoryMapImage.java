package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="ebay_inventory_image_url")
public class EbayInventoryMapImage implements Serializable {
	private Integer tourId;
	private Integer eventId;
	private String category;
	private String url;
	
	public EbayInventoryMapImage(Integer tourId, Integer eventId, String category, String url) {
		this.tourId = tourId;
		this.eventId = eventId;
		this.category = category;
		this.url = url;
	}
	
	public EbayInventoryMapImage() {}
	
	@Column(name="tour_id")
	public Integer getTourId() {
		return tourId;
	}
	
	public void setTourId(Integer tourId) {
		this.tourId = tourId;
	}

	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	@Column(name="url")
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
}
