package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.directwebremoting.annotations.DataTransferObject;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.OptimisticLockType;


@Entity
@Table(name="tn_exchange_event_zone_group_ticket")
@org.hibernate.annotations.Entity(
	dynamicUpdate = true,optimisticLock=OptimisticLockType.NONE)
@DataTransferObject
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class TnExchangeEventZoneGroupTicket implements Serializable {
	private Integer id; 
	private Integer categoryTicketGroupId; 
	private Integer eventId; 
	private String eventName;
	private Integer venueId; 
	private String venueName;
	private Date eventDateTime;
	private Integer noOfTickets;
	private String zone;
	private Float tgOldPrice;
	private Float tgNewPrice;
	private Float tmatLowestPrice;
	private Float cap;
	private String dayOfWeek;
	private Date lastupdate;
	private Date createdDateTime;
	private Float capDeduction;
	private Integer posHit;
	private String hitResult;
	private String apiResponse;
	private Integer pricingVersion;
	
    @Id
   	@Column(name="id")
   	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="event_date")
	public Date getEventDateTime() {
		return eventDateTime;
	}
	public void setEventDateTime(Date eventDateTime) {
		this.eventDateTime = eventDateTime;
	}
	
	@Column(name="ticket_qty")
	public Integer getNoOfTickets() {
		return noOfTickets;
	}
	public void setNoOfTickets(Integer noOfTickets) {
		this.noOfTickets = noOfTickets;
	}
	
	@Column(name="zone")
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	@Column(name="tg_old_price")
	public Float getTgOldPrice() {
		return tgOldPrice;
	}
	public void setTgOldPrice(Float tgOldPrice) {
		this.tgOldPrice = tgOldPrice;
	}
	
	@Column(name="tg_new_price")
	public Float getTgNewPrice() {
		return tgNewPrice;
	}
	public void setTgNewPrice(Float tgNewPrice) {
		this.tgNewPrice = tgNewPrice;
	}
	
	@Column(name="tmat_cheapest_price")
	public Float getTmatLowestPrice() {
		return tmatLowestPrice;
	}
	public void setTmatLowestPrice(Float tmatLowestPrice) {
		this.tmatLowestPrice = tmatLowestPrice;
	}
	
	@Column(name="zone_price_cap")
	public Float getCap() {
		return cap;
	}
	public void setCap(Float cap) {
		this.cap = cap;
	}
	
	@Column(name="day_of_week")
	public String getDayOfWeek() {
		return dayOfWeek;
	}
	public void setDayOfWeek(String dayOfWeek) {
		this.dayOfWeek = dayOfWeek;
	}
	
	
	@Column(name="last_update")
	public Date getLastupdate() {
		return lastupdate;
	}
	public void setLastupdate(Date lastupdate) {
		this.lastupdate = lastupdate;
	}
	@Column(name="cap_deduction")
	public Float getCapDeduction() {
		return capDeduction;
	}

	public void setCapDeduction(Float capDeduction) {
		this.capDeduction = capDeduction;
	}
	
	@Column(name="create_datetime")
	public Date getCreatedDateTime() {
		return createdDateTime;
	}
	public void setCreatedDateTime(Date createdDateTime) {
		this.createdDateTime = createdDateTime;
	}
	
	@Column(name="category_ticket_group_id")
	public Integer getCategoryTicketGroupId() {
		return categoryTicketGroupId;
	}
	public void setCategoryTicketGroupId(Integer categoryTicketGroupId) {
		this.categoryTicketGroupId = categoryTicketGroupId;
	}
	
	@Column(name="pos_hit")
	public Integer getPosHit() {
		return posHit;
	}
	public void setPosHit(Integer posHit) {
		this.posHit = posHit;
	}
	
	@Column(name="hit_result")
	public String getHitResult() {
		return hitResult;
	}
	public void setHitResult(String hitResult) {
		this.hitResult = hitResult;
	}
	
	@Column(name="api_response")
	public String getApiResponse() {
		return apiResponse;
	}
	public void setApiResponse(String apiResponse) {
		this.apiResponse = apiResponse;
	}
	
	@Column(name="pricing_version")
	public Integer getPricingVersion() {
		return pricingVersion;
	}
	public void setPricingVersion(Integer pricingVersion) {
		this.pricingVersion = pricingVersion;
	}
	
}
