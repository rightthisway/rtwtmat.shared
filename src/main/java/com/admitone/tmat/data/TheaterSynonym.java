package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@SuppressWarnings("serial")
@Entity
@Table(name="theater_synonym")

public class TheaterSynonym implements Serializable {

	private String name;
	private String value;
	
	
	public TheaterSynonym() {
		
	}
	
	public TheaterSynonym(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Id
	@Column(name="value")
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}