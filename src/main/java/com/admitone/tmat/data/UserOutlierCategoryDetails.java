package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.admitone.tmat.dao.DAORegistry;

@Entity
@Table(name="user_outlier_category_price_detail")
public class UserOutlierCategoryDetails implements Serializable {
	private static final long serialVersionUID = 1L;
	private Integer id;
	private Integer outlierId;
	private Integer eventId;
	private Integer categoryId;
	private Integer venueCategoryId;
	private Integer venueId;
	private String categoryGroupName;
	private String category;
	private Double firstcheapestamount;
	private Date lastUpdate;
	private Double secondcheapestamount;
	private Integer status;
	private Integer ticketId;
	private Integer count;
	private Event event;

	
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}


	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	@Transient
	public Ticket getTicket() {
		Ticket ticket = DAORegistry.getTicketDAO().get(ticketId);
		if (ticket == null) {
			return null;
		}
		return ticket;
	}

	@Transient
	public Event getEvent() {
		if(event==null){
			event = DAORegistry.getEventDAO().get(eventId);
		}
		return event;
	}

	public void setEvent(Event event) {
		this.event = event;
	}
	@Transient
	public Artist getArtist() {
		return getEvent().getArtist();
	}

	@Transient
	public UserOutlier getOutlier() {
		if(null != outlierId){
			return DAORegistry.getUserOutlierDAO().get(outlierId);
		}
		return null;
	}
	
	@Transient
	public Category getCategoryObject() {
		if(null != categoryId){
			return DAORegistry.getCategoryDAO().get(categoryId);
		}
		return null;
	}
	
	@Transient
	public Venue getVenue() {
		if(null != venueId){
			return DAORegistry.getVenueDAO().get(venueId);
		}
		return null;
	}
	
	@Transient
	public VenueCategory getVenueCategory() {
		if(null != venueCategoryId){
			return DAORegistry.getVenueCategoryDAO().get(venueCategoryId);
		}
		return null;
	}
	
	
	@Column(name="category")
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	
	@Column(name="category_group_name")
	public String getCategoryGroupName() {
		return categoryGroupName;
	}

	public void setCategoryGroupName(String categoryGroupName) {
		this.categoryGroupName = categoryGroupName;
	}

	
	@Column(name="last_update")
	public Date getLastUpdate() {
		return lastUpdate;
	}

	
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate= lastUpdate;
	}
	
	@Column(name="outlier_id")
	public Integer getOutlierId() {
		return outlierId;
	}

	public void setOutlierId(Integer outlierId) {
		this.outlierId = outlierId;
	}

	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	@Column(name="venue_category_id")
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}

	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}

	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	@Column(name="first_cheapest_amount")
	public Double getFirstcheapestamount() {
		return firstcheapestamount;
	}

	public void setFirstcheapestamount(Double firstcheapestamount) {
		this.firstcheapestamount = firstcheapestamount;
	}
	@Column(name="second_cheapest_amount_history")
	public Double getSecondcheapestamount() {
		return secondcheapestamount;
	}

	public void setSecondcheapestamount(Double secondcheapestamount) {
		this.secondcheapestamount = secondcheapestamount;
	}
	@Column(name="count")
	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}