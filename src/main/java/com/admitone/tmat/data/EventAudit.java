package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="event_audit")
public class EventAudit implements Serializable {
	 private Integer id;
	 private Integer eventId;
	 private Integer artistId;
	 private String userName;
	 private String action;
	 private Date modifiedDate;
	 private String eventName;
	 private String artistName;
	 private Date eventDate;
	 private Date eventTime;
	 private String venueName;
	 private String venueCategory;
	 private String eventType;
	 private String noPrice;
	 private Integer admitoneId;
	 private Integer venueId;
	 private Boolean presaleEvent;
	 
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	@Column(name="modified_date")
	public Date getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Date modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String EventName) {
		this.eventName = EventName;
	}
	@Column(name="event_date")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date EventDate) {
		this.eventDate = EventDate;
	}
	@Column(name="event_time")
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date EventTime) {
		this.eventTime = EventTime;
	}
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String VenueName) {
		this.venueName = VenueName;
	}
	@Column(name="venue_category")
	public String getVenueCategory() {
		return venueCategory;
	}
	public void setVenueCategory(String VenueCategory) {
		this.venueCategory =VenueCategory;
	}
	@Column(name="event_type")
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String EventType) {
		this.eventType = EventType;
	}
	@Column(name="no_price")
	public String getNoPrice() {
		return noPrice;
	}
	public void setNoPrice(String NoPrice) {
		this.noPrice =NoPrice;
	}
	@Column(name="admitone_id")
	public Integer getAdmitoneId() {
		return admitoneId;
	}
	public void setAdmitoneId(Integer AdmitoneId) {
		this.admitoneId = AdmitoneId;
	}
	@Column(name="artist_name")
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	@Column(name="venue_id")
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	@Column(name="artist_id")
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	
	@Column(name="presale_event")
	public Boolean getPresaleEvent() {
		return presaleEvent;
	}
	public void setPresaleEvent(Boolean presaleEvent) {
		this.presaleEvent = presaleEvent;
	}
	
}
