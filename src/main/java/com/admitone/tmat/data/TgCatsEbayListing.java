package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="tg_cats_ebay_listings")
public class TgCatsEbayListing implements Serializable{

	private Integer id;
	private String section;
	private String browseRow;
	private String ebayRow;
	private Integer quantity;
	private Double buyItNowPrice;
	private Double tmatOnlinePrice;
	private Double tmatpurchasePrice;
	private Double tnPrice;
	private Double vividPrice;
	private Double tickPickPrice;
	private Double scorebigPrice;
	
	private Date expectedArrivalDate;
	private Integer shippingMethodId;
	private Integer nearTermDisplayOptionId;
	private Integer eventId;
	//private Integer tnExchangeEventId;
	private Long tnCategoryTicketGroupId;
	private Integer categoryId;
	private Integer ticketId;
	private String status;
	private Date lastUpdated;
	private Date createdDate;
	private Date eventDate;
	//private Time eventTime;
	private String eventName;
	private String venueName;
	
	private Integer baseTicketOne;
	private Integer baseTicketTwo;
	private String priceHistory;
	private String popPrice;
	private String popDate;
	private String mainTicketHistory;
	private String baseTicketOneHistory;
	private String baseTicketTwoHistory;
	
	
	
	@Id
	@Column(name="Id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	@Column(name="quantity")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	@Column(name="expected_arrival_date", columnDefinition="DATE")
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	@Column(name="shipping_method_id")
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	
	@Column(name="near_term_display_option_id")
	public Integer getNearTermDisplayOptionId() {
		return nearTermDisplayOptionId;
	}
	public void setNearTermDisplayOptionId(Integer nearTermDisplayOptionId) {
		this.nearTermDisplayOptionId = nearTermDisplayOptionId;
	}
	
	@Column(name="event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
/*	@Column(name="tn_exchange_event_id")
	public Integer getTnExchangeEventId() {
		return tnExchangeEventId;
	}
	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		this.tnExchangeEventId = tnExchangeEventId;
	}*/
	
	@Column(name="tn_category_ticket_group_id")
	public Long getTnCategoryTicketGroupId() {
		return tnCategoryTicketGroupId;
	}
	public void setTnCategoryTicketGroupId(Long tnCategoryTicketGroupId) {
		this.tnCategoryTicketGroupId = tnCategoryTicketGroupId;
	}
	
	@Column(name="category_id")
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	@Column(name="ticket_id")
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	@Column(name="event_date",columnDefinition="DATE")
	public Date getEventDate() {
		return eventDate;
	}
	
	public void setEventDate(Date date) {
		this.eventDate = date;
	}

	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="last_updated")
	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	@Column(name="status")
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
	@Column(name="price_history")
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	
	
	@Column(name="tn_price")
	public Double getTnPrice() {
		return tnPrice;
	}
	public void setTnPrice(Double tnPrice) {
		this.tnPrice = tnPrice;
	}
	
	@Column(name="vividseat_price")
	public Double getVividPrice() {
		return vividPrice;
	}
	public void setVividPrice(Double vividPrice) {
		this.vividPrice = vividPrice;
	}
	
	@Column(name="tickpick_price")
	public Double getTickPickPrice() {
		return tickPickPrice;
	}
	public void setTickPickPrice(Double tickPickPrice) {
		this.tickPickPrice = tickPickPrice;
	}
	
	@Column(name="scorebig_price")
	public Double getScorebigPrice() {
		return scorebigPrice;
	}
	public void setScorebigPrice(Double scorebigPrice) {
		this.scorebigPrice = scorebigPrice;
	}
	
	
	@Transient
	public String getPopPrice() {
		return popPrice;
	}
	public void setPopPrice(String popPrice) {
		this.popPrice = popPrice;
	}
	@Transient
	public String getPopDate() {
		return popDate;
	}
	public void setPopDate(String popDate) {
		this.popDate = popDate;
	}
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	@Column(name="browse_row")
	public String getBrowseRow() {
		return browseRow;
	}
	public void setBrowseRow(String browseRow) {
		this.browseRow = browseRow;
	}
	@Column(name="ebay_row")
	public String getEbayRow() {
		return ebayRow;
	}
	public void setEbayRow(String ebayRow) {
		this.ebayRow = ebayRow;
	}
	
	@Column(name="buy_it_now_price")
	public Double getBuyItNowPrice() {
		return buyItNowPrice;
	}
	public void setBuyItNowPrice(Double buyItNowPrice) {
		this.buyItNowPrice = buyItNowPrice;
	}
	
	@Column(name="tmat_online_price")
	public Double getTmatOnlinePrice() {
		return tmatOnlinePrice;
	}
	public void setTmatOnlinePrice(Double tmatOnlinePrice) {
		this.tmatOnlinePrice = tmatOnlinePrice;
	}
	
	@Column(name="tmat_purchase_price")
	public Double getTmatpurchasePrice() {
		return tmatpurchasePrice;
	}
	public void setTmatpurchasePrice(Double tmatpurchasePrice) {
		this.tmatpurchasePrice = tmatpurchasePrice;
	}
	
	@Column(name="main_ticket_history")
	public String getMainTicketHistory() {
		return mainTicketHistory;
	}
	public void setMainTicketHistory(String mainTicketHistory) {
		this.mainTicketHistory = mainTicketHistory;
	}
	
	@Column(name="base_ticket_one_history")
	public String getBaseTicketOneHistory() {
		return baseTicketOneHistory;
	}
	public void setBaseTicketOneHistory(String baseTicketOneHistory) {
		this.baseTicketOneHistory = baseTicketOneHistory;
	}
	
	@Column(name="base_ticket_two_history")
	public String getBaseTicketTwoHistory() {
		return baseTicketTwoHistory;
	}
	public void setBaseTicketTwoHistory(String baseTicketTwoHistory) {
		this.baseTicketTwoHistory = baseTicketTwoHistory;
	}
	
	@Column(name="base_ticket_one")
	public Integer getBaseTicketOne() {
		return baseTicketOne;
	}
	public void setBaseTicketOne(Integer baseTicketOne) {
		this.baseTicketOne = baseTicketOne;
	}
	
	@Column(name="base_ticket_two")
	public Integer getBaseTicketTwo() {
		return baseTicketTwo;
	}
	public void setBaseTicketTwo(Integer baseTicketTwo) {
		this.baseTicketTwo = baseTicketTwo;
	}		
}
