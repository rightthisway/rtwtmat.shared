package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="exchange_event_auto_pricing_audit")
public class ExchangeEventAutoPricingAudit implements Serializable {
	 private Integer id;
	 private Event event;
	 private Double oldLowerMarkup;
	 private Double oldUpperMarkup;
	 private Double oldLowerShippingFees;
	 private Double oldUpperShippingFees;
	 private String oldExposure;
	 private Double oldRptFactor;
	 private Double oldPriceBreakup;
	 private Boolean oldIsFrequentEvent;
	 private String oldExchanges;
	 
	 private Double newLowerMarkup;
	 private Double newUpperMarkup;
	 private Double newLowerShippingFees;
	 private Double newUpperShippingFees;
	 private String newExposure;
	 private Double newRptFactor;
	 private Double newPriceBreakup;
	 private Boolean newIsFrequentEvent;
	 private String newExchanges;
	
	private String userName;
	private Date createdDate;
	private String action;
	
	@Transient
	 public void setOldAutoPricingValues(String exposure,Double rptFactor,Double priceBreakup,
				Double lMarkup,Double uMarkup,Double lShippingFees,Double uShippingFees, Boolean isFrequent) {
			
		this.oldExposure = exposure;
		this.oldRptFactor = rptFactor;
		this.oldPriceBreakup = priceBreakup;
		this.oldLowerMarkup = lMarkup;
		this.oldUpperMarkup = uMarkup;
		this.oldLowerShippingFees = lShippingFees;
		this.oldUpperShippingFees = uShippingFees;
		this.oldIsFrequentEvent = isFrequent;
	}
	
	@Transient
	 public void setNewAutoPricingValues(String exposure,Double rptFactor,Double priceBreakup,
				Double lMarkup,Double uMarkup,Double lShippingFees,Double uShippingFees, Boolean isFrequent) {
			
		this.newExposure = exposure;
		this.newRptFactor = rptFactor;
		this.newPriceBreakup = priceBreakup;
		this.newLowerMarkup = lMarkup;
		this.newUpperMarkup = uMarkup;
		this.newLowerShippingFees = lShippingFees;
		this.newUpperShippingFees = uShippingFees;
		this.newIsFrequentEvent = isFrequent;
	}
	
	@Id
	@Column(name="id")
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@ManyToOne
	@JoinColumn(name="event_id")
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	
	@Column(name="old_lower_markup")
	public Double getOldLowerMarkup() {
		return oldLowerMarkup;
	}
	public void setOldLowerMarkup(Double oldLowerMarkup) {
		this.oldLowerMarkup = oldLowerMarkup;
	}
	
	@Column(name="old_upper_markup")
	public Double getOldUpperMarkup() {
		return oldUpperMarkup;
	}
	public void setOldUpperMarkup(Double oldUpperMarkup) {
		this.oldUpperMarkup = oldUpperMarkup;
	}
	
	@Column(name="old_lower_shipping_fees")
	public Double getOldLowerShippingFees() {
		return oldLowerShippingFees;
	}
	public void setOldLowerShippingFees(Double oldLowerShippingFees) {
		this.oldLowerShippingFees = oldLowerShippingFees;
	}
	
	@Column(name="old_upper_shipping_fees")
	public Double getOldUpperShippingFees() {
		return oldUpperShippingFees;
	}
	public void setOldUpperShippingFees(Double oldUpperShippingFees) {
		this.oldUpperShippingFees = oldUpperShippingFees;
	}
	
	@Column(name="old_exposure")
	public String getOldExposure() {
		return oldExposure;
	}
	public void setOldExposure(String oldExposure) {
		this.oldExposure = oldExposure;
	}
	
	@Column(name="old_rpt_factor")
	public Double getOldRptFactor() {
		return oldRptFactor;
	}
	public void setOldRptFactor(Double oldRptFactor) {
		this.oldRptFactor = oldRptFactor;
	}
	
	@Column(name="old_is_frequent_event")
	public Boolean getOldIsFrequentEvent() {
		return oldIsFrequentEvent;
	}
	public void setOldIsFrequentEvent(Boolean oldIsFrequentEvent) {
		this.oldIsFrequentEvent = oldIsFrequentEvent;
	}
	
	@Column(name="old_exchanges")
	public String getOldExchanges() {
		return oldExchanges;
	}
	public void setOldExchanges(String oldExchanges) {
		this.oldExchanges = oldExchanges;
	}
	
	@Column(name="new_lower_markup")
	public Double getNewLowerMarkup() {
		return newLowerMarkup;
	}
	public void setNewLowerMarkup(Double newLowerMarkup) {
		this.newLowerMarkup = newLowerMarkup;
	}
	
	@Column(name="new_upper_markup")
	public Double getNewUpperMarkup() {
		return newUpperMarkup;
	}
	public void setNewUpperMarkup(Double newUpperMarkup) {
		this.newUpperMarkup = newUpperMarkup;
	}
	
	@Column(name="new_lower_shipping_fees")
	public Double getNewLowerShippingFees() {
		return newLowerShippingFees;
	}
	public void setNewLowerShippingFees(Double newLowerShippingFees) {
		this.newLowerShippingFees = newLowerShippingFees;
	}
	
	@Column(name="new_upper_shipping_fees")
	public Double getNewUpperShippingFees() {
		return newUpperShippingFees;
	}
	public void setNewUpperShippingFees(Double newUpperShippingFees) {
		this.newUpperShippingFees = newUpperShippingFees;
	}
	
	@Column(name="new_exposure")
	public String getNewExposure() {
		return newExposure;
	}
	public void setNewExposure(String newExposure) {
		this.newExposure = newExposure;
	}
	
	@Column(name="new_rpt_factor")
	public Double getNewRptFactor() {
		return newRptFactor;
	}
	public void setNewRptFactor(Double newRptFactor) {
		this.newRptFactor = newRptFactor;
	}
	
	@Column(name="new_is_frequent_event")
	public Boolean getNewIsFrequentEvent() {
		return newIsFrequentEvent;
	}
	public void setNewIsFrequentEvent(Boolean newIsFrequentEvent) {
		this.newIsFrequentEvent = newIsFrequentEvent;
	}
	
	@Column(name="new_exchanges")
	public String getNewExchanges() {
		return newExchanges;
	}
	public void setNewExchanges(String newExchanges) {
		this.newExchanges = newExchanges;
	}
	
	@Column(name="old_price_breakup")
	public Double getOldPriceBreakup() {
		return oldPriceBreakup;
	}
	public void setOldPriceBreakup(Double oldPriceBreakup) {
		this.oldPriceBreakup = oldPriceBreakup;
	}
	
	@Column(name="new_price_breakup")
	public Double getNewPriceBreakup() {
		return newPriceBreakup;
	}
	public void setNewPriceBreakup(Double newPriceBreakup) {
		this.newPriceBreakup = newPriceBreakup;
	}
	
	@Column(name="created_date")
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Column(name="user_name")
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	@Column(name="action")
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	


	
	/*@Transient
	public int compareTo(TNTGCatsCategoryEvent obj) {
		return this.getEvent().getId()- obj.getEvent().getId();
	}*/
	
	

	
	
	
}
