package com.admitone.tmat.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="sold_ticket_detail_vw")
public class SoldTicketDetail implements Serializable {
	
	SoldTicketDetailPkId soldTicketDetailPkId;
	Integer ticketGroupId;
	String brokerName;
	Date invoiceDateTime;
	Integer eventId;
	//Integer quantity;
	String eventName;
	Date eventDate;
	String venueName;
	String venueCity;
	String venueState;
	String venueCountry;
	String section;
	String row;
	Double retailPrice; 
	Double cost; 
	Double wholesalePrice; 
	Double actualSoldPrice; 
	Double totalPrice;
//	Double amount;
	String internalNotes;
	String clientBrokerCompanyName;
	Integer clientBrokerId; 
	String clientName;
	String clientTypeDesc;
	Integer purchaseOrderId;
	Date purchaseOrderDateTime;
	String invoiceType;
	String salesPerson;
	Integer soldQuantity;
	
	@EmbeddedId
	public SoldTicketDetailPkId getSoldTicketDetailPkId() {
		return soldTicketDetailPkId;
	}
	public void setSoldTicketDetailPkId(SoldTicketDetailPkId soldTicketDetailPkId) {
		this.soldTicketDetailPkId = soldTicketDetailPkId;
	}
	
	
	
	@Column(name="purchase_order_id")
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	
	@Column(name="purchase_order_date")
	public Date getPurchaseOrderDateTime() {
		return purchaseOrderDateTime;
	}
	public void setPurchaseOrderDateTime(Date purchaseOrderDateTime) {
		this.purchaseOrderDateTime = purchaseOrderDateTime;
	}
	
	@Column(name="ticket_group_id")
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	
	/*@Column(name="broker_id")
	public Integer getBrokerId() {
		return brokerId;
	}
	
	public void setBrokerId(Integer brokerId) {
		this.brokerId = brokerId;
	}
	*/
	
	@Column(name="exchange_event_id")
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	/*@Column(name="Invoice_no")
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}*/
	
	/*@Column(name="ticket_qty")
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}*/
	
	@Column(name="event_name")
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	@Column(name="event_datetime")
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	@Column(name="section")
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	@Column(name="row")
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	
	
	@Column(name="retail_price")
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	
	@Column(name="cost")
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	@Column(name="wholesale_price")
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	
	@Column(name="actual_sold_price")
	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	
	@Column(name="totalprice")
	public Double getTotalPrice() {
		return totalPrice;
	}
	
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	@Column(name="internal_notes")
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	
	@Column(name="invoice_datetime")
	public Date getInvoiceDateTime() {
		return invoiceDateTime;
	}
	public void setInvoiceDateTime(Date invoiceDateTime) {
		this.invoiceDateTime = invoiceDateTime;
	}

	@Transient
	public String getClientBrokerCompanyName() {
		return clientBrokerCompanyName;
	}
	public void setClientBrokerCompanyName(String clientBrokerCompanyName) {
		this.clientBrokerCompanyName = clientBrokerCompanyName;
	}
	
	@Transient
	public Integer getClientBrokerId() {
		return clientBrokerId;
	}
	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}
	
	@Transient
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	@Transient
	public String getClientTypeDesc() {
		return clientTypeDesc;
	}
	public void setClientTypeDesc(String clientTypeDesc) {
		this.clientTypeDesc = clientTypeDesc;
	}
	
	@Column(name="venue_name")
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	
	@Column(name="venue_city")
	public String getVenueCity() {
		return venueCity;
	}
	public void setVenueCity(String venueCity) {
		this.venueCity = venueCity;
	}
	
	@Column(name="state_name")
	public String getVenueState() {
		return venueState;
	}
	public void setVenueState(String venueState) {
		this.venueState = venueState;
	}
	
	@Column(name="system_country_name")
	public String getVenueCountry() {
		return venueCountry;
	}
	public void setVenueCountry(String venueCountry) {
		this.venueCountry = venueCountry;
	}
	
	@Column(name="client_broker_type")
	public String getBrokerName() {
		return brokerName;
	}
	
	public void setBrokerName(String brokerName) {
		this.brokerName = brokerName;
	}
	
	@Column(name="invoice_type")
	public String getInvoiceType() {
		return invoiceType;
	}
	public void setInvoiceType(String invoiceType) {
		this.invoiceType = invoiceType;
	}
	
	@Column(name="Sales_Person")
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	
	@Column(name="ticket_count")
	public Integer getSoldQuantity() {
		return soldQuantity;
	}
	public void setSoldQuantity(Integer soldQuantity) {
		this.soldQuantity = soldQuantity;
	}
	
	

}
