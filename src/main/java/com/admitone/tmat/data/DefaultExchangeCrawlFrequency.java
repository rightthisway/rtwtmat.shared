package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="default_exchange_crawl_frequency")
public class DefaultExchangeCrawlFrequency implements Serializable{
	private Integer id;
	private String exchange;
	private Integer defaultCrawlFrequency;
	private boolean automaticCrawlFrequency = true;
	
	
	@Id
	@Column(name="id")
    @GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name="exchange")
	public String getExchange() {
		return exchange;
	}
	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
	
	@Column(name="auto_crawl_frequency")
	public boolean isAutomaticCrawlFrequency() {
		return automaticCrawlFrequency;
	}
	public void setAutomaticCrawlFrequency(boolean automaticCrawlFrequency) {
		this.automaticCrawlFrequency = automaticCrawlFrequency;
	}
	
	@Column(name="default_crawl_frequency")
	public Integer getDefaultCrawlFrequency() {
		return defaultCrawlFrequency;
	}
	public void setDefaultCrawlFrequency(Integer defaultCrawlFrequency) {
		this.defaultCrawlFrequency = defaultCrawlFrequency;
	}
	
	

}
