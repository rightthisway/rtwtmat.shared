/**
 * 
 */
package com.admitone.tmat.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author msanghani
 *
 */

@SuppressWarnings("serial")
@Entity
@Table(name="concert_synonym")
public class ConcertSynonym implements Serializable {

	
	private String name;
	private String value;
	
	
	public ConcertSynonym() {
		
	}
	
	public ConcertSynonym(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
	
	@Column(name="name")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Id
	@Column(name="value")
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
