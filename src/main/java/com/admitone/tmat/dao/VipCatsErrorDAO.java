package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.VipCatsError;

public interface VipCatsErrorDAO extends RootDAO<Integer, VipCatsError>{
	public Collection<VipCatsError> getAllVipCatsErrorByDate(Date date);
}
