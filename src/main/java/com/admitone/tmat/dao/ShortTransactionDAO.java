package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.ShortTransaction;

public interface ShortTransactionDAO extends RootDAO<Integer, ShortTransaction> {
	Collection<ShortTransaction> getByCoverId(int transId);
	Collection<ShortTransaction> getAllShortsByEvent(int eventId);
	void deleteShortsFromUser(String username);
	Collection<ShortTransaction> deleteShortsByEvent(int eventId);
	void deleteById(Integer transId);
	void delete(ShortTransaction trans);
}
