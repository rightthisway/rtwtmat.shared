package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.ExchangeEventAutoPricingAudit;

public interface ExchangeEventAutoPricingAuditDAO extends RootDAO<Integer, ExchangeEventAutoPricingAudit> {

	public List<ExchangeEventAutoPricingAudit> getExchangeEventAutoPricingAuditByEventId(Integer eId);
	public void saveBulkAudits(List<ExchangeEventAutoPricingAudit> tnEvents) throws Exception;
}
