package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.enums.EventStatus;

public interface CategoryMappingDAO extends RootDAO<Integer, CategoryMapping> {
	List<CategoryMapping> getAllCategoryMappingsByVenueCategoryId(Integer venueCategoryId);
	List<CategoryMapping> getAllCategoryMappingsByCategoryId(Integer categoryId);
	List<String> getAllCategoryGroupNamesByTourIdByVenueCategoryId(Integer tourId,EventStatus eventStatus);
	
}