package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.UserAlertStatus;

public interface UserAlertDAO extends RootDAO<Integer, UserAlert> {
	Collection<UserAlert> getAlertsByUsername(String username);
	public Collection<UserAlert> getAlertsByMarketMakerAndTourId(String username, Integer tourId, AlertFor alertFor, UserAlertStatus userAlertStatus);
	public Collection<UserAlert> getAlertsByUsernameAndTourId(String username, Integer tourId, AlertFor alertFor, UserAlertStatus userAlertStatus);
//	public Collection<UserAlert> getAlertsByTourId(Integer tourId);
	Collection<UserAlert> getAlertsByUsernameAndEventId(String username, Integer eventId, AlertFor alertFor, UserAlertStatus userAlertStatus);
	Collection<UserAlert> getAlertsByMarketMakerAndEventId(String username, Integer eventId, AlertFor alertFor, UserAlertStatus userAlertStatus);
	
	Collection<UserAlert> getAlertsByUsernameAndEventIdAndShortDetails(String username, Integer eventId,String quantites,String category, UserAlertStatus userAlertStatus);
	
	Collection<UserAlert> getAllAlerts(UserAlertStatus alertStatus);

	Collection<UserAlert> getAllAlerts();
	void expireAlertsByEventId(Integer eventId);

	
	@Deprecated
	void deleteById(Integer id);
	@Deprecated
	void deleteAlertsFromUser(String username);
}