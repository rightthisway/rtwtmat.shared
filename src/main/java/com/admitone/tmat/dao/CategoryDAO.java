package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.Category;
import com.admitone.tmat.enums.EventStatus;

public interface CategoryDAO extends RootDAO<Integer, Category> {
	public void deleteByVenueCategoryId(Integer venueId);
	public Collection<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId);
	public Collection<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup);
	public Category getCategoryByVenueCategoryIdAndCategorySymbol(Integer venueCategoryId, String categorySymbol);
	public String getCategorieGroupByVenueCategoryId(Integer venueCategoryId);
	public Collection<Category> getAllCategoriesByVenuCategoryIdsByArtistId(Integer artistId,EventStatus eventSataus);
	public int countByVenueCategoryID(Integer id);
	
}