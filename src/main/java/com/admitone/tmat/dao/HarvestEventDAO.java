package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.HarvestEvent;

public interface HarvestEventDAO extends RootDAO<Integer, HarvestEvent> {

	void delete(HarvestEvent event);
	void deleteById(Integer id);
	void update(HarvestEvent event);
	Collection<HarvestEvent> getEventsInDateRange(Date fromDate, Date toDate);
}
