package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.VipAutoScoreBigExchangeEvent;

public interface VipAutoScoreBigExchangeEventDAO extends RootDAO<Integer, VipAutoScoreBigExchangeEvent> {
	
	public Collection<VipAutoScoreBigExchangeEvent> getAllVipAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVipAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds);
	public List<VipAutoScoreBigExchangeEvent> getAllActiveVipAutoScoreBigExchangeEvent();
	public Collection<VipAutoScoreBigExchangeEvent> getAllActiveVipAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VipAutoScoreBigExchangeEvent> getAllDeletedVipAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVipAutoScoreBigExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VipAutoScoreBigExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String vip_exposure_scorebig);
	public void updateRptFactor(String rpt_scorebig);
	public void updatePriceBreakup(String pricebreakup_scorebig);
	public void updateUpperMarkup(String upper_markpu_scorebig);
	public void updateLowerMarkup(String lower_markpu_scorebig);
	public void updateUpperShippingFees(String upper_shippingfees_scorebig);
	public void updateLowerShippingFees(String lower_shippingfees_scorebig);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}

