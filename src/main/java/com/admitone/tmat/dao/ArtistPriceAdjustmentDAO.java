package com.admitone.tmat.dao;

import java.util.Map;

import com.admitone.tmat.data.ArtistPriceAdjustment;
import com.admitone.tmat.data.ArtistPriceAdjustmentPk;

public interface ArtistPriceAdjustmentDAO extends RootDAO<ArtistPriceAdjustmentPk, ArtistPriceAdjustment> {
	Map<String, ArtistPriceAdjustment> getPriceAdjustments(int artistId);
	void deletePriceAdjustments(int artistId);
}