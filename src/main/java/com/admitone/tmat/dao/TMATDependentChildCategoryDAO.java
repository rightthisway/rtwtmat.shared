package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TMATDependentChildCategory;

public interface TMATDependentChildCategoryDAO extends RootDAO<Integer, TMATDependentChildCategory> {
	List<TMATDependentChildCategory> getTmatDependentsByChildCategoryId(Integer childCategoryId);

}
