package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.DefaultExchangeMarkupAudit;

public interface DefaultExchangeMarkupAuditDAO extends RootDAO<Integer,DefaultExchangeMarkupAudit> {

	public List<DefaultExchangeMarkupAudit> getDefaultExchangeMarkupAuditAll();
}
