package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.Proxy;

public interface ProxyDAO extends RootDAO<Integer, Proxy>{
	public List<Proxy> getAllProxy();
}
