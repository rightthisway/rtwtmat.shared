package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.VipMiniScoreBigExchangeEvent;

public interface VipMiniScoreBigExchangeEventDAO extends RootDAO<Integer, VipMiniScoreBigExchangeEvent> {
	
	public Collection<VipMiniScoreBigExchangeEvent> getAllVipMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVipMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds);
	public List<VipMiniScoreBigExchangeEvent> getAllActiveVipMiniScoreBigExchangeEvent();
	public Collection<VipMiniScoreBigExchangeEvent> getAllActiveVipMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VipMiniScoreBigExchangeEvent> getAllDeletedVipMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVipMiniScoreBigExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VipMiniScoreBigExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String vip_exposure_scorebig);
	public void updateRptFactor(String rpt_scorebig);
	public void updatePriceBreakup(String pricebreakup_scorebig);
	public void updateUpperMarkup(String upper_markpu_scorebig);
	public void updateLowerMarkup(String lower_markpu_scorebig);
	public void updateUpperShippingFees(String upper_shippingfees_scorebig);
	public void updateLowerShippingFees(String lower_shippingfees_scorebig);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
