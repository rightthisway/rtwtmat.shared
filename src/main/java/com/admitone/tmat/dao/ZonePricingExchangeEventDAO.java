package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.ZonePricingExchangeEvent;


public interface ZonePricingExchangeEventDAO extends RootDAO<Integer, ZonePricingExchangeEvent> {

	Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByEventId(Integer eventId);
	Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByVenueId(Integer venueId);
	Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByArtistId(Integer artistId);
	Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId);
	public Collection<ZonePricingExchangeEvent> getAllZonePricingExchangeEventsEligibleForUpdate(Long minute);
	public ZonePricingExchangeEvent getZonePricingExchangeEventsByEventId(Integer eventId);
}
