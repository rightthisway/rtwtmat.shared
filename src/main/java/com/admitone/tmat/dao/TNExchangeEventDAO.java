package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TNExchangeEvent;

public interface TNExchangeEventDAO extends RootDAO<Integer, TNExchangeEvent> {
	
	public Collection<TNExchangeEvent> getAllTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllTNExchangeEventsByEvent(List<Integer> eventIds);
	public List<TNExchangeEvent> getAllActiveTNExchangeEvent();
	public Collection<TNExchangeEvent> getAllActiveTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<TNExchangeEvent> getAllDeletedTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllTNExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<TNExchangeEvent> tnEvents) throws Exception;
	
}
