package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.ExchangeFiveExchangeEvent;

public interface ExchangeFiveExchangeEventDAO extends RootDAO<Integer, ExchangeFiveExchangeEvent> {
	
	public Collection<ExchangeFiveExchangeEvent> getAllExchangeFiveExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllExchangeFiveExchangeEventsByEvent(List<Integer> eventIds);
	public List<ExchangeFiveExchangeEvent> getAllActiveExchangeFiveExchangeEvent();
	public Collection<ExchangeFiveExchangeEvent> getAllActiveExchangeFiveExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<ExchangeFiveExchangeEvent> getAllDeletedExchangeFiveExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllExchangeFiveExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<ExchangeFiveExchangeEvent> tnEvents) throws Exception;
	
}
