package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.AdmitoneInventoryLocal;

public interface AdmitoneInventoryLocalDAO extends RootDAO<Integer, AdmitoneInventoryLocal> {
	// FIXME: not really sure what this method's purpose is. Remove it?
	@Deprecated
	Collection<AdmitoneInventoryLocal> getAllInventoryLocalByDate(Date eventDate);
	
	Collection<AdmitoneInventoryLocal> getAllInventoryLocalByAdmitOneEventId(Integer eventId);
	Collection<AdmitoneInventoryLocal> getAllInventoryLocalByEventId(Integer eventId);
}
