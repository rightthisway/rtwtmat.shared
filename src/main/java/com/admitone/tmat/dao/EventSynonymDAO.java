package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.EventSynonym;

public interface EventSynonymDAO extends RootDAO<Integer, EventSynonym> {
	EventSynonym getEventSynonymByMerged(Integer mergedId);
	Collection<EventSynonym> getEventSynonymByExisting(Integer eventId);
	void deleteSynonyms(Integer eventId);
}
