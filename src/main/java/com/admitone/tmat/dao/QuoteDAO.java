package com.admitone.tmat.dao;

import java.sql.Date;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Quote;

public interface QuoteDAO extends RootDAO<Integer, Quote> {
	Collection<String> getQuoteUsernames();
	Quote getMostRecentQuote(String username);
	Quote getOpenQuote(Integer customerId, String username, java.sql.Date date);
	Collection<Quote> getQuotesByUserAndCustomer(String username, Integer customerId);
	Collection<Quote> getQuotesByCustomerAndDate(Integer customerId, java.sql.Date date);
	Collection<Quote> getQuotesByUserCustomerAndDate(String username, Integer customerId, java.sql.Date date);
	Collection<java.sql.Date> getQuotesDatesByCustomer(Integer customerId);
	Collection<Quote> getQuotesByCustomer(Integer customerId);
	Collection<Quote> getQuotesByCustomer(Integer customerId,boolean isZoneQuote);
	void deleteByCustomer(Integer customerId);
	List<Quote> getFilteredQuotes(String creator, String custId,
			java.sql.Date fromQuoteDate, java.sql.Date toQuoteDate,
			String email, String status);//,boolean isZoneQuote
	List<Quote> getQuotes();
	List<Quote> getQuotes(boolean isZoneQuote);
	public Collection<String> getQuoteUsernamesByBrokerId(Integer brokerId);
	public List<Quote> getQuotesByBrokerId(Integer brokerId,boolean isZoneQuote);
	public List<Quote> getAllQuotesByBrokerId(Integer brokerId);
	public List<Quote> getFilteredQuotes(String creator, String custId,
			Date fromQuoteDate, Date toQuoteDate, String email, String status,Integer brokerId);
	
}
