package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TnExchangeEventZoneFloorCapAudit;

public interface TnExchangeEventZoneFloorCapAuditDAO extends RootDAO<Integer, TnExchangeEventZoneFloorCapAudit> {
	
	public List<TnExchangeEventZoneFloorCapAudit> getZoneFloorCapAuditByEvntIdAndZoneId(Integer eId, Integer zoneId,Integer brokerId);
}
