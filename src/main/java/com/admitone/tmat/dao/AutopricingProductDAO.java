package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.AutopricingProduct;



public interface AutopricingProductDAO extends RootDAO<Integer, AutopricingProduct>{
	Collection<AutopricingProduct> getAllAutopricingProductExceptGivenId(Integer id);
	AutopricingProduct getAutopricingProductByName(String name);
	AutopricingProduct getAutopricingProductByInternalNotes(String internalNotes);
	public List<String> getAutopricingProductInternalNotes();
}
