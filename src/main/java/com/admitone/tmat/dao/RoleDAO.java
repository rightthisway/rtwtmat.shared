package com.admitone.tmat.dao;

import com.admitone.tmat.data.Role;

public interface RoleDAO extends RootDAO<Integer, Role> {
	public Role getRoleByName(String name);
}
