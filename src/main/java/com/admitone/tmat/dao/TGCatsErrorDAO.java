package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.TGCatsError;

public interface TGCatsErrorDAO extends RootDAO<Integer, TGCatsError>{
	public Collection<TGCatsError> getAllTGCatsErrorByDate(Date date);
}
