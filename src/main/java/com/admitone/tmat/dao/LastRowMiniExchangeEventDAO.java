package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.LastRowMiniExchangeEvent;


public interface LastRowMiniExchangeEventDAO extends RootDAO<Integer, LastRowMiniExchangeEvent> {

	Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByEventId(Integer eventId);
	Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByVenueId(Integer venueId);
	Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByArtistId(Integer artistId);
	Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId);
	public Collection<LastRowMiniExchangeEvent> getAllLastRowMiniExchangeEventsEligibleForUpdate(Long minute);
	public LastRowMiniExchangeEvent getLastRowMiniExchangeEventsByEventId(Integer eventId);
}
