package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.AdmitoneTicketMark;

public interface AdmitoneTicketMarkDAO extends RootDAO<Integer, AdmitoneTicketMark> {
	Collection<AdmitoneTicketMark> getAdmitOneTicketMarksByEvent(Integer eventId);
}
