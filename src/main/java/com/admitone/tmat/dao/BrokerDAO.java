package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Broker;

public interface BrokerDAO extends RootDAO<Integer, Broker>{

	public List<Broker> getAllActiveBrokers();
	public List<Broker> getAllActiveAutopricingBrokers();
//	List<Object[]> getAllBroker();
	Collection<Broker> getAll() ;
	Collection<Broker> getBrokerById(int brokerId);
	Broker getBrokerByEmailSuffix(String emailSuffix);
	public List<Broker> getAllOpenOrderEligibleBrokers();
	
}
