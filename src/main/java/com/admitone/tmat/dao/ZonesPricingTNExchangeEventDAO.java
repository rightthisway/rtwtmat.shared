package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.ZonesPricingTNExchangeEvent;

public interface ZonesPricingTNExchangeEventDAO extends RootDAO<Integer, ZonesPricingTNExchangeEvent> {
	
	public Collection<ZonesPricingTNExchangeEvent> getAllZonesPricingTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllZonesPricingTNExchangeEventsByEvent(List<Integer> eventIds);
	//public List<ZonesPricingTNExchangeEvent> getAllActiveZonesPricingTNExchangeEvent();
	public Collection<ZonesPricingTNExchangeEvent> getAllActiveZonesPricingTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<ZonesPricingTNExchangeEvent> getAllDeletedZonesPricingTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllZonesPricingTNExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<ZonesPricingTNExchangeEvent> tnEvents) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
	
}
