package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.VividSeatsTicketInventory;

public interface VividSeatsTicketInventoryDAO extends RootDAO<Integer, VividSeatsTicketInventory> {
	
	public List<VividSeatsTicketInventory> getAllTicketInventory(String eventName);
	 
	
}
