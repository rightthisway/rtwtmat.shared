package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.AlertTicketGroup;

public interface AlertTicketGroupDAO extends RootDAO<Integer, AlertTicketGroup>{
	List<AlertTicketGroup> getAlertTicketGroupsByAlertId(Integer alertId);
}
