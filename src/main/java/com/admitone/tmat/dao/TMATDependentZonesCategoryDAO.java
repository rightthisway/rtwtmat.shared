package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TMATDependentZonesCategory;

public interface TMATDependentZonesCategoryDAO extends RootDAO<Integer, TMATDependentZonesCategory> {
	List<TMATDependentZonesCategory> getTmatDependentsByTourId(Integer tourId);

}
