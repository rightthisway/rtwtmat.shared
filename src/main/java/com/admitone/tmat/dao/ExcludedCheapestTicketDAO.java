package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ExcludedCheapestTicket;

public interface ExcludedCheapestTicketDAO extends RootDAO<Integer , ExcludedCheapestTicket> {

	Collection<ExcludedCheapestTicket> getAllExcludedTickets(String categorySelected, String sectionSelected, String rowSelected, String seatSelected,Collection<String> siteList);

	Collection<ExcludedCheapestTicket> getByTour(Integer tourId);

	Collection<ExcludedCheapestTicket> getAllExcludedTicketsByTourID(
			Integer tourId,String categorySelected, String sectionSelected, String rowSelected, String seatSelected, Collection<String> siteList);

	Collection<ExcludedCheapestTicket> getAllExcludedTicketsByEventID(
			Integer eventId,String categorySelected, String sectionSelected, String rowSelected, String seatSelected, Collection<String> siteList);

	Collection<Event> getDistinctEvents(Integer tourId);

	Collection<ExcludedCheapestTicket> getAllTickets();

	List<Event> getDistinctEvent();

	List<String> getDistinctSiteIds();

	List<String> getDistinctSections(String artistIdStr,String eventIdstr, String categorySelected);

	List<String> getDistinctRows(String artistIdStr,String eventIdstr,String categorySelected, String sectionSelected);

	List<String> getDistinctSeats(String artistIdStr,String eventIdstr,String categorySelected,String sectionSelected, String rowSelected);

	List<String> getDistinctPrices(String artistIdStr,String eventIdstr,String sectionSelected, String rowSelected, String seatSelected);

	List<String> getCategorys(String tourIdStr, String eventIdStr);
	}
