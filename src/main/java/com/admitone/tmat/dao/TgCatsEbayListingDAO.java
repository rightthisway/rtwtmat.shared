package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TgCatsEbayListing;

public interface TgCatsEbayListingDAO extends RootDAO<Integer, TgCatsEbayListing> {

	public Collection<TgCatsEbayListing> getAllActiveTgCatsEbayListings();
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByEventId(Integer eventId);
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsById(Integer Id);
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByTicketId(Integer Id);
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByBaseTicketOneId(Integer Id);
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByBaseTicketTwoId(Integer Id);
}
