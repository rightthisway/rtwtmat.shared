package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TixcityUnfilledOrderSummary;

public interface TixcityUnfilledOrderSummaryDAO extends RootDAO<Integer, TixcityUnfilledOrderSummary> {
	
	public TixcityUnfilledOrderSummary getLastWeekUnfilledOrderSummary();
	public List<TixcityUnfilledOrderSummary> getAllUnfilldedOrderSummary();
	
}
