package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.DefaultExchangeMarkupAudit;

public class DefaultExchangeMarkupAuditDAO extends HibernateDAO<Integer,DefaultExchangeMarkupAudit> implements com.admitone.tmat.dao.DefaultExchangeMarkupAuditDAO{

	public List<DefaultExchangeMarkupAudit> getDefaultExchangeMarkupAuditAll() {
		
		Session session = getSession();
		List<DefaultExchangeMarkupAudit> list = new ArrayList<DefaultExchangeMarkupAudit>();
		try{
			Query query = session.createQuery("FROM DefaultExchangeMarkupAudit");
			//query.setParameter("exCategory", exCategory);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
}