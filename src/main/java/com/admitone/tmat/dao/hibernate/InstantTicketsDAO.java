package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.InstantTickets;

public class InstantTicketsDAO extends HibernateDAO<Integer, InstantTickets> implements com.admitone.tmat.dao.InstantTicketsDAO {
	
	@SuppressWarnings("unchecked")
	public List<InstantTickets> getAllInstantTickets() {
	//	return find("FROM InstantTickets WHERE eventId in (100898508, 100898509,  100898510, 100898434,100898426,100898428,100898504,100898505, 100898506, 100898433,100898742, 100898743, 100898580,100898476,100898567,100898727,100898576, 100898737, 100898577,100899460, 100899654, 100899655,100899656, 100899461, 100899462,100899470, 100899675, 100899676, 100896425, 100896572, 100896426, 100896573, 100896430, 100896576, 100896577, 100896444, 100896446, 100896447, 100895921,100895923,100896012, 100896032, 100896033, 100896034, 100896035, 100895996, 100895997, 100896007) AND ticketDeliveryType = ? AND ticketStatus = ? ", new Object[] {ticketDeliveryType, ticketStatus});
		return find("FROM InstantTickets");
	}
	
	public List<InstantTickets> getAllInstantTickets(Integer eventId) {
	return find("FROM InstantTickets WHERE eventId=?", new Object[]{eventId});
	}
}
