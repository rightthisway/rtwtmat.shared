package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.LarryMiniTNExchangeEvent;

public class LarryMiniTNExchangeEventDAO extends HibernateDAO<Integer,LarryMiniTNExchangeEvent> implements com.admitone.tmat.dao.LarryMiniTNExchangeEventDAO{
	public Collection<LarryMiniTNExchangeEvent> getAllLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<LarryMiniTNExchangeEvent> result = new ArrayList<LarryMiniTNExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<LarryMiniTNExchangeEvent> temp = find("FROM LarryMiniTNExchangeEvent WHERE event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public Collection<LarryMiniTNExchangeEvent> getAllActiveLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<LarryMiniTNExchangeEvent> result = new ArrayList<LarryMiniTNExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<LarryMiniTNExchangeEvent> temp = find("FROM LarryMiniTNExchangeEvent WHERE status='ACTIVE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection<LarryMiniTNExchangeEvent> getAllDeletedLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<LarryMiniTNExchangeEvent> result = new ArrayList<LarryMiniTNExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<LarryMiniTNExchangeEvent> temp = find("FROM LarryMiniTNExchangeEvent WHERE status='DELETE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAllLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("DELETE FROM LarryMiniTNExchangeEvent WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public void updateAllLarryMiniTNExchangeEventAsDeleteByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("UPDATE LarryMiniTNExchangeEvent SET status='DELETE' WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public List<LarryMiniTNExchangeEvent> getAllActiveLarryMiniTNExchangeEvent() {
		try{
			return find("FROM LarryMiniTNExchangeEvent WHERE event.id IN(SELECT DISTINCT eventId from TGCatsCategoryTicket where status ='ACTIVE')", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveBulkEvents(List<LarryMiniTNExchangeEvent> tnEvents) throws Exception {
	
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (LarryMiniTNExchangeEvent event : tnEvents) {
			    session.save(event);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
	
	
public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) {
	Session session=null;
	try {
		String query = "";
		
		if(globalAudit.getMiniExposure() != null) {
			query = query + ",ee.exposure='"+globalAudit.getMiniExposure()+"'";
		}
		if(globalAudit.getRptFactor() != null) {
			query = query + ",ee.rpt_factor="+globalAudit.getRptFactor();
		}
		if(globalAudit.getShippingMethod() != null) {
			query = query + ",ee.shipping_method='"+globalAudit.getShippingMethod()+"'";
		}
		if(globalAudit.getNearTermDisplayOption() != null) {
			query = query + ",ee.near_term_display_option='"+globalAudit.getNearTermDisplayOption()+"'";
		}
		if(globalAudit.getPriceBreakup() != null) {
			query = query + ",ee.price_breakup="+globalAudit.getPriceBreakup();
		}
		if(globalAudit.getLowerMarkup() != null) {
			query = query + ",ee.lower_markup="+globalAudit.getLowerMarkup();
		}
		if(globalAudit.getUpperMarkup() != null) {
			query = query + ",ee.upper_markup="+globalAudit.getUpperMarkup();
		}
		if(globalAudit.getLowerShippingFees() != null) {
			query = query + ",ee.lower_shipping_fees="+globalAudit.getLowerShippingFees();
		}
		if(globalAudit.getUpperShippingFees() != null) {
			query = query + ",ee.upper_shipping_fees="+globalAudit.getUpperShippingFees();
		}
		query = "update ee set " + query.substring(1);
		
		query = query + " from larry_lastrow_mini_exchange_event ee " +
		" inner join event e on e.id=ee.event_id " +
		" inner join tour t on t.id=e.tour_id" +
		" inner join grand_child_tour_category gc on gc.id=t.grand_child_category_id" +
		" inner join child_tour_category cc on cc.id=gc.child_category_id" +
		" inner join tour_category tc on tc.id=cc.tour_category_id " +
		" where ee.status='ACTIVE' and tc.name in("+parentType+")";
		
		session = getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery(query);
		sqlQuery.executeUpdate();
		
	} catch (Exception e) {
		e.printStackTrace();
	} finally{
		session.close();
	}
	}
}
