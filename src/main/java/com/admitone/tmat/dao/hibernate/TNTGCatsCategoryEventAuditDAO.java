package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.TNTGCatsCategoryEventAudit;

public class TNTGCatsCategoryEventAuditDAO extends HibernateDAO<Integer,TNTGCatsCategoryEventAudit> implements com.admitone.tmat.dao.TNTGCatsCategoryEventAuditDAO{


	public List<TNTGCatsCategoryEventAudit> getTGCatsCategoryEventAuditByEventId(Integer eId) {
			
			Session session = getSession();
			List<TNTGCatsCategoryEventAudit> list = new ArrayList<TNTGCatsCategoryEventAudit>();
			try{
				Query query = session.createQuery("FROM TNTGCatsCategoryEventAudit where eventId = :eIds order by createdDate");
				query.setParameter("eIds", eId);
				list= query.list();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				session.close();
			}				
			return list;
		}
}
