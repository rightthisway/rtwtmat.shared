package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.ZonedLastRowMiniCategoryTicket;

public class ZonedLastRowMiniCategoryTicketDAO extends HibernateDAO<Integer, ZonedLastRowMiniCategoryTicket> implements com.admitone.tmat.dao.ZonedLastRowMiniCategoryTicketDAO {

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<ZonedLastRowMiniCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<ZonedLastRowMiniCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<ZonedLastRowMiniCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<ZonedLastRowMiniCategoryTicket> getAllActiveZonedLastRowMiniCategoryTickets() {
		return find("FROM ZonedLastRowMiniCategoryTicket where status ='ACTIVE'");
	}
	
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsByAll(Integer eventId, String section, String row, String quantity) {
		String query = "FROM ZonedLastRowMiniCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND lastRow=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(quantity);								
		
			}
		
		return find(query, param.toArray());
	}
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsById(Integer Id) {
		return find("FROM ZonedLastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM ZonedLastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	
}
