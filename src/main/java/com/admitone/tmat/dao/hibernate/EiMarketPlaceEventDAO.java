package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.EiMarketPlaceEvent;

public class EiMarketPlaceEventDAO extends HibernateDAO<Integer, EiMarketPlaceEvent> implements com.admitone.tmat.dao.EiMarketPlaceEventDAO {
	public Collection<EiMarketPlaceEvent> getEiMarketPlaceEvents(Date fromDate, Date toDate) {
		return find("FROM EiMarketPlaceEvent WHERE eventDate >= ? AND eventDate <= ?", new Object[]{fromDate, toDate});
	}
	
	public void deleteEventsNotUpdatedAfter(Date updatedDate) {
		bulkUpdate("DELETE FROM EiMarketPlaceEvent WHERE updatedDate < ?", new Object[]{updatedDate});
	}

	public Collection<Integer> getEiMarketPlaceEventIds() {
		return find("SELECT id FROM EiMarketPlaceEvent");
	}

	public void updateDate(Integer eventId, Date date) {
		bulkUpdate("UPDATE EiMarketPlaceEvent SET updatedDate = ? WHERE id = ?", new Object[]{date, eventId});
	}

}
