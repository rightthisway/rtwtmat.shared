package com.admitone.tmat.dao.hibernate;

import com.admitone.tmat.data.VenueSynonym;

public class VenueSynonymDAO extends HibernateDAO<String, VenueSynonym> implements com.admitone.tmat.dao.VenueSynonymDAO {

	public VenueSynonym getSynonymByVenueId(Integer venueId){
		return findSingle("FROM VenueSynonym WHERE venueId=?", new Object[]{venueId});
	}
	
}
