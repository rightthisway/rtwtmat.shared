package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.VipAutoVividSeatExchangeEvent;

public class VipAutoVividSeatExchangeEventDAO extends HibernateDAO<Integer,VipAutoVividSeatExchangeEvent> implements com.admitone.tmat.dao.VipAutoVividSeatExchangeEventDAO{

	public Collection<VipAutoVividSeatExchangeEvent> getAllVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<VipAutoVividSeatExchangeEvent> result = new ArrayList<VipAutoVividSeatExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<VipAutoVividSeatExchangeEvent> temp = find("FROM VipAutoVividSeatExchangeEvent WHERE event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public Collection<VipAutoVividSeatExchangeEvent> getAllActiveVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<VipAutoVividSeatExchangeEvent> result = new ArrayList<VipAutoVividSeatExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<VipAutoVividSeatExchangeEvent> temp = find("FROM VipAutoVividSeatExchangeEvent WHERE status='ACTIVE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection<VipAutoVividSeatExchangeEvent> getAllDeletedVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<VipAutoVividSeatExchangeEvent> result = new ArrayList<VipAutoVividSeatExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<VipAutoVividSeatExchangeEvent> temp = find("FROM VipAutoVividSeatExchangeEvent WHERE status='DELETE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAllVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("DELETE FROM VipAutoVividSeatExchangeEvent WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public void updateAllVipAutoVividSeatExchangeEventAsDeleteByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("UPDATE VipAutoVividSeatExchangeEvent SET status='DELETE' WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public List<VipAutoVividSeatExchangeEvent> getAllActiveVipAutoVividSeatExchangeEvent() {
		try{
			return find("FROM VipAutoVividSeatExchangeEvent WHERE event.id IN(SELECT DISTINCT eventId from TGCatsCategoryTicket where status ='ACTIVE')", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveBulkEvents(List<VipAutoVividSeatExchangeEvent> tnEvents) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (VipAutoVividSeatExchangeEvent event : tnEvents) {
			    session.save(event);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
	public void updateExposure(String vip_exposure_vivid)
	{
		String query="update VipAutoVividSeatExchangeEvent set exposure="+"'"+vip_exposure_vivid+"'";
		bulkUpdate(query);
	}
	public void updateRptFactor(String rpt_vivid)
	{
		String query="update VipAutoVividSeatExchangeEvent set rptFactor="+"'"+rpt_vivid+"'";
	    bulkUpdate(query);
	}
	public void updatePriceBreakup(String pricebreakup_vivid)
	{
		String query="update VipAutoVividSeatExchangeEvent set priceBreakup="+"'"+pricebreakup_vivid+"'";
	    bulkUpdate(query);
	}
	public void updateUpperMarkup(String upper_markpu_vivid)
	{
		String query="update VipAutoVividSeatExchangeEvent set upperMarkup="+"'"+upper_markpu_vivid+"'";
	    bulkUpdate(query);
	}
	public void updateLowerMarkup(String lower_markpu_vivid)
	{
		String query="update VipAutoVividSeatExchangeEvent set lowerMarkup="+"'"+lower_markpu_vivid+"'";
	    bulkUpdate(query);
	}
	public void updateUpperShippingFees(String upper_shippingfees_vivid)
	{
		String query="update VipAutoVividSeatExchangeEvent set upperShippingFees="+"'"+upper_shippingfees_vivid+"'";
	    bulkUpdate(query);
	}
	public void updateLowerShippingFees(String lower_shippingfees_vivid)
	{
		String query="update VipAutoVividSeatExchangeEvent set lowerShippingFees="+"'"+lower_shippingfees_vivid+"'";
	    bulkUpdate(query);
	}
	
public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) {
		
		Session session=null;
		try {
			String query = "";
			
			if(globalAudit.getVipAutoExposure() != null) {
				query = query + ",ee.exposure='"+globalAudit.getVipAutoExposure()+"'";
			}
			if(globalAudit.getShippingMethod() != null) {
				query = query + ",ee.shipping_method='"+globalAudit.getShippingMethod()+"'";
			}
			if(globalAudit.getNearTermDisplayOption() != null) {
				query = query + ",ee.near_term_display_option='"+globalAudit.getNearTermDisplayOption()+"'";
			}
			if(globalAudit.getRptFactor() != null) {
				query = query + ",ee.rpt_factor="+globalAudit.getRptFactor();
			}
			if(globalAudit.getPriceBreakup() != null) {
				query = query + ",ee.price_breakup="+globalAudit.getPriceBreakup();
			}
			if(globalAudit.getLowerMarkup() != null) {
				query = query + ",ee.lower_markup="+globalAudit.getLowerMarkup();
			}
			if(globalAudit.getUpperMarkup() != null) {
				query = query + ",ee.upper_markup="+globalAudit.getUpperMarkup();
			}
			if(globalAudit.getLowerShippingFees() != null) {
				query = query + ",ee.lower_shipping_fees="+globalAudit.getLowerShippingFees();
			}
			if(globalAudit.getUpperShippingFees() != null) {
				query = query + ",ee.upper_shipping_fees="+globalAudit.getUpperShippingFees();
			}
			query = "update ee set " + query.substring(1);
			
			query = query + " from vip_auto_vividseat_exchange_event ee " +
			" inner join event e on e.id=ee.event_id " +
			" inner join tour t on t.id=e.tour_id" +
			" inner join grand_child_tour_category gc on gc.id=t.grand_child_category_id" +
			" inner join child_tour_category cc on cc.id=gc.child_category_id" +
			" inner join tour_category tc on tc.id=cc.tour_category_id " +
			" where ee.status='ACTIVE' and tc.name in("+parentType+")";
			
			session = getSessionFactory().openSession();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			sqlQuery.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally{
			session.close();
		}
		
	}

}
