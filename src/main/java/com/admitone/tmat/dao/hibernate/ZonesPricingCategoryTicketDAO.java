package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.ZonesPricingCategoryTicket;

public class ZonesPricingCategoryTicketDAO extends HibernateDAO<Integer, ZonesPricingCategoryTicket> implements com.admitone.tmat.dao.ZonesPricingCategoryTicketDAO {

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<ZonesPricingCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<ZonesPricingCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<ZonesPricingCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<ZonesPricingCategoryTicket> getAllActiveZonesPricingCategoryTickets() {
		return find("FROM ZonesPricingCategoryTicket where status ='ACTIVE'");
	}
	
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketByAll(Integer eventId,String section,String row,String quantity) {
		String query = "FROM ZonesPricingCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty()) {
			query += " AND quantity=?";
			param.add(quantity);								
		
		} 
		
		
		return find(query, param.toArray());
	}
	
	
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketById(Integer Id) {
		return find("FROM ZonesPricingCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM ZonesPricingCategoryTicket where id=?" ,new Object[]{Id});
	}
	
}
