package com.admitone.tmat.dao.hibernate;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.SQLException;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.acegisecurity.context.SecurityContextHolder;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.EnumType;
import org.slf4j.Logger;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.BaseEvent.EventTicketStat;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventAudit;
import com.admitone.tmat.data.ManageTcapAllTourCsvGenerate;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentEvent;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.utils.TextUtil;

public class EventDAO extends HibernateDAO<Integer, Event> implements com.admitone.tmat.dao.EventDAO {	
	private Date lastEventUpdate = new Date(new Date().getTime() - 3600000L);
	public final static int OFFSET_AUTO_GEN_EVENT_ID = 10000000;
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(EventDAO.class);
	public Collection<Event> getAllEventsByNameAndDate(String name, Date dateAndTime) {
		DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
		DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		
		Date date=null;
		try {
			date = dateFormat.parse(df.format(dateAndTime));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Date time=null;
		String timeStr="";
		try {
			timeStr=timeFormat.format(dateAndTime);
			int hour = Integer.parseInt(timeStr.split(":")[0]);
			int min = Integer.parseInt(timeStr.split(":")[1]);
			Calendar cal = Calendar.getInstance();
			cal.set(Calendar.YEAR, 1970);
			cal.set(Calendar.MONTH, 0);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			cal.set(Calendar.HOUR_OF_DAY, hour);
			cal.set(Calendar.MINUTE, min);
			cal.set(Calendar.SECOND, 0);
			time = cal.getTime();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(date == null){
			return find("FROM Event WHERE name=? AND event_date is null AND eventStatus = ? ", new Object[]{name,EventStatus.ACTIVE});
		}else {
//			return find("FROM Event WHERE name=? AND event_date =? AND event_time=?", new Object[]{name, date,time});
			Session session = getSession();
//			session.beginTransaction();
			
			SQLQuery query = session.createSQLQuery("SELECT id,name,venue_id FROM event WHERE name=:name AND event_date=:date AND event_time=:time AND event_status = 'ACTIVE'");
			query.setString("name", name);
//			query.setDate("date", date);
//			query.setDate("time", time);
			query.setString("date", df.format(date));
			query.setString("time", "1970-01-01 " + timeStr);
			List<Object[]> list = query.list();
			List<Event> events= new ArrayList<Event>();
			Event event =null;
			for(Object[] obj:list){
				event = new Event();
				event.setId((Integer)obj[0]);
				event.setVenueId((Integer)obj[2]);
				event.setLocalDate(date);
				event.setLocalTime(new Time(time.getTime()));
				events.add(event);
			}
			releaseSession(session);
			return events;
		}
		
	}
	
	public Collection<Event> getAllActiveEvents() {
		return find("FROM Event WHERE eventStatus=? ORDER BY name, event_date, event_time Asc", new Object[]{EventStatus.ACTIVE});
	}
	
		public List<ManageTcapAllTourCsvGenerate> getAllActiveEventCsvData(){
		
		Session session = getSession();
		StringBuilder sb = new StringBuilder();
		sb.append("select e.id as id,v.id as venueId,isnull(replace(e.name,',',' '),'') as eventName,isnull((case when e.event_date is null then 'TBD' else ");
		sb.append("CONVERT(VARCHAR(10), e.event_date, 101) end+ ' ' + case when e.event_time is null then 'TBD' else ");
		sb.append("CONVERT(VARCHAR(10), e.event_time, 108) end ),'') as eventDateTime,isnull(replace(v.building,',',' '),'') as venueName,");
		sb.append("isnull(replace(t.name,',',' '),'') as tourName,isnull(tc.name,'') as tourCategoryName,isnull(c.name,'') as childCategoryName,isnull(g.name,'') as grandChildCategory,");
		sb.append("isnull((select COUNT(id) from ticket_listing_crawl where event_id=e.id and event_id is not null and enabled='true'),'') as crawlCount,");
		sb.append("isnull(replace((select distinct(c.group_name) + ',' from category c inner join category_mapping cm on c.id=cm.category_id where ");
		sb.append("cm.event_id=e.id for xml path('')),',',' '),'') as categoryGroupName,isnull(case e.auto_correct when 1 then 'yes' else 'no' end,'') as autoCorrect,");
		sb.append("isnull(case e.ignore_tbd_time when 1 then 'yes' else 'no' end,'') as ignoreTbdTime,");
		sb.append("isnull(case when e.admitone_id is not null then 'true' else 'flase' end,'') as link,e.venueCategoryId as venueCategoryId ");
		sb.append("from event e inner join tour t on e.tour_id=t.id inner join grand_child_tour_category g on t.grand_child_category_id=");
		sb.append("g.id inner join child_tour_category c on c.id=g.child_category_id inner join tour_category tc on tc.id=c.tour_category_id ");
		sb.append("inner join venue v on v.id=e.venue_id where e.event_status='active' order by e.name,e.event_date,e.event_time asc");
		
		try{			
			Query query = session.createSQLQuery(sb.toString()).addScalar("id", Hibernate.INTEGER).addScalar("venueId", Hibernate.INTEGER).
			addScalar("crawlCount",Hibernate.INTEGER).addScalar("eventName", Hibernate.STRING).addScalar("eventDateTime", Hibernate.STRING)
			.addScalar("venueName", Hibernate.STRING).addScalar("tourName", Hibernate.STRING).addScalar("tourCategoryName", Hibernate.STRING)
			.addScalar("childCategoryName", Hibernate.STRING).addScalar("grandChildCategory", Hibernate.STRING).addScalar("categoryGroupName", Hibernate.STRING)
			.addScalar("autoCorrect", Hibernate.STRING).addScalar("ignoreTbdTime",Hibernate.STRING)
			.addScalar("link",Hibernate.STRING).addScalar("venueCategoryId", Hibernate.INTEGER).
			setResultTransformer(Transformers.aliasToBean(ManageTcapAllTourCsvGenerate.class));
			return query.list();			
			
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
	}
	
	
	public Collection<Event> getAllActiveZoneEvents(){
		return find("FROM Event WHERE eventStatus=? AND zoneEvent=? ORDER BY name, event_date, event_time Asc", new Object[]{EventStatus.ACTIVE, Boolean.TRUE});
	}
	
	public Collection<Event> getAllActiveZoneEventsByTour(Integer artistId){
		return find("FROM Event WHERE eventStatus=? AND zoneEvent=? AND artistId=? ORDER BY name, event_date, event_time Asc", new Object[]{EventStatus.ACTIVE, Boolean.TRUE, artistId});
	}
	
	public Collection<Event> getAllEventsByTour(int artistId) {
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{artistId});
	}
	public Collection<Event> getAllEventsByTour(int artistId,Integer brokerId) {
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' AND brokerId=? ORDER BY event_date, event_time Asc", new Object[]{artistId,brokerId});
	}
	public Collection<Event> getAllEventsByTourOrderdByEventName(int artistId) {
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' ORDER BY name,event_date, event_time Asc", new Object[]{artistId});
	}

	public Collection<Event> getAllActiveEventsByArtistId(Integer artistId) {
		return find("FROM Event WHERE artistId=? AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{artistId});
	}
	
	public Collection<Event> getAllActiveEventsByArtistIdAndBrokerId(Integer artistId,Integer brokerId) {
		return find("FROM Event WHERE artistId=? AND brokerId = ? AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{artistId,brokerId});
	}
	public Collection<Event> getAllActiveEventsByArtistIds(List<Integer> artistIds) {
		String list = artistIds.toString().replace("[", "").replace("]", "").replaceAll(", ", ",");
		return find("FROM Event WHERE artistId in (" + list +") AND eventStatus='ACTIVE' ORDER BY event_date, event_time Asc");//, new Object[]{artistId});
	}
	public Collection<Event> getAllEventsByStatus(EventStatus eventStatus) {
		return find("FROM Event WHERE eventStatus=? ORDER BY event_date, event_time Asc", new Object[]{eventStatus});
	}
	
	public Collection<Event> getAllEventsByStatusAndName(EventStatus eventStatus) {
		return find("FROM Event WHERE eventStatus=? ORDER BY name Asc", new Object[]{eventStatus});
	}

	public Collection<Event> getAllEventsByVenueAndStatus(int venueId, EventStatus eventStatus) {
		return find("FROM Event WHERE venueId=? AND eventStatus=? ", new Object[]{venueId, eventStatus});
	}
	public Collection<Event> getAllEventsByArtistAndVenueAndStatus(int artistId,int venueId, EventStatus eventStatus) {
		return find("FROM Event WHERE venueId=? AND eventStatus=? AND artistId=?", new Object[]{venueId, eventStatus,artistId});
	}	
	public Collection<Event> getAllEventsByVenue(int venueId) {
		return find("FROM Event WHERE venueId=? AND event_status='ACTIVE' ORDER BY event_date, event_time Asc", new Object[]{venueId});
	}
	public Collection<Event> getAllEventsByVenue(int venueId,Integer brokerId) {
		return find("FROM Event WHERE venueId=? AND event_status='ACTIVE' AND brokerId = ? ORDER BY event_date, event_time Asc", new Object[]{venueId,brokerId});
	}
	public Collection<Event> getAllEventsByVenueOrderdByEventName(int venueId) {
		return find("FROM Event WHERE venueId=? AND event_status='ACTIVE' ORDER BY name,event_date, event_time Asc", new Object[]{venueId});
	}
	
	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventName(Integer grandChildCategoryId) {
		return find("SELECT e FROM Event e,Artist a WHERE e.artistId=a.id AND a.grandChildTourCategory.id=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.name,e.localDate, e.localTime", new Object[]{grandChildCategoryId});			
	}

	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventDate(Integer grandChildCategoryId) {
		return find("SELECT e FROM Event e,Artist a WHERE e.artistId=a.id AND a.grandChildTourCategoryId=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.localDate, e.localTime", new Object[]{grandChildCategoryId});			
	}
	public Collection<Event> getAllEventsByGrandChildCategoryOrderdByEventDate(Integer grandChildCategoryId,Integer brokerId) {
		return find("SELECT e FROM Event e,Artist a WHERE e.artistId=a.id AND a.grandChildTourCategoryId=? AND e.eventStatus='ACTIVE' " +
				" AND e.brokerId = ? ORDER BY e.localDate, e.localTime", new Object[]{grandChildCategoryId,brokerId});			
	}
	
	public Collection<Event> getAllEventsByChildCategory(Integer ChildCategoryId, Integer brokerId) {
		return find("SELECT e FROM Event e,Artist a, GrandChildTourCategory g WHERE e.artistId=a.id AND a.grandChildTourCategoryId = g.id AND g.childTourCategory.id=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.name,e.localDate, e.localTime", new Object[]{ChildCategoryId,brokerId});			
	}
	
	public Collection<Event> getAllEventsByChildCategory(Integer ChildCategoryId) {
		return find("SELECT e FROM Event e,Artist a, GrandChildTourCategory g WHERE e.artistId=a.id AND a.grandChildTourCategoryId = g.id AND g.childTourCategory.id=? AND e.eventStatus='ACTIVE' " +
				" ORDER BY e.name,e.localDate, e.localTime", new Object[]{ChildCategoryId});			
	}
	
	public Collection<Event> getAllEventsActiveandExpiredByVenue(int venueId){
		return find("FROM Event WHERE venueId=?", new Object[]{venueId});
	}
	
	public Collection<Event> getAllEventsByNameExcerpt(String excerpt, EventStatus eventStatus) {
		String eventStatusQuery = "";
		if (eventStatus != null) {
			eventStatusQuery = " AND eventStatus='" + eventStatus.toString() + "'";
		}
		return find("FROM Event WHERE name LIKE ? " + eventStatusQuery + " ORDER BY localDate, event_time ASC", new Object[]{"%" + TextUtil.removeExtraWhitespaces(excerpt) + "%"});
	}
	
	public Collection<Event> getAllEventsByNameBrokerId(String excerpt, EventStatus eventStatus,Integer brokerId) {
		String eventStatusQuery = "";
		if (eventStatus != null) {
			eventStatusQuery = " AND eventStatus='" + eventStatus.toString() + "' AND brokerId = "+brokerId+" AND brokerStatus = 'ACTIVE' ";
		}
		return find("FROM Event WHERE name LIKE ? " + eventStatusQuery + " ORDER BY localDate, event_time ASC", new Object[]{"%" + TextUtil.removeExtraWhitespaces(excerpt) + "%"});
	}

	public Collection<Event> getAllEventsByDates(Date startDate, Date endDate, EventStatus eventStatus) {
		String andEventStatusQuery = "";
		if (eventStatus != null) {
			andEventStatusQuery = "AND event_status='" + eventStatus.toString() + "'";
		}
		
		if (startDate == null) {
			if (endDate == null) {
				return find("FROM Event WHERE 1=1 " + andEventStatusQuery + " ORDER BY localDate");				
			} else {
				return find("FROM Event WHERE localDate <= ? " + andEventStatusQuery + " ORDER BY localDate, event_time", new Object[]{endDate});								
			}
		} else if (endDate == null) {
			return find("FROM Event WHERE localDate >= ? " + andEventStatusQuery + " ORDER BY localDate, event_time", new Object[]{startDate});								
		} else {
			return find("FROM Event WHERE localDate >= ? AND localDate <= ? " + andEventStatusQuery + " ORDER BY localDate, event_time", new Object[]{startDate, endDate});			
		}
	}

	public Collection<Event> getAllEventsByVenueAndDates(int venueId, Date startDate, Date endDate) {
		return find("FROM Event WHERE venueId=? AND localDate >= ? AND localDate <= ? AND event_status='ACTIVE'", new Object[]{venueId, startDate, endDate});
	}

	public Collection<Event> getAllEventsByTourVenueAndDates(int artistId, int venueId, Date startDate, Date endDate) {
		return find("FROM Event WHERE artistId=? AND venueId=? AND localDate >= ? AND localDate <= ? AND event_status='ACTIVE'", new Object[]{artistId, venueId, startDate, endDate});
	}

	public Collection<Event> getAllEventsByTourVenue(int artistId, int venueId) {
		return find("FROM Event WHERE artistId=? AND venueId=? AND event_status='ACTIVE'", new Object[]{artistId, venueId});
	}

	public Collection<Event> getAllEventsByIds(List<Integer> eventIds) {
		Session session = getSession();
		Collection<Event> list = null;
		try{
			Properties paramEventStatus = new Properties();
			paramEventStatus.put("enumClass", "com.admitone.tmat.enums.EventStatus");
			paramEventStatus.put("type", "12"); /*type 12 instructs to use the String representation of enum value*/
			
			Properties paramCreationType = new Properties();
			paramCreationType.put("enumClass", "com.admitone.tmat.enums.CreationType");
			paramCreationType.put("type", "12"); 
			
			Properties paramEventType = new Properties();
			paramEventType.put("enumClass", "com.admitone.tmat.enums.TourType");
			paramEventType.put("type", "12"); 
			String sql  = " SELECT id,name , admitone_id as admitoneId, artist_id as artistId, broker_id as brokerId, broker_status as brokerStatus, creation_date as creationDate, creation_type as creationType ,event_status as eventStatus, event_type as eventType,event_date as localDate, " +
			" event_time as localTime , name , venue_category_id as venueCategoryId,venue_id as venueId  FROM event WHERE id in (:eventIds)";
			list = session.createSQLQuery(sql)
			.addScalar("eventStatus", Hibernate.custom(EnumType.class,paramEventStatus))
			.addScalar("creationType", Hibernate.custom(EnumType.class,paramCreationType))
			.addScalar("eventType", Hibernate.custom(EnumType.class,paramEventType))
			.addScalar("id", Hibernate.INTEGER)
			.addScalar("admitoneId", Hibernate.INTEGER)
			.addScalar("artistId", Hibernate.INTEGER)
			.addScalar("brokerId", Hibernate.INTEGER)
			.addScalar("brokerStatus", Hibernate.STRING)
			.addScalar("name", Hibernate.STRING)
			.addScalar("creationDate", Hibernate.DATE)
			.addScalar("localDate", Hibernate.DATE)
			.addScalar("localTime", Hibernate.TIME)
			.addScalar("venueCategoryId", Hibernate.INTEGER)
			.addScalar("venueId", Hibernate.INTEGER)
			.setParameterList("eventIds", eventIds).setResultTransformer(Transformers.aliasToBean(Event.class)).list();
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
		if (list.size() == 0) {
			return null;
		}
		return list;
	}
	public Event getEvent(Integer artistId, Date date) {
		Collection<Event> list = find("FROM Event WHERE artistId=? and day(event_date)=day(?) AND event_status='ACTIVE'", new Object[]{artistId, date});
		if (list.size() == 0) {
			return null;
		}
		
		return list.iterator().next();
	}
	
	public Integer getUnusedId() {
//		Collection<Integer> list = find("SELECT MAX(id) FROM Event WHERE id BETWEEN ? AND ?", new Object[]{CreationEventListManager.OFFSET_AUTO_GEN_EVENT_ID, CreationEventListManager.OFFSET_STUBHUB_EVENT_ID});
		Collection<Integer> list = find("SELECT MAX(id) FROM Event");
		if (list == null || list.size() == 0) {
			return OFFSET_AUTO_GEN_EVENT_ID;
		}
		Object maxId =  list.iterator().next();
		if(maxId != null){
			return ((Integer)maxId + 1);
		} else {
			return OFFSET_AUTO_GEN_EVENT_ID;

		}
	}
	
	public Collection<Event> getEvent(String name, int venueId, Date date, Time time) {
		
		Collection<Event> list = null;
		if(time != null) {
			list = find("FROM Event WHERE name=? and venueId=? and day(event_date)=day(?) and event_time=? AND event_status='ACTIVE'", new Object[]{name, venueId, date, time});
		} else {
			list= find("FROM Event WHERE name=? and venueId=? and day(event_date)=day(?) AND event_status='ACTIVE'", new Object[]{name, venueId, date});
		}
		if (list.isEmpty()) {
			return null;
		}
		
		return list;
	}
	
	private void deleteEmptyEvent(Event event) {
    	// delete bookmarks
    	DAORegistry.getBookmarkDAO().deleteEventBookmarks(event.getId());

    	/*
    	 * They will be deleted by the SQL Server job
    	 */
    	// delete category mappings
    	//DAORegistry.getCategoryMappingDAO().deleteAllByEvent(event.getId());
    	
    	// delete price adjustments
    	//DAORegistry.getEventPriceAdjustmentDAO().deletePriceAdjustments(event.getId());
    	
    	// delete event synonyms
    	//DAORegistry.getEventSynonymDAO().deleteSynonyms(event.getId());
    	
    	// delete short broadcasts
    	//DAORegistry.getShortBroadcastDAO().deleteBroadcastsByEventId(event.getId());
    	
    	// delete market maker mappings
    	//DAORegistry.getMMEventMapDAO().deleteMapsByEventId(event.getId());
    	
    	/*
    	 * ----------------------------------------
    	 */
    	
    	// mark event for deletion
    	event.setEventStatus(EventStatus.DELETED);
    	update(event);
    	//super.deleteById(event.getId());
    	
    	// update tours stat
//    	Tour tour = event.getTour();
//    	DAORegistry.getTourDAO().update(tour);    	
	}
	
	public boolean updateAll(Collection<Event> entities) {
		/***/
    	Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null){
			for(Event event:entities){
				String dependents = property.getValue();
				if(dependents!= null && !dependents.isEmpty()){
					List<TMATDependentEvent> list = DAORegistry.getTmatDependentEventDAO().getTmatDependentsByEventId(event.getId());
					List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
					for(TMATDependentEvent dependent:list){
						if(dependents.contains(dependent.getDependent())){
								dependents= dependents.replace(dependent.getDependent(),"");
								dependent.setAdd(true);
								tmatDependents.add(dependent);
						}
					}
					String depenedentsList[] = dependents.split(",");
					
					for(String dependent:depenedentsList){
						if(dependent==null ||  dependent.isEmpty()){
							continue;
						}
						
						TMATDependentEvent tmatDependent = new TMATDependentEvent();
						tmatDependent.setDependent(dependent);
						tmatDependent.setEventId(event.getId());
						tmatDependent.setAdd(true);
						tmatDependents.add(tmatDependent);
						
					}
					if(!tmatDependents.isEmpty()){
						DAORegistry.getTmatDependentEventDAO().saveOrUpdateAll(tmatDependents);
					}
				}
			}
			
		}
		/***/
		return super.updateAll(entities);
	}
    public void deleteById(Integer id) {
    	Event event = DAORegistry.getEventDAO().get(id);
    	
    	// delete ebay events
//    	DAORegistry.getEbayInventoryEventDAO().deleteById(id);
    	
    	// delete bookmarks
    	DAORegistry.getBookmarkDAO().deleteTicketBookmarksWithEventId(id);
    	DAORegistry.getBookmarkDAO().deleteEventBookmarks(id);

    	/*
    	 * They will be moved to tmathistory by the sql job
    	 */
    	// delete tickets
    	//DAORegistry.getTicketDAO().deleteTicketsByEventId(id);

    	// delete category mappings
    	//DAORegistry.getCategoryMappingDAO().deleteAllByEvent(id);
    	
    	// delete price adjustments
    	//DAORegistry.getEventPriceAdjustmentDAO().deletePriceAdjustments(id);
    	
    	// delete event synonyms
    	//DAORegistry.getEventSynonymDAO().deleteSynonyms(id);
    	
    	// delete short broadcasts
    	DAORegistry.getShortBroadcastDAO().deleteBroadcastsByEventId(id);

    	// delete shorts
    	DAORegistry.getShortTransactionDAO().deleteShortsByEvent(id);
    	
    	// delete market maker mappings
    	//DAORegistry.getMMEventMapDAO().deleteMapsByEventId(id);
    	
    	// delete crawls
    	// get a copy of the list as we iterate on it at the same time than we delete entries
    	
    	Collection<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(id);
    	
    	//Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(SpringUtil.getTicketListingCrawler().getTicketListingCrawlsByEventId(id));
    	for (TicketListingCrawl crawl: crawls) {
    		//SpringUtil.getTicketListingCrawler().removeTicketListingCrawl(crawl);
			DAORegistry.getTicketListingCrawlDAO().deleteById(crawl.getId());    		
    	}
    	/***/
    	Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null){
			String dependents = property.getValue();
			if(dependents!= null && !dependents.isEmpty()){
				List<TMATDependentEvent> list = DAORegistry.getTmatDependentEventDAO().getTmatDependentsByEventId(event.getId());
				List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
				for(TMATDependentEvent dependent:list){
					if(dependents.contains(dependent.getDependent())){
//						if(!dependent.isAdd()){
//							dependents= dependents.replace(dependent.getDependent(),"");
//						}else{
							dependents= dependents.replace(dependent.getDependent(),"");
							dependent.setAdd(false);
							tmatDependents.add(dependent);
//						}
					}
				}
				String depenedentsList[] = dependents.split(",");
				
				for(String dependent:depenedentsList){
					if(dependent==null ||  dependent.isEmpty()){
						continue;
					}
					
					TMATDependentEvent tmatDependent = new TMATDependentEvent();
					tmatDependent.setDependent(dependent);
					tmatDependent.setEventId(event.getId());
					tmatDependent.setAdd(false);
					tmatDependents.add(tmatDependent);
					
				}
				if(!tmatDependents.isEmpty()){
					DAORegistry.getTmatDependentEventDAO().saveOrUpdateAll(tmatDependents);
				}
			}
		}
		/***/
    	// mark event for deletion
    	event.setEventStatus(EventStatus.DELETED);
    	String username = SecurityContextHolder.getContext().getAuthentication().getName();
		event.setUpdatedBy(username);
    	update(event);
    	Date now = new Date();
		EventAudit ea=new EventAudit();
		ea.setEventId(event.getId());
		ea.setUserName(username);
		ea.setAction("Removed");
		ea.setModifiedDate(now);
		Event oldEvent=DAORegistry.getEventDAO().get(event.getId());
		ea.setEventName(oldEvent.getName());
		//ea.setNewEventName(event.getName());
		/*ea.setTourName(event.getTour().getName());
		ea.setArtistName(event.getTour().getArtist().getName());
		ea.setArtistId(event.getTour().getArtistId());*/
		ea.setArtistName(event.getArtist().getName());
		ea.setArtistId(event.getArtistId());
		//ea.setNewTourName(event.getTour().getName());
		ea.setEventDate(event.getLocalDate());
		//ea.setNewEventDate(event.getLocalDate());
		ea.setEventTime(event.getLocalTime());
		//ea.setNewEventTime(event.getLocalTime());
		ea.setVenueName(event.getVenue().getBuilding());
		//ea.setNewVenueName(event.getVenue().getBuilding());
		ea.setVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
/*		if(event.getVenueCategoryId()!= null)
		{
			ea.setNewVenueCategory(DAORegistry.getVenueCategoryDAO().getCategoryGroupByVenueCategoryId(event.getVenueCategoryId()).toString());
		}
		else
		{
		ea.setNewVenueCategory(null);
		}
*/	
		if(event.getEventType().name()!=null){
		ea.setEventType(event.getEventType().name());
		}
		else
		{
			ea.setEventType(null);
		}

		/*if(event.getEventType().name()!= null)
		{
		ea.setNewEventType(event.getEventType().name());
		}
		else
		{
		ea.setNewEventType(null);
		}*/
		if(event.getNoPrice()!=null){
		ea.setNoPrice("YES");
		}
		else
		{
			ea.setNoPrice("NO");
		}

		/*if(event.getNoPrice()!= null){
		ea.setNewNoPrice("YES");
		}
		else
		{
		ea.setNewNoPrice("NO");
		}*/
		if(event.getAdmitoneId()!=null){
		ea.setAdmitoneId(event.getAdmitoneId());
		}
		else
		{
			ea.setAdmitoneId(null);
		}
	//	ea.setTourId(event.getTourId());
		ea.setVenueId(event.getVenueId());
		/*if(event.getAdmitoneId()!= null)
		{
		ea.setNewAdmitoneId(event.getAdmitoneId());
		}
		else
		{
		ea.setNewAdmitoneId(null);
		}*/
		DAORegistry.getEventAuditDAO().save(ea);
    	
//    	Tour tour = event.getTour();
//    	DAORegistry.getTourDAO().update(tour);    	
    }
    
    /*
    public void updateAllEventTicketValues() {
    	Date now = new Date();
    	if (now.getTime() - lastEventUpdate.getTime() > 5 * 60 * 1000) {
        	for (Event event: DAORegistry.getEventDAO().getAll()) {
        		updateEventTicketValues(event);
        	}
    		lastEventUpdate = now;
    	}
    }*/

    public Collection<Event> getEmptyEvents() {
    	Collection<Event> emptyEvents = (Collection<Event>)find("FROM Event e WHERE e.id NOT IN (SELECT DISTINCT(tlc.eventId) FROM TicketListingCrawl tlc WHERE tlc.eventId IS NOT NULL AND tlc.enabled=1) AND event_status!='ACTIVE'");
    	//Collection<Event> emptyEvents = (Collection<Event>)find("FROM Event e WHERE e.id NOT IN (SELECT DISTINCT(tlc.eventId) FROM TicketListingCrawl tlc WHERE tlc.eventId IS NOT NULL)");
    	return emptyEvents;
    }
    
    public void deleteEmptyEvents() {
    	for(Event event: this.getEmptyEvents()){
    		try{
    			deleteById(event.getId());
    		} catch(Exception e) {
    			e.printStackTrace();
    		}
    		
    	}
    	
    	for (Event event: DAORegistry.getEventDAO().getAll()) {
    		// if there are tickets => skip
    		if (!DAORegistry.getTicketDAO().getAllTicketsByEvent(event.getId()).isEmpty()) {
    			continue;
    		}

    		// there is a crawler associated to the tour for any events => skip
    		//if (!DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByTourWithNoEvent(event.getTourId()).isEmpty()) {
    		//	continue;
    		//}

    		// the event is associated to a crawler => skip
    		if (!DAORegistry.getTicketListingCrawlDAO().getAllActiveTicketListingCrawlByEvent(event.getId()).isEmpty()) {
    			continue;
    		}

    		// the tour is associated to categories => skip
    		//if (!DAORegistry.getCategoryDAO().getAllCategories(event.getTourId()).isEmpty()) {
    		//	continue;
    		//}

    		// there are shorts for this event => skip
    		if (!DAORegistry.getShortTransactionDAO().getAllShortsByEvent(event.getId().intValue()).isEmpty()) {
    			continue;
    		}

    		deleteById(event.getId());
    	}
    }
    
	public Event.EventTicketStat getNonDuplicateEventTicketStat(int eventId) {
		
		Session session = getSession();
		
//		session.beginTransaction();
		SQLQuery query = session.createSQLQuery("SELECT COUNT(remaining_quantity), SUM(remaining_quantity) FROM (select remaining_quantity FROM ticket WHERE event_id=:eventId and ticket_status='ACTIVE' GROUP BY ticket_duplicate_key) as o");
		query.setInteger("eventId", eventId);
		List<Object[]> list = query.list();
		releaseSession(session);
		
		if (list == null || list.size() == 0) {
			return null;
		}
		
		int ticketListingCount = 0;
		int ticketCount = 0;
		
		if (list.get(0)[0] != null) {
			ticketListingCount = ((BigInteger)list.get(0)[0]).intValue(); 
		}
		if (list.get(0)[1] != null) {
			ticketCount = ((BigDecimal)list.get(0)[1]).intValue(); 
		}
		
		return new Event.EventTicketStat(ticketListingCount, ticketCount);
	}
    
    public void delete(Event event) {
    	deleteById(event.getId());
    }    
    
	public int getEventCount() {
		List list = find("SELECT count(id) FROM Event WHERE event_status = 'ACTIVE'");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
	
	public int getEventCountSport() {
		List sports = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Sports'");
		if (sports == null || sports.get(0) == null) {
			return 0;
		}
		return ((Long)sports.get(0)).intValue();		
	}
	public int getEventCountConcerts() {
		List concerts = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Concerts'");
		if (concerts == null || concerts.get(0) == null) {
			return 0;
		}
		return ((Long)concerts.get(0)).intValue();		
	}
	public int getEventCountDefault() {
		List defaultList = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Default'");
		if (defaultList == null || defaultList.get(0) == null) {
			return 0;
		}
		return ((Long)defaultList.get(0)).intValue();		
	}
	public int getEventCountTheater() {
		List theaterList = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Theater'");
		if (theaterList == null || theaterList.get(0) == null) {
			return 0;
		}
		return ((Long)theaterList.get(0)).intValue();		
	}
	public int getEventCountOther() {
		List otherList = find("SELECT count(*)  FROM Event e,  Artist a,  GrandChildTourCategory gc,  ChildTourCategory cc,  TourCategory tc where e.artistId=a.id and a.grandChildTourCategoryId=gc.id and gc.childTourCategory.id=cc.id and cc.tourCategory.id=tc.id  and e.eventStatus='ACTIVE' and tc.name='Other'");
		if (otherList == null || otherList.get(0) == null) {
			return 0;
		}
		return ((Long)otherList.get(0)).intValue();		
	}
	public int getEventCountLasVegas() {
		List sports = find("SELECT count(*)  FROM Event a,  Tour b,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory e where a.artistId=b.id and a.grandChildTourCategoryId=c.id and c.childTourCategoryId=d.id and d.tourCategoryId=e.id  and a.eventStatus='ACTIVE' and e.name='Las Vegas'");
		if (sports == null || sports.get(0) == null) {
			return 0;
		}
		return ((Long)sports.get(0)).intValue();		
	}

	public int getEventCountByTour(Integer artistId) {
		List list = find("SELECT count(id) FROM Event WHERE artistId=? AND event_status = 'ACTIVE'", new Object[]{artistId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}
	
	public int getMaxEventId() {
		List list = find("SELECT MAX(id) FROM Event");
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Integer)list.get(0)).intValue();		
	}

	public Event getEventByStubhubId(Integer stubhubId) {
    	Collection<Event> events = find("FROM Event WHERE stubhubId=?", new Object[]{stubhubId});
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events.iterator().next();
	}

	public Event getEventByAdmitoneId(Integer admitoneId) {
		Collection<Event> events = find("FROM Event WHERE admitoneId=? AND eventStatus = ? ", new Object[]{admitoneId,EventStatus.ACTIVE});
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events.iterator().next();
	}

	public Collection<Event> getAllEventByAdmitoneId() {
    	Collection<Event> events = find("FROM Event WHERE admitoneId is not null AND event_status = 'ACTIVE'");
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events;
	}
	//TMATT-385
	
	/*public Collection<Event> getAllEventByAdmitoneIdNull() {
    	Collection<Event> events = find("FROM Event WHERE admitoneId is null AND event_status = 'ACTIVE' order by name");
    	if (events.size() == 0) {
    		return null;
    	}
    	
    	return events;
	}*/
	public Collection<Event> getAllEventByAdmitoneIdNull(){
    	Collection<Event> events = find("FROM Event WHERE event_status = 'ACTIVE' and (admitoneId is null or admitoneId not in(select eventId from AdmitoneEvent))");
    	if (events.size() == 0) {
    		return null;
    	}
		return events;
	}//select * from [tmatprodscale].[dbo].[event] where event_status = 'ACTIVE' and 
	//(admitone_id is null or admitone_id not in(select eventid from [tmatprodscale].[dbo].[admitone_event]) )
	
	public Collection<Event> getAllEventsToBeUnlinked(){
		Collection<Event> events = DAORegistry.getQueryManagerDAO().getAllEventsToBeUnlinked();//find("FROM Event WHERE admitoneId is not in( SELECT eventId FROM AdmitoneEvent) AND event_status = 'ACTIVE'");
		Collection<Event> allEvents = getAllEventByAdmitoneId();
    	if (events == null || events.size() == 0 || allEvents.size() == events.size()) {
    		return null;
    	}
    	return events;
	}
	
	public Collection<Event> getAllEventsByTourAndAdmitoneId(int artistId) {
		return find("FROM Event WHERE artistId=? AND event_status='ACTIVE' AND admitoneId is not null ORDER BY event_date, event_time Asc", new Object[]{artistId});
	}
	public Collection<Event> searchEvents(String name, java.sql.Date date, Time time,
			String location) {
		Collection<Event> result = new ArrayList<Event>();
		
		// get all events where date and time matches then do the filtering on name and location
		Collection<Event> events = find("FROM Event WHERE localDate=? and localTime=?", new Object[]{date, time});
		
		for (Event event: events) {
			Venue venue = event.getVenue();
			String eventLocation = venue.getBuilding() + " " + venue.getCity() + " " + venue.getState() + " " + venue.getCountry();
			if (TextUtil.isSimilar(name, event.getName())
				&& TextUtil.isSimilar(location, eventLocation)) {
				result.add(event);
			}
		}
		
		return result;
	}
	
	public Collection<Event> getAllEventsNotInStubhub() {
		return find("FROM Event WHERE stubhubId=null");
	}
	

    public void update(Event event) {
    	super.update(event);
//    	DAORegistry.getTourDAO().update(event.getTour());
    }
    
    public Integer save(Event event) {
    	Integer result = super.save(event);
//    	Tour tour = event.getTour();
//    	DAORegistry.getTourDAO().update(tour);
    	return result;
    }

    public void saveOrUpdate(Event event) {
    	super.saveOrUpdate(event);
//    	Tour tour = event.getTour();
//    	DAORegistry.getTourDAO().update(tour);
    }

    public void saveOrUpdateAll(Collection<Event> list) {
    	if (list.isEmpty()) {
    		return;
    	}
    	
    	super.saveOrUpdateAll(list);
    	
//    	Set<Tour> tours = new HashSet<Tour>();
    	/*Map<Integer, Tour> map = new HashMap<Integer, Tour>();
    	for (Event event: list) {
    		Tour tour= event.getTour();
    		map.put(tour.getId(),tour);
    	}
    	
    	for (Tour tour: map.values()) {
        	DAORegistry.getTourDAO().update(tour);    		
    	}  */  	
    }

    public void saveAll(final Collection<Event> list) {
    	if (list.isEmpty()) {
    		return;
    	}

    	super.saveAll(list);
    	
    	/*Set<Tour> tours = new HashSet<Tour>();
    	for (Event event: list) {
    		tours.add(event.getTour());
    	}
    	
    	for (Tour tour: tours) {
        	DAORegistry.getTourDAO().update(tour);    		
    	}*/
    }
    
    public void deleteAll(final Collection<Event> list) {
    	if (list == null || list.isEmpty()) {
    		return;
    	}
    	
//    	TicketListingCrawler ticketListingCrawler = SpringUtil.getTicketListingCrawler();
//    	Set<Tour> tours = new HashSet<Tour>();
    	// mark event for deletion
    	for (Event event: list) {
    		event.setEventStatus(EventStatus.DELETED);
//    		tours.add(event.getTour());
//    		ticketListingCrawler.disableCrawlsForEvent(event.getId());
//    		DAORegistry.getTicketListingCrawlDAO().deleteByEventId(event.getId());
    	}
    	saveOrUpdateAll(list);
    	
//    	for (Tour tour: tours) {
//        	DAORegistry.getTourDAO().update(tour);    		
//    	}
    }

	public Date getMaxDateFromEvents(Integer artistId) {
		List list = find("SELECT max(localDate) FROM Event WHERE artistId=? AND event_status='ACTIVE'", new Object[]{artistId});
		if (list == null || list.get(0) == null) {
			return null;
		}
		return (Date)list.get(0);		
	}

	public Date getMinDateFromEvents(Integer artistId) {
		List list = find("SELECT min(localDate) FROM Event WHERE artistId=? AND event_status='ACTIVE'", new Object[]{artistId});
		if (list == null || list.get(0) == null) {
			return null;
		}
		return (Date)list.get(0);		
	}

	public Event getEventWithSameTourAndVenueButDifferentFromEvent(Integer artistId, Integer venueId, Integer eventId) {
		return findSingle("FROM Event WHERE artistId=? AND venueId=? and id<>?", new Object[]{artistId, venueId, eventId});
	}
	
	// collection of two sized array:
	// [eventId, number of active tickets]
	public List<Event> getTopTicketCountEvents(final int eventCount) {
		return (List<Event>)getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException,
					SQLException {
				String hql = "SELECT eventId, SUM(1), SUM(remainingQuantity) FROM Ticket WHERE ticketStatus=:ticketStatus GROUP BY eventId ORDER BY SUM(remainingQuantity) DESC";
				Query query = session.createQuery(hql);
				query.setParameter("ticketStatus", TicketStatus.ACTIVE);
				query.setFetchSize(eventCount);
				query.setMaxResults(eventCount);
				List<Object[]> results = query.list();
				if (results == null) {
					return null;
				}
				
				Map<Integer, EventTicketStat> eventTicketStatByEventId = new HashMap<Integer, EventTicketStat>();
				
				String eventIds = "";
				List<Event> eventTicketStats = new ArrayList<Event>();
				for(Object[] result: results) {
					Integer eventId = (Integer)result[0];
					Integer ticketListingCount = ((Long)result[1]).intValue();
					Integer ticketCount = ((Long)result[2]).intValue();
					EventTicketStat eventTicketStat = new EventTicketStat(ticketListingCount, ticketCount);
					eventTicketStatByEventId.put(eventId, eventTicketStat);
					
					if (!eventIds.isEmpty()) {
						eventIds += ",";
					}
					eventIds += eventId;
				}
				
				List<Event> events = find("FROM Event WHERE id in (" + eventIds + ")");
				for(Event event: events) {
					event.setEventTicketStat(eventTicketStatByEventId.get(event.getId()));
				}

				/*Collections.sort(events, new Comparator<Event>() {

					
					public int compare(Event event1, Event event2) {
						Integer eventCount1 = event1.getEventTicketStat().getTicketCount();
						Integer eventCount2 = event2.getEventTicketStat().getTicketCount();
						
						return -eventCount1.compareTo(eventCount2);
					}
				});*/
				
				
				try {					
					events.sort(new Comparator<Event>() {
				    @Override
				    public int compare(final Event model1, final Event model2) {						    	
				    	Integer eventCount1 = model1.getEventTicketStat().getTicketCount();
						Integer eventCount2 = model2.getEventTicketStat().getTicketCount();
						
						return -eventCount1.compareTo(eventCount2);
					}
				});
				}catch(Exception ex) {
					System.out.println("Error Sorting in EventDAO  Line 977 " + ex);
				}				
				return events;
				
			}	
		});
	}

	
	public Event getEventWithSameTourAndVenueAndName(Integer artistId, Integer venueId, String eventName) {
		// TODO Auto-generated method stub
		return findSingle("FROM Event WHERE artistId=? AND venueId=? and name=?", new Object[]{artistId, venueId, eventName});
	}

	
	public Event getEventWithSameTourAndVenueAndNameAndDateTBDAndTimeTBD(
			Integer artistId, Integer venueId, String eventName) {
		// TODO Auto-generated method stub
		return findSingle("FROM Event WHERE artistId=? AND venueId=? and name=? and localDate = null and localTime = null", new Object[]{artistId, venueId, eventName});
	}
	
	
	public Collection<Event> getUnLinkedEmptyEvents(String firstLetter) {
		Collection<Event> emptyEvents = (Collection<Event>)find("FROM Event e WHERE event_status ='ACTIVE' AND (admitoneId is null or admitoneId not in(select eventId from AdmitoneEvent)) AND e.name like ? ORDER BY e.localDate ",new Object[]{firstLetter + "%"});
    	//Collection<Event> emptyEvents = (Collection<Event>)find("FROM Event e WHERE event_status ='ACTIVE' AND admitoneId IS NULL AND e.name like ? ORDER BY e.localDate ",new Object[]{firstLetter + "%"});
    	//Collection<Event> emptyEvents = (Collection<Event>)find("FROM Event e WHERE e.id NOT IN (SELECT DISTINCT(tlc.eventId) FROM TicketListingCrawl tlc WHERE tlc.eventId IS NOT NULL)");
    	return emptyEvents;
    }

	public Collection<Event> getTBDEvents(String firstLetter) {
		Collection<Event> tbdEvents = (Collection<Event>)find("FROM Event e WHERE e.localDate IS NULL  AND event_status ='ACTIVE' AND e.name like ? ORDER BY e.name ",new Object[]{firstLetter + "%"});
		return tbdEvents;
	}

	
	public List<Event> getToBeAutoCorrectEvents() {
		return find("FROM Event e WHERE e.autoCorrect = ?" , new Object[]{true});
	}

	/*public Collection<Event> getAllEventsByTourIdAndCategoryGroup(int artistId,String categoryGroup) {
		return find("FROM Event WHERE artistId = ? AND venueCategory.categoryGroup = ?", new Object[]{artistId,categoryGroup});
	}*/
	
	public Collection<Event> getAllEventsByTourIdAndCategoryGroup(int artistId,String categoryGroup) {
		return find("FROM Event WHERE artistId = ? AND venueCategoryId IN (SELECT id FROM VenueCategory WHERE categoryGroup = ?)",
				new Object[]{artistId,categoryGroup});
	}

	public List<Event> getAllEventsByVenueCategoryId(Integer venueCategoryId) {
		return find("FROM Event Where venueCategoryId = ?" ,new Object[]{venueCategoryId});
	}
	
	public boolean unLinkAdmintoneEvent(Integer eventId){
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("UPDATE event SET admitone_id=null where id="+eventId+"");
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}finally{
			session.close();
		}
		return true;
	}

	@Override
	public Collection<Event> getAllEventsByGrandChildCategoryAndDateRange(Date startDate, Date endDate, Integer grandChildCategoryId, EventStatus eventStatus) {
		String query = "SELECT e FROM Event e,Artist a WHERE e.artistId=a.id ";
		List<Object> param = new ArrayList<Object>();
		if (eventStatus != null) {
			query += " AND e.eventStatus=?";
			param.add(eventStatus);
		}
		if (startDate != null) {
			query += " AND e.localDate>=?";
			param.add(startDate);
		} 
		if (endDate != null) {
			query += " AND e.localDate<=?";
			param.add(endDate);								
		} 
		if(grandChildCategoryId!=null){
			query+= " AND a.grandChildTourCategory.id=?";
			param.add(grandChildCategoryId);						
		}
		return find(query + " ORDER BY e.localDate, e.localTime", param.toArray());			
		
	}
	
	public List<Event> getAllActiveEventsWithParentType(String parentType){
			
		Session session = getSession();
		try{
			Query query = session.createQuery("select e from Event e,Artist a,  GrandChildTourCategory c,  ChildTourCategory d,  TourCategory tc where " +
					" e.artistId=a.id and a.grandChildTourCategoryId=c.id and c.childTourCategory=d.id and d.tourCategory=tc.id and " +
					" tc.name = :parentType and tc.id is not null and e.eventStatus = :eventStatus " +
					" order by e.name,e.localDate,e.localTime").setParameter("parentType", parentType).setParameter("eventStatus", EventStatus.ACTIVE);
			return query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		
		return new ArrayList<Event>();		
		
	}
	
	public void updateBrokerEvents(ArrayList<Integer> eventIds,Integer brokerId,String brokerStatus){
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("update event set broker_id = :brokerId,broker_status = :brokerStatus WHERE id in (:eventIds) ").setParameterList("eventIds", eventIds).setParameter("brokerId", brokerId).setParameter("brokerStatus", brokerStatus);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
	}
	
	public void updateBrokerEvents(ArrayList<Integer> eventIds){
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("update event set broker_id = '',broker_status = '' WHERE id in (:eventIds) ").setParameterList("eventIds", eventIds);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
	}
	
	public void updateBrokerEventsBrowseLite(ArrayList<Integer> eventIds,String brokerStatus){
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("update event set broker_status = :brokerStatus WHERE id in (:eventIds) ").setParameterList("eventIds", eventIds).setParameter("brokerStatus", brokerStatus);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
	}

	public void deleteEventsByArtistId(int artistId){
		Session session = getSession();
		try{
			Query query = session.createSQLQuery("UPDATE event SET event_status = :status WHERE artist_id = :artistId").setParameter("status", EventStatus.DELETED).setParameter("artistId",artistId);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
	}

	@Override
	public Event getEventWithSameArtistAndVenueAndNameAndDateTBDAndTimeTBD(
			Integer artistId, Integer venueId, String eventName) {
		
		return findSingle("FROM Event WHERE artistId=? AND venueId=? and name=? and localDate = null and localTime = null", new Object[]{artistId, venueId, eventName});
	}
	
	public Collection<Event> getAllEventsByArtistVenueAndDates(int artistId, int venueId, Date startDate, Date endDate) {
		return find("FROM Event WHERE artistId=? AND venueId=? AND localDate >= ? AND localDate <= ? AND event_status='ACTIVE'", new Object[]{artistId, venueId, startDate, endDate});
	}
	
	public Collection<Event> getAllActiveEventsBySqlQuery() {
		
		Session session = getSession();
		SQLQuery query = session.createSQLQuery("select id,name,CONVERT(VARCHAR(19),event_date,101) as eventDate," +
				"CASE WHEN event_time is not null  THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_time, 100), 7)) " +
				"WHEN event_time is null THEN 'TBD' END as eventTime from event order by name asc");
		
		List<Object[]> list = query.list();
		List<Event> events= new ArrayList<Event>();
		Event event =null;
		for(Object[] obj:list){
			event = new Event();
			event.setId((Integer)obj[0]);
			event.setName((String)obj[1]);
			event.setTempEventDateStr((String)obj[2]);
			event.setTempEventTimeStr((String)obj[3]);
			events.add(event);
		}
		releaseSession(session);
		return events;
	
	}
	
	 public Collection<Event> filterByName(String pattern) {
	    	
	    	String hql="FROM Event WHERE eventStatus=? AND name LIKE ? ORDER BY name ASC";
			List<Object> parameters = new ArrayList<Object>();
			parameters.add(EventStatus.ACTIVE);
			parameters.add("%" + pattern + "%");
			return find(hql, parameters.toArray());
			
	}

	@Override
	public Collection<Event> getAllEligiblePresaleEventsByArtistIdAndVenueId(Integer artistId, Integer venueId) {
		String sql ="SELECT e FROM Event e,VenueCategory vc WHERE e.venueCategoryId = vc.id AND e.presaleEvent=? AND e.eventStatus = 'ACTIVE'";
		if(artistId!=null && artistId!=0){
			sql+=" AND e.artistId = " + artistId;
		}
		if(venueId!=null && venueId!=0){
			sql+=" AND e.venueId = " + venueId;
		}
		return find(sql,new Object[]{Boolean.TRUE});
	}
	
	@Override
	public Collection<Event> getAllOpenOrderEventsByArtistandVenueId(Integer artistId, Integer venueId) {
		String sql ="SELECT e FROM Event e,OpenOrderStatus vc WHERE e.id = vc.eventId  AND e.eventStatus = 'ACTIVE' " +
				"AND vc.status='ACTIVE' AND vc.eventId <> -1 ";
		if(artistId!=null && artistId!=0){
			sql+=" AND e.artistId = " + artistId;
		}
		if(venueId!=null && venueId!=0){
			sql+=" AND e.venueId = " + venueId;
		}
		sql+=" ORDER BY e.localDate, e.localTime Asc";
		System.out.println(sql);
		return find(sql);
	}
	
	public Collection<Event> getAllEventsByEventCreationDates(Date startDate, Date endDate, EventStatus eventStatus) {
		String andEventStatusQuery = "";
		if (eventStatus != null) {
			andEventStatusQuery = "AND event_status='" + eventStatus.toString() + "'"; 
		}
		
		if (startDate == null) {
			if (endDate == null) {
				return find("FROM Event WHERE 1=1 " + andEventStatusQuery + " ORDER BY creationDate");				
			} else {
				return find("FROM Event WHERE creationDate <= ? " + andEventStatusQuery + " ORDER BY creationDate", new Object[]{endDate});								
			}
		} else if (endDate == null) {
			return find("FROM Event WHERE creationDate >= ? " + andEventStatusQuery + " ORDER BY creationDate", new Object[]{startDate});								
		} else {
			return find("FROM Event WHERE creationDate >= ? AND creationDate <= ? " + andEventStatusQuery + " ORDER BY creationDate", new Object[]{startDate, endDate});			
		}
		
	}
	
	
}
