package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.AdmitoneInventory;

public class AdmitoneInventoryDAO extends HibernateDAO<Integer, AdmitoneInventory> implements com.admitone.tmat.dao.AdmitoneInventoryDAO {

	@Deprecated
	public Collection<AdmitoneInventory> getAllInventoryByDate(Date eventDate) {
		return new ArrayList<AdmitoneInventory>();
		//return find("FROM AdmitoneInventory WHERE eventId=?", new Object[]{eventId});
	}
	
	public Collection<AdmitoneInventory> getAllInventoryByAdmitOneEventId(Integer eventId) {
		return find("FROM AdmitoneInventory WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<AdmitoneInventory> getAllInventoryByEventId(Integer eventId) {
		return find("SELECT ae FROM AdmitoneInventory ae, Event e WHERE e.id=? AND e.admitoneId=ae.eventId", new Object[]{eventId});
	}

}
