package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.MiniTickPickExchangeEvent;

public class MiniTickPickExchangeEventDAO extends HibernateDAO<Integer,MiniTickPickExchangeEvent> implements com.admitone.tmat.dao.MiniTickPickExchangeEventDAO{

	public Collection<MiniTickPickExchangeEvent> getAllMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniTickPickExchangeEvent> result = new ArrayList<MiniTickPickExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniTickPickExchangeEvent> temp = find("FROM MiniTickPickExchangeEvent WHERE event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public Collection<MiniTickPickExchangeEvent> getAllActiveMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniTickPickExchangeEvent> result = new ArrayList<MiniTickPickExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniTickPickExchangeEvent> temp = find("FROM MiniTickPickExchangeEvent WHERE status='ACTIVE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection<MiniTickPickExchangeEvent> getAllDeletedMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniTickPickExchangeEvent> result = new ArrayList<MiniTickPickExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniTickPickExchangeEvent> temp = find("FROM MiniTickPickExchangeEvent WHERE status='DELETE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAllMiniTickPickExchangeEventsByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("DELETE FROM MiniTickPickExchangeEvent WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public void updateAllMiniTickPickExchangeEventAsDeleteByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("UPDATE MiniTickPickExchangeEvent SET status='DELETE' WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public List<MiniTickPickExchangeEvent> getAllActiveMiniTickPickExchangeEvent() {
		try{
			return find("FROM MiniTickPickExchangeEvent WHERE event.id IN(SELECT DISTINCT eventId from TGCatsCategoryTicket where status ='ACTIVE')", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveBulkEvents(List<MiniTickPickExchangeEvent> tnEvents) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (MiniTickPickExchangeEvent event : tnEvents) {
			    session.save(event);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
	
}
