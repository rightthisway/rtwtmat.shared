package com.admitone.tmat.dao.hibernate.shard;

import java.sql.SQLException;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

public abstract class ShardRoutingDataSource extends AbstractRoutingDataSource {

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return false;
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return null;
	}

	@Override
	protected Object determineCurrentLookupKey() {
		return ShardContextHolder.getShardContext();
	}
	   
}
