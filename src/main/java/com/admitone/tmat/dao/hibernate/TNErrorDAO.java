package com.admitone.tmat.dao.hibernate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.TNError;

public class TNErrorDAO extends HibernateDAO<Integer, TNError> implements com.admitone.tmat.dao.TNErrorDAO {

public Collection<TNError> getAllTNErrorByDate(Date date){
DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
DateFormat dft = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
try {
	Date startDate = dft.parse(df.format(date)+ " 00:00:00");
	Date endDate = dft.parse(df.format(date)+ " 23:59:59");
	return find("FROM TNError WHERE timeStamp >= ? and timeStamp <= ? ", new Object[]{startDate,endDate});
//	return find("FROM TNError");
} catch (ParseException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
return null;
}

}