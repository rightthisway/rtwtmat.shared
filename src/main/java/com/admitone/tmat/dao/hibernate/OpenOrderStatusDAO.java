package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.OpenOrderStatus;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.ProfitLossSign;

public class OpenOrderStatusDAO extends HibernateDAO<Integer, OpenOrderStatus> implements com.admitone.tmat.dao.OpenOrderStatusDAO{

	

	@Override
	public List<OpenOrderStatus> getAllActiveRTWOpenOrders() {
		return find("FROM OpenOrderStatus WHERE status = 'ACTIVE' ");
	}
	
	@Override
	public List<OpenOrderStatus> getSoldTicketDetailsByBrokerId(Integer brokerId) {
		return find("FROM OpenOrderStatus WHERE brokerId = ? ", new Object[]{brokerId});
	}

	@Override
	public List<OpenOrderStatus> getSoldTicketDetailsByBrokerIdAndEventId(Integer brokerId, Integer eventId) {
		return find("FROM OpenOrderStatus WHERE brokerId = ? AND eventId = ?", new Object[]{brokerId,eventId});
	}

	@Override
	public List<OpenOrderStatus> getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(
			Integer brokerId,Integer artistId, Integer eventId, Date startDate, Date endDate,ProfitLossSign profitLossSign) {
		String url ="FROM OpenOrderStatus WHERE status='ACTIVE' ";
		List<Object> params = new ArrayList<Object>();
		
		if(brokerId!=null ){
			url = url+" AND brokerId = ? ";
			params.add(brokerId);
		}
		
		if(eventId!=null && eventId!= 0){
			url = url+" AND eventId = ? ";
			params.add(eventId);
		}else if(artistId!=null && artistId!= 0 ){
			url = url+" AND eventId in (SELECT id from Event WHERE artistId = ? )";
			params.add(artistId);
		}
		
		if(startDate!=null){
			url = url+" AND invoiceDate >= ? ";
			params.add(startDate);
		}
		if(endDate!=null){
			url = url+" AND invoiceDate <= ? ";
			params.add(endDate);
		}
		
		if(null !=profitLossSign ){
			
			switch (profitLossSign) {
				case POSITIVE:
					url = url+" AND profitAndLoss > 0 ";
					break;
				case NEGATIVE:
					url = url+" AND profitAndLoss < 0 ";
					break;
				case ZERO:
					url = url+" AND profitAndLoss = 0 ";
					break;
				case ALL:
					url = url+"";
					break;
				default:
					break;
			}
		}

		 url = url + "ORDER BY invoiceDate desc"; 
		 System.out.println(url);
		return find(url, params.toArray());
	}
	
	@Override
	public List<Venue> getAllActiveOpenOrdersVenues(String param) {
		List<Venue> venues= new ArrayList<Venue>();
		Session session = getSession();
		try{
			SQLQuery query = session.createSQLQuery("select distinct venue_id as id ,venue_name as name from rtw_cats_open_order_status " +
					"where status='ACTIVE' and venue_name like '%"+param+"%' ");
			List<Object[]> list = query.list();
			Venue venue =null;
			for(Object[] obj:list){
				venue = new Venue();
				venue.setId((Integer)obj[0]);
				venue.setBuilding((String)obj[1]);
				venues.add(venue);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			releaseSession(session);
		}
		return venues;
	}
	
	@Override
	public List<Artist> getAllActiveOpenOrderArtist(String param) {
		List<Artist> artists= new ArrayList<Artist>();
		Session session = getSession();
		try{
			SQLQuery query = session.createSQLQuery("select distinct a.id as id ,a.name as name " +
					"from rtw_cats_open_order_status o inner join event e on e.id=o.event_id  inner join artist a on a.id = e.artist_id  " +
					"where o.status='ACTIVE' and a.name like '%"+param+"%'  ");
			
			List<Object[]> list = query.list();
			Artist artist =null;
			for(Object[] obj:list){
				artist = new Artist();
				artist.setId((Integer)obj[0]);
				artist.setName((String)obj[1]);
				artists.add(artist);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			releaseSession(session);
		}
		return artists;
	}
}
