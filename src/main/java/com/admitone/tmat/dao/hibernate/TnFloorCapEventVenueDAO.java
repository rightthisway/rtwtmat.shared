package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import com.admitone.tmat.data.TnFloorCapEventVenue;

public class TnFloorCapEventVenueDAO extends HibernateDAO<Integer, TnFloorCapEventVenue> 
implements com.admitone.tmat.dao.TnFloorCapEventVenueDAO{
	
	public void deleteTnFloorVenueByVenue(String venue){
		String query = "DELETE TnFloorCapEventVenue where venue=? ";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("" + venue + "");
		bulkUpdate(query, parameters.toArray());
	}
	public List<String> getAllTnFloorCapVenues(Collection<String> parentCategoryList){
		
	  List<String> result = null;
	  StringBuffer buffer = new StringBuffer ("SELECT venue FROM TnFloorCapEventVenue" +
	  		" where venue in (select distinct venueName from AdmitoneEvent where parentCategory in (:parentParam) ) order by venue" );
	  Query query = null;
	  org.hibernate.Session session = getSession();
	  try {   
	   query = session.createQuery(buffer.toString());
	   query.setParameterList("parentParam", parentCategoryList);
	   result = query.list();
	  } catch (Exception e) {		   
		  e.printStackTrace();
	  } finally {
		  session.close();
	  }
	  return result;
	}
}
