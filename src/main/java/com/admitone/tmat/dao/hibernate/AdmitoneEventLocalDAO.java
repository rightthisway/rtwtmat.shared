package com.admitone.tmat.dao.hibernate;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.admitone.tmat.data.AdmitoneEventLocal;

public class AdmitoneEventLocalDAO extends HibernateDAO<Integer, AdmitoneEventLocal> implements com.admitone.tmat.dao.AdmitoneEventLocalDAO {
	public Collection<Integer> getAllEventIds() {
		return find("SELECT id FROM AdmitoneEventLocal");
	}

	public Collection<AdmitoneEventLocal> getAllEventsNearDate(Date eventDate) {
		Calendar c = new GregorianCalendar();
		c.setTime(eventDate);
		//c.add(Calendar.DATE, 0);
		//Date endDate = new Date(c.getTime().getTime()); 
		//c.add(Calendar.DATE, 0);
		Date startDate = new Date(c.getTime().getTime()); 
		//return new ArrayList<AdmitoneEvent>();
		return find("FROM AdmitoneEventLocal WHERE EventDate = ? ORDER BY EventName", new Object[]{startDate});
	}
	
	public AdmitoneEventLocal getEventByEventId(Integer eventId) {
		return findSingle("FROM AdmitoneEventLocal WHERE eventId = ? ", new Object[]{eventId});
	}
	
	public AdmitoneEventLocal getEventByVenueCityStateDateTime(String venueName,String cityName,String stateName, Date eventDate, Date eventTime) {
		AdmitoneEventLocal admitoneEventLocal = null;
		try {
			List<AdmitoneEventLocal> localEvents = find("FROM AdmitoneEventLocal WHERE venueName like ? AND cityName = ? AND stateName = ? AND eventDate = ?", new Object[]{"%"+venueName+"%",cityName,stateName,eventDate});
			if(!localEvents.isEmpty()){
				for (AdmitoneEventLocal localEvent : localEvents){
					if(localEvent.getEventTime().equals(eventTime)){
						admitoneEventLocal = localEvent;
						break;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return admitoneEventLocal;
	}
	
}
