package com.admitone.tmat.dao.hibernate;


import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.CsvUploadRemoveAudit;

public class CsvUploadRemoveAuditDAO extends HibernateDAO<String,CsvUploadRemoveAudit> implements  com.admitone.tmat.dao.CsvUploadRemoveAuditDAO{

	@Override
	public List<CsvUploadRemoveAudit> getAllByVenueAndCategoryGroup(String venuename, String catScheme) {
		return find("FROM CsvUploadRemoveAudit WHERE building=? AND zonesgroup=? order by createdDate",new Object[]{venuename,catScheme});
	}

	public List<CsvUploadRemoveAudit> getAllAuditsbyFilter(String action,Date fromDate,Date toDate) {
		return find("FROM CsvUploadRemoveAudit WHERE action=? AND createdDate>=? and createdDate<=? order by createdDate desc",new Object[]{action,fromDate,toDate});
	}
	
}
