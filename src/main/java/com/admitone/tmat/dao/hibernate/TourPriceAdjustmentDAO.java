package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.admitone.tmat.data.TourPriceAdjustment;
import com.admitone.tmat.data.TourPriceAdjustmentPk;

public class TourPriceAdjustmentDAO extends HibernateDAO<TourPriceAdjustmentPk, TourPriceAdjustment> implements com.admitone.tmat.dao.TourPriceAdjustmentDAO{
	
	public Map<String, TourPriceAdjustment> getPriceAdjustments(int tourId) {
		Collection<TourPriceAdjustment> priceAdjustments = find("FROM TourPriceAdjustment WHERE tourId=?", new Object[]{tourId});
		Map<String, TourPriceAdjustment> priceAdjustmentMap = new HashMap<String, TourPriceAdjustment>();
		if (priceAdjustments != null) {
			for (TourPriceAdjustment priceAdjustment: priceAdjustments) {
				priceAdjustmentMap.put(priceAdjustment.getSiteId(), priceAdjustment);
			}
		}
		return priceAdjustmentMap;
	}
	
	public void deletePriceAdjustments(int tourId) {
		bulkUpdate("DELETE FROM TourPriceAdjustment WHERE tourId=?", new Object[]{tourId});
	}
	
}