package com.admitone.tmat.dao.hibernate;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RestartTransactionAspect implements MethodInterceptor {
	private static Logger log = LoggerFactory.getLogger(RestartTransactionAspect.class);

    public Object invoke(MethodInvocation invocation) throws Throwable {
    	Exception exception = null;
    	// 3 attempts
    	for (int i = 0; i < 3; i++) {
	    	try {
	    		Object retVal = invocation.proceed();
	            return retVal;
	    	} catch(Exception e) {
	    		String message = e.getMessage();
	    		if (message == null || !message.toLowerCase().contains("deadlock")) {
	    			throw e;
	    		}
	    		System.out.println("Deadlock detected... Retrying " + i + "/3");
	    		Thread.sleep(2000);
	    		exception = e;
	    	}
    	}
    	
    	throw exception;
    }
}
