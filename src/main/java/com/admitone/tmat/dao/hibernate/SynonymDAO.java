package com.admitone.tmat.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.Synonym;

public class SynonymDAO extends HibernateDAO<String, Synonym> implements com.admitone.tmat.dao.SynonymDAO {

	
	public List<Synonym> getSynonymByType(String type) {
		return find("from Synonym WHERE type=?",new Object[]{type});
	}
	
	public List<Synonym> getSynonymByName(String name) {
		return find("from Synonym WHERE name=?",new Object[]{name});
	}
	
	public List<Synonym> getSynonymByNameAndType(String name, String type){
		return find("from Synonym WHERE type=? AND name like ?",new Object[]{type,"%"+name+"%"});
	}
	
	public List<Synonym> getSynonymByNameAndTypeNew(String name, String type){
		return find("from Synonym WHERE type=? AND name like ?",new Object[]{type,name});
	}
	
	public void deleteSynonymByNameAndType(String name, String type){
		Session session = getSession();
		
		try{
			Query query = session.createQuery("delete from Synonym WHERE type= :type AND name like :name").setParameter("name", name).setParameter("type", type);
			query.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
	}
	
	public void updateSynonymByNameAndType(Synonym synonym){
		Session session = getSession();
		
		try{
			Query query = session.createQuery("update Synonym set value=:value WHERE type= :type AND name like :name").setParameter("name", synonym.getName()).setParameter("type", synonym.getType()).setParameter("value", synonym.getValue());
			query.executeUpdate();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
	}
}
