package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.AutoCatsCategoryTicket;
import com.admitone.tmat.data.CategoryTicket;

public class AutoCatsCategoryTicketDAO extends HibernateDAO<Integer, AutoCatsCategoryTicket> implements com.admitone.tmat.dao.AutoCatsCategoryTicketDAO {

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<AutoCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<AutoCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<AutoCatsCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(AutoCatsCategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<AutoCatsCategoryTicket> getAllActiveAutoCatsCategoryTickets() {
		return find("FROM AutoCatsCategoryTicket where status ='ACTIVE'");
	}
	
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByAll(Integer eventId,String section, String row, String quantity) {
		String query = "FROM AutoCatsCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(quantity);								
		
			}
		if (row !=null && !row.isEmpty())
		{
			query += " AND (rowRange=? OR lastRow=?)";
			param.add(row);
			param.add(row);
		}
		
		return find(query, param.toArray());
		
	}
	
	
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsById(Integer Id) {
		return find("FROM AutoCatsCategoryTicket where id=?" ,new Object[]{Id});
	}

	@Override
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByTicketId(
			int ticketId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByBaseoneticketId(
			int ticketId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByBasetwoticketId(
			int ticketId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByBasethreeticketId(
			int ticketId) {
		// TODO Auto-generated method stub
		return null;
	}
}

