package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentArtist;
import com.admitone.tmat.enums.ArtistStatus;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.utils.TextUtil;

public class ArtistDAO extends HibernateDAO<Integer, Artist> implements com.admitone.tmat.dao.ArtistDAO {
	
    public Collection<Artist> getAll() {
    	return find("FROM Artist ORDER BY name ASC");
    }

    public Collection<Artist> filterByName(String pattern, EventStatus eventStatus) {
    	return new HashSet<Artist>(find("SELECT a FROM Artist a, Event e WHERE a.artistStatus=? AND a.name LIKE ? AND e.artistId=a.id AND e.eventStatus=? ORDER BY a.name ASC", new Object[]{ArtistStatus.ACTIVE, "%" + TextUtil.removeExtraWhitespaces(pattern) + "%", eventStatus}));
    }

    public Collection<Artist> filterByName(String pattern) {
    	
    	String hql="FROM Artist WHERE artistStatus=? AND name LIKE ? ORDER BY name ASC";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(ArtistStatus.ACTIVE);
		parameters.add("%" + pattern + "%");
		return find(hql, parameters.toArray());
		
    }

	public Artist getArtistByStubhubId(Integer stubhubId) {
    	Collection<Artist> artists = find("FROM Artist WHERE artistStatus=? AND stubhubId=?", new Object[]{ArtistStatus.ACTIVE, stubhubId});
    	if (artists.size() == 0) {
    		return null;
    	}
    	
    	return artists.iterator().next();
	}

	public void deleteEmptyArtist(Artist artist) {
		Integer id = artist.getId();
    	// delete bookmarks
    	DAORegistry.getBookmarkDAO().deleteArtistBookmarks(id);

    	// Delete Events 
		
		DAORegistry.getEventDAO().deleteEventsByArtistId(id);
		
    	// delete artist price adjustments
    	DAORegistry.getArtistPriceAdjustmentDAO().deletePriceAdjustments(id);

    	// mark artist for deletion
    	artist.setArtistStatus(ArtistStatus.DELETED);
    	update(artist);	
	}
	
	public Collection<Artist> getAllActiveArtistsByGrandChildCategoryId(Integer id) {
		return find("FROM Artist WHERE grandChildTourCategoryId = ? and artistStatus = ? ", new Object[]{id,ArtistStatus.ACTIVE});
	}
	
	public Collection<Artist> getAllActiveArtistsByGrandChildCategorys(List<Integer> ids) {
		Query query = getSession().createQuery("FROM Artist where artistStatus = :status  and grandChildTourCategoryId in (:Ids)  ORDER By name").setParameter("status", ArtistStatus.ACTIVE).setParameterList("Ids", ids);
	   
	  @SuppressWarnings("unchecked")
	  List<Artist> list= query.list();
	  return list;
}
	public void deleteById(Integer id) {
    	// delete tours
    	/*for (Tour tour: DAORegistry.getTourDAO().getAllToursByArtist(id)) {
    		DAORegistry.getTourDAO().deleteById(tour.getId());
    	}*/
		
		// Delete Events 
		
		DAORegistry.getEventDAO().deleteEventsByArtistId(id);

    	// delete bookmarks
    	DAORegistry.getBookmarkDAO().deleteArtistBookmarks(id);

    	// delete artist price adjustments
    	DAORegistry.getArtistPriceAdjustmentDAO().deletePriceAdjustments(id);

    	// mark artist for deletion
    	Artist artist = get(id);
    	artist.setArtistStatus(ArtistStatus.DELETED);
    	update(artist);
    }	

	public void bulkDelete(List<Integer> ids) {
		Query query = getSession().createQuery("UPDATE Artist SET artistStatus = :status  where id in (:Ids)").setParameter("status", ArtistStatus.DELETED).setParameterList("Ids", ids);
		query.executeUpdate();
		
	}
	
	@Override
    public void delete(Artist artist) {
    	deleteById(artist.getId());
    }
	
    public void deleteEmptyArtists() {
    	for (Artist artist: (Collection<Artist>)find("FROM Artist WHERE artistStatus=? AND id NOT IN (SELECT artistId FROM Event)", new Object[]{ArtistStatus.ACTIVE})) {

    		if (!artist.getEvents().isEmpty()) {
    			continue;
    		}

    		deleteEmptyArtist(artist);
    	}
    }
	
	public int getArtistCount() {
		List list = find("SELECT count(id) FROM Artist WHERE artistStatus=?", new Object[]{ArtistStatus.ACTIVE});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}

	public Collection<Artist> searchArtists(String name) {
		
		String hql="FROM Artist WHERE name like ?";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
		
	}
	
    public Artist getArtistByName(String name) {
    	// use like so that it is not case sensitive
    	name = name.trim();
    	List<Artist> artists = find("FROM Artist WHERE name LIKE ? AND artistStatus=?", new Object[]{name, ArtistStatus.ACTIVE});
    	if (artists == null || artists.size() == 0) { 
    		return null;
    	}
    	return artists.get(0);
    }

    public Collection<Artist> getAllActiveArtists() {
    	Collection<Artist> list = find("FROM Artist WHERE artistStatus=? ORDER BY name", new Object[]{ArtistStatus.ACTIVE});
		return  list;
	}
    public Collection<Artist> getAllActiveArtistsWithEvents() {
    	Collection<Artist> list = find("FROM Artist WHERE artistStatus=? AND id IN (SELECT DISTINCT artistId FROM Event WHERE artistId is not null AND eventStatus='ACTIVE') ORDER BY name", new Object[]{ArtistStatus.ACTIVE});
		return  list;
	}

	@Override
	public Collection<Artist> searchArtistsByWord(String name) {
		
		String hql="FROM Artist WHERE (name like ? OR name like ?) AND artistStatus = ?";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("% " + name + "%");
		parameters.add("%" + name + " %");
		parameters.add(ArtistStatus.ACTIVE);
		return find(hql, parameters.toArray());
		
	}

	@Override
	public Collection<Artist> getArtistsByIds(List<Integer> ids) {
		Query query = getSession().createQuery("FROM Artist where id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<Artist> list= query.list();
		  return list;
	}
	
	@Override
	public Integer save(Artist artist) {
		super.save(artist);
		updateArtistDependant(artist,true);
		return artist.getId();
	}
	
	@Override
	public void saveAll(Collection<Artist> artists) {
		for(Artist artist:artists){
			updateArtistDependant(artist, true);
		}
		super.saveAll(artists);
		
	}
	
	@Override
	public void update(Artist artist) {
		super.update(artist);
		if(artist.getArtistStatus().equals(ArtistStatus.DELETED)){
			updateArtistDependant(artist,false);
		}else{
			updateArtistDependant(artist,true);
		}
	}
	
	@Override
	public boolean updateAll(Collection<Artist> artists) {
		for(Artist artist:artists){
			if(artist.getArtistStatus().equals(ArtistStatus.DELETED)){
				updateArtistDependant(artist,false);
			}else{
				updateArtistDependant(artist,true);
			}
		}
		return super.updateAll(artists);
	}
	
	@Override
	public void saveOrUpdate(Artist artist) {
		// TODO Auto-generated method stub
		super.saveOrUpdate(artist);
		if(artist.getArtistStatus().equals(ArtistStatus.DELETED)){
			updateArtistDependant(artist,false);
		}else{
			updateArtistDependant(artist,true);
		}
	}
	
	@Override
	public void saveOrUpdateAll(Collection<Artist> artists) {
		for(Artist artist:artists){
			if(artist.getArtistStatus().equals(ArtistStatus.DELETED)){
				updateArtistDependant(artist,false);
			}else{
				updateArtistDependant(artist,true);
			}
		}
		super.saveOrUpdateAll(artists);
	}
	
	
	@Override
	public void deleteAll(Collection<Artist> artists) {
		for(Artist artist:artists){
			updateArtistDependant(artist, false);
		}
		super.deleteAll(artists);
		
	}
	
	public void updateArtistDependant(Artist artist,boolean isAdd){
		if(artist.getGrandChildTourCategory().getId()!=51){
			Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
			if(property!=null){
				String dependents = property.getValue();
				if(dependents!= null && !dependents.isEmpty()){
					List<TMATDependentArtist> list = DAORegistry.getTmatDependentArtistDAO().getTmatDependentsByArtistId(artist.getId());
					List<TMATDependentArtist> tmatDependents = new ArrayList<TMATDependentArtist>();
					for(TMATDependentArtist dependent:list){
						if(dependents.contains(dependent.getDependent())){
								dependents = dependents.replace(dependent.getDependent(),"");
								
								dependent.setAdd(isAdd);
								tmatDependents.add(dependent);
						}
					}
					String depenedentsList[] = dependents.split(",");
					
					for(String dependent:depenedentsList){
						if(dependent.trim().equals("")){
							continue;
						}
						
						TMATDependentArtist tmatDependent = new TMATDependentArtist();
						tmatDependent.setDependent(dependent);
						tmatDependent.setArtistId(artist.getId());
						tmatDependent.setAdd(true);
						tmatDependents.add(tmatDependent);
						
					}
					if(!tmatDependents.isEmpty()){
						DAORegistry.getTmatDependentArtistDAO().saveOrUpdateAll(tmatDependents);
					}
				}
			}
		}
	}
	
	
public Collection<Artist> getAllArtistsBySqlQuery() {
		
		Session session = getSession();
		SQLQuery query = session.createSQLQuery("select id,name from artist order by name asc");
		
		List<Object[]> list = query.list();
		List<Artist> artists= new ArrayList<Artist>();
		Artist artist =null;
		for(Object[] obj:list){
			artist = new Artist();
			artist.setId((Integer)obj[0]);
			artist.setName((String)obj[1]);
			artists.add(artist);
		}
		releaseSession(session);
		return artists;
	
	}

	public Collection<Artist> getAllArtistsWithGrandChildBySqlQuery() {
		return find("FROM Artist WHERE grandChildTourCategoryId is not null AND artistStatus = ?",new Object[]{ArtistStatus.ACTIVE});
	
//	Session session = getSession();
//	SQLQuery query = session.createSQLQuery("select id,name,artist_status,grand_child_category_id from artist where grand_child_category_id is not null order by name asc");
//	
//	List<Object[]> list = query.list();
//	List<Artist> artists= new ArrayList<Artist>();
//	Artist artist =null;
//	for(Object[] obj:list){
//		artist = new Artist();
//		artist.setId((Integer)obj[0]);
//		artist.setName((String)obj[1]);
//		artists.add(artist);
//	}
//	releaseSession(session);
//	return artists;

}

}


