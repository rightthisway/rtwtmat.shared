package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.EIBoxEvent;
import com.admitone.tmat.utils.TextUtil;

public class EIBoxEventDAO extends HibernateDAO<Integer, EIBoxEvent> implements com.admitone.tmat.dao.EIBoxEventDAO {
	public Collection<EIBoxEvent> getEIBoxEvents(Date fromDate, Date toDate) {
		return find("FROM EIBoxEvent WHERE eventDate > ? AND eventDate < ?", new Object[]{fromDate, toDate});
	}

	
	public Collection<EIBoxEvent> getEIBoxEventsByNameFilter(String eventFilter) {
		// TODO Auto-generated method stub
		return find("FROM EIBoxEvent WHERE eventName LIKE ?",new Object[]{"%" + TextUtil.removeExtraWhitespaces(eventFilter) + "%"});
	}
}
