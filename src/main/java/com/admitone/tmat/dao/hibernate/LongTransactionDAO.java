package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.LongTransaction;

public class LongTransactionDAO extends HibernateDAO<Integer, LongTransaction> implements com.admitone.tmat.dao.LongTransactionDAO {

	public Collection<LongTransaction> getLongTransactionsByEventId(int eventId) {
		return find("SELECT lt FROM LongTransaction lt, Event e WHERE lt.admitoneId=e.admitoneId AND e.id=?", new Object[]{eventId});
	}
	
}
