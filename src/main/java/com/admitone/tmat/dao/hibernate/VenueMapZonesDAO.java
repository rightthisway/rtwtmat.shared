package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.VenueMapZones;

public class VenueMapZonesDAO extends HibernateDAO<Integer, VenueMapZones> implements
		com.admitone.tmat.dao.VenueMapZonesDAO {

	public List<VenueMapZones> getAllVenueMapZonesByVenueCategoryId(Integer venueCategoryId) throws Exception {
		return find("FROM VenueMapZones WHERE venueCategoryId=? order by zone",new Object[]{venueCategoryId});
	}
	
}
