package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.SeatWaveEvent;

public class SeatWaveEventDAO extends HibernateDAO<Integer, SeatWaveEvent> implements com.admitone.tmat.dao.SeatWaveEventDAO {
	public Collection<SeatWaveEvent> getSeatWaveEvents(int seasonId) {
		return getHibernateTemplate().find("FROM SeatWaveEvent WHERE seasonId=?", seasonId);
	}
	
	public Collection<SeatWaveEvent> getSeatWaveEvents(Date fromDate, Date toDate) {
		return getHibernateTemplate().find("FROM SeatWaveEvent WHERE eventDate >= ? AND eventDate <= ?", new Object[] {fromDate, toDate});		
	}

	public void deleteEventsNotUpdatedAfter(Date updatedDate) {
		getHibernateTemplate().bulkUpdate("DELETE FROM SeatWaveEvent WHERE updatedDate < ?", new Object[] {updatedDate});				
	}

}
