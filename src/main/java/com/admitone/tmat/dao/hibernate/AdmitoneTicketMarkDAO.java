package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.AdmitoneTicketMark;

public class AdmitoneTicketMarkDAO extends HibernateDAO<Integer, AdmitoneTicketMark> implements com.admitone.tmat.dao.AdmitoneTicketMarkDAO {
	public Collection<AdmitoneTicketMark> getAdmitOneTicketMarksByEvent(Integer eventId) {
		return find("FROM AdmitoneTicketMark WHERE eventId=?", new Object[]{eventId});
	}
}