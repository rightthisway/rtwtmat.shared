package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ChildTourCategory;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentChildCategory;

public class ChildTourCategoryDAO extends HibernateDAO<Integer, ChildTourCategory> implements com.admitone.tmat.dao.ChildTourCategoryDAO {
	public List<ChildTourCategory> getChildTourCategoryByTourCategory(Integer id){
		return find("FROM ChildTourCategory ctc where ctc.tourCategory.id= ? ORDER BY name ",new Object[]{id});
	}

	public ChildTourCategory getChildTourCategoryByNameAndTourCategoryName(String name, String tourCategoryName) {
		return findSingle("FROM ChildTourCategory ctc WHERE name = ? AND ctc.tourCategory.name=? ",new Object[]{name,tourCategoryName});
	}

	@SuppressWarnings("unchecked")
	public List<ChildTourCategory> getChildTourCategoryOrderByName() {
		return find("FROM ChildTourCategory ORDER BY name ");
	}

	public Collection<ChildTourCategory> getChildTourCategoryByTourCategories(List<Integer> ids) {
		Query query = getSession().createQuery("FROM ChildTourCategory where tourCategory.id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		@SuppressWarnings("unchecked")
		List<ChildTourCategory> list= query.list();
		return list;
	}
	
	/**
	 * return grand child by name where name should match the input
	 */
	public Collection<ChildTourCategory> getChildTourCategoriesByName(String name) {
		String hql="FROM ChildTourCategory WHERE name like ? and name <> 'NONE' and name <> 'OTHER' ORDER BY name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}
	
	@Override
	public Integer save(ChildTourCategory entity) {
		super.save(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentChildCategory dependentChildCategory = new TMATDependentChildCategory();
				dependentChildCategory.setChildCategoryId(entity.getId());
				dependentChildCategory.setDependent(zone);
				dependentChildCategory.setAdd(true);
				DAORegistry.getTmatDependentChildCategoryDAO().save(dependentChildCategory);
			}
		}
		
		return  entity.getId();
	}
	
	@Override
	public void update(ChildTourCategory entity) {
		super.update(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentChildCategory> list = DAORegistry.getTmatDependentChildCategoryDAO().getTmatDependentsByChildCategoryId(entity.getId());
		Map<String, TMATDependentChildCategory> map = new HashMap<String, TMATDependentChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentChildCategory dependentChildCategory = map.get(zone);
				if(dependentChildCategory==null){
					dependentChildCategory = new TMATDependentChildCategory();
				}
				dependentChildCategory.setChildCategoryId(entity.getId());
				dependentChildCategory.setDependent(zone);
				dependentChildCategory.setAdd(true);
				DAORegistry.getTmatDependentChildCategoryDAO().saveOrUpdate(dependentChildCategory);
			}
		}
	}
	
	@Override
	public void delete(ChildTourCategory entity) {
		super.delete(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentChildCategory> list = DAORegistry.getTmatDependentChildCategoryDAO().getTmatDependentsByChildCategoryId(entity.getId());
		Map<String, TMATDependentChildCategory> map = new HashMap<String, TMATDependentChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentChildCategory dependentChildCategory = map.get(zone);
				if(dependentChildCategory==null){
					dependentChildCategory = new TMATDependentChildCategory();
				}
				dependentChildCategory.setChildCategoryId(entity.getId());
				dependentChildCategory.setDependent(zone);
				dependentChildCategory.setAdd(false);
				DAORegistry.getTmatDependentChildCategoryDAO().saveOrUpdate(dependentChildCategory);
			}
		}
	}
}
