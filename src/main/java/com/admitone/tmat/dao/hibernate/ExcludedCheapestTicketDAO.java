package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ExcludedCheapestTicket;

public class ExcludedCheapestTicketDAO extends HibernateDAO<Integer, ExcludedCheapestTicket> implements com.admitone.tmat.dao.ExcludedCheapestTicketDAO {

	
	@SuppressWarnings("unchecked")
	public Collection<ExcludedCheapestTicket> getAllExcludedTickets(String categorySelected, String sectionSelected, String rowSelected, String seatSelected, Collection<String> siteList) {
		
		List<ExcludedCheapestTicket> result = null;
				   
	    StringBuffer buffer = new StringBuffer ("FROM ExcludedCheapestTicket e where e.ticket.siteId in (:Ids)");
	    if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	buffer.append(" and e.category=:selectedCategory");
	    }
	    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.section=:ticketSection");
	    }
	    if(rowSelected!=null && !rowSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.row=:ticketRow");
	    }
	    if(seatSelected!=null && !seatSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.seat=:ticketSeat");
	    }
	   
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());	    
	     query.setParameterList("Ids", siteList);
	     if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	 query.setParameter("selectedCategory", categorySelected);
		    }
	     if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	 query.setParameter("ticketSection", sectionSelected);		    	
		    }
		    if(rowSelected!=null && !rowSelected.equals("ALL")){
		    	query.setParameter("ticketRow", rowSelected);
		    }
		    if(seatSelected!=null && !seatSelected.equals("ALL")){
		    	query.setParameter("ticketSeat", seatSelected);
		    }
		    
		       
		    result = query.list();
		    
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	    		
	}

	@SuppressWarnings("unchecked")
	public Collection<ExcludedCheapestTicket> getByTour(Integer tourId) {
		return find("FROM ExcludedCheapestTicket WHERE event.artistId=? AND event.eventStatus='ACTIVE'", new Object[]{tourId});
	}


	public Collection<ExcludedCheapestTicket> getAllExcludedTicketsByTourID(
			Integer tourId,String categorySelected, String sectionSelected, String rowSelected, String seatSelected, Collection<String> siteList) {
		List<ExcludedCheapestTicket> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("FROM ExcludedCheapestTicket e WHERE e.event.artistId=:Id AND e.event.eventStatus='ACTIVE' AND e.ticket.siteId in(:siteIds)");
	    if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	buffer.append(" and e.category=:selectedCategory");
	    }
	    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.section=:ticketSection");
	    }
	    if(rowSelected!=null && !rowSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.row=:ticketRow");
	    }
	    if(seatSelected!=null && !seatSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.seat=:ticketSeat");
	    }
	   
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());
	     query.setParameterList("siteIds", siteList);
	     query.setParameter("Id", tourId);
	     if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	 query.setParameter("selectedCategory", categorySelected);
		    }
	     if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	 query.setParameter("ticketSection", sectionSelected);		    	
		    }
		    if(rowSelected!=null && !rowSelected.equals("ALL")){
		    	query.setParameter("ticketRow", rowSelected);
		    }
		    if(seatSelected!=null && !seatSelected.equals("ALL")){
		    	query.setParameter("ticketSeat", seatSelected);
		    }
		    
		    result = query.list();
	   
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public Collection<ExcludedCheapestTicket> getAllExcludedTicketsByEventID(
			Integer eventId,String categorySelected, String sectionSelected, String rowSelected, String seatSelected, Collection<String> siteList) {
		List<ExcludedCheapestTicket> result = null;		
		   
	    StringBuffer buffer = new StringBuffer ("FROM ExcludedCheapestTicket e WHERE e.event.id=:Id AND e.event.eventStatus='ACTIVE' AND e.ticket.siteId in(:siteIds)");
	    Query query = null;
	    Session session = null;
	    if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	buffer.append(" and e.category=:selectedCategory");
	    }
	    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.section=:ticketSection");
	    }
	    if(rowSelected!=null && !rowSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.row=:ticketRow");
	    }
	    if(seatSelected!=null && !seatSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.seat=:ticketSeat");
	    }
	    
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());
	     query.setParameterList("siteIds", siteList);
	     query.setParameter("Id", eventId);
	     if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	 query.setParameter("selectedCategory", categorySelected);
		    }
	     if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	 query.setParameter("ticketSection", sectionSelected);		    	
		    }
		    if(rowSelected!=null && !rowSelected.equals("ALL")){
		    	query.setParameter("ticketRow", rowSelected);
		    }
		    if(seatSelected!=null && !seatSelected.equals("ALL")){
		    	query.setParameter("ticketSeat", seatSelected);
		    }
		    
		    result = query.list();		   
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public Collection<Event> getDistinctEvents(Integer artistId) {
		List<Event> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select distinct(e.event) FROM ExcludedCheapestTicket e WHERE e.event.artistId=:Id AND e.event.eventStatus='ACTIVE' order by e.event.localDate");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());	    
	     query.setParameter("Id", artistId);
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;		
	}

	
	public Collection<ExcludedCheapestTicket> getAllTickets() {
		List<ExcludedCheapestTicket> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("FROM ExcludedCheapestTicket");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());    
	     
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;	
	}

	
	public List<Event> getDistinctEvent() {
		List<Event> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select distinct(event) FROM ExcludedCheapestTicket");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());	    
	    
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public List<String> getDistinctSiteIds() {
		List<String> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select distinct(ticket.siteId) FROM ExcludedCheapestTicket");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());	    
	    
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public List<String> getDistinctSections(String artistIdStr,String eventIdstr,  String categorySelected) {
		List<String> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select distinct(e.ticket.section) FROM ExcludedCheapestTicket e where 1=1");
	    if(eventIdstr!=null && !eventIdstr.equals("ALL")){
	    	buffer.append(" and e.event.id=:eventId");
	    }
	    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
	    	buffer.append(" and e.event.artistId=:artistId");
	    }
	    if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	buffer.append(" and e.category=:selectedCategory");
	    }
	    buffer.append(" order by e.ticket.section");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());
	     if(eventIdstr!=null && !eventIdstr.equals("ALL")){		    	
		    	query.setParameter("eventId", Integer.parseInt(eventIdstr));
		    }
		    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
		    	query.setParameter("artistId", Integer.parseInt(artistIdStr));
		    }
		    if(categorySelected!=null && !categorySelected.equals("ALL")){
		    	query.setParameter("selectedCategory", categorySelected);
		    }
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public List<String> getDistinctRows(String artistIdStr,String eventIdstr,String categorySelected,String sectionSelected) {
		List<String> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select distinct(e.ticket.row) FROM ExcludedCheapestTicket e where 1=1");
	    if(eventIdstr!=null && !eventIdstr.equals("ALL")){
	    	buffer.append(" and e.event.id=:eventId");
	    }
	    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
	    	buffer.append(" and e.event.artistId=:artistId");
	    }
	    if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	buffer.append(" and e.category=:selectedCategory");
	    }
	    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.section=:ticketSection");
	    }	
	    buffer.append(" order by e.ticket.row");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());	
	     if(eventIdstr!=null && !eventIdstr.equals("ALL")){		    	
		    	query.setParameter("eventId", Integer.parseInt(eventIdstr));
		    }
		    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
		    	query.setParameter("artistId", Integer.parseInt(artistIdStr));
		    }
		    if(categorySelected!=null && !categorySelected.equals("ALL")){
		    	query.setParameter("selectedCategory", categorySelected);
		    }
		    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	 query.setParameter("ticketSection", sectionSelected);		    	
		    }
		    
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public List<String> getDistinctSeats(String artistIdStr,String eventIdstr,String categorySelected,String sectionSelected, String rowSelected) {
		List<String> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select distinct(e.ticket.seat) FROM ExcludedCheapestTicket e where 1=1");
	    if(eventIdstr!=null && !eventIdstr.equals("ALL")){
	    	buffer.append(" and e.event.id=:eventId");
	    }
	    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
	    	buffer.append(" and e.event.artistId=:artistId");
	    }
	    if(categorySelected!=null && !categorySelected.equals("ALL")){
	    	buffer.append(" and e.category=:selectedCategory");
	    }
	    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.section=:ticketSection");
	    }
	    if(rowSelected!=null && !rowSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.row=:ticketRow");
	    }
	    buffer.append(" order by e.ticket.seat");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());
	     if(eventIdstr!=null && !eventIdstr.equals("ALL")){		    	
		    	query.setParameter("eventId", Integer.parseInt(eventIdstr));
		    }
		    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
		    	query.setParameter("artistId", Integer.parseInt(artistIdStr));
		    }
		    if(categorySelected!=null && !categorySelected.equals("ALL")){
		    	query.setParameter("selectedCategory", categorySelected);
		    }
		    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	 query.setParameter("ticketSection", sectionSelected);		    	
		    }
		    if(rowSelected!=null && !rowSelected.equals("ALL")){
		    	query.setParameter("ticketRow", rowSelected);
		    }
		    
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public List<String> getDistinctPrices(String artistIdStr,String eventIdstr,String sectionSelected, String rowSelected, String seatSelected) {
		List<String> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select distinct(e.ticket.currentPrice) FROM ExcludedCheapestTicket e where 1=1");
	    if(eventIdstr!=null && !eventIdstr.equals("ALL")){
	    	buffer.append(" and e.event.id=:eventId");
	    }
	    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
	    	buffer.append(" and e.event.artistId=:artistID");
	    }
	    if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.section=:ticketSection");
	    }
	    if(rowSelected!=null && !rowSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.row=:ticketRow");
	    }
	    if(seatSelected!=null && !seatSelected.equals("ALL")){
	    	buffer.append(" and e.ticket.seat=:ticketSeat");
	    }
	    
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());	
	     if(eventIdstr!=null && !eventIdstr.equals("ALL")){		    	
		    	query.setParameter("eventId", Integer.parseInt(eventIdstr));
		    }
		    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
		    	query.setParameter("artistID", Integer.parseInt(artistIdStr));
		    }
	     if(sectionSelected!=null && !sectionSelected.equals("ALL")){
	    	 query.setParameter("ticketSection", sectionSelected);		    	
		    }
		    if(rowSelected!=null && !rowSelected.equals("ALL")){
		    	query.setParameter("ticketRow", rowSelected);
		    }
		    if(seatSelected!=null && !seatSelected.equals("ALL")){
		    	query.setParameter("ticketSeat", seatSelected);
		    }
		    
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}

	
	public List<String> getCategorys(String artistIdStr, String eventIdStr) {
		List<String> result = null;
		StringBuffer buffer = new StringBuffer ("select distinct(e.category) FROM ExcludedCheapestTicket e where 1=1");
	    if(eventIdStr!=null && !eventIdStr.equals("ALL")){
	    	buffer.append(" and e.event.id=:eventId");
	    }
	    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
	    	buffer.append(" and e.event.artistId=:artistID");
	    }	   
	    buffer.append(" order by e.category");
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());	
	     if(eventIdStr!=null && !eventIdStr.equals("ALL")){		    	
		    	query.setParameter("eventId", Integer.parseInt(eventIdStr));
		    }
		    if(artistIdStr!=null && !artistIdStr.equals("ALL")){
		    	query.setParameter("artistID", Integer.parseInt(artistIdStr));
		    }
	    
	     result = query.list();
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}
	

	

	
}
