package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.TickPickExchangeEvent;

public class TickPickExchangeEventDAO extends HibernateDAO<Integer,TickPickExchangeEvent> implements com.admitone.tmat.dao.TickPickExchangeEventDAO{

	public Collection<TickPickExchangeEvent> getAllTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<TickPickExchangeEvent> result = new ArrayList<TickPickExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<TickPickExchangeEvent> temp = find("FROM TickPickExchangeEvent WHERE event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public Collection<TickPickExchangeEvent> getAllActiveTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<TickPickExchangeEvent> result = new ArrayList<TickPickExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<TickPickExchangeEvent> temp = find("FROM TickPickExchangeEvent WHERE status='ACTIVE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection<TickPickExchangeEvent> getAllDeletedTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<TickPickExchangeEvent> result = new ArrayList<TickPickExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<TickPickExchangeEvent> temp = find("FROM TickPickExchangeEvent WHERE status='DELETE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAllTickPickExchangeEventsByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("DELETE FROM TickPickExchangeEvent WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public void updateAllTickPickExchangeEventAsDeleteByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("UPDATE TickPickExchangeEvent SET status='DELETE' WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public List<TickPickExchangeEvent> getAllActiveTickPickExchangeEvent() {
		try{
			return find("FROM TickPickExchangeEvent WHERE event.id IN(SELECT DISTINCT eventId from TGCatsCategoryTicket where status ='ACTIVE')", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveBulkEvents(List<TickPickExchangeEvent> tnEvents) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (TickPickExchangeEvent event : tnEvents) {
			    session.save(event);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
	
}
