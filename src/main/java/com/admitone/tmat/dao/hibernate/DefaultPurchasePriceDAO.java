package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.DefaultPurchasePrice;

public class DefaultPurchasePriceDAO extends HibernateDAO<Integer, DefaultPurchasePrice> implements com.admitone.tmat.dao.DefaultPurchasePriceDAO {

	public DefaultPurchasePrice getAllDefaultTourPriceByTourId(Integer id) {
		return findSingle("FROM DefaultPurchasePrice WHERE id = ? ", new Object[]{id});
	}

	public List<DefaultPurchasePrice> getAllDefaultTourPrice() {
		return find("FROM DefaultPurchasePrice");
	}

	public DefaultPurchasePrice getDefaultTourPriceByExchangeAndTicketType(String exchange, String ticketType) {
		return findSingle("FROM DefaultPurchasePrice WHERE exchange= ? AND ticketType=?", new Object[]{exchange,ticketType});
	}


}
