package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.LastFiveRowMiniCategoryTicket;

public class LastFiveRowMiniCategoryTicketDAO extends HibernateDAO<Integer, LastFiveRowMiniCategoryTicket> implements com.admitone.tmat.dao.LastFiveRowMiniCategoryTicketDAO {

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<LastFiveRowMiniCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<LastFiveRowMiniCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<LastFiveRowMiniCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<LastFiveRowMiniCategoryTicket> getAllActiveLastFiveRowMiniCategoryTickets() {
		return find("FROM LastFiveRowMiniCategoryTicket where status ='ACTIVE'");
	}
	
	public List<LastFiveRowMiniCategoryTicket> getAllLastFiveRowMiniCategoryTicketsByAll(Integer eventId, String section, String row, String quantity) {
		String query = "FROM LastFiveRowMiniCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND rowRange=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(quantity);								
		
			}
		
		
		return find(query, param.toArray());
		
	}
	public List<LastFiveRowMiniCategoryTicket> getAllLastFiveRowMiniCategoryTicketsById(Integer Id) {
		return find("FROM LastFiveRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM LastFiveRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	
}
