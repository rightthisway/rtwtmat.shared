package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.TNInstantEvent;
//import com.admitone.tmat.data.Tour;

public class TNInstantEventDAO extends HibernateDAO<Integer, TNInstantEvent> implements com.admitone.tmat.dao.TNInstantEventDAO{

		
		public Collection<Event> getAllInstantEventsByTour(Integer tourId) {
//			return find("SELECT e FROM Event e, TNInstantEvent i WHERE i.tour.id=? AND e.eventStatus='ACTIVE' ORDER BY e.localDate, e.localTime ASC", new Object[]{tourId});		
			return null;
		}		
		
		public Collection<TNInstantEvent> getAllTNInstantEventsByTour(Integer tourId) {
			return find("FROM TNInstantEvent  WHERE tour.id=? AND event.eventStatus='ACTIVE' ORDER BY event.localDate, event.localTime ASC", new Object[]{tourId});
		}

		public int getInstantEventCountByTour(Integer tourId) {
			List list = find("SELECT count(i.id) FROM TNInstantEvent i, Event e WHERE i.id=e.id and e.tourId=? AND e.eventStatus = 'ACTIVE'", new Object[]{tourId});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();		
		}
		
		
		public boolean deleteEventsByIds(List<Integer> eventIds) {
			try{
				String events="";
				for(Integer event:eventIds){
					events+=event+",";
				}
				if(!"".equals(events)){
					bulkUpdate("DELETE from TNInstantEvent WHERE event.id in ("+ events.substring(0,events.length()-1)+ ")");
				}
			}catch (Exception e) {
				return false;
			}
			return true;
		}

		
		public TNInstantEvent getTNInstantEventByEventId(Integer eventId) {
			return findSingle("FROM TNInstantEvent WHERE event.id=? ", new Object[]{eventId});
		}

		
		/*public Collection<Tour> getAllTours() {
			return find("SELECT tour From TNInstantEvent");
		}*/
}
