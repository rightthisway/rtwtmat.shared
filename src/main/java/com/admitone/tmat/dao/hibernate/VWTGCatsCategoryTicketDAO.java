package com.admitone.tmat.dao.hibernate;


import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.VWTGCatsCategoryTicket;

public class VWTGCatsCategoryTicketDAO extends HibernateDAO<Integer, VWTGCatsCategoryTicket> implements com.admitone.tmat.dao.VWTGCatsCategoryTicketDAO {

	public VWTGCatsCategoryTicket get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<VWTGCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public Collection<VWTGCatsCategoryTicket> getAll() {
		return find("FROM VWTGCatsCategoryTicket");
	}

	public void saveOrUpdateAll(Collection<VWTGCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<VWTGCatsCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<VWTGCatsCategoryTicket> getAllActiveTnTgCatsCategoryTickets() {
		return find("FROM VWTGCatsCategoryTicket where tn_category_ticket_group_id is not null and tn_category_ticket_group_id !=0 and status='ACTIVE'");
	}
}
