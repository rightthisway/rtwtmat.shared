package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import com.admitone.tmat.data.TnFloorCapEventVenueAudit;

public class TnFloorCapEventVenueAuditDAO extends HibernateDAO<Integer, TnFloorCapEventVenueAudit> 
	implements com.admitone.tmat.dao.TnFloorCapEventVenueAuditDAO {

	public List<TnFloorCapEventVenueAudit> getTmFloorCapVenueAuditsByVenueName(String venueName) {
		
		String hql="FROM TnFloorCapEventVenueAudit WHERE venue=? order by createdDate";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add(venueName);
		return find(hql, parameters.toArray());
	}
}
