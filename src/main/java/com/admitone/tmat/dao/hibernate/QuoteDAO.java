package com.admitone.tmat.dao.hibernate;

import java.sql.Date;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Quote;

import edu.emory.mathcs.backport.java.util.TreeSet;

public class QuoteDAO extends HibernateDAO<Integer, Quote> implements com.admitone.tmat.dao.QuoteDAO {
	public Collection<String> getQuoteUsernames() {
		return find("SELECT DISTINCT(username) FROM Quote");
	}
	public Collection<String> getQuoteUsernamesByBrokerId(Integer brokerId) {
		return find("SELECT DISTINCT(q.username) FROM Quote q,User u WHERE u.username=q.username and u.brokerId=?",new Object[]{brokerId});
	}

	public List<Quote> getAllQuotesByBrokerId(Integer brokerId) {
		return find("SELECT q FROM Quote q,User u WHERE u.username=q.username and u.brokerId=?",new Object[]{brokerId});
	}
	public Quote getMostRecentQuote(String username) {
		return findSingle("FROM Quote WHERE username=? ORDER BY creationDate DESC", new Object[]{username});
	}
	
	public Quote getOpenQuote(Integer customerId, String username, java.sql.Date date) {
		return findSingle("FROM Quote WHERE username=? AND customer.id=? AND creationDate=? AND sentDate=null", new Object[]{username, customerId, date});
	}

	public Collection<Quote> getQuotesByCustomer(Integer customerId) {
		return find("FROM Quote WHERE customer.id=? ORDER BY quoteDate DESC", new Object[]{customerId});
	}
	public Collection<Quote> getQuotesByCustomer(Integer customerId,boolean isZoneQuote) {
		if(isZoneQuote){
			return find("FROM Quote WHERE quoteType='zone' AND customer.id=? ORDER BY quoteDate DESC", new Object[]{customerId});
		}else{
			return find("FROM Quote WHERE (quoteType <> 'zone' OR quoteType is null ) AND customer.id=? ORDER BY quoteDate DESC", new Object[]{customerId});
		}
	}

	public Collection<Quote> getQuotesByUserCustomerAndDate(String username, Integer customerId, java.sql.Date date) {
		Set<Quote> sortedQuotes = new TreeSet(new Comparator<Quote>() {

			
			public int compare(Quote q1, Quote q2) {
				if (q1.getSentDate() == null) {
					if (q2.getSentDate() == null) {
						return 0;
					}
					return -1;
				}

				if (q2.getSentDate() == null) {
					return 1;
				}

				return -q1.getSentDate().compareTo(q2.getSentDate());
			}
		});
		sortedQuotes.addAll(find("FROM Quote WHERE username=? AND customer.id=? AND quoteDate=? ORDER BY sentDate DESC", new Object[]{username, customerId, date}));
		return sortedQuotes;
	}

	public Collection<Quote> getQuotesByCustomerAndDate(Integer customerId, java.sql.Date date) {
		Set<Quote> sortedQuotes = new TreeSet(new Comparator<Quote>() {

			
			public int compare(Quote q1, Quote q2) {
				if (q1.getSentDate() == null) {
					if (q2.getSentDate() == null) {
						return 0;
					}
					return -1;
				}

				if (q2.getSentDate() == null) {
					return 1;
				}

				return -q1.getSentDate().compareTo(q2.getSentDate());
			}
		});
		sortedQuotes.addAll(find("FROM Quote WHERE customer.id=? AND quoteDate=? ORDER BY sentDate DESC", new Object[]{customerId, date}));
		return find("FROM Quote WHERE customer.id=? AND quoteDate=? ORDER BY sentDate DESC", new Object[]{customerId, date});
	}

	public Collection<Quote> getQuotesByUserAndCustomer(String username, Integer customerId) {
		return find("FROM Quote WHERE username=? AND customer.id=? ORDER BY quoteDate DESC", new Object[]{username, customerId});
	}

	public Collection<java.sql.Date> getQuotesDatesByCustomer(Integer customerId) {
		return (Collection<java.sql.Date>) find("SELECT DISTINCT(quoteDate) FROM Quote WHERE customer.id=? AND quoteDate<>null ORDER BY quoteDate DESC", new Object[]{customerId});
	}
	
	public void delete(Quote quote) {
		DAORegistry.getQuoteManualTicketDAO().deleteQuoteManualTicketByQuoteId(quote.getId());
		DAORegistry.getTicketQuoteDAO().deleteByQuoteId(quote.getId());
		super.delete(quote);
	}
	
	public void deleteByCustomer(Integer customerId) {
		for (Quote quote: getQuotesByCustomer(customerId)) {
			delete(quote);
		}
	}

	
	public List<Quote> getFilteredQuotes(String creator, String custId,
			Date fromQuoteDate, Date toQuoteDate, String email, String status) {//,boolean isZoneQuote
		List<Quote> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("FROM Quote WHERE 1=1");
	    if(creator!=null && !"ALL".equals(creator)){
	    	buffer.append(" and username=:selectedCreater");
	    }
	    /*if(isZoneQuote){
	    	buffer.append(" and quoteType = 'zone' ");
	    }else{
	    	buffer.append(" and ( quoteType <> 'zone' OR quoteType is null ) ");
	    }*/
	    if(custId!=null && !"ALL".equals(custId)){
	    	buffer.append(" and customer.id =:selectedCust");
	    }
	    if(fromQuoteDate!=null){
	    	buffer.append(" and creationDate >=:selectedFromDate");
	    }
	    if(toQuoteDate!=null){	    	
	    	buffer.append(" and creationDate <=:selectedToDate");
	    }
	    if(email != null && !"ALL".equals(email) ){
	    	buffer.append(" and sentTo like '%"+email+"%'");
	    }
	    if(status !=null && !"ALL".equals(status)){
	    	if(status.equals("-1")){
	    	buffer.append(" and sentDate <=:currDate");
	    	}else{
	    		buffer.append(" and sentDate is null");	
	    	}
	    }
	   
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());    
	     
	     if(creator!=null && !"ALL".equals(creator)){
	    	 query.setParameter("selectedCreater", creator);
		    }
	     if(custId!=null && !"ALL".equals(custId)){
	    	 query.setParameter("selectedCust", Integer.valueOf(custId));		    	
		    }
	     if(fromQuoteDate!=null){
		    	query.setParameter("selectedFromDate", fromQuoteDate);
		    }
		 if(toQuoteDate!=null){
		    	query.setParameter("selectedToDate", toQuoteDate);
		    }
		/* if(custId!=null && "ALL".equals(custId) && email != null && !"ALL".equals(email) ){		    	
		    	query.setParameter("selectedEmail", email);
		    }*/
		 if(status !=null && !"ALL".equals(status)){
		 if(status!=null && "-1".equals(status)){
		    	query.setParameter("currDate", new Date(new java.util.Date().getTime()));
		    }
		 }
		    
		       
		    result = query.list();
		    
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}
	
	public List<Quote> getFilteredQuotes(String creator, String custId,
			Date fromQuoteDate, Date toQuoteDate, String email, String status,Integer brokerId) {//,boolean isZoneQuote
		List<Quote> result = null;
		   
	    StringBuffer buffer = new StringBuffer ("select q FROM Quote q,User u WHERE q.username=u.username and u.brokerId=:brokerIdParam");
	    if(creator!=null && !"ALL".equals(creator)){
	    	buffer.append(" and q.username=:selectedCreater");
	    }
	    /*if(isZoneQuote){
	    	buffer.append(" and quoteType = 'zone' ");
	    }else{
	    	buffer.append(" and ( quoteType <> 'zone' OR quoteType is null ) ");
	    }*/
	    if(custId!=null && !"ALL".equals(custId)){
	    	buffer.append(" and q.customer.id =:selectedCust");
	    }
	    if(fromQuoteDate!=null){
	    	buffer.append(" and q.creationDate >=:selectedFromDate");
	    }
	    if(toQuoteDate!=null){	    	
	    	buffer.append(" and q.creationDate <=:selectedToDate");
	    }
	    if(email != null && !"ALL".equals(email) ){
	    	buffer.append(" and q.sentTo like '%"+email+"%'");
	    }
	    if(status !=null && !"ALL".equals(status)){
	    	if(status.equals("-1")){
	    	buffer.append(" and q.sentDate <=:currDate");
	    	}else{
	    		buffer.append(" and q.sentDate is null");	
	    	}
	    }
	   
	    Query query = null;
	    Session session = null;
	  try {
	     SessionFactory sessionFactory = getHibernateTemplate().getSessionFactory();
	     session  = sessionFactory.openSession();    
	     query = session.createQuery(buffer.toString());    
	     
	     query.setParameter("brokerIdParam", brokerId);
	     
	     if(creator!=null && !"ALL".equals(creator)){
	    	 query.setParameter("selectedCreater", creator);
		    }
	     if(custId!=null && !"ALL".equals(custId)){
	    	 query.setParameter("selectedCust", Integer.valueOf(custId));		    	
		    }
	     if(fromQuoteDate!=null){
		    	query.setParameter("selectedFromDate", fromQuoteDate);
		    }
		 if(toQuoteDate!=null){
		    	query.setParameter("selectedToDate", toQuoteDate);
		    }
		/* if(custId!=null && "ALL".equals(custId) && email != null && !"ALL".equals(email) ){		    	
		    	query.setParameter("selectedEmail", email);
		    }*/
		 if(status !=null && !"ALL".equals(status)){
		 if(status!=null && "-1".equals(status)){
		    	query.setParameter("currDate", new Date(new java.util.Date().getTime()));
		    }
		 }
		    
		       
		    result = query.list();
		    
	    } catch (Exception e) {
	     
	     e.printStackTrace();
	    } finally  {
	     session.close ();
	    }
	    
	    return result;
	}
	
	public List<Quote> getQuotes() {
		
			return find("FROM Quote  ORDER BY quoteDate DESC", new Object[]{});
		
	}
	
	public List<Quote> getQuotes(boolean isZoneQuote) {
		if(isZoneQuote){
			return find("FROM Quote WHERE quoteType='zone' ORDER BY quoteDate DESC", new Object[]{});
		}else{
			return find("FROM Quote WHERE (quoteType <> 'zone' OR quoteType is null )  ORDER BY quoteDate DESC", new Object[]{});
		}
	}
	
	public List<Quote> getQuotesByBrokerId(Integer brokerId,boolean isZoneQuote) {
		if(isZoneQuote){
			return find("select q FROM Quote q,User u WHERE q.username=u.username and u.brokerId=? and q.quoteType='zone' ORDER BY q.quoteDate DESC", new Object[]{brokerId});
		}else{
			return find("select q FROM Quote q,User u WHERE q.username=u.username and u.brokerId=? and (q.quoteType <> 'zone' OR q.quoteType is null )  ORDER BY q.quoteDate DESC", new Object[]{brokerId});
		}
	}
	
}
