package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.TMATDependentChildCategory;

public class TMATDependentChildCategoryDAO extends HibernateDAO<Integer, TMATDependentChildCategory> implements
		com.admitone.tmat.dao.TMATDependentChildCategoryDAO {

	
	public List<TMATDependentChildCategory> getTmatDependentsByChildCategoryId(Integer childCategoryId) {
		return find("FROM TMATDependentChildCategory WHERE childCategoryId = ? ",new Object[]{childCategoryId});
	}

		
}
