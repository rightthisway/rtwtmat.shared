package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.AutopricingProduct;



public class AutopricingProductDAO extends HibernateDAO<Integer, AutopricingProduct> implements com.admitone.tmat.dao.AutopricingProductDAO{

	public Collection<AutopricingProduct> getAllAutopricingProductExceptGivenId(Integer id) {
		return find("FROM AutopricingProduct WHERE id != ?" , new Object[]{id});
	}

	public AutopricingProduct getAutopricingProductByName(String name) {
		List<AutopricingProduct> list = find("FROM AutopricingProduct WHERE name = ?" , new Object[]{name});
		if(list==null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}

	public AutopricingProduct getAutopricingProductByInternalNotes(String internalNotes) {
		List<AutopricingProduct> list = find("FROM AutopricingProduct WHERE internalNotes = ?" , new Object[]{internalNotes});
		if(list==null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}
	
	public List<String> getAutopricingProductInternalNotes() {
		return find("select DISTINCT internalNotes FROM AutopricingProduct");
	}
	
}
