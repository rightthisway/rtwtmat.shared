package com.admitone.tmat.dao.hibernate;

import java.util.List;

import org.hibernate.Query;

import com.admitone.tmat.data.ZonePricingErrorLog;

public class ZonePricingErrorLogDAO extends HibernateDAO<Integer, ZonePricingErrorLog> implements 
com.admitone.tmat.dao.ZonePricingErrorLogDAO{

	
	
	public List<String> getDistinctVenuesByErrorType(String errorType){
		List<String> result = null;
		 
		  StringBuffer buffer = new StringBuffer ("SELECT DISTINCT e.venueName from ZonePricingErrorLog e where e.errorType=? " );
		  Query query = null;
		  org.hibernate.Session session = getSession();
		try {   
		   query = session.createQuery(buffer.toString());
		   query.setString(0, errorType);
		   result = query.list();
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally  {
			  session.close();
		  }
		  
		  return result;
	}
	public List<ZonePricingErrorLog> getAllFailedZonesByErrorType(String errorType){
		return find("FROM ZonePricingErrorLog where errorType=? ",new Object[]{errorType});
	}
	public List<ZonePricingErrorLog> getAllFailedFCByErrorType(String errorType1,String errorType2){
		return find("FROM ZonePricingErrorLog where ( errorType=? or errorType=? )",new Object[]{errorType1,errorType2});
	}
	
}
