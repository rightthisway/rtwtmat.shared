package com.admitone.tmat.dao.hibernate;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateCallback;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.data.UserOutlier;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.UserAlertType;
import com.admitone.tmat.utils.Categorizer;
import com.admitone.tmat.utils.TicketUtil;
import com.admitone.tmat.web.Constants;

public class TicketDAO extends HibernateDAO<Integer, Ticket> implements com.admitone.tmat.dao.TicketDAO {	
	public Ticket get(Integer id) {
		/*
		it's faster to not use a parametrized query on sqlserver
		*/
		/*
		List<Ticket> tickets = find("FROM Ticket WHERE id='" + id + "'");
		if (tickets != null && tickets.size() > 0) {
			return tickets.get(0);
		}
		return null;
		*/
		return super.get(id);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEvent(int event_id) {
		return find("FROM Ticket WHERE eventId = ?", new Object[]{event_id});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEventAndSiteId(int event_id, String siteId) {
		return find("FROM Ticket WHERE eventId = ? AND siteId = ?", new Object[] {event_id, siteId});
	}
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEventAndSiteId(int event_id, String siteId) {
		return find("FROM Ticket WHERE eventId = ? AND siteId = ? AND ticketStatus='ACTIVE'", new Object[] {event_id, siteId});
	}
	@SuppressWarnings("unchecked")
	// only gets id, siteId, itemId, currentPrice, priceUpdateCount, insertion date, seller, status
	public Collection<Ticket> getAllSimpleTicketsByEventAndSiteId(int event_id, String siteId) {		
		// gets 7 fields instead of 27 fields
		List<Object[]> simpleTickets = find("SELECT id, itemId, insertionDate, currentPrice, priceUpdateCount, seller, ticketStatus FROM Ticket WHERE eventId = ? AND siteId = ?", new Object[] {event_id, siteId});
		
		List<Ticket> tickets = new ArrayList<Ticket>();
		for (Object[] simpleTicket: simpleTickets) {
			Ticket ticket = new Ticket();
			ticket.setId((Integer)simpleTicket[0]);
			ticket.setItemId((String)simpleTicket[1]);
			ticket.setInsertionDate((Date)simpleTicket[2]);
			ticket.setCurrentPrice((Double)simpleTicket[3]);
			ticket.setPriceUpdateCount((Integer)simpleTicket[4]);
			ticket.setSeller((String)simpleTicket[5]);
			ticket.setTicketStatus((TicketStatus)simpleTicket[6]);
			tickets.add(ticket);			
		}
		return tickets;
	}

	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEvent(int event_id, TicketStatus status) {
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEventUpdatedAfter(int event_id, Date afterDate) {
		return find("FROM Ticket WHERE eventId = ? AND lastUpdate > ?", new Object[]{event_id, afterDate});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllBuyItNowTickets(TicketStatus status) {
		return find("FROM Ticket WHERE buyItNowPrice = 0 AND ticketStatus=?", new Object[]{status});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllOutDatedExpiredTickets() {
		// return all the tickets which are expired and which were updated before their expired
		return find("FROM Ticket WHERE endDate < ? AND lastUpdate < endDate", new Object[]{new Date()});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllOutDatedExpiredTicketsFromCrawl(Integer crawlId) {
		// return all the tickets which are expired and which were updated before their expired
		return find("FROM Ticket WHERE ticketListingCrawlId = ? AND endDate < ? AND lastUpdate < endDate AND ticketStatus=?", new Object[] {crawlId, new Date(), TicketStatus.ACTIVE});		
	}

	public void updateStatusOfOutdatedExpiredTicketsFromCrawl(Integer crawlId, TicketStatus ticketStatus) {
		bulkUpdate("UPDATE Ticket SET ticketStatus=? WHERE ticketListingCrawlId=? AND endDate < ? and lastUpdate < endDate AND ticketStatus=?", new Object[] {ticketStatus, crawlId, new Date(), TicketStatus.ACTIVE});	
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsUpdatedBefore(Date date) {
		return find("FROM Ticket WHERE lastUpdate < ? AND ticketStatus=?", new Object[] {date, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsUpdatedBeforeFromCrawl(Integer crawlId, Date date) {
		return find("FROM Ticket WHERE ticketListingCrawlId = ? AND lastUpdate < ? AND ticketStatus=?", new Object[] {crawlId, date, TicketStatus.ACTIVE});		
	}
	
	@SuppressWarnings("unchecked")
	public void updateStatusOfTicketsUpdatedBeforeFromCrawl(Integer crawlId, Date date, TicketStatus ticketStatus) {
		bulkUpdate("UPDATE Ticket SET ticketStatus=? WHERE ticketListingCrawlId=? AND lastUpdate < ? AND ticketStatus != ?", new Object[] {ticketStatus, crawlId, date,ticketStatus});				
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId) {
		// return all the tickets which are expired and which were updated before their expired
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus != ?", new Object[] {eventId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId) {
		// return all the tickets which are expired and which were updated before their expired
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ?", new Object[] {eventId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveRegularTicketsByEvent(Integer eventId) {
		// return all the tickets which are expired and which were updated before their expired
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? AND ticket_type='REGULAR' ORDER BY buyItNowPrice ASC", new Object[] {eventId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsBySiteId(String siteId) {
		// return all the tickets which are expired and which were updated before their expired
		return find("FROM Ticket WHERE siteId = ? AND ticketStatus = ?", new Object[] {siteId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, Integer categoryId) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = find("FROM Ticket WHERE eventId = ? AND ticketStatus != ?", new Object[] {eventId, TicketStatus.ACTIVE});
		return TicketUtil.filterCategory(categoryId, tickets);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, List<Integer> categoryIds) {
		// return all the tickets which are expired and which were updated before their expired
		String query = "FROM Ticket WHERE eventId = ? AND ticketStatus != ?";
		
		if (categoryIds != null && !categoryIds.isEmpty()) {
			query += " AND categoryId IN (" + categoryIds.get(0);
			for (int i = 1 ; i < categoryIds.size() ; i++) {
				query += ", " + categoryIds.get(i);
			}
			query += ") ";
		}
		Collection<Ticket> tickets = find(query, new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategories(categoryIds, tickets);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId, Integer categoryId) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = find("FROM Ticket WHERE eventId = ? AND ticketStatus = ?", new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategory(categoryId, tickets);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId, List<Integer> categoryIds) {
		// return all the tickets which are expired and which were updated before their expired
		String query = "FROM Ticket WHERE eventId = ? AND ticketStatus = ?";
		Collection<Ticket> tickets = find(query, new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategories(categoryIds, tickets);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEvent(Integer eventId, Integer categoryId, String day) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? and ", new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategory(categoryId, tickets);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getUncategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = this.getAllCompletedTicketsByEvent(eventId);		
		return TicketUtil.filterUncategorized(tickets, catScheme);
	}
	

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getUncategorizedActiveTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = this.getAllActiveTicketsByEvent(eventId);
		return TicketUtil.filterUncategorized(tickets, catScheme);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getCategorizedActiveTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = this.getAllActiveTicketsByEvent(eventId);
		return TicketUtil.filterCategorized(tickets, catScheme);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getCategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = getAllCompletedTicketsByEvent(eventId);		
		return TicketUtil.filterCategorized(tickets, catScheme);
	}

	@SuppressWarnings("unchecked")
	public Collection<Object[]> getSectionsAndRowsFromEventId(Integer eventId) {
		return find("SELECT section, row, COUNT(id) FROM Ticket WHERE eventId = ? AND ticketStatus = ? GROUP BY section, row", new Object[] {eventId, TicketStatus.ACTIVE});				
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByTour(Integer tourId) {
		return find("SELECT t FROM Ticket t, Event e WHERE t.eventId = e.id AND e.tourId=? AND t.ticketStatus = ?", new Object[] {tourId, TicketStatus.ACTIVE});		
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus) {
		return find("SELECT siteId, COUNT(id), SUM(remainingQuantity) FROM Ticket WHERE eventId=? AND ticketStatus=? GROUP BY siteId", new Object[] {event_id, ticketStatus});						
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getNonDuplicateTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus, RemoveDuplicatePolicy removeDuplicatePolicy) {
		List<Ticket> tickets = (List<Ticket>)find("FROM Ticket WHERE eventId=? AND ticketStatus=?", new Object[] {event_id, ticketStatus});
		if (tickets == null) {
			return null;
		}
		
		Map<String, Integer> ticketCountBySite = new HashMap<String, Integer>();
		Map<String, Integer> ticketEntryCountBySite = new HashMap<String, Integer>();
		for (String siteId: Constants.getInstance().getSiteIds()) {
			ticketCountBySite.put(siteId, 0);
			ticketEntryCountBySite.put(siteId, 0);
		}
		
		tickets = TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
		for (Ticket ticket: tickets) {
			ticketCountBySite.put(ticket.getSiteId(), ticketCountBySite.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
			ticketEntryCountBySite.put(ticket.getSiteId(), ticketEntryCountBySite.get(ticket.getSiteId()) + 1);
		}
		
		List<Object[]> ticketSiteStats = new  ArrayList<Object[]>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			ticketSiteStats.add(new Object[]{siteId, new Long(ticketEntryCountBySite.get(siteId)), new Long(ticketCountBySite.get(siteId))});
		}
		return ticketSiteStats;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getTicketQuantityStatsByEventId(int event_id, TicketStatus ticketStatus) {
		return find("SELECT remainingQuantity, COUNT(id), SUM(remainingQuantity) FROM Ticket WHERE eventId=? AND ticketStatus=? GROUP BY remainingQuantity", new Object[] {event_id, ticketStatus});
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getNonDuplicatesTicketQuantityStatsByEventId(int event_id, TicketStatus ticketStatus) {
		List<Ticket> tickets = (List<Ticket>)find("FROM Ticket WHERE eventId=? AND ticketStatus=?", new Object[] {event_id, ticketStatus});
		if (tickets == null) {
			return null;
		}

		Map<Integer, Integer> ticketCountBySite = new HashMap<Integer, Integer>();
		Map<Integer, Integer> ticketEntryCountBySite = new HashMap<Integer, Integer>();
		
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		for (Ticket ticket: tickets) {
			if (!ticketCountBySite.containsKey(ticket.getRemainingQuantity())) {
				ticketCountBySite.put(ticket.getRemainingQuantity(), ticket.getRemainingQuantity());
				ticketEntryCountBySite.put(ticket.getRemainingQuantity(), 1);				
			} else {
				ticketCountBySite.put(ticket.getRemainingQuantity(), ticketCountBySite.get(ticket.getRemainingQuantity()) + ticket.getRemainingQuantity());
				ticketEntryCountBySite.put(ticket.getRemainingQuantity(), ticketEntryCountBySite.get(ticket.getRemainingQuantity()) + 1);				
			}
		}
		
		List<Object[]> ticketSiteStats = new  ArrayList<Object[]>();
		for(Integer quantity: ticketEntryCountBySite.keySet()) {
			ticketSiteStats.add(new Object[]{quantity, new Long(ticketEntryCountBySite.get(quantity)), new Long(ticketCountBySite.get(quantity))});						
		}
		return ticketSiteStats;
	}

	public void deleteTicketsByCrawlId(int crawlId) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarksByCrawlId(crawlId);
//		 DAORegistry.getEbayInventoryGroupDAO().markAsDeletedByCrawlId(crawlId);
		bulkUpdate("DELETE FROM Ticket WHERE ticketListingCrawlId=?", new Object[] {crawlId});		
	}

	public void deleteTicketsByCrawlId(int crawlId, Date startDate, Date endDate) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarksByCrawlId(crawlId, startDate, endDate);
		bulkUpdate("DELETE FROM Ticket WHERE ticketListingCrawlId=? AND insertionDate >= ? AND insertionDate < ?", new Object[] {crawlId, startDate, endDate});		
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantities != null && !quantities.isEmpty()) {
			query += " AND remainingQuantity IN (" + quantities.get(0);
			for (int i = 1 ; i < quantities.size() ; i++) {
				query += ", " + quantities.get(i);
			}
			query += ") ";
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		// System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets;
		
		if (startDate != null) {
			tickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			tickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}
		
		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, List<Integer> categoryIds, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantities != null && !quantities.isEmpty()) {
			query += " AND remainingQuantity IN (" + quantities.get(0);
			for (int i = 1 ; i < quantities.size() ; i++) {
				query += ", " + quantities.get(i);
			}
			query += ") ";
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		// System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets;
		
		if (startDate != null) {
			tickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			tickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}

		if (categoryIds == null || categoryIds.get(0).equals(Category.ALL_ID)) {
		} else if (categoryIds.get(0).equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryIds.get(0).equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategories(categoryIds, tickets);
		}

		if (tickets == null) {
			//System.out.println("Returning null Stat");
			return null;
		}
		
		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}
		
		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantity != null && quantity > 0) {
			query += " AND remainingQuantity=" + quantity;
		}
		
		
		
		if (section != null && !section.isEmpty()) {
			if(section.contains("OR")){
				return null;
			}else if(section.contains("-")){
				
				String sectionArray[] = section.split("-");
				
				int i=0;
				for (String secObj : sectionArray) {
					i++;
				}
				if(i > 1 ){
					return null;
				}
				
				section =sectionArray[0].replace(" ", "");
				query += "AND lower(section) like '"+section+"'";	
								
			}
		}
		
		/*if (section != null && !section.isEmpty()) {
		query = addSectionToQuery(query, section);
		}*/

		/*if (row != null && !row.isEmpty()) {
			String[] cleanedRow = new String[4];
			
			if(row.contains("-")){
				cleanedRow =row.split("-");
				
				if(cleanedRow[0].matches("\\d+")){
					query += " AND lower(row) IN ( ";
					Integer start=null,end=null;
					Integer i =Integer.parseInt(cleanedRow[0].trim());
					Integer j =Integer.parseInt(cleanedRow[1].trim());
					if(i >j){
						start = j;
						end=i;
						query +="'"+start+"-"+end+"'";
					}else{
						start = i;
						end=j;
						query +="'"+start+"-"+end+"'";
					}
					int first=start;
					for(int k=start;k <=end;k++){
						if(k == first){
							query +="'"+k+"'";
						}else{
							query +=",'"+k+"'";
						}
					}
					query += " ) ";
				}else{
					query += " AND lower(row) like '%" + cleanedRow[0].toLowerCase() + "%'";
				}
			}else{
				query += " AND lower(row) IN ( '" + row.toLowerCase() + "' )";
			}
		}*/

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//			// do nothing: we select all categories
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";			
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";			
//		} else {
//			query += " AND category_id=" + categoryId;
//		}

		// System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		if (startDate != null) {
			tickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			
			tickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}
		
		if (null == tickets || tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}
	
	public String addSectionToQuery(String query,String section){
		
		String[] splittedSection = new String[10];
		String[] innerSection = new String[10];
		query += "AND lower(section) IN ( ";
		if(section.contains("OR")){
			splittedSection =section.split("OR");
			for (String splitSection : splittedSection) {
				
						splitSection = splitSection.replace(" ", "");
						splitSection = splitSection.replace(" ", "");
						innerSection = new String[10];
						innerSection = splitSection.split("-");
						Integer i = Integer.parseInt(innerSection[0].trim());
						Integer j = Integer.parseInt(innerSection[1].trim());
						Integer start=null;
						Integer end=null;
						if(i > j){
							start = j;
							end=i;
							query +="'"+start+"-"+end+"'";
						}else{
							start = i;
							end=j;
							query +="'"+start+"-"+end+"'";
						}
						int first=start;
						for(int k=start;k <=end;k++){
							
							if(k == first){
								query +="'"+k+"'";
							}else{
								query +=",'"+k+"'";
							}
						}
				}
			
		}else if(section.contains("-")){
			query +="'"+section+"'";
			splittedSection =section.split("-");
			if(null != splittedSection && splittedSection.length >0){
				Integer Start = Integer.parseInt(splittedSection[0]);
				Integer end = Integer.parseInt(splittedSection[1]);
				int j=Start;
				for(int i=Start;i <=end;i++){
					if(i == j){
						query +="'"+i+"'";
					}else{
						query +=",'"+i+"'";
					}
				}
			}
		}else if(section.contains(" ")){
			splittedSection =section.split(" ");
			
			if(null != splittedSection && splittedSection.length >0){
				
				Integer start =Integer.parseInt(splittedSection[0]);
				Integer end =Integer.parseInt(splittedSection[1]);
				int j=start;
				for(int i=start;i <=end;i++){
					if(i == j){
						query +="'"+i+"'";
					}else{
						query +=",'"+i+"'";
					}
				}
				
				 start =Integer.parseInt(splittedSection[2]);
				 end =Integer.parseInt(splittedSection[3]);
				 j=start;
				for(int i=start;i <=end;i++){
					if(i == j){
						query +="'"+i+"'";
					}else{
						query +=",'"+i+"'";
					}
				}
					
			}
		}else{
			section =section.replace(" ", "");
			query +="'"+section+"'";
			
		}
		query +=  " )";
		return query;
	}

	/*@SuppressWarnings("unchecked")
	public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(Integer eventId, Integer categoryId, Integer lotSize, String catScheme) {
		return this.getEventCategoryLotSimpleTickets(eventId, categoryId, lotSize, false, catScheme);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(Integer eventId, Integer categoryId, Integer lotSize, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";

		query += " AND ticket_status='ACTIVE' ";

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		//System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets;
		ArrayList<SimpleTicket> sTickets = new ArrayList<SimpleTicket>();
		
		tickets = find(query + " ORDER BY buyItNowPrice ASC", new Object[]{eventId});			

		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		//This method mutates the Collection tickets
		TicketUtil.removeDuplicateTickets(tickets);

		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}
	
		for(Ticket ticket : tickets){
			
			sTickets.add(new SimpleTicket(ticket, catScheme));
		}
		
		return sTickets;
	}*/
	
	public Collection<Ticket> getAllActiveTicketsByEventAndSiteIds(Integer eventId, String siteId) {
		return find("FROM Ticket WHERE event_id=? and site_id=? and ticket_status='ACTIVE'", new Object[]{eventId, siteId});
	}

//	
//	@SuppressWarnings("unchecked")
//	public SimpleTicket getLowestEventCategoriesLotsSimpleTicket(Integer eventId, Collection<Integer> categoryIds, Collection<Integer> lotSizes, Collection<Integer> usedTixIds) {
//		String query = "FROM Ticket WHERE eventId=? ";
//
//		query += " AND ticket_status='ACTIVE'";
//		query += " AND ticket_type='REGULAR'";
//		
//		if (lotSizes != null && !lotSizes.isEmpty()) {
//			query += " AND lot_size IN(1,";
//			Iterator<Integer> lotIt = lotSizes.iterator();
//			while(lotIt.hasNext()){
//				Integer lotSize = lotIt.next();
//				query += lotSize ;
//				if(lotIt.hasNext()){
//					query +=  ",";
//				} else {
//					query +=  ")";
//				}
//			}	
//			
//			query += " AND remaining_quantity IN(";
//			lotIt = lotSizes.iterator();
//			while(lotIt.hasNext()){
//				Integer lotSize = lotIt.next();
//				query += lotSize ;
//				if(lotIt.hasNext()){
//					query +=  ",";
//				} else {
//					query +=  ")";
//				}
//			}	
//		}
//
//		if (categoryIds != null) {
//			if(categoryIds.size() > 0) {
//				query += " AND category_id IN(";
//				Iterator<Integer> catIt = categoryIds.iterator();
//				while(catIt.hasNext()){
//					Integer catId = catIt.next();
//					query += catId ;
//					if(catIt.hasNext()){
//						query +=  ",";
//					} else {
//						query +=  ")";
//					}
//				}	
//			}
//		}
//
//		if (usedTixIds != null) {
//			if(usedTixIds.size() > 0) {
//				query += " AND id NOT IN(";
//				Iterator<Integer> tixIt = usedTixIds.iterator();
//				while(tixIt.hasNext()){
//					Integer tixId = tixIt.next();
//					query += "'" + tixId ;
//					if(tixIt.hasNext()){
//						query +=  "',";
//					} else {
//						query +=  "')";
//					}
//				}
//			}
//		}
//
//		// System.out.println("QUERY=" + query);
//		
//		Collection<Ticket> tickets;
//		
//		tickets = find(query + " ORDER BY buyItNowPrice ASC", new Object[]{eventId});			
//
//		if (tickets.size() == 0) {
//			//System.out.println("Returning null Stat");
//			return null;
//		}
//		
//		tickets = TicketUtil.removeAdmitOneTickets(tickets);
//		if (tickets.size() == 0) {
//			//System.out.println("Returning null Stat");
//			return null;
//		}
//			
//		//This method mutates the Collection tickets
//		TicketUtil.removeDuplicateTickets(tickets);
//
//		if (tickets.size() == 0) {
//			//System.out.println("Returning null Stat");
//			return null;
//		}
//
//		//Because the tickets are ordered, the first ticket is the lowest priced ticket
//		return new SimpleTicket(tickets.iterator().next());
//	}

	@SuppressWarnings("unchecked")
	public int getTicketCount(TicketStatus status) {
		List list = find("SELECT count(id) FROM Ticket WHERE ticketStatus=?", new Object[]{status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByEvent(int event_id, TicketStatus status) {
		List list = find("SELECT count(id) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketQuantitySumByEvent(int event_id, TicketStatus status) {
		List list = find("SELECT sum(remainingQuantity) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByCrawl(int crawl_id, TicketStatus status) {
		List list = find("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ? AND ticketStatus=?", new Object[]{crawl_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByCrawl(int crawl_id, Date startDate, Date endDate) {
		List list = find("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ? AND insertionDate >= ? AND insertionDate < ?", new Object[]{crawl_id, startDate, endDate});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketQuantitySumByCrawl(int crawl_id, TicketStatus status) {
		List list = find("SELECT sum(remainingQuantity) FROM Ticket WHERE ticketListingCrawlId = ? AND ticketStatus=?", new Object[]{crawl_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByCrawl(int crawl_id) {
		List list = find("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ?", new Object[]{crawl_id});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketQuantitySumByCrawl(int crawl_id) {
		List list = find("SELECT sum(remainingQuantity) FROM Ticket WHERE ticketListingCrawlId = ?", new Object[]{crawl_id});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	public void deleteTicketsByEventId(Integer eventId) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarksWithEventId(eventId);
		bulkUpdate("DELETE FROM Ticket WHERE eventId=? AND eventId IS NOT NULL", new Object[]{eventId});
	}
	
	public void enableTickets(Integer crawlerId) {
		bulkUpdate("UPDATE Ticket SET ticket_status='ACTIVE' WHERE crawl_id=? AND ticket_status='DISABLED'", new Object[]{crawlerId});		
	}

	public void disableTickets(Integer crawlerId) {
		bulkUpdate("UPDATE Ticket SET ticket_status='DISABLED',last_update=getdate() WHERE crawl_id=? AND ticket_status!='DISABLED'", new Object[]{crawlerId});		
	}
	public void disableTicketsByCrawlId(int crawlId, Date startDate, Date endDate) {
		bulkUpdate("UPDATE Ticket SET ticket_status='DISABLED',last_update=getdate() WHERE ticketListingCrawlId=? AND insertionDate >= ? AND insertionDate < ? AND ticket_status!='DISABLED'", new Object[] {crawlId, startDate, endDate});		
	}
	
	public void disableAndUnassignCrawlTickets(Integer crawlId) {
		bulkUpdate("UPDATE Ticket SET ticket_status='DISABLED', crawl_id=NULL WHERE crawl_id=?", new Object[]{crawlId});		
	}
	
	public void recomputeTicketRemainingQuantities() {
		bulkUpdate("UPDATE Ticket SET remaining_quantity=quantity - sold_quantity");				
	}	
	
	public void saveOrUpdateAllTicketsWithSameEventAndSite(final Collection<Ticket> tickets) {
		getCachedHibernateTemplate().execute(new HibernateCallback() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				if (tickets == null || tickets.isEmpty()) {
					return null;
				}
				
				Ticket firstTicket = tickets.iterator().next();
				
				Collection<Ticket> updatedTickets = new ArrayList<Ticket>();
				Collection<Ticket> newTickets = new ArrayList<Ticket>();
				Map<String, Ticket> existingTicketMap = new HashMap<String, Ticket>();

				try {
					session.beginTransaction();
					Query query = session.createQuery("FROM Ticket WHERE event_id=? and site_id=?");
					query.setInteger(0, firstTicket.getEventId());
					query.setString(1, firstTicket.getSiteId());
					
					for(Object obj: query.list()) {
						Ticket ticket = (Ticket)obj;
						existingTicketMap.put(ticket.getTicketKey(), ticket);
					}
					
					for (Ticket ticket: tickets) {
						Ticket existingTicket = existingTicketMap.get(ticket.getTicketKey());
						if (existingTicket == null) {
							session.save(ticket);
						} else {
							existingTicket.setEventId(ticket.getEventId());
							existingTicket.setTicketType(ticket.getTicketType());
							existingTicket.setQuantity(ticket.getQuantity());
							existingTicket.setSoldQuantity(ticket.getSoldQuantity());
							existingTicket.setRemainingQuantity(ticket.getRemainingQuantity());
							existingTicket.setSection(ticket.getSection());
							existingTicket.setRow(ticket.getRow());
							existingTicket.setSeat(ticket.getSeat());
							existingTicket.setCurrentPrice(ticket.getCurrentPrice());
							existingTicket.setBuyItNowPrice(ticket.getBuyItNowPrice());
							existingTicket.setEndDate(ticket.getEndDate());
							existingTicket.setLastUpdate(ticket.getLastUpdate());
							existingTicket.setLotSize(ticket.getLotSize());
							//existingTicket.setNormalizedSection(ticket.getNormalizedSection());
							//existingTicket.setNormalizedRow(ticket.getNormalizedRow());
							//existingTicket.setNormalizedSeat(ticket.getNormalizedSeat());
							//existingTicket.setCategoryId(ticket.getCategoryId());
							//existingTicket.setAdjustedCurrentPrice(ticket.getAdjustedCurrentPrice());
							session.update(existingTicket);
						}
					}
					
					session.getTransaction().commit();
				} catch (Exception e) {
					session.getTransaction().rollback();
					throw new RuntimeException(e);
				}
				
				return null;
			}
			
		});
	}

	public Collection<Ticket> getAllTicketsByEventAndDate(Integer eventId, Date date) {
		return find("FROM Ticket WHERE eventId=? AND date(lastUpdate)=date(?) ORDER BY currentPrice ASC", new Object[]{eventId, date});
	}

	public void deleteById(Integer ticketId) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarks(ticketId);
		super.deleteById(ticketId);
	}
	
	public void delete(Ticket ticket) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarks(ticket.getId());
		super.delete(ticket);
	}


	/*public Collection<Ticket> getAllTicketsMatchingUserAlert(UserAlert userAlert) {
		return getAllTicketsMatchingUserAlert(userAlert, userAlert.getLastCheck(), TicketStatus.ACTIVE);
	}*/

	public Collection<Ticket> getAllTicketsMatchingUserAlert(UserAlert userAlert) {
		List<Object> parameters = new ArrayList<Object>();
		
		/*Logic for  odd even based on quantites -begins*/
		List<Object> evenQuantities = new ArrayList<Object>();
		List<Object> oddQuantities = new ArrayList<Object>();
		Object[] evenObjArray =  {"2+","4+","6+","8+","10+"};
		Object[] oddObjArray =  {"3+","5+","7+","9+","11+"};
		for (Object evenObj : evenObjArray) {
			evenQuantities.add(evenObj);
		}
		for (Object oddObj : oddObjArray) {
			oddQuantities.add(oddObj);
		}
		/*Logic for odd even based on quantites -Ends*/
		
		String query = "FROM Ticket WHERE eventId=?";// AND ticketType=?";
		parameters.add(userAlert.getEventId());
//		parameters.add(TicketType.REGULAR);
		//System.out.println("PARAM EVENTID=" + userAlert.getEventId());
		//System.out.println("PARAM TICKETTYPE=" + TicketType.REGULAR);
		
		if (userAlert.getUserAlertType().equals(UserAlertType.IN)) {
			query += " AND ticketStatus = ?";
			parameters.add(TicketStatus.ACTIVE);		
			//System.out.println("PARAM TICKET STATUS=" + ticketStatus);
		}else if(userAlert.getUserAlertType().equals(UserAlertType.OUT)) {
			query += " AND ticketStatus != ?";
			parameters.add(TicketStatus.ACTIVE);
			
			query += " AND lastUpdate > ?";
			parameters.add(userAlert.getCreationDate());
			//System.out.println("PARAM TICKET STATUS=" + ticketStatus);
		}
//		Date checkDate= userAlert.getLastEmailDate()!=null?userAlert.getLastEmailDate():userAlert.getLastCheck();
//		if (checkDate != null) {
//			query += " AND (insertionDate > ? OR lastUpdate > ?)";
//			parameters.add(checkDate);
//			parameters.add(checkDate);
//			//System.out.println("PARAM LAST UPDATE=" + lastUpdate);
//		}

		if (userAlert.getQuantities().isEmpty()) {
			// we take all
		} else {
			String[] tokens = userAlert.getQuantities().split(",");
			query += " AND ( 0=1"; // so we can start with OR after that
			for(String token: tokens) {
				token = token.trim().toLowerCase();
				
				/*odd even logic --Begins-By Ulaganathan */
				if(evenQuantities.contains(token)){
					
					query += " OR (remainingQuantity >=? AND mod(remainingQuantity,2)=0) ";
					parameters.add(Integer.parseInt(token.substring(0, token.length() - 1)));	
					
				}else if(oddQuantities.contains(token)){
					query += " OR (remainingQuantity >=?  AND mod(remainingQuantity,2)=1) ";
					parameters.add(Integer.parseInt(token.substring(0, token.length() - 1)));
				}
				/*odd even logic --Ends-By Ulaganathan */
				
				else if (token.matches("^\\d*$")) {
					query += " OR remainingQuantity=?";
					parameters.add(Integer.parseInt(token));
				} else if (token.matches("^\\d*[+]$")) {
					query += " OR remainingQuantity>=?";
					parameters.add(Integer.parseInt(token.substring(0, token.length() - 1)));					
				} else if (token.matches("^\\d+-\\d+$")) {
					query += " OR (remainingQuantity>=? AND remainingQuantity <=?)";
					String[] numbers = token.split("-");
					parameters.add(Integer.parseInt(numbers[0]));					
					parameters.add(Integer.parseInt(numbers[1]));					
				} else if (token.equals("odd")) {
					query += " OR (remainingQuantity!=1 AND mod(remainingQuantity,2)=1)";
				} else if (token.equals("even")) {
					query += " OR mod(remainingQuantity,2)=0";
				}
			}
			query += ")";

		//	System.out.println("QUERY=" + query); (wTicketRow.getBuyItNowPrice()*(1+serviceFee/100)) + (shippingFee/wTicketRow.getQuantity())):
		} 

		if (userAlert.getSectionRange() == null) {
			query += " AND (normalizedSection = ? OR normalizedSection = ?)";
			parameters.add(null);			
			parameters.add("");			
		} else if (userAlert.getSectionRange().isEmpty() || userAlert.getSectionRange().equals(":")) {
			// we take all
		} else {
			if (userAlert.getSectionLow() != null) {
				Integer intSectionLow=0;
//				String sectionLow="";
				Integer intSectionHigh=0;
//				String sectionHigh="";
				try{
					intSectionLow=Integer.parseInt(userAlert.getSectionLow());
					query += " AND ISNUMERIC(normalizedSection)=1 ";
					query += " AND normalizedSection >= ? ";
					parameters.add(userAlert.getSectionLow());
					if(userAlert.getSectionHigh()!=null && !userAlert.getSectionHigh().isEmpty()){
						intSectionHigh=Integer.parseInt(userAlert.getSectionHigh());
						query += " AND normalizedSection <= ?";
						parameters.add(userAlert.getSectionHigh());	
					}
					query += " AND len(normalizedSection)<= ?";
					Integer len=(userAlert.getSectionHigh()!=null && !userAlert.getSectionHigh().isEmpty())?userAlert.getSectionHigh().length():userAlert.getSectionLow().length();
					parameters.add(len.longValue());
				}catch (Exception e) {
					Pattern pp= Pattern.compile("([a-zA-Z]+)(\\s*)(\\d+)");
					Matcher matcher=pp.matcher(userAlert.getSectionLow());
					Matcher matcher1=pp.matcher(userAlert.getSectionHigh());
					if(matcher.find()){
						intSectionLow=Integer.parseInt(matcher.group(3));
						intSectionHigh=Integer.parseInt(matcher1.group(3));
						query += " AND ISNUMERIC(normalizedSection)=0 AND (";
						for(int i=intSectionLow;i<=intSectionHigh;i++){
							query += " normalizedSection = ? OR ";
							parameters.add(matcher1.group(1)+ matcher1.group(1) + i);
						}
						query= query.substring(0,query.length()-3);
						query += ")";
					}else{
						query += " AND (normalizedSection = ? OR normalizedSection = ?)";
						parameters.add(userAlert.getSectionLow());
						parameters.add(userAlert.getSectionHigh());
					}
				}
				
//				query += " AND normalizedSection >= ?";
//				parameters.add(userAlert.getSectionLow());
				//System.out.println("PARAM SECTION < =" + userAlert.getSectionLow());				
			}

//			if (userAlert.getSectionHigh() != null) {
//				query += " AND normalizedSection <= ?";
//				parameters.add(userAlert.getSectionHigh());
//				//System.out.println("PARAM SECTION > =" + userAlert.getSectionHigh());				
//			}
		} 
		
		if (userAlert.getRowRange() == null) {
			query += " AND (row = ? OR row = ?)";
			parameters.add(null);			
			parameters.add("");			
		} else if (userAlert.getRowRange().isEmpty() || userAlert.getRowRange().equals(":")) {
			// we take all
		} else {
			if (userAlert.getRowLow() != null) {
				query += " AND row >= ?";
				parameters.add(userAlert.getRowLow());
			}

			if (userAlert.getRowHigh() != null) {
				query += " AND row <= ?";
				parameters.add(userAlert.getRowHigh());
			}
		} 
		
		if (userAlert.getAlertFor().equals(AlertFor.TMAT)
				&& userAlert.getEventId() != null && userAlert.getSiteIds().length > 0) {
			query += " AND siteId in (";
			boolean firstElement = true;
			for(String siteId: userAlert.getSiteIds()) {
				if (!firstElement) {
					query += ",";					
				} else {
					firstElement = false;
				}
				query += "'" + siteId + "'";					
			}
			query += ")";
		}

//		System.out.println("ALERTQUERY=" + query);

		Object[] params = parameters.toArray(new Object[parameters.size()]);
		Collection<Ticket> tickets = find(query, params);
		if (tickets == null) {
			tickets = new ArrayList<Ticket>();
		}

		Collection<Ticket> filteredTickets = new ArrayList<Ticket>();
			
		//DAOCall method - for fetching event by userAlert eventId
		Event event = DAORegistry.getEventDAO().get(userAlert.getEventId());
//		Tour tour = event.getTour();
		
		//DAOCall method - for fetching categories based on venue id and category group name
		Collection<Category> categories = null;
		if(event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), event.getVenueCategory().getCategoryGroup());
		}
		Map<Integer, Category> categoryById = new HashMap<Integer, Category>();
		if (categories != null) {
			for(Category category:categories) {
				categoryById.put(category.getId(), category);
			}
		}
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = null;
		if(event.getVenueCategory()!=null){
			categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
		}
		if(categoryMappingList != null){
			for(CategoryMapping mapping:categoryMappingList){
				List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
				if(list==null){
					list = new ArrayList<CategoryMapping>();
				}
				list.add(mapping);
				catMappingMap.put(mapping.getCategoryId(), list);
			}
		}
		if (userAlert.getPriceRange() == null || userAlert.getPriceRange().isEmpty() || userAlert.getPriceRange().equals(":")) {
			//Tickets to be added.
		}else{
			Collection<Ticket> newTicketList = new ArrayList<Ticket>();
			newTicketList.addAll(tickets);
			Map<String, ManagePurchasePrice> purchasePriceMap = new HashMap<String, ManagePurchasePrice>();
			List<ManagePurchasePrice> manageTourPrices = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(event.getArtistId());
			for (ManagePurchasePrice manageTourPrice : manageTourPrices) {
				purchasePriceMap.put(manageTourPrice.getExchange()+"-"+manageTourPrice.getTicketType(), manageTourPrice);
			}
			for (Ticket ticket: tickets) {
				/*Ticket purchase price calculation based on ManagePurchasePrice - begins*/
				ManagePurchasePrice manageTourPrice = purchasePriceMap.get(ticket.getSiteId()+"-"+(null == ticket.getTicketType()?"REGULAR":ticket.getTicketType()));
				Double purchasePrice=ticket.getBuyItNowPrice();
				if( null != manageTourPrice && manageTourPrice.getCurrencyType().equals(1)){
					purchasePrice =(ticket.getBuyItNowPrice()*(1+Double.valueOf(manageTourPrice.getServiceFee())/100)) +
					(manageTourPrice.getShipping() /ticket.getRemainingQuantity());
				}else if(null != manageTourPrice){
					purchasePrice =(ticket.getBuyItNowPrice() + Double.valueOf(manageTourPrice.getServiceFee())) +
					(manageTourPrice.getShipping() /ticket.getRemainingQuantity());
				}
				/*Ticket purchase price calculation based on ManagePurchasePrice - begins*/
				
				if (userAlert.getPriceLow() != null) {
					if(userAlert.getAdjustedPriceLow() >= purchasePrice){
						newTicketList.remove(ticket);
					}
				}
				if (userAlert.getPriceHigh() != null) {
					if(userAlert.getAdjustedPriceHigh() <= purchasePrice){
						if(newTicketList.contains(ticket)){
							newTicketList.remove(ticket);
						}
					}
				}
			}
			tickets = new ArrayList<Ticket>();
			tickets.addAll(newTicketList);
		}
		
		Map<String, Category> categoryBySymbolByVenuCatId = new HashMap<String, Category>();
		if (categories != null) {
			for(Category category:categories) {
				categoryBySymbolByVenuCatId.put(category.getVenueCategoryId()+"-"+category.getSymbol(), category);
			}
		}
		
		String venueCatId =null;
		if(null != userAlert.getEvent().getVenueCategory()){
			venueCatId = userAlert.getEvent().getVenueCategoryId().toString();
		}

		String cats = (userAlert.getCategory()==null||userAlert.getCategory().isEmpty())?"ALL CATS":userAlert.getCategory();
		Category catObj = null;
		if(null != venueCatId && venueCatId.trim().length() >0){
			catObj = categoryBySymbolByVenuCatId.get(venueCatId+"-"+cats);
		}
		
		// post filtering (we cannot do these filtering in SQL)		
		for(Ticket ticket: tickets) {			
				
			// filtering on category
			Integer categoryId = Categorizer.computeCategory(ticket,userAlert.getEvent().getVenueCategory(),event.getVenueCategory().getCategoryGroup(),categoryById,catMappingMap);
			Category category = categoryById.get(categoryId);
			/*if (userAlert.getCategory() == null) {
			// we only want ticket with no categories
			if (category != null) {
				//System.out.println("++ 4 REMOVE " + ticket);
				continue;
			}
		} else if (!userAlert.getCategory().isEmpty()) {
			if (category == null || !category.getSymbol().equals(userAlert.getCategory())) {
				//System.out.println("++ 5 REMOVE " + ticket);
				continue;
			}
		}
		filteredTickets.add(ticket);*/
			
			if(category==null &&  cats.equalsIgnoreCase("NO CATS")){
				filteredTickets.add(ticket);
			}else if(category!=null  && cats.equalsIgnoreCase("ALL CATS")){
				filteredTickets.add(ticket);
			}else if(cats.equalsIgnoreCase("ANY")){
				filteredTickets.add(ticket);
			}else if(category !=null &&  cats.equalsIgnoreCase("ALL Zones")){
				filteredTickets.add(ticket);
			}else if(category==null && cats.equalsIgnoreCase("NO ZONES")){
				filteredTickets.add(ticket);
			}else if(null != category ){
				String upgradeCategory =catObj.getEqualCats();
				String[] catStr = upgradeCategory.split(" ");
				Boolean isDiffCategory = true;
				for (String upgradeCat : catStr) {
					if(upgradeCat.equals("*")){
						filteredTickets.add(ticket);
						isDiffCategory = false;
					}else if(upgradeCat.equalsIgnoreCase(category.getSymbol())){
						filteredTickets.add(ticket);
						isDiffCategory =false;
					}
				}
				if(isDiffCategory){
					if(upgradeCategory.equalsIgnoreCase(category.getEqualCats())){
						filteredTickets.add(ticket);	
					}
				}
			}
			
		}
		return filteredTickets;
	}

	
	public Collection<Ticket> getAllActiveNonZonesTicketsByEvent(Integer eventId) {
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? AND ticketListingCrawlId != null", new Object[] {eventId, TicketStatus.ACTIVE});
	}
	
	public List<Ticket> getAllTmatTicketsById(Integer Id) {
		return find("FROM Ticket where id=?" ,new Object[]{Id});
	}
	
/*	@SuppressWarnings("unchecked")
	public List<Ticket> getAllInstantTickets(TicketDeliveryType ticketDeliveryType,TicketStatus ticketStatus) {
		return find("FROM Ticket WHERE eventId in (100898508, 100898509,  100898510, 100898434,100898426,100898428,100898504,100898505, 100898506, 100898433,100898742, 100898743, 100898580,100898476,100898567,100898727,100898576, 100898737, 100898577,100899460, 100899654, 100899655,100899656, 100899461, 100899462,100899470, 100899675, 100899676, 100896425, 100896572, 100896426, 100896573, 100896430, 100896576, 100896577, 100896444, 100896446, 100896447, 100895921,100895923,100896012, 100896032, 100896033, 100896034, 100896035, 100895996, 100895997, 100896007) AND ticketDeliveryType = ? AND ticketStatus = ? ", new Object[] {ticketDeliveryType, ticketStatus});
	}*/
	public Collection<Ticket> getAllTicketsMatchingUserOutlier(UserOutlier userOutlier) {
		
		String query = "(";
		if (userOutlier.getEventId() != null && userOutlier.getSiteIds().length > 0) {
			boolean firstElement = true;
			for(String siteId: userOutlier.getSiteIds()) {
				if (!firstElement) {
					query += ",";					
				} else {
					firstElement = false;
				}
				query += "'" + siteId + "'";					
			}
			
		}else {
			return null;
		}
		query += ")";
		try{
	
//		System.out.println("OUTLIERQUERY:FROM Ticket WHERE eventId = ? AND ticketStatus = ?  AND remainingQuantity >1 AND siteId in ()");
			return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ?  AND remainingQuantity >1 " +
				" AND siteId in "+query, new Object[] {userOutlier.getEventId(), TicketStatus.ACTIVE});
		}catch(Exception e){
			e.printStackTrace();
		}
return null;
	}

	public String getTicketDeliveryType(String exchangeName){
		
		Session session = null;
		String result="";
		try{
//			StringBuilder sb = new StringBuilder();
			session = getSession();
			Query query = session.createQuery("SELECT DISTINCT ticketDeliveryType FROM Ticket WHERE siteId = '"+exchangeName+"'");
			List<TicketDeliveryType> siteList = query.list();
			if(siteList!=null && !siteList.isEmpty()){
				for(TicketDeliveryType siteOb : siteList){
					if(siteOb != null){
						result+= siteOb.name()+",";
					}else{
						result+= "REGULAR,";
					}					
				}
			}else{
				return "REGULAR";
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return result.replaceAll(",$", "");		
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketForAutoCatsByEventId(Integer eventId) {
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus =? AND id not in (SELECT ticketId FROM SoldCategoryTicket)" +
				"AND siteId in ('ticketnetworkdirect','ticketevolution','stubhub') and ticketType = 'Regular'" +
				"", new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveInstantTicketForAutoCatByEventId(Integer eventId) {
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus =? AND id not in (SELECT ticketId FROM SoldCategoryTicket)" +
				"AND siteId in ('ticketnetworkdirect','ticketevolution','stubhub') and ticketType = 'Regular' AND ticketDeliveryType is not null " 
				, new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketForAutoCatSalesByEventId(Integer eventId) {
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus =? " +
				"AND ticketType = 'Regular'" +
				"", new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveInstantTicketForAutoCatSalesByEventId(Integer eventId) {
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus =? " +
				"AND ticketType = 'Regular' AND ticketDeliveryType is not null " 
				, new Object[]{eventId,TicketStatus.ACTIVE});
	}
	
	public Collection<Ticket> getAllActiveTicketsBySectionAndEventId(Integer eventId, String section){
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? And  section = ?" 
				, new Object[]{eventId, TicketStatus.ACTIVE, section});
	}
	
	public Collection<Ticket> getAllActiveTicketsByNormalizedSectionAndEventId(Integer eventId, String section){
		return find("FROM Ticket WHERE eventId = ? AND ticketStatus = ? And  normalizedSection = ?" 
				, new Object[]{eventId, TicketStatus.ACTIVE, section});
	}
}