package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.TnExchangeEventZoneFloorCapAudit;

public class TnExchangeEventZoneFloorCapAuditDAO extends HibernateDAO<Integer, TnExchangeEventZoneFloorCapAudit>
	implements com.admitone.tmat.dao.TnExchangeEventZoneFloorCapAuditDAO {

	public List<TnExchangeEventZoneFloorCapAudit> getZoneFloorCapAuditByEvntIdAndZoneId(Integer eId, Integer zoneId,Integer brokerId) {
		
		Session session = getSession();
		List<TnExchangeEventZoneFloorCapAudit> list = new ArrayList<TnExchangeEventZoneFloorCapAudit>();
		try{
			Query query = session.createQuery("FROM TnExchangeEventZoneFloorCapAudit where eventId = :eIds and zoneId = :zonecIds and brokerId=:brokerIdParam order by createdDate");
			query.setParameter("eIds", eId);
			query.setParameter("zonecIds", zoneId);
			query.setParameter("brokerIdParam", brokerId);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
	}
