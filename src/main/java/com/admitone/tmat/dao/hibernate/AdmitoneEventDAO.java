package com.admitone.tmat.dao.hibernate;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.admitone.tmat.data.AdmitoneEvent;

public class AdmitoneEventDAO extends HibernateDAO<Integer, AdmitoneEvent> implements com.admitone.tmat.dao.AdmitoneEventDAO {
	
	public List<AdmitoneEvent> getAllEventsByEventNamesAndVenueName(String[] eventNames,String venueName,List<String> parentCategoryList){
		
		
		 List<AdmitoneEvent> result = null;
		String hql="FROM AdmitoneEvent WHERE eventName in(:eventNameParam) AND venueName= :venueParam AND parentCategory in (:parentParam) ";
		 Query query = null;
		  org.hibernate.Session session = getSession();
		  try {   
		   query = session.createQuery(hql);
		   query.setParameterList("eventNameParam",Arrays.asList(eventNames));
		   query.setParameter("venueParam", venueName);
		   query.setParameterList("parentParam",parentCategoryList);
		   result = query.list();
		   
		   return result;
		   
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally {
			  session.close();
		  }
		  return null;
	}
	
	public List<AdmitoneEvent> getAllEventsByEventIds(List<Integer> eventIds){
		Session session  = getSession();
		try{
			Query query = session.createQuery("FROM AdmitoneEvent where eventId in (:Ids) ").setParameterList("Ids", eventIds);
			@SuppressWarnings("unchecked")
			List<AdmitoneEvent> list= query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
	}
	
	public List<String> getAllDistinctZoneEventsByVenueName(String venueName,List<String> parentCategoryList,Integer brokerId){
		List<String> result= new ArrayList<String>();
		Session session = getSession();
		try{
			SQLQuery query = null;
			if(brokerId == 4) {//for Manhattan broker we are using manhattan zones pricing product 
				query = session.createSQLQuery("select distinct ae.EventName from admitone_event ae with(nolock) inner join  event e with(nolock) on " +
						" e.admitone_id=ae.EventId inner join manhattan_zone_pricing_exchange_event zptn with(nolock) on zptn.event_id= e.id  " +
				  		" where ae.is_zone_event=1  and zptn.tn_broker_id=:brokerIdParam and ae.VenueName=:venueParam " +
				  		" AND ae.parent_category in (:parentParam) order by ae.EventName " );
			} else {
				query = session.createSQLQuery("select distinct ae.EventName from admitone_event ae with(nolock) inner join  event e with(nolock) on " +
						" e.admitone_id=ae.EventId inner join zone_pricing_exchange_event zptn with(nolock) on zptn.event_id= e.id  " +
				  		" where ae.is_zone_event=1  and zptn.tn_broker_id=:brokerIdParam and ae.VenueName=:venueParam " +
				  		" AND ae.parent_category in (:parentParam) order by ae.EventName " );
			}
			query.setInteger("brokerIdParam", brokerId);
			query.setString("venueParam", venueName);
			query.setParameterList("parentParam", parentCategoryList);
			result = query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			releaseSession(session);
		}
		return result;
	}
	
	public List<AdmitoneEvent> getAllZoneEventsByEventNamesAndVenueName(String[] eventNames,String venueName,List<String> parentCategoryList,Integer brokerId){
		Session session = getSession();
		List<AdmitoneEvent> events= new ArrayList<AdmitoneEvent>();
		try{
			SQLQuery query = null;
			if(brokerId == 4) {//for Manhattan broker we are using manhattan zones pricing product 
				query = session.createSQLQuery("select distinct ae.EventId,ae.EventName,ae.EventDate,ae.eventTime from " +
					 	" admitone_event ae with(nolock) inner join  event e with(nolock) on " +
					 	" e.admitone_id=ae.EventId inner join manhattan_zone_pricing_exchange_event zptn with(nolock) on zptn.event_id= e.id  " +
				  		" where ae.is_zone_event=1  and zptn.tn_broker_id=:brokerIdParam and ae.EventName  in (:eventNameParam) AND  ae.VenueName=:venueParam " +
				  		" AND ae.parent_category in (:parentParam) order by ae.EventDate,ae.eventTime  " );
			} else {
				query = session.createSQLQuery("select distinct ae.EventId,ae.EventName,ae.EventDate,ae.eventTime from " +
					 	" admitone_event ae with(nolock) inner join  event e with(nolock) on " +
					 	" e.admitone_id=ae.EventId inner join zone_pricing_exchange_event zptn with(nolock) on zptn.event_id= e.id  " +
				  		" where ae.is_zone_event=1  and zptn.tn_broker_id=:brokerIdParam and ae.EventName  in (:eventNameParam) AND  ae.VenueName=:venueParam " +
				  		" AND ae.parent_category in (:parentParam) order by ae.EventDate,ae.eventTime  " );
			}
			
			query.setInteger("brokerIdParam", brokerId);
			query.setParameterList("eventNameParam",Arrays.asList(eventNames));
			query.setParameter("venueParam", venueName);
			query.setParameterList("parentParam", parentCategoryList);
			List<Object[]> list = query.list();
			AdmitoneEvent event =null;
			DateFormat df = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
			
			for(Object[] obj:list){
				event = new AdmitoneEvent();
				event.setEventId((Integer)obj[0]);
				event.setEventName((String)obj[1]);
				event.setEventDate((Timestamp)obj[2]);
				event.setEventTime(new Time(((Timestamp)obj[3]).getTime()));
				events.add(event);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			releaseSession(session);
		}
		return events;
	}
	
	
	public List<String> getAllDistinctZonedVenues(Integer brokerId){
		List<String> result= new ArrayList<String>();
		Session session = getSession();
		try{
			SQLQuery query = null;
			if(brokerId == 4) {//for Manhattan broker we are using manhattan zones pricing product 
				query = session.createSQLQuery("select distinct ae.VenueName from admitone_event ae with(nolock) " +
						" inner join  event e with(nolock) on " +
						" e.admitone_id=ae.EventId inner join manhattan_zone_pricing_exchange_event zptn with(nolock) on zptn.event_id= e.id  " +
						" where ae.is_zone_event=1  and zptn.tn_broker_id=:brokerIdParam order by ae.VenueName " );
			} else {
				query = session.createSQLQuery("select distinct ae.VenueName from admitone_event ae with(nolock) " +
						" inner join  event e with(nolock) on " +
						" e.admitone_id=ae.EventId inner join zone_pricing_exchange_event zptn with(nolock) on zptn.event_id= e.id  " +
						" where ae.is_zone_event=1  and zptn.tn_broker_id=:brokerIdParam order by ae.VenueName " );	
			}
			
			
		/*	SQLQuery query = session.createSQLQuery("select distinct ae.VenueName from admitone_event ae with(nolock) " +
					" inner join  event e with(nolock) on " +
					 " e.admitone_id=ae.EventId " +
				  		" where ae.is_zone_event=1 order by ae.VenueName " );*/
			
			/*select distinct ae.VenueName from admitone_event ae   with(nolock)
			inner join  event e  with(nolock) on  e.admitone_id=ae.EventId 
			inner join zones_pricing_ticketnetwork_exchange_event zptn   with(nolock) on zptn.event_id= e.id  
			  where ae.is_zone_event=1  and e.broker_id=3 order by ae.VenueName*/
			  
			query.setInteger("brokerIdParam", brokerId);
			result = query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			releaseSession(session);
		}
		return result;
	}

	public List<String> getAllDistinctEventsByVenueName(String venueName,List<String> parentCategoryList){
		
	  List<String> result = null;
	  StringBuffer buffer = new StringBuffer ("select distinct eventName from AdmitoneEvent where venueName=:venueParam  " +
	  		"AND parentCategory in (:parentParam) order by eventName " );
	  Query query = null;
	  org.hibernate.Session session = getSession();
	  try {   
	   query = session.createQuery(buffer.toString());
	   query.setString("venueParam", venueName);
	   query.setParameterList("parentParam", parentCategoryList);
	   result = query.list();
	  } catch (Exception e) {		   
		  e.printStackTrace();
	  } finally {
		  session.close();
	  }
	 return result;
	}
	
	
	public List<AdmitoneEvent> getAllZoneEventsByVenueName(String venueName,List<String> parentCategoryList){
		
		
		 List<AdmitoneEvent> result = null;
		  StringBuffer buffer = new StringBuffer ("FROM AdmitoneEvent WHERE zoneEvent=1  AND venueName= :venueParam " +
		  		" AND parentCategory in (:parentParam) " );
		  Query query = null;
		  org.hibernate.Session session = getSession();
		  try {   
		   query = session.createQuery(buffer.toString());
		   query.setParameter("venueParam", venueName);
		   query.setParameterList("parentParam", parentCategoryList);
		   result = query.list();
		   return result;
		   
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally {
			  session.close();
		  }
		return null;
	}
	
	public List<Integer> getAllEventIdsByVenue(String venue,List<String> parentCategoryList){
		  List<Integer> result = null;
		  StringBuffer buffer = new StringBuffer ("SELECT e.eventId from AdmitoneEvent e where e.venueName =:venueParam " +
		  		"AND e.parentCategory in (:parentParam) " );
		  Query query = null;
		  org.hibernate.Session session = getSession();
		  try {   
		   query = session.createQuery(buffer.toString());
		   query.setString("venueParam", venue);
		   query.setParameterList("parentParam", parentCategoryList);
		   result = query.list();
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally {
			  session.close();
		  }
		  return result;
	}
	
	public List<Integer> getAllEventIdsByParentCategoryList(List<String> parentCategoryList){
		  List<Integer> result = null;
		  StringBuffer buffer = new StringBuffer ("SELECT e.eventId from AdmitoneEvent e where e.parentCategory in (:parentParam) " );
		  Query query = null;
		  org.hibernate.Session session = getSession();
		  try {   
		   query = session.createQuery(buffer.toString());
		   query.setParameterList("parentParam", parentCategoryList);
		   result = query.list();
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally {
			  session.close();
		  }
		  return result;
	}
	
	public List<String> getDistinctVenues(List<String> parentCategoryList){
		
		  List<String> result = null;
		  StringBuffer buffer = new StringBuffer ("SELECT DISTINCT e.venueName from AdmitoneEvent e where e.parentCategory in (:parentParam) " +
		  		" order by e.venueName" );
		  Query query = null;
		  org.hibernate.Session session = getSession();
		  try {   
		   query = session.createQuery(buffer.toString());
		   query.setParameterList("parentParam", parentCategoryList);
		   result = query.list();
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally {
			  session.close();
		  }
		  
		  return result;
	
}
	public Collection<Integer> getAllEventIds() {
		return find("SELECT id FROM AdmitoneEvent");
	}

	public Collection<AdmitoneEvent> getAllEventsNearDate(Date eventDate) {
		Calendar c = new GregorianCalendar();
		c.setTime(eventDate);
		//c.add(Calendar.DATE, 0);
		//Date endDate = new Date(c.getTime().getTime()); 
		//c.add(Calendar.DATE, 0);
		Date startDate = new Date(c.getTime().getTime()); 
		//return new ArrayList<AdmitoneEvent>();
		return find("FROM AdmitoneEvent WHERE EventDate = ? ORDER BY EventName", new Object[]{startDate});
	}

	public AdmitoneEvent getEventByEventId(Integer eventId) {
		return findSingle("FROM AdmitoneEvent WHERE eventId = ? ", new Object[]{eventId});
	}

	@Override
	public List<AdmitoneEvent> getEventsByName(String eventName) {
		String hql="FROM AdmitoneEvent WHERE eventName like ? ORDER BY eventName";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + eventName + "%");
		return find(hql, parameters.toArray());
	}
}
