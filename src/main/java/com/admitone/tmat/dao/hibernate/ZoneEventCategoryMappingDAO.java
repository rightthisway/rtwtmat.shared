package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.ZonesEventCategoryMapping;

public class ZoneEventCategoryMappingDAO extends HibernateDAO<Integer, ZonesEventCategoryMapping> implements com.admitone.tmat.dao.ZonesEventCategoryMappingDAO {

	public ZonesEventCategoryMapping getZonesEventCategoryMappingByEvent(Integer eventId) {
		// TODO Auto-generated method stub
		List<ZonesEventCategoryMapping> zonesEventCategoryMappingList = find("FROM ZonesEventCategoryMapping zecm WHERE zecm.eventId = ?", new Object[]{eventId});
		if(zonesEventCategoryMappingList != null && !zonesEventCategoryMappingList.isEmpty()){
			return zonesEventCategoryMappingList.get(0);
		}
		return null;
	}
	
	public void deleteZonesEventCategoryMappingByEvent(Integer eventId){
		bulkUpdate("DELETE FROM ZonesEventCategoryMapping zecm WHERE zecm.eventId = ?", new Object[]{eventId});
	}
}
