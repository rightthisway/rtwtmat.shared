package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.TnExchangeEventZoneGroupTicket;

public class TnExchangeEventZoneGroupTicketDAO extends HibernateDAO<Integer, TnExchangeEventZoneGroupTicket> implements 
com.admitone.tmat.dao.TnExchangeEventZoneGroupTicketDAO{

	public TnExchangeEventZoneGroupTicket getZoneTicketsByEventIdAndCategoryTicketGroupId(Integer eventId,Integer ticketGrpId){
		return findSingle("FROM TnExchangeEventZoneGroupTicket WHERE eventId=? AND categoryTicketGroupId=?", 
				new Object[]{eventId,ticketGrpId});
	}
	
	public List<TnExchangeEventZoneGroupTicket> getAllZonesAutoPricesedTicketsByEventId(ArrayList<Integer> eventIds){
		int i =0;
		int fromIndex=0;
		List<TnExchangeEventZoneGroupTicket> result = new ArrayList<TnExchangeEventZoneGroupTicket>();
	
		Map<Integer, List<Integer>> eventIdsMap = new HashMap<Integer, List<Integer>>();
		for(int j =0 ;j<=eventIds.size();j=j+2000){
			if(eventIds.size()<2000){
				eventIdsMap.put(i,eventIds.subList(fromIndex,eventIds.size()));
			}
			else{
				if(eventIds.size()>(fromIndex)){
					if(eventIds.size()>=(fromIndex+2000)){
						eventIdsMap.put(i,eventIds.subList(fromIndex, fromIndex+2000));
					}else{
						eventIdsMap.put(i,eventIds.subList(fromIndex,eventIds.size()));
						System.out.println(eventIds.size()-fromIndex+" message Size:"+"");
					}
						fromIndex += 2000;  
					
				}
					i++;
			}
		}
		Session session  = getSession();
		try{
			for(int k=0;k<eventIdsMap.size();k++){
				
				List<Integer> ids = eventIdsMap.get(k);					
				Query query = session.createQuery("FROM ZoneAutoPricedTicket where eventId in (:Ids) order by " +
						" eventDateTime ").setParameterList("Ids", ids);
				try{
					@SuppressWarnings("unchecked")
					List<TnExchangeEventZoneGroupTicket> list= query.list();					
					System.out.println("size of list 1 :" + list.size());		
					result.addAll(list);						
					System.out.println("size of result 1 :" + result.size());
				}catch (Exception e) {
					e.printStackTrace();
				}finally{
					query=null;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		System.out.println("size of : "+result.size());
	  return result;
	}

	
	public List<String> getDistinctVenues(){
		List<String> result = null;
		 
		  StringBuffer buffer = new StringBuffer ("SELECT DISTINCT e.venueName from ZoneAutoPricedTicket e  " );
		  Query query = null;
		  org.hibernate.Session session = getSession();
		try {   
		   query = session.createQuery(buffer.toString());
		   result = query.list();
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally  {
			  session.close();
		  }
		  
		  return result;
	}
	
	public List<TnExchangeEventZoneGroupTicket> getAllProcessedLisitngs(){
		return find("FROM TnExchangeEventZoneGroupTicket where posHit=1 and hitResult='Success' and pricingVersion=2 ");
	}
	public List<TnExchangeEventZoneGroupTicket> getAllFailedLisitngs(){
		return find("FROM TnExchangeEventZoneGroupTicket where hitResult='Failure' and pricingVersion=2 ");
	}
	
	public List<TnExchangeEventZoneGroupTicket> getAllProcessedLisitngsByDate(Date fromDate,Date toDate){
		try{
			return find("FROM TnExchangeEventZoneGroupTicket where posHit=1 and hitResult='Success' and pricingVersion=2 " +
					"and lastupdate between ? and ?",new Object[]{fromDate,toDate});
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
}
