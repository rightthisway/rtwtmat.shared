package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.DefaultAutoPricingAudit;

public class DefaultAutoPricingAuditDAO extends HibernateDAO<Integer,DefaultAutoPricingAudit> implements com.admitone.tmat.dao.DefaultAutoPricingAuditDAO{

	public List<DefaultAutoPricingAudit> getDefaultAutoPricingAuditByCategory(String exCategory) {
		
		Session session = getSession();
		List<DefaultAutoPricingAudit> list = new ArrayList<DefaultAutoPricingAudit>();
		try{
			Query query = session.createQuery("FROM DefaultAutoPricingAudit where category = :exCategory order by createdDate");
			query.setParameter("exCategory", exCategory);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
}