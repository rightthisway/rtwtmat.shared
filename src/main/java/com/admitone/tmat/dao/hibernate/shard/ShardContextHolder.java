package com.admitone.tmat.dao.hibernate.shard;

public class ShardContextHolder {
	private static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();
	
	public static void setShardContext(String shard) {
		contextHolder.set(shard);
	}

	public static String getShardContext() {
		return contextHolder.get();
	}

}
