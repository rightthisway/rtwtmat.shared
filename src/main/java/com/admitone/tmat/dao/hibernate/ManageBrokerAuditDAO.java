package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.ManageBrokerAudit;

public class ManageBrokerAuditDAO extends HibernateDAO<Integer,ManageBrokerAudit> implements com.admitone.tmat.dao.ManageBrokerAuditDAO{


	public List<ManageBrokerAudit> getManageBrokerAuditByEventId(Integer eId) {
			
			Session session = getSession();
			List<ManageBrokerAudit> list = new ArrayList<ManageBrokerAudit>();
			try{
				Query query = session.createQuery("FROM ManageBrokerAudit where eventId = :eIds order by createdDate");
				query.setParameter("eIds", eId);
				list= query.list();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				session.close();
			}				
			return list;
		}
	public void saveBulkAudits(List<ManageBrokerAudit> auditList) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (ManageBrokerAudit audit : auditList) {
			    session.save(audit);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
}
