package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.TnExchageEventZoneFloorCap;
import com.admitone.tmat.data.ZonesPricingFloorCap;

public class ZonesPricingFloorCapDAO extends HibernateDAO<Integer, ZonesPricingFloorCap>
implements com.admitone.tmat.dao.ZonesPricingFloorCapDAO{
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(ZonesPricingCategoryTicketDAO.class);
	public ZonesPricingFloorCap getEventZoneFloorCapByEventByZone(Integer eventId,String zone){
		try{
			return findSingle("FROM ZonesPricingFloorCap WHERE exchangeEvent.id=?  AND zone=?", 
					new Object[]{eventId,zone});
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	public List<ZonesPricingFloorCap> getAllZonesFloorCapByEventId(ArrayList<Integer> eventIds){
		int i =0;
		int fromIndex=0;
		List<ZonesPricingFloorCap> result = new ArrayList<ZonesPricingFloorCap>();
	
		Map<Integer, List<Integer>> eventIdsMap = new HashMap<Integer, List<Integer>>();
		for(int j =0 ;j<=eventIds.size();j=j+2000){
			if(eventIds.size()<2000){
				eventIdsMap.put(i,eventIds.subList(fromIndex,eventIds.size()));
			}
			else{
				if(eventIds.size()>(fromIndex)){
					if(eventIds.size()>=(fromIndex+2000)){
						eventIdsMap.put(i,eventIds.subList(fromIndex, fromIndex+2000));
					}else{
						eventIdsMap.put(i,eventIds.subList(fromIndex,eventIds.size()));
						logger.info(eventIds.size()-fromIndex+" message Size:"+"");
					}
						fromIndex += 2000;  
					
				}
					i++;
			}
		}
		Session session  = getSession();
		try{
			for(int k=0;k<eventIdsMap.size();k++){
				
				List<Integer> ids = eventIdsMap.get(k);					
				Query query = session.createQuery("FROM ZonesPricingFloorCap where exchangeEvent.id in (:Ids) ").setParameterList("Ids", ids);
				try{
					@SuppressWarnings("unchecked")
					List<ZonesPricingFloorCap> list= query.list();					
					result.addAll(list);						
				}catch (Exception e) {
					e.printStackTrace();
				}finally{
					query=null;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
	  return result;
	}
	
	public void deleteExpiredEventsByIds(List<Integer> eventIds){
		Session session  = getSession();
		try{
			Query query = session.createQuery("DELETE TnExchageEventZoneFloorCap where exchangeEvent.id in (:Ids) ").setParameterList("Ids", eventIds);
			query.executeUpdate();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
	}
	
	
	public void deleteFloorCapByVenue(String venueName,List<String> parentCategoryList) {
		List<Integer> eventIds = DAORegistry.getAdmitoneEventDAO().getAllEventIdsByVenue(venueName,parentCategoryList);
		if(null != eventIds && !eventIds.isEmpty()){
			Session session  = getSession();
			try{
				Query query = session.createQuery("DELETE TnExchageEventZoneFloorCap where exchangeEvent.id in (:Ids) ").setParameterList("Ids", eventIds);
				query.executeUpdate();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				session.close();
			}
		}
	}
	
	public List<TnExchageEventZoneFloorCap> getAllFloorCapByVenue(String venueName,List<String> parentCategoryList) {
		
		List<TnExchageEventZoneFloorCap> result = null;
		  StringBuffer buffer = new StringBuffer ("FROM TnExchageEventZoneFloorCap WHERE exchangeEvent.venueName =?= :venueParam " +
		  		" AND exchangeEvent.parentCategory in (:parentParam) " );
		  Query query = null;
		  org.hibernate.Session session = getSession();
		  try {   
		   query = session.createQuery(buffer.toString());
		   query.setParameter("venueParam", venueName);
		   query.setParameterList("parentParam", parentCategoryList);
		   result = query.list();
		   return result;
		   
		  } catch (Exception e) {		   
			  e.printStackTrace();
		  } finally {
			  session.close();
		  }
		return null;
	}
}
