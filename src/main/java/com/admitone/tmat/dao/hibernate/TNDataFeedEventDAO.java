package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.TNDataFeedEvent;

public class TNDataFeedEventDAO extends HibernateDAO<Integer, TNDataFeedEvent> implements com.admitone.tmat.dao.TNDataFeedEventDAO {
	@Override
	public Collection<TNDataFeedEvent> getAll() {
		return find("FROM TNDataFeedEvent WHERE status in('ACTIVE','EXISTING')");
	}
	
}
