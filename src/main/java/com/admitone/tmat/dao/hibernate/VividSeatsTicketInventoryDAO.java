package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.VividSeatsTicketInventory;
import com.admitone.tmat.utils.TextUtil;

public class VividSeatsTicketInventoryDAO extends HibernateDAO<Integer,VividSeatsTicketInventory> implements com.admitone.tmat.dao.VividSeatsTicketInventoryDAO{

	public List<VividSeatsTicketInventory> getAllTicketInventory(String eventName){
		
		if(null != eventName && !eventName.isEmpty()) {
			return find("FROM VividSeatsTicketInventory WHERE status=? AND eventName like ? ORDER BY eventDate, eventTime, section, row ASC", new Object[]{"ACTIVE","%" + TextUtil.removeExtraWhitespacesWithLike(eventName) + "%"});
		}else {
			return find("FROM VividSeatsTicketInventory WHERE status=? ORDER BY eventDate, eventTime, section, row ASC",new Object[]{"ACTIVE"});
		}
		
	}
	 
}
