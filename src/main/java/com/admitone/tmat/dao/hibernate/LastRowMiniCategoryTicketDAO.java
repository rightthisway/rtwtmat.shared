package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.LastRowMiniCategoryTicket;

public class LastRowMiniCategoryTicketDAO extends HibernateDAO<Integer, LastRowMiniCategoryTicket> implements com.admitone.tmat.dao.LastRowMiniCategoryTicketDAO {

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<LastRowMiniCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<LastRowMiniCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<LastRowMiniCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<LastRowMiniCategoryTicket> getAllActiveLastRowMiniCategoryTickets() {
		return find("FROM LastRowMiniCategoryTicket where status ='ACTIVE'");
	}
	
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsByAll(Integer eventId, String section, String row, String quantity) {
		String query = "FROM LastRowMiniCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND lastRow=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(quantity);								
		
			}
		
		return find(query, param.toArray());
	}
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsById(Integer Id) {
		return find("FROM LastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM LastRowMiniCategoryTicket where id=?" ,new Object[]{Id});
	}
	
}
