package com.admitone.tmat.dao.hibernate;

import java.util.Date;

import com.admitone.tmat.data.SeatWaveSeason;

public class SeatWaveSeasonDAO extends HibernateDAO<Integer, SeatWaveSeason> implements com.admitone.tmat.dao.SeatWaveSeasonDAO {
	public void deleteSeasonsNotUpdatedAfter(Date updatedDate) {
		getHibernateTemplate().bulkUpdate("DELETE FROM SeatWaveSeason WHERE updatedDate < ?", new Object[] {updatedDate});				
	}

}
