package com.admitone.tmat.dao.hibernate;


import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.TgCatsEbayListing;

public class TgCatsEbayListingDAO extends HibernateDAO<Integer, TgCatsEbayListing> implements com.admitone.tmat.dao.TgCatsEbayListingDAO {

	public TgCatsEbayListing get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<TgCatsEbayListing> list) {
		// TODO Auto-generated method stub

	}

	public Collection<TgCatsEbayListing> getAll() {
		return find("FROM TgCatsEbayListings");
	}

	public void saveOrUpdateAll(Collection<TgCatsEbayListing> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<TgCatsEbayListing> List) {
		// TODO Auto-generated method stub

	}

	public void delete(TgCatsEbayListing entity) {
		// TODO Auto-generated method stub

	}

	public List<TgCatsEbayListing> getAllActiveTgCatsEbayListings() {
		return find("FROM TgCatsEbayListing where status ='ACTIVE'");
	}
	
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByEventId(Integer eventId) {
		return find("FROM TgCatsEbayListing where eventId=?" ,new Object[]{eventId});
	}
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsById(Integer Id) {
		return find("FROM TgCatsEbayListing where Id=?" ,new Object[]{Id});
	}
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByTicketId(Integer Id) {
		return find("FROM TgCatsEbayListing where ticketId=?" ,new Object[]{Id});
	}
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByBaseTicketOneId(Integer Id) {
		return find("FROM TgCatsEbayListing where baseTicketOne=?" ,new Object[]{Id});
	}
	public List<TgCatsEbayListing> getAllTgCatsEbayListingsByBaseTicketTwoId(Integer Id) {
		return find("FROM TgCatsEbayListing where baseTicketTwo=?" ,new Object[]{Id});
	}
	
}
