package com.admitone.tmat.dao.hibernate.shard;

public class TicketDAO {/*extends HibernateDAO<Integer, Ticket> implements com.admitone.tmat.dao.TicketDAO {
	private final static int SHARD_COUNT = 2;
	
	private Map<Integer, Integer> crawlShardIdMap = new HashMap<Integer, Integer>();
	
	public Ticket get(Integer id) {
		for(int i = 0 ; i < SHARD_COUNT ; i++) {
			ShardContextHolder.setShardContext("" + i);
			Ticket ticket = super.get(id);
			if (ticket != null) {
				return ticket;
			}
		}
		return null;
	}
	
	protected List findOnShard(Integer shardId, String query, Object[] objects) {
		ShardContextHolder.setShardContext("" + shardId);
		return super.find(query, objects);
	}

	protected List findOnAllShards(String query, Object[] objects) {
		List<Ticket> tickets = new ArrayList<Ticket>();
		for(int i = 0 ; i < SHARD_COUNT ; i++) {
			ShardContextHolder.setShardContext("0");
			Collection foundTickets = super.find(query, objects);
			if (foundTickets != null) {
				tickets.addAll(foundTickets);
			}
		}
		
		return tickets;
	}
	
	protected void bulkUpdateOnShard(Integer shardId, String query, Object[] objects) {
		ShardContextHolder.setShardContext("" + shardId);
		super.bulkUpdate(query, objects);
	}

	protected void bulkUpdateOnAllShards(String query, Object[] objects) {
		for(int i = 0 ; i < SHARD_COUNT ; i++) {
			ShardContextHolder.setShardContext("" + i);
			super.bulkUpdate(query, objects);
		}
	}	
	
	protected void bulkUpdateOnAllShards(String query) {
		bulkUpdateOnAllShards(query, null);
	}
	
	protected Integer getShardByEventId(int eventId) {
		return (eventId % SHARD_COUNT);
	}
	
	// FIXME: handle the case when a editor edits the crawl and change the event
	protected Integer getShardByCrawlId(int crawlId) {
		Integer shardId = crawlShardIdMap.get(crawlId);
		if (shardId == null) {
			TicketListingCrawl crawl = DAORegistry.getTicketListingCrawlDAO().get(crawlId);
			shardId = getShardByEventId(crawl.getEventId());
			crawlShardIdMap.put(crawlId, shardId);
		}
		return shardId;
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEvent(int event_id) {
		return findOnShard(getShardByEventId(event_id), "FROM Ticket WHERE eventId = ?", new Object[]{event_id});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEventAndSiteId(int event_id, String siteId) {
		return findOnShard(getShardByEventId(event_id), "FROM Ticket WHERE eventId = ? AND siteId = ?", new Object[] {event_id, siteId});
	}

	@SuppressWarnings("unchecked")
	// only gets id, siteId, itemId, currentPrice, priceUpdateCount, insertion date, seller, status
	public Collection<Ticket> getAllSimpleTicketsByEventAndSiteId(int event_id, String siteId) {
		
		// gets 7 fields instead of 27 fields
		// List<Object[]> simpleTickets = find("SELECT id, itemId, insertionDate, currentPrice, priceUpdateCount, seller, ticketStatus FROM Ticket WHERE eventId = ? AND siteId = ?", new Object[] {event_id, siteId});
		
		// List<Ticket> tickets = new ArrayList<Ticket>();
		// for (Object[] simpleTicket: simpleTickets) {
		// 	Ticket ticket = new Ticket();
		// 	ticket.setId((Integer)simpleTicket[0]);
		// 	ticket.setItemId((String)simpleTicket[1]);
		// 	ticket.setInsertionDate((Date)simpleTicket[2]);
		// 	ticket.setCurrentPrice((Double)simpleTicket[3]);
		// 	ticket.setPriceUpdateCount((Integer)simpleTicket[4]);
		// 	ticket.setSeller((String)simpleTicket[5]);
		// 	ticket.setTicketStatus((TicketStatus)simpleTicket[6]);
		// 	tickets.add(ticket);			
		// }
		// return tickets;
		// 
		return null;
	}

	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEvent(int event_id, TicketStatus status) {
		return findOnShard(getShardByEventId(event_id), "FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEventUpdatedAfter(int event_id, Date afterDate) {
		return findOnShard(getShardByEventId(event_id), "FROM Ticket WHERE eventId = ? AND lastUpdate > ?", new Object[]{event_id, afterDate});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllBuyItNowTickets(TicketStatus status) {
		return findOnAllShards("FROM Ticket WHERE buyItNowPrice = 0 AND ticketStatus=?", new Object[]{status});
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllOutDatedExpiredTickets() {
		// return all the tickets which are expired and which were updated before their expired
		return findOnAllShards("FROM Ticket WHERE endDate < ? AND lastUpdate < endDate", new Object[]{new Date()});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllOutDatedExpiredTicketsFromCrawl(Integer crawlId) {
		// return all the tickets which are expired and which were updated before their expired
		return findOnAllShards("FROM Ticket WHERE ticketListingCrawlId = ? AND endDate < ? AND lastUpdate < endDate AND ticketStatus=?", new Object[] {crawlId, new Date(), TicketStatus.ACTIVE});		
	}

	public void updateStatusOfOutdatedExpiredTicketsFromCrawl(Integer crawlId, TicketStatus ticketStatus) {
		bulkUpdateOnAllShards("UPDATE Ticket SET ticketStatus=? WHERE ticketListingCrawlId=? AND endDate < ? and lastUpdate < endDate AND ticketStatus=?", new Object[] {ticketStatus, crawlId, new Date(), TicketStatus.ACTIVE});	
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsUpdatedBefore(Date date) {
		return findOnAllShards("FROM Ticket WHERE lastUpdate < ? AND ticketStatus=?", new Object[] {date, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsUpdatedBeforeFromCrawl(Integer crawlId, Date date) {
		return findOnAllShards("FROM Ticket WHERE ticketListingCrawlId = ? AND lastUpdate < ? AND ticketStatus=?", new Object[] {crawlId, date, TicketStatus.ACTIVE});		
	}
	
	@SuppressWarnings("unchecked")
	public void updateStatusOfTicketsUpdatedBeforeFromCrawl(Integer crawlId, Date date, TicketStatus ticketStatus) {
		bulkUpdateOnAllShards("UPDATE Ticket SET ticketStatus=? WHERE ticketListingCrawlId=? AND lastUpdate < ?", new Object[] {ticketStatus, crawlId, date});				
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId) {
		// return all the tickets which are expired and which were updated before their expired
		return findOnShard(getShardByEventId(eventId), "FROM Ticket WHERE eventId = ? AND ticketStatus != ?", new Object[] {eventId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId) {
		// return all the tickets which are expired and which were updated before their expired
		return findOnShard(getShardByEventId(eventId), "FROM Ticket WHERE eventId = ? AND ticketStatus = ?", new Object[] {eventId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveRegularTicketsByEvent(Integer eventId) {
		// return all the tickets which are expired and which were updated before their expired
		return findOnShard(getShardByEventId(eventId), "FROM Ticket WHERE eventId = ? AND ticketStatus = ? AND ticket_type='REGULAR' ORDER BY buyItNowPrice ASC", new Object[] {eventId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsBySiteId(String siteId) {
		// return all the tickets which are expired and which were updated before their expired
		return findOnAllShards("FROM Ticket WHERE siteId = ? AND ticketStatus = ?", new Object[] {siteId, TicketStatus.ACTIVE});		
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, Integer categoryId) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = findOnShard(getShardByEventId(eventId), "FROM Ticket WHERE eventId = ? AND ticketStatus != ?", new Object[] {eventId, TicketStatus.ACTIVE});
		return TicketUtil.filterCategory(categoryId, tickets);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId, List<Integer> categoryIds) {
		// return all the tickets which are expired and which were updated before their expired
		String query = "FROM Ticket WHERE eventId = ? AND ticketStatus != ?";
		
		if (categoryIds != null && !categoryIds.isEmpty()) {
			query += " AND categoryId IN (" + categoryIds.get(0);
			for (int i = 1 ; i < categoryIds.size() ; i++) {
				query += ", " + categoryIds.get(i);
			}
			query += ") ";
		}
		Collection<Ticket> tickets = findOnShard(getShardByEventId(eventId), query, new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategories(categoryIds, tickets);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId, Integer categoryId) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = findOnShard(getShardByEventId(eventId),"FROM Ticket WHERE eventId = ? AND ticketStatus = ?", new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategory(categoryId, tickets);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId, List<Integer> categoryIds) {
		// return all the tickets which are expired and which were updated before their expired
		String query = "FROM Ticket WHERE eventId = ? AND ticketStatus = ?";
		Collection<Ticket> tickets = findOnShard(getShardByEventId(eventId), query, new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategories(categoryIds, tickets);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllTicketsByEvent(Integer eventId, Integer categoryId, String day) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = findOnShard(getShardByEventId(eventId), "FROM Ticket WHERE eventId = ? AND ticketStatus = ? and ", new Object[] {eventId, TicketStatus.ACTIVE});		
		return TicketUtil.filterCategory(categoryId, tickets);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getUncategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = this.getAllCompletedTicketsByEvent(eventId);		
		return TicketUtil.filterUncategorized(tickets, catScheme);
	}
	

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getUncategorizedActiveTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = this.getAllActiveTicketsByEvent(eventId);
		return TicketUtil.filterUncategorized(tickets, catScheme);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getCategorizedActiveTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = this.getAllActiveTicketsByEvent(eventId);
		return TicketUtil.filterCategorized(tickets, catScheme);
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getCategorizedCompletedTicketsByEvent(Integer eventId, String catScheme) {
		// return all the tickets which are expired and which were updated before their expired
		Collection<Ticket> tickets = getAllCompletedTicketsByEvent(eventId);		
		return TicketUtil.filterCategorized(tickets, catScheme);
	}

	@SuppressWarnings("unchecked")
	public Collection<Object[]> getSectionsAndRowsFromEventId(Integer eventId) {
		return findOnShard(getShardByEventId(eventId), "SELECT section, row, COUNT(id) FROM Ticket WHERE eventId = ? AND ticketStatus = ? GROUP BY section, row", new Object[] {eventId, TicketStatus.ACTIVE});				
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getAllActiveTicketsByTour(Integer tourId) {
		return findOnAllShards("SELECT t FROM Ticket t, Event e WHERE t.eventId = e.id AND e.tourId=? AND t.ticketStatus = ?", new Object[] {tourId, TicketStatus.ACTIVE});		
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus) {
		Map<String, Object[]> siteStats = new HashMap<String, Object[]>();

		List<Object[]> results = findOnAllShards("SELECT siteId, COUNT(id), SUM(remainingQuantity) FROM Ticket WHERE eventId=? AND ticketStatus=? GROUP BY siteId", new Object[] {event_id, ticketStatus});
		for(Object[] result: results) {
			if (result == null) {
				continue;
			}
			
			String siteId = (String)result[0];
			long count = (Long)result[1];
			long sum = (Long)result[2];
			
			Object[] siteStat = siteStats.get(siteId);
			if (siteStat == null) {
				siteStats.put(siteId, new Object[]{siteId, count, sum});
			} else {
				siteStat[1] = new Long((Long)siteStat[1] + count); 
				siteStat[2] = new Long((Long)siteStat[2] + sum); 
			}
		}
		return new ArrayList(siteStats.values());
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getNonDuplicateTicketSiteStatsByEventId(int event_id, TicketStatus ticketStatus, RemoveDuplicatePolicy removeDuplicatePolicy) {
		List<Ticket> tickets = (List<Ticket>)findOnShard(getShardByEventId(event_id), "FROM Ticket WHERE eventId=? AND ticketStatus=?", new Object[] {event_id, ticketStatus});
		if (tickets == null) {
			return null;
		}
		
		Map<String, Integer> ticketCountBySite = new HashMap<String, Integer>();
		Map<String, Integer> ticketEntryCountBySite = new HashMap<String, Integer>();
		for (String siteId: Constants.getInstance().getSiteIds()) {
			ticketCountBySite.put(siteId, 0);
			ticketEntryCountBySite.put(siteId, 0);
		}
		
		tickets = TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
		for (Ticket ticket: tickets) {
			ticketCountBySite.put(ticket.getSiteId(), ticketCountBySite.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
			ticketEntryCountBySite.put(ticket.getSiteId(), ticketEntryCountBySite.get(ticket.getSiteId()) + 1);
		}
		
		List<Object[]> ticketSiteStats = new  ArrayList<Object[]>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			ticketSiteStats.add(new Object[]{siteId, new Long(ticketEntryCountBySite.get(siteId)), new Long(ticketCountBySite.get(siteId))});
		}
		return ticketSiteStats;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getTicketQuantityStatsByEventId(int event_id, TicketStatus ticketStatus) {
		return (List<Object[]>)findOnShard(getShardByEventId(event_id), "SELECT remainingQuantity, COUNT(id), SUM(remainingQuantity) FROM Ticket WHERE eventId=? AND ticketStatus=? GROUP BY remainingQuantity", new Object[] {event_id, ticketStatus});
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getNonDuplicatesTicketQuantityStatsByEventId(int event_id, TicketStatus ticketStatus) {
		List<Ticket> tickets = (List<Ticket>)findOnShard(getShardByEventId(event_id), "FROM Ticket WHERE eventId=? AND ticketStatus=?", new Object[] {event_id, ticketStatus});
		if (tickets == null) {
			return null;
		}

		Map<Integer, Integer> ticketCountBySite = new HashMap<Integer, Integer>();
		Map<Integer, Integer> ticketEntryCountBySite = new HashMap<Integer, Integer>();
		
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		for (Ticket ticket: tickets) {
			if (!ticketCountBySite.containsKey(ticket.getRemainingQuantity())) {
				ticketCountBySite.put(ticket.getRemainingQuantity(), ticket.getRemainingQuantity());
				ticketEntryCountBySite.put(ticket.getRemainingQuantity(), 1);				
			} else {
				ticketCountBySite.put(ticket.getRemainingQuantity(), ticketCountBySite.get(ticket.getRemainingQuantity()) + ticket.getRemainingQuantity());
				ticketEntryCountBySite.put(ticket.getRemainingQuantity(), ticketEntryCountBySite.get(ticket.getRemainingQuantity()) + 1);				
			}
		}
		
		List<Object[]> ticketSiteStats = new  ArrayList<Object[]>();
		for(Integer quantity: ticketEntryCountBySite.keySet()) {
			ticketSiteStats.add(new Object[]{quantity, new Long(ticketEntryCountBySite.get(quantity)), new Long(ticketCountBySite.get(quantity))});						
		}
		return ticketSiteStats;
	}

	public void deleteTicketsByCrawlId(int crawlId) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarksByCrawlId(crawlId);
		DAORegistry.getEbayInventoryGroupDAO().markAsDeletedByCrawlId(crawlId);
		bulkUpdateOnAllShards("DELETE FROM Ticket WHERE ticketListingCrawlId=?", new Object[] {crawlId});		
	}

	public void deleteTicketsByCrawlId(int crawlId, Date startDate, Date endDate) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarksByCrawlId(crawlId, startDate, endDate);
		bulkUpdateOnAllShards("DELETE FROM Ticket WHERE ticketListingCrawlId=? AND insertionDate >= ? AND insertionDate < ?", new Object[] {crawlId, startDate, endDate});		
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantities != null && !quantities.isEmpty()) {
			query += " AND remainingQuantity IN (" + quantities.get(0);
			for (int i = 1 ; i < quantities.size() ; i++) {
				query += ", " + quantities.get(i);
			}
			query += ") ";
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		// System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets;
		
		if (startDate != null) {
			tickets = findOnShard(getShardByEventId(eventId), query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			tickets = findOnShard(getShardByEventId(eventId), query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}
		
		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, List<Integer> categoryIds, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantities != null && !quantities.isEmpty()) {
			query += " AND remainingQuantity IN (" + quantities.get(0);
			for (int i = 1 ; i < quantities.size() ; i++) {
				query += ", " + quantities.get(i);
			}
			query += ") ";
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		// System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets;
		
		if (startDate != null) {
			tickets = findOnShard(getShardByEventId(eventId), query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			tickets = findOnShard(getShardByEventId(eventId), query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}

		if (categoryIds == null || categoryIds.get(0).equals(Category.ALL_ID)) {
		} else if (categoryIds.get(0).equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryIds.get(0).equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategories(categoryIds, tickets);
		}

		if (tickets == null) {
			//System.out.println("Returning null Stat");
			return null;
		}
		
		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}
		
		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantity != null && quantity > 0) {
			query += " AND remainingQuantity=" + quantity;
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//			// do nothing: we select all categories
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";			
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";			
//		} else {
//			query += " AND category_id=" + categoryId;
//		}

		// System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets;
		
		if (startDate != null) {
			tickets = findOnShard(getShardByEventId(eventId), query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			tickets = findOnShard(getShardByEventId(eventId), query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}
		
		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(Integer eventId, Integer categoryId, Integer lotSize, String catScheme) {
		return this.getEventCategoryLotSimpleTickets(eventId, categoryId, lotSize, false, catScheme);
	}
	
	@SuppressWarnings("unchecked")
	public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(Integer eventId, Integer categoryId, Integer lotSize, boolean removeA1Tix, String catScheme) {
		String query = "FROM Ticket WHERE eventId=? ";

		query += " AND ticket_status='ACTIVE' ";

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		//System.out.println("QUERY=" + query);
		
		Collection<Ticket> tickets;
		ArrayList<SimpleTicket> sTickets = new ArrayList<SimpleTicket>();
		
		tickets = findOnShard(getShardByEventId(eventId), query + " ORDER BY buyItNowPrice ASC", new Object[]{eventId});			

		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		//This method mutates the Collection tickets
		TicketUtil.removeDuplicateTickets(tickets);

		if (tickets.size() == 0) {
			//System.out.println("Returning null Stat");
			return null;
		}
	
		for(Ticket ticket : tickets){
			
			sTickets.add(new SimpleTicket(ticket, catScheme));
		}
		
		return sTickets;
	}
	
	public Collection<Ticket> getAllActiveTicketsByEventAndSiteIds(Integer eventId, String siteId) {
		return findOnShard(getShardByEventId(eventId), "FROM Ticket WHERE event_id=? and site_id=? and ticket_status='ACTIVE'", new Object[]{eventId, siteId});
	}

//	
//	@SuppressWarnings("unchecked")
//	public SimpleTicket getLowestEventCategoriesLotsSimpleTicket(Integer eventId, Collection<Integer> categoryIds, Collection<Integer> lotSizes, Collection<Integer> usedTixIds) {
//		String query = "FROM Ticket WHERE eventId=? ";
//
//		query += " AND ticket_status='ACTIVE'";
//		query += " AND ticket_type='REGULAR'";
//		
//		if (lotSizes != null && !lotSizes.isEmpty()) {
//			query += " AND lot_size IN(1,";
//			Iterator<Integer> lotIt = lotSizes.iterator();
//			while(lotIt.hasNext()){
//				Integer lotSize = lotIt.next();
//				query += lotSize ;
//				if(lotIt.hasNext()){
//					query +=  ",";
//				} else {
//					query +=  ")";
//				}
//			}	
//			
//			query += " AND remaining_quantity IN(";
//			lotIt = lotSizes.iterator();
//			while(lotIt.hasNext()){
//				Integer lotSize = lotIt.next();
//				query += lotSize ;
//				if(lotIt.hasNext()){
//					query +=  ",";
//				} else {
//					query +=  ")";
//				}
//			}	
//		}
//
//		if (categoryIds != null) {
//			if(categoryIds.size() > 0) {
//				query += " AND category_id IN(";
//				Iterator<Integer> catIt = categoryIds.iterator();
//				while(catIt.hasNext()){
//					Integer catId = catIt.next();
//					query += catId ;
//					if(catIt.hasNext()){
//						query +=  ",";
//					} else {
//						query +=  ")";
//					}
//				}	
//			}
//		}
//
//		if (usedTixIds != null) {
//			if(usedTixIds.size() > 0) {
//				query += " AND id NOT IN(";
//				Iterator<Integer> tixIt = usedTixIds.iterator();
//				while(tixIt.hasNext()){
//					Integer tixId = tixIt.next();
//					query += "'" + tixId ;
//					if(tixIt.hasNext()){
//						query +=  "',";
//					} else {
//						query +=  "')";
//					}
//				}
//			}
//		}
//
//		// System.out.println("QUERY=" + query);
//		
//		Collection<Ticket> tickets;
//		
//		tickets = find(query + " ORDER BY buyItNowPrice ASC", new Object[]{eventId});			
//
//		if (tickets.size() == 0) {
//			//System.out.println("Returning null Stat");
//			return null;
//		}
//		
//		tickets = TicketUtil.removeAdmitOneTickets(tickets);
//		if (tickets.size() == 0) {
//			//System.out.println("Returning null Stat");
//			return null;
//		}
//			
//		//This method mutates the Collection tickets
//		TicketUtil.removeDuplicateTickets(tickets);
//
//		if (tickets.size() == 0) {
//			//System.out.println("Returning null Stat");
//			return null;
//		}
//
//		//Because the tickets are ordered, the first ticket is the lowest priced ticket
//		return new SimpleTicket(tickets.iterator().next());
//	}

	@SuppressWarnings("unchecked")
	public int getTicketCount(TicketStatus status) {
		List list = findOnAllShards("SELECT count(id) FROM Ticket WHERE ticketStatus=?", new Object[]{status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByEvent(int event_id, TicketStatus status) {
		List list = findOnShard(getShardByEventId(event_id), "SELECT count(id) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketQuantitySumByEvent(int event_id, TicketStatus status) {
		List list = findOnShard(getShardByEventId(event_id), "SELECT sum(remainingQuantity) FROM Ticket WHERE eventId = ? AND ticketStatus=?", new Object[]{event_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByCrawl(int crawl_id, TicketStatus status) {
		List list = findOnAllShards("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ? AND ticketStatus=?", new Object[]{crawl_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByCrawl(int crawl_id, Date startDate, Date endDate) {
		List list = findOnAllShards("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ? AND insertionDate >= ? AND insertionDate < ?", new Object[]{crawl_id, startDate, endDate});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketQuantitySumByCrawl(int crawl_id, TicketStatus status) {
		List list = findOnAllShards("SELECT sum(remainingQuantity) FROM Ticket WHERE ticketListingCrawlId = ? AND ticketStatus=?", new Object[]{crawl_id, status});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketCountByCrawl(int crawl_id) {
		List list = findOnAllShards("SELECT count(id) FROM Ticket WHERE ticketListingCrawlId = ?", new Object[]{crawl_id});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	@SuppressWarnings("unchecked")
	public int getTicketQuantitySumByCrawl(int crawl_id) {
		List list = findOnAllShards("SELECT sum(remainingQuantity) FROM Ticket WHERE ticketListingCrawlId = ?", new Object[]{crawl_id});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();
	}

	public void deleteTicketsByEventId(Integer eventId) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarksWithEventId(eventId);
		bulkUpdateOnShard(getShardByEventId(eventId), "DELETE FROM Ticket WHERE eventId=? AND eventId IS NOT NULL", new Object[]{eventId});
	}
	
	public void enableTickets(Integer crawlerId) {
		bulkUpdateOnAllShards("UPDATE Ticket SET ticket_status='ACTIVE' WHERE crawl_id=? AND ticket_status='DISABLED'", new Object[]{crawlerId});		
	}

	public void disableTickets(Integer crawlerId) {
		bulkUpdateOnAllShards("UPDATE Ticket SET ticket_status='DISABLED' WHERE crawl_id=?", new Object[]{crawlerId});		
	}
	
	public void disableAndUnassignCrawlTickets(Integer crawlId) {
		bulkUpdateOnAllShards("UPDATE Ticket SET ticket_status='DISABLED', crawl_id=NULL WHERE crawl_id=?", new Object[]{crawlId});		
	}
	
	public void recomputeTicketRemainingQuantities() {
		bulkUpdateOnAllShards("UPDATE Ticket SET remaining_quantity=quantity - sold_quantity");				
	}	
	
	public void saveOrUpdateAllTicketsWithSameEventAndSite(final Collection<Ticket> tickets) {
		getCachedHibernateTemplate().execute(new HibernateCallback() {

			public Object doInHibernate(Session session)
					throws HibernateException, SQLException {
				if (tickets == null || tickets.isEmpty()) {
					return null;
				}
				
				Ticket firstTicket = tickets.iterator().next();
				
				Collection<Ticket> updatedTickets = new ArrayList<Ticket>();
				Collection<Ticket> newTickets = new ArrayList<Ticket>();
				Map<String, Ticket> existingTicketMap = new HashMap<String, Ticket>();

				try {
					session.beginTransaction();
					Query query = session.createQuery("FROM Ticket WHERE event_id=? and site_id=?");
					query.setInteger(0, firstTicket.getEventId());
					query.setString(1, firstTicket.getSiteId());
					
					ShardContextHolder.setShardContext("" + getShardByEventId(firstTicket.getEventId()));
					
					for(Object obj: query.list()) {
						Ticket ticket = (Ticket)obj;
						existingTicketMap.put(ticket.getTicketKey(), ticket);
					}
					
					for (Ticket ticket: tickets) {
						Ticket existingTicket = existingTicketMap.get(ticket.getTicketKey());
						if (existingTicket == null) {
							session.save(ticket);
						} else {
							existingTicket.setEventId(ticket.getEventId());
							existingTicket.setTicketType(ticket.getTicketType());
							existingTicket.setQuantity(ticket.getQuantity());
							existingTicket.setSoldQuantity(ticket.getSoldQuantity());
							existingTicket.setRemainingQuantity(ticket.getRemainingQuantity());
							existingTicket.setSection(ticket.getSection());
							existingTicket.setRow(ticket.getRow());
							existingTicket.setSeat(ticket.getSeat());
							existingTicket.setCurrentPrice(ticket.getCurrentPrice());
							existingTicket.setBuyItNowPrice(ticket.getBuyItNowPrice());
							existingTicket.setEndDate(ticket.getEndDate());
							existingTicket.setLastUpdate(ticket.getLastUpdate());
							existingTicket.setLotSize(ticket.getLotSize());
							//existingTicket.setNormalizedSection(ticket.getNormalizedSection());
							//existingTicket.setNormalizedRow(ticket.getNormalizedRow());
							//existingTicket.setNormalizedSeat(ticket.getNormalizedSeat());
							//existingTicket.setCategoryId(ticket.getCategoryId());
							//existingTicket.setAdjustedCurrentPrice(ticket.getAdjustedCurrentPrice());
							session.update(existingTicket);
						}
					}
					
					session.getTransaction().commit();
				} catch (Exception e) {
					session.getTransaction().rollback();
					throw new RuntimeException(e);
				}
				
				return null;
			}
			
		});
	}

	public Collection<Ticket> getAllTicketsByEventAndDate(Integer eventId, Date date) {
		return findOnShard(getShardByEventId(eventId), "FROM Ticket WHERE eventId=? AND date(lastUpdate)=date(?) ORDER BY currentPrice ASC", new Object[]{eventId, date});
	}

	public void deleteById(Integer ticketId) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarks(ticketId);
		
		for(int i = 0 ; i < SHARD_COUNT ; i++) {
			ShardContextHolder.setShardContext("" + i);			
			super.deleteById(ticketId);
		}
	}
	
	public void delete(Ticket ticket) {
		DAORegistry.getBookmarkDAO().deleteTicketBookmarks(ticket.getId());
		getShardByEventId(ticket.getEventId());
		ShardContextHolder.setShardContext("" + getShardByEventId(ticket.getEventId()));

		super.delete(ticket);
	}
	
	public Collection<Ticket> getAllTicketsMatchingUserAlert(UserAlert userAlert) {
		return null;
	}

	public Collection<Ticket> getAllTicketsMatchingUserAlert(UserAlert userAlert, Date lastUpdate, TicketStatus ticketStatus) {
		return null;
	}

	
	*/

}