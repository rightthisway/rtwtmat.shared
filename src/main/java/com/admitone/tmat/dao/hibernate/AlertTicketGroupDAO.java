package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.AlertTicketGroup;

public class AlertTicketGroupDAO extends HibernateDAO<Integer, AlertTicketGroup> implements com.admitone.tmat.dao.AlertTicketGroupDAO {

	public List<AlertTicketGroup> getAlertTicketGroupsByAlertId(Integer alertId) {
		return find("FROM AlertTicketGroup WHERE alertId= ? ", new Object[]{alertId});
	}
	
}
