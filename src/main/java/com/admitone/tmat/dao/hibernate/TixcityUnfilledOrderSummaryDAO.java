package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.TixcityUnfilledOrderSummary;

public class TixcityUnfilledOrderSummaryDAO extends HibernateDAO<Integer,TixcityUnfilledOrderSummary> implements com.admitone.tmat.dao.TixcityUnfilledOrderSummaryDAO{

	
	public TixcityUnfilledOrderSummary getLastWeekUnfilledOrderSummary() {
		try{
			return findSingle("FROM TixcityUnfilledOrderSummary order by summaryDate desc", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public List<TixcityUnfilledOrderSummary> getAllUnfilldedOrderSummary() {
		return find("FROM TixcityUnfilledOrderSummary order by summaryDate desc", new Object[]{});
	}
}
