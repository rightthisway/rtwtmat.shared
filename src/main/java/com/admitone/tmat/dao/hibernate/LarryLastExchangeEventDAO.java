package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;

import com.admitone.tmat.data.LarryLastExchangeEvent;


public class LarryLastExchangeEventDAO  extends HibernateDAO<Integer, LarryLastExchangeEvent> implements com.admitone.tmat.dao.LarryLastExchangeEventDAO{

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByEventId(Integer eventId) {
		return find("FROM LarryLastExchangeEvent WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByVenueId(Integer venueId) {
		return find("SELECT le FROM LarryLastExchangeEvent le ,Event e WHERE e.id = le.eventId And e.venueId=?", new Object[]{venueId});
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByArtistId(Integer artistId) {
		return find("SELECT le FROM LarryLastExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId=?", new Object[]{artistId});
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM le FROM LarryLastExchangeEvent le ,Event e WHERE e.id = le.eventId And e.artistId in (SELECT id FROM Artist WHERE grandChildCategoryId = ?", new Object[]{grandChildCategoryId});
	}

	public Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsEligibleForUpdate(Long minute) {
		String sql =" select distinct le.id as id,le.event_id as eventId, le.exposure as exposure, " +
					" le.rpt_factor as rptFactor,le.price_breakup as priceBreakup,le.lower_markup as lowerMarkup, " +
					" le.upper_markup as upperMarkup,le.lower_shipping_fees as lowerShippingFees, " +
					" le.upper_shipping_fees as upperShippingFees,le.shipping_method as shippingMethod, " +
					" le.near_term_display_option as nearTermDisplayOption,le.allow_section_range as allowSectionRange, " +
					" le.shipping_days as shippingDays,le.vivid_broker_id as vividBrokerId,le.tn_broker_id as ticketNetworkBrokerId,le.scorebig_broker_id as scoreBigBrokerId,le.is_zone as zone " + 
					" from larrylast_exchange_event le " + 
					" INNER join ticket_listing_crawl tc on tc.event_id = le.event_id " +
					" INNER JOIN event e on e.id = le.event_id " +
					" INNER JOIN venue_category vc on vc.id = e.venue_category_id " +
					" WHERE le.status = 'ACTIVE'  " +
					" AND tc.site_id in ('ticketnetworkdirect','stubhub','ticketevolution') " +
					" AND ((DATEDIFF(MINUTE,tc.end_crawl,GETDATE())<=  " + minute +
					" OR DATEDIFF(MINUTE,vc.last_updated_date,GETDATE())<=  " + minute +
					" OR (DATEDIFF(MINUTE,le.last_updated_date,GETDATE())<= " + minute + ")" +
					" OR (DATEDIFF(MINUTE,e.last_update,GETDATE())<= " + minute + "))) " ;
		Session session=null;
		try{
//			session = getSessionFactory().openSession();
			session = getSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			sqlQuery.addScalar("id",Hibernate.INTEGER);
			sqlQuery.addScalar("exposure",Hibernate.STRING);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("rptFactor",Hibernate.DOUBLE);
			sqlQuery.addScalar("shippingMethod",Hibernate.INTEGER);
			sqlQuery.addScalar("nearTermDisplayOption",Hibernate.INTEGER);
			
			sqlQuery.addScalar("priceBreakup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperMarkup",Hibernate.DOUBLE);
			sqlQuery.addScalar("lowerShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("upperShippingFees",Hibernate.DOUBLE);
			sqlQuery.addScalar("vividBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("scoreBigBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("ticketNetworkBrokerId",Hibernate.INTEGER);
			sqlQuery.addScalar("allowSectionRange",Hibernate.BOOLEAN);
			sqlQuery.addScalar("zone",Hibernate.BOOLEAN);
			sqlQuery.addScalar("shippingDays",Hibernate.INTEGER);
			List<LarryLastExchangeEvent> exchangeEvents =  sqlQuery.setResultTransformer(Transformers.aliasToBean(LarryLastExchangeEvent.class)).list();
			return exchangeEvents;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
	}

	public LarryLastExchangeEvent getLarryLastExchangeEventsByEventId(Integer eventId) {
		List<LarryLastExchangeEvent> list = find("FROM LarryLastExchangeEvent WHERE eventId =?", new Object[]{eventId});
		if(list==null || list.isEmpty()){
			return null;
		}
		return list.get(0);
	}

}