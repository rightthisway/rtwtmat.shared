package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.HarvestTicket;

public class HarvestTicketDAO extends HibernateDAO<Integer, HarvestTicket> implements com.admitone.tmat.dao.HarvestTicketDAO {
	

	@Override
	public void deleteById(Integer id) {
		//do nothing because we don't want to delete archived Tickets
		return;
	}
	
	@Override
	public void delete(HarvestTicket Ticket) {
		//do nothing because we don't want to delete archived Tickets
		return;
	}

	@Override
	public void update(HarvestTicket Ticket) {
		//do nothing because we don't want to update archived Tickets
		return;
	}

	public Collection<HarvestTicket> getTicketsForEvent(Integer eventId) {
			return find("FROM HarvestTicket WHERE event_id = ? ORDER BY insertion_date", new Object[]{eventId});								
	}


}
