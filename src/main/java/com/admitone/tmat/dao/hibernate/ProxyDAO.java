package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.Proxy;

public class ProxyDAO extends HibernateDAO<Integer, Proxy> implements com.admitone.tmat.dao.ProxyDAO{

	public List<Proxy> getAllProxy() {
		return find("FROM Proxy WHERE isWorking = ?" , new Object[]{true});
	}
	
}
