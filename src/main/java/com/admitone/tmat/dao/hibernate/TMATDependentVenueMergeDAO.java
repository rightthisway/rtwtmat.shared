package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.TMATDependentVenueMerge;

public class TMATDependentVenueMergeDAO extends HibernateDAO<Integer, TMATDependentVenueMerge> implements
		com.admitone.tmat.dao.TMATDependentVenueMergeDAO {
	public List<TMATDependentVenueMerge> getTMATDependentVenueMergeByVenueIdAndMergeVenueId(Integer venueId,Integer toMergedVenueId){
		return find("FROM TMATDependentVenueMerge WHERE venueId= ? AND toMergedVenueId = ? ",new Object[]{venueId,toMergedVenueId});
	}
}
