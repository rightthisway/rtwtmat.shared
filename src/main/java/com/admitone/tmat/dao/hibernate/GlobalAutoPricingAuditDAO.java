package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.GlobalAutoPricingAudit;

public class GlobalAutoPricingAuditDAO extends HibernateDAO<Integer,GlobalAutoPricingAudit> implements com.admitone.tmat.dao.GlobalAutoPricingAuditDAO{


	public List<GlobalAutoPricingAudit> getAllAuditsOrderByCreatedDate() {
			
			Session session = getSession();
			List<GlobalAutoPricingAudit> list = new ArrayList<GlobalAutoPricingAudit>();
			try{
				Query query = session.createQuery("FROM GlobalAutoPricingAudit order by createdDate");
				list= query.list();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				session.close();
			}				
			return list;
		}
}
