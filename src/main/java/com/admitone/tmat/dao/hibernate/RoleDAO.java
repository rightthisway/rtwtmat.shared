package com.admitone.tmat.dao.hibernate;

import com.admitone.tmat.data.Role;

public class RoleDAO extends HibernateDAO<Integer, Role> implements com.admitone.tmat.dao.RoleDAO {

	@Override
	public Role getRoleByName(String name) {
		return findSingle("FROM Role WHERE name = ?", new Object[]{name});
	}
}
