package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.ConcertSynonym;

public class ConcertSynonymDAO  extends HibernateDAO<String, ConcertSynonym> implements com.admitone.tmat.dao.ConcertSynonymDAO{

	public Collection<ConcertSynonym> getValuesByName(String name) {
		return (find ("from ConcertSynonym where name = ?" , new Object[]{name}));
	}

	
}
