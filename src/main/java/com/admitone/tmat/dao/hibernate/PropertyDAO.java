package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.admitone.tmat.data.Property;
import com.admitone.tmat.web.Constants;

public class PropertyDAO extends HibernateDAO<String, Property> implements com.admitone.tmat.dao.PropertyDAO {
	
	@Override
	protected void initDao() throws Exception {
		Constants.getInstance().setAdmit1NotePattern(get("admit1.note.pattern").getValue());
	}
	
	public Map<String, String> getPropertyMap() {
		Collection<Property> properties = getAll();
		Map<String, String> propertyMap = new HashMap<String, String>();;
		for (Property property: properties) {
			propertyMap.put(property.getName(), property.getValue());
		}
		return propertyMap;
	}
	
	public Collection<Property> getPropertyByPrefix(String prefix) {
		return find("FROM Property WHERE name LIKE '" + prefix + "%'");
	}
	public int addStubhubMinuteHitCount(int minuteCount) throws Exception {
		
		Session session=null;
		int count=0;
		try{
			session = getSessionFactory().openSession();
			SQLQuery sqlQuery = session.createSQLQuery("update property set value=CAST(value AS int)+"+minuteCount+" where name='stubhub.hit.minute.count'");
			count = sqlQuery.executeUpdate();
			session.close();
			
		} catch (Exception e) {
			if(session!= null && session.isOpen()) {
				session.close();
			}
			e.printStackTrace();
		} 
		return count;
	}
}
