package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import com.admitone.tmat.data.QuoteManualTicket;

public class QuoteManualTicketDAO extends HibernateDAO<Integer, QuoteManualTicket> implements com.admitone.tmat.dao.QuoteManualTicketDAO {
	
	
	public QuoteManualTicket getQuoteManualTicket(Integer eventId,Integer quoteId) {
		return findSingle("FROM QuoteManualTicket WHERE event.id=? AND quote.id=? ", new Object[]{eventId, quoteId});
	}
	
	
	public Collection<QuoteManualTicket> getAllQuoteManualTicket(Integer eventId,Integer quoteId) {
		return find("FROM QuoteManualTicket WHERE event.id=? AND quote.id=? ", new Object[]{eventId, quoteId});
	}
	
	
	public void deleteQuoteManualTicketByQuoteId(Integer quoteId) {
		bulkUpdate("DELETE FROM QuoteManualTicket WHERE quote.id=?", new Object[]{quoteId});
		
	}
	
	
	public List<QuoteManualTicket> getAllQuoteManualTicketByQuoteID(List<Integer> ids) {
		
	  Query query = getSession().createQuery("FROM QuoteManualTicket WHERE quote.id in (:Ids) order by date").setParameterList("Ids", ids);   
	  @SuppressWarnings("unchecked")
	  List<QuoteManualTicket> list= query.list();
	  return list;
	}

	
}
