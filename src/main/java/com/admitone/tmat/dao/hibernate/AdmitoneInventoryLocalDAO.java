package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.AdmitoneInventoryLocal;

public class AdmitoneInventoryLocalDAO extends HibernateDAO<Integer, AdmitoneInventoryLocal> implements com.admitone.tmat.dao.AdmitoneInventoryLocalDAO {

	@Deprecated
	public Collection<AdmitoneInventoryLocal> getAllInventoryLocalByDate(Date eventDate) {
		return new ArrayList<AdmitoneInventoryLocal>();
		//return find("FROM AdmitoneInventoryLocal WHERE eventId=?", new Object[]{eventId});
	}
	
	public Collection<AdmitoneInventoryLocal> getAllInventoryLocalByAdmitOneEventId(Integer eventId) {
		return find("FROM AdmitoneInventoryLocal WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<AdmitoneInventoryLocal> getAllInventoryLocalByEventId(Integer eventId) {
		return find("SELECT ae FROM AdmitoneInventoryLocal ae, Event e WHERE e.id=? AND e.admitoneId=ae.eventId", new Object[]{eventId});
	}

}
