package com.admitone.tmat.dao.hibernate;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.AuditTnApiCallLimit;

public class AuditTnApiCallLimitDAO extends HibernateDAO<Integer, AuditTnApiCallLimit> implements com.admitone.tmat.dao.AuditTnApiCallLimitDAO {

	public List<AuditTnApiCallLimit> getByProjectName(String project){
		
		Session session = null;
		try{
			session = getSession();
			Query query = session.createQuery("FROM AuditTnApiCallLimit WHERE project = :project order by createDate desc").setParameter("project", project);
			return query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
	}	
}
