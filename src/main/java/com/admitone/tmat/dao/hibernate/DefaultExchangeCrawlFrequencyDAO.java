package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.DefaultExchangeCrawlFrequency;

public class DefaultExchangeCrawlFrequencyDAO extends HibernateDAO<Integer, DefaultExchangeCrawlFrequency> implements com.admitone.tmat.dao.DefaultExchangeCrawlFrequencyDAO{

	public List<DefaultExchangeCrawlFrequency> getAllDefaultExchangeCrawlFrequency() {
		return find("FROM DefaultExchangeCrawlFrequency");
	}
	
	public DefaultExchangeCrawlFrequency getDefaultExchangeCrawlFrequencyByExchange(String exchange) {
		return findSingle("FROM DefaultExchangeCrawlFrequency WHERE exchange= ? ", new Object[]{exchange});
	}


}
