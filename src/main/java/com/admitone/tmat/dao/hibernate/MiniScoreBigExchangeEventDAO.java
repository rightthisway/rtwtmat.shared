package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.MiniScoreBigExchangeEvent;

public class MiniScoreBigExchangeEventDAO extends HibernateDAO<Integer,MiniScoreBigExchangeEvent> implements com.admitone.tmat.dao.MiniScoreBigExchangeEventDAO{

	public Collection<MiniScoreBigExchangeEvent> getAllMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniScoreBigExchangeEvent> result = new ArrayList<MiniScoreBigExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniScoreBigExchangeEvent> temp = find("FROM MiniScoreBigExchangeEvent WHERE event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public Collection<MiniScoreBigExchangeEvent> getAllActiveMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniScoreBigExchangeEvent> result = new ArrayList<MiniScoreBigExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniScoreBigExchangeEvent> temp = find("FROM MiniScoreBigExchangeEvent WHERE status='ACTIVE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection<MiniScoreBigExchangeEvent> getAllDeletedMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniScoreBigExchangeEvent> result = new ArrayList<MiniScoreBigExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniScoreBigExchangeEvent> temp = find("FROM MiniScoreBigExchangeEvent WHERE status='DELETE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAllMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("DELETE FROM MiniScoreBigExchangeEvent WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public void updateAllMiniScoreBigExchangeEventAsDeleteByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("UPDATE MiniScoreBigExchangeEvent SET status='DELETE' WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public List<MiniScoreBigExchangeEvent> getAllActiveMiniScoreBigExchangeEvent() {
		try{
			return find("FROM MiniScoreBigExchangeEvent WHERE event.id IN(SELECT DISTINCT eventId from TGCatsCategoryTicket where status ='ACTIVE')", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	public void saveBulkEvents(List<MiniScoreBigExchangeEvent> tnEvents) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (MiniScoreBigExchangeEvent event : tnEvents) {
			    session.save(event);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
	public void updateExposure(String mini_exposure_scorebig)
	{
		String query="update MiniScoreBigExchangeEvent set exposure="+"'"+mini_exposure_scorebig+"'";
		bulkUpdate(query);
	}
	public void updateRptFactor(String rpt_scorebig)
	{
		String query="update MiniScoreBigExchangeEvent set rptFactor="+"'"+rpt_scorebig+"'";
	    bulkUpdate(query);
	}
	public void updatePriceBreakup(String pricebreakup_scorebig)
	{
		String query="update MiniScoreBigExchangeEvent set priceBreakup="+"'"+pricebreakup_scorebig+"'";
	    bulkUpdate(query);
	}
	public void updateUpperMarkup(String upper_markpu_scorebig)
	{
		String query="update MiniScoreBigExchangeEvent set upperMarkup="+"'"+upper_markpu_scorebig+"'";
	    bulkUpdate(query);
	}
	public void updateLowerMarkup(String lower_markpu_scorebig)
	{
		String query="update MiniScoreBigExchangeEvent set lowerMarkup="+"'"+lower_markpu_scorebig+"'";
	    bulkUpdate(query);
	}
	public void updateUpperShippingFees(String upper_shippingfees_scorebig)
	{
		String query="update MiniScoreBigExchangeEvent set upperShippingFees="+"'"+upper_shippingfees_scorebig+"'";
	    bulkUpdate(query);
	}
	public void updateLowerShippingFees(String lower_shippingfees_scorebig)
	{
		String query="update MiniScoreBigExchangeEvent set lowerShippingFees="+"'"+lower_shippingfees_scorebig+"'";
	    bulkUpdate(query);
	}
public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) {
	Session session=null;
	try {
		String query = "";
		
		if(globalAudit.getMiniExposure() != null) {
			query = query + ",ee.exposure='"+globalAudit.getMiniExposure()+"'";
		}
		if(globalAudit.getShippingMethod() != null) {
			query = query + ",ee.shipping_method='"+globalAudit.getShippingMethod()+"'";
		}
		if(globalAudit.getNearTermDisplayOption() != null) {
			query = query + ",ee.near_term_display_option='"+globalAudit.getNearTermDisplayOption()+"'";
		}
		if(globalAudit.getRptFactor() != null) {
			query = query + ",ee.rpt_factor="+globalAudit.getRptFactor();
		}
		if(globalAudit.getPriceBreakup() != null) {
			query = query + ",ee.price_breakup="+globalAudit.getPriceBreakup();
		}
		if(globalAudit.getLowerMarkup() != null) {
			query = query + ",ee.lower_markup="+globalAudit.getLowerMarkup();
		}
		if(globalAudit.getUpperMarkup() != null) {
			query = query + ",ee.upper_markup="+globalAudit.getUpperMarkup();
		}
		if(globalAudit.getLowerShippingFees() != null) {
			query = query + ",ee.lower_shipping_fees="+globalAudit.getLowerShippingFees();
		}
		if(globalAudit.getUpperShippingFees() != null) {
			query = query + ",ee.upper_shipping_fees="+globalAudit.getUpperShippingFees();
		}
		query = "update ee set " + query.substring(1);
		
		query = query + " from mini_scorebig_exchange_event ee " +
		" inner join event e on e.id=ee.event_id " +
		" inner join tour t on t.id=e.tour_id" +
		" inner join grand_child_tour_category gc on gc.id=t.grand_child_category_id" +
		" inner join child_tour_category cc on cc.id=gc.child_category_id" +
		" inner join tour_category tc on tc.id=cc.tour_category_id " +
		" where ee.status='ACTIVE' and tc.name in("+parentType+")";
		
		session = getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery(query);
		sqlQuery.executeUpdate();
		
	} catch (Exception e) {
		e.printStackTrace();
	} finally{
		session.close();
	}
	}

}
