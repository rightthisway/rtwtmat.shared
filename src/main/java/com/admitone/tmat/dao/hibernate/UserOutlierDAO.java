package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.UserOutlier;
import com.admitone.tmat.enums.UserOutlierStatus;

public class UserOutlierDAO extends HibernateDAO<Integer, UserOutlier> implements com.admitone.tmat.dao.UserOutlierDAO {
	
	public Collection<UserOutlier> getOutliersByUsernameAndArtistId(String username, Integer artistId, UserOutlierStatus userOutlierStatus) {
		Collection<Object> parameters = new ArrayList<Object>();
		String query = "FROM UserOutlier WHERE 1=1";

		if (userOutlierStatus != null) {
			query += " AND userOutlierStatus = ?";
			parameters.add(userOutlierStatus);
		} else {
			query += " AND (userOutlierStatus = ? OR userOutlierStatus = ?)";
			parameters.add(UserOutlierStatus.ACTIVE);
			parameters.add(UserOutlierStatus.DISABLED);			
		}

		if (artistId != null) {
			Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			if (events == null || events.isEmpty()) {
				return null;
			}
			
			String eventIds = "";		
			for(Event event: events) {
				if (!eventIds.isEmpty()) {
					eventIds += ",";				
				}
				eventIds += event.getId();
			}
			
			query += " AND event_id IN (" + eventIds + ")";
		}

		if (username != null) {
			query += " AND username=?";
			parameters.add(username);
		}
		
		query += " order by creationDate ";

		return find(query, parameters.toArray(new Object[parameters.size()]));
	}
	
	
	public Collection<UserOutlier> getOutliersByUsernameAndEventId(String username, Integer eventId, UserOutlierStatus userOutlierStatus) {
		Collection<Object> parameters = new ArrayList<Object>();
		String query = "FROM UserOutlier WHERE 1=1";

		if (userOutlierStatus != null) {
			query += " AND userOutlierStatus = ?";
			parameters.add(userOutlierStatus);
		} else {
			query += " AND (userOutlierStatus = ? OR userOutlierStatus = ?)";
			parameters.add(UserOutlierStatus.ACTIVE);
			parameters.add(UserOutlierStatus.DISABLED);			
		}
		
		if (eventId != null) {
			query += " AND eventId=?";
			parameters.add(eventId);
		}
	
		if (username != null) {
			query += " AND username=?";
			parameters.add(username);
		}

		query += " order by creationDate ";				
			
	
		return find(query, parameters.toArray(new Object[parameters.size()]));
	}

	
	public Collection<UserOutlier> getOutliersByMarketMakerAndArtistId(String username, Integer artistId, UserOutlierStatus userOutlierStatus) {
		String marketMakerQuery = "SELECT a FROM UserOutlier a, MarketMakerEventTracking t WHERE a.eventId = t.eventId";
		Collection<Object> marketMakerParameters = new ArrayList<Object>();

		String defaultMarketMakerQuery = "FROM UserOutlier a WHERE 1=1";
		Collection<Object> defaultMarketMakerParameters = new ArrayList<Object>();

		if (userOutlierStatus != null) {
			marketMakerQuery += " AND a.userOutlierStatus=?";	
			marketMakerParameters.add(userOutlierStatus);
			defaultMarketMakerQuery += " AND a.userOutlierStatus=?";	
			defaultMarketMakerParameters.add(userOutlierStatus);
		} else {
			marketMakerQuery += " AND (a.userOutlierStatus = ? OR a.userOutlierStatus = ?)";
			marketMakerParameters.add(UserOutlierStatus.ACTIVE);
			marketMakerParameters.add(UserOutlierStatus.DISABLED);
			
			defaultMarketMakerQuery += " AND (a.userOutlierStatus = ? OR a.userOutlierStatus = ?)";
			defaultMarketMakerParameters.add(UserOutlierStatus.ACTIVE);
			defaultMarketMakerParameters.add(UserOutlierStatus.DISABLED);
		}

		if (artistId != null) {
			Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			if (events == null || events.isEmpty()) {
				return null;
			}
			
			String eventIds = "";		
			for(Event event: events) {
				if (!eventIds.isEmpty()) {
					eventIds += ",";				
				}
				eventIds += event.getId();
			}
			
			marketMakerQuery += " AND a.eventId IN (" + eventIds + ")";
			defaultMarketMakerQuery += " AND a.eventId IN (" + eventIds + ")";
		}

		if (username != null) {
			marketMakerQuery += " AND t.marketMakerUsername=? AND t.enabled=?";				
			marketMakerParameters.add(username);
			marketMakerParameters.add(true);
		}

		
		marketMakerQuery += " order by a.creationDate ";
		

		Collection<UserOutlier> userOutliers = find(marketMakerQuery, marketMakerParameters.toArray(new Object[marketMakerParameters.size()]));
		
		Map<Integer, UserOutlier> userOutlierById = new HashMap<Integer, UserOutlier>();
		for(UserOutlier userOutlier: userOutliers) {
			userOutlierById.put(userOutlier.getId(), userOutlier);
		}

		if (username != null && username.equals("mdesantis")) {			
			defaultMarketMakerQuery += " AND NOT EXISTS (FROM MarketMakerEventTracking t WHERE t.eventId=a.eventId AND t.enabled=?)";
			defaultMarketMakerParameters.add(true);
			defaultMarketMakerQuery += " order by a.creationDate ";
			Collection<UserOutlier> defaultUserOutliers = find(defaultMarketMakerQuery, defaultMarketMakerParameters.toArray(new Object[defaultMarketMakerParameters.size()]));
			for(UserOutlier userOutlier: defaultUserOutliers) {
				userOutlierById.put(userOutlier.getId(), userOutlier);
			}
		} 
		
		return userOutlierById.values();
	}

	
	public Collection<UserOutlier> getOutliersByMarketMakerAndEventId(String username, Integer eventId,  UserOutlierStatus userOutlierStatus) {
		String marketMakerQuery = "SELECT a FROM UserOutlier a, MarketMakerEventTracking t WHERE a.eventId = t.eventId";
		Collection<Object> marketMakerParameters = new ArrayList<Object>();

		String defaultMarketMakerQuery = "FROM UserOutlier a WHERE 1=1";
		Collection<Object> defaultMarketMakerParameters = new ArrayList<Object>();

		if (userOutlierStatus != null) {
			marketMakerQuery += " AND a.userOutlierStatus=?";	
			marketMakerParameters.add(userOutlierStatus);
			defaultMarketMakerQuery += " AND a.userOutlierStatus=?";	
			defaultMarketMakerParameters.add(userOutlierStatus);
		} else {
			marketMakerQuery += " AND (a.userOutlierStatus = ? OR a.userOutlierStatus = ?)";
			marketMakerParameters.add(UserOutlierStatus.ACTIVE);
			marketMakerParameters.add(UserOutlierStatus.DISABLED);
			
			defaultMarketMakerQuery += " AND (a.userOutlierStatus = ? OR a.userOutlierStatus = ?)";
			defaultMarketMakerParameters.add(UserOutlierStatus.ACTIVE);
			defaultMarketMakerParameters.add(UserOutlierStatus.DISABLED);
		}

		
		if (eventId != null) {
			marketMakerQuery += "AND a.eventId=?";
			marketMakerParameters.add(eventId);

			defaultMarketMakerQuery += "AND a.eventId=?";
			defaultMarketMakerParameters.add(eventId);
		}

		if (username != null) {
			marketMakerQuery += "AND t.marketMakerUsername=? AND t.enabled=?";				
			marketMakerParameters.add(username);
			marketMakerParameters.add(true);
		}


		marketMakerQuery += " order by a.creationDate ";
		
		
		Collection<UserOutlier> userOutliers = find(marketMakerQuery, marketMakerParameters.toArray(new Object[marketMakerParameters.size()]));
		
		Map<Integer, UserOutlier> userOutlierById = new HashMap<Integer, UserOutlier>();
		for(UserOutlier userOutlier: userOutliers) {
			userOutlierById.put(userOutlier.getId(), userOutlier);
		}

		if (username != null && username.equals("mdesantis")) {			
			defaultMarketMakerQuery += " AND NOT EXISTS (FROM MarketMakerEventTracking t WHERE t.eventId=a.eventId AND t.enabled=?)";
			defaultMarketMakerParameters.add(true);
			
			defaultMarketMakerQuery += " order by a.creationDate ";
			
			Collection<UserOutlier> defaultUserOutliers = find(defaultMarketMakerQuery, defaultMarketMakerParameters.toArray(new Object[defaultMarketMakerParameters.size()]));
			for(UserOutlier userOutlier: defaultUserOutliers) {
				userOutlierById.put(userOutlier.getId(), userOutlier);
			}
		} 
		
		return userOutlierById.values();
	}
	
	public Collection<UserOutlier> getAllOutliers(UserOutlierStatus outlierStatus) {
		return getOutliersByUsernameAndArtistId(null, null, outlierStatus);
	}
	
	
}
