package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentEvent;

public class TMATDependentEventDAO extends HibernateDAO<Integer, TMATDependentEvent> implements
		com.admitone.tmat.dao.TMATDependentEventDAO {

	
	public List<TMATDependentEvent> getTmatDependentsByEventId(Integer eventId) {
		return find("FROM TMATDependentEvent WHERE eventId = ? ",new Object[]{eventId});
	}

	@Override
	public void saveorUpdateTMATDependentEvent(Collection<Event> events) {
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null){
			for(Event event:events){
				String dependents = property.getValue();
				if(dependents!= null && !dependents.isEmpty()){
					List<TMATDependentEvent> list = DAORegistry.getTmatDependentEventDAO().getTmatDependentsByEventId(event.getId());
					List<TMATDependentEvent> tmatDependents = new ArrayList<TMATDependentEvent>();
					for(TMATDependentEvent dependent:list){
						if(dependents.contains(dependent.getDependent())){
								dependents= dependents.replace(dependent.getDependent(),"");
								dependent.setAdd(true);
								tmatDependents.add(dependent);
						}
					}
					String depenedentsList[] = dependents.split(",");
					
					for(String dependent:depenedentsList){
						if(dependent==null ||  dependent.isEmpty()){
							continue;
						}
						
						TMATDependentEvent tmatDependent = new TMATDependentEvent();
						tmatDependent.setDependent(dependent);
						tmatDependent.setEventId(event.getId());
						tmatDependent.setAdd(true);
						tmatDependents.add(tmatDependent);
						
					}
					if(!tmatDependents.isEmpty()){
						DAORegistry.getTmatDependentEventDAO().saveOrUpdateAll(tmatDependents);
					}
				}
			}
			
		}
		
	}

		
}
