package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import org.hibernate.Query;

import com.admitone.tmat.data.TicketQuote;

public class TicketQuoteDAO extends HibernateDAO<Integer, TicketQuote> implements com.admitone.tmat.dao.TicketQuoteDAO {
	public void deleteByQuoteId(Integer quoteId) {
		bulkUpdate("DELETE FROM TicketQuote WHERE quote.id=?", new Object[]{quoteId});
	}

	public TicketQuote getMostRecentTicketQuote(String username) {
		return findSingle("SELECT t FROM TicketQuote t WHERE t.customer.creator=? ORDER BY date DESC", new Object[]{username});
	}

	public Collection<TicketQuote> getTicketQuotesByQuoteId(Integer quoteId) {
		return find("FROM TicketQuote WHERE quote.id=?", new Object[]{quoteId});
	}
	public List<TicketQuote> getTicketQuotesByQuoteIds(List<Integer> ids) {
		Query query = getSession().createQuery("FROM TicketQuote WHERE quote.id in (:Ids) order by quote.creationDate").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<TicketQuote> list= query.list();
		  return list;
	}

	
	public void deleteByIds(List<Integer> ids) {
		Query query = getSession().createQuery("Delete FROM TicketQuote WHERE id in (:Ids)").setParameterList("Ids", ids);		   
		  
		  query.executeUpdate();
		
	}
	
	
}
