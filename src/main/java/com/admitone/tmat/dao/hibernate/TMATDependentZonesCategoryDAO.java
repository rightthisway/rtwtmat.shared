package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.TMATDependentZonesCategory;

public class TMATDependentZonesCategoryDAO extends HibernateDAO<Integer, TMATDependentZonesCategory> implements
		com.admitone.tmat.dao.TMATDependentZonesCategoryDAO {

	
	public List<TMATDependentZonesCategory> getTmatDependentsByTourId(Integer tourId) {
		return find("FROM TMATDependentZonesCategory WHERE tourId = ?",new Object[]{tourId});
	}

	

		
}
