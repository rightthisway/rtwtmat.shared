package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.ArtistBookmark;
import com.admitone.tmat.data.Bookmark;
import com.admitone.tmat.data.BookmarkPk;
import com.admitone.tmat.data.EventBookmark;
import com.admitone.tmat.data.TicketBookmark;
import com.admitone.tmat.data.TourBookmark;
import com.admitone.tmat.data.VenueBookmark;
import com.admitone.tmat.enums.BookmarkType;

public class BookmarkDAO extends HibernateDAO<BookmarkPk, Bookmark> implements com.admitone.tmat.dao.BookmarkDAO {
	public void deleteBookmarksFromUser(String username) {
		bulkUpdate("DELETE FROM Bookmark WHERE username=?", new Object[]{username});
	}
	
	public Collection<ArtistBookmark> getAllArtistBookmarks(String username) {
		return find("FROM ArtistBookmark WHERE username=?", new Object[]{username});
	}

	public Collection<TourBookmark> getAllTourBookmarks(String username) {
		return find("FROM TourBookmark WHERE username=?", new Object[]{username});
	}

	public Collection<EventBookmark> getAllEventBookmarks(String username) {
		return find("FROM EventBookmark WHERE username=?", new Object[]{username});
	}

	public Collection<TicketBookmark> getAllTicketBookmarks(String username) {
		return find("FROM TicketBookmark WHERE username=?", new Object[]{username});
	}

	
	public Collection<VenueBookmark> getAllVenueBookmarks(String username){
		Collection<VenueBookmark> ven = find("FROM VenueBookmark WHERE username=?", new Object[]{username});
		return ven;
	}
	
	//public Bookmark getBookmark(String username, BookmarkType type, String objectId) {
	public Bookmark getBookmark(String username, BookmarkType type, Integer objectId) {
		Collection<Bookmark> list = find("FROM Bookmark WHERE username=? AND type=? AND objectId=?",
										   new Object[] {username, type, objectId});
		if (list.size() == 0) {
			return null;
		}
		
		return list.iterator().next();
	}
	
	public Collection<EventBookmark> getOutlier(String username){
		return find("FROM Bookmark WHERE username=? AND type=? AND outlier=? ",
				   new Object[] {username, BookmarkType.EVENT,1});		
	}
	
	public void deleteArtistBookmarks(Integer artistId) {
		bulkUpdate("DELETE FROM ArtistBookmark WHERE object_id=?", new Object[]{artistId});
	}
	
	public void deleteVenueBookmarks(Integer venueId) {
		bulkUpdate("DELETE FROM VenueBookmark WHERE object_id=?", new Object[]{venueId});
	}

	public void deleteTourBookmarks(Integer tourId) {
		bulkUpdate("DELETE FROM TourBookmark WHERE object_id=?", new Object[]{tourId});
	}

	public void deleteTourBookmarksWithArtistId(Integer artistId) {
		bulkUpdate("DELETE FROM TourBookmark WHERE object_id IN (SELECT id FROM Tour WHERE artist_id=?)", new Object[]{artistId});
	}

	public void deleteEventBookmarks(Integer eventId) {
		deleteTicketBookmarksWithEventId(eventId);
		bulkUpdate("DELETE FROM EventBookmark WHERE object_id=?", new Object[]{eventId});
	}

	public void deleteEventBookmarksWithTourId(Integer tourId) {
		bulkUpdate("DELETE FROM EventBookmark WHERE object_id IN (SELECT id FROM Event WHERE tour_id=?)", new Object[]{tourId});				
	}
	
	public void deleteTicketBookmarks(Integer ticketId) {
		bulkUpdate("DELETE FROM TicketBookmark WHERE object_id=?", new Object[]{ticketId});
	}

	public void deleteTicketBookmarksByCrawlId(Integer crawlId) {
		bulkUpdate("DELETE FROM TicketBookmark WHERE object_id IN (SELECT id from Ticket WHERE ticketListingCrawlId=?)", new Object[]{crawlId});
	}

	public void deleteTicketBookmarksByCrawlId(Integer crawlId, Date startDate, Date endDate) {
		bulkUpdate("DELETE FROM TicketBookmark WHERE object_id IN (SELECT id from Ticket WHERE ticketListingCrawlId=? AND insertionDate >= ? AND insertionDate < ?)", new Object[]{crawlId, startDate, endDate});
	}

	public void deleteTicketBookmarksWithEventId(Integer eventId) {
		bulkUpdate("DELETE FROM TicketBookmark WHERE object_id IN (SELECT id FROM Ticket WHERE event_id=?)", new Object[]{eventId});		
	}
	
	public void cleanBookmarks() {
		bulkUpdate("DELETE FROM TicketBookmark WHERE object_id NOT IN (SELECT id FROM Ticket)");
		bulkUpdate("DELETE FROM EventBookmark WHERE object_id NOT IN (SELECT id FROM Event)");
		bulkUpdate("DELETE FROM TourBookmark WHERE object_id NOT IN (SELECT id FROM Tour)");
		bulkUpdate("DELETE FROM ArtistBookmark WHERE object_id NOT IN (SELECT id FROM Artist)");
		bulkUpdate("DELETE FROM VenueBookmark WHERE object_id NOT IN (SELECT id FROM Artist)");
	}
}