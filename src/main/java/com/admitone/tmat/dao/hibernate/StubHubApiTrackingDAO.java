package com.admitone.tmat.dao.hibernate;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.admitone.tmat.data.StubHubApiTracking;

public class StubHubApiTrackingDAO extends HibernateDAO<Integer, StubHubApiTracking> implements com.admitone.tmat.dao.StubHubApiTrackingDAO{

	@Override
	public int updateStubHubStatus(int crawlId) {
		
		Session session = null;
		int count = 0;		
		try {
			String query = "UPDATE stubhub_api_tracking SET status = 'DELETED',last_updated=getdate() WHERE crawl_id ="+crawlId;
			session = getSessionFactory().openSession();
			SQLQuery sqlQuery = session.createSQLQuery(query);
			count = sqlQuery.executeUpdate();
			session.close();
		}catch (Exception e) {
			if(session != null && session.isOpen()){
				session.close();
			}
			e.printStackTrace();
		}
		
		return count;
	}

}
