package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.SoldTicketDetail;

public class SoldTicketDetailDAO extends HibernateDAO<Integer, SoldTicketDetail> implements com.admitone.tmat.dao.SoldTicketDetailDAO{

	@Override
	public Integer save(SoldTicketDetail entity) {
		return -1;
	}
	
	@Override
	public void saveAll(Collection<SoldTicketDetail> entities) {
	}
	
	@Override
	public void update(SoldTicketDetail entity) {
	}
	
	@Override
	public boolean updateAll(Collection<SoldTicketDetail> entities) {
		return false;
	}

	@Override
	public void saveOrUpdate(SoldTicketDetail entity) {
	}
	
	@Override
	public void saveOrUpdateAll(Collection<SoldTicketDetail> list) {
	}

	@Override
	public List<SoldTicketDetail> getSoldTicketDetailsByEventId(Integer eventId) {
		return find("FROM SoldTicketDetail WHERE eventId = ? ", new Object[]{eventId});
	}
	
	@Override
	public List<SoldTicketDetail> getSoldUnfilledTicketDetailsByBrokerId(Integer brokerId) {
		return find("FROM SoldTicketDetail std WHERE std.eventDate > ? and std.soldTicketDetailPkId.brokerId = ? " +
				"and ( std.purchaseOrderId is null or std.purchaseOrderId = 0 ) ", new Object[]{new Date(),brokerId});
	}
	
	public List<SoldTicketDetail> getAllBrokersSoldUnfilledTicketDetails() {
		return find("FROM SoldTicketDetail std WHERE std.actualSoldPrice>=0 and std.eventDate > ?  and std.soldTicketDetailPkId.brokerId in(5,10) " +
				"and ( std.purchaseOrderId is null or std.purchaseOrderId = 0 ) ", new Object[]{new Date()});
	}
	
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerId(Integer brokerId) {
		return find("FROM SoldTicketDetail std WHERE std.soldTicketDetailPkId.brokerId = ? ", new Object[]{brokerId});
	}

	@Override
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndEventId(Integer brokerId, Integer eventId) {
		return find("FROM SoldTicketDetail std WHERE std.soldTicketDetailPkId.brokerId = ? AND std.eventId = ?", new Object[]{brokerId,eventId});
	}

	@Override
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(
			Integer brokerId,Integer artistId, Integer eventId, Date startDate, Date endDate) {
		String url ="FROM SoldTicketDetail std WHERE 1=1";
		List<Object> params = new ArrayList<Object>();
		
		if(brokerId!=null){
			url = url+" AND std.soldTicketDetailPkId.brokerId = ? ";
			params.add(brokerId);
		}
		
		if(eventId!=null){
			url = url+" AND std.eventId = ? ";
			params.add(eventId);
		}else if(artistId!=null){
			url = url+" AND std.eventId in (SELECT admitoneId from Event WHERE artistId = ? )";
			params.add(artistId);
		}
		
		if(startDate!=null){
			url = url+" AND std.invoiceDateTime >= ? ";
			params.add(startDate);
		}
		if(endDate!=null){
			url = url+" AND std.invoiceDateTime <= ? ";
			params.add(endDate);
		}
		 url = url + "ORDER BY std.invoiceDateTime desc"; 
		return find(url, params.toArray());
	}
}
