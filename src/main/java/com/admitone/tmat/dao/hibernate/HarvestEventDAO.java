package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.HarvestEvent;

public class HarvestEventDAO extends HibernateDAO<Integer, HarvestEvent> implements com.admitone.tmat.dao.HarvestEventDAO {
	

	@Override
	public void deleteById(Integer id) {
		//do nothing because we don't want to delete archived events
		return;
	}
	
	@Override
	public void delete(HarvestEvent event) {
		//do nothing because we don't want to delete archived events
		return;
	}

	@Override
	public void update(HarvestEvent event) {
		// this is used because matt wants to update event type for some events
		super.update(event);
	}
	public Collection<HarvestEvent> getEventsInDateRange(Date startDate, Date endDate) {
		if (startDate == null) {
			if (endDate == null) {
				return null;				
			} else {
				return find("FROM HarvestEvent WHERE event_date <= ? ORDER BY event_date, event_time", new Object[]{endDate});								
			}
		} else if (endDate == null) {
			return find("FROM HarvestEvent WHERE event_date >= ? ORDER BY event_date, event_time", new Object[]{startDate});								
		} else {
			return find("FROM HarvestEvent WHERE event_date >= ? AND event_date <= ? ORDER BY event_date, event_time", new Object[]{startDate, endDate});			
		}
	}


}
