package com.admitone.tmat.dao.hibernate;

import com.admitone.tmat.data.EbayInventoryGroup;

public class EbayInventoryGroupDAO extends HibernateDAO<Integer, EbayInventoryGroup> implements com.admitone.tmat.dao.EbayInventoryGroupDAO {
	
	/*private Collection<EbayInventoryGroup> computeInternalValues(Collection<EbayInventoryGroup> groups) {
		for (EbayInventoryGroup group: groups) {
			if (group.isDeleted() || group.isDisabled()) {
				continue;
			}
			group.computeInternalValuesFromDB();
		}
		return groups;
	}*/
	
	/*public Object[] bookFirstGroupWithAvailableTicketId() {
		return (Object[])getHibernateTemplate().execute(new HibernateCallback() {
			
			
			public Object doInHibernate(Session session) throws HibernateException,
					SQLException {
				String hql = "FROM EbayInventoryGroup WHERE ticketId is not null AND status<>'ACTIVE' AND first_time_used IS NOT NULL ORDER BY first_time_used ASC";
				Query query = session.createQuery(hql);
				query.setFetchSize(1);
				query.setMaxResults(1);
				EbayInventoryGroup group = (EbayInventoryGroup)query.uniqueResult();
				if (group == null) {
					Collection<Integer> maxList = find("SELECT MAX(ticketId) FROM EbayInventoryGroup");
					Integer max = maxList.iterator().next();
					if (max == null) {
						max = 1;
					}

					return new Object[]{max + 1, null};
				}
				
				Integer ticketId = group.getTicketId();
				
				Object[] result = new Object[]{ticketId, group.getFirstTimeUsed()};
				
				if (group.isDeleted()) {
					DAORegistry.getEbayInventoryDAO().deleteByGroupId(group.getId());
					session.delete(group);
				} else {
					group.setTicketId(null);
					group.setFirstTimeUsed(null);
					session.update(group);
				}
				return result;
			}
		});
	}*/

	/*public Collection<EbayInventoryGroup> getAllEbayInventoryGroupsWithTicketId() {
		Collection<EbayInventoryGroup> groups = find("FROM EbayInventoryGroup WHERE ticketId IS NOT null");
		Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventoryGroup g, EbayInventory i WHERE g.id=i.groupId AND g.ticketId IS NOT null");
		Collection<Event> events = find("SELECT e FROM Event e, EbayInventoryGroup g WHERE e.id=g.eventId AND g.ticketId IS NOT null");
		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);
	}*/

	/*public void update(EbayInventoryGroup group) {
		group.setLastUpdate(new Date());
		super.update(group);
	}
	
	private Collection<EbayInventoryGroup> assignEventAndInventories(Collection<EbayInventoryGroup> groups, Collection<Event> events, Collection<EbayInventory> inventories) {
		// assign event
		Map<Integer, Event> eventMap = new HashMap<Integer, Event>();
		for (Event event: events) {
			eventMap.put(event.getId(), event);
		}

		for (EbayInventoryGroup group: groups) {
			if (group.getEventId() == null) {
				continue;
			}
			
			Integer eventId = group.getEventId();
			Event event = eventMap.get(eventId);
			if (event == null) {
				event = DAORegistry.getEventDAO().get(eventId);
				eventMap.put(eventId, event);
			} 
			
			group.setEvent(event);
		}
		
		// assign inventories
		Map<Integer, List<EbayInventory>> inventoryMap = new HashMap<Integer, List<EbayInventory>>();
		for (EbayInventory inventory: inventories) {
			List<EbayInventory> list = inventoryMap.get(inventory.getGroupId());
			if (list == null) {
				list = new ArrayList<EbayInventory>();
				inventoryMap.put(inventory.getGroupId(), list);
			}
			
			list.add(inventory);
		}
		
		for (EbayInventoryGroup group: groups) {
			List<EbayInventory> list = inventoryMap.get(group.getId());
			if (list != null && !list.isEmpty()) {
				group.setEbayInventories(list);
			}
		}
		
		return groups;
	}
	
	public Collection<EbayInventoryGroup> getAllEbayInventoryGroupsByEventId(Integer eventId) {
		if (eventId == null) {
			return getAll();
		}	
		
		Event event = DAORegistry.getEventDAO().get(eventId);
		Collection<EbayInventoryGroup> groups = find("FROM EbayInventoryGroup WHERE eventId=?", new Object[]{eventId});
		Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventoryGroup g, EbayInventory i WHERE g.id=i.groupId AND g.eventId=?", new Object[]{eventId});
		Collection<Event> events = new ArrayList<Event>();
		events.add(event);

		return computeInternalValues(assignEventAndInventories(groups, events, inventories));
	}*/

	/*public Collection<EbayInventoryGroup> getAllEbayInventoryGroupsByTourId(Integer tourId) {
		if (tourId == null) {
			return getAll();
		}
		
		Collection<EbayInventoryGroup> groups = find("SELECT g FROM EbayInventoryGroup g, Event e WHERE g.eventId=e.id AND e.tourId=?", new Object[]{tourId});
		Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventory i WHERE i.groupId IN (SELECT g.id FROM EbayInventoryGroup g, Event e WHERE g.eventId=e.id AND e.tourId=?)", new Object[]{tourId});
		Collection<Event> events = find("SELECT e FROM EbayInventoryGroup g, Event e WHERE g.eventId=e.id AND e.tourId=?", new Object[]{tourId});

		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);
	}*/
	
	/*public Collection<EbayInventoryGroup> getAllEbayInventoryGroupsByEventId(Integer eventId, Object[] statuses) {
		Collection<EbayInventoryGroup> groups = null;
		Collection<EbayInventory> inventories = null;

		String statusHql = " (";
		for (Object status: statuses) {
			statusHql += "g.status='" + status + "' OR ";
		}
		statusHql = statusHql.substring(0, statusHql.length() - 4) + ")";
		String andStatusHql = " AND " + statusHql;

		Collection<Event> events;
		
		if (eventId == null) {
			groups = find("FROM EbayInventoryGroup g WHERE " + statusHql);
			inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id " + andStatusHql);
			events = find("SELECT e FROM Event e, EbayInventoryEvent ee WHERE e.id=ee.id");
		} else {
			groups = find("FROM EbayInventoryGroup g WHERE g.eventId=? " + andStatusHql, new Object[]{eventId});
			inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id AND g.eventId=? " + andStatusHql, new Object[]{eventId});
			events = new ArrayList<Event>();
			events.add(DAORegistry.getEventDAO().get(eventId));
		}

		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);
	}*/

	/*public Collection<EbayInventoryGroup> getAllEbayInventoryGroupsByTourId(Integer tourId, Object[] statuses) {
		Collection<EbayInventoryGroup> groups = null;
		Collection<EbayInventory> inventories = null;
		Collection<Event> events = null;

		String statusHql = " (";
		for (Object status: statuses) {
			statusHql += "g.status='" + status + "' OR ";
		}
		statusHql = statusHql.substring(0, statusHql.length() - 4) + ")";
		String andStatusHql = " AND " + statusHql;

		if (tourId == null) {
			groups = find("FROM EbayInventoryGroup g WHERE " + statusHql);
			inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id " + andStatusHql);
			events = find("SELECT e FROM Event e WHERE e.id IN (SELECT g.eventId FROM EbayInventoryGroup g WHERE " + statusHql + ")");
		} else {
			groups = find("SELECT g FROM EbayInventoryGroup g, Event e WHERE g.eventId=e.id AND e.tourId=? " + andStatusHql, new Object[]{tourId});
			inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g, Event e WHERE g.id=i.groupId AND g.eventId=e.id AND e.tourId=? " + andStatusHql, new Object[]{tourId});
			events = find("SELECT e FROM Event e WHERE e.tourId=? AND e.id IN (SELECT g.eventId FROM EbayInventoryGroup g WHERE " + statusHql + ")", new Object[]{tourId});
		}
		
		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);
	}*/

	/*public Collection<EbayInventoryGroup> getAll() {
		Collection<EbayInventoryGroup> groups = super.getAll();
		Collection<EbayInventory> inventories = DAORegistry.getEbayInventoryDAO().getAll();
		Collection<Event> events = find("SELECT e FROM Event e, EbayInventoryEvent ie WHERE e.id=ie.id");
		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);
	}*/
	
	/*public Integer getMaxTicketId() {
		Collection<Integer> maxList = find("SELECT MAX(ticketId) FROM EbayInventoryGroup");
		Integer max = maxList.iterator().next();
		if (max == null) {
			return 1;
		}
		
		return max;
	}*/
	
	/*public Collection<EbayInventoryGroup> getAllActiveEbayInventoryGroups() {
		Collection<EbayInventoryGroup> groups = find("FROM EbayInventoryGroup WHERE status='ACTIVE'");
		Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id AND g.status='ACTIVE'");
		Collection<Event> events = find("SELECT e FROM Event e, EbayInventoryGroup g WHERE e.id=g.eventId AND g.status='ACTIVE'");
		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);		
	}*/

	/*public Collection<EbayInventoryGroup> getByEventCategoryQuantity(Integer eventId, String category, Integer quantity, EbayInventoryStatus status) {
		if (status == null) {
			Collection<EbayInventoryGroup> groups = find("FROM EbayInventoryGroup WHERE eventId=? AND category=?  AND quantity=?", new Object[]{eventId, category, quantity});
			Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id AND g.eventId=? AND g.category=?  AND g.quantity=?", new Object[]{eventId, category, quantity});
			Collection<Event> events = find("SELECT e FROM Event e, EbayInventoryGroup g WHERE e.id=g.eventId AND g.eventId=? AND g.category=?  AND g.quantity=?", new Object[]{eventId, category, quantity});
			assignEventAndInventories(groups, events, inventories);
			return computeInternalValues(groups);			
		}
		Collection<EbayInventoryGroup> groups = find("FROM EbayInventoryGroup WHERE eventId=? AND category=?  AND quantity=? AND status=?", new Object[]{eventId, category, quantity, status});
		Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id AND g.eventId=? AND g.category=?  AND g.quantity=? AND g.status=?", new Object[]{eventId, category, quantity, status});
		Collection<Event> events = find("SELECT e FROM Event e, EbayInventoryGroup g WHERE e.id=g.eventId AND g.eventId=? AND g.category=?  AND g.quantity=? AND g.status=?", new Object[]{eventId, category, quantity, status});
		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);

//		if (groups.isEmpty()) {
//			return null;
//		}
//
//		if (groups.size() > 1) {
//			throw new RuntimeException("Duplicate groups for eventId=" + eventId + ", category=" + category + ", quantity=" + quantity);
//		}
//		
//		EbayInventoryGroup group = groups.iterator().next();
//		group.computeInternalValuesFromDB();
//		return group;
		
	}*/

	/*public Collection<EbayInventoryGroup> getAllEbayInventoryGroupsByEventCategoryQuantity(Integer eventId, String category, Integer quantity) {
		Collection<EbayInventoryGroup> groups = find("FROM EbayInventoryGroup WHERE eventId=? AND category=?  AND quantity=?", new Object[]{eventId, category, quantity});
		Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id AND g.eventId=? AND g.category=?  AND quantity=?", new Object[]{eventId, category, quantity});
		Collection<Event> events = find("SELECT e FROM Event e, EbayInventoryGroup g WHERE g.eventId=e.id AND g.eventId=? AND g.category=?  AND quantity=?", new Object[]{eventId, category, quantity});
		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);		
	}*/

	/*public EbayInventoryGroup get(Integer groupId) {
		EbayInventoryGroup group = super.get(groupId);
		if (group == null) {
			return null;
		}
		group.computeInternalValuesFromDB();
		return group;
	}*/
		
	/*public void deleteById(Integer id) {
		EbayInventoryGroup group = get(id);
		if (group != null) {
			delete(group);
		}
	}
	
	public void delete(EbayInventoryGroup group) {
		DAORegistry.getEbayInventoryDAO().deleteByGroupId(group.getId());
		super.delete(group);
	}
	
	public void deleteByEventId(Integer eventId) {
		for (EbayInventoryGroup group: getAllEbayInventoryGroupsByEventId(eventId)) {
			delete(group);
		}
	}

	public void markAsDeletedByEventId(Integer eventId) {
		markAsDeleted(getAllEbayInventoryGroupsByEventId(eventId));
	}*/
	
	/*public void markAsDeletedByCrawlId(Integer crawlId) {
		Set<Integer> groupIds = new HashSet<Integer>();
		for (EbayInventory inventory: (Collection<EbayInventory>)super.find("FROM EbayInventory WHERE ticket.ticketListingCrawlId=?", new Object[]{crawlId})) {
			groupIds.add(inventory.getGroupId());
		}
		
		Collection<EbayInventoryGroup> groups = new ArrayList<EbayInventoryGroup>();
		for (Integer groupId: groupIds) {
			groups.add(get(groupId));
		}
		
		markAsDeleted(groups);		
	}

	public void markAsDeleted(Collection<EbayInventoryGroup> groups) {
		for (EbayInventoryGroup group: groups) {
			if (group.getTicketId() == null) {
				delete(group);
			} else {
				group.setEventId(null);
				DAORegistry.getEbayInventoryDAO().deleteByGroupId(group.getId());
				group.setStatus(EbayInventoryStatus.DELETED);
				update(group);
			}
		}		
	}
	
	public Integer save(EbayInventoryGroup group) {
		super.save(group);
		
		// save ebay inventories if their group id is null
		for (EbayInventory inventory: group.getEbayInventories()) {
			if (inventory.getGroupId() == null) {
				inventory.setGroupId(group.getId());
				DAORegistry.getEbayInventoryDAO().save(inventory);
			}
		}
		
		return group.getId();
	}

	public Long getInventoryGroupToTakeActionCount() {
		Collection<Long> list = find("SELECT COUNT(*) FROM EbayInventoryGroup WHERE status<>? AND status<>?", new Object[]{EbayInventoryStatus.ACTIVE, EbayInventoryStatus.DISABLED});
		if (list.isEmpty()) {
			return 0L;
		}
		
		Long count = list.iterator().next();
		if (count == null) {
			return 0L;
		}
		
		return count;
	}

	public Long getNumPendingInventoryGroups(Integer eventId) {
		Collection<Long> list = find("SELECT COUNT(*) FROM EbayInventoryGroup WHERE eventId=? AND (status=? OR status=? OR status=?)", new Object[]{eventId, EbayInventoryStatus.REPLACEMENT,EbayInventoryStatus.SUGGESTED, EbayInventoryStatus.INVALID});		
		if (list.isEmpty()) {
			return 0L;
		}

		Long count = list.iterator().next();
		if (count == null) {
			return 0L;
		}
		
		return count;		
	}
	
	public Long getNumInventoryGroups(Integer eventId, EbayInventoryStatus status) {
		Collection<Long> list = find("SELECT COUNT(*) FROM EbayInventoryGroup WHERE eventId=? AND status=?", new Object[]{eventId, status});		
		if (list.isEmpty()) {
			return 0L;
		}

		Long count = list.iterator().next();
		if (count == null) {
			return 0L;
		}
		
		return count;
	}
	
	public EbayInventoryGroup findFirstGroupWithAvailableTicketId() {
		return (EbayInventoryGroup)getHibernateTemplate().execute(new HibernateCallback() {
			public Object doInHibernate(Session session) throws HibernateException,
					SQLException {
				String hql = "FROM EbayInventoryGroup WHERE ticketId is not null AND status<>'ACTIVE' AND first_time_used IS NOT NULL ORDER BY first_time_used ASC";
				Query query = session.createQuery(hql);
				query.setFetchSize(1);
				query.setMaxResults(1);
				return query.uniqueResult();
			}
		});
	}

	public Collection<EbayInventoryGroup> getAllEbayInventoryGroupsWithTicketId(Object[] statuses) {
		return getAllEbayInventoryGroups(statuses, true);
	}

	// FIXME: we don't compute the inventory for these groups
	public Collection<EbayInventoryGroup> getAllEbayInventoryGroups(Object[] statuses, boolean withTicketId) {
		String orderHql = " ORDER BY g.eventId, g.category, g.quantity ";
		String statusHql = " (";
		for (Object status: statuses) {
			statusHql += " g.status='" + status + "' OR ";
		}
		statusHql = statusHql.substring(0, statusHql.length() - 4) + ")";
		String andStatusHql = " AND " + statusHql;
		String whereStatusHql = " WHERE " + statusHql;

		String withTicketIdHql = "";
		if (withTicketId) {
			withTicketIdHql = " AND g.ticketId IS NOT NULL ";
		}
		
		Collection<EbayInventoryGroup> groups = find("FROM EbayInventoryGroup g " + whereStatusHql + orderHql);
		Collection<EbayInventory> inventories = find("SELECT i FROM EbayInventory i, EbayInventoryGroup g WHERE i.groupId=g.id " + withTicketIdHql + andStatusHql + orderHql);
		Collection<Event> events = find("SELECT e FROM Event e, EbayInventoryGroup g WHERE e.id=g.eventId " + withTicketIdHql + andStatusHql + orderHql);
		assignEventAndInventories(groups, events, inventories);
		return computeInternalValues(groups);		
	}

	public Collection<EbayInventoryGroup> getAllEbayInventoryGroups(Object[] statuses) {
		return getAllEbayInventoryGroups(statuses, false);
	}
	
	public void cleanGroups() {
		bulkUpdate("UPDATE EbayInventoryGroup SET status='DELETED' WHERE id NOT IN (SELECT groupId FROM EbayInventory)");
	}*/

	
	
}
