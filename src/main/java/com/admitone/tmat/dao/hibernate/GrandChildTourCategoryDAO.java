package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.GrandChildTourCategory;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.TMATDependentGrandChildCategory;

public class GrandChildTourCategoryDAO extends HibernateDAO<Integer, GrandChildTourCategory>implements
		com.admitone.tmat.dao.GrandChildTourCategoryDAO {

	
	public Collection<GrandChildTourCategory> getGrandChildTourCategoryByChildTourCategory(Integer id) {
		return find("FROM GrandChildTourCategory ctc where ctc.childTourCategory.id= ? ORDER BY name",new Object[]{id});
	}

	
	public Collection<GrandChildTourCategory> getGrandChildTourCategoryByChildTourCategoryName(String name) {
		return find("FROM GrandChildTourCategory ctc where ctc.childTourCategory.name=? ORDER BY name",new Object[]{name});
	}

	
	public GrandChildTourCategory getGrandChildTourCategoryByNameAndChildTourCategoryNameAndGrandChildTourCategoryName(
			String name, String categoryName,String parentCategoryName) {
		return findSingle("FROM GrandChildTourCategory ctc WHERE name = ? AND ctc.childTourCategory.name=? and ctc.childTourCategory.tourCategory.name = ? ",new Object[]{name,categoryName,parentCategoryName});
	}

	public void deleteAllById(List<Integer> ids) {
		String stringIds="";
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		
		for(Integer id:ids){
			if(id==null){
				continue;
			}
			List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(id);
			Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
			if(list!=null && !list.isEmpty()){
				for(TMATDependentGrandChildCategory cat:list){
					map.put(cat.getDependent(), cat);
				}
			}
			
			stringIds+=id +",";
			if(property!=null && !property.getValue().isEmpty()){
				String temp[] = property.getValue().split(",");
				for(String zone:temp){
					TMATDependentGrandChildCategory dependent = map.get(zone);
					if(dependent==null){
						 dependent = new TMATDependentGrandChildCategory();
					}
					dependent.setGrandChildCategoryId(id);
					dependent.setDependent(zone);
					dependent.setAdd(false);
					DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
				}
			}
		}
		
		stringIds=stringIds.substring(0,stringIds.length()-1);
		if(!"".equals(stringIds)){
			bulkUpdate("DELETE FROM GrandChildTourCategory WHERE id in(" + stringIds + ")");
		}
		
	}

	/**
	 * return grand child by name where name should match the input
	 */
	public Collection<GrandChildTourCategory> getGrandChildCategoriesByName(String name) {
		String hql="FROM GrandChildTourCategory WHERE name like ? and name <> 'NONE' and name <> 'OTHER' ORDER BY name";
		List<Object> parameters = new ArrayList<Object>();
		parameters.add("%" + name + "%");
		return find(hql, parameters.toArray());
	}

	public Collection<GrandChildTourCategory> getAllGrandChildTourOrderByName() {
		return find("FROM GrandChildTourCategory ORDER BY name");
		
	}


	public Collection<GrandChildTourCategory> getGrandChildTourCategoryByChildTourCategorys(
			List<Integer> ids) {
		
			Query query = getSession().createQuery("FROM GrandChildTourCategory where childTourCategory.id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<GrandChildTourCategory> list= query.list();
		  return list;
	}

	@Override
	public Integer save(GrandChildTourCategory entity) {
		// TODO Auto-generated method stub
		super.save(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentGrandChildCategory dependent = new TMATDependentGrandChildCategory();
				dependent.setGrandChildCategoryId(entity.getId());
				dependent.setDependent(zone);
				dependent.setAdd(true);
				DAORegistry.getTmatDependentGrandChildCategoryDAO().save(dependent);
			}
		}
		return entity.getId();
	}
	
	@Override
	public void update(GrandChildTourCategory entity) {
		super.update(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(entity.getId());
		Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentGrandChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentGrandChildCategory dependent = map.get(zone);
				if(dependent==null){
					 dependent = new TMATDependentGrandChildCategory();
				}
				dependent.setGrandChildCategoryId(entity.getId());
				dependent.setDependent(zone);
				dependent.setAdd(true);
				DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
			}
		}
	}
	
	@Override
	public void delete(GrandChildTourCategory entity) {
		super.delete(entity);
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(entity.getId());
		Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
		if(list!=null && !list.isEmpty()){
			for(TMATDependentGrandChildCategory cat:list){
				map.put(cat.getDependent(), cat);
			}
		}
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(String zone:temp){
				TMATDependentGrandChildCategory dependent = map.get(zone);
				if(dependent==null){
					 dependent = new TMATDependentGrandChildCategory();
				}
				dependent.setGrandChildCategoryId(entity.getId());
				dependent.setDependent(zone);
				dependent.setAdd(false);
				DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
			}
		}
	}
	
	@Override
	public void deleteAll(Collection<GrandChildTourCategory> entities) {
		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		
		if(property!=null && !property.getValue().isEmpty()){
			String temp[] = property.getValue().split(",");
			for(GrandChildTourCategory entity:entities){
				List<TMATDependentGrandChildCategory> list = DAORegistry.getTmatDependentGrandChildCategoryDAO().getTmatDependentsByGrandChildCategoryId(entity.getId());
				Map<String, TMATDependentGrandChildCategory> map = new HashMap<String, TMATDependentGrandChildCategory>();
				if(list!=null && !list.isEmpty()){
					for(TMATDependentGrandChildCategory cat:list){
						map.put(cat.getDependent(), cat);
					}
				}
				for(String zone:temp){
					TMATDependentGrandChildCategory dependent = map.get(zone);
					if(dependent==null){
						 dependent = new TMATDependentGrandChildCategory();
					}
					dependent.setGrandChildCategoryId(entity.getId());
					dependent.setDependent(zone);
					dependent.setAdd(true);
					DAORegistry.getTmatDependentGrandChildCategoryDAO().saveOrUpdate(dependent);
				}
			}
		}
		super.deleteAll(entities);
	}
}
