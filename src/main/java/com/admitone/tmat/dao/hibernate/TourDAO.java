package com.admitone.tmat.dao.hibernate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Class that handle all the operations on the Tour table in the DB
 */
public class TourDAO {//extends HibernateDAO<Integer, Tour> implements com.admitone.tmat.dao.TourDAO {

	private final Logger logger = LoggerFactory.getLogger(TourDAO.class);
	/**
	 * return all active tours for a given artist
	 */
	/*public Collection<Tour> getAllToursByArtist(int artistId) {
		return find("FROM Tour WHERE artist_id=? AND tourStatus=? ORDER BY name ASC", new Object[]{artistId, TourStatus.ACTIVE});
	}

	*//**
	 * return all the tours in the DB
	 *//*
	public Collection<Tour> getAll() {
		return find("FROM Tour WHERE tourStatus<>1 and tourStatus <>2 ORDER BY name ASC");
	}

	 public Collection<Tour> filterTour(String tourName,String artist) {
	    String hql = "FROM Tour WHERE tourStatus=? ";
	    List<Object> params = new ArrayList<Object>();
	    params.add(TourStatus.ACTIVE);
	   
	    if(!tourName.isEmpty()){
	    	hql += " AND name LIKE '%"+tourName+"%' ";
	    }
	    if(!artist.isEmpty()){
	    	hql += " AND artist.name LIKE '%"+artist+"%'";
	    }
	        
	    hql += " ORDER By eventCount DESC";
	    return find(hql,params.toArray());
	 }
	
	*//**
	 * Return Tours which are active and which name matches a pattern (for the autocomplete)
	 *//*
    public Collection<Tour> filterByName(String pattern) {
    	return find("FROM Tour WHERE name LIKE '%" + pattern + "%' AND tourStatus=? ORDER BY name ASC", new Object[]{TourStatus.ACTIVE});
    }

	*//**
	 * Return Tours which are active and belong to a given artist and which name matches a pattern
	 *//*
    public Collection<Tour> filterToursByArtistByName(Integer artistId, String pattern) {
		return find("FROM Tour WHERE artist_id=? AND name LIKE '%" + pattern + "%' AND tourStatus=? ORDER BY name ASC", new Object[]{artistId, TourStatus.ACTIVE});
    }
    
	*//**
	 * Return Tours which are active and have events which name matches a pattern and with a given status
	 *//*
	public Collection<Tour> getAllToursByNameExcerpt(String excerpt, EventStatus eventStatus) {
		return new HashSet<Tour>(find("SELECT t FROM Tour t, Event e WHERE t.name LIKE ? AND e.tourId=t.id AND e.eventStatus=? AND t.tourStatus=? ORDER BY t.name", new Object[]{"%" + TextUtil.removeExtraWhitespaces(excerpt) + "%", eventStatus, TourStatus.ACTIVE}));
	}

	
	public void saveOrUpdate(Tour tour) {
		super.saveOrUpdate(tour);
		if(tour.getGrandChildTourCategory().getId()!=51){
			Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
			if(property!=null){
				String dependents = property.getValue();
				if(dependents!= null && !dependents.isEmpty()){
					List<TMATDependentTour> list = DAORegistry.getTmatDependentTourDAO().getTmatDependentsByTourId(tour.getId());
					List<TMATDependentTour> tmatDependents = new ArrayList<TMATDependentTour>();
					for(TMATDependentTour dependent:list){
						if(dependents.contains(dependent.getDependent())){
//							if(dependent.isAdd()){
//								dependents = dependents.replace(dependent.getDependent(),"");
//							}else{
								dependents = dependents.replace(dependent.getDependent(),"");
								dependent.setAdd(true);
								tmatDependents.add(dependent);
//							}
						}
					}
					String depenedentsList[] = dependents.split(",");
					
					for(String dependent:depenedentsList){
						if(dependent.trim().equals("")){
							continue;
						}
						
						TMATDependentTour tmatDependent = new TMATDependentTour();
						tmatDependent.setDependent(dependent);
						tmatDependent.setTourId(tour.getId());
						tmatDependent.setAdd(true);
						tmatDependents.add(tmatDependent);
						
					}
					if(!tmatDependents.isEmpty()){
						DAORegistry.getTmatDependentTourDAO().saveOrUpdateAll(tmatDependents);
					}
				}
			}
		}
		
	}
	
	
	public Integer save(Tour tour) {
		// TODO Auto-generated method stub
		super.save(tour);
//		Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
//		if(property!=null){
//			String dependents = property.getValue();
//			if(dependents!= null && !dependents.isEmpty()){
//				List<TMATDependentTour> list = DAORegistry.getTmatDependentTourDAO().getTmatDependentsByTourId(tour.getId());
//				List<TMATDependentTour> tmatDependents = new ArrayList<TMATDependentTour>();
//				for(TMATDependentTour dependent:list){
//					if(dependents.contains(dependent.getDependent())){
////						if(dependent.isAdd()){
////							dependents = dependents.replace(dependent.getDependent(),"");
////						}else{
//							dependents = dependents.replace(dependent.getDependent(),"");
//							dependent.setAdd(true);
//							tmatDependents.add(dependent);
////						}
//					}
//				}
//				String depenedentsList[] = dependents.split(",");
//				
//				for(String dependent:depenedentsList){
//					if(dependent.trim().equals("")){
//						continue;
//					}
//					
//					TMATDependentTour tmatDependent = new TMATDependentTour();
//					tmatDependent.setDependent(dependent);
//					tmatDependent.setTourId(tour.getId());
//					tmatDependent.setAdd(true);
//					tmatDependents.add(tmatDependent);
//					
//				}
//				if(!tmatDependents.isEmpty()){
//					DAORegistry.getTmatDependentTourDAO().saveOrUpdateAll(tmatDependents);
//				}
//			}
//		}
		return tour.getId();
	}
	*//**
	 * delete a tour by id
	 *//*
	
	
    public void deleteById(Integer id) {    	
    	// delete events
    	for (Event event: DAORegistry.getEventDAO().getAllEventsByTour(id)) {
    		DAORegistry.getEventDAO().deleteById(event.getId());
    	}
    	
    	// delete categories now we category is depends on venue 
//    	for (Category category: DAORegistry.getCategoryDAO().getAllCategories(id)) {
//    		DAORegistry.getCategoryDAO().delete(category);
//    	}

    	// delete bookmarks
    	DAORegistry.getBookmarkDAO().deleteTourBookmarks(id);
    	
    	// delete price adjustments
    	DAORegistry.getTourPriceAdjustmentDAO().deletePriceAdjustments(id);    	

    	// delete crawls
    	TicketListingCrawler crawler = SpringUtil.getTicketListingCrawler();
    	if (crawler != null) {
	    	Collection<TicketListingCrawl> crawls = new ArrayList<TicketListingCrawl>(SpringUtil.getTicketListingCrawler().getTicketListingCrawlsByTourId(id));
	    	for (TicketListingCrawl crawl: crawls) {
	       		SpringUtil.getTicketListingCrawler().removeTicketListingCrawl(crawl);
				DAORegistry.getTicketListingCrawlDAO().deleteById(crawl.getId());    		
	    	}
    	}
    	
    	Collection<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByTour(id);
    	for (TicketListingCrawl crawl: crawls) {
       		//SpringUtil.getTicketListingCrawler().removeTicketListingCrawl(crawl);
			DAORegistry.getTicketListingCrawlDAO().deleteById(crawl.getId());    		
    	}
    	
    	// mark for deletion
    	Tour tour = get(id);
    	Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null){
			String dependents = property.getValue();
			if(dependents!= null && !dependents.isEmpty()){
				List<TMATDependentTour> list = DAORegistry.getTmatDependentTourDAO().getTmatDependentsByTourId(tour.getId());
				List<TMATDependentTour> tmatDependents = new ArrayList<TMATDependentTour>();
				for(TMATDependentTour dependent:list){
					if(dependents.contains(dependent.getDependent())){
//						if(!dependent.isAdd()){
//							dependents=  dependents.replace(dependent.getDependent(),"");
////					}else{
							dependents=  dependents.replace(dependent.getDependent(),"");
							dependent.setAdd(false);
							tmatDependents.add(dependent);
//						}
					}
				}
				String depenedentsList[] = dependents.split(",");
				
				for(String dependent:depenedentsList){
					if(dependent.trim().equals("")){
						continue;
					}
					TMATDependentTour tmatDependent = new TMATDependentTour();
					tmatDependent.setDependent(dependent);
					tmatDependent.setTourId(tour.getId());
					tmatDependent.setAdd(false);
					tmatDependents.add(tmatDependent);
				}
				if(!tmatDependents.isEmpty()){
					DAORegistry.getTmatDependentTourDAO().saveOrUpdateAll(tmatDependents);
				}
			}
		}
    	tour.setTourStatus(TourStatus.DELETED);
    	updateTour(tour);
    }

	*//**
	 * Retrieve all tours which have no active events and which are not bound to any ticket listing crawl. 
	 *//*
	
    public Collection<Tour> getEmptyTours() {
    	Collection<Tour> emptyTours = (Collection<Tour>)find("SELECT t FROM Tour t WHERE t.id NOT IN (SELECT DISTINCT(tlc.tourId) FROM TicketListingCrawl tlc WHERE tlc.enabled=1 AND tlc.tourId IS NOT NULL) AND t.id NOT IN (SELECT DISTINCT(e.tourId) FROM Event e WHERE e.tourId IS NOT NULL AND e.eventStatus='ACTIVE')");
    	return emptyTours;
    }
    
    *//**
     * delete all empty tours (tours not associated to events and crawls)
     *//*
    public void deleteEmptyTours() {
    	for (Tour tour: this.getEmptyTours()) {
    		// the tour is associated to an even => skip
    		if (!tour.getEvents().isEmpty()) {
    			continue;
    		}
    		
    		// the tour is associated to a crawler => skip
    		if (!DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByTour(tour.getId()).isEmpty()) {
    			continue;
    		}

    		// the tour is associated to categories => skip
    		//if (!DAORegistry.getCategoryDAO().getAllCategories(tour.getId()).isEmpty()) {
    		//	continue;
    		//}
    		 
    		//System.out.println("Deleting tour " + tour.getName() + " " + tour.getId());
    		// mark tour for deletion
    		tour.setTourStatus(TourStatus.DELETED);
    		update(tour);
    	}
    }
    
    *//**
     * delete a tour
     *//*
    
    public void delete(Tour tour) {
    	deleteById(tour.getId());
    }

    *//**
     * Return the number of active tours
     *//*
	public int getTourCount() {
		List list = find("SELECT count(id) FROM Tour WHERE tourStatus=?", new Object[]{TourStatus.ACTIVE});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}

    *//**
     * Return the number of active tours for a given artist
     *//*
	public int getTourCount(Integer artistId) {
		List list = find("SELECT count(id) FROM Tour WHERE artist_id=? AND tourStatus=?", new Object[]{artistId, TourStatus.ACTIVE});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	}

	*//**
	 * Return tour by its stubhub id
	 *//*
	
	public Tour getTourByStubhubId(Integer stubhubId) {
    	Collection<Tour> tours = find("FROM Tour WHERE stubhubId=?", new Object[]{stubhubId});
    	if (tours.isEmpty()) {
    		return null;
    	}
    	
    	return tours.iterator().next();
	}

	*//**
	 * Return tours with a given status and which has a name similar to a given name (i.e., number of common
	 * words quite high) 
	 *//*
	public Collection<Tour> searchTours(String name, TourType tourType) {
		Collection<Tour> result = new ArrayList<Tour>();
		Collection<Tour> tours = find("FROM Tour WHERE tour_type=? AND tourStatus=?", new Object[]{tourType.toString(), TourStatus.ACTIVE});
		for (Tour tour: tours) {
			if (TextUtil.isSimilar(tour.getName(), name)) {
				result.add(tour);
			}
		}
		
		return result;
	}
	
	*//**
	 * get all tours which have events
	 *//*
	public Collection<Tour> getAllToursWithEvents() {
		return find("FROM Tour WHERE event_count > 0 ORDER BY name ASC");
	}
	
	*//**
	 * get all active tours 
	 *//*
	public Collection<Tour> getAllActiveTours() {
		return find("FROM Tour WHERE tourStatus=? ORDER BY eventCount DESC", new Object[]{TourStatus.ACTIVE});
	}
	
	public Collection<Tour> getAllActiveToursOrderByName() {
		return find("FROM Tour WHERE tourStatus=? ORDER BY name ASC", new Object[]{TourStatus.ACTIVE});
	}
	*//**
	 * get all active tours which have events
	 *//*
	public Collection<Tour> getAllActiveToursWithEvents() {
		return find("FROM Tour WHERE event_count > 0 AND tourStatus=? ORDER BY name ASC", new Object[]{TourStatus.ACTIVE});
	}
	
	
	*//**
	 * update a tour in the DB
	 *//*
	
	public void update(Tour tour){
    	tour.setEventCount(DAORegistry.getEventDAO().getEventCountByTour(tour.getId()));
    	tour.setStartDate(DAORegistry.getEventDAO().getMinDateFromEvents(tour.getId()));
    	tour.setEndDate(DAORegistry.getEventDAO().getMaxDateFromEvents(tour.getId()));
    	Property property = DAORegistry.getPropertyDAO().get("tmat.dependent");
		if(property!=null){
			String dependents = property.getValue();
			if(dependents!= null && !dependents.isEmpty()){
				List<TMATDependentTour> list = DAORegistry.getTmatDependentTourDAO().getTmatDependentsByTourId(tour.getId());
				List<TMATDependentTour> tmatDependents = new ArrayList<TMATDependentTour>();
				for(TMATDependentTour dependent:list){
					if(dependents.contains(dependent.getDependent())){
//						if(dependent.isAdd()){
//							dependents = dependents.replace(dependent.getDependent(),"");
//						}else{
							dependents = dependents.replace(dependent.getDependent(),"");
							dependent.setAdd(true);
							tmatDependents.add(dependent);
//						}
					}
				}
				String depenedentsList[] = dependents.split(",");
				
				for(String dependent:depenedentsList){
					if(dependent.trim().equals("")){
						continue;
					}
					
					TMATDependentTour tmatDependent = new TMATDependentTour();
					tmatDependent.setDependent(dependent);
					tmatDependent.setTourId(tour.getId());
					tmatDependent.setAdd(true);
					tmatDependents.add(tmatDependent);
					
				}
				if(!tmatDependents.isEmpty()){
					DAORegistry.getTmatDependentTourDAO().saveOrUpdateAll(tmatDependents);
				}
			}
		}
    	super.update(tour);
	}
	
	public void updateTour(Tour tour){
    	tour.setEventCount(DAORegistry.getEventDAO().getEventCountByTour(tour.getId()));
    	tour.setStartDate(DAORegistry.getEventDAO().getMinDateFromEvents(tour.getId()));
    	tour.setEndDate(DAORegistry.getEventDAO().getMaxDateFromEvents(tour.getId()));
		super.update(tour);
	}
	*//**
	 * get tour by name
	 *//*
    public Tour getTourByName(String name) {
    	// use like so that it is not case sensitive
    	name = name.trim();
    	List<Tour> tours = find("FROM Tour WHERE name LIKE ? AND tourStatus=?", new Object[]{name, TourStatus.ACTIVE});
    	if (tours == null || tours.isEmpty()) { 
    		return null;
    	}
    	return tours.get(0);    	
    }

    *//**
     * Merge several tours (mergeIds) into an existing tour (goodId)
     *//*
	public boolean mergeTours(Collection<Integer> mergeIds, Integer goodId,String userName) {
		Boolean result = true;
		for (Integer mergeId: mergeIds) {
			result &= mergeTours(mergeId, goodId,userName);
		}
		return result;
	}

	*//**
	 * Merge 2 tours, the tour1 (mergeId) into tour2 (goodId)
	 *//*
	public boolean mergeTours(Integer mergeId, Integer goodId,String userName) {	
		try {
			Tour mergeTour = DAORegistry.getTourDAO().get(mergeId);
			Tour goodTour = DAORegistry.getTourDAO().get(goodId);
			
			
			Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByTour(mergeTour.getId());
			
			
			if (events != null) {
				List<ManageBrokerAudit> manageBrokerAudits =new ArrayList<ManageBrokerAudit>();
				ManageBrokerAudit managebrokeraudit = null;
				Date now = new Date();
				
				for(Event event: events) {

					try {
						
						event.setTourId(goodTour.getId());
						String tourtype= goodTour.getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName();
						
						if ( tourtype.equalsIgnoreCase("Sports"))
						{
							event.setBrokerId(2);
							event.setBrokerStatus("ACTIVE");
						}
						else if ( tourtype.equalsIgnoreCase("Concerts"))
						{
							event.setBrokerId(2);
							event.setBrokerStatus("ACTIVE");
						}
						else if ( tourtype.equalsIgnoreCase("Theater"))
						{
							event.setBrokerId(3);
							event.setBrokerStatus("ACTIVE");
						}
						else if ( tourtype.equalsIgnoreCase("Other"))
						{
							event.setBrokerId(3);
							event.setBrokerStatus("ACTIVE");
						}
						event.setLastUpdate(new Date());
						logger.info(event.getLastUpdate() + ":" + event.getId() +":" + goodTour.getId() + ": from merge tour in tourDAO");
						System.out.println(event.getLastUpdate() + ":" + event.getId() +":" + goodTour.getId() + ": from merge tour in tourDAO");
						
						
						managebrokeraudit = new ManageBrokerAudit();
						managebrokeraudit = new ManageBrokerAudit();
						managebrokeraudit.setEventId(event.getId());
						managebrokeraudit.setBrokerId(event.getBrokerId());
						managebrokeraudit.setBrokerStatus(event.getBrokerStatus());
						managebrokeraudit.setUserName(userName);
						managebrokeraudit.setCreatedDate(now);
						managebrokeraudit.setAction("Merging Tour");
						manageBrokerAudits.add(managebrokeraudit);
						
						
//				    	DAORegistry.getCategoryMappingDAO().deleteAllByEvent(event.getId());
//						Categorizer.update(mergeTour.getId());

						List<TicketListingCrawl> crawls = DAORegistry.getTicketListingCrawlDAO().getTicketListingCrawlByEvent(event.getId());
	
						if (crawls != null) {
							for(TicketListingCrawl crawl: crawls) {
								crawl.setTourId(goodTour.getId());
								DAORegistry.getTicketListingCrawlDAO().update(crawl);
								SpringUtil.getTicketListingCrawler().updateTicketListingCrawl(crawl);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						return false;
					}
				}
				
				DAORegistry.getEventDAO().updateAll(events);
				DAORegistry.getManageBrokerAuditDAO().saveAll(manageBrokerAudits);
			}

//			Collection<Category> cats = DAORegistry.getCategoryDAO().getAllCategories(mergeTour.getId()); 
//			if (cats != null) {
//				for(Category cat: cats) {
//					DAORegistry.getCategoryDAO().delete(cat);
//				}
//			}

			Map<String, TourPriceAdjustment> adjs = DAORegistry.getTourPriceAdjustmentDAO().getPriceAdjustments(mergeTour.getId());
			if (adjs != null) {
				for(TourPriceAdjustment adj: adjs.values()) {
					DAORegistry.getTourPriceAdjustmentDAO().delete(adj);				
				}
			}

			DAORegistry.getTourDAO().deleteById(mergeId);
			DAORegistry.getTourDAO().update(goodTour);
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	
	public Collection<Tour> getAllActiveToursByGrandChildCategoryId(Integer id) {
		return find("FROM Tour WHERE grandChildTourCategory.id = ? and tourStatus = ? ", new Object[]{id,TourStatus.ACTIVE});
	}

	
	public Tour getSingleActiveTourByKeywords(String[] keywords) {
		String hql = "FROM Tour WHERE tourStatus=? ";
		List<Object> params= new ArrayList<Object>();
		params.add(TourStatus.ACTIVE);
		for(String keyword:keywords){
			hql+=" AND name like ?";
			params.add("%" + keyword + "%");
		}
		return findSingle(hql, params.toArray());
	}

	
	public Collection<Tour> getAllToursFromTNowInstatnEvent() {
		return find("SELECT t FROM Tour t WHERE t.id in(SELECT DISTINCT tie.tour.id FROM TnowInstantEvent tie) ORDER BY name");
	}
	
	
	public Collection<Tour> getAllToursFromTNInstatnEvent() {
		return find("SELECT t FROM Tour t WHERE t.id in(SELECT DISTINCT tie.tour.id FROM TNInstantEvent tie ) ORDER BY name");
	}

	
	public Collection<Tour> getAllActiveToursByGrandChildCategorys(List<Integer> ids) {
			Query query = getSession().createQuery("FROM Tour where grandChildTourCategory.id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<Tour> list= query.list();
		  return list;
	}

	
	public List<Tour> getTourByIds(List<Integer> ids) {
		Query query = getSession().createQuery("FROM Tour where id in (:Ids)  ORDER By name").setParameterList("Ids", ids);
		   
		  @SuppressWarnings("unchecked")
		  List<Tour> list= query.list();
		  return list;
	}

	@Override
	public void bulkDelete(List<Integer> ids) {
		Query query = getSession().createQuery("UPDATE Tour SET tourStatus = :status  where id in (:Ids)").setParameter("status", TourStatus.DELETED).setParameterList("Ids", ids);
		query.executeUpdate();
		
	}
	
	public List<Object[]> getNoSalesReportByTour(){
		
		Session session = getSession();
		List<Object[]> list = null;
		StringBuilder queryString = new StringBuilder();
		
		try{
			queryString.append("select distinct t.name as tour_name,t.start_date,t.end_date,t.type,v.building as ");
			queryString.append("venue_name from event e inner join (select distinct event_id from mini_tn_exchange_event "); 
			queryString.append("union select event_id from mini_vividseat_exchange_event union select event_id ");
			queryString.append("from mini_scorebig_exchange_event) as x on x.event_id=e.id inner join tour t on ");
			queryString.append("t.id=e.tour_id inner join venue v on v.id=e.venue_id where x.event_id in ( ");
			queryString.append("select id from event where admitone_id in(select exchange_event_id from "); 
			queryString.append("[TN1].[indux].[dbo].[event] where event_id not in (select distinct e.event_id "); 
	        queryString.append("from [TN1].[indux].[dbo].[invoice] i inner join [TN1].[indux].[dbo].[category_ticket] ");
	        queryString.append("ct on ct.invoice_id=i.invoice_id inner join [TN1].[indux].[dbo].[category_ticket_group] ctg ");
			queryString.append("on ctg.category_ticket_group_id=ct.category_ticket_group_id inner join ");
			queryString.append("[TN1].[indux].[dbo].[event] e on e.event_id=ctg.event_id) and event_datetime >= GETDATE()))");
			
			Query query = session.createSQLQuery(queryString.toString());
			list = query.list();
						
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}*/
}
