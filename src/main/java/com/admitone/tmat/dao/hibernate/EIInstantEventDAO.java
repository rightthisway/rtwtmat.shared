package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.EIInstantEvent;
import com.admitone.tmat.data.Event;

public class EIInstantEventDAO extends HibernateDAO<Integer, EIInstantEvent> implements com.admitone.tmat.dao.EIInstantEventDAO{

		
		public Collection<Event> getAllInstantEventsByTour(Integer tourId) {
		//	return find("FROM InstantEvent WHERE tourId=? ORDER BY name,event_date, event_time Asc", new Object[]{tourId});
		//	return find("FROM InstantEvent WHERE tourId=?", new Object[]{tourId});
			return find("SELECT e FROM Event e, EIInstantEvent i WHERE i.id=e.id AND e.tourId=? AND e.eventStatus='ACTIVE' ORDER BY e.localDate, e.localTime ASC", new Object[]{tourId});		
		
		}		
		
		public Collection<EIInstantEvent> getInstantEventsByTour(Integer tourId) {
		//	return find("FROM InstantEvent WHERE tourId=? ORDER BY name,event_date, event_time Asc", new Object[]{tourId});
		//	return find("FROM InstantEvent WHERE tourId=? ", new Object[]{tourId});
			return find("SELECT i FROM Event e, EIInstantEvent i WHERE i.id=e.id AND e.tourId=? AND e.eventStatus='ACTIVE' ORDER BY e.localDate, e.localTime ASC", new Object[]{tourId});
		}

		public int getInstantEventCountByTour(Integer tourId) {
			List list = find("SELECT count(i.id) FROM EIInstantEvent i, Event e WHERE i.id=e.id and e.tourId=? AND e.eventStatus = 'ACTIVE'", new Object[]{tourId});
			if (list == null || list.get(0) == null) {
				return 0;
			}
			return ((Long)list.get(0)).intValue();		
		}
}
