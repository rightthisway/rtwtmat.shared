package com.admitone.tmat.dao.hibernate;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.admitone.tmat.data.ActiveEventAndActiveCrawls;


public class ActiveEventAndActiveCrawlsDAO extends HibernateDAO<Integer,ActiveEventAndActiveCrawls> implements com.admitone.tmat.dao.ActiveEventAndActiveCrawlsDAO{

	@Override
	public List<ActiveEventAndActiveCrawls> getActiveEventAndActiveCrawl(String applyDate) {
		// TODO Auto-generated method stub
		
		Session session=getSession();	
		List<ActiveEventAndActiveCrawls> activeEventAndActiveCrawls=null;
		   
			try {
		
			String sql =" SELECT * FROM tmat_active_events_crawls WHERE convert(VARCHAR(10), stats_date, 101)='"+applyDate.toString()+"'";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addEntity(ActiveEventAndActiveCrawls.class);
			activeEventAndActiveCrawls= query.list();	
		   
		   
		}catch(Exception e){
			e.printStackTrace();
			
		}finally{
			session.close();
		}
		
		return activeEventAndActiveCrawls;
	
	}

}
