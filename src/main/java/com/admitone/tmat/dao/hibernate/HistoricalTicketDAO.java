package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.HistoricalTicket;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.utils.TicketUtil;

public class HistoricalTicketDAO extends HibernateDAO<Integer, HistoricalTicket> implements com.admitone.tmat.dao.HistoricalTicketDAO {
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM HistoricalTicket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantities != null && !quantities.isEmpty()) {
			query += " AND remainingQuantity IN (" + quantities.get(0);
			for (int i = 1 ; i < quantities.size() ; i++) {
				query += ", " + quantities.get(i);
			}
			query += ") ";
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		Collection<HistoricalTicket> historicalTickets; 
		
		if (startDate != null) {
			historicalTickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			historicalTickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}
		
		for (HistoricalTicket hticket: historicalTickets) {
			tickets.add(new Ticket(hticket));
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if (tickets == null) {
			return null;
		}
		
		if (tickets.size() == 0) {
			return null;
		}
		
		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}
	
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, List<Integer> categoryIds, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM HistoricalTicket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantities != null && !quantities.isEmpty()) {
			query += " AND remainingQuantity IN (" + quantities.get(0);
			for (int i = 1 ; i < quantities.size() ; i++) {
				query += ", " + quantities.get(i);
			}
			query += ") ";
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";
//		} else {
//			query += " AND category_id =" + categoryId;
//		}

		
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		Collection<HistoricalTicket> historicalTickets; 
		
		if (startDate != null) {
			historicalTickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			historicalTickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}
		
		for (HistoricalTicket hticket: historicalTickets) {
			tickets.add(new Ticket(hticket));
		}

		if (categoryIds == null || categoryIds.get(0).equals(Category.ALL_ID)) {
		} else if (categoryIds.get(0).equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryIds.get(0).equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategories(categoryIds, tickets);
		}

		if (tickets == null) {
			return null;
		}
		
		if (tickets.size() == 0) {
			return null;
		}
		
		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}

	public Collection<Ticket> getAllTicketsByEventAndDate(Integer eventId, Date date) {
		Collection<HistoricalTicket> htickets = find("FROM HistoricalTicket WHERE eventId=? AND date(lastUpdate)=date(?) ORDER BY currentPrice ASC", new Object[]{eventId, date});
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		for (HistoricalTicket hticket: htickets) {
			Ticket ticket = new Ticket(hticket);
			tickets.add(ticket);
		}
		
		return tickets;
	}

	public Collection<Ticket> getAllTicketsByEvent(Integer eventId) {
//		Collection<HistoricalTicket> htickets = find("FROM HistoricalTicket WHERE eventId=? ORDER BY currentPrice ASC", new Object[]{eventId});
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
//		for (HistoricalTicket hticket: htickets) {
//			Ticket ticket = new Ticket(hticket);
//			tickets.add(ticket);
//		}
		
		return tickets;
	}

	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean removeA1Tix, String catScheme) {
		String query = "FROM HistoricalTicket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantity != null && quantity > 0) {
			query += " AND remainingQuantity=" + quantity;
		}
		
		if (section != null && !section.isEmpty()) {
			if(section.contains("OR")){
				return null;
			}else if(section.contains("-")){
				
				String sectionArray[] = section.split("-");
				
				int i=0;
				for (String secObj : sectionArray) {
					i++;
				}
				if(i > 1 ){
					return null;
				}
				
				section =sectionArray[0].replace(" ", "");
				query += "AND lower(section) like '"+section+"'";	
								
			}
		}

		/*if (section != null && !section.isEmpty()) {
			query = addSectionToQuery(query, section);
		}

		if (row != null && !row.isEmpty()) {
			String[] cleanedRow = new String[4];
			
			if(row.contains("-")){
				cleanedRow =row.split("-");
				
				if(cleanedRow[0].matches("\\d+")){
					query += " AND lower(row) IN ( ";
					Integer start=null,end=null;
					Integer i =Integer.parseInt(cleanedRow[0].trim());
					Integer j =Integer.parseInt(cleanedRow[1].trim());
					if(i >j){
						start = j;
						end=i;
						query +="'"+start+"-"+end+"'";
					}else{
						start = i;
						end=j;
						query +="'"+start+"-"+end+"'";
					}
					int first=start;
					for(int k=start;k <=end;k++){
						if(k == first){
							query +="'"+k+"'";
						}else{
							query +=",'"+k+"'";
						}
					}
					query += " ) ";
				}else{
					query += " AND lower(row) like '%" + cleanedRow[0].toLowerCase() + "%'";
				}
			}else{
				query += " AND lower(row) IN ( '" + row.toLowerCase() + "' )";
			}
		}*/

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//			// do nothing: we select all categories
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";			
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";			
//		} else {
//			query += " AND category_id=" + categoryId;
//		}


		Collection<HistoricalTicket> htickets = new ArrayList<HistoricalTicket>();
		if (startDate != null) {
			htickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			htickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}
		
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		
		for (HistoricalTicket hticket: htickets) {
			Ticket ticket = new Ticket(hticket);
			tickets.add(ticket);
		}
		
		if (tickets.size() == 0) {
			return null;
		}

		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}
	
public String addSectionToQuery(String query,String section){
		
		String[] splittedSection = new String[10];
		String[] innerSection = new String[10];
		query += "AND lower(section) IN ( ";
		if(section.contains("OR")){
			splittedSection =section.split("OR");
			for (String splitSection : splittedSection) {
				
						splitSection = splitSection.replace(" ", "");
						splitSection = splitSection.replace(" ", "");
						innerSection = new String[10];
						innerSection = splitSection.split("-");
						Integer i = Integer.parseInt(innerSection[0].trim());
						Integer j = Integer.parseInt(innerSection[1].trim());
						Integer start=null;
						Integer end=null;
						if(i > j){
							start = j;
							end=i;
							query +="'"+start+"-"+end+"'";
						}else{
							start = i;
							end=j;
							query +="'"+start+"-"+end+"'";
						}
						int first=start;
						for(int k=start;k <=end;k++){
							
							if(k == first){
								query +="'"+k+"'";
							}else{
								query +=",'"+k+"'";
							}
						}
				}
			
		}else if(section.contains("-")){
			query +="'"+section+"'";
			splittedSection =section.split("-");
			if(null != splittedSection && splittedSection.length >0){
				Integer Start = Integer.parseInt(splittedSection[0]);
				Integer end = Integer.parseInt(splittedSection[1]);
				int j=Start;
				for(int i=Start;i <=end;i++){
					if(i == j){
						query +="'"+i+"'";
					}else{
						query +=",'"+i+"'";
					}
				}
			}
		}else if(section.contains(" ")){
			splittedSection =section.split(" ");
			
			if(null != splittedSection && splittedSection.length >0){
				
				Integer start =Integer.parseInt(splittedSection[0]);
				Integer end =Integer.parseInt(splittedSection[1]);
				int j=start;
				for(int i=start;i <=end;i++){
					if(i == j){
						query +="'"+i+"'";
					}else{
						query +=",'"+i+"'";
					}
				}
				
				 start =Integer.parseInt(splittedSection[2]);
				 end =Integer.parseInt(splittedSection[3]);
				 j=start;
				for(int i=start;i <=end;i++){
					if(i == j){
						query +="'"+i+"'";
					}else{
						query +=",'"+i+"'";
					}
				}
					
			}
		}else{
			section =section.replace(" ", "");
			query +="'"+section+"'";
			
		}
		query +=  " )";
		return query;
	}


	public List<HistoricalTicket> getAllTmatTicketsById(Integer Id) {
		return find("FROM HistoricalTicket where id=?" ,new Object[]{Id});
	}
	@SuppressWarnings("unchecked")
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, Date startDate, Date endDate, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		String query = "FROM HistoryTicket WHERE eventId=? ";
		
		if (startDate != null) {
			query += " AND ? >= insertionDateWithoutTime AND ? <= lastUpdateWithoutTime ";
		}
		
		if (keepOnlyActive) {
			query += " AND ticket_status='ACTIVE' ";
		}
		
		if (siteIds != null && siteIds.length > 0) {
			query += " AND siteId IN ('" + siteIds[0] + "'";
			for (int i = 1 ; i < siteIds.length ; i++) {
				query += ", '" + siteIds[i] + "'";
			}
			query += ") ";
		}
				
		if (quantity != null && quantity > 0) {
			query += " AND remainingQuantity=" + quantity;
		}

		if (section != null && !section.isEmpty()) {
			query += " AND lower(section) like '%" + section.toLowerCase() + "%'";
		}

		if (row != null && !row.isEmpty()) {
			String cleanedRow = row.replace("-", " ").trim();
			query += " AND lower(row) like '%" + cleanedRow.toLowerCase() + "%'";
		}

		if (lotSize != null && lotSize > 0) {
			query += " AND lotSize=" + lotSize;
		}

//		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
//			// do nothing: we select all categories
//		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
//			query += " AND category_id is null";			
//		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
//			query += " AND category_id is not null";			
//		} else {
//			query += " AND category_id=" + categoryId;
//		}

		
		Collection<HistoricalTicket> htickets;
		
		if (startDate != null) {
			htickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId,
					new Date((startDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L)),
					new Date((endDate.getTime() / (24L * 3600L * 1000L)) * (24L * 3600L * 1000L))});
		} else {
			htickets = find(query + " ORDER BY current_price ASC", new Object[]{eventId});			
		}
		
		if (htickets.size() == 0) {
			return null;
		}

		Collection<Ticket> tickets = new ArrayList<Ticket>();
		for (HistoricalTicket hticket: htickets) {
			tickets.add(new Ticket(hticket));
		}
		
		if (categoryId == null || categoryId.equals(Category.ALL_ID)) {
		} else if (categoryId.equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else if (categoryId.equals(Category.UNCATEGORIZED_ID)) {
			tickets = TicketUtil.filterUncategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategory(categoryId, tickets);
		}

		if(removeA1Tix){
			tickets = TicketUtil.removeAdmitOneTickets(tickets);
		}
		
		return TicketUtil.removeDuplicateTickets(tickets, removeDuplicatePolicy);
	}

}
