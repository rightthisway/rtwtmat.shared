package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.TMATDependentArtistMerge;

public class TMATDependentArtistMergeDAO extends HibernateDAO<Integer, TMATDependentArtistMerge> implements
		com.admitone.tmat.dao.TMATDependentArtistMergeDAO {
	
	
	public List<TMATDependentArtistMerge> getTMATDependentArtistMergeByArtistIdAndMergeArtistId(Integer ArtistId, Integer toMergedArtistId){
		return find("FROM TMATDependentArtistMerge WHERE ArtistId= ? AND toMergedArtistId = ?  ",new Object[]{ArtistId,toMergedArtistId});
	}

}
