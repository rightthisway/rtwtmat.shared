package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.EventAudit;

public class EventAuditDAO extends HibernateDAO<Integer,EventAudit> implements com.admitone.tmat.dao.EventAuditDAO{

	public List<EventAudit> getEventAuditByEventId(Integer eventId) {
		
		Session session = getSession();
		List<EventAudit> list = new ArrayList<EventAudit>();
		try{
			Query query = session.createQuery("FROM EventAudit where eventId = :eventId order by modifiedDate");
			query.setParameter("eventId", eventId);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
public List<EventAudit> getEventRemovedByAction() {
		
		Session session = getSession();
		List<EventAudit> list = new ArrayList<EventAudit>();
		try{
			Query query = session.createQuery("FROM EventAudit where action = 'Removed' order by modifiedDate desc");
			//query.setParameter("eventId", eventId);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
public List<EventAudit>  getAllEventsByTourId(Integer tourId) {
	
	Session session = getSession();
	List<EventAudit> list = new ArrayList<EventAudit>();
	try{
		Query query = session.createQuery("FROM EventAudit where artistId = :tourId and action='Removed' order by modifiedDate desc");
		query.setParameter("tourId", tourId);
		list= query.list();
	}catch (Exception e) {
		e.printStackTrace();
	}finally{
		session.close();
	}				
	return list;
}

public List<EventAudit> getAllEventsByVenueId(Integer venueId){
	
	Session session = getSession();
	List<EventAudit> list = new ArrayList<EventAudit>();
	try{
		Query query = session.createQuery("FROM EventAudit where venueId = :venueId and action='Removed' order by modifiedDate desc");
		query.setParameter("venueId", venueId);
		list= query.list();
	}catch (Exception e) {
		e.printStackTrace();
	}finally{
		session.close();
	}				
	return list;
}
}