package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.PresaleEvent;

public class PresaleEventDAO extends HibernateDAO<Integer, PresaleEvent> implements com.admitone.tmat.dao.PresaleEventDAO {

	@Override
	public Collection<PresaleEvent> getAllPresaleEventsByArtistIdAndVenueId(
			Integer artistId, Integer venueId) {
		String sql =" FROM PresaleEvent WHERE status = 'ACTIVE' ";
		if(artistId!=null && artistId!=0){
			sql+=" AND event.artistId = " + artistId;
		}
		if(venueId!=null && venueId!=0){
			sql+=" AND event.venueId = " + venueId;
		}
		
		return find(sql);
	}

	/*@Override
	public Collection<PresaleEvent> getAllEligiblePresaleEvents() {
		return find("FROM PresaleEvent WHERE applyTime < ? and status = 'ACTIVE' ", new Object[]{new Date()});
	}*/
	public Collection<PresaleEvent> getAllEligiblePresaleEvents(Date createdDateParam) {
		return find("FROM PresaleEvent WHERE (applyTime <= ?) and status = 'ACTIVE' ", new Object[]{new Date()});//createdDate<= ? or//createdDateParam
	}
	
	public PresaleEvent getActivePresaleEventByEventId(Integer eventId) {
		return findSingle("FROM PresaleEvent WHERE event.id=? and status = 'ACTIVE' ", new Object[]{eventId});
	}

}
