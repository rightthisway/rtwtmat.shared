package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserAction;
import com.admitone.tmat.web.Constants;

public class UserActionDAO extends HibernateDAO<Integer, UserAction> implements com.admitone.tmat.dao.UserActionDAO {
	public Collection<UserAction> getActionsByUsername(String username) {
		Collection<UserAction> list = find("FROM UserAction WHERE username=? ORDER BY tstamp ASC", new Object[]{username});
		if (list.size() == 0) {
			return null;
		}
		
		return list;
	}	
	
	@Override
	public Collection<UserAction> getAll() {
		Collection<UserAction> list = find("FROM UserAction ORDER BY tstamp ASC");
		if (list.size() == 0) {
			return null;
		}
		
		return list;
	}
	
	public void logUserAction(User user, String action, String page,String ipAddress,Date timeStamp){
		
		
		if(action.equalsIgnoreCase("/Logout")){
			if(null != user){
				String appName = Constants.getInstance().getHostName();
				DAORegistry.getLiveUserDAO().updateLiveUserByUserName(user.getUsername(),appName, 'N');
			}
		}
		UserAction userAction = new UserAction();
		userAction.setAction(action);
		userAction.setTimeStamp(timeStamp);
		userAction.setUser(user);
		userAction.setIpAddress(ipAddress);
		
		DAORegistry.getUserActionDAO().save(userAction);
	}

	public Collection<UserAction> getActionsByUsernameAndDate(String userName,Date startDate, Date endDate) {
		Collection<UserAction> list = null;
		List<Object> params = new ArrayList<Object>();
		String sql ="FROM UserAction WHERE 1=1";
		if(startDate != null ){
			sql += " AND timeStamp >= ?";
			params.add(startDate);
		}
		if(endDate != null){
			sql += " AND timeStamp <= ?";
			params.add(endDate);
		}
		if(userName !=null){
			sql += " AND user.username = ?";
			params.add(userName);
		}
		list = find(sql + " ORDER BY user.username,timeStamp" ,params.toArray());
		if (list.size() == 0) {
			return null;
		}
		return list;
	}
	
	public void deleteUserActionByUserName(String userName){
		
		bulkUpdate("DELETE FROM UserAction WHERE user.username=?", new Object[]{userName});
		
	}
	
   public void deleteUserActionByUserId(Integer userId){
		
		bulkUpdate("DELETE FROM UserAction WHERE user_id=?", new Object[]{userId});
		
	}
	
}
