package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ShortTransaction;

/**
 * Short Transaction DAO.
 *
 */
public class ShortTransactionDAO extends HibernateDAO<Integer, ShortTransaction> implements com.admitone.tmat.dao.ShortTransactionDAO {

	/**
	 * Get short transaction by cover id.
	 */
	public Collection<ShortTransaction> getByCoverId(int transId) {
		Collection<ShortTransaction> list = find("FROM ShortTransaction WHERE coverId=?", new Object[]{transId});
		if (list.size() == 0) {
			return null;
		}
		
		return list;
	}
	
	public Collection<ShortTransaction> getAllShortsByEvent(int eventId) {
		Event event = DAORegistry.getEventDAO().get(eventId);
		
		if (event == null || event.getAdmitoneId() == null) {
			return new ArrayList<ShortTransaction>();
		}
		
		return find("FROM ShortTransaction WHERE admitoneId=?", new Object[]{event.getAdmitoneId()});
	}
	
	public void deleteShortsFromUser(String username) {
		//bulkUpdate("DELETE FROM ShortTransaction WHERE username=?", new Object[]{username});
		Collection<ShortTransaction> oldTranses = find("FROM ShortTransaction WHERE username=?", new Object[]{username});
		if(oldTranses.size() == 0){
			return;
		}
		for(ShortTransaction oldTrans : oldTranses){
			super.deleteById(oldTrans.getId());
		}
	}
	
	public Collection<ShortTransaction> deleteShortsByEvent(int eventId) {
		Collection<ShortTransaction> shorts = getAllShortsByEvent(eventId);
		for(ShortTransaction oldTrans : shorts){
			this.deleteById(oldTrans.getId());
		}
		return shorts;
	}
	
	public void delete(ShortTransaction trans) {
		this.deleteById(trans.getId());
	}
}
