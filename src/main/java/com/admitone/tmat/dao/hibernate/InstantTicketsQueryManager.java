package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.admitone.tmat.data.Ticket;


public class InstantTicketsQueryManager {
	private SessionFactory sessionFactory;
	
	public HashMap<Integer,Collection<Ticket>> getAllInstantTickets(){
		String sqlQueryString = 
			"SELECT "
			+ "    e.name, "
			+ "    v.building, "
			+ "    e.event_date, "
			+ "    e.event_time, "
			+ "    t.remaining_quantity, "
			+ "    t.normalized_section, "
			+ "    t.row, "
			+ "    t.seat, "
			+ "    t.current_price, "
			+ "    t.id ticket_id, "
			+ "	   e.id event_id "
			+ "FROM ticket t "
			+ "INNER JOIN event e ON t.event_id=e.id "
			+ "INNER JOIN venue v ON v.id=e.venue_id "
			+ "WHERE "
			+ "t.ticket_status='active' "
			+"AND "
			+"t.ticket_delivery_type = 'instant'";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		
		
		Session  session = null;
	//	Collection<InstantTickets> instantTickets = new ArrayList<InstantTickets>();
		//Collection<Ticket> instantTickets = new ArrayList<Ticket>();
		HashMap<Integer, Collection<Ticket>> instantTicketsByEvent = new HashMap<Integer, Collection<Ticket>>();
		
		try {
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sqlQueryString);
			sqlQuery.setProperties(parameters);
			
			// build object from result
			for(Object[] resultRow: (List<Object[]>)sqlQuery.list()) {
	//			InstantTickets instantTicket = new InstantTickets();
				Ticket instantTicket = new Ticket();
				instantTicket.setEventId((Integer)resultRow[10]);
				instantTicket.setEventName((String)resultRow[0]);
				instantTicket.setVenue((String)resultRow[1]);
				instantTicket.setEventDate((Date)resultRow[2]);
				instantTicket.setEventTime((Date)resultRow[3]);
				instantTicket.setRemainingQuantity((Integer)resultRow[4]);
				instantTicket.setNormalizedSection((String)resultRow[5]);
				instantTicket.setRow((String)resultRow[6]);
				instantTicket.setSeat((String)resultRow[7]);
				instantTicket.setCurrentPrice((Double) resultRow[8]);
		//		instantTicket.setTicketId((Integer)resultRow[9]);
				instantTicket.setId((Integer)resultRow[9]);
				Integer eventId =  (Integer)resultRow[10];
				Collection<Ticket> instantTickets = instantTicketsByEvent.get(eventId);
				if(instantTickets == null){
					instantTickets = new ArrayList<Ticket>();
				}
				instantTickets.add(instantTicket);
				instantTicketsByEvent.put(eventId, instantTickets);
				
			}
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {		
			session.close();
		}
		return instantTicketsByEvent;
	}
	
	//public Collection<InstantTickets> getInstantTickets(Integer eventId) {
		public Collection<Ticket> getInstantTickets(Integer eventId) {

		String sqlQueryString = 
			"SELECT "
			+ "    e.name, "
			+ "    v.building, "
			+ "    e.event_date, "
			+ "    e.event_time, "
			+ "    t.remaining_quantity, "
			+ "    t.normalized_section, "
			+ "    t.row, "
			+ "    t.seat, "
			+ "    t.current_price, "
			+ "    t.id "
			+ "FROM ticket t "
			+ "INNER JOIN event e ON t.event_id=e.id "
			+ "INNER JOIN venue v ON v.id=e.venue_id "
			+ "WHERE "
			+ "t.ticket_status='active' "
			+"AND "
			+"t.ticket_delivery_type = 'instant'";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		
/*	if (eventIds != null && eventIds.length > 0) {
			String eventIdsString = "";
			for(Integer eventId: eventIds) {
				if (!eventIdsString.isEmpty()) {
					eventIdsString += ",";
				}
				eventIdsString += eventId;
			}
			sqlQueryString += " AND t.event_id IN (" + eventIdsString + ")";
		}

*/
		sqlQueryString += " AND t.event_id=" + eventId;
		
		Session session = null;
	//	Collection<InstantTickets> instantTickets = new ArrayList<InstantTickets>();
		Collection<Ticket> instantTickets = new ArrayList<Ticket>();
		
		try {
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sqlQueryString);
			sqlQuery.setProperties(parameters);
			
			// build object from result
			for(Object[] resultRow: (List<Object[]>)sqlQuery.list()) {
	//			InstantTickets instantTicket = new InstantTickets();
				Ticket instantTicket = new Ticket();
				instantTicket.setEventId(eventId);
				instantTicket.setEventName((String)resultRow[0]);
				instantTicket.setVenue((String)resultRow[1]);
				instantTicket.setEventDate((Date)resultRow[2]);
				instantTicket.setEventTime((Date)resultRow[3]);
				instantTicket.setRemainingQuantity((Integer)resultRow[4]);
				instantTicket.setNormalizedSection((String)resultRow[5]);
				instantTicket.setRow((String)resultRow[6]);
				instantTicket.setSeat((String)resultRow[7]);
				instantTicket.setCurrentPrice((Double) resultRow[8]);
		//		instantTicket.setTicketId((Integer)resultRow[9]);
				instantTicket.setId((Integer)resultRow[9]);
				instantTickets.add(instantTicket);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {		
			session.close();
		}
		return instantTickets;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}		
}
