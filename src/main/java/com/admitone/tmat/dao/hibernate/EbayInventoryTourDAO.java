package com.admitone.tmat.dao.hibernate;

import com.admitone.tmat.data.EbayInventoryTour;
//import com.admitone.tmat.data.Tour;

public class EbayInventoryTourDAO extends HibernateDAO<Integer, EbayInventoryTour> implements com.admitone.tmat.dao.EbayInventoryTourDAO {
	/*public Collection<Tour> getAllToursWithEbayInventoryEvents() {
		return find("SELECT t FROM Tour t WHERE t.id IN (SELECT tourId FROM EbayInventoryTour)");
	}
	
	public Collection<Tour> getAllToursWithEbayInventoryEventsByUsername(String username) {
		return find("SELECT t FROM Tour t WHERE t.id IN (SELECT e.tourId FROM Event e, EbayInventoryEvent ee WHERE e.id=ee.id AND ee.username=?) ORDER BY t.id", new Object[]{username});
//		
//		Set<Tour> tours = new TreeSet<Tour>(new Comparator<Tour>() {
//			public int compare(Tour tour1, Tour tour2) {
//				return tour1.getId().compareTo(tour2.getId());
//			}
//		});
//				
//		tours.addAll(find("SELECT t FROM Tour t, EbayInventoryEvent ee, Event e WHERE e.id=ee.id and e.tourId=t.id and ee.username=?", new Object[]{username}));
//		return tours;
	}

	public Collection<EbayInventoryTour> getAllEbayInventoryToursByUsername(String username) {
		Set<EbayInventoryTour> tours = new TreeSet<EbayInventoryTour>(new Comparator<EbayInventoryTour>() {
			public int compare(EbayInventoryTour tour1, EbayInventoryTour tour2) {
				return tour1.getTourId().compareTo(tour2.getTourId());
			}
		});
				
		tours.addAll(find("SELECT et FROM EbayInventoryTour et, EbayInventoryEvent ee, Event e WHERE e.id=ee.id and e.tourId=et.tourId and ee.username=?", new Object[]{username}));
		return tours;
	}*/

}