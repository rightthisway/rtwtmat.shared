package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.EventSynonym;

public class EventSynonymDAO extends HibernateDAO<Integer, EventSynonym> implements com.admitone.tmat.dao.EventSynonymDAO {

	public EventSynonym getEventSynonymByMerged(Integer mergedId) {
		Collection<EventSynonym> list = find("FROM EventSynonym WHERE mergedId=? ", new Object[]{mergedId});
		if (list.size() == 0) {
			return null;
		}
		return list.iterator().next();
	}

	public Collection<EventSynonym> getEventSynonymByExisting(Integer existingId) {
		Collection<EventSynonym> list = find("FROM EventSynonym WHERE eventId=? ", new Object[]{existingId});
		if (list.size() == 0) {
			return null;
		}
		return list;
	}

	public void deleteSynonyms(Integer eventId) {
		bulkUpdate("DELETE FROM EventSynonym WHERE event_id=?", new Object[]{eventId});
	}
}
