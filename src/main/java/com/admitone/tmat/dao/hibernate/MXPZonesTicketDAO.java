package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.MXPZonesTicket;

public class MXPZonesTicketDAO extends HibernateDAO<Integer, MXPZonesTicket> implements
		com.admitone.tmat.dao.MXPZonesTicketDAO {

	
	@Override
	public Integer save(MXPZonesTicket t) {
		return null;
	}

	@Override
	public void update(MXPZonesTicket t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void delete(MXPZonesTicket t) {
		// TODO Auto-generated method stub

	}

	@Override
	public void deleteById(Integer id) {
		// TODO Auto-generated method stub

	}

	@Override
	public void saveOrUpdate(MXPZonesTicket entity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void saveOrUpdateAll(Collection<MXPZonesTicket> list) {
		// TODO Auto-generated method stub

	}

	@Override
	public void saveAll(Collection<MXPZonesTicket> list) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean updateAll(Collection<MXPZonesTicket> list) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void deleteAll(Collection<MXPZonesTicket> list) {
		// TODO Auto-generated method stub

	}

	public List<MXPZonesTicket> getMXPZonesTicketByTNEventId(Integer eventId) {
		
		return find("FROM MXPZonesTicket WHERE tnEventId=?",new Object[]{eventId});
	}


}
