package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.TnExchangeTheatreEventZones;

public class TnExchangeTheatreEventZonesDAO extends HibernateDAO<Integer, TnExchangeTheatreEventZones> implements com.admitone.tmat.dao.TnExchangeTheatreEventZonesDAO{

	
	public TnExchangeTheatreEventZones getTnExchangeTheatreEventZones(Integer zoneId){
		try{
			return findSingle("FROM TnExchangeTheatreEventZones where id=?", new Object[]{zoneId});
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
	
	public List<TnExchangeTheatreEventZones> getAllTnExchangeTheatreEventZones(){
		try{
			return find("FROM TnExchangeTheatreEventZones order by id");
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
		
	}
}
