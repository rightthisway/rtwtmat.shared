package com.admitone.tmat.dao.hibernate;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEvent;
import com.admitone.tmat.data.Broker;
import com.admitone.tmat.data.BrokerSoldTicketDetail;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CrawlerHistoryManagement;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.LarryLastTnCategoryTicket;
import com.admitone.tmat.data.NewPOSEvents;
import com.admitone.tmat.data.SoldTicketDetail;
import com.admitone.tmat.data.VWLastRowMiniCatsCategoryTicket;
import com.admitone.tmat.data.VWTGCatsCategoryTicket;
import com.admitone.tmat.data.VWVipCatsCategoryTicket;
import com.admitone.tmat.data.VividSeatsTicketInventory;
import com.admitone.tmat.data.ZonePricingTnCategoryTicket;
import com.admitone.tmat.data.ZonedLastRowTnCategoryTicket;
import com.admitone.tmat.enums.CreationType;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.pojo.ClientBroker;
import com.admitone.tmat.pojo.EventCrawlerDetails;
import com.admitone.tmat.pojo.EventDTO;
import com.admitone.tmat.pojo.EventSectionZoneDetails;
import com.admitone.tmat.pojo.Invoice;
import com.admitone.tmat.pojo.POSTicketGroup;
import com.admitone.tmat.pojo.PosEvent;
import com.admitone.tmat.pojo.PosTicket;
import com.admitone.tmat.pojo.PurchaseOrder;
import com.admitone.tmat.pojo.RTFTicketGroup;
import com.admitone.tmat.pojo.SVGFileMissingZoneData;
import com.admitone.tmat.pojo.ShortLongTicketGroup;
import com.admitone.tmat.pojo.ShortTicketGroup;
import com.admitone.tmat.pojo.TNDEventSalesReportDetail;
import com.admitone.tmat.pojo.TNDRtfNoSaleReport;
import com.admitone.tmat.util.ScoreBigListedEvent;
import com.admitone.tmat.util.TGScoreBigListedEvent;
import com.admitone.tmat.util.TGTNListedEvent;
import com.admitone.tmat.util.TGTickPickListedEvent;
import com.admitone.tmat.util.TGVividListedEvent;
import com.admitone.tmat.util.TNListedEvent;
import com.admitone.tmat.util.TickPickListedEvent;
import com.admitone.tmat.util.VividListedEvent;


public class QueryManagerDAO {
	private SessionFactory sessionFactory;
	private String tnInduxDbName;
	private String stubJktPos;
	private String tn2RotPos;
	private String tixCityPos;
	private String tixCityNewPos;
	private String manhattanPos;
	private String rtwPos;
	private String rtwTwoPos;
	private String zoneTickets;
	
	public String getManhattanPos() {
		return manhattanPos;
	}

	public void setManhattanPos(String manhattanPos) {
		this.manhattanPos = manhattanPos;
	}

	public String getTixCityNewPos() {
		return tixCityNewPos;
	}

	public void setTixCityNewPos(String tixCityNewPos) {
		this.tixCityNewPos = tixCityNewPos;
	}
	
	public String getRtwPos() {
		return rtwPos;
	}

	public void setRtwPos(String rtwPos) {
		this.rtwPos = rtwPos;
	}

	public String getRtwTwoPos() {
		return rtwTwoPos;
	}

	public void setRtwTwoPos(String rtwTwoPos) {
		this.rtwTwoPos = rtwTwoPos;
	}

	public String getZoneTickets() {
		return zoneTickets;
	}

	public void setZoneTickets(String zoneTickets) {
		this.zoneTickets = zoneTickets;
	}

	/**
	 * Get information of all crawlers,which is present in crawler history management, by site id
	 * @param siteId
	 * @param system
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public  List<CrawlerHistoryManagement> getCrawlersBySiteId(String siteId,String system,Date startDate,Date endDate) {
		
		String sql="select t.id,name,sum(counter),system from ticket_listing_crawl t ";
		sql+=" inner join tmathistory.dbo.crawler_history_management chm on t.id=chm.crawl_id";
		sql+=" where t.site_id='" + siteId +"' AND system = '" + system + "'";
		if(startDate!=null){
			sql+= " AND successful_hit >= :startDate";
		}
		if(endDate!=null){
			sql+= " AND successful_hit <= :endDate";
		}
		sql+=" group by t.id,name,system";
		sql+=" order by sum(counter) desc";
		
		Session session=null;
		List<CrawlerHistoryManagement> list = new ArrayList<CrawlerHistoryManagement>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			if(startDate!=null)
				sqlQuery.setTimestamp("startDate", startDate);
			if(endDate!=null)
				sqlQuery.setTimestamp("endDate", endDate);
//			if(system !=null)
//				sqlQuery.setString("system", system);
			CrawlerHistoryManagement crawlerHistoryManagement= null;
			
			for(Object[] resultRow: (List<Object[]>)sqlQuery.list()) {
				crawlerHistoryManagement= new CrawlerHistoryManagement();
				crawlerHistoryManagement.setCrawlId((Integer)(resultRow[0]));
				crawlerHistoryManagement.setSiteId((String) resultRow[1]);
				crawlerHistoryManagement.setCounter((Integer)resultRow[2]);
				crawlerHistoryManagement.setSystem((String)resultRow[3]);
				list.add(crawlerHistoryManagement);
			}
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}finally{
			session.close();
		}
		return list;
	}
	
	public String getTixCityPos() {
		return tixCityPos;
	}

	public void setTixCityPos(String tixCityPos) {
		this.tixCityPos = tixCityPos;
	}

	public List<com.admitone.tmat.util.MiniCatsListedEvent> getAllZonesPricingCategoryEvents() {
		String sql =" SELECT distinct event_id as id,event_name as name,event_date as eventDate,venue_name as venue from tn_exchange_event_zone_group_ticket where pos_hit=1 and  hit_result='Success'";
		List<com.admitone.tmat.util.MiniCatsListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(com.admitone.tmat.util.MiniCatsListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
	
	public  List<Event> getAllEventsToBeUnlinked() {
		String sql="select id,event_date,artist_id,venue_id,timezone,event_time " ;
		sql+=" ,stubhub_id,creation_date,last_update,creation_type " ;
		sql+=" ,admitone_id,event_type,seatwave_id,auto_correct "; 
		sql+=" ,ignore_tbd_time,event_over_riden,name";//,zone_event ";
		sql+=" from event e ";
		sql+=" where e.event_status = 'ACTIVE' and e.admitone_id not in(select eventid from admitone_event) ";
		Session session=null;
		List<Event> list = new ArrayList<Event>();
//		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
//		SimpleDateFormat tf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			Event event= null;
			Collection<AdmitoneEvent> admitoneEvents = DAORegistry.getAdmitoneEventDAO().getAll();
			Date now = new Date();
			if (admitoneEvents.size()!= 0){
				for(Object[] resultRow: (List<Object[]>)sqlQuery.list()){
					Date date =(Date)resultRow[1];
					if((now.getTime() - date.getTime()) <= 6*24*60*60*1000){
						continue;
					}
					event = new Event();
					event.setId((Integer)(resultRow[0]));
					event.setLocalDate(date);
					event.setArtistId((Integer)(resultRow[2]));
					event.setVenueId((Integer)(resultRow[3]));
					
					event.setTimeZoneId((String)resultRow[4]);
					Object time = resultRow[5];
					if(time!=null){
						event.setLocalTime(new Time(((Timestamp)time).getTime()));
					}
					event.setEventStatus(EventStatus.ACTIVE);
					
					event.setStubhubId(resultRow[6]!=null?(Integer)(resultRow[6]):null);
					event.setCreationDate((Date)resultRow[7]);
					event.setLastUpdate((Date)resultRow[8]);
					
					String creationType = (String)resultRow[9];
					if(creationType==null){
						event.setCreationType(CreationType.ERROR);
					}else if(creationType.equalsIgnoreCase("AUTO")){
						event.setCreationType(CreationType.AUTO);
					}else if(creationType.equalsIgnoreCase("MANUAL")){
						event.setCreationType(CreationType.MANUAL);
					}else{
						event.setCreationType(CreationType.ERROR);
					}
					event.setAdmitoneId(resultRow[10]!=null?(Integer)(resultRow[10]):null);
					String eventType = (String)resultRow[11];
					if(eventType.equalsIgnoreCase("SPORT")){
						event.setEventType(TourType.SPORT);
					}else if(eventType.equalsIgnoreCase("CONCERT")){
						event.setEventType(TourType.CONCERT);
					}else if(eventType.equalsIgnoreCase("THEATER")){
						event.setEventType(TourType.THEATER);
					}else if(eventType.equalsIgnoreCase("OTHER")){
						event.setEventType(TourType.OTHER);
					}
					event.setSeatwaveId(resultRow[12]!=null?(Integer)(resultRow[12]):null);
					event.setAutoCorrect(resultRow[13]!=null?(Boolean)resultRow[13]:null);
					event.setIgnoreTBDTime(resultRow[14]!=null?(Boolean)resultRow[14]:null);
					event.setEventOverridden(resultRow[15]!=null?(String)resultRow[15]:null);
					event.setName((String)resultRow[16]);
//					event.setZoneEvent(resultRow[17]!=null?(Boolean)resultRow[17]:null);
	//				crawlerHistoryManagement.setCrawlId((Integer)(resultRow[0]));
	//				crawlerHistoryManagement.setSiteId((String) resultRow[1]);
	//				crawlerHistoryManagement.setCounter((Integer)resultRow[2]);
					list.add(event);
				}
			}
		}catch (Exception e) {
			return null;
		}finally{
			session.close();
		}
		return list;
	}

/**
 * Get Crawler information from specified System i.e tmat or ebayprod2
 * @param crawlId
 * @param system
 * @return
 */
	public TicketListingCrawl getCrawlSystemIndependently(Integer crawlId,String system){
		String sql="select id,name,site_id,crawl_frequency,creator,last_updater,last_successful_run,last_updated from tmatprod.dbo.ticket_listing_crawl t ";
		sql+=" where id=" + crawlId ;
		Session session=null;
		TicketListingCrawl crawl= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			
			
			Object[] resultRow=(Object[]) sqlQuery.uniqueResult();
			crawl= new TicketListingCrawl();
			crawl.setId((Integer)(resultRow[0]));
			crawl.setName((String) resultRow[1]);
			crawl.setSiteId((String)resultRow[2]);
			crawl.setCrawlFrequency((Integer)resultRow[3]);
			
			crawl.setCreator((String) resultRow[4]);
			crawl.setLastUpdater((String)resultRow[5]);
			crawl.setCreated((Date)resultRow[6]);
			crawl.setLastUpdated((Date)resultRow[7]);
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}
		finally{
			session.close();
		}
		return crawl;
	}
	
	public Map<String,Integer> getTicketListingCrawlsQueryUrlMap(){
		String sql="select distinct query_url,id from ticket_listing_crawl with(nolock) where enabled=1";
		Session session = null;
		TicketListingCrawl crawl = null;
		Map<String,Integer> crawlsQueryUrlMap = new HashMap<String, Integer>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object[]> result = sqlQuery.list();
			for(Object[] resultRow : result){
				crawlsQueryUrlMap.put((String) resultRow[0], (Integer)(resultRow[1]));
			}
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			session.close();
		}
		return crawlsQueryUrlMap;
	}

	public  List<Category> getAllCategoriesByVenueIdAndCategoryGroup(Integer venueId, String categoryGroup){
		String sql=" SELECT c.id,c.symbol,c.description,c.group_name as groupName,c.equal_cats as equalCats,c.normalized_zone as normalizedZone,";
			sql += " c.seat_quantity as seatQuantity,c.category_capacity as categoryCapacity,c.venue_category_id as venueCategoryId FROM category_new c "; 
			sql += " INNER JOIN category_mapping_new cm on cm.category_id = c.id ";
			sql += " INNER JOIN venue_category vc on vc.id = c.venue_category_id "; 
			sql += " WHERE vc.venue_id = " + venueId ;
			sql += " AND vc.category_group = :categoryParam";
			sql += " GROUP BY c.id,c.symbol,c.description,c.group_name,c.equal_cats,c.normalized_zone, "; 
			sql += " c.seat_quantity,c.category_capacity,c.venue_category_id "; 
			sql += " ORDER BY min(cm.id) "; 

		Session session=null;
		List<Category> list = new ArrayList<Category>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			sqlQuery.setParameter("categoryParam", categoryGroup);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(Category.class)).list();
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}finally{
			session.close();
		}
		return list;
	}
	
	public  List<Category> getAllCategoriesByVenueCategoryId(Integer venueCategoryId){
		String sql=" SELECT c.id,c.symbol,c.description,c.group_name as groupName,c.equal_cats as equalCats,c.normalized_zone as normalizedZone,";
		sql += " c.seat_quantity as seatQuantity,c.category_capacity as categoryCapacity,c.venue_category_id as venueCategoryId FROM category_new c "; 
			sql += " LEFT JOIN category_mapping_new cm on cm.category_id = c.id ";
			sql += " LEFT JOIN venue_category vc on vc.id = c.venue_category_id "; 
			sql += " WHERE vc.id = " + venueCategoryId ;
			sql += " GROUP BY c.id,c.symbol,c.description,c.group_name,c.equal_cats,c.normalized_zone, "; 
			sql += " c.seat_quantity,c.category_capacity,c.venue_category_id "; 
			sql += " ORDER BY min(cm.id) "; 

		Session session=null;
		List<Category> list = new ArrayList<Category>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(Category.class)).list();
		}catch (Exception e) {
			// TODO: handle exception
			return null;
		}finally{
			session.close();
		}
		return list;
	}
	public List<EventDTO> getTNTGCatsAutoEvents(){
		List<EventDTO> events =null;
//		String tmat = "tmtdb3.[tmatprodscale].[dbo]";
//		String zones = "tmtdb2.zoneplatform.[dbo]";
		String sql ="SELECT distinct  e.id as id,e.name as name,e.event_date as eventDate,e.event_time as eventTime," +
					" e.event_status as eventStatus," +
					" v.id as venueId,e.admitone_id as admitoneId,a.id as tourId,e.venue_category_id as venueCategoryId," +
					" count(tc.id) as noOfCrawls " +
					" ,a.grand_child_category_id as grandChildCategoryId,ae.is_zone_event as isZoneEvent" +
					" from event e" +
					" inner join artist a on a.id = e.artist_id" +
					" inner join venue v on v.id = e.venue_id" +
					" inner join grand_child_tour_category gc on gc.id = a.grand_child_category_id" +
					" inner join child_tour_category cc on cc.id = gc.child_category_id" +
					" inner join tour_category pc on pc.id = cc.tour_category_id" +
					" left join ticket_listing_crawl tc on tc.event_id = e.id" +
					" left join admitone_event ae on ae.EventId=e.admitone_id" +
					" where event_status = 'ACTIVE' and e.broker_id is not null" +
					
					//" and e.id not in (select event_id from tn_exchange_event)" +
					
					//" and e.id not in (select event_id from " + propertySetting.getZonesDbUrl() +".tg_cats_removed_category_event)" +
					
					" group by e.id ,e.name ,e.event_date ,e.event_time ,e.event_status ," +
					" v.id,e.admitone_id,a.id ,e.venue_category_id," +
					" e.venue_category_id,a.grand_child_category_id,ae.is_zone_event" ;
		
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(EventDTO.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
	
	public List<EventDTO> getAllActiveTMATEventsForZoneTickets(){
		List<EventDTO> events =null;
		String sql ="SELECT distinct  e.id as id,e.name as name,e.event_date as eventDate,e.event_time as eventTime," +
					" e.event_status as eventStatus," +
					" v.id as venueId,e.admitone_id as admitoneId,a.id as artistId,e.venue_category_id as venueCategoryId," +
					" count(tc.id) as noOfCrawls " +
					" ,a.grand_child_category_id as grandChildCategoryId,ae.is_zone_event as isZoneEvent," +
					" vc.category_group as venueCategoryName,v.building as venueName,v.city ,v.state,v.country," +
					" a.name as artistName , gc.name as grandChildCategoryName,cc.name as childCategoryName,pc.name as parentCategoryName," +
					" ae.postal_code as postalCode from event e" +
					" inner join artist a on a.id = e.artist_id" +
					" inner join venue v on v.id = e.venue_id" +
					" inner join grand_child_tour_category gc on gc.id = a.grand_child_category_id" +
					" inner join child_tour_category cc on cc.id = gc.child_category_id" +
					" inner join tour_category pc on pc.id = cc.tour_category_id" +
					" inner join ticket_listing_crawl tc on tc.event_id = e.id" +
					" inner join admitone_event ae on ae.EventId=e.admitone_id " +
					" inner join venue_category vc on vc.id=e.venue_category_id " +
					" where e.event_status = 'ACTIVE' and e.broker_id is not null" +
					" group by e.id ,e.name ,e.event_date ,e.event_time ,e.event_status ," +
					" v.id,e.admitone_id,a.id ,e.venue_category_id," +
					" e.venue_category_id,a.grand_child_category_id,ae.is_zone_event," +
					" vc.category_group ,v.building ,v.city ,v.state,v.country,a.name ,gc.name ,cc.name,pc.name,ae.postal_code" ;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(EventDTO.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		return events;
	}
	
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}


	public String getTnInduxDbName() {
		return tnInduxDbName;
	}

	public void setTnInduxDbName(String tnInduxDbName) {
		this.tnInduxDbName = tnInduxDbName;
	}

	public String getStubJktPos() {
		return stubJktPos;
	}

	public void setStubJktPos(String stubJktPos) {
		this.stubJktPos = stubJktPos;
	}

	public String getTn2RotPos() {
		return tn2RotPos;
	}

	public void setTn2RotPos(String tn2RotPos) {
		this.tn2RotPos = tn2RotPos;
	}

	public List<TNListedEvent> getAllTicketNetworkCategoryEvents() {
		String sql ="select id,name,eventDate,eventTime,frequentEvent from vw_not_theater_category_event";
		List<TNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<VividListedEvent> getAutoVividTheaterCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
		" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
		" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
		" FROM  dbo.auto_vividseat_exchange_event  AS te " +
		" INNER JOIN dbo.event e on te.event_id = e.id " +
		" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
		" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
		" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
		" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
		" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
		" WHERE status='ACTIVE' AND e.event_status='ACTIVE' and tc.id=4";
		List<VividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(VividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
		
	}
	public List<VividListedEvent> getAutoVividSportsCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
		" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
		" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
		" FROM  dbo.auto_vividseat_exchange_event  AS te " +
		" INNER JOIN dbo.event e on te.event_id = e.id " +
		" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
		" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
		" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
		" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
		" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
		" WHERE status='ACTIVE' AND e.event_status='ACTIVE' and tc.id=1";
		List<VividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(VividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
		
	}
	public List<VividListedEvent> getAutoVividConcertsCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
		" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
		" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
		" FROM  dbo.auto_vividseat_exchange_event  AS te " +
		" INNER JOIN dbo.event e on te.event_id = e.id " +
		" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
		" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
		" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
		" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
		" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
		" WHERE status='ACTIVE' AND e.event_status='ACTIVE' and tc.id=2";
		List<VividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(VividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
		
	}
	/*public List<VividListedEvent> getAllTicketNetworkTheaterCategoryEvents() {
		String sql ="select id,name,eventDate,eventTime,frequentEvent from vw_theater_auto_category_event";
		List<VividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(VividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}*/
	/*public List<TGTNListedEvent> getAllTicketNetworkNotTheaterCategoryEvents() {
		String sql ="select id,name,eventDate,eventTime,frequentEvent from vw_not_theater_category_event";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}*/
	public List<Object[]> getVividTgcatCategoryTickets(){
		Session session = null;
		try {
			session = sessionFactory.openSession();
			StringBuilder query = new StringBuilder();
			query.append("select * from vw_vivid_tgcat_category_tickets");
			Query queryString = session.createSQLQuery(query.toString());
			List<Object[]> list = queryString.list();
		
			return list;
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}
	public List<Object[]> getTickPickTgcatCategoryTickets(){
		Session session = null;
		try {
			session = sessionFactory.openSession();
			StringBuilder query = new StringBuilder();
			query.append("select * from vw_tickpick_tgcat_category_tickets");
			Query queryString = session.createSQLQuery(query.toString());
			List<Object[]> list = queryString.list();
		
			return list;
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}
	public List<Object[]> getScoreBigTgcatCategoryTickets(){
		Session session = null;
		try {
			session = sessionFactory.openSession();
			StringBuilder query = new StringBuilder();
			query.append("select * from vw_scorebig_tgcat_category_tickets");
			Query queryString = session.createSQLQuery(query.toString());
			List<Object[]> list = queryString.list();
		
			return list;
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}
	public List<Object[]> getVividVipCatCategoryTickets(){
		Session session = null;
		try {
			session = sessionFactory.openSession();
			StringBuilder query = new StringBuilder();
			query.append("select * from vw_vivid_vipcat_category_tickets");
			Query queryString = session.createSQLQuery(query.toString());
			List<Object[]> list = queryString.list();
		
			return list;
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}
	public List<Object[]> getTickPickVipCatCategoryTickets(){
		Session session = null;
		try {
			session = sessionFactory.openSession();
			StringBuilder query = new StringBuilder();
			query.append("select * from vw_tickpick_vipcat_category_tickets");
			Query queryString = session.createSQLQuery(query.toString());
			List<Object[]> list = queryString.list();
		
			return list;
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}
	public List<Object[]> getScoreBigVipCatCategoryTickets(){
		Session session = null;
		try {
			session = sessionFactory.openSession();
			StringBuilder query = new StringBuilder();
			query.append("select * from vw_scorebig_vipcat_category_tickets");
			Query queryString = session.createSQLQuery(query.toString());
			List<Object[]> list = queryString.list();
		
			return list;
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}
	public List<TGTNListedEvent> getAllTGTNCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
			" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
			" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
			" FROM  dbo.mini_tn_exchange_event  AS te " +
			" INNER JOIN dbo.event e on te.event_id = e.id " +
			" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
			" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
			" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
			" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
			" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
			" WHERE tc.id != 4 and status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGVividListedEvent> getTGVividConcertsCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.mini_vividseat_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
				" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE' and tc.id=2";
		List<TGVividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGVividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGVividListedEvent> getTGVividSportsCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.mini_vividseat_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
				" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE' and tc.id=1";
		List<TGVividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGVividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGVividListedEvent> getTGVividTheaterCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.mini_vividseat_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
				" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE' and tc.id=4";
		List<TGVividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGVividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	/*public List<TGVividListedEvent> getTGVividCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.mini_vividseat_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGVividListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGVividListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}*/
	public List<TGTickPickListedEvent> getTGTickPickCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate," +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.mini_tickpick_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTickPickListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTickPickListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGScoreBigListedEvent> getTGScoreBigCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.mini_scorebig_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGScoreBigListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGScoreBigListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllVipTnCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
				" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.vip_mini_tn_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
				" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE tc.id != 4 and status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllVipVividCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
				" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.vip_mini_vividseat_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllVipTickPickCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
				" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.vip_mini_tickpick_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllVipScoreBigCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
				" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.vip_mini_scorebig_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllVipAutoTnCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
				" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.vip_auto_tn_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
				" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE tc.id != 4 and status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllVipAutoVividCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
				" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.vip_auto_vividseat_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	
	public List<TGTNListedEvent> getAllVipAutoScoreBigCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
				" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.vip_auto_scorebig_exchange_event  AS te " +
				" INNER JOIN dbo.event e on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TickPickListedEvent> getTickPickCategoryEvents() {
		String sql ="SELECT   distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.event  AS e " +
				" INNER JOIN dbo.auto_tickpick_exchange_event te on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE e.event_status='ACTIVE' and status='ACTIVE'";
		List<TickPickListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TickPickListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<ScoreBigListedEvent> getScoreBigCategoryEvents() {
		String sql ="SELECT   distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END AS EventDate, " +
				" CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
				" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
				" FROM  dbo.event  AS e " +
				" INNER JOIN dbo.auto_scorebig_exchange_event te on te.event_id = e.id " +
				" INNER JOIN dbo.venue AS v ON v.id = e.venue_id " +
				//" INNER JOIN dbo.artist AS t ON t.id = e.artist_id " +
				//" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
				//" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
				//" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
				" WHERE e.event_status='ACTIVE' and status='ACTIVE'";
		List<ScoreBigListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(ScoreBigListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllLastRowMiniTNCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
			" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
			" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
			" FROM  dbo.lastrow_mini_tn_exchange_event  AS te " +
			" INNER JOIN dbo.event e on te.event_id = e.id " +
			" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
			" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
			" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
			" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
			" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
			" WHERE tc.id != 4 and status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}
	public List<TGTNListedEvent> getAllLastFiveRowMiniCatStubhubCategoryEvents() {
		String sql ="SELECT  distinct e.id AS Id,e.name AS Name, CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10)," +
			" event_date, 101) END AS EventDate, CASE WHEN event_time IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_time, 108) END AS EventTime ," +
			" CASE WHEN te.is_frequent_event is null or te.is_frequent_event =0  then 'False' else 'True' END as FrequentEvent " +
			" FROM  dbo.lastfiverow_mini_stubhub_exchange_event  AS te " +
			" INNER JOIN dbo.event e on te.event_id = e.id " +
			" INNER JOIN dbo.venue AS v ON v.id = e.venue_id  " +
			" INNER JOIN dbo.artist AS a ON a.id = e.artist_id " +
			" INNER JOIN dbo.grand_child_tour_category gc on gc.id = a.grand_child_category_id " +
			" INNER JOIN dbo.child_tour_category cc on cc.id = gc.child_category_id " +
			" INNER JOIN dbo.tour_category tc on tc.id = cc.tour_category_id " +
			" WHERE tc.id = 4 and status='ACTIVE' AND e.event_status='ACTIVE'";
		List<TGTNListedEvent> events =null;
		Query query = null;
		Session session = null;
		try {
			session = sessionFactory.openSession();
			query = session.createSQLQuery(sql);
			events =  query.setResultTransformer(Transformers.aliasToBean(TGTNListedEvent.class)).list();
		} catch (Exception e) {
			e.printStackTrace();
		} finally  {
			session.close();
		}
		
		return events;
	}

public List<Object[]> getVividTheaterCsv(){
		
		Session session = sessionFactory.openSession();
		
		try{
			Query queryString = session.createSQLQuery("SELECT * FROM vw_vivid_autocat_category_tickets");
			List<Object[]> list = queryString.list();
			return list;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}
	
	public List<Object[]> getTickPickTheaterCsv(){
		
		//Session session = getSession();
		Session session = sessionFactory.openSession();
		try{
			Query queryString = session.createSQLQuery("SELECT * FROM vw_tickpick_autocat_category_tickets");
			List<Object[]> list = queryString.list();
			return list;
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return null;
		
	}

	public List<Object[]> getScoreBigTheaterCsv(){
	
	//Session session = getSession();
	Session session = sessionFactory.openSession();
	try{
		Query queryString = session.createSQLQuery("SELECT * FROM vw_scorebig_autocat_category_tickets");
		List<Object[]> list = queryString.list();
		return list;
	}catch (Exception e) {
		e.printStackTrace();
	}finally{
		session.close();
	}
	return null;
	
	}
public  List<EventCrawlerDetails> getEventsWithLessCrawlCount(Boolean isLatestEventOnly) {
		
		String sql="select e.id as eventId,e.name AS eventName, CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END AS EventDate," +
				" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))" +
				" WHEN e.event_time is null THEN 'TBD' END as eventTime,pc.name as parentCategory," +
				" v.building AS venueName,v.city ,v.state,v.country,a.name as tourName,cr.site_id as siteId," +
				" CONVERT(VARCHAR, e.creation_date, 120) AS  eventCreatedDate" +
				"  from ticket_listing_crawl cr " +
				" inner join event e on e.id=cr.event_id" +
				" inner join venue v on v.id = e.venue_id"+
				" inner join artist a on a.id=e.artist_id " +
				" inner join grand_child_tour_category gc on gc.id=a.grand_child_category_id" +
				" inner join child_tour_category cc on cc.id=gc.child_category_id" +
				" inner join tour_category pc on pc.id=cc.tour_category_id" +
				" where e.event_status='ACTIVE' and cr.enabled=1 and e.venue_category_id is not null " +
				" and cr.event_id in(select event_id from ticket_listing_crawl where enabled=1 group by event_id having COUNT(id)<=2)";
		
		if(isLatestEventOnly) {
			sql = sql + " and datediff(day,e.creation_date,getdate())<=14";
		}
		//sql = sql + " order by e.creation_date";
		//System.out.println("Less than 2 crawls querys -> "+sql);
		Session session=null;
		List<EventCrawlerDetails> list = null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(EventCrawlerDetails.class)).list();
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getEventsWithDuplicateCrawls(){
	
	String sql="select e.id as EventId,e.name as Event,CONVERT(VARCHAR(19),e.event_date,101) as EventDate,CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as EventTime,a.name as Artist,v.building as Venue,v.city as City,v.state as State,v.country as Country,"+
	"cr.site_id as SiteId,cr.query_url as QueryUrl,convert(varchar(10),cr.created, 101) + right(convert(varchar(32),cr.created,100),8) as'Create Time',"+
	"cr.creator as 'Creator' "+ 
	"from ticket_listing_crawl cr "+
	"inner join event e on e.id=cr.event_id "+
	"inner join venue v on v.id=e.venue_id "+
	"inner join artist a on a.id=e.artist_id "+
	"inner join("+
	"select event_id,site_id from ticket_listing_crawl where enabled=1 "+
	"group by event_id,site_id "+
	"having count(*)>1) as tt on tt.event_id=cr.event_id and tt.site_id=cr.site_id "+
	"where enabled=1 and  e.event_date > GETDATE() "+
	"order by e.event_date,e.event_time,e.id,cr.site_id,cr.id ";
	
	
	Session session = null;
	List<EventCrawlerDetails> listDetails = null;
	List<Object[]> list=null;
	try{
		session=sessionFactory.openSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		list=sqlQuery.list();
		//listDetails = sqlQuery.setResultTransformer(Transformers.aliasToBean(EventCrawlerDetails.class)).list();
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}finally{
		session.close();
	}
	return list;
}

	@SuppressWarnings("unchecked")
	public List<Object[]> getEventsWithZeroCrawls(){
		
	String sql="select e.id,e.name,CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END AS EventDate,"
		+"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as eventTime,"
		+"v.building,a.name from event e "
		+"inner join venue v on e.venue_id=v.id "
		+"inner join artist a on e.artist_id=a.id "
		+"where e.id not in (select distinct event_id from ticket_listing_crawl) "
		+"and e.event_status='ACTIVE' and CONVERT(VARCHAR(10), e.event_date, 101)>=CONVERT(VARCHAR(10), GETDATE(), 101) order by e.event_date";
	
	Session session = null;
	List<Object[]> list = null;
	try{
		session = sessionFactory.openSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		list = sqlQuery.list();
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}finally{
			session.close();
		}

	return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> geteventsWithZeroCrawlsForShTevoTnow(){
		
	String sql="select e.id,e.name,CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END AS EventDate,"
		+"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as eventTime,"
		+"v.building,a.name from event e "
		+"inner join venue v on e.venue_id=v.id "
		+"inner join artist a on e.artist_id=a.id "
		+"where e.id not in (select distinct event_id from ticket_listing_crawl where " 
		+"site_id in ('stubhub','ticketevolution','ticketsnow','vividseat','flashseats') and enabled=1) "
		+"and e.event_status='ACTIVE' and e.event_date > GETDATE() order by e.event_date ,e.event_time,e.name";
	
	Session session = null;
	List<Object[]> list = null;
	try{
		session = sessionFactory.openSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		list = sqlQuery.list();
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}finally{
			session.close();
		}

	return list;
	}

	
	@SuppressWarnings("unchecked")
	public List<Object[]> getBrokerEventsInTmat(String brokerId, String fromDate, String toDate){
		/*
		String sql="select e.id as'EventId',e.name as 'EventName',CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END AS EventDate,"
					+"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as EventTime,"
					+"v.building as'Venue',a.name as'Tour',"
					+"CASE WHEN e.id not in(select event_id from tn_exchange_event where status='ACTIVE') THEN 'N' "
					+"WHEN e.id in(select event_id from tn_exchange_event where status='ACTIVE') THEN 'Y' END as 'TN',"
					+"CASE WHEN e.id not in(select event_id from vivid_exchange_event where status='ACTIVE') THEN 'N' "
					+"WHEN e.id in(select event_id from vivid_exchange_event where status='ACTIVE') THEN 'Y' END as 'Vivid',"
					+"CASE WHEN e.id not in(select event_id from scorebig_exchange_event where status='ACTIVE') THEN 'N' "
					+"WHEN e.id in(select event_id from scorebig_exchange_event where status='ACTIVE') THEN 'Y' END as 'ScoreBig' from event e "
					+"inner join venue v on e.venue_id=v.id "
					+"inner join artist t on e.artist_id=t.id "
					+"where event_status='ACTIVE' and broker_id='"+brokerId+"' and broker_status='ACTIVE' order by e.event_date";
		
		sql="";
		*/
		
		String sql="select e.id as'EventId',e.name as 'EventName',CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END AS EventDate,"
		+"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as EventTime,"
		+"tc.name as 'Parenet Category',ctc.name as 'Child Category',gctc.name as 'Grand Child Category',"
		+"v.building as'Venue',a.name as'Artist',"
		+"CASE WHEN e.id in(select event_id from auto_tn_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Auto Cats TN',"
		+"CASE WHEN e.id in (select event_id from lastrow_mini_tn_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Lastrow Mini Cats TN',"
		+"CASE WHEN e.id in (select event_id from mini_tn_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Mini Cats TN',"
		+"CASE WHEN e.id in (select event_id from vip_auto_tn_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Vip Auto Cats TN',"
		+"CASE WHEN e.id in (select event_id from vip_mini_tn_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Vip Mini Cats TN',"
		+"CASE WHEN e.id in(select event_id from mini_vividseat_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Mini Cats Vivid',"
		+"CASE WHEN e.id in (select event_id from vip_mini_vividseat_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Vip Mini Cats Vivid',"
		+"CASE WHEN e.id in (select event_id from vip_auto_vividseat_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Vip Auto Cats Vivid',"
		+"CASE WHEN e.id in (select event_id from auto_vividseat_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Auto Cats Vivid',"
		+"CASE WHEN e.id in(select event_id from vip_mini_scorebig_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Vip Mini Cats ScoreBig',"
		+"CASE WHEN e.id in (select event_id from vip_auto_scorebig_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Vip Auto Cats ScoreBig',"
		+"CASE WHEN e.id in (select event_id from auto_scorebig_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Auto Cats ScoreBig',"
		+"CASE WHEN e.id in (select event_id from mini_scorebig_exchange_event where status='ACTIVE') THEN 'Y' ELSE 'N' END as 'Mini Cats ScoreBig' "
		+"from event e "
		+"inner join venue v on e.venue_id=v.id  "
		+"inner join artist a on e.artist_id=a.id "
		+"inner join grand_child_tour_category gctc on a.grand_child_category_id=gctc.id "
		+"inner join child_tour_category ctc on gctc.child_category_id=ctc.id "
		+"inner join tour_category tc on ctc.tour_category_id=tc.id "
		+"where e.event_status='ACTIVE' and e.broker_id='"+brokerId+"' and e.broker_status='ACTIVE' and CONVERT(VARCHAR(10), e.event_date, 101)>=CONVERT(VARCHAR(10), GETDATE(), 101) "
		+"and CAST( e.creation_date as DATE)>='"+fromDate+"' and CAST( e.creation_date as DATE)<='"+toDate+"' order by e.event_date";
		
		Session session = null;
		List<Object[]> list = null;
		try{
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			list = sqlQuery.list();
		}catch(Exception e){
			e.printStackTrace();
			return null;
			
		}finally{
			session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getAllActiveTmatEventsByParentCategory(String parentCategory,List<String> country){
		String sql = "select e.id,e.name,CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END AS EventDate,"
			+"CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as EventTime,"
			+"v.building,v.city,v.state,v.country,vc.category_group,e.venue_id,tc.name as parentCategory,"
			+" sum( case when tn.order_id is not null and tn.accept_sub_status_desc not like '%rejected%' then 1 else 0 end) as totalOrders "
			+" from event e "
			+"inner join artist a on e.artist_id=a.id "
			+"inner join grand_child_tour_category gctc on a.grand_child_category_id=gctc.id "
			+"inner join child_tour_category ctc on gctc.child_category_id=ctc.id "
			+"inner join tour_category tc on ctc.tour_category_id=tc.id "
			+"inner join venue v on e.venue_id=v.id "
			+"left join venue_category vc on e.venue_category_id=vc.id "
			+" left join tnd_sales_autopricing tn on tn.event_id=e.admitone_id "
			+"where e.event_status='ACTIVE' and e.event_date>=getdate() and (vc.venue_map is null or vc.venue_map <> 1) "
			+"  and e.name not like '%parking%' ";
		
		if(parentCategory != null && !parentCategory.equals("All")) {
			sql = sql + " and tc.name='"+parentCategory+"' ";
		}
		//Country filter criteria 
		if(!country.contains("others")){
			sql += " and v.country in ('US','USA','CA') ";
		}else if(country.contains("others") && !country.contains("US&CA")){
			sql += " and v.country not in ('US','USA','CA') ";
		}
		sql = sql + " group by e.id,e.event_date,e.event_time,e.name,v.building,v.city,v.state,v.country,vc.category_group,e.venue_id,tc.name " 
				+" order by e.event_date,e.event_time,e.name";
		
		Session session = null;
		List<Object[]> list = null;
		try{
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			list = sqlQuery.list();
		}catch(Exception e){
			e.printStackTrace();
			return null;
			
		}finally{
			session.close();
		}
		return list;
	}
	

	public  List<NewPOSEvents> getNewPosEventsFromStub(String dateTimeStr) {
			
		/*String sql="SELECT  DISTINCT "+ 
			" ee.Id AS id,ee.create_datetime AS createdDate,"+
			" ee.event AS name,ee.eventdate as date,ee.eventdate as time,"+
			" ee.venue AS venue,ee.City as city,ee.State as state,"+
			" pc.event_parent_category_desc as parentCategory,cc.event_child_category_desc as childCategory,"+
			" gc.event_grandchild_category_desc as grandChildCategory"+
			" FROM "+tnInduxDbName+".exchange_event ee WITH (NOLOCK)"+
			" left join  "+tnInduxDbName+".event_grandchild_category gc on gc.event_grandchild_category_id=ee.grandchildID"+
			" left join "+tnInduxDbName+".event_child_category cc on cc.event_child_category_id=ee.childID"+
			" left join "+tnInduxDbName+".event_parent_category pc on pc.event_parent_category_id=ee.parentID" +
			" left join tmatprodscale.dbo.event te on te.admitone_id = ee.Id"+
			" where eventdate>=getdate() and te.id is null ";
		
		if(null != dateTimeStr) {
			sql = sql + " and ee.create_datetime>='"+dateTimeStr+"' ";	
		} else {
			sql = sql + " and ee.create_datetime >= CONVERT(DATE,GETDATE())";	
		}
		sql = sql + " ORDER BY ee.create_datetime desc";*/
		
		String sql=" SELECT  DISTINCT df.exchange_event_id AS id,df.last_update_date AS createdDate," +
			" df.exchange_event_name AS name,df.exchange_event_date as date,df.exchange_event_time as time," +
			" df.exchange_venue_name AS venue,df.city as city,df.state as state," +
			" df.parent_category_name as parentCategory,df.child_category_name as childCategory," +
			" df.grand_child_category_name as grandChildCategory," +
			" CASE when a.id is null THEN '-' ELSE a.name END as tmatArtistName," +
			" CASE when vc.category_group is null THEN '-' ELSE vc.category_group END as categoryGroup," +
			" CASE WHEN e.id is not null THEN 'Yes' ELSE 'No' END as eventExistinTMAT," +
			" CASE when tc.name is null THEN '-' ELSE tc.name END as tmatParentCategory," +
			" CASE WHEN tc.id is not null and REPLACE(tc.name ,'theater','theatre')!= df.parent_category_name" +
			" THEN 'Yes' ELSE 'No' END as isParentTypeMismatch" +
			" from tn_data_feed_event df" +
			" left join event e on e.admitone_id=df.exchange_event_id" +
			" left join artist a on a.id=e.artist_id" +
			" left join grand_child_tour_category gc on gc.id=a.grand_child_category_id" +
			" left join child_tour_category cc on cc.id=gc.child_category_id" +
			" left join tour_category tc on tc.id=cc.tour_category_id" +
			" left join venue_category vc on vc.id=e.venue_category_id" +
			" where df.status in('ACTIVE','EXISTING') ";
			
		if(null != dateTimeStr) {
			sql = sql + " and df.last_update_date>='"+dateTimeStr+"' ";	
		} else {
			sql = sql + " and df.last_update_date >= CONVERT(DATE,GETDATE())";	
		}
		sql = sql + " ORDER BY eventExistinTMAT,df.last_update_date desc";
		
		Session session=null;
		List<NewPOSEvents> list = null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(NewPOSEvents.class)).list();
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
		return list;
	}
	
	public  List<Object[]> getRtwPosUnSentInvocieTicketDetails() throws Exception {
		
		//Get Unsent invocie ticket details for events in next 10 days
		String sql="select distinct tt.event_name as 'event name',CONVERT(VARCHAR(19),tt.event_datetime,101) as EventDate,   " + 
				"   CASE WHEN tt.event_datetime is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), tt.event_datetime, 100), 7))  " + 
				"   WHEN tt.event_datetime is null THEN 'TBD' END as EventTime,  " + 
				"  tt.name as 'venue',tt.section,tt.row,tt.seat,tt.qty as 'quantity',tt.retail_price as 'retail price',tt.cost,  " + 
				"  tt.wholesale_price as 'wholesale price',tt.actual_sold_price as 'actual sold price',tt.[total price] as 'total price' ,  " + 
				"  case when tt.ticketonHandStatus =1 then 'Yes' else 'No' end as ETicket,i.invoice_id as 'invoice id',i.create_date as 'sales date',   " + 
				"  (su.system_user_fname+' '+su.system_user_lname) as 'sales person',  " + 
				"  isnull(tt.clientBrokerCompanyName,'') as 'vendor company name',isnull(tt.clientBrokerEmail,'') as 'vendor email',  " + 
				"  'RTW' as broker,  " + 
				"  case when tt.ticketonHandStatus=1 then 'Yes' else 'No' end as 'on Hand',tt.stockType as 'stock type',  " + 
				"  isnull(i.ticket_request_id,'') as 'Ticket Request Id',tt.poCreatedDate as 'PO created date',  " + 
				"  (isnull(cc.client_fname,'')+' '+isnull(cc.client_lname,'')) as 'Customer Name', cc.email as 'Customer Email', " + 
				"  tt.event_datetime,tt.event_id  " + 
				"  from "+rtwPos+".invoice i with(nolock)  " + 
				"  inner join "+rtwPos+".shipping_tracking st with(nolock) on st.shipping_tracking_id=i.shipping_tracking_id  " + 
				"  inner join "+rtwPos+".shipping_tracking_status sts with(nolock) on sts.shipping_tracking_status_id=st.shipping_tracking_status_id  " + 
				"  inner join "+rtwPos+".shipping_account_delivery_method sad with(nolock) on sad.shipping_account_delivery_method_id=st.shipping_account_delivery_method_id  " + 
				"  inner join (select distinct count(t.invoice_id) as 'qty',  " + 
				"  case when t.wholesale_price <> 0 then (t.wholesale_price*count(t.invoice_id)) else  " + 
				"  (t.actual_sold_price*count(t.invoice_id)) end as 'total price',t.invoice_id,cast(MIN(CAST(t.seat_number AS int)) as varchar)+'-'+  " + 
				"  cast(Max(CAST(t.seat_number AS int)) as varchar) as 'seat',e.event_name,e.event_datetime,v.name,  " + 
				"  t.section,t.row,t.retail_price,t.wholesale_price,t.cost,t.actual_sold_price,tg.event_id,  " + 
				"  t.ticket_on_hand_status_id as ticketonHandStatus,st.ticket_group_stock_type_desc as stockType,  " + 
				"  cb.client_broker_id as clientBrokerId,cb.client_broker_company_name as clientBrokerCompanyName,  " + 
				"  cb.client_broker_email as clientBrokerEmail,po.create_date as poCreatedDate   " + 
				"  from "+rtwPos+".ticket t with(nolock)  " + 
				"  inner join "+rtwPos+".ticket_group tg with(nolock) on tg.ticket_group_id=t.ticket_group_id  " + 
				"  inner join "+rtwPos+".ticket_group_stock_type st with(nolock) on st.ticket_group_stock_type_id=tg.ticket_group_stock_type_id  " + 
				"  inner join "+rtwPos+".event e with(nolock) on e.event_id=tg.event_id  " + 
				"  inner join "+rtwPos+".venue v with(nolock) on v.venue_id=e.venue_id  " + 
				"  inner join "+rtwPos+".purchase_order po with(nolock) on po.purchase_order_id=t.purchase_order_id  " + 
				"  inner join "+rtwPos+".client_broker cb with(nolock) on cb.client_broker_id=po.buyer_broker_id  " + 
				"  group by t.invoice_id,e.event_name,e.event_datetime,v.name,  " + 
				"  t.section,t.row,t.retail_price,t.wholesale_price,t.cost,t.actual_sold_price,tg.event_id,t.ticket_on_hand_status_id,  " + 
				"  st.ticket_group_stock_type_desc,cb.client_broker_id,cb.client_broker_company_name,cb.client_broker_email,po.create_date   " + 
				" ) as tt on tt.invoice_id=i.invoice_id  " + 
				" inner join (select i.invoice_id as invoice_id,c.client_id as client_id,c.client_fname as client_fname,c.client_lname as client_lname, " + 
				"	c.company as company,c.email as email,c.main_address_id as main_address_id ,c.main_phone_id as main_phone_id,'' as 'referred by' " + 
				"from "+rtwPos+".invoice i inner join "+rtwPos+".client_invoice ci on ci.invoice_id=i.invoice_id inner join " + 
				""+rtwPos+".client c on c.client_id=ci.client_id  " + 
				"union " + 
				"select i.invoice_id as invoice_id,cb.client_broker_id as client_id,cb.client_broker_fname as client_fname,cb.client_broker_lname as client_lname, " + 
				"cb.client_broker_company_name as company,cb.client_broker_email as email,cb.main_address_id as main_address_id,cb.main_phone_id as main_phone_id, " + 
				"cb.referred_by as 'referred by' " + 
				" from "+rtwPos+".invoice i  " + 
				"inner join "+rtwPos+".client_broker_invoice cbi on cbi.invoice_id=i.invoice_id inner join " + 
				""+rtwPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id ) as cc on cc.invoice_id=i.invoice_id " + 
				"  inner join "+rtwPos+".system_users su with(nolock) on su.system_user_id=i.system_user_id  " + 
				"  where  i.invoice_status_id != 2 and tt.event_datetime>getdate()  " + 
				"  and tt.event_datetime<dateadd(dd,16,convert(date,getdate()))  " + 
				"  and shipping_tracking_status_desc not in('Shipped','Delivered')  " + 
				"" +
				" union  " +
				"select distinct tt.event_name as 'event name',CONVERT(VARCHAR(19),tt.event_datetime,101) as EventDate, " +
				"  CASE WHEN tt.event_datetime is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), tt.event_datetime, 100), 7))" +
				"  WHEN tt.event_datetime is null THEN 'TBD' END as EventTime," +
				" tt.name as 'venue',tt.section,tt.row,tt.seat,tt.qty as 'quantity',tt.retail_price as 'retail price',tt.cost," +
				" tt.wholesale_price as 'wholesale price',tt.actual_sold_price as 'actual sold price',tt.[total price] as 'total price' ," +
				" case when tt.ticketonHandStatus =1 then 'Yes' else 'No' end as ETicket,i.invoice_id as 'invoice id',i.create_date as 'sales date', " +
				" (su.system_user_fname+' '+su.system_user_lname) as 'sales person'," +
				" tt.clientBrokerCompanyName as 'vendor company name',tt.clientBrokerEmail as 'vendor email'," +
				" 'RTW-2' as broker," +
				" case when tt.ticketonHandStatus=1 then 'Yes' else 'No' end as 'on Hand',tt.stockType as 'stock type'," +
				" i.ticket_request_id as 'Ticket Request Id',tt.poCreatedDate as 'PO created date'," +
				"  (isnull(cc.client_fname,'')+' '+isnull(cc.client_lname,'')) as 'Customer Name', cc.email as 'Customer Email', " + 
				" tt.event_datetime,tt.event_id" +
				" from "+rtwTwoPos+".invoice i with(nolock)" +
				" inner join "+rtwTwoPos+".shipping_tracking st with(nolock) on st.shipping_tracking_id=i.shipping_tracking_id" +
				" inner join "+rtwTwoPos+".shipping_tracking_status sts with(nolock) on sts.shipping_tracking_status_id=st.shipping_tracking_status_id" +
				" inner join "+rtwTwoPos+".shipping_account_delivery_method sad with(nolock) on sad.shipping_account_delivery_method_id=st.shipping_account_delivery_method_id" +
				" inner join (select distinct count(t.invoice_id) as 'qty'," +
				" case when t.wholesale_price <> 0 then (t.wholesale_price*count(t.invoice_id)) else" +
				" (t.actual_sold_price*count(t.invoice_id)) end as 'total price',t.invoice_id,cast(MIN(CAST(t.seat_number AS int)) as varchar)+'-'+" +
				" cast(Max(CAST(t.seat_number AS int)) as varchar) as 'seat',e.event_name,e.event_datetime,v.name," +
				" t.section,t.row,t.retail_price,t.wholesale_price,t.cost,t.actual_sold_price,tg.event_id," +
				" t.ticket_on_hand_status_id as ticketonHandStatus,st.ticket_group_stock_type_desc as stockType," +
				" cb.client_broker_id as clientBrokerId,cb.client_broker_company_name as clientBrokerCompanyName," +
				" cb.client_broker_email as clientBrokerEmail,po.create_date as poCreatedDate " +
				" from "+rtwTwoPos+".ticket t with(nolock)" +
				" inner join "+rtwTwoPos+".ticket_group tg with(nolock) on tg.ticket_group_id=t.ticket_group_id" +
				" inner join "+rtwTwoPos+".ticket_group_stock_type st with(nolock) on st.ticket_group_stock_type_id=tg.ticket_group_stock_type_id" +
				" inner join "+rtwTwoPos+".event e with(nolock) on e.event_id=tg.event_id" +
				" inner join "+rtwTwoPos+".venue v with(nolock) on v.venue_id=e.venue_id" +
				" inner join "+rtwTwoPos+".purchase_order po with(nolock) on po.purchase_order_id=t.purchase_order_id" +
				" inner join "+rtwTwoPos+".client_broker cb with(nolock) on cb.client_broker_id=po.buyer_broker_id" +
				" group by t.invoice_id,e.event_name,e.event_datetime,v.name," +
				" t.section,t.row,t.retail_price,t.wholesale_price,t.cost,t.actual_sold_price,tg.event_id,t.ticket_on_hand_status_id," +
				" st.ticket_group_stock_type_desc,cb.client_broker_id,cb.client_broker_company_name,cb.client_broker_email,po.create_date" +
				") as tt on tt.invoice_id=i.invoice_id" +
				" inner join (select i.invoice_id as invoice_id,c.client_id as client_id,c.client_fname as client_fname,c.client_lname as client_lname, " + 
				"	c.company as company,c.email as email,c.main_address_id as main_address_id ,c.main_phone_id as main_phone_id,'' as 'referred by' " + 
				"from "+rtwTwoPos+".invoice i inner join "+rtwTwoPos+".client_invoice ci on ci.invoice_id=i.invoice_id inner join " + 
				""+rtwTwoPos+".client c on c.client_id=ci.client_id  " + 
				"union " + 
				"select i.invoice_id as invoice_id,cb.client_broker_id as client_id,cb.client_broker_fname as client_fname,cb.client_broker_lname as client_lname, " + 
				"cb.client_broker_company_name as company,cb.client_broker_email as email,cb.main_address_id as main_address_id,cb.main_phone_id as main_phone_id, " + 
				"cb.referred_by as 'referred by' " + 
				" from "+rtwTwoPos+".invoice i  " + 
				"inner join "+rtwTwoPos+".client_broker_invoice cbi on cbi.invoice_id=i.invoice_id inner join " + 
				""+rtwTwoPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id ) as cc on cc.invoice_id=i.invoice_id " + 
				" inner join "+rtwTwoPos+".system_users su with(nolock) on su.system_user_id=i.system_user_id" +
				" where  i.invoice_status_id != 2 and tt.event_datetime>getdate()" +
				" and tt.event_datetime<dateadd(dd,16,convert(date,getdate()))" +
				" and shipping_tracking_status_desc not in('Shipped','Delivered')" +
				" order by event_datetime,event_id ";
				
		Session session=null;
		List<Object[]> list = null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			
			list = sqlQuery.list();
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
		return list;
	}

	public  List<NewPOSEvents> getAllEventsNotInTmat(String parentCategory,String fromDateTimeStr) {
		
		/*String sql="SELECT  DISTINCT "+ 
			" ee.Id AS id,ee.create_datetime AS createdDate,"+
			" ee.event AS name,ee.eventdate as date,ee.eventdate as time,"+
			" ee.venue AS venue,ee.City as city,ee.State as state,"+
			" pc.event_parent_category_desc as parentCategory,cc.event_child_category_desc as childCategory,"+
			" gc.event_grandchild_category_desc as grandChildCategory"+
			" FROM "+tnInduxDbName+".exchange_event ee WITH (NOLOCK)"+
			" left join  "+tnInduxDbName+".event_grandchild_category gc on gc.event_grandchild_category_id=ee.grandchildID"+
			" left join "+tnInduxDbName+".event_child_category cc on cc.event_child_category_id=ee.childID"+
			" left join "+tnInduxDbName+".event_parent_category pc on pc.event_parent_category_id=ee.parentID" +
			" left join tmatprodscale.dbo.event te on te.admitone_id = ee.Id"+
			" where eventdate>=getdate() and te.id is null";
		
		if(parentCategory.equals("All")){
			sql += " ORDER BY ee.create_datetime desc";
		}
		else{
			sql += " and pc.event_parent_category_desc='"+parentCategory+"' ORDER BY ee.create_datetime desc";
		}*/
		
		String sql=" SELECT  DISTINCT df.exchange_event_id AS id,df.last_update_date AS createdDate," +
			" df.exchange_event_name AS name,df.exchange_event_date as date,df.exchange_event_time as time," +
			" df.exchange_venue_name AS venue,df.city as city,df.state as state," +
			" df.parent_category_name as parentCategory,df.child_category_name as childCategory," +
			" df.grand_child_category_name as grandChildCategory" +
			" from tn_data_feed_event df" +
			" left join event e on e.admitone_id=df.exchange_event_id" +
			" where df.status in('ACTIVE','EXISTING') and e.id is null";
		
		if(fromDateTimeStr != null) {
			sql = sql + " and df.last_update_date>='"+fromDateTimeStr+"' ";	
		} 
		if(parentCategory != null && !parentCategory.equals("All")) {
			sql += " and df.parent_category_name='"+parentCategory+"'";
		}
		
		sql += " ORDER BY df.last_update_date desc";
		
		
		Session session=null;
		List<NewPOSEvents> list = null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(NewPOSEvents.class)).list();
			
			
		}catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> getTmatPOSParentCategoryMissmatchEvents(){
		/*String sql = "select te.creation_date as 'Event Creation Date',te.name as Event,"
					+"CONVERT(VARCHAR(19),te.event_date,101) as EventDate,"
					+"CASE WHEN te.event_time is not null THEN "
					+"LTRIM(RIGHT(CONVERT(VARCHAR(20), te.event_time, 100), 7)) "
					+"WHEN te.event_time is null THEN 'TBD' END as EventTime,"
					+"tv.building as Venue,ttc.name as TmatEventType,pc.event_parent_category_desc as PosEventType "
					//+"CONVERT(VARCHAR(10), te.creation_date, 101) as 'Event Creation Date' "
					+"from event te "
					+"inner join venue tv on tv.id=te.venue_id "
					+"inner join artist a on a.id=te.artist_id "
					+"inner join grand_child_tour_category tgc on tgc.id=ta.grand_child_category_id "
					+"inner join child_tour_category tcc on tcc.id=tgc.child_category_id "
					+"inner join tour_category ttc on ttc.id=tcc.tour_category_id "
					+"inner join "+tnInduxDbName+".[exchange_event] ee on ee.id = te.admitone_id "
					+"inner join "+tnInduxDbName+".[event_grandchild_category] gc on gc.event_grandchild_category_id=ee.grandchildID "
					+"left join "+tnInduxDbName+".[event_child_category] cc on cc.event_child_category_id=ee.childID "
					+"left join "+tnInduxDbName+".[event_parent_category] pc on pc.event_parent_category_id=ee.parentID  "
					+"left join "+tnInduxDbName+".[event] e on e.exchange_event_id = ee.id "
					+"where te.event_status='ACTIVE' and REPLACE(ttc.name ,'theater','theatre')!= pc.event_parent_category_desc " 
					+"order by te.creation_date";*/
		
		String sql = "select e.creation_date as 'Event Creation Date',e.name as Event," +
				" CONVERT(VARCHAR(19),e.event_date,101) as EventDate," +
				" CASE WHEN e.event_time is not null THEN" +
				"  LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))" +
				" WHEN e.event_time is null THEN 'TBD' END as EventTime," +
				" v.building as Venue,tc.name as TmatEventType,df.parent_category_name as PosEventType" +
				" from event e " +
				" inner join venue v on v.id=e.venue_id" +
				" inner join artist a on a.id=e.artist_id" +
				" inner join grand_child_tour_category gc on gc.id=a.grand_child_category_id" +
				" inner join child_tour_category cc on cc.id=gc.child_category_id" +
				" inner join tour_category tc on tc.id=cc.tour_category_id" +
				" inner join tn_data_feed_event df on df.exchange_event_id=e.admitone_id" +
				" where e.event_status='ACTIVE' and REPLACE(tc.name ,'theater','theatre')!= df.parent_category_name" +
				" order by e.creation_date";
		
		List<Object[]> list = null;
		Session session = null;
		
		try{
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			list = sqlQuery.list();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
			finally{
				session.close();
			}
			return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<NewPOSEvents> getExchangeEventsWithoutLocalEvents(String brokerId){
		String posUrl;
		if(brokerId.equals("2")) posUrl = stubJktPos;
		else if(brokerId.equals("3")) posUrl = tn2RotPos;
		else if(brokerId.equals("5")) posUrl = rtwPos;
		else if(brokerId.equals("10")) posUrl = rtwTwoPos;
		else posUrl = "";
		/*
		String sql = "select ee.Id as 'id',ee.create_datetime AS 'createdDate',ee.Event as 'name',ee.EventDate 'date',ee.EventDate as 'time',"
					+"ee.Venue AS 'venue',ee.City as 'city',ee.State as 'state',tc.name as 'parentCategory',"
					+"ctc.name as 'childCategory',gctc.name as 'grandChildCategory' from "+posUrl+".exchange_event ee "
					+"inner join event e on ee.Id = e.admitone_id "
					+"left join artist t on e.artist_id = t.id "
					+"left join grand_child_tour_category gctc on a.grand_child_category_id = gctc.id "
					+"left join child_tour_category ctc on gctc.child_category_id = ctc.id "
					+"left join tour_category tc on ctc.tour_category_id = tc.id "
					+"where e.broker_id='"+brokerId+"' and e.event_status='ACTIVE' "
					+"and ee.Id not in (select exchange_event_id from "+posUrl+".event) "
					+"order by ee.create_datetime";
					*/
		String sql = "select ee.Id as 'id',ee.create_datetime AS 'createdDate',ee.Event as 'name',ee.EventDate 'date',ee.EventDate as 'time',"
					+"ee.Venue AS 'venue',ee.City as 'city',ee.State as 'state',pc.event_parent_category_desc as parentCategory,cc.event_child_category_desc as childCategory,"
					+"gc.event_grandchild_category_desc as grandChildCategory from "+posUrl+".exchange_event ee "
					+"inner join event e on ee.Id = e.admitone_id "
					+"left join "+posUrl+".[event_grandchild_category] gc on gc.event_grandchild_category_id=ee.grandchildID "
					+"left join "+posUrl+".[event_child_category] cc on cc.event_child_category_id=ee.childID "
					+"left join "+posUrl+".[event_parent_category] pc on pc.event_parent_category_id=ee.parentID "
					+"where e.broker_id='"+brokerId+"' and e.event_status='ACTIVE' "
					+"and ee.Id not in (select exchange_event_id from "+posUrl+".event  where exchange_event_id is not null) "
					+"order by ee.create_datetime";
		//System.out.println(sql);
		List<NewPOSEvents> list = null;
		Session session = null;
		try{
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(NewPOSEvents.class)).list();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getAllActiveTmatEventsWithoutCSVByParentCategory(String parentCategory, String fromDate, String toDate, List<String> country){
		String sql = "select distinct e.id,e.name as EventName,CASE WHEN e.event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), e.event_date, 101) END AS EventDate,"
			+" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))WHEN e.event_time is null THEN 'TBD' END as EventTime,"
			+" v.building as Venue,tc.name as parentCategory,e.event_date,e.event_time,"
			+" CASE WHEN e.creation_date IS NULL THEN ' ' ELSE CONVERT(VARCHAR(10), e.creation_date, 101)+'  '+"
			+" LTRIM(RIGHT(CONVERT(VARCHAR(20), e.creation_date, 100), 7)) END as 'Creation Date',t.name as 'artist name',v.city,v.state,v.country"
			+" from event e "
			+" inner join artist t on e.artist_id=t.id "
			+" inner join grand_child_tour_category gctc on t.grand_child_category_id=gctc.id "
			+" inner join child_tour_category ctc on gctc.child_category_id=ctc.id "
			+" inner join tour_category tc on ctc.tour_category_id=tc.id "
			+" left join venue v on e.venue_id=v.id "
			+" left join category_new c on c.venue_category_id=e.venue_category_id "
			+" where e.event_status='ACTIVE' and c.venue_category_id is null"
			+" and CAST( e.creation_date as DATE)>='"+fromDate+"' and CAST( e.creation_date as DATE)<='"+toDate+"'"
			+" and e.name not like '%parking%'";
		if(!parentCategory.equals("All")) {
			sql += " and tc.name='"+parentCategory+"'";
		}
		
		//Country filter criteria 
		if(!country.contains("others")){
			sql += " and v.country in ('US','CA')";
		}else if(country.contains("others") && !country.contains("US&CA")){
			sql += " and v.country not in ('US','CA')";
		}
		
		sql += " order by e.event_date,e.event_time,e.name";
		
		Session session = null;
		List<Object[]> list = null;
		try{
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sql);
			list = sqlQuery.list();
		}catch(Exception e){
			e.printStackTrace();
			return null;
			
		}finally{
			session.close();
		}
		return list;
	}
	public List<VWTGCatsCategoryTicket> getTnTgCategoryTickets(String brokerId){
		
		//System.out.println("In Mini cat");
		Session session = null;
	    List<VWTGCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		
		String  brokerLinkedServer = "";
		
		if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
		}
		
		try{
			
		queryString.append(" select distinct ee.Event as EventName, ");
		queryString.append(" CONVERT(VARCHAR(19),ee.EventDate,101) as EventDate,");
		queryString.append(" CASE WHEN ee.EventDate is not null THEN"); 
		queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), ee.EventDate, 100), 7))"); 
		queryString.append(" WHEN ee.EventDate is null THEN 'TBD' END as EventTime,");
		queryString.append(" ee.Venue as VenueName,vc.section_low as Section,vc.row_low as RowRange,tg.ticket_count as Quantity,retail_price as TnPrice,ee.EventDate ");
		queryString.append(" from "+brokerLinkedServer+".category_ticket_group tg ");
		queryString.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		queryString.append(" inner join "+brokerLinkedServer+".exchange_event ee on ee.Id=tg.exchange_event_id ");
		queryString.append(" where internal_notes='MINICATS' and retail_price>0 and broadcast=1 ");
		queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null) "); 
		queryString.append(" order by ee.EventDate,EventName,Section,RowRange,Quantity ");
		
		//System.out.println("download category "+queryString.toString());
		
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("RowRange", new StringType()).addScalar("TnPrice", new DoubleType());
		
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWTGCatsCategoryTicket.class)).list();
	    tickets=query.list();    
	   
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		
		 return tickets;
			
		}
	public List<VWVipCatsCategoryTicket> getVwVipCatsCategoryTicketDAO(String brokerId)
	{
		//System.out.println("In VIP MINI ");
		
		Session session = null;
		List<VWVipCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
	    String  brokerLinkedServer = "";
		
	    if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
		}
	    
		try
		{
		queryString.append(" select distinct ee.Event as EventName,");
		queryString.append(" CONVERT(VARCHAR(19),ee.EventDate,101) as EventDate,");
		queryString.append(" CASE WHEN ee.EventDate is not null THEN "); 
		queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), ee.EventDate, 100), 7)) ");
		queryString.append(" WHEN ee.EventDate is null THEN 'TBD' END as EventTime,");
		queryString.append(" ee.Venue as VenueName,vc.section_low as Section,vc.row_low as AlternateRow,tg.ticket_count as Quantity,retail_price as TnPrice,ee.EventDate ");
		queryString.append(" from "+brokerLinkedServer+".category_ticket_group tg "); 
		queryString.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		queryString.append(" inner join "+brokerLinkedServer+".exchange_event ee on ee.Id=tg.exchange_event_id ");
		queryString.append(" where internal_notes='VIPMINICATS' and retail_price>0 and broadcast=1 ");
		queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null) "); 
		queryString.append(" order by ee.EventDate,EventName,section,AlternateRow,Quantity");
		

		session = sessionFactory.openSession();
		//System.out.println(" download category "+queryString.toString());
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("AlternateRow", new StringType()).addScalar("TnPrice", new DoubleType());
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWVipCatsCategoryTicket.class)).list();
	    tickets=query.list();  
	    
	   
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return tickets;
		
	}
	public List<VWLastRowMiniCatsCategoryTicket> getTnLastRowMiniCatsCatsCategoryTickets(String brokerId){
		
		//System.out.println("In Last Row MINI");
		Session session = null;
		List<VWLastRowMiniCatsCategoryTicket> tickets=null;
		StringBuilder queryString = new StringBuilder();
		String   brokerLinkedServer = "";
			
		if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
		}
		try{
		queryString.append("select distinct ee.Event as EventName,");
		queryString.append("CONVERT(VARCHAR(19),ee.EventDate,101) as EventDate,");
	    queryString.append("CASE WHEN ee.EventDate is not null THEN "); 
		queryString.append("LTRIM(RIGHT(CONVERT(VARCHAR(20), ee.EventDate, 100), 7)) ");
		queryString.append("WHEN ee.EventDate is null THEN 'TBD' END as EventTime,");
		queryString.append("ee.Venue as VenueName,vc.section_low as Section,vc.row_low as LastRow,tg.ticket_count as Quantity,retail_price as TnPrice,ee.EventDate ");
		queryString.append("from "+brokerLinkedServer+".category_ticket_group tg "); 
		queryString.append("inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id ");
		queryString.append("inner join "+brokerLinkedServer+".exchange_event ee on ee.Id=tg.exchange_event_id ");
		queryString.append("where internal_notes='LASTROW MINICATS' and retail_price>0 and broadcast=1 ");
		queryString.append("and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null) "); 
		queryString.append("order by ee.EventDate,EventName,Section,LastRow,Quantity");
		
		session = sessionFactory.openSession();
		
		Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("VenueName", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("TnPrice", new DoubleType()).addScalar("LastRow", new StringType());
		//System.out.println("query"+query);
		tickets=query.setResultTransformer(Transformers.aliasToBean(VWLastRowMiniCatsCategoryTicket.class)).list();
	    tickets=query.list();  
	    

		}catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		return tickets;
	}

	public List<ZonePricingTnCategoryTicket> getTnZonesCategoryTickets(String brokerId){
		
		//System.out.println("In Zones Categrry");
		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<ZonePricingTnCategoryTicket> tickets=null;
		String  brokerLinkedServer = "";
		String internalNotes="";
		if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
			internalNotes="'SCZP'";
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
			internalNotes="'ZP'";
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
			internalNotes="'ZP'";
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
			internalNotes="'ZP'";
		}
			try
			{
				queryString.append(" select distinct ee.Event as EventName,");
				queryString.append(" CONVERT(VARCHAR(19),ee.EventDate,101) as EventDate,");
				queryString.append(" CASE WHEN ee.EventDate is not null THEN");
				queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), ee.EventDate, 100), 7))"); 
				queryString.append(" WHEN ee.EventDate is null THEN 'TBD' END as EventTime,");
				queryString.append(" ee.Venue as Venue,vc.section_low as Section,vc.row_low as Row,tg.ticket_count as Quantity,retail_price as Price,ee.EventDate");
				queryString.append(" from "+brokerLinkedServer+".category_ticket_group tg"); 
				queryString.append(" inner join "+brokerLinkedServer+".venue_category vc on vc.venue_category_id=tg.venue_category_id");
			    queryString.append(" inner join "+brokerLinkedServer+".exchange_event ee on ee.Id=tg.exchange_event_id");
			   	queryString.append(" where internal_notes="+internalNotes+" and retail_price>0 and broadcast=1");
			   	queryString.append(" and tg.category_ticket_group_id not in(select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)"); 
			    queryString.append(" order by ee.EventDate,EventName,section,row,Quantity");

				session = sessionFactory.openSession();	
			
				Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
				//System.out.println("query"+query);
				tickets=query.setResultTransformer(Transformers.aliasToBean(ZonePricingTnCategoryTicket.class)).list();
			    tickets=query.list();  
			    
			    
			    
			}catch(Exception e)
			{
				e.printStackTrace();
			}finally
			{
				session.close();
			}
		
		return tickets;
	}
	public List<ZonedLastRowTnCategoryTicket> getTnZonedLastRowCategoryTickets(String brokerId){
		
		//System.out.println("In Zoned Last Row Categrry");
		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<ZonedLastRowTnCategoryTicket> tickets=null;
		String  brokerLinkedServer = "";
		
		if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
		}
		
			try{
				queryString.append(" select distinct ee.Event as EventName,");
				queryString.append(" CONVERT(VARCHAR(19),ee.EventDate,101) as EventDate,");
				queryString.append(" CASE WHEN ee.EventDate is not null THEN"); 
				queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), ee.EventDate, 100), 7))"); 
				queryString.append(" WHEN ee.EventDate is null THEN 'TBD' END as EventTime,");
				queryString.append(" ee.Venue as Venue,section as Section,row as Row,tg.original_ticket_count as Quantity,retail_price as Price");
				queryString.append(" ,ee.EventDate");
				queryString.append(" from "+brokerLinkedServer+".ticket_group tg"); 
				queryString.append(" inner join "+brokerLinkedServer+".exchange_event ee on ee.Id=tg.exchange_event_id");
				queryString.append(" where internal_notes like 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and retail_price>0 ");
				queryString.append(" and ticket_group_id in (select distinct ticket_group_id from "+brokerLinkedServer+".ticket_group_distribution_method)");
				queryString.append(" and ticket_group_id not in(select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)"); 
				queryString.append(" order by ee.EventDate,EventName,section,row,Quantity");
		
				session = sessionFactory.openSession();	
				Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
				//System.out.println("query"+query);
				tickets=query.setResultTransformer(Transformers.aliasToBean(ZonedLastRowTnCategoryTicket.class)).list();
			    tickets=query.list();  
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				session.close();
			}	
		return tickets;
	}

	public List<LarryLastTnCategoryTicket> getTnLarryLastCategoryTickets(String brokerId){

		
		//System.out.println("In Larry Last Row Categrry");
		Session session = null;
		StringBuilder queryString = new StringBuilder();
		List<LarryLastTnCategoryTicket> tickets=null;
		String  brokerLinkedServer = "";
		
		if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
		}
			
			try{
				
				queryString.append(" select distinct ee.Event as EventName,");
				queryString.append(" CONVERT(VARCHAR(19),ee.EventDate,101) as EventDate,");
				queryString.append(" CASE WHEN ee.EventDate is not null THEN"); 
			    queryString.append(" LTRIM(RIGHT(CONVERT(VARCHAR(20), ee.EventDate, 100), 7))"); 
			    queryString.append(" WHEN ee.EventDate is null THEN 'TBD' END as EventTime,");
			    queryString.append(" ee.Venue as Venue,section as Section,row as Row,tg.original_ticket_count as Quantity,retail_price as Price,ee.EventDate");
			    queryString.append(" from "+brokerLinkedServer+".ticket_group tg"); 
			    queryString.append(" inner join "+brokerLinkedServer+".exchange_event ee on ee.Id=tg.exchange_event_id");
			    queryString.append(" where internal_notes like 'LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' and retail_price>0"); 
			    queryString.append(" and ticket_group_id in (select distinct ticket_group_id from "+brokerLinkedServer+".ticket_group_distribution_method)");
			    queryString.append(" and ticket_group_id not in(select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)"); 
			    queryString.append(" order by ee.EventDate,Event,section,row,Quantity");
				
				
				
				session = sessionFactory.openSession();	
				Query query = session.createSQLQuery(queryString.toString()).addScalar("EventDate", new StringType()).addScalar("EventTime", new StringType()).addScalar("EventName",new StringType()).addScalar("Venue", new StringType()).addScalar("Quantity", new IntegerType()).addScalar("Section", new StringType()).addScalar("Price", new DoubleType()).addScalar("Row", new StringType());
				//System.out.println("query"+query);
				tickets=query.setResultTransformer(Transformers.aliasToBean(LarryLastTnCategoryTicket.class)).list();
			    tickets=query.list();  
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				session.close();
			}	
			
			return tickets;
	}	
	public List<Object[]> getCategoryTicketListingCount(String brokerId){
		List<Object[]> list = null;
		String  brokerLinkedServer = "";
		Session session = null;
		StringBuilder queryString = new StringBuilder();
		
		if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
		}
		try
		{
		queryString.append(" select count(category_ticket_group_id) as count,'TotaLisitngs' as Type,internal_notes as ProductType from "+brokerLinkedServer+".category_ticket_group ctg");
		queryString.append(" where retail_price > 0 and category_ticket_group_id not in(");
		queryString.append(" select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)");
		queryString.append(" and internal_notes in('MINICATS','LASTROW MINICATS','VIPMINICATS','SCZP','AUTOCAT')");
		queryString.append(" group by internal_notes union all");
		queryString.append(" select count(category_ticket_group_id) as count, 'BroadCastedListings' as  Type,internal_notes as ProductType from "+brokerLinkedServer+".category_ticket_group ctg");
		queryString.append(" where retail_price > 0 and broadcast=1 and category_ticket_group_id not in(");
		queryString.append(" select category_ticket_group_id from "+brokerLinkedServer+".category_ticket where invoice_id is not null)");
		queryString.append(" and internal_notes in('MINICATS','LASTROW MINICATS','VIPMINICATS','SCZP','AUTOCAT')");
		queryString.append(" group by internal_notes");
		
		session = sessionFactory.openSession();	
		Query query = session.createSQLQuery(queryString.toString());
		//System.out.println("query"+query);
		list=query.list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}	
		return list;
	}
	public List<Object[]> getTicketGroupListingCount(String brokerId){
		
		String  brokerLinkedServer = "";
		List<Object[]> list = null;
		Session session = null;
		StringBuilder queryString = new StringBuilder();
		Broker broker = null;
		if(brokerId!=null && !brokerId.isEmpty()) {
			broker = DAORegistry.getBrokerDAO().get(Integer.parseInt(brokerId));
		}
		if(brokerId.equals("2")) {
			brokerLinkedServer=stubJktPos;
		}else if(brokerId.equals("3")){
			brokerLinkedServer=tn2RotPos;
		} else if(brokerId.equals("5")) {
			brokerLinkedServer=tixCityPos;
		} else if(brokerId.equals("10")) {
			brokerLinkedServer=rtwTwoPos;
		}
			try{

				queryString.append(" select count(ticket_group_id) as count,'TotaLisitngs' as Type,internal_notes as ProductType from "+brokerLinkedServer+".ticket_group"); 
				queryString.append(" where retail_price > 0 and office_id=2 and ticket_group_id not in(");
				queryString.append(" select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)");
				queryString.append(" and internal_notes in('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')");
				queryString.append(" group by internal_notes");
				queryString.append(" union all");
				queryString.append(" select count(ticket_group_id) as count, 'BroadCastedListings' as  Type,internal_notes as ProductType from "+brokerLinkedServer+".ticket_group");
				queryString.append(" where retail_price > 0 and office_id="+broker.getSsAccountOfficeId()+" and ticket_group_id not in(");
				queryString.append(" select ticket_group_id from "+brokerLinkedServer+".ticket where invoice_id is not null)");
				queryString.append(" and internal_notes in('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')");
				queryString.append(" and ticket_group_id in(select distinct ticket_group_id from "+brokerLinkedServer+".ticket_group_distribution_method)");
				queryString.append(" group by internal_notes");
				
				session = sessionFactory.openSession();	
				Query query = session.createSQLQuery(queryString.toString());
				//System.out.println("query"+query);
				list=query.list();
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				if(session != null) {
					session.close();
				}
			}	
		return list;
	}	
	
public List<Object[]> getRotCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
		
		String posUrl = rtwTwoPos;
		
		Session session = null;
		List<Object[]> list = null;
		StringBuilder queryString = new StringBuilder();
		
		try{
			/*queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
			queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
			queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
			queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");
			queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
			queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
			queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
			queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
			queryString.append("left join (select cb.client_broker_id,ii.invoice_id,cb.client_broker_company_name from invoice ii ");
			queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
			queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
			queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
			queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
			queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");*/
			
			queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
			queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
			queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
			queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
			queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
			queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
			queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
			queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+posUrl+".category_ticket ct inner join "+posUrl+".category_ticket_group tg on "); 
			queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
			queryString.append("inner join "+posUrl+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
			queryString.append("UNION ");
			queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
			queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
			queryString.append("from "+posUrl+".ticket t inner join "+posUrl+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
			queryString.append("inner join "+posUrl+".invoice i on i.invoice_id = ctgg.invoice_id ");
			queryString.append("inner join "+posUrl+".event e on e.event_id = ctgg.event_id left join "+posUrl+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
			queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+posUrl+".invoice ii ");
			queryString.append("inner join "+posUrl+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
			queryString.append("inner join "+posUrl+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
			queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+posUrl+".client c inner join "+posUrl+".client_invoice ci on ");
			queryString.append("c.client_id=ci.client_id inner join "+posUrl+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
			queryString.append("left join "+posUrl+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
			
			
			if(reportType.equalsIgnoreCase("AUTOCAT")){
				queryString.append("ctgg.internal_notes ='AUTOCAT' and ");
			}else if(reportType.equalsIgnoreCase("MINICATS")){
				queryString.append("ctgg.internal_notes = 'MINICATS' and ");
			}else if(reportType.equalsIgnoreCase("VIPCATS")){
				queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
			}else if(reportType.equalsIgnoreCase("VIPAUTO")){
				queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
			}else if(reportType.equalsIgnoreCase("VIPLR")){
				queryString.append("ctgg.internal_notes = 'VIPLR' and ");
			}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
				queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
			}else if(reportType.equalsIgnoreCase("SGK")){
				queryString.append("ctgg.internal_notes = 'SGK' and ");
			}else if(reportType.equalsIgnoreCase("LARRYLAST")){
				queryString.append("ctgg.internal_notes = 'LarryLast' and ");
			}else if(reportType.equalsIgnoreCase("ZONES")){
				queryString.append("ctgg.internal_notes not in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','VIPLR','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','SGK') and ");
				queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
				//queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
				//queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
			}
			else if(reportType.equalsIgnoreCase("ZONES PRICING")){
				queryString.append("ctgg.internal_notes in('ZP','RTWZP','SCZP') and ");
			}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
				queryString.append("ctgg.internal_notes = 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and ");
			}else if(reportType.equalsIgnoreCase("MANUAL")){
				queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ctgg.internal_notes != 'SGK' and ");
				queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
			}else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
				queryString.append("ccb.client_broker_id=4759 and ");
			}else if(reportType.equalsIgnoreCase("VIVID LARRY")){
				queryString.append("ccb.client_broker_id=4760 and ");
			}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
				queryString.append("ccb.client_broker_id=4763 and ");
			}/*else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
				queryString.append("ccb.client_broker_id=4761 and ");
			}*/
			else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
				queryString.append("ccb.client_broker_id=4761 and ");
			}/*else if(reportType.equalsIgnoreCase("TICKET CITY MINICATS")){
				queryString.append("ccb.client_broker_id=4417 and ");
			}else if(reportType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
				queryString.append("ccb.client_broker_id=4418and ");
			}else if(reportType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
				queryString.append("ccb.client_broker_id=4416 and ");
			}else if(reportType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
				queryString.append("ccb.client_broker_id=4420 and ");
			}*/
			
			if(poInfo.equalsIgnoreCase("WITHPO")){
				queryString.append("p.purchase_order_id is not null and ");
			}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
				queryString.append("p.purchase_order_id is null and ");
			}
				
			queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
			//System.out.println("Rot download category sales report query "+queryString.toString());
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
			list = query.list();
						
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}

public List<Object[]> getJktCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		/*queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
		queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
		queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
		queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
		queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
		queryString.append("left join (select cb.client_broker_id,ii.invoice_id,cb.client_broker_company_name from invoice ii ");
		queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
		queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");*/
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+stubJktPos+".category_ticket ct inner join "+stubJktPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+stubJktPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+stubJktPos+".ticket t inner join "+stubJktPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+stubJktPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+stubJktPos+".event e on e.event_id = ctgg.event_id left join "+stubJktPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+stubJktPos+".invoice ii ");
		queryString.append("inner join "+stubJktPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+stubJktPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+stubJktPos+".client c inner join "+stubJktPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+stubJktPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+stubJktPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		
		if(reportType.equalsIgnoreCase("AUTOCAT")){
			queryString.append("ctgg.internal_notes ='AUTOCAT' and ");
		}else if(reportType.equalsIgnoreCase("MINICATS")){
			queryString.append("ctgg.internal_notes = 'MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPCATS")){
			queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPAUTO")){
			queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
		}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("LARRYLAST")){
			queryString.append("ctgg.internal_notes = 'LarryLast' and ");
		}else if(reportType.equalsIgnoreCase("ZONES")){
			queryString.append("ctgg.internal_notes not in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') and ");
			queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
			//queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			//queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
		}
		else if(reportType.equalsIgnoreCase("ZONES PRICING")){
			queryString.append("ctgg.internal_notes = 'ZP' and ");
		}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}else if(reportType.equalsIgnoreCase("MANUAL")){
			queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
		}else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
			queryString.append("ccb.client_broker_id=3903 and ");
		}else if(reportType.equalsIgnoreCase("VIVID LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=3904 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
			queryString.append("ccb.client_broker_id=1234567890 and ");
		}/*else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
			queryString.append("ccb.client_broker_id=3902 and ");
		}*/
		else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=3902 and ");
		}/*else if(reportType.equalsIgnoreCase("TICKET CITY MINICATS")){
			queryString.append("ccb.client_broker_id=4417 and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
			queryString.append("ccb.client_broker_id=4418and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4416 and ");
		}else if(reportType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4420 and ");
		}*/
		
		if(poInfo.equalsIgnoreCase("WITHPO")){
			queryString.append("p.purchase_order_id is not null and ");
		}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
			queryString.append("p.purchase_order_id is null and ");
		}
			
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		//System.out.println("Jkt download category sales report query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		session.close();
	}
	return list;
}

public List<Object[]> getRtwCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
	
	String posUrl = rtwPos;
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+posUrl+".category_ticket ct inner join "+posUrl+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+posUrl+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+posUrl+".ticket t inner join "+posUrl+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+posUrl+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+posUrl+".event e on e.event_id = ctgg.event_id left join "+posUrl+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+posUrl+".invoice ii ");
		queryString.append("inner join "+posUrl+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+posUrl+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+posUrl+".client c inner join "+posUrl+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+posUrl+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+posUrl+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		
		if(reportType.equalsIgnoreCase("AUTOCAT")){
			queryString.append("ctgg.internal_notes ='AUTOCAT' and ");
		}else if(reportType.equalsIgnoreCase("MINICATS")){
			queryString.append("ctgg.internal_notes = 'MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPCATS")){
			queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPAUTO")){
			queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
		}else if(reportType.equalsIgnoreCase("VIPLR")){
			queryString.append("ctgg.internal_notes = 'VIPLR' and ");
		}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("SGK")){
			queryString.append("ctgg.internal_notes = 'SGK' and ");
		}else if(reportType.equalsIgnoreCase("LARRYLAST")){
			queryString.append("ctgg.internal_notes = 'LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}/*else if(reportType.equalsIgnoreCase("ZONES")){
			queryString.append("ctgg.internal_notes not in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') and ");
			queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
		}*/
		else if(reportType.equalsIgnoreCase("ZONES PRICING")){
			queryString.append("ctgg.internal_notes in ('SCZP','ZP','RTWZP') and ");
		}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}/*else if(reportType.equalsIgnoreCase("TICKET CITY MINICATS")){
			queryString.append("ccb.client_broker_id=4417 and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
			queryString.append("ccb.client_broker_id=4418and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4416 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4419 and ");
		}else if(reportType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4420 and ");
		}*//*else if(reportType.equalsIgnoreCase("MANUAL")){
			queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
			queryString.append("ccb.client_broker_id=4759 and ");
		}
		else if(reportType.equalsIgnoreCase("VIVID LARRY")){
			queryString.append("ccb.client_broker_id=4760 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
			queryString.append("ccb.client_broker_id=4763 and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
			queryString.append("ccb.client_broker_id=4761 and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4761 and ");
		}*/
		else {
			queryString.append("ctgg.internal_notes in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','VIPLR','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','SGK','RTWZP','SCZP') and ");
		}
		
		if(poInfo.equalsIgnoreCase("WITHPO")){
			queryString.append("p.purchase_order_id is not null and ");
		}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
			queryString.append("p.purchase_order_id is null and ");
		}
			
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		//System.out.println("Rot download category sales report query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		session.close();
	}
	return list;
}

public List<Object[]> getTixcityCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
	
	String posUrl = tixCityPos;
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+posUrl+".category_ticket ct inner join "+posUrl+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+posUrl+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+posUrl+".ticket t inner join "+posUrl+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+posUrl+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+posUrl+".event e on e.event_id = ctgg.event_id left join "+posUrl+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+posUrl+".invoice ii ");
		queryString.append("inner join "+posUrl+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+posUrl+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+posUrl+".client c inner join "+posUrl+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+posUrl+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+posUrl+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		
		if(reportType.equalsIgnoreCase("AUTOCAT")){
			queryString.append("ctgg.internal_notes ='AUTOCAT' and ");
		}else if(reportType.equalsIgnoreCase("MINICATS")){
			queryString.append("ctgg.internal_notes = 'MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPCATS")){
			queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPAUTO")){
			queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
		}else if(reportType.equalsIgnoreCase("VIPLR")){
			queryString.append("ctgg.internal_notes = 'VIPLR' and ");
		}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("SGK")){
			queryString.append("ctgg.internal_notes = 'SGK' and ");
		}else if(reportType.equalsIgnoreCase("LARRYLAST")){
			queryString.append("ctgg.internal_notes = 'LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}/*else if(reportType.equalsIgnoreCase("ZONES")){
			queryString.append("ctgg.internal_notes not in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') and ");
			queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
		}*/
		else if(reportType.equalsIgnoreCase("ZONES PRICING")){
			queryString.append("ctgg.internal_notes in ('SCZP','ZP','RTWZP') and ");
		}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}/*else if(reportType.equalsIgnoreCase("TICKET CITY MINICATS")){
			queryString.append("ccb.client_broker_id=4417 and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
			queryString.append("ccb.client_broker_id=4418and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4416 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4419 and ");
		}else if(reportType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4420 and ");
		}*//*else if(reportType.equalsIgnoreCase("MANUAL")){
			queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
			queryString.append("ccb.client_broker_id=4759 and ");
		}
		else if(reportType.equalsIgnoreCase("VIVID LARRY")){
			queryString.append("ccb.client_broker_id=4760 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
			queryString.append("ccb.client_broker_id=4763 and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
			queryString.append("ccb.client_broker_id=4761 and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4761 and ");
		}*/
		else {
			queryString.append("ctgg.internal_notes in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','VIPLR','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','SGK','RTWZP','SCZP') and ");
		}
		
		if(poInfo.equalsIgnoreCase("WITHPO")){
			queryString.append("p.purchase_order_id is not null and ");
		}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
			queryString.append("p.purchase_order_id is null and ");
		}
			
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		//System.out.println("Rot download category sales report query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		session.close();
	}
	return list;
}

public List<Object[]> getTixcityNewCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
	
	String posUrl = tixCityNewPos;
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+posUrl+".category_ticket ct inner join "+posUrl+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+posUrl+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+posUrl+".ticket t inner join "+posUrl+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+posUrl+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+posUrl+".event e on e.event_id = ctgg.event_id left join "+posUrl+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+posUrl+".invoice ii ");
		queryString.append("inner join "+posUrl+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+posUrl+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+posUrl+".client c inner join "+posUrl+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+posUrl+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+posUrl+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		
		if(reportType.equalsIgnoreCase("AUTOCAT")){
			queryString.append("ctgg.internal_notes ='AUTOCAT' and ");
		}else if(reportType.equalsIgnoreCase("MINICATS")){
			queryString.append("ctgg.internal_notes = 'MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPCATS")){
			queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPAUTO")){
			queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
		}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("LARRYLAST")){
			queryString.append("ctgg.internal_notes = 'LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}/*else if(reportType.equalsIgnoreCase("ZONES")){
			queryString.append("ctgg.internal_notes not in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') and ");
			queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
		}*/
		else if(reportType.equalsIgnoreCase("ZONES PRICING")){
			queryString.append("ctgg.internal_notes in ('ZP','TCZP') and ");
		}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}/*else if(reportType.equalsIgnoreCase("TICKET CITY MINICATS")){
			queryString.append("ccb.client_broker_id=4417 and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
			queryString.append("ccb.client_broker_id=4418and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4416 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4419 and ");
		}else if(reportType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4420 and ");
		}*//*else if(reportType.equalsIgnoreCase("MANUAL")){
			queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
			queryString.append("ccb.client_broker_id=4759 and ");
		}
		else if(reportType.equalsIgnoreCase("VIVID LARRY")){
			queryString.append("ccb.client_broker_id=4760 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
			queryString.append("ccb.client_broker_id=4763 and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
			queryString.append("ccb.client_broker_id=4761 and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4761 and ");
		}*/
		else {
			queryString.append("ctgg.internal_notes in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') and ");
		}
		
		if(poInfo.equalsIgnoreCase("WITHPO")){
			queryString.append("p.purchase_order_id is not null and ");
		}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
			queryString.append("p.purchase_order_id is null and ");
		}
			
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		//System.out.println("Rot download category sales report query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		session.close();
	}
	return list;
}

public List<Object[]> ManhattanCategorySalesReport(Date startDate,Date endDate,String reportType,String poInfo){
	
	String posUrl = manhattanPos;
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+posUrl+".category_ticket ct inner join "+posUrl+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+posUrl+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+posUrl+".ticket t inner join "+posUrl+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+posUrl+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+posUrl+".event e on e.event_id = ctgg.event_id left join "+posUrl+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name,cb.client_broker_id from "+posUrl+".invoice ii ");
		queryString.append("inner join "+posUrl+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+posUrl+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+posUrl+".client c inner join "+posUrl+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+posUrl+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+posUrl+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		
		
		if(reportType.equalsIgnoreCase("AUTOCAT")){
			queryString.append("ctgg.internal_notes ='AUTOCAT' and ");
		}else if(reportType.equalsIgnoreCase("MINICATS")){
			queryString.append("ctgg.internal_notes = 'MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPCATS")){
			queryString.append("ctgg.internal_notes = 'VIPMINICATS' and ");
		}else if(reportType.equalsIgnoreCase("VIPAUTO")){
			queryString.append("ctgg.internal_notes = 'VIPAUTO' and ");
		}else if(reportType.equalsIgnoreCase("LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'LASTROW MINICATS' and ");
		}else if(reportType.equalsIgnoreCase("LARRYLAST")){
			queryString.append("ctgg.internal_notes = 'LarryLast NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}/*else if(reportType.equalsIgnoreCase("ZONES")){
			queryString.append("ctgg.internal_notes not in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') and ");
			queryString.append("(ctgg.section like '%zone%' or ctgg.row like '%zone%') and ");
		}*/
		else if(reportType.equalsIgnoreCase("ZONES PRICING")){
			queryString.append("ctgg.internal_notes in ('ZP','MZP') and ");
		}else if(reportType.equalsIgnoreCase("ZONED LASTROW MINICATS")){
			queryString.append("ctgg.internal_notes = 'ZLR NOSTUB NOTNOW NOVIVID NOTEVO' and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY MINICATS")){
			queryString.append("ccb.client_broker_id=4417 and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY VIPMINICATS")){
			queryString.append("ccb.client_broker_id=4418and ");
		}else if(reportType.equalsIgnoreCase("TICKET CITY LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4416 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4419 and ");
		}else if(reportType.equalsIgnoreCase("FANXCHANGE LASTROW MINICATS")){
			queryString.append("ccb.client_broker_id=4420 and ");
		}/*else if(reportType.equalsIgnoreCase("MANUAL")){
			queryString.append("ctgg.internal_notes !='AUTOCAT' and ctgg.internal_notes !='VIPMINICATS' and ctgg.internal_notes != 'MINICATS' and ctgg.internal_notes != 'LarryLast' and ");
			queryString.append("ctgg.section not like '%zone%' and ctgg.row not like '%zone%' and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("VIVID MINICATS")){
			queryString.append("ccb.client_broker_id=4759 and ");
		}
		else if(reportType.equalsIgnoreCase("VIVID LARRY")){
			queryString.append("ccb.client_broker_id=4760 and ");
		}else if(reportType.equalsIgnoreCase("SCOREBIG MINICATS")){
			queryString.append("ccb.client_broker_id=4763 and ");
		}*/
		
		/*else if(reportType.equalsIgnoreCase("SCOREBIG AUTO")){
			queryString.append("ccb.client_broker_id=4761 and ");
		}*/
		
		else {
			queryString.append("ctgg.internal_notes in('AUTOCAT','MINICATS','VIPMINICATS','VIPAUTO','LASTROW MINICATS','ZLR NOSTUB NOTNOW NOVIVID NOTEVO','LarryLast NOSTUB NOTNOW NOVIVID NOTEVO') and ");
		}
		
		if(poInfo.equalsIgnoreCase("WITHPO")){
			queryString.append("p.purchase_order_id is not null and ");
		}else if(poInfo.equalsIgnoreCase("WITHOUTPO")){
			queryString.append("p.purchase_order_id is null and ");
		}
			
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		//System.out.println("Rot download category sales report query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
					
	}catch(Exception e){
		e.printStackTrace();
	}finally{
		session.close();
	}
	return list;
}


public List<Object[]> getRotCategorySalesReportToday(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
/*		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
		queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
		queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");		
		//queryString.append("case when tg.internal_notes='AUTOCAT' or tg.internal_notes='MINICATS' or tg.internal_notes='TGCATS' then 'TN ' + tg.internal_notes when ccb.client_broker_company_name = 'Vivid2@ticketgallery.com' then 'VIVID TGCATS' when ccb.client_broker_company_name = 'VividTheater@ticketgallery.com' then 'VIVID AUTO'  else 'MANUAL' end as ProcessedAs, ");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
		queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
		queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from invoice ii ");
		queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
		queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");*/
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+rtwTwoPos+".category_ticket ct with(nolock) inner join "+rtwTwoPos+".category_ticket_group tg with(nolock) on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+rtwTwoPos+".venue_category vc with(nolock) on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+rtwTwoPos+".ticket t with(nolock) inner join "+rtwTwoPos+".ticket_group tg with(nolock) on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg ");  
		queryString.append("inner join "+rtwTwoPos+".invoice i with(nolock) on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+rtwTwoPos+".event e with(nolock) on e.event_id = ctgg.event_id left join "+rtwTwoPos+".ticket t with(nolock) on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+rtwTwoPos+".invoice ii with(nolock) ");
		queryString.append("inner join "+rtwTwoPos+".client_broker_invoice cbi with(nolock) on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+rtwTwoPos+".client_broker cb with(nolock) on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+rtwTwoPos+".client c with(nolock) inner join "+rtwTwoPos+".client_invoice ci with(nolock) on ");
		queryString.append("c.client_id=ci.client_id inner join "+rtwTwoPos+".client_type cy with(nolock) on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+rtwTwoPos+".purchase_order p with(nolock) on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Rot daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
	
}

public List<Object[]> getJktCategorySalesReportToday(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
/*		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,vc.section_low as Section,");
		queryString.append("vc.row_low as Row,ct.category_ticket_group_id as CategoryTicketGroupId,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,tg.retail_price as RetailPrice,");
		queryString.append("ct.actual_price as ActualPrice,i.invoice_total as InvoiceTotal,p.po_total as POTotal,tg.internal_notes,");		
		//queryString.append("case when tg.internal_notes='AUTOCAT' or tg.internal_notes='MINICATS' or tg.internal_notes='TGCATS' then 'TN ' + tg.internal_notes when ccb.client_broker_company_name = 'Vivid2@ticketgallery.com' then 'VIVID TGCATS' when ccb.client_broker_company_name = 'VividTheater@ticketgallery.com' then 'VIVID AUTO'  else 'MANUAL' end as ProcessedAs, ");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from category_ticket ct inner join category_ticket_group tg on ");
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id inner join invoice i on i.invoice_id = ct.invoice_id ");
		queryString.append("inner join venue_category vc on vc.venue_category_id = tg.venue_category_id inner join event e on ");
		queryString.append("e.event_id = tg.event_id left join ticket t on t.ticket_id  = ct.ticket_id ");
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from invoice ii ");
		queryString.append("inner join client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id ");
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from client c inner join client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id ");
		queryString.append("left join purchase_order p on p.purchase_order_id = t.purchase_order_id where ");*/
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+stubJktPos+".category_ticket ct inner join "+stubJktPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+stubJktPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+stubJktPos+".ticket t inner join "+stubJktPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg ");  
		queryString.append("inner join "+stubJktPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+stubJktPos+".event e on e.event_id = ctgg.event_id left join "+stubJktPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+stubJktPos+".invoice ii ");
		queryString.append("inner join "+stubJktPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+stubJktPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+stubJktPos+".client c inner join "+stubJktPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+stubJktPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+stubJktPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Jkt daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
	
}
public List<Object[]> getRtwCategorySalesReportTodayWithQty(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	
	try{
		
		String queryString = "select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section," +
				" ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId," +
				" i.create_date as SalesDate,null as PurchaseOrderId,ctgg.retail_price," +
				" ctgg.actual_price,i.invoice_total as InvoiceTotal,null as POTotal,ctgg.internal_notes," +
				" ccb.client_broker_company_name,c.client_type_desc,v.name as venue,ctgg.qty as quantity,ctgg.sort_col " +
				" from" +
				" (select ct.category_ticket_group_id as ticket_group_id," +
				" vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price," +
				" tg.internal_notes,ct.invoice_id,tg.event_id,0 as sort_col,count(*) as qty" +
				" from "+rtwPos+".category_ticket ct with(nolock) inner join "+rtwPos+".category_ticket_group tg with(nolock) on" +
				" tg.category_ticket_group_id = ct.category_ticket_group_id" +
				" inner join "+rtwPos+".venue_category vc with(nolock) on vc.venue_category_id = tg.venue_category_id" +
				" group by ct.invoice_id,tg.event_id,ct.category_ticket_group_id,vc.section_low,vc.row_low,ct.actual_price,tg.retail_price," +
				" tg.internal_notes" +
				" UNION" +
				" select t.ticket_group_id as ticket_group_id,tg.section,tg.row," +
				" t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id,0 as sort_col,count(*) as qty" +
				" from "+rtwPos+".ticket t with(nolock)" +
				" inner join "+rtwPos+".ticket_group tg with(nolock) on tg.ticket_group_id=t.ticket_group_id" +
				" where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')" +
				" group by t.invoice_id,tg.event_id,t.ticket_group_id,tg.section,tg.row,t.actual_sold_price,t.retail_price,tg.internal_notes" +
				" ) as ctgg" +
				" inner join "+rtwPos+".invoice i with(nolock) on i.invoice_id = ctgg.invoice_id" +
				" inner join "+rtwPos+".event e with(nolock) on e.event_id = ctgg.event_id" +
				" inner join "+rtwPos+".venue v with(nolock) on v.venue_id=e.venue_id" +
				" left join (select ii.invoice_id,cb.client_broker_company_name from "+rtwPos+".invoice ii with(nolock)" +
				" inner join "+rtwPos+".client_broker_invoice cbi with(nolock) on cbi.invoice_id=ii.invoice_id" +
				" inner join "+rtwPos+".client_broker cb with(nolock) on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id" +
				" left join (select cy.client_type_desc,ci.invoice_id from "+rtwPos+".client c with(nolock) inner join "+rtwPos+".client_invoice ci with(nolock) on" +
				" c.client_id=ci.client_id inner join "+rtwPos+".client_type cy with(nolock) on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id" +
				" where" +
				" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id,ctgg.sort_col";
		
		System.out.println("RTW daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
}

public List<Object[]> getRtwCategorySalesReportTodayOld(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+rtwPos+".category_ticket ct with(nolock) inner join "+rtwPos+".category_ticket_group tg with(nolock) on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+rtwPos+".venue_category vc with(nolock) on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+rtwPos+".ticket t with(nolock) inner join "+rtwPos+".ticket_group tg with(nolock) on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+rtwPos+".invoice i with(nolock) on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+rtwPos+".event e with(nolock) on e.event_id = ctgg.event_id left join "+rtwPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+rtwPos+".invoice ii with(nolock) ");
		queryString.append("inner join "+rtwPos+".client_broker_invoice cbi with(nolock) on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+rtwPos+".client_broker cb with(nolock) on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+rtwPos+".client c with(nolock) inner join "+rtwPos+".client_invoice ci with(nolock) on ");
		queryString.append("c.client_id=ci.client_id inner join "+rtwPos+".client_type cy with(nolock) on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+rtwPos+".purchase_order p with(nolock) on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Jkt daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
	
}
public List<Object[]> getSeatGeekCategorySalesReportToday(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		
		queryString.append("select distinct sg.event_name as EventName,sg.event_date as EventDate,sg.section,");
				queryString.append(" sg.row,sg.cat_ticket_id,sg.order_id as InvoiceId,"); 
				queryString.append(" sg.created_date as SalesDate,null as PurchaseOrderId,sg.ticket_sold_price,"); 
				queryString.append(" sg.ticket_sold_price,sg.total_payment as InvoiceTotal,0 as POTotal,'SGK', ");
				queryString.append(" '','',sg.event_time as EventTime,sg.id from  "+zoneTickets+".seatgeek_orders sg with(nolock)");
				queryString.append(" left join "+zoneTickets+".customer_order co with(nolock) on co.secondary_order_id=sg.order_id and co.secondary_order_type='SEATGEEK'");
				queryString.append(" left join "+zoneTickets+".invoice i with(nolock) on i.order_id=co.id");
				queryString.append(" where sg.created_date >= :startDate and sg.created_date <= :endDate");
				queryString.append(" and sg.status not in('failed','denied')");
				queryString.append(" order by sg.created_date,sg.id");
		
		//System.out.println("Jkt daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
	
}

public List<Object[]> getTixcityCategorySalesReportTodayWithQty(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	
	try{
		
		String queryString = "select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section," +
				" ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId," +
				" i.create_date as SalesDate,null as PurchaseOrderId,ctgg.retail_price," +
				" ctgg.actual_price,i.invoice_total as InvoiceTotal,null as POTotal,ctgg.internal_notes," +
				" ccb.client_broker_company_name,c.client_type_desc,v.name as venue,ctgg.qty as quantity,ctgg.sort_col " +
				" from" +
				" (select ct.category_ticket_group_id as ticket_group_id," +
				" vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price," +
				" tg.internal_notes,ct.invoice_id,tg.event_id,0 as sort_col,count(*) as qty" +
				" from "+tixCityPos+".category_ticket ct with(nolock) inner join "+tixCityPos+".category_ticket_group tg with(nolock) on" +
				" tg.category_ticket_group_id = ct.category_ticket_group_id" +
				" inner join "+tixCityPos+".venue_category vc with(nolock) on vc.venue_category_id = tg.venue_category_id" +
				" group by ct.invoice_id,tg.event_id,ct.category_ticket_group_id,vc.section_low,vc.row_low,ct.actual_price,tg.retail_price," +
				" tg.internal_notes" +
				" UNION" +
				" select t.ticket_group_id as ticket_group_id,tg.section,tg.row," +
				" t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id,0 as sort_col,count(*) as qty" +
				" from "+tixCityPos+".ticket t with(nolock)" +
				" inner join "+tixCityPos+".ticket_group tg with(nolock) on tg.ticket_group_id=t.ticket_group_id" +
				" where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')" +
				" group by t.invoice_id,tg.event_id,t.ticket_group_id,tg.section,tg.row,t.actual_sold_price,t.retail_price,tg.internal_notes" +
				" ) as ctgg" +
				" inner join "+tixCityPos+".invoice i with(nolock) on i.invoice_id = ctgg.invoice_id" +
				" inner join "+tixCityPos+".event e with(nolock) on e.event_id = ctgg.event_id" +
				" inner join "+tixCityPos+".venue v with(nolock) on v.venue_id=e.venue_id" +
				" left join (select ii.invoice_id,cb.client_broker_company_name from "+tixCityPos+".invoice ii with(nolock)" +
				" inner join "+tixCityPos+".client_broker_invoice cbi with(nolock) on cbi.invoice_id=ii.invoice_id" +
				" inner join "+tixCityPos+".client_broker cb with(nolock) on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id" +
				" left join (select cy.client_type_desc,ci.invoice_id from "+tixCityPos+".client c with(nolock) inner join "+tixCityPos+".client_invoice ci with(nolock) on" +
				" c.client_id=ci.client_id inner join "+tixCityPos+".client_type cy with(nolock) on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id" +
				" where" +
				" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id,ctgg.sort_col";
		
		//System.out.println("Jkt daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
}

public List<Object[]> getTixCityNewCategorySalesReportToday(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+tixCityNewPos+".category_ticket ct inner join "+tixCityNewPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+tixCityNewPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+tixCityNewPos+".ticket t inner join "+tixCityNewPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+tixCityNewPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+tixCityNewPos+".event e on e.event_id = ctgg.event_id left join "+tixCityNewPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+tixCityNewPos+".invoice ii ");
		queryString.append("inner join "+tixCityNewPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+tixCityNewPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+tixCityNewPos+".client c inner join "+tixCityNewPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+tixCityNewPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+tixCityNewPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Jkt daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
	
}

// manhattan category sales report
public List<Object[]> getManhattanCategorySalesReportToday(Date startDate,Date endDate) throws Exception{
	
	Session session = null;
	List<Object[]> list = null;
	StringBuilder queryString = new StringBuilder();
	
	try{
		
		queryString.append("select distinct e.event_name as EventName,e.event_datetime as EventDate,ctgg.section,");
		queryString.append("ctgg.row,ctgg.ticket_group_id,i.invoice_id as InvoiceId,");
		queryString.append("i.create_date as SalesDate,p.purchase_order_id as PurchaseOrderId,ctgg.retail_price,");
		queryString.append("ctgg.actual_price,i.invoice_total as InvoiceTotal,p.po_total as POTotal,ctgg.internal_notes,");
		queryString.append("ccb.client_broker_company_name,c.client_type_desc from ");
		queryString.append("(select ct.category_ticket_id as ticket_id,ct.category_ticket_group_id as ticket_group_id, ");
		queryString.append("vc.section_low as section,vc.row_low as row,ct.actual_price,tg.retail_price, ");
		queryString.append("tg.internal_notes,ct.invoice_id,tg.event_id from "+manhattanPos+".category_ticket ct inner join "+manhattanPos+".category_ticket_group tg on "); 
		queryString.append("tg.category_ticket_group_id = ct.category_ticket_group_id ");
		queryString.append("inner join "+manhattanPos+".venue_category vc on vc.venue_category_id = tg.venue_category_id ");
		queryString.append("UNION ");
		queryString.append("select t.ticket_id as ticket_id,t.ticket_group_id as ticket_group_id,tg.section,tg.row, ");
		queryString.append("t.actual_sold_price,t.retail_price,tg.internal_notes,t.invoice_id,tg.event_id ");
		queryString.append("from "+manhattanPos+".ticket t inner join "+manhattanPos+".ticket_group tg on tg.ticket_group_id=t.ticket_group_id where tg.internal_notes in ('LarryLast NOSTUB NOTNOW NOVIVID NOTEVO','ZLR NOSTUB NOTNOW NOVIVID NOTEVO')) as ctgg "); 
		queryString.append("inner join "+manhattanPos+".invoice i on i.invoice_id = ctgg.invoice_id ");
		queryString.append("inner join "+manhattanPos+".event e on e.event_id = ctgg.event_id left join "+manhattanPos+".ticket t on t.ticket_id  = ctgg.ticket_id "); 
		queryString.append("left join (select ii.invoice_id,cb.client_broker_company_name from "+manhattanPos+".invoice ii ");
		queryString.append("inner join "+manhattanPos+".client_broker_invoice cbi on cbi.invoice_id=ii.invoice_id ");
		queryString.append("inner join "+manhattanPos+".client_broker cb on cb.client_broker_id=cbi.client_broker_id) as ccb on ccb.invoice_id=i.invoice_id "); 
		queryString.append("left join (select cy.client_type_desc,ci.invoice_id from "+manhattanPos+".client c inner join "+manhattanPos+".client_invoice ci on ");
		queryString.append("c.client_id=ci.client_id inner join "+manhattanPos+".client_type cy on cy.client_type_id=c.client_type_id) as c on c.invoice_id=i.invoice_id "); 
		queryString.append("left join "+manhattanPos+".purchase_order p on p.purchase_order_id = t.purchase_order_id where ");
		queryString.append(" i.invoice_status_id != 2 and i.create_date >= :startDate and i.create_date <= :endDate order by i.create_Date, i.invoice_id");
		
		//System.out.println("Jkt daily query "+queryString.toString());
		session = sessionFactory.openSession();
		Query query = session.createSQLQuery(queryString.toString()).setParameter("startDate", startDate).setParameter("endDate", endDate);
		list = query.list();
		return list;			
	}catch(Exception e){
		e.printStackTrace();
		throw e;
	}finally{
		session.close();
	}
	
}

// queries for tnd sales report in tmat admin - admin tab
// tmatprodscale db table name = tndsalesreport

	public List<String> getAllChild(){
		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct child from tndsalesreport";
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session != null) {
				session.close();
			}
		}
	}
	
	public List<String> getAllVenue(){
		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct venue_name from tndsalesreport";
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session != null) {
				session.close();
			}
		}
	}
	
	public List<String> getAllCompanyName(){
		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct owner_company_name from tndsalesreport";
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session != null) {
				session.close();
			}
		}
	}
	
	public List<String> getAllStockType(){
		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct stock_type from tndsalesreport";
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(sql);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session != null) {
				session.close();
			}
		}
	}

	public List<Object[]> getTndSalesReport(String orderStartDate,
			String orderEndDate, String parent, String child, String venueName,
			String acceptStatus, String ticketType,
			String stockType) {
		
		Session session = null;
		List<Object[]> list = null;
		try{
			String sql = "select order_id,CASE WHEN order_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), order_date, 101) END +'  '+ "+
						 "CASE WHEN order_date is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), order_date, 100), 7)) END as 'order_date', "+
						 "parent,child,event_name,event_id,venue_name, "+
						 "CASE WHEN event_date IS NULL THEN 'TBD' ELSE CONVERT(VARCHAR(10), event_date, 101) END +'  '+ "+
						 "CASE WHEN event_date is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), event_date, 100), 7)) END as 'event_date', "+
						 "ticket_quantity,section,row,wholesale_price,revenue,accept_sub_status_desc,owner_customer_id, "+
						 "owner_company_name,is_category,cost,ticket_type,city_name,state_province,stock_type from tndsalesreport "+
						 "where order_date>= '"+orderStartDate+"' and order_date<= '"+orderEndDate+"' ";
			if(!parent.equalsIgnoreCase("all")){
				sql += "and parent='"+parent+"' ";
			}
			if(!child.equalsIgnoreCase("all")){
				sql += "and child='"+child+"' ";
			}
			if(!venueName.equalsIgnoreCase("all")){
				sql += "and venue_name='"+venueName+"' ";
			}
			if(!acceptStatus.equalsIgnoreCase("all")){
				sql += "and accept_sub_status_desc like '%"+acceptStatus+"%' ";
			}
			if(!ticketType.equalsIgnoreCase("all")){
				sql += "and ticket_type='"+ticketType+"' ";
			}
			/*if(!companyName.equalsIgnoreCase("all")){
				sql += "and owner_company_name='"+companyName+"' ";
			}*/
			if(!stockType.equalsIgnoreCase("all")){
				sql += "and stock_type='"+stockType+"' ";
			}
			
			sql += "order by order_date";
			//System.out.println("sql for tnd sales report -> "+sql);
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(sql);//.setParameter("orderStartDate", orderStartDate).setParameter("orderEndDate", orderEndDate);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
		return null;
		}finally{
			if(session != null) {
				session.close();
			}
		}
	}
	
	public List<String> getChildsByParent(String parent){
		Session session = null;
		List<String> list = null;
		
		try{
			String sql = "select distinct child from tndsalesreport where parent= :parent";
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(sql).setParameter("parent", parent);
			list = query.list();
			return list;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}finally{
			if(session != null) {
				session.close();
			}
		}
	}
	
	public List<PosEvent> getPOSAllSoldShortEvents() throws Exception{
		Session session = null;
		session = sessionFactory.openSession();
		
		List<PosEvent> posEvents = new ArrayList<PosEvent>();
		try {
			String sql = " select distinct eventName,exchangeEventId,eventDate,venueName from (" +
					" SELECT ee.event as eventName,ee.id as exchangeEventId,ee.eventDate as eventDate," +
					" v.name as venueName" +
					" FROM "+rtwPos+".event e with(nolock)" +
					" inner join "+rtwPos+".exchange_event ee with(nolock)  on ee.id=e.exchange_event_id" +
					" inner join "+rtwPos+".venue v with(nolock) on ee.vid=v.venue_id" +
					" where ee.eventDate > GETDATE() and event_id in (" +
					" select distinct event_id from "+rtwPos+".category_ticket_group ctg with(nolock)" +
					" inner join "+rtwPos+".category_ticket ct with(nolock) on ct.category_ticket_group_id=ctg.category_ticket_group_id" +
					" where ct.invoice_id is not null and ct.ticket_id is null)" +
					" union" +
					" SELECT ee.event as eventName,ee.id as exchangeEventId,ee.eventDate as eventDate," +
					" v.name as venueName" +
					" FROM "+rtwTwoPos+".event e with(nolock)" +
					" inner join "+rtwTwoPos+".exchange_event ee with(nolock)  on ee.id=e.exchange_event_id" +
					" inner join "+rtwTwoPos+".venue v with(nolock) on ee.vid=v.venue_id" +
					" where ee.eventDate > GETDATE() and event_id in (" +
					" select distinct event_id from "+rtwTwoPos+".category_ticket_group ctg with(nolock)" +
					" inner join "+rtwTwoPos+".category_ticket ct with(nolock) on ct.category_ticket_group_id=ctg.category_ticket_group_id" +
					" where ct.invoice_id is not null and ct.ticket_id is null) ) as g" +
					" group by eventName,exchangeEventId,eventDate,venueName" +
					" order by eventDate ";
			
			Query query = session.createSQLQuery(sql);
			posEvents = query.setResultTransformer(Transformers.aliasToBean(PosEvent.class)).list();
			
			return posEvents;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null) {
				session.close();
			}
		}
		return null;
	}
	
	public List<ShortTicketGroup> getPOSAllSoldShortTickets() throws Exception{
		Session session = null;
		session = sessionFactory.openSession();
		
		List<ShortTicketGroup> shortTicketGroups = new ArrayList<ShortTicketGroup>();
		try {
			String sql = "select ct.category_ticket_group_id as ticketGroupId,ct.invoice_id as invoiceId,vc.section_low as section,vc.row_low as row," +
					" count(ct.category_ticket_id) as qty,MAX(ct.actual_price) as actualSoldPrice," +
					" ctg.retail_price as retailPrice,ctg.wholesale_price as wholesalePrice,ctg.internal_notes as notes,e.exchange_event_id as exchangeEventId,'RTW' as broker" +
					" from "+rtwPos+".category_ticket ct with(nolock)" +
					" inner join "+rtwPos+".category_ticket_group ctg with(nolock) on ctg.category_ticket_group_id=ct.category_ticket_group_id" +
					" inner join "+rtwPos+".event e on e.event_id=ctg.event_id" +
					" inner join "+rtwPos+".venue_category vc with(nolock) on vc.venue_category_id=ctg.venue_category_id" +
					" inner join "+rtwPos+".invoice i with(nolock) on i.invoice_id = ct.invoice_id where ct.invoice_id is not null and ct.ticket_id is null" +
					" and e.event_datetime>getdate()" +
					" group by ct.category_ticket_group_id,ctg.venue_category_id,ct.invoice_id,vc.section_low," +
					" vc.row_low,ctg.retail_price,ctg.wholesale_price,ctg.internal_notes,e.exchange_event_id" +
					//" order by e.exchange_event_id,section,row,qty,notes" +
					" union " +
					"select ct.category_ticket_group_id as ticketGroupId,ct.invoice_id as invoiceId,vc.section_low as section,vc.row_low as row," +
					" count(ct.category_ticket_id) as qty,MAX(ct.actual_price) as actualSoldPrice," +
					" ctg.retail_price as retailPrice,ctg.wholesale_price as wholesalePrice,ctg.internal_notes as notes,e.exchange_event_id as exchangeEventId,'RTW-2' as broker" +
					" from "+rtwTwoPos+".category_ticket ct with(nolock)" +
					" inner join "+rtwTwoPos+".category_ticket_group ctg with(nolock) on ctg.category_ticket_group_id=ct.category_ticket_group_id" +
					" inner join "+rtwTwoPos+".event e on e.event_id=ctg.event_id" +
					" inner join "+rtwTwoPos+".venue_category vc with(nolock) on vc.venue_category_id=ctg.venue_category_id" +
					" inner join "+rtwTwoPos+".invoice i with(nolock) on i.invoice_id = ct.invoice_id " +
					" where ct.invoice_id is not null and ct.ticket_id is null and e.event_datetime>getdate()" +
					" group by ct.category_ticket_group_id,ctg.venue_category_id,ct.invoice_id,vc.section_low," +
					" vc.row_low,ctg.retail_price,ctg.wholesale_price,ctg.internal_notes,e.exchange_event_id" +
					" order by exchange_event_id,section,row,qty,notes";
			
			System.out.println("SHORT LONG Query : "+sql);
		SQLQuery query = session.createSQLQuery(sql);
		query.addScalar("ticketGroupId",Hibernate.INTEGER);
		query.addScalar("invoiceId",Hibernate.INTEGER);
		//query.addScalar("eventId",Hibernate.INTEGER);
		query.addScalar("exchangeEventId",Hibernate.INTEGER);
		query.addScalar("section",Hibernate.STRING);
		query.addScalar("row",Hibernate.STRING);
		query.addScalar("qty",Hibernate.INTEGER);
		query.addScalar("actualSoldPrice",Hibernate.DOUBLE);
		query.addScalar("retailPrice",Hibernate.DOUBLE);
		query.addScalar("wholesalePrice",Hibernate.DOUBLE);
		query.addScalar("notes",Hibernate.STRING);
		query.addScalar("broker",Hibernate.STRING);
		
			shortTicketGroups = query.setResultTransformer(Transformers.aliasToBean(ShortTicketGroup.class)).list();
			
			return shortTicketGroups;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null) {
				session.close();
			}
		}
		return null;
	}
	
	public List<ShortLongTicketGroup> getPOSAllUnSoldLongTicketsByEvent() throws Exception{
		Session session = null;
		session = sessionFactory.openSession();
		
		List<ShortLongTicketGroup> longTicketGroups = new ArrayList<ShortLongTicketGroup>();
		try {
			String sql = " select tg.ticket_group_id as ticketGroupId,e.exchange_event_id as exchangeEventId,tg.section as section,tg.row as row," +
					" count(tg.ticket_group_id) as remainingTicketCount,tg.retail_price as retailPrice,tg.wholesale_price as wholesalePrice," +
					" t.face_price as facePrice,t.cost as cost,'RTW' as broker" +
					" from "+rtwPos+".ticket_group tg with(nolock)" +
					" inner join "+rtwPos+".ticket t with(nolock) on t.ticket_group_id=tg.ticket_group_id" +
					" inner join "+rtwPos+".event e on e.event_id=tg.event_id" +
					" where office_id=1 and invoice_id is null and e.event_datetime>=getdate() and exchange_event_id is not null and exchange_event_id>0" +
					" group by e.exchange_event_id,tg.ticket_group_id,tg.section,tg.row,tg.retail_price,tg.wholesale_price,t.face_price,t.cost" +
					" union " +
					" select tg.ticket_group_id as ticketGroupId,e.exchange_event_id as exchangeEventId,tg.section as section,tg.row as row," +
					" count(tg.ticket_group_id) as remainingTicketCount,tg.retail_price as retailPrice,tg.wholesale_price as wholesalePrice," +
					" t.face_price as facePrice,t.cost as cost,'RTW-2' as broker" +
					" from "+rtwTwoPos+".ticket_group tg with(nolock)" +
					" inner join "+rtwTwoPos+".ticket t with(nolock) on t.ticket_group_id=tg.ticket_group_id" +
					" inner join "+rtwTwoPos+".event e on e.event_id=tg.event_id" +
					" where office_id=1 and invoice_id is null and e.event_datetime>=getdate() and exchange_event_id is not null and exchange_event_id>0" +
					" group by e.exchange_event_id,tg.ticket_group_id,tg.section,tg.row,tg.retail_price,tg.wholesale_price,t.face_price,t.cost" +
					
					" order by exchangeEventId,section,row,remainingTicketCount";
			
			SQLQuery query = session.createSQLQuery(sql);
			query.addScalar("ticketGroupId",Hibernate.INTEGER);
			//query.addScalar("invoiceId",Hibernate.INTEGER);
			query.addScalar("exchangeEventId",Hibernate.INTEGER);
			query.addScalar("section",Hibernate.STRING);
			query.addScalar("row",Hibernate.STRING);
			query.addScalar("remainingTicketCount",Hibernate.INTEGER);
			query.addScalar("facePrice",Hibernate.DOUBLE);
			query.addScalar("retailPrice",Hibernate.DOUBLE);
			query.addScalar("wholesalePrice",Hibernate.DOUBLE);
			query.addScalar("cost",Hibernate.DOUBLE);
			query.addScalar("broker",Hibernate.STRING);
			longTicketGroups = query.setResultTransformer(Transformers.aliasToBean(ShortLongTicketGroup.class)).list();
			
			return longTicketGroups;
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null) {
				session.close();
			}
		}
		return null;
	}
	
	public Map<String,Set<String>> getAllLarrySectionAndLastRowByVenueCategoryId(Integer venueCategoryId) throws SQLException{
		Session session = null;
		String sql = " SELECT distinct cm.larry_section,cm.last_row " +
				     " FROM category_new c " + 
					 " INNER JOIN category_mapping_new cm ON c.id = cm.category_id " +
					 " WHERE venue_category_id =  :venueCategoryId and cm.larry_section is not null and cm.larry_section !='' " +
					 " and last_row is not null and last_row !=''";//order by cm.id
		Map<String,Set<String>> larrySectionMap = new HashMap<String, Set<String>>();
		try {
//			PreparedStatement statement = connection.prepareStatement(sql);
//			statement.setInt(1, venueCategoryId);
//			resultSet = statement.executeQuery();
			
			session = sessionFactory.openSession();
//			session = QueryManagerDAO.getSession();
//			if(session==null){
//				session = sessionFactory.openSession();
//				QueryManagerDAO.setSession(session);
//			}
			Query query = session.createSQLQuery(sql).setParameter("venueCategoryId", venueCategoryId);
			List<Object> list = null;
			list = query.list();
			if(list != null && !list.isEmpty()) { 
				for(Object object:list){
					Object[] row = (Object[]) object;
					String larrySec = (String)row[0];
					//String larrySec = resultSet.getString("larry_section").replaceAll("\\s+", " ").toLowerCase();
					
					Set<String> lastRows = larrySectionMap.get(larrySec);
					if(lastRows == null) {
						lastRows = new HashSet<String>();
					}
					lastRows.add(((String)row[1]).toUpperCase());
					larrySectionMap.put(larrySec, lastRows);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally{
			if(session != null) {
				session.close();
			}
		}
		return larrySectionMap;
	}

	
	public List<SoldTicketDetail> getSoldTicketDetails(Integer brokerId,Integer artistId,Integer venueId,Integer eventId,Date startDate,Date endDate){
		Session session = null;
		session = sessionFactory.openSession();
		if(eventId!=0){
			artistId=0;
			venueId=0;
		}
		List<SoldTicketDetail> list = null;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		try {
			Query query = session.createSQLQuery("EXEC [pro_sold_ticket_details_new] :brokerId,:startDate,:endDate,:eventId,:artistId,:venueId")
			.addEntity(SoldTicketDetail.class)
			.setParameter("brokerId", brokerId)
			.setParameter("startDate", startDate!=null?df.format(startDate):0)
			.setParameter("endDate", endDate!=null?df.format(endDate):0)
			.setParameter("eventId", eventId)
			.setParameter("artistId", artistId)
			.setParameter("venueId", venueId);
			System.out.println("Start::" + new Date());
			list = query.list();
			System.out.println("End::" + new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null) {
				session.close();
			}
		}
		return list;
	}
	
	public List<SoldTicketDetail> getSoldFilledTicketDetails(Integer brokerId,Integer artistId,Integer venueId,Integer eventId,Date startDate,Date endDate){
		Session session = null;
		session = sessionFactory.openSession();
		if(eventId!=0){
			artistId=0;
			venueId=0;
		}
		List<SoldTicketDetail> list = null;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		try {
			Query query = session.createSQLQuery("EXEC [pro_sold_ticket_details_fulfilled_new] :brokerId,:startDate,:endDate,:eventId,:artistId,:venueId")
			.addEntity(SoldTicketDetail.class)
			.setParameter("brokerId", brokerId)
			.setParameter("startDate", startDate!=null?df.format(startDate):0)
			.setParameter("endDate", endDate!=null?df.format(endDate):0)
			.setParameter("eventId", eventId)
			.setParameter("artistId", artistId)
			.setParameter("venueId", venueId);
			System.out.println("Start::" + new Date());
			list = query.list();
			System.out.println("End::" + new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null) {
				session.close();
			}
		}
		return list;
	}
	
	public BrokerSoldTicketDetail getBrokerSoldTicketDetails(Integer id, Integer brokerId){
		Session session = null;
		session = sessionFactory.openSession();
		
		List<BrokerSoldTicketDetail> list = null;
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		try {
			String sql = "";
			if(brokerId == 3){
				sql = "select ticket_qty as 'quantity',section,internal_notes as 'internalNotes' from rot_sold_ticket_detail_vw where ticket_group_id = :id";
			}else if(brokerId == 4){
				sql = "select ticket_qty as 'quantity',section,internal_notes as 'internalNotes' from manhattan_sold_ticket_detail_vw where ticket_group_id = :id";
			}else if(brokerId == 5){
				sql = "select ticket_qty as 'quantity',section,internal_notes as 'internalNotes' from rtw_sold_ticket_detail_vw where ticket_group_id = :id";
			}else{
				return null;
			}
			Query query = session.createSQLQuery(sql)
			.setParameter("id", id);
			System.out.println("Start::" + new Date());
			list = query.setResultTransformer(Transformers.aliasToBean(BrokerSoldTicketDetail.class)).list();
			System.out.println("End::" + new Date());
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			if(session != null) {
				session.close();
			}
		}
		if(list != null && list.size() == 1){
			return list.get(0);
		}else{
			return null;
		}
	}
	
	public List<Object[]> getTodaysEventSalesList(){
		Session session = null;
		session = sessionFactory.openSession();
		List<Object[]> list = null;
		
		try{
			String query = "select * from vw_tnd_todays_sales_detail_list order by OrderDate desc";
			list = session.createSQLQuery(query).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}
	public List<Object[]> getTnEventSalesListByDateRange(String fromDate,String toDate){
		Session session = null;
		session = sessionFactory.openSession();
		List<Object[]> list = null;
		
		try{
			String query = "SELECT tnd.event_id AS EventId, tnd.event_name AS EventName, CONVERT(VARCHAR(19),tnd.event_date,101) AS EventDate," +
					" CASE WHEN tnd.event_date IS NOT NULL THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), tnd.event_date, 100), 7)) WHEN tnd.event_date IS NULL THEN 'TBD' END AS EventTime," +
					" tnd.venue_name AS Venue, tnd.city_name AS City, tnd.state_province AS State," +
					" ISNULL((SELECT COUNT(id) AS CNT FROM tnd_sales_autopricing WHERE CONVERT(DATE, order_date)= CONVERT(DATE, GETDATE()) AND event_id=tnd.event_id), 0) AS TotalSales," +
					" ISNULL((CASE WHEN tac.EventId IS NOT NULL THEN 'Y' ELSE 'N' END), 'N') AS TmatEvent," +
					" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('ticketnetworkdirect') AND event_id=tac.EventId)='ticketnetworkdirect' THEN 'Y' ELSE 'N' END, 'N') AS TicketNetworkDirect," +
					" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('stubhub') AND event_id=tac.EventId)='stubhub' THEN 'Y' ELSE 'N' END, 'N') AS StubHub," +
					" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('ticketevolution') AND event_id=tac.EventId)='ticketevolution' THEN 'Y' ELSE 'N' END, 'N') AS TicketEvolution," +
					" ISNULL(CASE WHEN (SELECT DISTINCT TOP 1 e.id FROM category_mapping_new cmn INNER JOIN category_new cn ON cn.id=cmn.category_id INNER JOIN event e ON e.venue_category_id=cn.venue_category_id WHERE e.event_status='ACTIVE' and e.id=tac.EventId)=tac.EventId THEN 'Y' ELSE 'N' END, 'N') AS CSV," +
					" MAX(tnd.order_date) AS OrderDate" +
					" FROM" +
					" tnd_sales_autopricing tnd" +
					" left join (SELECT DISTINCT e.id AS EventId, e.admitone_id AS AdmitoneId, e.name AS EventName, CONVERT(VARCHAR(19),e.event_date,101) AS EventDate," +
					" CASE WHEN e.event_time IS NOT NULL THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time IS NULL THEN 'TBD' END AS EventTime" +
					" FROM event e" +
					" INNER JOIN ticket_listing_crawl t on t.event_id=e.id" +
					" WHERE e.event_status='ACTIVE' AND t.enabled=1" +
					" AND CONVERT(DATE,e.event_date) >= CONVERT(DATE, GETDATE())" +
					" AND t.site_id IN ('stubhub','ticketevolution','ticketnetworkdirect')" +
					" ) tac on  tnd.event_id=tac.AdmitoneId" +
					" WHERE tnd.order_date>='2017-09-01' and tnd.order_date<='2017-10-01'" +
					" GROUP BY tnd.event_id, tnd.event_name, tnd.event_date, tnd.venue_name, tnd.city_name, tnd.state_province, tac.EventId" +
					" order by OrderDate desc";
			
			list = session.createSQLQuery(query).list();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}
	
	public List<Invoice> getRTWFulfiledInvoiceList(String startDateStr,String endDateStr , Connection connection) throws Exception{
		
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String sql = "SELECT i.invoice_id,i.ticket_request_id ,i.invoice_total,i.invoice_total_due,i.invoice_balance_due," +
				"i.invoice_due_date,i.invoice_status_id,i.create_date,i.invoice_void_date,i.system_user_id," +
				"u.system_user_fname,u.system_user_lname " +
				" FROM  invoice i with(nolock) inner join system_users u with(nolock) on i.system_user_id=u.system_user_id " +
				" where (i.create_date between ? and ?) and i.system_user_id not in( 9,27)  order by i.create_date";

		  System.out.println(sql);
		ResultSet resultSet =null;
		List<Invoice> posInvoices = new ArrayList<Invoice>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, startDateStr);
			statement.setString(2, endDateStr);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				Invoice invoice = new Invoice();
				invoice.setInvoiceId(resultSet.getInt("invoice_id"));
				invoice.setTicketRequestId(0 != resultSet.getInt("ticket_request_id") ? resultSet.getInt("ticket_request_id"):null);
				invoice.setInvoiceTotal(resultSet.getDouble("invoice_total"));
				invoice.setInvoiceTotalDue(resultSet.getDouble("invoice_total_due"));
				invoice.setInvoiceBalanceDue(resultSet.getDouble("invoice_balance_due"));
				invoice.setInvoiceDueDate(dbDateTimeFormat.parse(resultSet.getString("invoice_due_date")));
				invoice.setInvoiceStatusId(resultSet.getInt("invoice_status_id"));
				invoice.setCreateDate(dbDateTimeFormat.parse(resultSet.getString("create_date")));
				invoice.setInvoiceVoidDate(dbDateTimeFormat.parse(resultSet.getString("invoice_void_date")));
				invoice.setSystemUserId(resultSet.getInt("system_user_id"));
				invoice.setSalesPersonName(resultSet.getString("system_user_fname")+" "+resultSet.getString("system_user_lname"));
				posInvoices.add(invoice);
			}
			return posInvoices;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
public List<Invoice> getTixCityFulfiledInvoiceList(String startDateStr,String endDateStr , Connection connection) throws Exception{
		
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String sql = "SELECT i.invoice_id,i.ticket_request_id ,i.invoice_total,i.invoice_total_due,i.invoice_balance_due," +
				"i.invoice_due_date,i.invoice_status_id,i.create_date,i.invoice_void_date,i.system_user_id," +
				"u.system_user_fname,u.system_user_lname " +
				" FROM  invoice i with(nolock) inner join system_users u with(nolock) on i.system_user_id=u.system_user_id " +
				" where (i.create_date between ? and ?) order by i.create_date";//and i.system_user_id not in( 9,27)

		  System.out.println(sql);
		ResultSet resultSet =null;
		List<Invoice> posInvoices = new ArrayList<Invoice>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, startDateStr);
			statement.setString(2, endDateStr);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				Invoice invoice = new Invoice();
				invoice.setInvoiceId(resultSet.getInt("invoice_id"));
				invoice.setTicketRequestId(0 != resultSet.getInt("ticket_request_id") ? resultSet.getInt("ticket_request_id"):null);
				invoice.setInvoiceTotal(resultSet.getDouble("invoice_total"));
				invoice.setInvoiceTotalDue(resultSet.getDouble("invoice_total_due"));
				invoice.setInvoiceBalanceDue(resultSet.getDouble("invoice_balance_due"));
				invoice.setInvoiceDueDate(dbDateTimeFormat.parse(resultSet.getString("invoice_due_date")));
				invoice.setInvoiceStatusId(resultSet.getInt("invoice_status_id"));
				invoice.setCreateDate(dbDateTimeFormat.parse(resultSet.getString("create_date")));
				invoice.setInvoiceVoidDate(dbDateTimeFormat.parse(resultSet.getString("invoice_void_date")));
				invoice.setSystemUserId(resultSet.getInt("system_user_id"));
				invoice.setSalesPersonName(resultSet.getString("system_user_fname")+" "+resultSet.getString("system_user_lname"));
				posInvoices.add(invoice);
			}
			return posInvoices;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
public List<RTFTicketGroup> getRewardTheFanOrderDetails(String startDateStr,String endDateStr , Connection connection) throws Exception{
	
	DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	String sql = " select i.id as invoiceId,co.id as orderId,i.created_date as invoiceDate,i.invoice_total as invoiceTotal,i.created_by as salesPerson,i.status as invoiceStatus," +
			" e.id as eventId,e.name as eventName,e.event_date as eventDate,e.event_time as eventTime,v.building as venue," +
			" tt.internalNotes,tt.section as section,tt.row,tt.seatLow,tt.seatHigh,tt.quantity , tt.retailPrice,tt.wholesalePrice,tt.cost," +
			" tt.soldPrice,tt.totalSoldPrice,tt.sectionRange,tt.rowRange,sm.name as  deliveryMethod," +
			" CONCAT(c.cust_name,', ',c.last_name) as customerName,c.email as customerEmail,c.phone as customerPhone,c.customer_type as customerType," +
			" tt.isLongSale,tt.purchaseOrderId,tt.purchaseOrderDate,tt.price," +
			" case when i.is_real_tix_upload='Yes' then 'Yes' else 'No' end as isRealTixUploaded" +
			"  from invoice i" +
			" inner join customer_order co on co.id=i.order_id" +
			" inner join customer c on i.customer_id=c.id" +
			" inner join (" +
			" select ctg.internal_notes as internalNotes,ctg.section as section,ctg.row as row,count(*) as quantity," +
			" ctg.price as retailPrice,ctg.price as wholesalePrice," +
			" case when t.cost is not null then t.cost else ctg.cost end as cost," +
			" ct.sold_price as soldPrice, sum(ct.sold_price) as totalSoldPrice,ct.invoice_id,ctg.event_id," +
			" ctg.section_range as sectionRange,ctg.row_range as rowRange,ctg.price as price,ctg.seat_low as seatLow,ctg.seat_high as seatHigh," +
			" co.is_long_sale as isLongSale,po.id as purchaseOrderId,po.created_time as purchaseOrderDate" +
			" from category_ticket ct" +
			" inner join category_ticket_group ctg on ctg.id=ct.category_ticket_group_id" +
			" inner join invoice i on i.id=ct.invoice_id" +
			" inner join customer_order co on co.id=i.order_id" +
			" left join ticket t on t.id=ct.ticket_id" +
			" left join purchase_order po on po.id=t.purchase_order_id" +
			" where ct.invoice_id is not null and co.is_long_sale=0" +
			" group by ctg.id,ctg.internal_notes,ctg.section,ctg.row,ctg.cost,ct.sold_price,ct.sold_price," +
			" ct.invoice_id,ctg.event_id,ctg.section_range,ctg.row_range,ctg.price,ctg.seat_low,ctg.seat_high,t.cost,co.is_long_sale,po.id,po.created_time" +
			" union all" +
			" select tg.internal_notes as internalNotes,tg.section as section,tg.row as row,count(*) as quantity," +
			" tg.price as retailPrice,tg.price as wholesalePrice,t.cost as cost," +
			" t.actual_sold_price as soldPrice, sum(t.actual_sold_price) as totalSoldPrice,t.invoice_id,tg.event_id," +
			" '' as sectionRange,'' as rowRange,tg.price as price,min(t.seat_number)as seatLow,max(t.seat_number) as seatHigh,co.is_long_sale as isLongSale," +
			" po.id as purchaseOrderId,po.created_time as purchaseOrderDate" +
			" from ticket t" +
			" inner join ticket_group tg on tg.id=t.ticket_group_id" +
			" inner join invoice i on i.id=t.invoice_id" +
			" inner join customer_order co on co.id=i.order_id" +
			" inner join purchase_order po on po.id=t.purchase_order_id" +
			" where t.invoice_id is not null and co.is_long_sale=1" +
			" group by tg.id,tg.internal_notes,tg.section,tg.row,tg.price,t.cost,t.actual_sold_price,t.actual_sold_price," +
			" t.invoice_id,tg.event_id,co.is_long_sale,po.id,po.created_time) as tt on tt.invoice_id=i.id" +
			" inner join event e on e.id=tt.event_id" +
			" inner join venue v on v.id=e.venue_id" +
			" inner join shipping_method sm on sm.id=i.shipping_method_id" +
			" where c.is_test_account=0 and co.product_type='REWARDTHEFAN'" +//and i.status!='Voided'
			" and i.created_date >= '"+startDateStr+"' and i.created_date <= '"+endDateStr+"'" +
			" order by i.created_date";//and i.system_user_id not in( 9,27)

	  System.out.println(sql);
	ResultSet resultSet =null;
	List<RTFTicketGroup> rtfTixGroups = new ArrayList<RTFTicketGroup>();
	
	try {
		PreparedStatement statement = connection.prepareStatement(sql);
		//statement.setString(1, startDateStr);
		//statement.setString(2, endDateStr);
		resultSet = statement.executeQuery();
		while(resultSet.next()){
			
			RTFTicketGroup rtfticketGroup = new RTFTicketGroup();
			rtfticketGroup.setInvoiceId(resultSet.getInt("invoiceId"));
			rtfticketGroup.setOrderId(resultSet.getInt("orderId"));
			rtfticketGroup.setInvoiceDate(resultSet.getTimestamp("invoiceDate"));
			rtfticketGroup.setInvoiceTotal(resultSet.getDouble("invoiceTotal"));
			rtfticketGroup.setSalesPerson(resultSet.getString("salesPerson"));
			rtfticketGroup.setInvoiceStatus(resultSet.getString("invoiceStatus"));
			rtfticketGroup.setEventId(resultSet.getInt("eventId"));
			rtfticketGroup.setEventName(resultSet.getString("eventName"));
			rtfticketGroup.setEventDate(resultSet.getTimestamp("eventDate"));
			rtfticketGroup.setEventTime(resultSet.getTimestamp("eventTime"));
			rtfticketGroup.setVenue(resultSet.getString("venue"));
			rtfticketGroup.setInternalNotes(resultSet.getString("internalNotes"));
			rtfticketGroup.setSection(resultSet.getString("section"));
			rtfticketGroup.setRow(resultSet.getString("row"));
			rtfticketGroup.setSeatLow(resultSet.getString("seatLow"));
			rtfticketGroup.setSeatHigh(resultSet.getString("seatHigh"));
			rtfticketGroup.setQuantity(resultSet.getInt("quantity"));
			rtfticketGroup.setRetailPrice(resultSet.getDouble("retailPrice"));
			rtfticketGroup.setWholesalePrice(resultSet.getDouble("wholesalePrice"));
			rtfticketGroup.setCost(resultSet.getDouble("cost"));
			rtfticketGroup.setSoldPrice(resultSet.getDouble("soldPrice"));
			rtfticketGroup.setTotalSoldPrice(resultSet.getDouble("totalSoldPrice"));
			rtfticketGroup.setSectionRange(resultSet.getString("sectionRange"));
			rtfticketGroup.setRowRange(resultSet.getString("rowRange"));
			rtfticketGroup.setDeliveryMethod(resultSet.getString("deliveryMethod"));
			rtfticketGroup.setCustomerName(resultSet.getString("customerName"));
			rtfticketGroup.setCustomerEmail(resultSet.getString("customerEmail"));
			rtfticketGroup.setCustomerPhone(resultSet.getString("customerPhone"));
			rtfticketGroup.setCustomerType(resultSet.getString("customerType"));
			rtfticketGroup.setIsLongSale(resultSet.getBoolean("isLongSale"));
			rtfticketGroup.setPurchaseOrderId(resultSet.getInt("purchaseOrderId"));
			rtfticketGroup.setPurchaseOrderDate(resultSet.getTimestamp("purchaseOrderDate"));
			rtfticketGroup.setPrice(resultSet.getDouble("price"));
			rtfticketGroup.setIsRealTixUploaded(resultSet.getString("isRealTixUploaded"));
			
			rtfTixGroups.add(rtfticketGroup);
		}
		return rtfTixGroups;
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{

	}
	return null;
}
	
	public List<Invoice> getRTWFilledProfitandLossReportInvoiceList(String startDateStr,String endDateStr, Connection connection) throws Exception{
		
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String sql = "SELECT distinct i.invoice_id,i.ticket_request_id ,i.invoice_total,i.invoice_total_due,i.invoice_balance_due," +
				" i.invoice_due_date,i.invoice_status_id,i.create_date,i.invoice_void_date,i.system_user_id," +
				" u.system_user_fname,u.system_user_lname" +
				" FROM  invoice i with(nolock) inner join system_users u with(nolock) on i.system_user_id=u.system_user_id" +
				" inner join (select distinct ct.invoice_id as invoice_id from category_ticket ct" +
				" where fill_date between ? and ? and invoice_id is not null and ticket_id is not null" +
				" union" +
				" select distinct t.invoice_id from ticket t" +
				" inner join invoice i on i.invoice_id=t.invoice_id" +
				" left join category_ticket ct on ct.ticket_id=t.ticket_id" +
				" where ct.ticket_id is null and i.create_date between ? and ?" +
				" ) as tt on tt.invoice_id=i.invoice_id" +
				" where  i.system_user_id not in( 9,27)  order by i.create_date";

		  System.out.println(sql);
		ResultSet resultSet =null;
		List<Invoice> posInvoices = new ArrayList<Invoice>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, startDateStr);
			statement.setString(2, endDateStr);
			statement.setString(3, startDateStr);
			statement.setString(4, endDateStr);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				Invoice invoice = new Invoice();
				invoice.setInvoiceId(resultSet.getInt("invoice_id"));
				invoice.setTicketRequestId(0 != resultSet.getInt("ticket_request_id") ? resultSet.getInt("ticket_request_id"):null);
				invoice.setInvoiceTotal(resultSet.getDouble("invoice_total"));
				invoice.setInvoiceTotalDue(resultSet.getDouble("invoice_total_due"));
				invoice.setInvoiceBalanceDue(resultSet.getDouble("invoice_balance_due"));
				invoice.setInvoiceDueDate(dbDateTimeFormat.parse(resultSet.getString("invoice_due_date")));
				invoice.setInvoiceStatusId(resultSet.getInt("invoice_status_id"));
				invoice.setCreateDate(dbDateTimeFormat.parse(resultSet.getString("create_date")));
				invoice.setInvoiceVoidDate(dbDateTimeFormat.parse(resultSet.getString("invoice_void_date")));
				invoice.setSystemUserId(resultSet.getInt("system_user_id"));
				invoice.setSalesPersonName(resultSet.getString("system_user_fname")+" "+resultSet.getString("system_user_lname"));
				posInvoices.add(invoice);
			}
			return posInvoices;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
public List<Invoice> getRTWFilledProfitandLossReportInvoiceListForNY(String startDateStr,String endDateStr, Connection connection) throws Exception{
		
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String sql = "SELECT distinct i.invoice_id,i.ticket_request_id ,i.invoice_total,i.invoice_total_due,i.invoice_balance_due," +
				" i.invoice_due_date,i.invoice_status_id,i.create_date,i.invoice_void_date,i.system_user_id," +
				" u.system_user_fname,u.system_user_lname,i.notes" +
				" FROM  invoice i with(nolock) inner join system_users u with(nolock) on i.system_user_id=u.system_user_id" +
				" inner join (select distinct ct.invoice_id as invoice_id from category_ticket ct" +
				" inner join category_ticket_group ctg on ctg.category_ticket_group_id=ct.category_ticket_group_id" +
				" inner join event e on e.event_id= ctg.event_id" +
				" inner join venue v on v.venue_id=e.venue_id" +
				" where fill_date between ? and ? and invoice_id is not null and ticket_id is not null" +
				"   and v.state_id=35 " +
				" union" +
				" select distinct t.invoice_id from ticket t" +
				" inner join invoice i on i.invoice_id=t.invoice_id" +
				" left join category_ticket ct on ct.ticket_id=t.ticket_id" +
				" inner join ticket_group tg on tg.ticket_group_id=t.ticket_group_id" +
				" inner join event e on e.event_id= tg.event_id" +
				" inner join venue v on v.venue_id=e.venue_id" +
				" where ct.ticket_id is null and i.create_date between ? and ?" +
				" and v.state_id=35 " +
				" ) as tt on tt.invoice_id=i.invoice_id" +
				" where  i.system_user_id not in( 9,27)  " +
				//" and (i.ticket_request_id is null or i.ticket_request_id not in (select order_id from csv_order_ids)) " +
				" order by i.create_date";

		  System.out.println(sql);
		ResultSet resultSet =null;
		List<Invoice> posInvoices = new ArrayList<Invoice>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, startDateStr);
			statement.setString(2, endDateStr);
			statement.setString(3, startDateStr);
			statement.setString(4, endDateStr);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				Invoice invoice = new Invoice();
				invoice.setInvoiceId(resultSet.getInt("invoice_id"));
				invoice.setTicketRequestId(0 != resultSet.getInt("ticket_request_id") ? resultSet.getInt("ticket_request_id"):null);
				invoice.setInvoiceTotal(resultSet.getDouble("invoice_total"));
				invoice.setInvoiceTotalDue(resultSet.getDouble("invoice_total_due"));
				invoice.setInvoiceBalanceDue(resultSet.getDouble("invoice_balance_due"));
				invoice.setInvoiceDueDate(dbDateTimeFormat.parse(resultSet.getString("invoice_due_date")));
				invoice.setInvoiceStatusId(resultSet.getInt("invoice_status_id"));
				invoice.setCreateDate(dbDateTimeFormat.parse(resultSet.getString("create_date")));
				invoice.setInvoiceVoidDate(dbDateTimeFormat.parse(resultSet.getString("invoice_void_date")));
				invoice.setSystemUserId(resultSet.getInt("system_user_id"));
				invoice.setSalesPersonName(resultSet.getString("system_user_fname")+" "+resultSet.getString("system_user_lname"));
				invoice.setNotes(resultSet.getString("notes"));
				posInvoices.add(invoice);
			}
			return posInvoices;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
public List<PurchaseOrder> getRTWFulfiledPurchaseOrderList(String startDateStr,String endDateStr , Connection connection) throws Exception{
		
		DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		String sql = "select distinct po.purchase_order_id,convert(datetime,po.create_date) as create_date,po.po_total,po.system_user_id" +
				" from purchase_order po with(nolock)" +
				" inner join ticket t with(nolock) on t.purchase_order_id=po.purchase_order_id" +
				" where t.invoice_id is not null and (convert(datetime,po.create_date) between ? and ?)" +
				" order by create_date";

		  System.out.println(sql);
		ResultSet resultSet =null;
		List<PurchaseOrder> posPurchaseOrders = new ArrayList<PurchaseOrder>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setString(1, startDateStr);
			statement.setString(2, endDateStr);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				PurchaseOrder purchaseOrder = new PurchaseOrder();
				purchaseOrder.setPurchaseOrderId(resultSet.getInt("purchase_order_id"));
				purchaseOrder.setPurchaseOrderTotal(resultSet.getDouble("po_total"));
				purchaseOrder.setCreateDate(dbDateTimeFormat.parse(resultSet.getString("create_date")));
				purchaseOrder.setSystemUserId(resultSet.getInt("system_user_id"));
				//purchaseOrder.setSalesPersonName(resultSet.getString("system_user_fname")+" "+resultSet.getString("system_user_lname"));
				posPurchaseOrders.add(purchaseOrder);
			}
			return posPurchaseOrders;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public List<PosEvent> getAllLocalAEvents(Connection connection) throws Exception{
	    DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
	    String sql = "SELECT ee.allow_zone_tickets,e.event_name,e.exchange_event_id,e.event_id,e.event_datetime,v.name as venueName,e.venue_id," +
		"ec.event_parent_category_id FROM event e with(nolock) inner join venue v with(nolock) on e.venue_id=v.venue_id inner join " +
		"event_category ec with(nolock) on e.event_id=ec.event_id left join exchange_event ee with(nolock) on  e.exchange_event_id=ee.Id " +
		"order by e.event_datetime ";
		  
		ResultSet resultSet =null;
		List<PosEvent> posEvents = new ArrayList<PosEvent>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				
				PosEvent event = new PosEvent();
				
				Integer eventID= resultSet.getInt("exchange_event_id");
				
				event.setId(resultSet.getInt("event_id"));
				event.setExchangeEventId((null !=eventID && eventID != 0 )?eventID:-1);
				event.setEventDateStr(dbDateTimeFormat.format(dbDateTimeFormat.parse(resultSet.getString("event_datetime"))));
				event.setEventName(resultSet.getString("event_name"));
				event.setVenueName(resultSet.getString("venueName"));
				event.setVenueId(resultSet.getInt("venue_id"));
				event.setParentCatId(resultSet.getInt("event_parent_category_id"));
				
				Integer zoneticket= resultSet.getInt("allow_zone_tickets");
				
				event.setZoneEvent((null !=zoneticket )?zoneticket:0);
				posEvents.add(event);
			}
			return posEvents;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public List<Integer> getAllSSListingCsvOrderIdsForNY(Connection connection) throws Exception{
		
	    String sql = "select order_id from csv_order_ids";
		  
		ResultSet resultSet =null;
		List<Integer> ordrIds = new ArrayList<Integer>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				Integer orderId= resultSet.getInt("order_id");
				ordrIds.add(orderId);
			}
			return ordrIds;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public List<PosEvent> getAllEventsWithExchangeEvent(Connection connection) throws Exception{
	    DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		
		String sql = "SELECT a.Event,b.event_id,a.Id,a.EventDate,a.VId,a.Venue as venueName" +
				" FROM exchange_event a with(nolock) left join event b with(nolock) on b.exchange_event_id=a.Id order by b.event_id";
		  
		ResultSet resultSet =null;
		List<PosEvent> posEvents = new ArrayList<PosEvent>();
		
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				PosEvent event = new PosEvent();
				Integer eventID= resultSet.getInt("event_id");
				event.setId((null !=eventID && eventID != 0 )?eventID:-1);
				event.setExchangeEventId(resultSet.getInt("Id"));
				event.setEventDateStr(dbDateTimeFormat.format(dbDateTimeFormat.parse(resultSet.getString("EventDate"))));
				event.setEventName(resultSet.getString("Event"));
				event.setVenueName(resultSet.getString("venueName"));
				event.setVenueId(resultSet.getInt("VId"));
				posEvents.add(event);
			}
			return posEvents;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public List<POSTicketGroup> getRTWShortSalesByInvoiceId(Integer invoiceId,Connection connection) throws Exception{
		 DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String sql = "SELECT count(ct.category_ticket_id) as soldQty,MAX(ct.actual_price) as actualPrice,ctg.retail_price," +
					"ctg.wholesale_price,MAX(ptr.wholesale_price) as actualWholeSalePrice,ctg.cost,ctg.event_id," +
					"ctg.category_ticket_group_id, vc.section_low,vc.section_high,vc.row_low,vc.row_high,vc.seat_low,vc.seat_high," +
					"ctg.ticket_count,ctg.internal_notes,MAX(ticket_id) as ticket_id,MAX(fill_date) as fill_date" +
					" FROM category_ticket ct with(nolock) inner join category_ticket_group ctg with(nolock) on " +
					"ctg.category_ticket_group_id=ct.category_ticket_group_id inner join venue_category vc with(nolock) on vc.venue_category_id = ctg.venue_category_id " +
					"left join pos_ticket_request ptr with(nolock) on ptr.ticket_request_id=ct.exchange_request_id where ct.invoice_id=?  " +
					"group by ctg.category_ticket_group_id,ctg.event_id,ctg.category_ticket_group_id, vc.section_low,vc.section_high," +
					"vc.row_low,vc.row_high,vc.seat_low,vc.seat_high,ctg.ticket_count,ctg.retail_price,ctg.wholesale_price,ctg.cost,ctg.internal_notes  ";
			ResultSet resultSet =null;
			List<POSTicketGroup> ticketGroups = new ArrayList<POSTicketGroup>();
			POSTicketGroup categoryTicketGroup = null;
			try {
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setInt(1, invoiceId);
				resultSet = statement.executeQuery();
				while(resultSet.next()){
					categoryTicketGroup = new POSTicketGroup();
					categoryTicketGroup.setEventId(resultSet.getInt("event_id"));
					//categoryTicketGroup.setExchangeEventId(resultSet.getInt("exchange_event_id"));
					categoryTicketGroup.setTicketGroupId(resultSet.getInt("category_ticket_group_id"));
					categoryTicketGroup.setSection(resultSet.getString("section_low"));
					categoryTicketGroup.setRow(resultSet.getString("row_low"));
					categoryTicketGroup.setCatSeatLow(resultSet.getString("seat_low"));
					categoryTicketGroup.setCatSeatHigh(resultSet.getString("seat_high"));
					categoryTicketGroup.setQty(resultSet.getInt("soldQty"));
					categoryTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
					categoryTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
					categoryTicketGroup.setActualSoldPrice(resultSet.getDouble("actualPrice"));			
					categoryTicketGroup.setActualWholeSalePrice(resultSet.getDouble("actualWholeSalePrice"));			
					categoryTicketGroup.setInternalNotes(null !=resultSet.getString("internal_notes")?resultSet.getString("internal_notes"):"");
					categoryTicketGroup.setCost(resultSet.getDouble("cost"));
					if(resultSet.getInt("ticket_id") != 0 &&
							resultSet.getInt("ticket_id") != -1){
						categoryTicketGroup.setIsUnfilled(false);
					}else{
						categoryTicketGroup.setIsUnfilled(true);
					}
					categoryTicketGroup.setFillDate(null !=resultSet.getString("fill_date") && "" !=resultSet.getString("fill_date")?dbDateTimeFormat.parse(resultSet.getString("fill_date")):null);
					
					
					ticketGroups.add(categoryTicketGroup);
				}
				return ticketGroups;
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
			}
			return null;
		}
	
	public List<POSTicketGroup> getTixCityShortSalesByInvoiceId(Integer invoiceId,Connection connection) throws Exception{
		 DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String sql = "SELECT count(ct.category_ticket_id) as soldQty,MAX(ct.actual_price) as actualPrice,ctg.retail_price," +
					"ctg.wholesale_price,MAX(ptr.wholesale_price) as actualWholeSalePrice,ctg.cost,ctg.event_id," +
					"ctg.category_ticket_group_id, vc.section_low,vc.section_high,vc.row_low,vc.row_high,vc.seat_low,vc.seat_high," +
					"ctg.ticket_count,ctg.internal_notes,MAX(ticket_id) as ticket_id,MAX(fill_date) as fill_date" +
					" FROM category_ticket ct with(nolock) inner join category_ticket_group ctg with(nolock) on " +
					"ctg.category_ticket_group_id=ct.category_ticket_group_id inner join venue_category vc with(nolock) on vc.venue_category_id = ctg.venue_category_id " +
					"left join pos_ticket_request ptr with(nolock) on ptr.ticket_request_id=ct.exchange_request_id where ct.invoice_id=?  " +
					"group by ctg.category_ticket_group_id,ctg.event_id,ctg.category_ticket_group_id, vc.section_low,vc.section_high," +
					"vc.row_low,vc.row_high,vc.seat_low,vc.seat_high,ctg.ticket_count,ctg.retail_price,ctg.wholesale_price,ctg.cost,ctg.internal_notes  ";
			ResultSet resultSet =null;
			List<POSTicketGroup> ticketGroups = new ArrayList<POSTicketGroup>();
			POSTicketGroup categoryTicketGroup = null;
			try {
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setInt(1, invoiceId);
				resultSet = statement.executeQuery();
				while(resultSet.next()){
					categoryTicketGroup = new POSTicketGroup();
					categoryTicketGroup.setEventId(resultSet.getInt("event_id"));
					//categoryTicketGroup.setExchangeEventId(resultSet.getInt("exchange_event_id"));
					categoryTicketGroup.setTicketGroupId(resultSet.getInt("category_ticket_group_id"));
					categoryTicketGroup.setSection(resultSet.getString("section_low"));
					categoryTicketGroup.setRow(resultSet.getString("row_low"));
					categoryTicketGroup.setCatSeatLow(resultSet.getString("seat_low"));
					categoryTicketGroup.setCatSeatHigh(resultSet.getString("seat_high"));
					categoryTicketGroup.setQty(resultSet.getInt("soldQty"));
					categoryTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
					categoryTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
					categoryTicketGroup.setActualSoldPrice(resultSet.getDouble("actualPrice"));			
					categoryTicketGroup.setActualWholeSalePrice(resultSet.getDouble("actualWholeSalePrice"));			
					categoryTicketGroup.setInternalNotes(null !=resultSet.getString("internal_notes")?resultSet.getString("internal_notes"):"");
					categoryTicketGroup.setCost(resultSet.getDouble("cost"));
					if(resultSet.getInt("ticket_id") != 0 &&
							resultSet.getInt("ticket_id") != -1){
						categoryTicketGroup.setIsUnfilled(false);
					}else{
						categoryTicketGroup.setIsUnfilled(true);
					}
					categoryTicketGroup.setFillDate(null !=resultSet.getString("fill_date") && "" !=resultSet.getString("fill_date")?dbDateTimeFormat.parse(resultSet.getString("fill_date")):null);
					
					
					ticketGroups.add(categoryTicketGroup);
				}
				return ticketGroups;
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
			}
			return null;
		}
	public List<POSTicketGroup> getRTWShortSalesByInvoiceIdandFillDateRange(Integer invoiceId,String startDateStr,String endDateStr,Connection connection) throws Exception{
		 DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String sql = "SELECT count(ct.category_ticket_id) as soldQty,MAX(ct.actual_price) as actualPrice,ctg.retail_price," +
					" ctg.wholesale_price,MAX(ptr.wholesale_price) as actualWholeSalePrice,ctg.cost,ctg.event_id," +
					" ctg.category_ticket_group_id, vc.section_low,vc.section_high,vc.row_low,vc.row_high,vc.seat_low,vc.seat_high," +
					" ctg.ticket_count,ctg.internal_notes,MAX(ct.ticket_id) as ticket_id,fill_date as fill_date," +
					" po.purchase_order_id,(convert(datetime,po.create_date)) as purchase_order_date" +
					" FROM category_ticket ct with(nolock) inner join category_ticket_group ctg with(nolock) on" +
					" ctg.category_ticket_group_id=ct.category_ticket_group_id inner join venue_category vc with(nolock) on vc.venue_category_id = ctg.venue_category_id" +
					" left join pos_ticket_request ptr with(nolock) on ptr.ticket_request_id=ct.exchange_request_id" +
					" inner join ticket t on t.ticket_id=ct.ticket_id" +
					" inner join purchase_order po on po.purchase_order_id=t.purchase_order_id" +
					" where ct.invoice_id=?  and fill_date between ? and ? " +
					" group by ctg.category_ticket_group_id,ctg.event_id,ctg.category_ticket_group_id, vc.section_low,vc.section_high," +
					" vc.row_low,vc.row_high,vc.seat_low,vc.seat_high,ctg.ticket_count,ctg.retail_price,ctg.wholesale_price,ctg.cost,ctg.internal_notes," +
					" fill_date,po.purchase_order_id,convert(datetime,po.create_date) ";
			ResultSet resultSet =null;
			List<POSTicketGroup> ticketGroups = new ArrayList<POSTicketGroup>();
			POSTicketGroup categoryTicketGroup = null;
			try {
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setInt(1, invoiceId);
				statement.setString(2, startDateStr);
				statement.setString(3, endDateStr);
				resultSet = statement.executeQuery();
				while(resultSet.next()){
					categoryTicketGroup = new POSTicketGroup();
					categoryTicketGroup.setEventId(resultSet.getInt("event_id"));
					//categoryTicketGroup.setExchangeEventId(resultSet.getInt("exchange_event_id"));
					categoryTicketGroup.setTicketGroupId(resultSet.getInt("category_ticket_group_id"));
					categoryTicketGroup.setSection(resultSet.getString("section_low"));
					categoryTicketGroup.setRow(resultSet.getString("row_low"));
					categoryTicketGroup.setCatSeatLow(resultSet.getString("seat_low"));
					categoryTicketGroup.setCatSeatHigh(resultSet.getString("seat_high"));
					categoryTicketGroup.setQty(resultSet.getInt("soldQty"));
					categoryTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
					categoryTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
					categoryTicketGroup.setActualSoldPrice(resultSet.getDouble("actualPrice"));			
					categoryTicketGroup.setActualWholeSalePrice(resultSet.getDouble("actualWholeSalePrice"));			
					categoryTicketGroup.setInternalNotes(null !=resultSet.getString("internal_notes")?resultSet.getString("internal_notes"):"");
					categoryTicketGroup.setCost(resultSet.getDouble("cost"));
					if(resultSet.getInt("ticket_id") != 0 &&
							resultSet.getInt("ticket_id") != -1){
						categoryTicketGroup.setIsUnfilled(false);
					}else{
						categoryTicketGroup.setIsUnfilled(true);
					}
					categoryTicketGroup.setFillDate(null !=resultSet.getString("fill_date") && "" !=resultSet.getString("fill_date")?dbDateTimeFormat.parse(resultSet.getString("fill_date")):null);
					
					categoryTicketGroup.setJktPoId(resultSet.getInt("purchase_order_id"));
					categoryTicketGroup.setPurchaseOrderDate(resultSet.getTimestamp("purchase_order_date"));
					
					ticketGroups.add(categoryTicketGroup);
				}
				return ticketGroups;
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
			}
			return null;
		}
	
	public List<POSTicketGroup> getRTWShortSalesByInvoiceIdandFillDateRangeForNY(Integer invoiceId,String startDateStr,String endDateStr,Connection connection) throws Exception{
		 DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
			String sql = "SELECT count(ct.category_ticket_id) as soldQty,MAX(ct.actual_price) as actualPrice,ctg.retail_price," +
					" ctg.wholesale_price,MAX(ptr.wholesale_price) as actualWholeSalePrice,ctg.cost,ctg.event_id," +
					" ctg.category_ticket_group_id, vc.section_low,vc.section_high,vc.row_low,vc.row_high,vc.seat_low,vc.seat_high," +
					" ctg.ticket_count,ctg.internal_notes,MAX(ct.ticket_id) as ticket_id,fill_date as fill_date," +
					" po.purchase_order_id,(convert(datetime,po.create_date)) as purchase_order_date,ctg.create_date,ctg.notes " +
					" FROM category_ticket ct with(nolock) inner join category_ticket_group ctg with(nolock) on" +
					" ctg.category_ticket_group_id=ct.category_ticket_group_id " +
					" inner join venue_category vc with(nolock) on vc.venue_category_id = ctg.venue_category_id" +
					" inner join event e on e.event_id=ctg.event_id" +
					" inner join venue v on v.venue_id=e.venue_id and v.state_id=35" +
					" left join pos_ticket_request ptr with(nolock) on ptr.ticket_request_id=ct.exchange_request_id" +
					" inner join ticket t on t.ticket_id=ct.ticket_id" +
					" inner join purchase_order po on po.purchase_order_id=t.purchase_order_id" +
					" where ct.invoice_id=? " +// and fill_date between ? and ? 
					" group by ctg.category_ticket_group_id,ctg.event_id,ctg.category_ticket_group_id, vc.section_low,vc.section_high," +
					" vc.row_low,vc.row_high,vc.seat_low,vc.seat_high,ctg.ticket_count,ctg.retail_price,ctg.wholesale_price,ctg.cost,ctg.internal_notes," +
					" fill_date,po.purchase_order_id,convert(datetime,po.create_date),ctg.create_date,ctg.notes  ";
			ResultSet resultSet =null;
			List<POSTicketGroup> ticketGroups = new ArrayList<POSTicketGroup>();
			POSTicketGroup categoryTicketGroup = null;
			try {
				PreparedStatement statement = connection.prepareStatement(sql);
				statement.setInt(1, invoiceId);
				//statement.setString(2, startDateStr);
				//statement.setString(3, endDateStr);
				resultSet = statement.executeQuery();
				while(resultSet.next()){
					categoryTicketGroup = new POSTicketGroup();
					categoryTicketGroup.setEventId(resultSet.getInt("event_id"));
					//categoryTicketGroup.setExchangeEventId(resultSet.getInt("exchange_event_id"));
					categoryTicketGroup.setTicketGroupId(resultSet.getInt("category_ticket_group_id"));
					categoryTicketGroup.setSection(resultSet.getString("section_low"));
					categoryTicketGroup.setRow(resultSet.getString("row_low"));
					categoryTicketGroup.setCatSeatLow(resultSet.getString("seat_low"));
					categoryTicketGroup.setCatSeatHigh(resultSet.getString("seat_high"));
					categoryTicketGroup.setQty(resultSet.getInt("soldQty"));
					categoryTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
					categoryTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
					categoryTicketGroup.setActualSoldPrice(resultSet.getDouble("actualPrice"));			
					categoryTicketGroup.setActualWholeSalePrice(resultSet.getDouble("actualWholeSalePrice"));			
					categoryTicketGroup.setInternalNotes(null !=resultSet.getString("internal_notes")?resultSet.getString("internal_notes"):"");
					categoryTicketGroup.setCost(resultSet.getDouble("cost"));
					categoryTicketGroup.setCreatedDate(resultSet.getTimestamp("create_date"));
					if(resultSet.getInt("ticket_id") != 0 &&
							resultSet.getInt("ticket_id") != -1){
						categoryTicketGroup.setIsUnfilled(false);
					}else{
						categoryTicketGroup.setIsUnfilled(true);
					}
					categoryTicketGroup.setFillDate(null !=resultSet.getString("fill_date") && "" !=resultSet.getString("fill_date")?dbDateTimeFormat.parse(resultSet.getString("fill_date")):null);
					
					categoryTicketGroup.setJktPoId(resultSet.getInt("purchase_order_id"));
					categoryTicketGroup.setPurchaseOrderDate(resultSet.getTimestamp("purchase_order_date"));
					categoryTicketGroup.setExternalNotes(resultSet.getString("notes"));
					
					ticketGroups.add(categoryTicketGroup);
				}
				return ticketGroups;
			} catch (SQLException e) {
				e.printStackTrace();
			}finally{
			}
			return null;
		}
	
	public List<PosTicket> getJKTTicketsByInvoiceId(Integer invoiceId , Connection connection) throws Exception{
		String sql = "select t.invoice_id,t.ticket_id,t.ticket_group_id,t.section,t.row,t.seat_number,t.seat_order," +
				"t.actual_sold_price,t.purchase_order_id,convert(datetime,po.create_date) as purchase_order_date from ticket t with(nolock) " +
				" left join purchase_order po on po.purchase_order_id=t.purchase_order_id " +
				" where t.invoice_id=? order by t.ticket_group_id,t.ticket_id";
		ResultSet resultSet =null;
		List<PosTicket> posTickets = new ArrayList<PosTicket>();
		PosTicket ticket = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, invoiceId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ticket = new PosTicket();
				ticket.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				ticket.setInvoiceId(resultSet.getInt("invoice_id"));
				ticket.setTicketId(resultSet.getInt("ticket_id"));
				ticket.setSection(resultSet.getString("section"));
				ticket.setRow(resultSet.getString("row"));
				ticket.setSeatNumber(resultSet.getInt("seat_number"));
				ticket.setSeatOrder(resultSet.getInt("seat_order"));
				ticket.setPurchaseOrderId(resultSet.getInt("purchase_order_id"));
				ticket.setActualSoldPrice(resultSet.getDouble("actual_sold_price"));
				
				if(resultSet.getTimestamp("purchase_order_date") != null) {
					ticket.setPurchaseOrderDate(new Date(resultSet.getTimestamp("purchase_order_date").getTime()));
				}
				posTickets.add(ticket);
			}
			return posTickets;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public List<PosTicket> getJKTTicketsByInvoiceIdandPurchaseOrderId(Integer invoiceId ,Integer purchaseOrderId, Connection connection) throws Exception{
		String sql = "select t.invoice_id,t.ticket_id,t.ticket_group_id,t.section,t.row,t.seat_number,t.seat_order," +
				"t.actual_sold_price,t.purchase_order_id from ticket t with(nolock) where t.invoice_id=? and t.purchase_order_id=? order by t.ticket_group_id,t.ticket_id";
		ResultSet resultSet =null;
		List<PosTicket> posTickets = new ArrayList<PosTicket>();
		PosTicket ticket = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, invoiceId);
			statement.setInt(2, purchaseOrderId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ticket = new PosTicket();
				ticket.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				ticket.setInvoiceId(resultSet.getInt("invoice_id"));
				ticket.setTicketId(resultSet.getInt("ticket_id"));
				ticket.setSection(resultSet.getString("section"));
				ticket.setRow(resultSet.getString("row"));
				ticket.setSeatNumber(resultSet.getInt("seat_number"));
				ticket.setSeatOrder(resultSet.getInt("seat_order"));
				ticket.setPurchaseOrderId(resultSet.getInt("purchase_order_id"));
				ticket.setActualSoldPrice(resultSet.getDouble("actual_sold_price"));
				posTickets.add(ticket);
			}
			return posTickets;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	
	public List<PosTicket> getJKTTicketsByInvoiceIdandFillDateRange(Integer invoiceId ,Integer purchaseOrderId,String startDateStr,String endDateStr, Connection connection) throws Exception{
		String sql = "select t.invoice_id,t.ticket_id,t.ticket_group_id,t.section,t.row,t.seat_number,t.seat_order," +
				" t.actual_sold_price,t.purchase_order_id from ticket t with(nolock)" +
				" inner join category_ticket ct on ct.ticket_id=t.ticket_id" +
				" where t.invoice_id=? and t.purchase_order_id=? and ct.fill_date between ? and ? order by t.ticket_group_id,t.ticket_id";
		ResultSet resultSet =null;
		List<PosTicket> posTickets = new ArrayList<PosTicket>();
		PosTicket ticket = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, invoiceId);
			statement.setInt(2, purchaseOrderId);
			statement.setString(3, startDateStr);
			statement.setString(4, endDateStr);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ticket = new PosTicket();
				ticket.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				ticket.setInvoiceId(resultSet.getInt("invoice_id"));
				ticket.setTicketId(resultSet.getInt("ticket_id"));
				ticket.setSection(resultSet.getString("section"));
				ticket.setRow(resultSet.getString("row"));
				ticket.setSeatNumber(resultSet.getInt("seat_number"));
				ticket.setSeatOrder(resultSet.getInt("seat_order"));
				ticket.setPurchaseOrderId(resultSet.getInt("purchase_order_id"));
				ticket.setActualSoldPrice(resultSet.getDouble("actual_sold_price"));
				posTickets.add(ticket);
			}
			return posTickets;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
	public List<PosTicket> getJKTTicketsByInvoiceIdandFillDateRangeForNY(Integer invoiceId ,Integer purchaseOrderId,String startDateStr,String endDateStr, Connection connection) throws Exception{
		String sql = "select t.invoice_id,max(t.ticket_id) as ticket_id,t.ticket_group_id,t.section,t.row,min(convert(int,t.seat_number)) as seat_low," +
				"  max(convert(int,t.seat_number)) as seat_high, t.actual_sold_price,t.purchase_order_id,tg.event_id,tg.cost,count(t.ticket_id) as quantity " +
				" from ticket t with(nolock)" +
				" inner join category_ticket ct on ct.ticket_id=t.ticket_id" +
				" inner join ticket_group tg on tg.ticket_group_id=t.ticket_group_id " +
				" inner join event e on e.event_id=tg.event_id" +
				" inner join venue v on v.venue_id=e.venue_id and v.state_id=35" +
				" where t.invoice_id=? and t.purchase_order_id=?" +
				" group by t.invoice_id,t.ticket_group_id,t.section,t.row,t.actual_sold_price,t.purchase_order_id,tg.event_id,tg.cost ";
		ResultSet resultSet =null;
		List<PosTicket> posTickets = new ArrayList<PosTicket>();
		PosTicket ticket = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, invoiceId);
			statement.setInt(2, purchaseOrderId);
			//statement.setString(3, startDateStr);
			//statement.setString(4, endDateStr);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				ticket = new PosTicket();
				ticket.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				ticket.setInvoiceId(resultSet.getInt("invoice_id"));
				ticket.setTicketId(resultSet.getInt("ticket_id"));
				ticket.setSection(resultSet.getString("section"));
				ticket.setRow(resultSet.getString("row"));
				//ticket.setSeatNumber(resultSet.getInt("seat_number"));
				//ticket.setSeatOrder(resultSet.getInt("seat_order"));
				ticket.setPurchaseOrderId(resultSet.getInt("purchase_order_id"));
				ticket.setActualSoldPrice(resultSet.getDouble("actual_sold_price"));
				ticket.setSeatRange(resultSet.getInt("seat_low")+"-"+resultSet.getInt("seat_high"));
				ticket.setCost(resultSet.getDouble("cost"));
				ticket.setQuantity(resultSet.getInt("quantity"));
				posTickets.add(ticket);
			}
			return posTickets;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}
public POSTicketGroup getBrokerTicketGroupByEventId(Integer ticketGroupId,Connection connection) throws Exception{
		
		String sql = "select event_id,ticket_group_id,remaining_ticket_count,section,row ," +
				" retail_price,wholesale_price,cost,internal_notes,office_id from ticket_group with(nolock) where ticket_group_id=? ";
		
		ResultSet resultSet =null;
		POSTicketGroup pOSTicketGroup = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, ticketGroupId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				pOSTicketGroup = new POSTicketGroup();
				pOSTicketGroup.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				pOSTicketGroup.setEventId(resultSet.getInt("event_id"));
				//pOSTicketGroup.setExchangeEventId(resultSet.getInt("exchange_event_id"));
				pOSTicketGroup.setSection(resultSet.getString("section"));
				pOSTicketGroup.setRow(resultSet.getString("row"));
				pOSTicketGroup.setQty(resultSet.getInt("remaining_ticket_count"));
				pOSTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
				pOSTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
				pOSTicketGroup.setInternalNotes(null !=resultSet.getString("internal_notes")?resultSet.getString("internal_notes"):"");
				pOSTicketGroup.setCost(resultSet.getDouble("cost"));
				pOSTicketGroup.setTicketGroupOfficeId(resultSet.getInt("office_id"));
			}
			return pOSTicketGroup;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{

		}
		return null;
	}

	public List<POSTicketGroup> getBrokerTicketGroupByInvoiceId(Integer invoiceId,Connection connection) throws Exception{
		
		String sql = "SELECT count(t.ticket_id) as soldQty,MAX(t.actual_sold_price) as actualSoldPrice,tg.retail_price," +
				" tg.wholesale_price,tg.cost,tg.event_id," +
				" tg.ticket_group_id,tg.section,tg.row,min(convert(int,seat_number)) as seat_low,max(convert(int,seat_number)) as seat_high," +
				" tg.internal_notes,MAX(t.ticket_id) as ticket_id," +
				" po.purchase_order_id,(convert(datetime,po.create_date)) as purchase_order_date" +
				" FROM ticket t with(nolock) inner join ticket_group tg with(nolock) on" +
				" tg.ticket_group_id=t.ticket_group_id" +
				" inner join purchase_order po with(nolock) on po.purchase_order_id=t.purchase_order_id" +
				" where t.invoice_id=? " +
				" group by tg.ticket_group_id,tg.event_id,tg.ticket_group_id, tg.section,tg.row," +
				" tg.ticket_group_id,tg.retail_price,tg.wholesale_price,tg.cost,tg.internal_notes," +
				" po.purchase_order_id,convert(datetime,po.create_date) ";
		
		ResultSet resultSet =null;
		List<POSTicketGroup> posTicketGroupList = new ArrayList<POSTicketGroup>();
		POSTicketGroup pOSTicketGroup = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, invoiceId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				pOSTicketGroup = new POSTicketGroup();
				pOSTicketGroup.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				pOSTicketGroup.setEventId(resultSet.getInt("event_id"));
				//pOSTicketGroup.setExchangeEventId(resultSet.getInt("exchange_event_id"));
				pOSTicketGroup.setSection(resultSet.getString("section"));
				pOSTicketGroup.setRow(resultSet.getString("row"));
				pOSTicketGroup.setSeatLow(resultSet.getInt("seat_low"));
				pOSTicketGroup.setSeatHigh(resultSet.getInt("seat_high"));
				pOSTicketGroup.setQty(resultSet.getInt("soldQty"));
				pOSTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
				pOSTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
				pOSTicketGroup.setInternalNotes(null !=resultSet.getString("internal_notes")?resultSet.getString("internal_notes"):"");
				pOSTicketGroup.setCost(resultSet.getDouble("cost"));
				pOSTicketGroup.setActualSoldPrice(resultSet.getDouble("actualSoldPrice"));
				pOSTicketGroup.setJktPoId(resultSet.getInt("purchase_order_id"));
				
				if(resultSet.getTimestamp("purchase_order_date") != null) {
					pOSTicketGroup.setPurchaseOrderDate(new Date(resultSet.getTimestamp("purchase_order_date").getTime()));
				}
				posTicketGroupList.add(pOSTicketGroup);
			}
			return posTicketGroupList;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
	
		}
		return null;
	}
	
public List<POSTicketGroup> getBrokerTicketGroupByInvoiceIdForNY(Integer invoiceId,Connection connection) throws Exception{
		
		String sql = "SELECT count(t.ticket_id) as soldQty,MAX(t.actual_sold_price) as actualSoldPrice,tg.retail_price," +
				" tg.wholesale_price,tg.cost,tg.event_id," +
				" tg.ticket_group_id,tg.section,tg.row,min(convert(int,seat_number)) as seat_low,max(convert(int,seat_number)) as seat_high," +
				" tg.internal_notes,MAX(t.ticket_id) as ticket_id," +
				" po.purchase_order_id,(convert(datetime,po.create_date)) as purchase_order_date,tg.create_date,tg.notes" +
				" FROM ticket t with(nolock) inner join ticket_group tg with(nolock) on" +
				" tg.ticket_group_id=t.ticket_group_id" +
				" inner join event e on e.event_id=tg.event_id" +
				" inner join venue v on v.venue_id=e.venue_id and v.state_id=35" +
				" inner join purchase_order po with(nolock) on po.purchase_order_id=t.purchase_order_id" +
				" where t.invoice_id=? " +
				" group by tg.ticket_group_id,tg.event_id,tg.ticket_group_id, tg.section,tg.row," +
				" tg.ticket_group_id,tg.retail_price,tg.wholesale_price,tg.cost,tg.internal_notes," +
				" po.purchase_order_id,convert(datetime,po.create_date),tg.create_date,tg.notes ";
		
		ResultSet resultSet =null;
		List<POSTicketGroup> posTicketGroupList = new ArrayList<POSTicketGroup>();
		POSTicketGroup pOSTicketGroup = null;
		try {
			PreparedStatement statement = connection.prepareStatement(sql);
			statement.setInt(1, invoiceId);
			resultSet = statement.executeQuery();
			while(resultSet.next()){
				pOSTicketGroup = new POSTicketGroup();
				pOSTicketGroup.setTicketGroupId(resultSet.getInt("ticket_group_id"));
				pOSTicketGroup.setEventId(resultSet.getInt("event_id"));
				//pOSTicketGroup.setExchangeEventId(resultSet.getInt("exchange_event_id"));
				pOSTicketGroup.setSection(resultSet.getString("section"));
				pOSTicketGroup.setRow(resultSet.getString("row"));
				pOSTicketGroup.setSeatLow(resultSet.getInt("seat_low"));
				pOSTicketGroup.setSeatHigh(resultSet.getInt("seat_high"));
				pOSTicketGroup.setQty(resultSet.getInt("soldQty"));
				pOSTicketGroup.setRetailPrice(resultSet.getDouble("retail_price"));
				pOSTicketGroup.setWholesalePrice(resultSet.getDouble("wholesale_price"));
				pOSTicketGroup.setInternalNotes(null !=resultSet.getString("internal_notes")?resultSet.getString("internal_notes"):"");
				pOSTicketGroup.setCost(resultSet.getDouble("cost"));
				pOSTicketGroup.setActualSoldPrice(resultSet.getDouble("actualSoldPrice"));
				pOSTicketGroup.setJktPoId(resultSet.getInt("purchase_order_id"));
				pOSTicketGroup.setCreatedDate(resultSet.getTimestamp("create_date"));
				pOSTicketGroup.setExternalNotes(resultSet.getString("notes"));
				
				if(resultSet.getTimestamp("purchase_order_date") != null) {
					pOSTicketGroup.setPurchaseOrderDate(new Date(resultSet.getTimestamp("purchase_order_date").getTime()));
				}
				posTicketGroupList.add(pOSTicketGroup);
			}
			return posTicketGroupList;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
	
		}
		return null;
	}

public ClientBroker getInvoiceClientInfo(Integer invoiceId, Connection connection) throws Exception{
	
	String sql = "select * from (select client_id as ClientId,CONCAT('CLIENT-',ct.client_type_desc) as ClientType, " +
			"CASE WHEN c.client_fname is null THEN '' ELSE  c.client_fname END as  ClientFirstName, " +
			"CASE WHEN c.client_lname is null THEN '' ELSE  c.client_lname END as  ClientLastName,c.email as Email," +
			"CASE WHEN c.company is null THEN '' ELSE  c.company END as  CompanyName, a.street_address1 as StreetAddress1," +
			"CASE WHEN a.street_address2 is null THEN '' ELSE  a.street_address2 END as   StreatAddress2,a.city as City," +
			"CASE WHEN ss.state_name is null THEN '' ELSE  ss.state_name END as   State, " +
			"CASE WHEN sc.system_country_name is null THEN '' ELSE  sc.system_country_name END as CountryName,p.phone_number " +
			"from client c with(nolock) inner join client_type ct with(nolock) on ct.client_type_id=c.client_type_id " +
			"left join phone p with(nolock) on p.phone_id=c.main_phone_id " +
			"left join address a with(nolock) on a.address_id=c.main_address_id " +
			"left join system_state ss with(nolock) on ss.state_id=a.state_id " +
			"left join system_country sc with(nolock) on sc.system_country_id=a.system_country_id  where " +
			"c.client_id in (select distinct client_id from client_invoice where invoice_id =? ) " +
			" UNION ALL " +
			" select cb.client_broker_id as ClientId ,CONCAT('CLIENT BROKER-',cbt.client_broker_type_desc) as ClientType ," +
			"CASE WHEN cb.client_broker_fname is null THEN ''   ELSE  cb.client_broker_fname END as  ClientFirstName," +
			"CASE WHEN cb.client_broker_lname is null THEN ''  ELSE  cb.client_broker_lname END as  ClientLastName," +
			"cb.client_broker_email as Email,CASE WHEN cb.client_broker_company_name is null THEN '' ELSE  cb.client_broker_company_name END as  CompanyName," +
			"a.street_address1 as StreetAddress1,CASE WHEN a.street_address2 is null THEN '' ELSE  a.street_address2 END as  StreatAddress2," +
			"a.city as City,CASE WHEN ss.state_name is null THEN '' ELSE  ss.state_name END as   State," +
			"CASE WHEN sc.system_country_name is null THEN '' ELSE  sc.system_country_name END as CountryName,p.phone_number " +
			"from client_broker cb with(nolock) inner join client_broker_type cbt with(nolock) on cbt.client_broker_type_id=cb.client_broker_type_id " +
			"left join phone p with(nolock) on p.phone_id=cb.main_phone_id " +
			"left join address a with(nolock) on a.address_id=cb.main_address_id " +
			"left join system_state ss with(nolock) on ss.state_id=a.state_id " +
			"left join system_country sc with(nolock) on sc.system_country_id=a.system_country_id " +
			"where client_broker_id in (select distinct client_broker_id from client_broker_invoice where invoice_id =?)) " +
			"clients order by  ClientFirstName ";
	
	
	
	ResultSet resultSet =null;
	ClientBroker clientBroker = new ClientBroker();
	try {
		PreparedStatement statement = connection.prepareStatement(sql);
		statement.setInt(1, invoiceId);
		statement.setInt(2, invoiceId);
		resultSet = statement.executeQuery();
		while(resultSet.next()){
			clientBroker.setClientType(resultSet.getString("ClientType"));
			clientBroker.setClientId(resultSet.getInt("ClientId"));
			clientBroker.setClientCompanyName(resultSet.getString("CompanyName"));
			clientBroker.setClientFirstName(resultSet.getString("ClientFirstName"));
			clientBroker.setClientLastName(resultSet.getString("ClientLastName"));
			clientBroker.setClientMail(resultSet.getString("Email"));
			clientBroker.setStreetAddress1(resultSet.getString("StreetAddress1"));
			clientBroker.setStreetAddress2(resultSet.getString("StreatAddress2"));
			clientBroker.setCity(resultSet.getString("City"));
			clientBroker.setState(resultSet.getString("State"));
			clientBroker.setCountry(resultSet.getString("CountryName"));
			clientBroker.setPhoneNumber(resultSet.getString("phone_number"));
		}
		return clientBroker;
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{

	}
	return null;
}


	public List<Object[]> getEventCrawlSalesListOld(Connection connection , String procName) throws Exception {
	 
	 CallableStatement cs = null;
	 try{
	     //cs = connection.prepareCall("{call event_crawl_list3}");
	     cs = connection.prepareCall("{call "+procName+"}");
	    
	     ResultSet resultSet = cs.executeQuery();
	     
	     List<Object[]> list = new ArrayList<Object[]>();
	     Object[] objects = null;
	     while(resultSet.next()){
	    	 objects = new Object[25];
	    	 objects[0] = resultSet.getInt("Event_Id");
	    	 objects[1] = resultSet.getString("EventName");
	    	 objects[2] = resultSet.getString("Event_Creation_Date");
	    	 objects[3] = resultSet.getString("EventDate");
	    	 objects[4] = resultSet.getString("EventTime");
	    	 objects[5] = resultSet.getString("Parent");
	    	 objects[6] = resultSet.getString("Child");
	    	 objects[7] = resultSet.getString("Grand_child");
	    	 objects[8] = resultSet.getString("VenueName");
	    	 objects[9] = resultSet.getString("city");
	    	 objects[10] = resultSet.getString("state");
	    	 objects[11] = resultSet.getString("country");
	    	 objects[12] = resultSet.getString("Event_creation_Time");
	    	 objects[13] = resultSet.getString("ticketnetworkdirect");
	    	 objects[14] = resultSet.getString("stubhub");
	    	 objects[15] = resultSet.getString("ticketevolution");
	    	 objects[16] = resultSet.getString("vividseat");
	    	 objects[17] = resultSet.getString("Sales_hit");
	    	 objects[18] = resultSet.getString("csv");
	    	 objects[19] = resultSet.getInt("y2015_sales_count");
	    	 objects[20] = resultSet.getInt("y2016_sales_count");
	    	 objects[21] = resultSet.getInt("venue_id");
	    	 objects[22] = resultSet.getString("category_group");
	    	 objects[23] = resultSet.getString("venue_map");
	    	 objects[24] = resultSet.getInt("last2daysales");
	    	 list.add(objects);
	     }
	    return list;
	 }catch(Exception e){
		 throw e;
		
	 }
}
	public List<Object[]> getEventCrawlSalesList(Boolean isFirstSheet) throws Exception {
		 
		Session session = null;
		 try{
			 
			 String sql = "select EventId,EventName,EventDate,EventTime,EventCreationDate,EventCreationTime,Parent,Child,Grand_Child,VenueName,city,state,country," +
			 		" ticketnetworkdirect,stubhub,ticketevolution,vividseat,Sales_hit,csv,last3MonthsSales,venue_id,Venue_Category_Name,venue_map,last2DaysSales" +
			 		"  from report_event_crawls_sales_vw ";
			 if(isFirstSheet) {
				 sql = sql + " where csv = 'Yes' and ticketnetworkdirect  = 'Yes' and stubhub = 'Yes' and ticketevolution = 'Yes'" +
				 		" and vividseat  = 'Yes' and venue_map = 'Yes'";	 
			 } else {
				 sql = sql + " where csv = 'No' or ticketnetworkdirect  = 'No' or stubhub = 'No' or ticketevolution = 'No'" +
			 		" or vividseat  = 'No' or venue_map = 'No'";
			 }
			 sql = sql + " order by last2DaysSales desc,last3MonthsSales desc,event_creation_Date desc ";
			 
		
			 
			 ResultSet resultSet =null;
			 session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object[]> list = sqlQuery.list();
				
		    return list;
		 }catch(Exception e){
			 throw e;
			
		 }
	}
	
	public List<Object[]> getRTWUnSoldLongInventory(Integer brokerId) throws Exception {
		 
		Session session = null;
		 try{
			 
			 String posUrl = null;
			 
			 if(brokerId.equals(5)) {//RTW
				 posUrl = rtwPos;
			 } else if(brokerId.equals(10)) {//RTW-2
				 posUrl = rtwTwoPos;
			 }
			 
			 String sql = "select  tg.event_id ,e.event_name, CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate, " +
			 		" CASE WHEN e.event_datetime is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7))" +
			 		" WHEN e.event_datetime is null THEN 'TBD' END as EventTime,v.name as venue_name, v.city as venue_city," +
			 		" tg.section ,tg.row, cast(MIN(CAST(seat_number AS int)) as varchar)+'-'+cast(MAX(CAST(seat_number AS int)) as varchar) as seat," +
			 		" tg.original_ticket_count, count(t.ticket_id) as remaining_ticket_count ," +
			 		" tg.retail_price , tg.face_price,tg.cost, tg.wholesale_price,tg.internal_notes, iif(t.ticket_on_hand_status_id = 1, 'Yes', 'No')  as onHand," +
			 		" iif(tg.eticket_instant_download = 1, 'Instant Download', 'No') as InstantDownload," +
			 		" tgst.ticket_group_stock_type_desc as StockType," +
			 		" iif(t.purchase_order_id is NULL, ' ', t.purchase_order_id) as  purchase_order_id," +
			 		" cb.client_broker_company_name , (cbe.employee_fname + cbe.employee_lname ) as client_broker_employee_name," +
			 		" convert(datetime,po.create_date) as purchase_order_date,e.event_datetime" +
			 		" from "+posUrl+".ticket_group tg with(nolock)" +
			 		" inner join "+posUrl+".event e  with(nolock) on tg.event_id = e.event_id" +
			 		" inner join "+posUrl+".venue v with(nolock) on v.venue_id = e.venue_id" +
			 		" inner join "+posUrl+".client_broker cb with(nolock) on tg.client_broker_id = cb.client_broker_id" +
			 		" inner join "+posUrl+".client_broker_employee  cbe with(nolock) on tg.client_broker_employee_id  = cbe.client_broker_employee_id" +
			 		" inner join "+posUrl+".ticket_group_stock_type tgst with(nolock) on  tg.ticket_group_stock_type_id = tgst.ticket_group_stock_type_id" +
			 		" inner join "+posUrl+".ticket t on t.ticket_group_id=tg.ticket_group_id" +
			 		" inner join "+posUrl+".purchase_order po on po.purchase_order_id=t.purchase_order_id" +
			 		" where  tg.office_id  = 1 and tg.cost > 0 and t.purchase_order_id is not null and" +
			 		" e.event_datetime >= GETDATE() and t.invoice_id is null" +
			 		" group by tg.event_id,e.event_name,e.event_datetime,v.name,v.city,tg.section,tg.row,tg.original_ticket_count," +
			 		" tg.retail_price,tg.face_price,tg.cost,tg.wholesale_price,tg.internal_notes,t.ticket_on_hand_status_id,tg.eticket_instant_download," +
			 		" tgst.ticket_group_stock_type_desc,t.purchase_order_id,cb.client_broker_company_name,cbe.employee_fname,cbe.employee_lname,po.create_date," +
			 		" tg.ticket_group_id" +
			 		" order by event_datetime,event_name,event_id,section,row,remaining_ticket_count";
		
			 
			 ResultSet resultSet =null;
			 session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object[]> list = sqlQuery.list();
				
		    /* List<Object[]> list = new ArrayList<Object[]>();
		     Object[] objects = null;
		     while(resultSet.next()){
		    	 objects = new Object[23];
		    	 objects[0] = resultSet.getInt("event_id");
		    	 objects[1] = resultSet.getString("event_name");
		    	 objects[2] = resultSet.getString("EventDate");
		    	 objects[3] = resultSet.getString("EventTime");
		    	 objects[4] = resultSet.getString("venue_name");
		    	 objects[5] = resultSet.getString("venue_city");
		    	 objects[6] = resultSet.getString("section");
		    	 objects[7] = resultSet.getString("row");
		    	 objects[8] = resultSet.getString("low")+"-"+resultSet.getString("high");
		    	 objects[9] = resultSet.getString("original_ticket_count");
		    	 objects[10] = resultSet.getString("remaining_ticket_count");
		    	 objects[11] = resultSet.getString("retail_price");
		    	 objects[12] = resultSet.getString("face_price");
		    	 objects[13] = resultSet.getString("cost");
		    	 objects[14] = resultSet.getString("wholesale_price");
		    	 objects[15] = resultSet.getString("internal_notes");
		    	 objects[16] = resultSet.getString("onHand");
		    	 objects[17] = resultSet.getString("InstantDownload");
		    	 objects[18] = resultSet.getString("StockType");
		    	 objects[19] = resultSet.getString("purchase_order_id");
		    	 objects[20] = resultSet.getString("client_broker_company_name");
		    	 objects[21] = resultSet.getString("client_broker_employee_name");
		    	 objects[22] = resultSet.getString("broker");
		    	 list.add(objects);
		     }*/
		    return list;
		 }catch(Exception e){
			 throw e;
			
		 }
	}
	
	public List<Object[]> getAllRTWUnSoldLongInventoryAvgCost() throws Exception {
		 
		Session session = null;
		 try{
			 
			 String sql = "select process_date,average_cost from rtw_unsold_inventory_average_cost order by process_date";
			 ResultSet resultSet =null;
			 session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object[]> list = sqlQuery.list();
				
		    return list;
		 }catch(Exception e){
			 throw e;
			
		 }
	}
	
	public List<Object[]> getRTWLongInventory(Connection connection) throws Exception {
		 
		 try{
			/* String sql = "select  tg.event_id ,e.event_name, CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate," +
			 		"CASE WHEN e.event_datetime is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7)) " +
			 		"WHEN e.event_datetime is null THEN 'TBD' END as EventTime,v.name as venue_name, v.city as venue_city, " +
			 		"tg.original_ticket_count, tg.remaining_ticket_count , iif(t.ticket_on_hand_status_id = 1, 'Yes', 'No')  as onHand," +
			 		"iif(tg.eticket_instant_download = 1, 'Instant Download', 'No') as InstantDownload," +
			 		"tgst.ticket_group_stock_type_desc as StockType,tg.retail_price , tg.face_price, " +
			 		"tg.cost, tg.wholesale_price, tg.row, tg.section ,t1.low, t1.high," +
			 		"iif(t.purchase_order_id is NULL, ' ', t.purchase_order_id) as  purchase_order_id," +
			 		"cb.client_broker_company_name , (cbe.employee_fname + cbe.employee_lname ) as client_broker_employee_name, " +
			 		"tg.internal_notes  from ticket_group tg inner join ticket t  on tg.ticket_group_id = t.ticket_group_id " +
			 		"inner join event e  on tg.event_id = e.event_id inner join venue v on v.venue_id = e.venue_id " +
			 		"inner join client_broker cb on tg.client_broker_id = cb.client_broker_id " +
			 		"inner join client_broker_employee  cbe on tg.client_broker_employee_id  = cbe.client_broker_employee_id " +
			 		"inner join ticket_group_stock_type tgst on  tg.ticket_group_stock_type_id = tgst.ticket_group_stock_type_id " +
			 		"cross join (select  ticket_group_id, min(seat_number) as low, max(seat_number) as high  from ticket " +
			 		"group by ticket_group_id)  t1   where  t.ticket_group_id = t1.ticket_group_id " +
			 		"and t.invoice_id is NULL  and  tg.office_id  = 1 and tg.cost > 0 and " +
			 		"tg.ticket_group_id not in (select distinct ticket_group_id from ticket where invoice_id is not null)";*/
			 
			 String sql = "select  tg.event_id ,e.event_name, CONVERT(VARCHAR(19),e.event_datetime,101) as EventDate," +
			 		"CASE WHEN e.event_datetime is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_datetime, 100), 7)) " +
			 		"WHEN e.event_datetime is null THEN 'TBD' END as EventTime,v.name as venue_name, v.city as venue_city, " +
			 		"tg.original_ticket_count, t1.remaining as remaining_ticket_count , " +
			 		"iif(t.ticket_on_hand_status_id = 1, 'Yes', 'No')  as onHand," +
			 		"iif(tg.eticket_instant_download = 1, 'Instant Download', 'No') as InstantDownload," +
			 		"tgst.ticket_group_stock_type_desc as StockType,tg.retail_price , tg.face_price, " +
			 		"tg.cost, tg.wholesale_price, tg.row, tg.section ,t.low, t.high," +
			 		"iif(t.purchase_order_id is NULL, ' ', t.purchase_order_id) as  purchase_order_id," +
			 		"cb.client_broker_company_name , (cbe.employee_fname + cbe.employee_lname ) as client_broker_employee_name," +
			 		"tg.internal_notes  from ticket_group tg inner join event e  on tg.event_id = e.event_id " +
			 		"inner join venue v on v.venue_id = e.venue_id inner join client_broker cb on tg.client_broker_id = cb.client_broker_id " +
			 		"inner join client_broker_employee  cbe on tg.client_broker_employee_id  = cbe.client_broker_employee_id " +
			 		"inner join ticket_group_stock_type tgst on  tg.ticket_group_stock_type_id = tgst.ticket_group_stock_type_id " +
			 		"cross join (select  ticket_group_id, min(seat_number) as low, max(seat_number) as high , ticket_on_hand_status_id, " +
			 		"purchase_order_id from ticket group by ticket_group_id,ticket_on_hand_status_id,purchase_order_id)  t " +
			 		"cross join (select  ticket_group_id as id , count(ticket_group_id) as remaining from ticket where invoice_id is null " +
			 		"group by ticket_group_id) t1 where  tg.ticket_group_id = t.ticket_group_id  and tg.ticket_group_id=t1.id    " +
			 		" and  tg.office_id  = 1 and tg.cost > 0 and t.purchase_order_id is not null and  " +
			 		"e.event_datetime >= GETDATE() and tg.ticket_group_id in (select distinct ticket_group_id from " +
			 		"ticket where invoice_id is null)";
		
			 
			 ResultSet resultSet =null;
			 PreparedStatement statement = connection.prepareStatement(sql);
			 resultSet = statement.executeQuery();
		     
		     List<Object[]> list = new ArrayList<Object[]>();
		     Object[] objects = null;
		     while(resultSet.next()){
		    	 objects = new Object[22];
		    	 objects[0] = resultSet.getInt("event_id");
		    	 objects[1] = resultSet.getString("event_name");
		    	 objects[2] = resultSet.getString("EventDate");
		    	 objects[3] = resultSet.getString("EventTime");
		    	 objects[4] = resultSet.getString("venue_name");
		    	 objects[5] = resultSet.getString("venue_city");
		    	 objects[6] = resultSet.getString("section");
		    	 objects[7] = resultSet.getString("row");
		    	 objects[8] = resultSet.getString("low")+"-"+resultSet.getString("high");
		    	 objects[9] = resultSet.getString("original_ticket_count");
		    	 objects[10] = resultSet.getString("remaining_ticket_count");
		    	 objects[11] = resultSet.getString("retail_price");
		    	 objects[12] = resultSet.getString("face_price");
		    	 objects[13] = resultSet.getString("cost");
		    	 objects[14] = resultSet.getString("wholesale_price");
		    	 objects[15] = resultSet.getString("internal_notes");
		    	 objects[16] = resultSet.getString("onHand");
		    	 objects[17] = resultSet.getString("InstantDownload");
		    	 objects[18] = resultSet.getString("StockType");
		    	 objects[19] = resultSet.getString("purchase_order_id");
		    	 objects[20] = resultSet.getString("client_broker_company_name");
		    	 objects[21] = resultSet.getString("client_broker_employee_name");
		    	 list.add(objects);
		     }
		    return list;
		 }catch(Exception e){
			 throw e;
			
		 }
	}
	
	public String getAutocats96CategoryTicketZoneByTnCategoryTicketGroupId(Integer eventId,Integer tnCategoryTicketGroupId,Integer brokerId) throws Exception {
		String sql = "select tmat_zone from autocats96_category_ticket where event_id="+eventId+" and tn_category_ticket_group_id="+tnCategoryTicketGroupId+" and broker_id="+brokerId;
		Session session=null;
		String zone= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object> objList = sqlQuery.list();
			if(objList != null && !objList.isEmpty()) {
				zone = (String)(objList.get(0));
				//System.out.println("..."+objList.get(0)[0]);
				//eventId = (Integer)(resultRow[0]);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return zone;
	}
	
	public String getLarryLastCategoryTicketZoneByTnTicketGroupId(Integer eventId,Integer tnTicketGroupId,Integer brokerId) throws Exception {
		String sql = "select tmat_zone from larrylast_category_ticket where event_id="+eventId+" and tn_ticket_group_id="+tnTicketGroupId+" and tn_broker_id="+brokerId;
		Session session=null;
		String zone= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object> objList = sqlQuery.list();
			if(objList != null && !objList.isEmpty()) {
				zone = (String)(objList.get(0));
				//System.out.println("..."+objList.get(0)[0]);
				//eventId = (Integer)(resultRow[0]);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return zone;
	}
	public String getZoneLastRowCategoryTicketZoneByTnTicketGroupId(Integer eventId,Integer tnTicketGroupId,Integer brokerId) throws Exception {
		String sql = "select tmat_zone from zone_lastrow_minicat_category_ticket where event_id="+eventId+" and tn_ticket_group_id="+tnTicketGroupId+" and tn_broker_id="+brokerId;
		Session session=null;
		String zone= null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			List<Object> objList = sqlQuery.list();
			if(objList != null && !objList.isEmpty()) {
				zone = (String)(objList.get(0));
				//System.out.println("..."+objList.get(0)[0]);
				//eventId = (Integer)(resultRow[0]);
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return zone;
	}
	public List<EventSectionZoneDetails> getAllAutopricingProductsSectionAndTmatZone() throws Exception {
		String sql = " select distinct event_id as eventId,section as section,tmat_zone as zone from autocats96_category_ticket tg where status='ACTIVE'" +
				" union all" +
				" select distinct event_id as eventId,section as section,tmat_zone as zone from larrylast_category_ticket tg where status='ACTIVE'" +
				" union all" +
				" select distinct event_id as eventId,section as section,tmat_zone as zone from zone_lastrow_minicat_category_ticket tg where status='ACTIVE'" +
				" union all" +
				" select distinct event_id as eventId,section as section,tmat_zone as zone from zone_tickets_category_ticket_group tg where status='ACTIVE'";
		Session session=null;
		List<EventSectionZoneDetails> eventSectionZoneList = new ArrayList<EventSectionZoneDetails>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("section",Hibernate.STRING);
			sqlQuery.addScalar("zone",Hibernate.STRING);
			eventSectionZoneList =  sqlQuery.setResultTransformer(Transformers.aliasToBean(EventSectionZoneDetails.class)).list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return eventSectionZoneList;
	}
	public List<Object[]> getTmatandPosVenueMismatchingEvents() throws Exception {
		String sql = "select e.admitone_id as AdmitOneId,e.id as TMAT_EventId,e.name as TMAT_EventName," +
				" CONVERT(VARCHAR(19),e.event_date,101) as TMAT_EventDate," +
				" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))" +
				" WHEN e.event_time is null THEN 'TBD' END as TMAT_EventTime," +
				" v.building as TMAT_Venue,v.city as TMAT_City,v.state as TMAT_State,v.country as TMAT_Country,v.tn_venue_id as TN_VenueId," +
				" ae.venueName as POS_Venue,ae.city as POS_City,ae.state_short_desc as POS_State,ae.country as POS_Country,ae.venue_id as POS_VenueId" +
				" from event e" +
				" inner join venue v on v.id=e.venue_id" +
				" inner join admitone_event ae on ae.eventid=e.admitone_id" +
				" where e.event_status='ACTIVE' " +//and v.building!=ae.venueName
				" and v.country in ('USA','US','CA')" +
				" and (v.tn_venue_id!=ae.venue_id or v.building!=ae.venueName or v.city!=ae.city or v.state!=ae.state_short_desc or v.country!=ae.country)" +
				" order by ae.VenueName,ae.city,ae.state_short_desc,ae.country,e.name,e.event_date,e.event_time";
		
		Session session=null;
		List<Object[]> objList = null;
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			objList = sqlQuery.list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return objList;
	}
	public List<String> getAllZoneByVenueCategory(Integer venueCatId) throws Exception{
			
		String sql = "select distinct c.symbol from category_mapping_new cm inner join category_new c on cm.category_id=c.id " +
				"where c.venue_category_id="+venueCatId;
		Map<String, Boolean> map = new HashMap<String, Boolean>();
		Session session=null;
		List<String> objList = null;
		try {
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			objList = sqlQuery.list();
			
			/*while(objList != null){
				for (String zone : objList) {
					map.put(zone.toUpperCase(), true);
				}
			}
			return map;*/
		} catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return objList;
	}
public List<TNDEventSalesReportDetail> getTNDEventSalesDetailReport(String fromDate,String toDate) throws Exception {
		

		/*String sql = "select  e.id as eventId,e.name as eventName,CONVERT(VARCHAR(19),e.event_date,101) as eventDateStr," +
				" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null THEN 'TBD' END as eventTimeStr," +
				" v.building as venue,v.city as city,v.state as state,v.country as country,tnd.salesCount as salesCount," +
				" CONVERT(VARCHAR(19),tnd.latest_order_date,101)+' '+LTRIM(RIGHT(CONVERT(VARCHAR(20), tnd.latest_order_date, 100), 7)) as recentSaleDateStr" +
				" from event e" +
				" inner join venue v on v.id=e.venue_id" +
				" inner join (select count(*) as salesCount,event_id,max(order_date) as latest_order_date from tnd_sales_autopricing where" +
				//" order_date>convert(date,(DATEADD(dd, -2, getdate())))" +
				" order_date>='"+fromDate+"' and order_date<='"+toDate+"'"+
				" and accept_sub_status_desc='Accepted'" +
				" group by event_id) as tnd on tnd.event_id=e.admitone_id" +
				" where e.event_date>getdate()" +
				" order by tnd.latest_order_date desc";*/
	
	/*String sql = "SELECT tnd.event_id AS eventId, tnd.event_name AS eventName, CONVERT(VARCHAR(19),tnd.event_date,101) AS eventDateStr," +
			" CASE WHEN tnd.event_date IS NOT NULL THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), tnd.event_date, 100), 7)) WHEN tnd.event_date IS NULL THEN 'TBD' END AS eventTimeStr," +
			" tnd.venue_name AS venue, tnd.city_name AS city, tnd.state_province AS state," +
			" count(distinct tnd.id) AS salesCount," +
			//" --ISNULL((CASE WHEN e.id IS NOT NULL THEN 'Y' ELSE 'N' END), 'N') AS TmatEvent," +
			//" --CASE WHEN  e.venue_category_id IS NOT NULL THEN 'Y' ELSE 'N' END AS CSV," +
			" MAX(tnd.order_date) AS recentSaleDate" +
			" FROM tnd_sales_autopricing tnd with(nolock)" +
			//" --left join event e with(nolock) on e.admitone_id=tnd.event_id and e.event_status='ACTIVE'" +
			" WHERE tnd.order_date>='2017-09-01' and tnd.order_date<='2017-10-01'" +
			" and tnd.event_date>getdate() and accept_sub_status_desc='Accepted'" +
			" GROUP BY tnd.event_id, tnd.event_name, tnd.event_date, tnd.venue_name, tnd.city_name, tnd.state_province" +
			//" --,e.id,e.venue_category_id" +
			" order by recentSaleDate desc";*/
	
	/*String sql = "SELECT tnd.event_id AS eventId, tnd.event_name AS eventName, tnd.event_date as eventDate," +
			" tnd.venue_name AS venue, tnd.city_name AS city, tnd.state_province AS state," +
			" count(1) AS salesCount," +
			" ISNULL((CASE WHEN tac.EventId IS NOT NULL THEN 'Y' ELSE 'N' END), 'N') AS tmatEvent," +
			" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('ticketnetworkdirect') AND event_id=tac.EventId)='ticketnetworkdirect' THEN 'Y' ELSE 'N' END, 'N') AS ticketNetworkDirectCrawl," +
			" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('stubhub') AND event_id=tac.EventId)='stubhub' THEN 'Y' ELSE 'N' END, 'N') AS stubhubCrawl," +
			" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('ticketevolution') AND event_id=tac.EventId)='ticketevolution' THEN 'Y' ELSE 'N' END, 'N') AS ticketEvolutionCrawl," +
			" ISNULL(CASE WHEN (SELECT DISTINCT TOP 1 cn.id FROM category_mapping_new cmn INNER JOIN category_new cn ON cn.id=cmn.category_id WHERE cn.venue_category_id=tac.VenueCategoryId)=tac.EventId THEN 'Y' ELSE 'N' END, 'N') AS csvUploaded," +
			" MAX(tnd.order_date) AS recentSaleDate" +
			" FROM tnd_sales_autopricing tnd" +
			" left join (SELECT DISTINCT e.id AS EventId, e.admitone_id AS AdmitoneId,e.venue_category_id as VenueCategoryId" +
			" FROM event e" +
			" WHERE e.event_status='ACTIVE'" +
			" ) tac on tac.AdmitoneId=tnd.event_id" +
			" WHERE tnd.order_date>='"+fromDate+"' and tnd.order_date<='"+toDate+"'" +
			" GROUP BY tnd.event_id, tnd.event_name, tnd.event_date, tnd.venue_name, tnd.city_name, tnd.state_province, tac.EventId,tac.VenueCategoryId" +
			" order by recentSaleDate desc";*/
	
	String sql = "SELECT tnd.event_id AS eventId, e.name AS eventName, e.event_date as eventDate, e.event_time as eventTime," +
			" v.building AS venue, v.city AS city, v.state AS state," +
			" count(1) AS salesCount," +
			" ISNULL((CASE WHEN e.id IS NOT NULL THEN 'Y' ELSE 'N' END), 'N') AS tmatEvent," +
			" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('ticketnetworkdirect') AND event_id=e.id)='ticketnetworkdirect' THEN 'Y' ELSE 'N' END, 'N') AS ticketNetworkDirectCrawl," +
			" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('stubhub') AND event_id=e.id)='stubhub' THEN 'Y' ELSE 'N' END, 'N') AS stubhubCrawl," +
			" ISNULL(CASE WHEN (SELECT TOP 1 site_id FROM ticket_listing_crawl WHERE enabled=1 AND site_id IN ('ticketevolution') AND event_id=e.id)='ticketevolution' THEN 'Y' ELSE 'N' END, 'N') AS ticketEvolutionCrawl," +
			" ISNULL(CASE WHEN (SELECT TOP 1 cn.id FROM category_mapping_new cmn INNER JOIN category_new cn ON cn.id=cmn.category_id WHERE cn.venue_category_id=e.venue_category_id) is not null THEN 'Y' ELSE 'N' END, 'N') AS csvUploaded," +
			" MAX(tnd.order_date) AS recentSaleDate,max(df.last_update_date) as tnEventCreatedDate" +
			" FROM tnd_sales_autopricing tnd with(nolock)" +
			" inner join event e with(nolock) on e.admitone_id=tnd.event_id " +//--and e.event_status='ACTIVE'
			" inner join venue v with(nolock) on v.id=e.venue_id" +
			" inner join tn_data_feed_event df on df.exchange_event_id=tnd.event_id" +
			" WHERE tnd.order_date>='"+fromDate+"' and tnd.order_date<='"+toDate+"'" +
			" GROUP BY tnd.event_id,e.id,e.name,e.event_Date,e.event_time,v.building,v.city,v.state,e.venue_category_id" +
			" order by recentSaleDate desc";
	
		
		Session session=null;
		List<TNDEventSalesReportDetail> list = new ArrayList<TNDEventSalesReportDetail>();
		try{
			session=sessionFactory.openSession();
			SQLQuery sqlQuery=session.createSQLQuery(sql);
			sqlQuery.addScalar("eventId",Hibernate.INTEGER);
			sqlQuery.addScalar("eventName",Hibernate.STRING);
			sqlQuery.addScalar("eventDate",Hibernate.TIMESTAMP);
			sqlQuery.addScalar("eventTime",Hibernate.TIMESTAMP);
			//sqlQuery.addScalar("eventDateStr",Hibernate.STRING);
			//sqlQuery.addScalar("eventTimeStr",Hibernate.STRING);
			sqlQuery.addScalar("venue",Hibernate.STRING);
			sqlQuery.addScalar("city",Hibernate.STRING);
			sqlQuery.addScalar("state",Hibernate.STRING);
			//sqlQuery.addScalar("country",Hibernate.STRING);
			sqlQuery.addScalar("salesCount",Hibernate.INTEGER);
			//sqlQuery.addScalar("recentSaleDateStr",Hibernate.STRING);
			sqlQuery.addScalar("recentSaleDate",Hibernate.TIMESTAMP);
			
			sqlQuery.addScalar("tmatEvent",Hibernate.STRING);
			sqlQuery.addScalar("ticketNetworkDirectCrawl",Hibernate.STRING);
			sqlQuery.addScalar("ticketEvolutionCrawl",Hibernate.STRING);
			sqlQuery.addScalar("stubhubCrawl",Hibernate.STRING);
			sqlQuery.addScalar("csvUploaded",Hibernate.STRING);
			sqlQuery.addScalar("tnEventCreatedDate",Hibernate.TIMESTAMP);
			
			list = sqlQuery.setResultTransformer(Transformers.aliasToBean(TNDEventSalesReportDetail.class)).list();
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return list;
	}

public List<TNDRtfNoSaleReport> getTNDRtfNoSaleeport() throws Exception {
	

String sql = "select gg.TNEventId as eventId,gg.eventName as eventName,gg.eventDate as eventDate,gg.venue as venue,gg.city as city,gg.state as state," +
		" gg.TMATEvent as tmatEvent,gg.csv as csvUploaded,gg.map as mapUploaded,gg.tnSales as tnSalesCount,gg.tixCitySales as tixCitySalesCount," +
		" gg.RTFTixCount as rtfTicketsCount,gg.tixcityTixCount as tixcityTicketsCount,gg.TicketNetwork as ticketNetworkDirectCrawl,gg.TicketEvolution as ticketEvolutionCrawl," +
		" gg.StubHub as stubhubCrawl,gg.Vividseats as vividSeatsCrawl,gg.Flashseats as flashSeatsCrawl,gg.Autocats96Enabled as autocats96Enabled," +
		" gg.LarryLastEnabled as larryLastEnabled,gg.ZoneLastRowEnabled as zoneLastRowEnabled,gg.RtfListingEnabled as rtfListingEnabled from (" +
		" select g.TNEventId,g.eventName,g.eventDate,g.venue,g.city,g.state," +
		" case when e.id is not null then 'Y' else 'N' end as TMATEvent," +
		" case when vc.id is not null then 'Y' else 'N' end as CSV," +
		" case when vc.venue_map =1 then 'Y' else 'N' end as map,g.tnSales,g.tixCitySales," +
		" case when (select cr.id from ticket_listing_crawl cr with(nolock) where enabled=1 and site_id='ticketnetworkdirect' and cr.event_id=e.id) is not null then 'Y' else 'N' end as 'TicketNetwork'," +
		" case when (select cr.id from ticket_listing_crawl cr with(nolock) where enabled=1 and site_id='ticketevolution' and cr.event_id=e.id) is not null then 'Y' else 'N' end as 'TicketEvolution'," +
		" case when (select cr.id from ticket_listing_crawl cr with(nolock) where enabled=1 and site_id='stubhub' and cr.event_id=e.id) is not null then 'Y' else 'N' end as 'StubHub'," +
		" case when (select cr.id from ticket_listing_crawl cr with(nolock) where enabled=1 and site_id='vividseat' and cr.event_id=e.id) is not null then 'Y' else 'N' end as 'Vividseats'," +
		" case when (select cr.id from ticket_listing_crawl cr with(nolock) where enabled=1 and site_id='flashseats' and cr.event_id=e.id) is not null then 'Y' else 'N' end as 'Flashseats'" +
		" ,isnull((select count(*) from zone_tickets_category_ticket_group with(nolock) where status='ACTIVE' and event_id=e.id and  zone_tickets_ticket_group_id is not null ),0) as RTFTixCount," +
		" (select sum(isnull(tg.cnt,0)) from (" +
		" select count(*) as cnt from autocats96_category_ticket with(nolock) where status='ACTIVE' and event_id=e.id and  tn_category_ticket_group_id is not null" +
		" union select count(*) as cnt from larrylast_category_ticket with(nolock)  where status='ACTIVE' and event_id=e.id and  tn_ticket_group_id is not null" +
		" union select count(*) as cnt from zone_lastrow_minicat_category_ticket with(nolock)  where status='ACTIVE' and event_id=e.id and  tn_ticket_group_id is not null" +
		" ) as tg) as tixcityTixCount ," +
		" case when (select top 1 event_id from autocats96_exchange_Event with(nolock) where status='ACTIVE' and event_id=e.id  ) is not null then 'Y'  else 'N' end as Autocats96Enabled," +
		" case when (select top 1 event_id from larrylast_exchange_Event with(nolock)  where status='ACTIVE' and event_id=e.id ) is not null then 'Y'  else 'N' end as LarryLastEnabled," +
		" case when (select top 1 event_id from zone_lastrow_mini_exchange_event with(nolock)  where status='ACTIVE' and event_id=e.id ) is not null then 'Y'  else 'N' end as ZoneLastRowEnabled," +
		" case when (select top 1 event_id from zone_tickets_processor_exchange_event with(nolock)  where status='ACTIVE' and event_id=e.id ) is not null then 'Y'  else 'N' end as RtfListingEnabled" +
		" from (" +
		" select td.event_id as TNEventId,td.event_name as eventName,td.event_date as eventDate,td.venue_name as venue," +
		" td.city_name as city,td.state_province as state," +
		" (tt.totalTNSales-ISNULL(tx.tixCitySales,0)) as tnSales,ISNULL(tx.tixCitySales,0) as tixCitySales" +
		" from tnd_sales_autopricing td with(nolock)" +
		" inner join (" +
		" select count(*) as totalTNSales,event_id,max(id) as id from tnd_sales_autopricing tnd with(nolock)" +
		" where event_Date>getdate()+2 and order_Date>=convert(date,getdate()-1)" +
		" group by event_id having count(*)>=5 ) as tt on tt.id=td.id" +
		" left join (" +
		" select count(*) as tixCitySales,event_id,max(id) as id from tnd_sales_autopricing tnd with(nolock)" +
		" where event_Date>getdate()+2 and order_Date>=convert(date,getdate()-1) and owner_company_name like '%TIX CITY%'" +
		" group by event_id ) as tx on tx.event_id=td.event_id" +
		" ) as g" +
		" left join event e with(nolock) on e.admitone_id=g.TNEventId" +
		" left join venue_category vc with(nolock) on vc.id=e.venue_category_id" +
		" left join venue v with(nolock) on v.id=e.venue_id" +
		" where g.eventName not like '%parking%' and (v.id is null or v.country in ('US','CA')) and g.tnSales>=5" +
		" ) as gg" +
		" where gg.TMATEvent='N' or gg.CSV='N' or gg.tixCitySales=0 or (gg.tnSales-gg.tixCitySales) >10 or" +
		" (gg.TicketEvolution='N' and gg.StubHub='N') or (gg.RTFTixCount=0 and gg.tixcityTixCount=0)" +
		" order by gg.eventDate,gg.eventName";

	
	Session session=null;
	List<TNDRtfNoSaleReport> list = new ArrayList<TNDRtfNoSaleReport>();
	try{
		session=sessionFactory.openSession();
		SQLQuery sqlQuery=session.createSQLQuery(sql);
		sqlQuery.addScalar("eventId",Hibernate.INTEGER);
		sqlQuery.addScalar("eventName",Hibernate.STRING);
		sqlQuery.addScalar("eventDate",Hibernate.TIMESTAMP);
		//sqlQuery.addScalar("eventTime",Hibernate.TIMESTAMP);
		//sqlQuery.addScalar("eventDateStr",Hibernate.STRING);
		//sqlQuery.addScalar("eventTimeStr",Hibernate.STRING);
		sqlQuery.addScalar("venue",Hibernate.STRING);
		sqlQuery.addScalar("city",Hibernate.STRING);
		sqlQuery.addScalar("state",Hibernate.STRING);
		//sqlQuery.addScalar("country",Hibernate.STRING);
		sqlQuery.addScalar("tmatEvent",Hibernate.STRING);
		sqlQuery.addScalar("csvUploaded",Hibernate.STRING);
		sqlQuery.addScalar("mapUploaded",Hibernate.STRING);
		sqlQuery.addScalar("tnSalesCount",Hibernate.INTEGER);
		sqlQuery.addScalar("tixCitySalesCount",Hibernate.INTEGER);
		sqlQuery.addScalar("rtfTicketsCount",Hibernate.INTEGER);
		sqlQuery.addScalar("tixcityTicketsCount",Hibernate.INTEGER);
		
		
		sqlQuery.addScalar("ticketNetworkDirectCrawl",Hibernate.STRING);
		sqlQuery.addScalar("ticketEvolutionCrawl",Hibernate.STRING);
		sqlQuery.addScalar("stubhubCrawl",Hibernate.STRING);
		sqlQuery.addScalar("vividSeatsCrawl",Hibernate.STRING);
		sqlQuery.addScalar("flashSeatsCrawl",Hibernate.STRING);
		
		sqlQuery.addScalar("autocats96Enabled",Hibernate.STRING);
		sqlQuery.addScalar("larryLastEnabled",Hibernate.STRING);
		sqlQuery.addScalar("zoneLastRowEnabled",Hibernate.STRING);
		sqlQuery.addScalar("rtfListingEnabled",Hibernate.STRING);
		
		list = sqlQuery.setResultTransformer(Transformers.aliasToBean(TNDRtfNoSaleReport.class)).list();
		
	}catch (Exception e) {
		e.printStackTrace();
	}finally{
		session.close();
	}
	return list;
}
public List<SVGFileMissingZoneData> getAllVenueMapZonesNotInCSV() throws Exception {
	String sql = "select distinct v.id as venueId,v.building as building,v.city as city,v.state as state,v.country as country," +
			" vc.id venueCategoryId,vc.category_group as categoryGroupName,vmz.zone as zone from venue_map_zones vmz" +
			" inner join venue_category vc on vc.id=vmz.venue_category_id" +
			" inner join venue v on v.id=vc.venue_id" +
			" where v.country in ('US','CA')" +
			" and not exists (select * from category_new cc where cc.venue_category_id=vmz.venue_category_id and cc.symbol=vmz.zone)" +
			" order by building,city,state,country,category_group";
	Session session=null;
	List<SVGFileMissingZoneData> missingVenueMapZonesList = new ArrayList<SVGFileMissingZoneData>();
	try{
		session=sessionFactory.openSession();
		SQLQuery sqlQuery=session.createSQLQuery(sql);
		missingVenueMapZonesList =  sqlQuery.setResultTransformer(Transformers.aliasToBean(SVGFileMissingZoneData.class)).list();
	}catch (Exception e) {
		e.printStackTrace();
	}finally{
		session.close();
	}
	return missingVenueMapZonesList;
}
public List<SVGFileMissingZoneData> getAllCsvZonesNotInVenueMap() throws Exception {
	String sql = "select distinct v.id as venueId,v.building as building,v.city as city,v.state as state,v.country as country," +
			" vc.id venueCategoryId,vc.category_group as categoryGroupName,cc.symbol as zone from category_new cc" +
			" inner join venue_category vc on vc.id=cc.venue_category_id" +
			" inner join venue v on v.id=vc.venue_id" +
			" where v.country in ('US','CA') and vc.venue_map=1" +
			" and not exists (select * from venue_map_zones vmz where cc.venue_category_id=vmz.venue_category_id and cc.symbol=vmz.zone)" +
			" order by building,city,state,country,category_group";
	Session session=null;
	List<SVGFileMissingZoneData> missingVenueMapZonesList = new ArrayList<SVGFileMissingZoneData>();
	try{
		session=sessionFactory.openSession();
		SQLQuery sqlQuery=session.createSQLQuery(sql);
		missingVenueMapZonesList =  sqlQuery.setResultTransformer(Transformers.aliasToBean(SVGFileMissingZoneData.class)).list();
	}catch (Exception e) {
		e.printStackTrace();
	}finally{
		session.close();
	}
	return missingVenueMapZonesList;
}

public List<Object[]> getAllVenueMapZonesWihoutListingsinRTF() throws Exception {
	String sql = "select distinct e.id as EventId,e.name as Event," +
			" CASE WHEN e.event_time is not null then CONVERT(VARCHAR(19),e.event_date,101) else 'TBD' end as EventDate," +
			" CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7))" +
			" WHEN e.event_time is null THEN 'TBD' END as EventTime,a.name as artistName," +
			" v.building as Venue,v.city as City,v.state as State,v.country as Country,vmz.zone as mapZone," +//vc.category_group as venueCategory,
			" e.event_date,e.event_time" +
			" from event e" +
			" inner join artist a on a.id=e.artist_id" +
			" inner join zone_tickets_processor_exchange_event tn on tn.event_id=e.id" +
			" inner join venue_Category vc on vc.id=e.venue_Category_id" +
			" inner join venue v on v.id=e.venue_id" +
			" inner join venue_map_zones vmz on vmz.venue_category_id=e.venue_category_id" +
			" where e.event_status='ACTIVE' and tn.status='ACTIVE'  and v.country in ('US','CA') and vc.venue_map=1 and" +
			" not exists (select * from zone_tickets_category_ticket_group tg where tg.event_id=e.id and status='ACTIVE' and tg.section=vmz.zone and tg.zone_tickets_ticket_group_id is not null)" +
			" order by e.event_date,e.event_time,e.name,e.id,zone";
	
	Session session=null;
	List<Object[]> objList = null;
	try{
		session=sessionFactory.openSession();
		SQLQuery sqlQuery=session.createSQLQuery(sql);
		objList = sqlQuery.list();
		
	}catch (Exception e) {
		e.printStackTrace();
	}finally{
		session.close();
	}
	return objList;
}
public boolean updateRTFVenueMapStatus(Integer venueCategoryId,Connection connection) throws Exception {
	 
	 CallableStatement cs = null;
	 try{
	     cs = connection.prepareCall("{call SP_update_venue_map(?)}");
	     cs.setInt(1, venueCategoryId);
        return cs.execute();
	 }catch(Exception e){
		 //e.printStackTrace();
		 //return false;
		 throw e;
	 }
}


public List<Integer> AllEventsWhichHasSH_TE_TS_VDCrawls(Connection connection) throws Exception{
	
    String sql = "select order_id from csv_order_ids";
	  
	ResultSet resultSet =null;
	List<Integer> ordrIds = new ArrayList<Integer>();
	
	try {
		PreparedStatement statement = connection.prepareStatement(sql);
		resultSet = statement.executeQuery();
		while(resultSet.next()){
			Integer orderId= resultSet.getInt("order_id");
			ordrIds.add(orderId);
		}
		return ordrIds;
	} catch (SQLException e) {
		e.printStackTrace();
	}finally{

	}
	return null;
}

public List<Integer> AllEventsWhichHasSH_TE_TS_VDCrawls() {
	String sql ="select distinct e.id from  event e WITH(NOLOCK) where e.event_status='ACTIVE' and " +
"e.event_Date>getdate() and"
+" e.id in (select event_id from ticket_listing_Crawl WITH(NOLOCK) where enabled=1 and site_id ='stubhub') and "
+ " e.id in (select event_id from ticket_listing_Crawl WITH(NOLOCK) where enabled=1 and site_id ='ticketevolution') and " +
" e.id in (select event_id from ticket_listing_Crawl WITH(NOLOCK) where enabled=1 and site_id ='vividseat')  and " +
" e.id in (select event_id from ticket_listing_Crawl WITH(NOLOCK) where enabled=1 and site_id ='ticketsnow') and " +
"  e.id in (select event_id from ticket_listing_Crawl WITH(NOLOCK) where enabled=1 and site_id ='ticketmaster')";
	Session session=null;
	SQLQuery query = null;
	try {
		session=sessionFactory.openSession();
		query=session.createSQLQuery(sql);
		List<Integer> result = query.list();
		return result;
	} catch (Exception e) {
		if(session != null && session.isOpen() ){
			session.close();
		}
		e.printStackTrace();
	} 
	return null;
}





public List<Integer> AllEventsWhichHasTMCrawls() {
	String sql ="select distinct e.id from  event e WITH(NOLOCK) where e.event_status='ACTIVE' and " +
"e.event_Date>getdate() and " + 
"  e.id in (select event_id from ticket_listing_Crawl WITH(NOLOCK) where enabled=1 and site_id ='ticketmaster')";
	Session session=null;
	SQLQuery query = null;
	try {
		session=sessionFactory.openSession();
		query=session.createSQLQuery(sql);
		List<Integer> result = query.list();
		return result;
	} catch (Exception e) {
		if(session != null && session.isOpen() ){
			session.close();
		}
		e.printStackTrace();
	} 
	return null;
}






public List<Integer> AllTNEventIdsWhichHasMoreThan5Sales() {
	String sql ="select distinct event_id from (select event_id , count(id) as " +
			"soldTixCount from tnd_sales_autopricing WITH(NOLOCK) where event_date > GETDATE() group by event_id) a " +
			"where soldTixCount >= 5  ";
	Session session=null;
	SQLQuery query = null;
	try {
		session=sessionFactory.openSession();
		query=session.createSQLQuery(sql);
		List<Integer> result = query.list();
		return result;
	} catch (Exception e) {
		if(session != null && session.isOpen() ){
			session.close();
		}
		e.printStackTrace();
	} 
	return null;
}

public List<Integer> AllTNEventIdsWhichHasMoreThan1AndUpto5Sales() {
	String sql ="select distinct event_id from (select event_id , count(id) as " +
			"soldTixCount from tnd_sales_autopricing WITH(NOLOCK) where event_date > GETDATE() group by event_id) a " +
			"where soldTixCount < 5 and soldTixCount >= 2  ";
	Session session=null;
	SQLQuery query = null;
	try {
		session=sessionFactory.openSession();
		query=session.createSQLQuery(sql);
		List<Integer> result = query.list();
		return result;
	} catch (Exception e) {
		if(session != null && session.isOpen() ){
			session.close();
		}
		e.printStackTrace();
	} 
	return null;
}

	public List<Integer> getAllEventsWhichDoNotAnyCrawls() {
		String sql ="select distinct e.id from event e WITH(NOLOCK) inner join venue v WITH(NOLOCK) on e.venue_id=v.id " +
				"inner join artist a WITH(NOLOCK) " +
				"on e.artist_id=a.id where e.id not in (select distinct event_id from ticket_listing_crawl WITH(NOLOCK) " +
				"where site_id in ('stubhub','ticketevolution','ticketsnow','vividseat') and enabled=1) " +
				"and e.event_status='ACTIVE' and e.event_date > GETDATE()";
		Session session=null;
		SQLQuery query = null;
		try {
			session=sessionFactory.openSession();
			query=session.createSQLQuery(sql);
			List<Integer> result = query.list();
			return result;
		} catch (Exception e) {
			if(session != null && session.isOpen() ){
				session.close();
			}
			e.printStackTrace();
		} 
		return null;
	}
	public Double getTixCityUnfilledTotalWholesaleAmount(String summaryDateStr) throws Exception {
		
		String posUrl = tixCityPos;
		
		Session session = null;
		List<Object> list = null;
		try{

			String queryString="select sum(tt.[total price])  as totalPrice" +
					" from "+posUrl+".invoice i" +
					" inner join (select distinct count(t.invoice_id) as 'qty'," +
					" case when t.wholesale_price <> 0 then (t.wholesale_price*count(t.invoice_id)) else" +
					" (t.actual_sold_price*count(t.invoice_id)) end as 'total price',t.invoice_id" +
					" from  "+posUrl+".ticket t with(nolock)" +
					" inner join  "+posUrl+".ticket_group tg with(nolock) on tg.ticket_group_id=t.ticket_group_id" +
					" inner join  "+posUrl+".event e with(nolock) on e.event_id=tg.event_id" +
					" left join  "+posUrl+".category_ticket ct with(nolock) on ct.ticket_id=t.ticket_id" +
					" where tg.office_id=2 and ct.category_ticket_id is null and e.event_datetime>'"+summaryDateStr+"'" +
					" group by t.invoice_id,e.event_id," +
					" t.section,t.row,t.retail_price,t.wholesale_price,t.cost,t.actual_sold_price" +
					" union" +
					" select distinct count(ct.invoice_id) as 'qty'," +
					" case when ctg.wholesale_price <> 0 then (ctg.wholesale_price*count(ct.invoice_id)) else" +
					" (ct.actual_price*count(ct.invoice_id)) end as 'total price',ct.invoice_id" +
					" from  "+posUrl+".category_ticket ct with(nolock)" +
					" inner join  "+posUrl+".category_ticket_group ctg with(nolock) on ct.category_ticket_group_id=ctg.category_ticket_group_id" +
					" inner join  "+posUrl+".venue_category vc with(nolock) on vc.venue_category_id=ctg.venue_category_id" +
					" inner join  "+posUrl+".event e with(nolock) on e.event_id=ctg.event_id" +
					" left join  "+posUrl+".ticket t with(nolock) on t.ticket_id=ct.ticket_id" +
					" where e.event_datetime>'"+summaryDateStr+"' and (t.ticket_id is null or ct.fill_date>'"+summaryDateStr+"')" +
					" group by e.event_id,vc.section_low,vc.row_low,vc.seat_high,vc.seat_low," +
					" ctg.retail_price,ctg.wholesale_price,ctg.cost,ct.actual_price,ct.invoice_id" +
					" ) as tt on tt.invoice_id=i.invoice_id" +
					" where  i.invoice_status_id != 2 and i.create_Date<'"+summaryDateStr+"'" +
					" ";
			
			
			session = sessionFactory.openSession();
			Query query = session.createSQLQuery(queryString);
			list = query.list();
			if(list != null && list.size() == 1){
				return Double.valueOf(list.get(0).toString());
			}else{
				return null;
			}
						
		}catch(Exception e){
			e.printStackTrace();
			throw e;
		}finally{
			session.close();
		}
		//return null;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<Object[]> getVividSeatsLatestInventory(String eventIds, Integer noOfdaysCreatedEvent){
		
		String subQuery = "";
		if(null != eventIds && !eventIds.isEmpty()) {
			subQuery = subQuery + " and e.id in ("+eventIds.trim()+") ";
		}else {
			subQuery = subQuery + " and e.creation_date >= getdate() - "+noOfdaysCreatedEvent+" ";
		}
	
		String query = "select e.id,'807308' as 'Vendor ID',	e.name as Event,	v.building as Venue,	CONVERT(VARCHAR(19),e.event_date,101) as EventDate, "
				+ "CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null "
				+ "THEN 'TBD' END as	EventTime,tt.quantity as Quantity,tt.section as Section, tt.row as Row, '1001' as 'Low Seat'	, '' as 'Public Notes', "
				+ "'' as 'Internal Notes',	 '0' as 'Group Cost',tt.price as 'Unit List Price', '' as 'Expected Value','xfer' as StockType, "
				/*+ "--case when (select count(*) from ticket t where t.event_id=tt.event_id and t.section=tt.section and t.row=tt.row and t.current_price=tt.price and "
				+ "--t.ticket_type='REGULAR' and t.ticket_status='ACTIVE' and t.quantity>=2 and t.ticket_delivery_type is not null) > 0 then 'etickets' else '' end as StockType, "*/
				+ "'N' as 'Electronic Transfer',CONVERT(VARCHAR(19),dateadd(DD,-2,e.event_date),101) as 'InHandDate', 'Y' as 'Broadcast?', 'Y' as 'Hide Seat Numbers',"
				+ "'default' as SplitType, '' as SplitValue, 'consecutive' as 'Seat Type', "
				+ "'Y' as	'Zone Listing', 'RTF' as 'Tags' "
				/*+ "--,case when (select count(*) from ticket t where t.event_id=tt.event_id and t.section=tt.section and t.row=tt.row and t.current_price=tt.price and "
				+ "--t.ticket_type='REGULAR' and t.ticket_status='ACTIVE' and t.quantity>=2 and t.ticket_delivery_type is not null) > 0 then 'Y' else 'N' end as isInstantTix "*/
				+ "from event e with(nolock) inner join venue v with(nolock) on v.id=e.venue_id "
				+ "inner join ( select event_id,section,row,min(current_price) as price,4 as quantity from ticket t with(nolock) inner join "
				+ "event e with(nolock) on e.id=t.event_id inner join artist a with(nolock) on a.id=e.artist_id inner join grand_child_tour_Category "
				+ "gc with(nolock) on gc.id=a.grand_child_category_id inner join child_tour_Category cc with(nolock) on cc.id=gc.child_Category_id "
				+ "inner join venue v with(nolock) on v.id=e.venue_id where t.ticket_Status='ACTIVE' and e.event_status='ACTIVE' "
				+ " "+subQuery+" "
				/*+ "--and gc.name like 'nba' and v.state!='NY' "
				+ "--and e.name like '%Dead & Company%' "*/
				+ "and t.quantity>=2 "
				/*"--and e.event_Date>='2020-01-01' "*/
				+ "and ticket_type='REGULAR' and site_id='ticketnetworkdirect' group by event_id,section,row ) as tt on tt.event_id=e.id "
				+ "where e.event_date>dateadd(dd,7,getdate()) "
				+ " "+subQuery+" "
				+ "and v.country in ('us','usa','ca') "
				+ "and (tt.section not like '%park%' and tt.section not like '%pass%' and tt.section not like '%vippk%' and tt.section not like '%vipk%' "
				+ "and tt.section not like '%pltprk%' and tt.section not like '%prmprk%' and tt.section not like '%vipprk%' and tt.section not like '%lot%' "
				+ "and tt.section not like '%garage%' and tt.section not like '%aces%' and tt.section not like '%package%' "
				+ "and tt.row not like '%park%' and tt.row not like '%pass%' and tt.row not like '%vippk%' and tt.row not like '%vipk%' and tt.row not like '%pltprk%' "
				+ "and tt.row not like '%prmprk%' and tt.row not like '%vipprk%' and tt.row not like '%lot%' and tt.row not like '%garage%' and tt.row not like '%aces%' "
				+ "and tt.row not like '%package%' ) order by e.id,event_date,event_time,e.name,section,row";
	
	Session session = null;
	List<EventCrawlerDetails> listDetails = null;
	List<Object[]> list=null;
	try{
		System.out.println("VividSeats Inventory: SQL Query Execution Started at "+ new Date());
		System.out.println(query);
		session=sessionFactory.openSession();
		SQLQuery sqlQuery = session.createSQLQuery(query);
		list=sqlQuery.list();
		System.out.println("VividSeats Inventory: SQL Query Execution Ended at "+ new Date());
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}finally{
		session.close();
	}
	return list;
}
	
	public List<Object[]> AllEventsCreatedinLast7Days(String eventIds,Integer noOfDaysEventCreated) {
		String sql ="select e.id, tc.id as crawlId from event e with(nolock) inner join ticket_listing_crawl tc on tc.event_id = e.id "
				+ "and tc.site_id = 'ticketnetworkdirect' and tc.enabled=1 inner join artist a with(nolock) on a.id=e.artist_id "
				+ "inner join grand_child_tour_Category gc with(nolock) on gc.id=a.grand_child_category_id "
				+ "inner join child_tour_Category cc with(nolock) on cc.id=gc.child_Category_id inner join "
				+ "venue v with(nolock) on v.id=e.venue_id  where e.event_status = 'ACTIVE' and "
				+ "e.event_date>dateadd(dd,7,getdate()) and v.country in ('us','usa','ca')  ";
		
		if(null != eventIds && !eventIds.isEmpty()) {
			sql =  sql +" and e.id in (" + eventIds +") ";
			
			//sql = sql +" and ( tc.end_crawl is null or datediff(HOUR,GETDATE(),tc.end_crawl) > 2) ";
		}else {
			sql =  sql +" and e.creation_date >= getdate() - "+noOfDaysEventCreated;
		}
		
		sql =  sql +" order by e.creation_date desc";
				 
		
		Session session=null;
		SQLQuery query = null;
		try {
			System.out.println(sql);
			session=sessionFactory.openSession();
			query=session.createSQLQuery(sql);
			List<Object[]> result = query.list();
			return result;
		} catch (Exception e) {
			if(session != null && session.isOpen() ){
				session.close();
			}
			e.printStackTrace();
		} 
		return null;
	}
	
	
	public List<VividSeatsTicketInventory> AllEventsCreatedinLast7Days(String eventName, String artistIds, String venueIds, String fromDate, String toDate) {
		String sql ="select e.id, e.name as eventName,CONVERT(VARCHAR(19),e.event_date,101) as EventDate, "
				+ "CASE WHEN e.event_time is not null THEN LTRIM(RIGHT(CONVERT(VARCHAR(20), e.event_time, 100), 7)) WHEN e.event_time is null "
				+ "THEN 'TBD' END as	EventTime, a.name as artistName, v.building as venueName, v.city, v.state, v.country "
				+ " from event e with(nolock) inner join ticket_listing_crawl tc on tc.event_id = e.id "
				+ "and tc.site_id = 'ticketnetworkdirect' and tc.enabled=1 inner join artist a with(nolock) on a.id=e.artist_id "
				+ "inner join grand_child_tour_Category gc with(nolock) on gc.id=a.grand_child_category_id "
				+ "inner join child_tour_Category cc with(nolock) on cc.id=gc.child_Category_id inner join "
				+ "venue v with(nolock) on v.id=e.venue_id  where e.event_status = 'ACTIVE' and "
				+ "e.event_date>dateadd(dd,7,getdate()) and v.country in ('us','usa','ca')  ";
		
		if(null != eventName && !eventName.isEmpty()) {
			sql =  sql +" and e.name like '%"+eventName.trim()+"%'";
		}else if(null != artistIds && !artistIds.isEmpty() && artistIds.length() > 0) {
			
			if(null != venueIds && !venueIds.isEmpty() && venueIds.length() > 0) {
				sql =  sql +" and ( a.id in ("+artistIds+") or v.id in ("+venueIds+"))";
			}else {
				sql =  sql +" and a.id in ("+artistIds+")";
			}
			/*sql =  sql +" and a.id in ("+artistIds+")";*/
		}else if(null != venueIds && !venueIds.isEmpty() && venueIds.length() > 0) {
			sql =  sql +" and v.id in ("+venueIds+")";
		} 
		
		if(null != fromDate && !fromDate.isEmpty()) {
			sql = sql + " and CAST( e.event_date as DATE)>='"+fromDate+"' ";
		}
		
		if(null != toDate && !toDate.isEmpty()) {
			sql = sql + " and CAST( e.event_date as DATE)<='"+toDate+"'";
		}
		
		
		sql =  sql +" order by e.event_date asc";
		List<VividSeatsTicketInventory> list = new ArrayList<>();
		Session session=null;
		SQLQuery query = null;
		VividSeatsTicketInventory invObj = null;
		try {
			System.out.println(sql);
			session=sessionFactory.openSession();
			query=session.createSQLQuery(sql);
			List<Object[]> result = query.list();
			if(null != result && !result.isEmpty() && result.size() > 0) {
				
				
				for (Object[] object : result) {
					invObj = new VividSeatsTicketInventory();
					invObj.setEventId(Integer.parseInt(object[0].toString()));
					invObj.setEventName(object[1].toString());
					invObj.setEventDate(object[2].toString());
					invObj.setEventTime(object[3].toString());
					invObj.setArtistName(object[4].toString());
					invObj.setVenueName(object[5].toString()+", "+object[6].toString()+", "+object[7].toString()+", "+object[8].toString());
					list.add(invObj);
				}
			}
			return list;
		} catch (Exception e) {
			if(session != null && session.isOpen() ){
				session.close();
			}
			e.printStackTrace();
		} 
		return null;
	}
	
	public static void main(String[] args) throws InterruptedException {
		
		QueryManagerDAO dao = new QueryManagerDAO();
		
		List<Integer> eventIds = new ArrayList<>();
		eventIds.add(12345);
		
		dao.AllEventsCreatedinLast7Days("4545", 5);
		
		
	}

}
