package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.admitone.tmat.data.ZonesPricingFloorCapAudit;

public class ZonesPricingFloorCapAuditDAO extends HibernateDAO<Integer, ZonesPricingFloorCapAudit>
	implements com.admitone.tmat.dao.ZonesPricingFloorCapAuditDAO {

	public List<ZonesPricingFloorCapAudit> getZoneFloorCapAuditByEvntIdAndZoneId(Integer eId, String zone) {
		
		Session session = getSession();
		List<ZonesPricingFloorCapAudit> list = new ArrayList<ZonesPricingFloorCapAudit>();
		try{
			Query query = session.createQuery("FROM ZonesPricingFloorCapAudit where eventId = :eIds and zone = :zoneParam order by createdDate");
			query.setParameter("eIds", eId);
			query.setParameter("zoneParam", zone);
			list= query.list();
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			session.close();
		}				
		return list;
	}
	}
