package com.admitone.tmat.dao.hibernate;

import com.admitone.tmat.data.User;

public class UserBrokerDetailsDAO extends HibernateDAO<Integer, User> implements com.admitone.tmat.dao.UserBrokerDetailsDAO {

	@Override
	public void deleteBrokerDetailsByUserId(Integer userId) {
		// TODO Auto-generated method stub
		bulkUpdate("DELETE FROM UserBrokerDetails WHERE user_id=?", new Object[]{userId});
	}
}
