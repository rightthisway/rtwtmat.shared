package com.admitone.tmat.dao.hibernate;


import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.VWVipCatsCategoryTicket;

public class VWVipCatsCategoryTicketDAO extends HibernateDAO<Integer, VWVipCatsCategoryTicket> implements com.admitone.tmat.dao.VWVipCatsCategoryTicketDAO {

	public VWVipCatsCategoryTicket get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<VWVipCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public Collection<VWVipCatsCategoryTicket> getAll() {
		return find("FROM VWVipCatsCategoryTicket");
	}

	public void saveOrUpdateAll(Collection<VWVipCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<VWVipCatsCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<VWVipCatsCategoryTicket> getAllActiveTnVipCatsCategoryTickets() {
		return find("FROM VWVipCatsCategoryTicket where status='ACTIVE' and tn_category_ticket_group_id is not null and tn_category_ticket_group_id !=0");
	}
}
