package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.MiniTNExchangeEvent;

public class MiniTNExchangeEventDAO extends HibernateDAO<Integer,MiniTNExchangeEvent> implements com.admitone.tmat.dao.MiniTNExchangeEventDAO{

	public Collection<MiniTNExchangeEvent> getAllMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniTNExchangeEvent> result = new ArrayList<MiniTNExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniTNExchangeEvent> temp = find("FROM MiniTNExchangeEvent WHERE event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public Collection<MiniTNExchangeEvent> getAllActiveMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniTNExchangeEvent> result = new ArrayList<MiniTNExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniTNExchangeEvent> temp = find("FROM MiniTNExchangeEvent WHERE status='ACTIVE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection<MiniTNExchangeEvent> getAllDeletedMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<MiniTNExchangeEvent> result = new ArrayList<MiniTNExchangeEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<MiniTNExchangeEvent> temp = find("FROM MiniTNExchangeEvent WHERE status='DELETE' and event.id " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAllMiniTNExchangeEventsByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("DELETE FROM MiniTNExchangeEvent WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public void updateAllMiniTNExchangeEventAsDeleteByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("UPDATE MiniTNExchangeEvent SET status='DELETE' WHERE event.id IN ("  + eventList + ")");
		}
	}
	
	public List<MiniTNExchangeEvent> getAllActiveMiniTNExchangeEvent() {
		try{
			return find("FROM MiniTNExchangeEvent WHERE event.id IN(SELECT DISTINCT eventId from TGCatsCategoryTicket where status ='ACTIVE')", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public void saveBulkEvents(List<MiniTNExchangeEvent> tnEvents) throws Exception {
	
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (MiniTNExchangeEvent event : tnEvents) {
			    session.save(event);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
	
	public void updateExposure(String mini_exposure_ticketnetwork)
	{
		String query="update MiniTNExchangeEvent set exposure="+"'"+mini_exposure_ticketnetwork+"'";
		bulkUpdate(query);
	}
	public void updateRptFactor(String rpt_ticketnetwork)
	{
		String query="update MiniTNExchangeEvent set rptFactor="+"'"+rpt_ticketnetwork+"'";
	    bulkUpdate(query);
	}
	public void updatePriceBreakup(String pricebreakup_ticketnetwork)
	{
		String query="update MiniTNExchangeEvent set priceBreakup="+"'"+pricebreakup_ticketnetwork+"'";
	    bulkUpdate(query);
	}
	public void updateUpperMarkup(String upper_markpu_ticketnetwork)
	{
		String query="update MiniTNExchangeEvent set upperMarkup="+"'"+upper_markpu_ticketnetwork+"'";
	    bulkUpdate(query);
	}
	public void updateLowerMarkup(String lower_markpu_ticketnetwork)
	{
		String query="update MiniTNExchangeEvent set lowerMarkup="+"'"+lower_markpu_ticketnetwork+"'";
	    bulkUpdate(query);
	}
	public void updateUpperShippingFees(String upper_shippingfees_ticketnetwork)
	{
		String query="update MiniTNExchangeEvent set upperShippingFees="+"'"+upper_shippingfees_ticketnetwork+"'";
	    bulkUpdate(query);
	}
	public void updateLowerShippingFees(String lower_shippingfees_ticketnetwork)
	{
		String query="update MiniTNExchangeEvent set lowerShippingFees="+"'"+lower_shippingfees_ticketnetwork+"'";
	    bulkUpdate(query);
	}
	
public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType) {
	Session session=null;
	try {
		String query = "";
		
		if(globalAudit.getMiniExposure() != null) {
			query = query + ",ee.exposure='"+globalAudit.getMiniExposure()+"'";
		}
		if(globalAudit.getRptFactor() != null) {
			query = query + ",ee.rpt_factor="+globalAudit.getRptFactor();
		}
		if(globalAudit.getShippingMethod() != null) {
			query = query + ",ee.shipping_method='"+globalAudit.getShippingMethod()+"'";
		}
		if(globalAudit.getNearTermDisplayOption() != null) {
			query = query + ",ee.near_term_display_option='"+globalAudit.getNearTermDisplayOption()+"'";
		}
		if(globalAudit.getPriceBreakup() != null) {
			query = query + ",ee.price_breakup="+globalAudit.getPriceBreakup();
		}
		if(globalAudit.getLowerMarkup() != null) {
			query = query + ",ee.lower_markup="+globalAudit.getLowerMarkup();
		}
		if(globalAudit.getUpperMarkup() != null) {
			query = query + ",ee.upper_markup="+globalAudit.getUpperMarkup();
		}
		if(globalAudit.getLowerShippingFees() != null) {
			query = query + ",ee.lower_shipping_fees="+globalAudit.getLowerShippingFees();
		}
		if(globalAudit.getUpperShippingFees() != null) {
			query = query + ",ee.upper_shipping_fees="+globalAudit.getUpperShippingFees();
		}
		query = "update ee set " + query.substring(1);
		
		query = query + " from mini_tn_exchange_event ee " +
		" inner join event e on e.id=ee.event_id " +
		" inner join tour t on t.id=e.tour_id" +
		" inner join grand_child_tour_category gc on gc.id=t.grand_child_category_id" +
		" inner join child_tour_category cc on cc.id=gc.child_category_id" +
		" inner join tour_category tc on tc.id=cc.tour_category_id " +
		" where ee.status='ACTIVE' and tc.name in("+parentType+")";
		
		session = getSessionFactory().openSession();
		SQLQuery sqlQuery = session.createSQLQuery(query);
		sqlQuery.executeUpdate();
		
	} catch (Exception e) {
		e.printStackTrace();
	} finally{
		session.close();
	}
	}

}
