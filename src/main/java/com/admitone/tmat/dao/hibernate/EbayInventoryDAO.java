package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.EbayInventory;

public  class EbayInventoryDAO extends HibernateDAO<Integer, EbayInventory> implements com.admitone.tmat.dao.EbayInventoryDAO {
	public Collection<EbayInventory> getEbayInventoriesByEventUsername(Integer eventId, String username) {
		String query = "FROM EbayInventory WHERE 1 ";
		
		if (eventId != null) {
			query += "AND eventId=" + eventId + " ";
		}
		
		if (username != null) {
			query += "AND username='" + username + "' ";
		}
		
		return find(query);
	}

	public Collection<EbayInventory> getEbayInventoriesByGroupId(Integer groupId) {
		return find("FROM EbayInventory WHERE group_id=? ORDER BY rank ASC", new Object[]{groupId});
	}
	
	public EbayInventory getEbayInventoriesByGroupIdAndRank(Integer groupId, Integer rank) {
		Collection<EbayInventory> list = find("FROM EbayInventory WHERE group_id=? AND rank=?", new Object[]{groupId, rank});
		if (list.isEmpty()) {
			return null;
		}
		
		return list.iterator().next();
	}
	
	public void deleteByGroupId(Integer groupId) {
		bulkUpdate("DELETE FROM EbayInventory WHERE group_id=?", new Object[]{groupId});
	}
	
	public void deleteEbayInventoryByEventId(Integer eventId) {
		bulkUpdate("DELETE FROM EbayInventory WHERE groupId in (SELECT id FROM EbayInventoryGroup where eventId = ? )", new Object[]{eventId});
		
	}	
}
