package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.data.TNTGCatsCategoryEvent;

public class TNTGCatsCategoryEventDAO extends HibernateDAO<Integer,TNTGCatsCategoryEvent> implements com.admitone.tmat.dao.TNTGCatsCategoryEventDAO{

	public Collection<TNTGCatsCategoryEvent> getAllTGCatsCategoryEventByEvent(List<Integer> eventIds, boolean isEventInclusive) {
		try {
		String inclusive= "";
		if(!isEventInclusive){
			inclusive =   " NOT ";
		}
		Map<Integer,String> map = new HashMap<Integer, String>();
		Collection<TNTGCatsCategoryEvent> result = new ArrayList<TNTGCatsCategoryEvent>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			Collection<TNTGCatsCategoryEvent> temp = find("FROM TNTGCatsCategoryEvent WHERE eventId " + inclusive + " IN ("  + eventList + ")");
			if(temp!=null){
				result.addAll(temp);
			}
		}
		return result;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public void deleteAllTGCatsCategoryEventByEvent(List<Integer> eventIds) {
		Map<Integer,String> map = new HashMap<Integer, String>();
		String eventList = "";
		int i = 0;
		int count =0;
		for(Integer id:eventIds){
			if(count==2000){
				count =0;
				i++;
				map.put(i, eventList);
				eventList ="";
			}
			eventList = eventList + id + ",";
			count++;
		}
		if(!eventList.isEmpty()){
			map.put(++i, eventList);
		}
		for(Integer ii:map.keySet()){
			eventList = map.get(ii);
			if(!eventList.isEmpty()){
				eventList = eventList.substring(0, eventList.length()-1);
			}
			bulkUpdate("DELETE FROM TNTGCatsCategoryEvent WHERE eventId IN ("  + eventList + ")");
		}
	}
	
	public List<TNTGCatsCategoryEvent> getAllActiveTGCatsCategoryEvent() {
		try{
			return find("FROM TNTGCatsCategoryEvent WHERE eventId IN(SELECT DISTINCT eventId from TGCatsCategoryTicket where status ='ACTIVE')", new Object[]{});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
}
