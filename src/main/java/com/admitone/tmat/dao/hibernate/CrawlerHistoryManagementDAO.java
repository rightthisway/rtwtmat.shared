package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.CrawlerHistoryManagement;

public class CrawlerHistoryManagementDAO extends HibernateDAO<Integer, CrawlerHistoryManagement> implements com.admitone.tmat.dao.CrawlerHistoryManagementDAO{

	public List<CrawlerHistoryManagement> getCountersOfAllSiteIds(Date startDate,Date endDate){
		String sql="";
		sql = "SELECT SUM(counter),siteId,system FROM CrawlerHistoryManagement chm WHERE 1=1";
		List<Object> parameter= new ArrayList<Object>();
		if(startDate != null ){
			sql+=" AND successfulHit >= ?";
			parameter.add(startDate);
		}
		if(endDate != null ){
			sql+=" AND successfulHit <= ?";
			parameter.add(endDate);
		}
		sql+=" GROUP BY siteId,system ORDER BY system";
		List<Object[]> result=find(sql,parameter.toArray());
		List<CrawlerHistoryManagement> list= new ArrayList<CrawlerHistoryManagement>();
		Long  counter=0L;
		for(Object[]temp:result){
			CrawlerHistoryManagement crawlerHistoryManagement= new CrawlerHistoryManagement();
			counter=(Long)temp[0];
			String siteId=(String) temp[1];
			String system=(String) temp[2];
//			if(siteId.equalsIgnoreCase("eimarketplace")){
//				counter=counter*5/3;
//			}
			crawlerHistoryManagement.setCounter(counter.intValue());
			crawlerHistoryManagement.setSiteId(siteId);
			crawlerHistoryManagement.setSystem(system);
			list.add(crawlerHistoryManagement);
		}
		return list;
	}
	
	public  List<CrawlerHistoryManagement> getCrawlerHistory(Integer crawlId,Date startDate,Date endDate,String system) {
		List<CrawlerHistoryManagement> list =null;
		String sql="";
		sql = "FROM CrawlerHistoryManagement WHERE crawlId= ?";
		List<Object> parameter= new ArrayList<Object>();
		parameter.add(crawlId);
		if(startDate != null ){
			sql+=" AND successfulHit >= ?";
			parameter.add(startDate);
		}
		if(endDate != null ){
			sql+=" AND successfulHit <= ?";
			parameter.add(endDate);
		}
		if(system != null ){
			sql+=" AND system = ?";
			parameter.add(system);
		}
		sql+=" ORDER BY successfulHit ";
		list = find(sql,parameter.toArray());
		return list;
	}

}
