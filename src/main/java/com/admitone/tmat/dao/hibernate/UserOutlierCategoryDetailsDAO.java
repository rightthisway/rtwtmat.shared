package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.data.UserOutlierCategoryDetails;

public class UserOutlierCategoryDetailsDAO extends HibernateDAO<Integer, UserOutlierCategoryDetails> implements com.admitone.tmat.dao.UserOutlierCategoryDetailsDAO {
	
	public Collection<UserOutlierCategoryDetails> getAllOutlierCategoryPriceDetailsByOutlierId(Integer outlierId){
		Collection<Object> parameters = new ArrayList<Object>();
		String query = "FROM UserOutlierCategoryDetails WHERE outlierId=?";
		parameters.add(outlierId);
		return find(query, parameters.toArray(new Object[parameters.size()]));
	}
	
	public void deleteAllOutlierCatPriceDtlByOutlierID(Integer outlierId){
		
		bulkUpdate("DELETE FROM UserOutlierCategoryDetails WHERE outlierId=?", new Object[]{outlierId});
	}
	
}
