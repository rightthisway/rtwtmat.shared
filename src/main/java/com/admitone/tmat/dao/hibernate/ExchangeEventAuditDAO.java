package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.ExchangeEventAudit;

public class ExchangeEventAuditDAO extends HibernateDAO<Integer,ExchangeEventAudit> implements com.admitone.tmat.dao.ExchangeEventAuditDAO{


	public List<ExchangeEventAudit> getExchangeEventAuditByEventId(Integer eId) {
			
			Session session = getSession();
			List<ExchangeEventAudit> list = new ArrayList<ExchangeEventAudit>();
			try{
				Query query = session.createQuery("FROM ExchangeEventAudit where event.id = :eIds order by createdDate");
				query.setParameter("eIds", eId);
				list= query.list();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				session.close();
			}				
			return list;
		}
	public void saveBulkAudits(List<ExchangeEventAudit> auditList) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (ExchangeEventAudit audit : auditList) {
			    session.save(audit);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
}
