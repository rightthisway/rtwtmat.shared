package com.admitone.tmat.dao.hibernate;

import com.admitone.tmat.data.EbayInventoryEvent;

public class EbayInventoryEventDAO extends HibernateDAO<Integer, EbayInventoryEvent> implements com.admitone.tmat.dao.EbayInventoryEventDAO {
	/*public void delete(EbayInventoryEvent ebayInventoryEvent) {
//		DAORegistry.getEbayInventoryGroupDAO().deleteByEventId(ebayInventoryEvent.getEventId());
		DAORegistry.getEbayInventoryGroupDAO().markAsDeletedByEventId(ebayInventoryEvent.getEventId());
		super.delete(ebayInventoryEvent);

		Event event = ebayInventoryEvent.getEvent();
		if (getAllEventsByArtistIdWithEbayInventoryEvents(event.getArtistId()).isEmpty()) {
			EbayInventoryTour ebayInventoryTour = DAORegistry.getEbayInventoryTourDAO().get(event.getArtistId());
			DAORegistry.getEbayInventoryTourDAO().delete(ebayInventoryTour);
		}
	}*/

	// FIXME: dirty trick otherwise it throws
	// an exception if it cannot load the ebay inventory event (to investigate)
	/*public void deleteById(Integer eventId) {
		try {
			List<EbayInventoryEvent> list = find("FROM EbayInventoryEvent where id=?", new Object[]{eventId});
			if (list == null || list.isEmpty()) {
				return;
			}
			EbayInventoryEvent ebayInventoryEvent = list.iterator().next();
			if (ebayInventoryEvent == null) {
				return;
			}
			delete(ebayInventoryEvent);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public Collection<EbayInventoryEvent> getAllEbayInventoryEventsByArtistId(Integer artistId) {
		return find("SELECT ee FROM Event e, EbayInventoryEvent ee WHERE ee.id=e.id AND e.artistId=? ORDER BY e.localDate, e.localTime ASC", new Object[]{artistId});		
	}

	public Collection<EbayInventoryEvent> getAllEbayInventoryEventsByArtistIdAndUsername(Integer artistId, String username) {
		return find("SELECT ee FROM Event e, EbayInventoryEvent ee WHERE ee.id=e.id AND e.artistId=? AND ee.username=? ORDER BY e.localDate, e.localTime ASC", new Object[]{artistId, username});		
	}

	public Collection<Event> getAllEventsByArtistIdWithEbayInventoryEvents(Integer artistId) {
		return find("SELECT e FROM Event e, EbayInventoryEvent ee WHERE ee.id=e.id AND e.artistId=? ORDER BY e.localDate, e.localTime ASC", new Object[]{artistId});		
	}

	public Collection<Event> getAllEventsByArtistIdWithEbayInventoryEventsByUsername(Integer artistId, String username) {
		return find("SELECT e FROM Event e, EbayInventoryEvent ee WHERE ee.id=e.id AND e.artistId=? AND ee.username=? ORDER BY e.localDate, e.localTime ASC", new Object[]{artistId, username});		
	}

	public Collection<EbayInventoryEvent> getAllEbayInventoryEventsByCreator(String username) {
		return find("FROM EbayInventoryEvent WHERE username=?", new Object[]{username});
	}
	
	// FIXME: need to name like that so that we don't get confuse with the other delete method
	public void realDeleteByEventId(Integer eventId) {
		// The current structure is as follow:
		
		// - ebay_inventory_event
		//       - ebay_inventory_group
		//              - ebay_inventory
		
		// first we need to get all the groups associated to the event id
		Collection<EbayInventoryGroup> inventoryGroups = DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByEventId(eventId);
		if (inventoryGroups != null) {
			for(EbayInventoryGroup inventoryGroup: inventoryGroups) {
				DAORegistry.getEbayInventoryDAO().deleteByGroupId(inventoryGroup.getId());
				DAORegistry.getEbayInventoryGroupDAO().deleteById(inventoryGroup.getId());
			}
		}
		bulkUpdate("DELETE FROM EbayInventoryEvent WHERE id=?", new Object[]{eventId});
	}

	public EbayInventoryEvent getEbayInventoryEventByEventId(Integer eventId) {
		return findSingle("FROM EbayInventoryEvent where id=?", new Object[]{eventId});
	}*/	
	
}