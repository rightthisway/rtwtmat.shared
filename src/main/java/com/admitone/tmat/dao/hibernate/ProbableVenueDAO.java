package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.ProbableVenue;

public class ProbableVenueDAO extends HibernateDAO<String, ProbableVenue> implements com.admitone.tmat.dao.ProbableVenueDAO {

	public List<ProbableVenue> getSynonymByVenueId(Integer venueId){
		return find("FROM ProbableVenue WHERE venueId=?", new Object[]{venueId});
	}
	
	public List<ProbableVenue> getAllAddedVenueSynonymsByMarkedAsSynonym(Boolean markedAsSynonym){
		return find("FROM ProbableVenue WHERE markedAsSynonym=?", new Object[]{markedAsSynonym});
	}
	
	public List<ProbableVenue> getAllAddedVenueSynonymsByVenue(Boolean markedAsSynonym, Integer venueId){
		return find("FROM ProbableVenue WHERE markedAsSynonym=? and venueId=?", new Object[]{markedAsSynonym,venueId});
	}
	
}
