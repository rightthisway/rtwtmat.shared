package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Quote;
import com.admitone.tmat.data.QuoteCustomer;


public class QuoteCustomerDAO extends HibernateDAO<Integer, QuoteCustomer> implements com.admitone.tmat.dao.QuoteCustomerDAO {
	public QuoteCustomer getCustomerByName(String name) {
		return findSingle("FROM QuoteCustomer WHERE name=?", new Object[]{name});
	}
	
	public QuoteCustomer getCustomerByNameandBrokerId(String name,Integer brokerId) {
		return findSingle("select qc FROM QuoteCustomer qc,User u WHERE qc.creator=u.username and qc.name=? and u.brokerId=?", new Object[]{name,brokerId});
	}
	
	public Collection<QuoteCustomer> getAllCustomersByBrokerId(Integer brokerId) {
		return find("select qc FROM QuoteCustomer qc,User u WHERE qc.creator=u.username and u.brokerId=?", new Object[]{brokerId});
	}

	public Collection<QuoteCustomer> getCustomersByUsername(String username) {
		Map<Integer, Quote> map = new HashMap<Integer, Quote>();
		
		for (Quote quote: (Collection<Quote>)getHibernateTemplate().find("FROM Quote WHERE username=? ORDER BY creationDate ASC", new Object[]{username})) {
			map.put(quote.getCustomer().getId(), quote);
		}
		
		List<Quote> sortedQuotes = new ArrayList<Quote>(map.values());
		java.util.Collections.sort(sortedQuotes, new Comparator<Quote>() {
			
			public int compare(Quote q1, Quote q2) {
				return -q1.getCreationDate().compareTo(q2.getCreationDate());
			}
		});
		
		Collection<QuoteCustomer> result = new ArrayList<QuoteCustomer>();
		for (Quote quote: sortedQuotes) {
			result.add(quote.getCustomer());
		}

		return result;
	}
	
	public Collection<String> getCreators() {
		return find("SELECT DISTINCT(creator) FROM QuoteCustomer");
	}

	public Collection<QuoteCustomer> getCustomersByCreator(String creator) {
		return find("FROM QuoteCustomer WHERE creator=?", new Object[]{creator});
	}

	public void deleteById(Integer id) {
		DAORegistry.getQuoteDAO().deleteByCustomer(id);
		super.deleteById(id);
	}
}
