package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.data.MMEventMap;

public class MMEventMapDAO extends HibernateDAO<Integer, MMEventMap> implements com.admitone.tmat.dao.MMEventMapDAO {
	public Collection<MMEventMap> getMapsByUsername(String username) {
		Collection<MMEventMap> list = find("FROM MMEventMap WHERE username=?", new Object[]{username});
		if (list.size() == 0) {
			return null;
		}
		return list;
	}	
	public Collection<MMEventMap> getMapsByEventId(Integer eventId) {
		Collection<MMEventMap> list = find("FROM MMEventMap WHERE eventId=?", new Object[]{eventId});
		if (list.size() == 0) {
			return null;
		}
		return list;
	}	
	public Collection<Integer> getEventIdsByUsername(String username) {
		Collection<MMEventMap> list = find("FROM MMEventMap WHERE username=?", new Object[]{username});
		if (list.size() == 0) {
			return null;
		}
		Collection<Integer> returnList = new ArrayList<Integer>();
		for(MMEventMap map : list){
			returnList.add(map.getEventId());
		}
		return returnList;
	}
	public Collection<String> getUsernamesByEventId(Integer eventId) {
		Collection<MMEventMap> list = find("FROM MMEventMap WHERE eventId=?", new Object[]{eventId});
		if (list.size() == 0) {
			return null;
		}
		Collection<String> returnList = new ArrayList<String>();
		for(MMEventMap map : list){
			returnList.add(map.getUsername());
		}
		return returnList;
	}
	public MMEventMap getMap(String username, Integer eventId){
		Collection<MMEventMap> list = find("FROM MMEventMap WHERE username=? AND eventId=?", new Object[]{username, eventId});
		if (list.size() == 0) {
			return null;
		}
		return list.iterator().next();
	}

	public void deleteMapsByEventId(Integer eventId) {
		bulkUpdate("DELETE FROM MMEventMap WHERE eventId=?", new Object[]{eventId});
	}
	
}
