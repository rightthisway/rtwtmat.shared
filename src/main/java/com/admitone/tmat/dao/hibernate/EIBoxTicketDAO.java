package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.EIBoxTicket;

public class EIBoxTicketDAO extends HibernateDAO<Integer, EIBoxTicket> implements com.admitone.tmat.dao.EIBoxTicketDAO {
	public Collection<EIBoxTicket> getEIBoxTicketsByEventId(Integer eventId, Integer venueId, Date eventDate) {
		return find("FROM EIBoxTicket WHERE eventId=? AND venueId=? AND eventDate=?", new Object[]{eventId, venueId, eventDate});
	}
}
