package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.TGCatsCategoryTicket;

public class TGCatsCategoryTicketDAO extends HibernateDAO<Integer, TGCatsCategoryTicket> implements com.admitone.tmat.dao.TGCatsCategoryTicketDAO {

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<TGCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<TGCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<TGCatsCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<TGCatsCategoryTicket> getAllActiveTgCatsCategoryTickets() {
		return find("FROM TGCatsCategoryTicket where status ='ACTIVE'");
	}
	
	public List<TGCatsCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity) {
		String query = "FROM TGCatsCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(quantity);								
		
			} 
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND (rowRange=? OR lastRow=?)";
			param.add(row);
			param.add(row);
		}
		
		
		return find(query, param.toArray());
	}
	
	
	public List<TGCatsCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id) {
		return find("FROM TGCatsCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM TGCatsCategoryTicket where id=?" ,new Object[]{Id});
	}
	
}
