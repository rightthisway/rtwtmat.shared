package com.admitone.tmat.dao.hibernate;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.VipAutoCatsError;

public class VipAutoCatsErrorDAO extends HibernateDAO<Integer, VipAutoCatsError> implements com.admitone.tmat.dao.VipAutoCatsErrorDAO {

public Collection<VipAutoCatsError> getAllVipAutoCatsErrorByDate(Date date){
DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
DateFormat dft = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
try {
	Date startDate = dft.parse(df.format(date)+ " 00:00:00");
	Date endDate = dft.parse(df.format(date)+ " 23:59:59");
	return find("FROM VipAutoCatsError WHERE timeStamp >= ? and timeStamp <= ? ", new Object[]{startDate,endDate});
//	return find("FROM VipAutoCatsError");
} catch (ParseException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
return null;
}

}