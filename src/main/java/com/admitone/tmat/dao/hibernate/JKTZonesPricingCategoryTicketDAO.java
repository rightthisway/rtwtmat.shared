package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.admitone.tmat.data.JKTZonesPricingCategoryTicket;
import com.admitone.tmat.data.ZonesPricingCategoryTicket;

public class JKTZonesPricingCategoryTicketDAO extends HibernateDAO<Integer, JKTZonesPricingCategoryTicket> implements com.admitone.tmat.dao.JKTZonesPricingCategoryTicketDAO {


	public List<ZonesPricingCategoryTicket> getAllActiveZonesPricingCategoryTickets() {
		return find("FROM ZonesPricingCategoryTicket where status ='ACTIVE'");
	}
	
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketByAll(Integer eventId,String section,String row,String quantity) {
		String query = "FROM ZonesPricingCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty()) {
			query += " AND quantity=?";
			param.add(quantity);								
		
		} 
		
		
		return find(query, param.toArray());
	}
	
	public List<JKTZonesPricingCategoryTicket> getAllJktZonesPricingCategoryTicketByAll(Integer eventId,String section,String row,String quantity) {
		String query = "FROM JKTZonesPricingCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (quantity !=null && !quantity.isEmpty()) {
			query += " AND quantity=?";
			param.add(quantity);								
		
		} 
		
		
		return find(query, param.toArray());
	}
	
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketById(Integer Id) {
		return find("FROM ZonesPricingCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM ZonesPricingCategoryTicket where id=?" ,new Object[]{Id});
	}
	
	
	public List<String> getDistinctZonesByTnExchangeEventId(Integer tnExchangeEventId) {
		return find(" select distinct section FROM JKTZonesPricingCategoryTicket where tnExchangeEventId=?" ,new Object[]{tnExchangeEventId});
	}
	
	public List<String> getAllDistinctVenues() {
		List<String> venues= new ArrayList<String>();
		Session session = getSession();
		try{
			SQLQuery query = session.createSQLQuery("SELECT distinct ae.venueName as VenueName FROM zones_pricing_category_ticket_jkt zpzt " +
					"inner join admitone_event ae on ae.eventId = zpzt.tn_exchange_event_id order by VenueName");
			List<String> list = query.list();
			venues.addAll(list);
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			releaseSession(session);
		}
		return venues;
	}
	
	
	public List<JKTZonesPricingCategoryTicket> getAllActiveZonesByEventIds(ArrayList<Integer> eventIds){
		int i =0;
		int fromIndex=0;
		List<JKTZonesPricingCategoryTicket> result = new ArrayList<JKTZonesPricingCategoryTicket>();
	
		Map<Integer, List<Integer>> eventIdsMap = new HashMap<Integer, List<Integer>>();
		for(int j =0 ;j<=eventIds.size();j=j+2000){
			if(eventIds.size()<2000){
				eventIdsMap.put(i,eventIds.subList(fromIndex,eventIds.size()));
			}
			else{
				if(eventIds.size()>(fromIndex)){
					if(eventIds.size()>=(fromIndex+2000)){
						eventIdsMap.put(i,eventIds.subList(fromIndex, fromIndex+2000));
					}else{
						eventIdsMap.put(i,eventIds.subList(fromIndex,eventIds.size()));
					}
						fromIndex += 2000;  
					
				}
					i++;
			}
		}
		Session session  = getSession();
		try{
			for(int k=0;k<eventIdsMap.size();k++){
				
				List<Integer> ids = eventIdsMap.get(k);					
				Query query = session.createQuery("FROM JKTZonesPricingCategoryTicket where tnExchangeEventId in (:Ids) ").setParameterList("Ids", ids);
				try{
					@SuppressWarnings("unchecked")
					List<JKTZonesPricingCategoryTicket> list= query.list();					
					result.addAll(list);						
				}catch (Exception e) {
					e.printStackTrace();
				}finally{
					query=null;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			session.close();
		}
	  return result;
	}
}
