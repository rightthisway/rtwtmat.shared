package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.NewPOSEvents;

public class NewPOSEventDAO extends HibernateDAO<Integer, NewPOSEvents> implements com.admitone.tmat.dao.NewPOSEventDAO {

	public Collection<NewPOSEvents> getAllNewPOSEvents() {
		return find("FROM NewPOSEvents ORDER BY name,date");
	}
	public Collection<NewPOSEvents> getTodayNewPOSEvents() {
	try {
		return find("FROM NewPOSEvents where created_date >= CONVERT(DATE,GETDATE()) order by created_date desc");
	} catch (Exception e) {
		e.printStackTrace();
	}
	return null;
	}
	public List<NewPOSEvents> getNewPOSEventsByDateStr(String dateTimeParm) {
		try {
		
		System.out.println("hours......."+dateTimeParm);
		
		return find("FROM NewPOSEvents where created_date >= '"+dateTimeParm+"' order by created_date desc");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
