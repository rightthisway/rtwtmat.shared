package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.ValuationFactor;

public class ValuationFactorDAO extends HibernateDAO<Integer, ValuationFactor> implements com.admitone.tmat.dao.ValuationFactorDAO {
	public Collection<ValuationFactor> getAllValuationFactorsFromEvent(Integer eventId) {
		return find("FROM ValuationFactor WHERE event_id=?", new Object[]{eventId});
	}
	
	/*public Collection<ValuationFactor> getAllValuationFactorsFromTour(Integer tourId) {
		return find("SELECT v FROM ValuationFactor v, Event e WHERE e.id=v.eventId AND e.tourId=?", new Object[]{tourId});
	}*/
	
	public Collection<ValuationFactor> getAllValuationFactorsFromArtist(Integer artistId) {
		return find("SELECT v FROM ValuationFactor v, Event e WHERE e.id=v.eventId AND e.artistId=?", new Object[]{artistId});
	}
}
