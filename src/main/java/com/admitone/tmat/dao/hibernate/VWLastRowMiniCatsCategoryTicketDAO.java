package com.admitone.tmat.dao.hibernate;


import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.VWLastRowMiniCatsCategoryTicket;

public class VWLastRowMiniCatsCategoryTicketDAO extends HibernateDAO<Integer, VWLastRowMiniCatsCategoryTicket> implements com.admitone.tmat.dao.VWLastRowMiniCatsCategoryTicketDAO {

	public VWLastRowMiniCatsCategoryTicket get(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<VWLastRowMiniCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public Collection<VWLastRowMiniCatsCategoryTicket> getAll() {
		return find("FROM VWLastRowMiniCatsCategoryTicket");
	}

	public void saveOrUpdateAll(Collection<VWLastRowMiniCatsCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<VWLastRowMiniCatsCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<VWLastRowMiniCatsCategoryTicket> getAllActiveTnLastRowMiniCatsCatsCategoryTickets() {
		return find("FROM VWLastRowMiniCatsCategoryTicket where tn_category_ticket_group_id is not null and tn_category_ticket_group_id !=0 and status='ACTIVE'");
	}
}
