package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.CategoryTicket;
import com.admitone.tmat.data.LarryLastCategoryTicket;

public class LarryLastCategoryTicketDAO extends HibernateDAO<Integer, LarryLastCategoryTicket> implements com.admitone.tmat.dao.LarryLastCategoryTicketDAO {


	public void saveOrUpdate(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public void saveAll(Collection<LarryLastCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void saveOrUpdateAll(Collection<LarryLastCategoryTicket> list) {
		// TODO Auto-generated method stub

	}

	public void deleteAll(Collection<LarryLastCategoryTicket> List) {
		// TODO Auto-generated method stub

	}

	public void delete(CategoryTicket entity) {
		// TODO Auto-generated method stub

	}

	public List<LarryLastCategoryTicket> getAllActiveLarryLastCategoryTickets() {
		return find("FROM LarryLastCategoryTicket where status ='ACTIVE'");
	}
	
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByAll(Integer eventId, String section, String row,String quantity) {
		String query = "FROM LarryLastCategoryTicket";
		List<Object> param = new ArrayList<Object>();
		
		query +=" WHERE eventId=?";
		param.add(eventId);
		
		if (section !=null && !section.isEmpty())
		{
			query += " AND section=?";
			param.add(section);
		}
		
		if (row !=null && !row.isEmpty())
		{
			query += " AND lastRow=?";
			param.add(row);
		}
		if (quantity !=null && !quantity.isEmpty())
			{
			query += " AND quantity=?";
			param.add(quantity);								
		
			}
		
		
		return find(query, param.toArray());
	}
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsById(Integer Id) {
		return find("FROM LarryLastCategoryTicket where id=?" ,new Object[]{Id});
	}
	public List<String> getPriceHistoryById(Integer Id) {
		return find(" select priceHistory FROM LarryLastCategoryTicket where id=?" ,new Object[]{Id});
	}
	
}
