package com.admitone.tmat.dao.hibernate;

import java.util.List;

import com.admitone.tmat.data.TMATDependentArtistMerge;

public class TMATDependentTourMergeDAO extends HibernateDAO<Integer, TMATDependentArtistMerge> implements
		com.admitone.tmat.dao.TMATDependentTourMergeDAO {
	
	
	public List<TMATDependentArtistMerge> getTMATDependentTourMergeByTourIdAndMergeTourId(Integer tourId, Integer toMergedTourId){
		return find("FROM TMATDependentTourMerge WHERE tourId= ? AND toMergedTourId = ?  ",new Object[]{tourId,toMergedTourId});
	}

}
