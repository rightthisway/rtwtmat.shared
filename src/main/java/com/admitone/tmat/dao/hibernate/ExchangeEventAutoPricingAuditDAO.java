package com.admitone.tmat.dao.hibernate;


import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.admitone.tmat.data.ExchangeEventAutoPricingAudit;

public class ExchangeEventAutoPricingAuditDAO extends HibernateDAO<Integer,ExchangeEventAutoPricingAudit> implements com.admitone.tmat.dao.ExchangeEventAutoPricingAuditDAO{


	public List<ExchangeEventAutoPricingAudit> getExchangeEventAutoPricingAuditByEventId(Integer eId) {
			
			Session session = getSession();
			List<ExchangeEventAutoPricingAudit> list = new ArrayList<ExchangeEventAutoPricingAudit>();
			try{
				Query query = session.createQuery("FROM ExchangeEventAutoPricingAudit where event.id = :eIds order by createdDate");
				query.setParameter("eIds", eId);
				list= query.list();
			}catch (Exception e) {
				e.printStackTrace();
			}finally{
				session.close();
			}				
			return list;
		}
	public void saveBulkAudits(List<ExchangeEventAutoPricingAudit> auditList) throws Exception {
		
		Session session = null;
		Transaction tx = null;
		try{
			session = getSessionFactory().openSession();
			tx = session.beginTransaction();
			
			int i= 0;
			for (ExchangeEventAutoPricingAudit audit : auditList) {
			    session.save(audit);
			    i++;
			    if ( i % 50 == 0 ) { 
			        session.flush();
			        session.clear();
			    }
			}
			
		}catch(Exception e){
			tx.rollback();
			throw e;
		}finally{
			tx.commit();
			session.close();
		}
	}
}
