package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.admitone.tmat.data.Ticket;

public class EITicketsQueryManager {
private SessionFactory sessionFactory;
	
	
		public Collection<Ticket> getEITickets(Integer eventId) {
		
		String sqlQueryString = 
			"SELECT "
			+ "    e.name, "
			+ "    v.building, "
			+ "    e.event_date, "
			+ "    e.event_time, "
			+ "    t.remaining_quantity, "
			+ "    t.normalized_section, "
			+ "    t.row, "
			+ "    t.seat, "
			+ "    t.current_price, "
			+ "    t.id "
			+ "FROM ticket t "
			+ "INNER JOIN event e ON t.event_id=e.id "
			+ "INNER JOIN venue v ON v.id=e.venue_id "
			+ "WHERE "
			+ "t.ticket_status='active' "
			+"AND "
			+"t.seller IN ('OE', 'AM', 'Fan Inventory','FAN')";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		
/*	if (eventIds != null && eventIds.length > 0) {
			String eventIdsString = "";
			for(Integer eventId: eventIds) {
				if (!eventIdsString.isEmpty()) {
					eventIdsString += ",";
				}
				eventIdsString += eventId;
			}
			sqlQueryString += " AND t.event_id IN (" + eventIdsString + ")";
		}

*/
		sqlQueryString += " AND t.event_id=" + eventId;
		
		Session session = null;
		Collection<Ticket> eiTickets = new ArrayList<Ticket>();
		
		try {
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sqlQueryString);
			sqlQuery.setProperties(parameters);
			
			// build object from result
			for(Object[] resultRow: (List<Object[]>)sqlQuery.list()) {
	
				Ticket eiTicket = new Ticket();
				eiTicket.setEventId(eventId);
				eiTicket.setEventName((String)resultRow[0]);
				eiTicket.setVenue((String)resultRow[1]);
				eiTicket.setEventDate((Date)resultRow[2]);
				eiTicket.setEventTime((Date)resultRow[3]);
				eiTicket.setRemainingQuantity((Integer)resultRow[4]);
				eiTicket.setNormalizedSection((String)resultRow[5]);
				eiTicket.setRow((String)resultRow[6]);
				eiTicket.setSeat((String)resultRow[7]);
				eiTicket.setCurrentPrice((Double) resultRow[8]);
				eiTicket.setId((Integer)resultRow[9]);
				eiTickets.add(eiTicket);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {		
			session.close();
		}
		return eiTickets;
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}		
}
