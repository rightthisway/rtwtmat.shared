package com.admitone.tmat.dao.hibernate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.admitone.tmat.data.Ticket;

public class MercuryTicketsQueryManager {
private SessionFactory sessionFactory;


public HashMap<Integer,Collection<Ticket>> getAllMercuryTickets() {
	HashMap<Integer,Collection<Ticket>> mercuryTicketsByEventId = new HashMap<Integer,Collection<Ticket>>();
	String sqlQueryString = 
		"SELECT "
		+ "    e.name, "
		+ "    v.building, "
		+ "    e.event_date, "
		+ "    e.event_time, "
		+ "    t.remaining_quantity, "
		+ "    t.normalized_section, "
		+ "    t.row, "
		+ "    t.seat, "
		+ "    t.current_price, "
		+ "    t.id ticket_id, "
		+ "    e.id event_id "
		+ "FROM ticket t "
		+ "INNER JOIN event e ON t.event_id=e.id "
		+ "INNER JOIN venue v ON v.id=e.venue_id "
		+ "WHERE "
		+ "t.ticket_status='active' "
		+"AND "
		+"t.ticket_delivery_type = 'MERCURYPOS'";
	
	Map<String, Object> parameters = new HashMap<String, Object>();
	
	
/*	if (eventIds != null && eventIds.length > 0) {
		String eventIdsString = "";
		for(Integer eventId: eventIds) {
			if (!eventIdsString.isEmpty()) {
				eventIdsString += ",";
			}
			eventIdsString += eventId;
		}
		sqlQueryString += " AND t.event_id IN (" + eventIdsString + ")";
	}

*/
//	System.out.println("MercurytixQUERY=" + sqlQueryString);
	
	Session session = null;
//	Collection<MercuryTickets> mercuryTickets = new ArrayList<MercuryTickets>();
	try {
		session = sessionFactory.openSession();
		SQLQuery sqlQuery = session.createSQLQuery(sqlQueryString);
		sqlQuery.setProperties(parameters);
		
		// build object from result
		for(Object[] resultRow: (List<Object[]>)sqlQuery.list()) {
		//	MercuryTickets mercuryTicket = new MercuryTickets();
			Ticket mercuryTicket = new Ticket();
	
			mercuryTicket.setEventName((String)resultRow[0]);
			mercuryTicket.setVenue((String)resultRow[1]);
			mercuryTicket.setEventDate((Date)resultRow[2]);
			mercuryTicket.setEventTime((Date)resultRow[3]);
			mercuryTicket.setRemainingQuantity((Integer)resultRow[4]);
			mercuryTicket.setNormalizedSection((String)resultRow[5]);
			mercuryTicket.setRow((String)resultRow[6]);
			mercuryTicket.setSeat((String)resultRow[7]);
			mercuryTicket.setCurrentPrice((Double) resultRow[8]);
			mercuryTicket.setId((Integer)resultRow[9]);
			mercuryTicket.setEventId((Integer)resultRow[10]);
			
			Integer eventId = (Integer)resultRow[10];
			//System.out.println("Inserting into mercuryTicketsByEventId for event: " + eventId);
			Collection<Ticket> mercuryTickets = mercuryTicketsByEventId.get(eventId);
			if(mercuryTickets == null){
				mercuryTickets = new ArrayList<Ticket>();
			}
			mercuryTickets.add(mercuryTicket);
			mercuryTicketsByEventId.put(eventId, mercuryTickets);
			
			
			
		}
	} catch (Throwable t) {
		t.printStackTrace();
	} finally {		
		session.close();
	}
	//System.out.println("Size of mercuryTicketsByEventId : " + mercuryTicketsByEventId.size());
	return mercuryTicketsByEventId;
}


//	public Collection<MercuryTickets> getMercuryTickets(Integer eventId) {
public Collection<Ticket> getMercuryTickets(Integer eventId) {
		
		String sqlQueryString = 
			"SELECT "
			+ "    e.name, "
			+ "    v.building, "
			+ "    e.event_date, "
			+ "    e.event_time, "
			+ "    t.remaining_quantity, "
			+ "    t.normalized_section, "
			+ "    t.row, "
			+ "    t.seat, "
			+ "    t.current_price, "
			+ "    t.id "
			+ "FROM ticket t "
			+ "INNER JOIN event e ON t.event_id=e.id "
			+ "INNER JOIN venue v ON v.id=e.venue_id "
			+ "WHERE "
			+ "t.ticket_status='active' "
			+"AND "
			+"t.ticket_delivery_type = 'MERCURYPOS'";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		
		
/*	if (eventIds != null && eventIds.length > 0) {
			String eventIdsString = "";
			for(Integer eventId: eventIds) {
				if (!eventIdsString.isEmpty()) {
					eventIdsString += ",";
				}
				eventIdsString += eventId;
			}
			sqlQueryString += " AND t.event_id IN (" + eventIdsString + ")";
		}

*/
		sqlQueryString += " AND t.event_id=" + eventId;
//		System.out.println("MercurytixQUERY=" + sqlQueryString);
		
		Session session = null;
	//	Collection<MercuryTickets> mercuryTickets = new ArrayList<MercuryTickets>();
		Collection<Ticket> mercuryTickets = new ArrayList<Ticket>();
		
		try {
			session = sessionFactory.openSession();
			SQLQuery sqlQuery = session.createSQLQuery(sqlQueryString);
			sqlQuery.setProperties(parameters);
			
			// build object from result
			for(Object[] resultRow: (List<Object[]>)sqlQuery.list()) {
			//	MercuryTickets mercuryTicket = new MercuryTickets();
				Ticket mercuryTicket = new Ticket();
		
				mercuryTicket.setEventId(eventId);
				mercuryTicket.setEventName((String)resultRow[0]);
				mercuryTicket.setVenue((String)resultRow[1]);
				mercuryTicket.setEventDate((Date)resultRow[2]);
				mercuryTicket.setEventTime((Date)resultRow[3]);
				mercuryTicket.setRemainingQuantity((Integer)resultRow[4]);
				mercuryTicket.setNormalizedSection((String)resultRow[5]);
				mercuryTicket.setRow((String)resultRow[6]);
				mercuryTicket.setSeat((String)resultRow[7]);
				mercuryTicket.setCurrentPrice((Double) resultRow[8]);
				mercuryTicket.setId((Integer)resultRow[9]);
				mercuryTickets.add(mercuryTicket);
			}
		} catch (Throwable t) {
			t.printStackTrace();
		} finally {		
			session.close();
		}
		return mercuryTickets;
	}


public Collection<Ticket> getScrubedMercuryTickets() {
	
//	String sqlQueryString ="";
	Session session = null;
	try{
		Ticket ticket ;
		String sql ="SELECT * FROM (";
		sql+=" SELECT distinct"; 
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid"; 
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
	//	sql+=" and t.ticket_delivery_type = 'instant'";
		sql+=" and t.seller = 'stubhub'";
		sql+=" and event_status = 'ACTIVE'";
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=0 then 0";
		sql+=" when isnumeric(cm.start_section)=0 then 0";
		sql+=" when isnumeric(cm.end_section)=0 then 0";
		sql+=" when cast(t.normalized_section as int) between cast(cm.start_section as int) and cast(cm.end_section as int) then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=0 then 0";
		sql+=" when isnumeric(cm.start_row)=0 then 0";
		sql+=" when isnumeric(cm.end_row)=0 then 0";
		sql+=" when cast(t.row as int) between cast(cm.start_row as int) and cast(cm.end_row as int) then 1";
		sql+=" end =1";
		sql+=" union";
		sql+=" SELECT distinct";
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid ";
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
	//	sql+=" and t.ticket_delivery_type = 'instant'";
		sql+=" and t.seller = 'stubhub'";
		sql+=" and event_status = 'ACTIVE'";
			
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=1 then 0";
		sql+=" when isnumeric(cm.start_section)=1 then 0";
		sql+=" when isnumeric(cm.end_section)=1 then 0";
		sql+=" when len(t.normalized_section) <> len(cm.start_section) and len(t.normalized_section) <> len(cm.end_section) then 0";
		sql+=" when t.normalized_section between cm.start_section and cm.end_section then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=1 then 0";
		sql+=" when isnumeric(cm.start_row)=1 then 0";
		sql+=" when isnumeric(cm.end_row)=1 then 0";
		sql+=" when len(t.row) <> len(cm.start_row) and len(t.row) <> len(cm.end_row) then 0";
		sql+=" when t.row between cm.start_row and cm.end_row then 1";
		sql+=" end =1";
		sql+=" union";
		sql+=" SELECT distinct"; 
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid ";
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
	//	sql+=" and t.ticket_delivery_type = 'instant'";
		sql+=" and t.seller = 'stubhub'";
		sql+=" and event_status = 'ACTIVE'";
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=1 then 0";
		sql+=" when isnumeric(cm.start_section)=1 then 0";
		sql+=" when isnumeric(cm.end_section)=1 then 0";
		sql+=" when len(t.normalized_section) <> len(cm.start_section) and len(t.normalized_section) <> len(cm.end_section) then 0";
		sql+=" when t.normalized_section between cm.start_section and cm.end_section then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=0 then 0";
		sql+=" when isnumeric(cm.start_row)=0 then 0";
		sql+=" when isnumeric(cm.end_row)=0 then 0";
		sql+=" when cast(t.row as int) between cast(cm.start_row as int) and cast(cm.end_row as int) then 1";
		sql+=" end =1";
		sql+=" union";
	
		sql+=" SELECT distinct"; 
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid ";
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
	//	sql+=" and t.ticket_delivery_type = 'instant'";
		sql+=" and t.seller = 'stubhub'";
		sql+=" and event_status = 'ACTIVE'";
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=0 then 0";
		sql+=" when isnumeric(cm.start_section)=0 then 0";
		sql+=" when isnumeric(cm.end_section)=0 then 0";
		sql+=" when cast(t.normalized_section as int) between cast(cm.start_section as int) and cast(cm.end_section as int) then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=1 then 0";
		sql+=" when isnumeric(cm.start_row)=1 then 0";
		sql+=" when isnumeric(cm.end_row)=1 then 0";
		sql+=" when len(t.row) <> len(cm.start_row) and len(t.row) <> len(cm.end_row) then 0";
		sql+=" when t.row between cm.start_row and cm.end_row then 1";
		sql+=" end =1";
		sql+=" ) abc where eid=100804682";
		session= sessionFactory.openSession();
		SQLQuery sqlQuery2 = session.createSQLQuery(sql);
		
		Map<String, List<Ticket>> map2= new HashMap<String, List<Ticket>>();
		Collection<Ticket> list;
		List<Object[]> objs=(List<Object[]>)sqlQuery2.list();
		for(Object[] resultRow:objs ){
		
			ticket = new Ticket();
			ticket.setEventName((String)resultRow[0]);
			ticket.setVenue((String)resultRow[1]);
			ticket.setEventDate((Date)resultRow[2]);
			ticket.setEventTime((Date)resultRow[3]);
	//		ticket.setEventDate(dateFormat.parse((inFormat.format((Date)resultRow[2]).toString())));
	//		ticket.setEventTime(timeFormat.parse((inFormat.format((Date)resultRow[3]).toString())));
			ticket.setQuantity((Integer)resultRow[4]);
			ticket.setSection((String)resultRow[6]);
			ticket.setRow((String)resultRow[7]);
			if((String)resultRow[8]!=null){
				ticket.setSeat((String)resultRow[8]);
			}
			ticket.setCurrentPrice((Double)resultRow[9]);
			ticket.setId((Integer)resultRow[10]);
			ticket.setEventId((Integer)resultRow[11]);
			String key=ticket.getEventId()+"_"+ticket.getSection()+"_"+ticket.getRow()+"_"+ ticket.getQuantity();
			List<Ticket> tickets=map2.get(key);
			if(tickets==null){
				tickets= new ArrayList<Ticket>();
			}
			tickets.add(ticket);
			map2.put(key, tickets);
		}
//		System.out.println(map2.size());
		sql ="SELECT * FROM (";
		sql+=" SELECT distinct"; 
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid,ie.markup_percent"; 
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
		sql+=" and t.ticket_delivery_type = 'MERCURYPOS'";
		sql+=" and event_status = 'ACTIVE'";
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=0 then 0";
		sql+=" when isnumeric(cm.start_section)=0 then 0";
		sql+=" when isnumeric(cm.end_section)=0 then 0";
		sql+=" when cast(t.normalized_section as int) between cast(cm.start_section as int) and cast(cm.end_section as int) then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=0 then 0";
		sql+=" when isnumeric(cm.start_row)=0 then 0";
		sql+=" when isnumeric(cm.end_row)=0 then 0";
		sql+=" when cast(t.row as int) between cast(cm.start_row as int) and cast(cm.end_row as int) then 1";
		sql+=" end =1";
		sql+=" union";
		sql+=" SELECT distinct";
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid,ie.markup_percent ";
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
		sql+=" and t.ticket_delivery_type = 'MERCURYPOS'";
		sql+=" and event_status = 'ACTIVE'";
			
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=1 then 0";
		sql+=" when isnumeric(cm.start_section)=1 then 0";
		sql+=" when isnumeric(cm.end_section)=1 then 0";
		sql+=" when len(t.normalized_section) <> len(cm.start_section) and len(t.normalized_section) <> len(cm.end_section) then 0";
		sql+=" when t.normalized_section between cm.start_section and cm.end_section then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=1 then 0";
		sql+=" when isnumeric(cm.start_row)=1 then 0";
		sql+=" when isnumeric(cm.end_row)=1 then 0";
		sql+=" when len(t.row) <> len(cm.start_row) and len(t.row) <> len(cm.end_row) then 0";
		sql+=" when t.row between cm.start_row and cm.end_row then 1";
		sql+=" end =1";
		sql+=" union";
		sql+=" SELECT distinct"; 
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid,ie.markup_percent ";
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
		sql+=" and t.ticket_delivery_type = 'MERCURYPOS'";
		sql+=" and event_status = 'ACTIVE'";
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=1 then 0";
		sql+=" when isnumeric(cm.start_section)=1 then 0";
		sql+=" when isnumeric(cm.end_section)=1 then 0";
		sql+=" when len(t.normalized_section) <> len(cm.start_section) and len(t.normalized_section) <> len(cm.end_section) then 0";
		sql+=" when t.normalized_section between cm.start_section and cm.end_section then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=0 then 0";
		sql+=" when isnumeric(cm.start_row)=0 then 0";
		sql+=" when isnumeric(cm.end_row)=0 then 0";
		sql+=" when cast(t.row as int) between cast(cm.start_row as int) and cast(cm.end_row as int) then 1";
		sql+=" end =1";
		sql+=" union";
	
		sql+=" SELECT distinct"; 
		sql+=" e.name,v.building, e.event_date, e.event_time, t.remaining_quantity, t.normalized_section,"; 
		sql+=" t.section,t.row, t.seat, t.current_price, t.id,e.id as eid,ie.markup_percent ";
		sql+=" FROM ticket t ";
		sql+=" INNER JOIN event e ON t.event_id=e.id"; 
		sql+=" INNER JOIN venue v ON v.id=e.venue_id";
		sql+=" inner join category_mapping cm on cm.event_id=e.id";
		sql+=" inner join category c on cm.category_id = c.id";
		sql+=" inner join tninstant_event ie on ie.event_id=e.id";
		sql+=" WHERE ";
		sql+=" t.ticket_status='active'"; 
		sql+=" and t.ticket_delivery_type = 'MERCURYPOS'";
		sql+=" and event_status = 'ACTIVE'";
		sql+=" and datediff(hour,getdate(),event_date) >= ie.time_expiry";
		sql+=" and";
		sql+=" case ";
		sql+=" 	when cm.start_section like '*' and cm.end_section like '*' then 1";
		sql+=" when isnumeric(t.normalized_section)=0 then 0";
		sql+=" when isnumeric(cm.start_section)=0 then 0";
		sql+=" when isnumeric(cm.end_section)=0 then 0";
		sql+=" when cast(t.normalized_section as int) between cast(cm.start_section as int) and cast(cm.end_section as int) then 1";
		sql+=" end =1";
		sql+=" and";
		sql+=" case ";
		sql+=" when cm.start_row like '*' and cm.end_row like '*' then 1";
		sql+=" when isnumeric(t.row)=1 then 0";
		sql+=" when isnumeric(cm.start_row)=1 then 0";
		sql+=" when isnumeric(cm.end_row)=1 then 0";
		sql+=" when len(t.row) <> len(cm.start_row) and len(t.row) <> len(cm.end_row) then 0";
		sql+=" when t.row between cm.start_row and cm.end_row then 1";
		sql+=" end =1";
		sql+=" ) abc where eid=100804682";
		session = sessionFactory.openSession();
		SQLQuery sqlQuery = session.createSQLQuery(sql);
		//sqlQuery.setProperties(parameters);
		
		
		Map<String, List<Ticket>> map= new HashMap<String, List<Ticket>>();
		// build object from result
		for(Object[] resultRow: (List<Object[]>)sqlQuery.list()) {
	
			ticket = new Ticket();
			ticket.setEventName((String)resultRow[0]);
			ticket.setVenue((String)resultRow[1]);
			ticket.setEventDate((Date)resultRow[2]);
			ticket.setEventTime((Date)resultRow[3]);
		//	ticket.setEventDate(dateFormat.parse((inFormat.format((Date)resultRow[2]).toString())));
		//	ticket.setEventTime(timeFormat.parse((inFormat.format((Date)resultRow[3]).toString())));
			ticket.setQuantity((Integer)resultRow[4]);
			ticket.setSection((String)resultRow[6]);
			ticket.setRow((String)resultRow[7]);
			if((String)resultRow[8]!=null){
				ticket.setSeat((String)resultRow[8]);
			}
			ticket.setCurrentPrice((Double)resultRow[9]);
			ticket.setId((Integer)resultRow[10]);
			ticket.setEventId((Integer)resultRow[11]);
			ticket.setBuyItNowPrice(ticket.getCurrentPrice()+(ticket.getCurrentPrice()*(Double)resultRow[12]/100));
			String key=ticket.getEventId()+"_"+ticket.getSection()+"_"+ticket.getRow()+"_"+ ticket.getQuantity();
			List<Ticket> tickets=map.get(key);
			if(tickets==null){
				tickets= new ArrayList<Ticket>();
			}
			tickets.add(ticket);
			map.put(key, tickets);
		}
		
//		System.out.println(map.size());
		Set<String> keys=map.keySet();
		list= new ArrayList<Ticket>();
		for(String tempKey:keys){
			List<Ticket> mapTickets=(map.get(tempKey));
			List<Ticket> map2Tickets=(map2.remove(tempKey));
			if(map2Tickets!= null){
				
				for(Ticket temp: mapTickets){
					boolean flag=true;
					double tempCost = temp.getCurrentPrice();
					for(Ticket map2temp: map2Tickets){
						double tempCost2 = map2temp.getCurrentPrice();
						if(tempCost + (tempCost * 0.2)> tempCost2 ){
							flag=false;
							break;
						}
					}
					if(flag==true){
				    	list.add(temp);
				    }
				}
			}else{
				for(Ticket temp: mapTickets){
					list.add(temp);
				}
			}
		
		}
		map.clear();
		map=null;
		map2.clear();
		map2=null;
	
		return list;
	
	}catch(Exception e){
		e.printStackTrace();
		return null;
	}finally{
		session.close();
	}
	
}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}		
}
