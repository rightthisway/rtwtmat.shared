package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.enums.AlertFor;
import com.admitone.tmat.enums.UserAlertStatus;

public class UserAlertDAO extends HibernateDAO<Integer, UserAlert> implements com.admitone.tmat.dao.UserAlertDAO {
	public Collection<UserAlert> getAlertsByUsername(String username) {
		return find("FROM UserAlert WHERE username=? AND (userAlertStatus = ? OR userAlertStatus = ?)", new Object[]{username, UserAlertStatus.ACTIVE, UserAlertStatus.DISABLED});
	}	
	
	public Collection<UserAlert> getAlertsByMarketMakerAndTourId(String username, Integer artistId, AlertFor alertFor, UserAlertStatus userAlertStatus) {
		String marketMakerQuery = "SELECT a FROM UserAlert a, MarketMakerEventTracking t WHERE a.eventId = t.eventId";
		Collection<Object> marketMakerParameters = new ArrayList<Object>();

		String defaultMarketMakerQuery = "FROM UserAlert a WHERE 1=1";
		Collection<Object> defaultMarketMakerParameters = new ArrayList<Object>();

		if (userAlertStatus != null) {
			marketMakerQuery += " AND a.userAlertStatus=?";	
			marketMakerParameters.add(userAlertStatus);
			defaultMarketMakerQuery += " AND a.userAlertStatus=?";	
			defaultMarketMakerParameters.add(userAlertStatus);
		} else {
			marketMakerQuery += " AND (a.userAlertStatus = ? OR a.userAlertStatus = ?)";
			marketMakerParameters.add(UserAlertStatus.ACTIVE);
			marketMakerParameters.add(UserAlertStatus.DISABLED);
			
			defaultMarketMakerQuery += " AND (a.userAlertStatus = ? OR a.userAlertStatus = ?)";
			defaultMarketMakerParameters.add(UserAlertStatus.ACTIVE);
			defaultMarketMakerParameters.add(UserAlertStatus.DISABLED);
		}

		if (artistId != null) {
			Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			if (events == null || events.isEmpty()) {
				return null;
			}
			
			String eventIds = "";		
			for(Event event: events) {
				if (!eventIds.isEmpty()) {
					eventIds += ",";				
				}
				eventIds += event.getId();
			}
			
			marketMakerQuery += " AND a.eventId IN (" + eventIds + ")";
			defaultMarketMakerQuery += " AND a.eventId IN (" + eventIds + ")";
		}

		if (username != null) {
			marketMakerQuery += " AND t.marketMakerUsername=? AND t.enabled=?";				
			marketMakerParameters.add(username);
			marketMakerParameters.add(true);
		}

		if (alertFor != null) {
			marketMakerQuery += " AND a.alertFor=?";	
			marketMakerParameters.add(alertFor);
			defaultMarketMakerQuery += " AND a.alertFor=?";	
			defaultMarketMakerParameters.add(alertFor);
		}
		
		marketMakerQuery += " order by a.creationDate ";
		

		Collection<UserAlert> userAlerts = find(marketMakerQuery, marketMakerParameters.toArray(new Object[marketMakerParameters.size()]));
		
		Map<Integer, UserAlert> userAlertById = new HashMap<Integer, UserAlert>();
		for(UserAlert userAlert: userAlerts) {
			userAlertById.put(userAlert.getId(), userAlert);
		}

		if (username != null && username.equals("mdesantis")) {			
			defaultMarketMakerQuery += " AND NOT EXISTS (FROM MarketMakerEventTracking t WHERE t.eventId=a.eventId AND t.enabled=?)";
			defaultMarketMakerParameters.add(true);
			defaultMarketMakerQuery += " order by a.creationDate ";
			Collection<UserAlert> defaultUserAlerts = find(defaultMarketMakerQuery, defaultMarketMakerParameters.toArray(new Object[defaultMarketMakerParameters.size()]));
			for(UserAlert userAlert: defaultUserAlerts) {
				userAlertById.put(userAlert.getId(), userAlert);
			}
		} 
		
		return userAlertById.values();
	}


	public Collection<UserAlert> getAlertsByUsernameAndTourId(String username, Integer artistId, AlertFor alertFor, UserAlertStatus userAlertStatus) {
		Collection<Object> parameters = new ArrayList<Object>();
		String query = "FROM UserAlert WHERE 1=1";

		if (userAlertStatus != null) {
			query += " AND userAlertStatus = ?";
			parameters.add(userAlertStatus);
		} else {
			query += " AND (userAlertStatus = ? OR userAlertStatus = ?)";
			parameters.add(UserAlertStatus.ACTIVE);
			parameters.add(UserAlertStatus.DISABLED);			
		}

		if (artistId != null) {
			Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			if (events == null || events.isEmpty()) {
				return null;
			}
			
			String eventIds = "";		
			for(Event event: events) {
				if (!eventIds.isEmpty()) {
					eventIds += ",";				
				}
				eventIds += event.getId();
			}
			
			query += " AND event_id IN (" + eventIds + ")";
		}

		if (username != null) {
			query += " AND username=?";
			parameters.add(username);
		}
		
		if (alertFor != null) {
			query += " AND alertFor=?";				
			parameters.add(alertFor);
		}
		query += " order by creationDate ";

		return find(query, parameters.toArray(new Object[parameters.size()]));
	}
	
	public Collection<UserAlert> getAlertsByTourId(Integer tourId) {
		return getAlertsByUsernameAndTourId(null, tourId, null, null);
	}

	/*public Collection<UserAlert> getAllAlerts() {
		return getAlertsByUsernameAndTourId(null, null, null, null);
	}*/

	public Collection<UserAlert> getAllAlerts(UserAlertStatus alertStatus) {
		return getAlertsByUsernameAndTourId(null, null, null, alertStatus);
	}

	public void expireAlertsByEventId(Integer eventId) {
		bulkUpdate("UPDATE UserAlert SET userAlertStatus=? WHERE eventId=?", new Object[]{UserAlertStatus.EXPIRED, eventId});
	}

	public void deleteAlertsFromUser(String username) {
		bulkUpdate("DELETE FROM UserAlert WHERE username=?", new Object[]{username});
	}	
	
	public Collection<UserAlert> getAlertsByUsernameAndEventId(String username, Integer eventId, AlertFor alertFor, UserAlertStatus userAlertStatus) {
		Collection<Object> parameters = new ArrayList<Object>();
		String query = "FROM UserAlert WHERE 1=1";

		if (userAlertStatus != null) {
			query += " AND userAlertStatus = ?";
			parameters.add(userAlertStatus);
		} else {
			query += " AND (userAlertStatus = ? OR userAlertStatus = ?)";
			parameters.add(UserAlertStatus.ACTIVE);
			parameters.add(UserAlertStatus.DISABLED);			
		}
		
		if (eventId != null) {
			query += " AND eventId=?";
			parameters.add(eventId);
		}
	
		if (username != null) {
			query += " AND username=?";
			parameters.add(username);
		}

		if (alertFor != null) {
			query += " AND alertFor=?";				
			parameters.add(alertFor);
		}	
		
		query += " order by creationDate ";				
			
	
		return find(query, parameters.toArray(new Object[parameters.size()]));
	}

	public Collection<UserAlert> getAlertsByMarketMakerAndEventId(String username, Integer eventId, AlertFor alertFor, UserAlertStatus userAlertStatus) {
		String marketMakerQuery = "SELECT a FROM UserAlert a, MarketMakerEventTracking t WHERE a.eventId = t.eventId";
		Collection<Object> marketMakerParameters = new ArrayList<Object>();

		String defaultMarketMakerQuery = "FROM UserAlert a WHERE 1=1";
		Collection<Object> defaultMarketMakerParameters = new ArrayList<Object>();

		if (userAlertStatus != null) {
			marketMakerQuery += " AND a.userAlertStatus=?";	
			marketMakerParameters.add(userAlertStatus);
			defaultMarketMakerQuery += " AND a.userAlertStatus=?";	
			defaultMarketMakerParameters.add(userAlertStatus);
		} else {
			marketMakerQuery += " AND (a.userAlertStatus = ? OR a.userAlertStatus = ?)";
			marketMakerParameters.add(UserAlertStatus.ACTIVE);
			marketMakerParameters.add(UserAlertStatus.DISABLED);
			
			defaultMarketMakerQuery += " AND (a.userAlertStatus = ? OR a.userAlertStatus = ?)";
			defaultMarketMakerParameters.add(UserAlertStatus.ACTIVE);
			defaultMarketMakerParameters.add(UserAlertStatus.DISABLED);
		}

		
		if (eventId != null) {
			marketMakerQuery += "AND a.eventId=?";
			marketMakerParameters.add(eventId);

			defaultMarketMakerQuery += "AND a.eventId=?";
			defaultMarketMakerParameters.add(eventId);
		}

		if (username != null) {
			marketMakerQuery += "AND t.marketMakerUsername=? AND t.enabled=?";				
			marketMakerParameters.add(username);
			marketMakerParameters.add(true);
		}

		if (alertFor != null) {
			marketMakerQuery += " AND a.alertFor=?";	
			marketMakerParameters.add(alertFor);
			defaultMarketMakerQuery += " AND a.alertFor=?";	
			defaultMarketMakerParameters.add(alertFor);
		}

		marketMakerQuery += " order by a.creationDate ";
		
		
		Collection<UserAlert> userAlerts = find(marketMakerQuery, marketMakerParameters.toArray(new Object[marketMakerParameters.size()]));
		
		Map<Integer, UserAlert> userAlertById = new HashMap<Integer, UserAlert>();
		for(UserAlert userAlert: userAlerts) {
			userAlertById.put(userAlert.getId(), userAlert);
		}

		if (username != null && username.equals("mdesantis")) {			
			defaultMarketMakerQuery += " AND NOT EXISTS (FROM MarketMakerEventTracking t WHERE t.eventId=a.eventId AND t.enabled=?)";
			defaultMarketMakerParameters.add(true);
			
			defaultMarketMakerQuery += " order by a.creationDate ";
			
			Collection<UserAlert> defaultUserAlerts = find(defaultMarketMakerQuery, defaultMarketMakerParameters.toArray(new Object[defaultMarketMakerParameters.size()]));
			for(UserAlert userAlert: defaultUserAlerts) {
				userAlertById.put(userAlert.getId(), userAlert);
			}
		} 
		
		return userAlertById.values();
	}
	
	public Collection<UserAlert> getAlertsByUsernameAndEventIdAndShortDetails(String username, Integer eventId,String quantites,String category, UserAlertStatus userAlertStatus) {
		
		Collection<Object> parameters = new ArrayList<Object>();
		String query = "FROM UserAlert WHERE userAlertStatus = ? and alertTransType = ?";
		parameters.add(userAlertStatus);
		parameters.add("IRA");
		
		query += " AND eventId=? and category=?";
		parameters.add(eventId);
		parameters.add(category);
		
		/*query += " AND quantities=?";
		parameters.add(quantites);*/
		
		if(null != username && username.trim().length() >0){
			query += " AND username=? ";
			parameters.add(username);
		}
		return find(query, parameters.toArray(new Object[parameters.size()]));
	}

	@Override
	public Collection<UserAlert> getAllAlerts() {
		return getAllAlerts(UserAlertStatus.ACTIVE);
	}

}
