package com.admitone.tmat.dao.hibernate;
import java.util.Collection;

import com.admitone.tmat.data.TheaterSynonym;

public class TheaterSynonymDAO extends HibernateDAO<String, TheaterSynonym> implements com.admitone.tmat.dao.TheaterSynonymDAO {

	public Collection<TheaterSynonym> getValuesByName(String name){
		return (find ("from TheaterSynonym where name = ?" , new Object[]{name}));
	}
}
