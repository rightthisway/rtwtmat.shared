package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Broker;

public class BrokerDAO extends HibernateDAO<Integer, Broker> implements com.admitone.tmat.dao.BrokerDAO{

	
	public List<Broker> getAllActiveBrokers() {
		return find("FROM Broker WHERE status='ACTIVE'", new Object[]{});
	}
	public List<Broker> getAllActiveAutopricingBrokers() {
		return find("FROM Broker WHERE status='ACTIVE' AND isAutopricingBroker=?", new Object[]{Boolean.TRUE});
	}
	
	public List<Broker> getAllOpenOrderEligibleBrokers() {
		return find("FROM Broker WHERE status='ACTIVE' AND id in (5,10)");//RTW,RTW-2
	}
	
	/*@Override
	public List<Object[]> getAllBroker() {
		
		Session session = getSession();
		
		try
		{
			Query query = session.createSQLQuery("select id,name from broker");
			List<Object[]> brokerList = query.list();
			return brokerList;
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		finally{
			session.close();
		}
		
		return null;
	}*/
	 public Collection<Broker> getAll() {
	    	return find("FROM Broker WHERE  is_autopricing_broker = 1");
	    }
	 
	 
	public Collection<Broker> getBrokerById(int brokerId) {
	    	return find("FROM Broker WHERE  is_autopricing_broker = 1 and id="+brokerId);
	}
	
	@Override
	public Broker getBrokerByEmailSuffix(String emailSuffix) {
		List<Broker> brokers =  find("FROM Broker WHERE  emailSuffix = ? ", new Object[]{emailSuffix});
		if(brokers!=null && !brokers.isEmpty()){
			return brokers.get(0);
		}
		return null;
	}
}
