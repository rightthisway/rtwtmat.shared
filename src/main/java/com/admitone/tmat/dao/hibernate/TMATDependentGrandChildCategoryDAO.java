package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.TMATDependentGrandChildCategory;

public class TMATDependentGrandChildCategoryDAO extends HibernateDAO<Integer, TMATDependentGrandChildCategory> implements
		com.admitone.tmat.dao.TMATDependentGrandChildCategoryDAO {

	
	public List<TMATDependentGrandChildCategory> getTmatDependentsByGrandChildCategoryId(Integer grandChildCategoryId) {
		return find("FROM TMATDependentGrandChildCategory WHERE grandChildCategoryId = ? ",new Object[]{grandChildCategoryId});
	}

		
}
