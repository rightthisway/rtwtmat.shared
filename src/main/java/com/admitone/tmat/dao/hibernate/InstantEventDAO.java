package com.admitone.tmat.dao.hibernate;


import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.InstantEvent;


public class InstantEventDAO extends HibernateDAO<Integer, InstantEvent> implements com.admitone.tmat.dao.InstantEventDAO{

	
	public Collection<InstantEvent> getAllInstantEventsByTour(Integer tourId) {
//		return find("FROM InstantEvent WHERE tourId=? ORDER BY name,event_date, event_time Asc", new Object[]{tourId});
		return find("SELECT i FROM InstantEvent i WHERE i.event.tourId=? ORDER BY i.event.name,i.event.localDate,i.event.localTime", new Object[]{tourId});
	}		
	
	public Collection<InstantEvent> getInstantEventsByTour(Integer tourId) {
	//	return find("FROM InstantEvent WHERE tourId=? ORDER BY name,event_date, event_time Asc", new Object[]{tourId});
	//	return find("FROM InstantEvent WHERE tourId=? ", new Object[]{tourId});
		return find("SELECT i FROM InstantEvent i WHERE i.event.tourId=? AND i.event.eventStatus='ACTIVE' ORDER BY i.event.localDate, i.event.localTime ASC", new Object[]{tourId});
	}

	public int getInstantEventCountByTour(Integer tourId) {
		List list = find("SELECT count(i.id) FROM InstantEvent i WHERE i.event.tourId=? AND i.event.eventStatus = 'ACTIVE'", new Object[]{tourId});
		if (list == null || list.get(0) == null) {
			return 0;
		}
		return ((Long)list.get(0)).intValue();		
	
		
		}

	
	public void deleteEventsByIds(List<Integer> eventIds) {
		String events="";
		for(Integer event:eventIds){
			events+=event+",";
		}
		if(!"".equals(events)){
			bulkUpdate("DELETE from InstantEvent WHERE event.id in ("+ events.substring(0,events.length()-1)+ ")");
		}
	}

	
	public InstantEvent getInstantEventByEventId(Integer eventId) {
		return findSingle("FROM InstantEvent where event.id=?", new Object[]{eventId});
	}
}
