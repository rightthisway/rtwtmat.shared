package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.Preference;
import com.admitone.tmat.data.PreferencePK;

public class PreferenceDAO extends HibernateDAO<PreferencePK, Preference> implements com.admitone.tmat.dao.PreferenceDAO {

	public Collection<Preference> getAllPreferences(String username) {
		return find("FROM Preference WHERE username=?", new Object[]{username});
	}

	public Preference getPreference(String username, String name) {
		//return get(new PreferencePK(username, name));
		return findSingle("FROM Preference WHERE username=? AND name=?", new Object[]{username,name});
	}

	public void deletePreference(String username, String name) {
		bulkUpdate("DELETE FROM Preference where username=? AND name=?", new Object[]{username, name});
	}
	
	public void deleteAllPreferences(String username) {
		bulkUpdate("DELETE FROM Preference where username=?", new Object[]{username});
	}

	public void deleteAllPreferences() {
		bulkUpdate("DELETE FROM Preference");		
	}
}
