package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.TMATDependentArtist;

public class TMATDependentArtistDAO extends HibernateDAO<Integer, TMATDependentArtist> implements
		com.admitone.tmat.dao.TMATDependentArtistDAO {

	
	public List<TMATDependentArtist> getTmatDependentsByArtistId(Integer artistId) {
		return find("FROM TMATDependentArtist WHERE artistId = ? ",new Object[]{artistId});
	}

		
}
