package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.ExcludeTicketMap;

public class ExcludeTicketMapDAO extends HibernateDAO<Integer, ExcludeTicketMap> implements com.admitone.tmat.dao.ExcludeTicketMapDAO {
	
	public Collection<ExcludeTicketMap> getAllMapsByTicketId(Integer ticketId) {
		return find("FROM ExcludeTicketMap WHERE ticketId=?", new Object[]{ticketId});
	}

	public Collection<ExcludeTicketMap> getAllMapsByEventId(String eventId) {
		return find("FROM ExcludeTicketMap WHERE eventId=?", new Object[]{eventId});
	}

	public Collection<ExcludeTicketMap> getAllShortMapsByEventId(String eventId) {
		return find("FROM ExcludeTicketMap WHERE eventId=? AND transactionType=?", new Object[]{eventId, ExcludeTicketMap.TYPE_SHORT});
	}

	public Collection<ExcludeTicketMap> getAllLongMapsByEventId(String eventId) {
		return find("FROM ExcludeTicketMap WHERE eventId=? AND transactionType=?", new Object[]{eventId, ExcludeTicketMap.TYPE_LONG});
	}

	public Collection<ExcludeTicketMap> getAllMapsByShortId(Integer shortId) {
		return find("FROM ExcludeTicketMap WHERE transactionId=? AND transactionType=?", new Object[]{shortId, ExcludeTicketMap.TYPE_SHORT});
	}

	public Collection<ExcludeTicketMap> getAllMapsByTransactionIdAndType(Integer transactionId, Integer transactionType) {
		return find("FROM ExcludeTicketMap WHERE transactionId=? AND transactionType=?", new Object[]{transactionId, transactionType});		
	}

	public Collection<ExcludeTicketMap> getAllMapsByLongId(Integer shortId) {
		return find("FROM ExcludeTicketMap WHERE transactionId=? AND transactionType=?", new Object[]{shortId, ExcludeTicketMap.TYPE_LONG});
	}

	public Collection<ExcludeTicketMap> getAllMapsByEventAndCategory(Integer eventId, Integer categoryId) {
		return find("FROM ExcludeTicketMap WHERE eventId=? AND categoryId=?", new Object[]{eventId, categoryId});
	}

	public Collection<ExcludeTicketMap> getAllShortMapsByEventAndCategory(Integer eventId, Integer categoryId) {
		return find("FROM ExcludeTicketMap WHERE eventId=? AND transactionType=? AND categoryId=?", new Object[]{eventId, ExcludeTicketMap.TYPE_SHORT, categoryId});
	}

	public Collection<ExcludeTicketMap> getAllLongMapsByEventAndCategory(Integer eventId, Integer categoryId) {
		return find("FROM ExcludeTicketMap WHERE eventId=? AND transactionType=? AND categoryId=?", new Object[]{eventId, ExcludeTicketMap.TYPE_LONG, categoryId});
	}

	public Collection<ExcludeTicketMap> getAllMapsByEventId(Integer eventId) {
		return find("FROM ExcludeTicketMap WHERE eventId=?", new Object[]{eventId});
	}
	
	public void deleteEventMapForTicket(Integer ticketId) {
		bulkUpdate("DELETE FROM ExcludeTicketMap WHERE scope=? AND ticketId=?", new Object[]{ExcludeTicketMap.SCOPE_EVENT, ticketId});
	}
	
	public void deleteAllCatMapsForTicket(Integer ticketId) {
		bulkUpdate("DELETE FROM ExcludeTicketMap WHERE scope=? AND ticketId=?", new Object[]{ExcludeTicketMap.SCOPE_CAT, ticketId});		
	}

	public void deleteAllTxMapsForTicket(Integer ticketId) {
		bulkUpdate("DELETE FROM ExcludeTicketMap WHERE scope=? AND ticketId=?", new Object[]{ExcludeTicketMap.SCOPE_TX, ticketId});				
	}

	public void deleteAllCatMapsForTicket(Integer ticketId, Integer categoryId) {
		bulkUpdate("DELETE FROM ExcludeTicketMap WHERE scope=? AND ticketId=? AND categoryId=?", new Object[]{ExcludeTicketMap.SCOPE_CAT, ticketId, categoryId});				
	}

	public void deleteAllTxMapsForTicket(Integer ticketId, Integer categoryId) {
		bulkUpdate("DELETE FROM ExcludeTicketMap WHERE scope=? AND ticketId=? AND categoryId=?", new Object[]{ExcludeTicketMap.SCOPE_TX, ticketId, categoryId});				
	}

}
