package com.admitone.tmat.dao.hibernate;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ShortBroadcast;

public class ShortBroadcastDAO extends HibernateDAO<Integer, ShortBroadcast> implements com.admitone.tmat.dao.ShortBroadcastDAO {

	public Collection<ShortBroadcast> getAllBroadcastsByEvent(int eventId) {
		Event event = DAORegistry.getEventDAO().get(eventId);
		List<ShortBroadcast> result = find("SELECT sb FROM ShortBroadcast sb, Event e WHERE sb.admitoneEventId=e.admitoneId AND e.id=?", new Object[]{eventId});
		for (ShortBroadcast broadcast: result) {
			broadcast.setEvent(event);
		}
		return result;
	}

	public void deleteBroadcastsByEventId(Integer eventId) {
		bulkUpdate("DELETE FROM ShortBroadcast WHERE admitoneEventId IN (SELECT admitoneId FROM Event WHERE id=?)", new Object[]{eventId});
	}
	
	public Collection<ShortBroadcast> getAllLinkedShortBroadcasts() {
		List<Object> list = getHibernateTemplate().find("SELECT sb, e FROM ShortBroadcast sb, Event e WHERE sb.admitoneEventId=e.admitoneId");
		List<ShortBroadcast> result = new ArrayList<ShortBroadcast>();
		
		for (Object obj: list) {
			Object[] pair = (Object[]) obj;
			ShortBroadcast broadcast = (ShortBroadcast) pair[0];
			Event event = (Event) pair[1];
			broadcast.setEvent(event);
			result.add(broadcast);
		}
		return result;

	}
}
