package com.admitone.tmat.dao.hibernate;


import java.util.List;

import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.enums.EventStatus;

public class CategoryMappingDAO extends HibernateDAO<Integer, CategoryMapping> implements com.admitone.tmat.dao.CategoryMappingDAO {

	public List<CategoryMapping> getAllCategoryMappingsByVenueCategoryId(Integer venueCategoryId) {
		return find("FROM CategoryMapping WHERE categoryId IN (SELECT id FROM Category WHERE venueCategoryId = ? )", new Object[]{venueCategoryId});
	}

	public List<CategoryMapping> getAllCategoryMappingsByCategoryId(Integer categoryId) {
		return find("FROM CategoryMapping WHERE categoryId = ? ", new Object[]{categoryId}); 
	}
	
	public List<String> getAllCategoryGroupNamesByTourIdByVenueCategoryId(Integer tourId,EventStatus eventStatus) {
		return find("select categoryGroup from VenueCategory WHERE id in (Select distinct venueCategoryId from Event where tourId = ? and eventStatus =? )", 
				new Object[]{tourId,eventStatus});
	}
	
}
