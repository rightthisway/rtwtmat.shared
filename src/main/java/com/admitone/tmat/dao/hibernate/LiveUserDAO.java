package com.admitone.tmat.dao.hibernate;

import java.util.Collection;

import com.admitone.tmat.data.LiveUser;

public class LiveUserDAO extends HibernateDAO<String, LiveUser> implements com.admitone.tmat.dao.LiveUserDAO {
	
	public Collection<LiveUser> getLiveUserByUsername(String username) {
		Collection<LiveUser> list = find("FROM LiveUser WHERE username=?", new Object[]{username});
		if (list.size() == 0) {
			return null;
		}
		
		return list;
	}	
	public void updateLiveUserByUserName(String userName,String applicationName,Character isLoggedIn) {
		Collection<LiveUser> list = find("FROM LiveUser WHERE username=? and appName=? ", new Object[]{userName,applicationName});
		if (null != list && list.size() >= 0 && !list.isEmpty()) {
			bulkUpdate("update LiveUser set loggedIn=? where username=? and appName=? ", new Object[]{isLoggedIn,userName,applicationName});
		}else{
			LiveUser liveUser = new LiveUser();
			liveUser.setUsername(userName);
			liveUser.setAppName(applicationName);
			liveUser.setLoggedIn(isLoggedIn);
			save(liveUser);
		}
		
	}


}
