package com.admitone.tmat.dao.hibernate;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.admitone.tmat.data.ArtistPriceAdjustment;
import com.admitone.tmat.data.ArtistPriceAdjustmentPk;

public class ArtistPriceAdjustmentDAO extends HibernateDAO<ArtistPriceAdjustmentPk, ArtistPriceAdjustment> implements com.admitone.tmat.dao.ArtistPriceAdjustmentDAO {
	
	public Map<String, ArtistPriceAdjustment> getPriceAdjustments(int artistId) {
		Collection<ArtistPriceAdjustment> priceAdjustments = find("FROM ArtistPriceAdjustment WHERE artistId=?", new Object[]{artistId});
		Map<String, ArtistPriceAdjustment> priceAdjustmentMap = new HashMap<String, ArtistPriceAdjustment>();
		if (priceAdjustments != null) {
			for (ArtistPriceAdjustment priceAdjustment: priceAdjustments) {
				priceAdjustmentMap.put(priceAdjustment.getSiteId(), priceAdjustment);
			}
		}
		return priceAdjustmentMap;
	}
	
	public void deletePriceAdjustments(int artistId) {
		bulkUpdate("DELETE FROM ArtistPriceAdjustment WHERE artistId=?", new Object[]{artistId});
	}
	
}