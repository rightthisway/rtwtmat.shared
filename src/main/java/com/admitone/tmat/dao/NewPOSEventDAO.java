package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.NewPOSEvents;

public interface NewPOSEventDAO extends RootDAO<Integer, NewPOSEvents>{
	public Collection<NewPOSEvents> getAllNewPOSEvents();
	public Collection<NewPOSEvents> getTodayNewPOSEvents();
	public List<NewPOSEvents> getNewPOSEventsByDateStr(String dateTimeParm);
	//public Collection<NewPOSEvents> getHourNewPOSEvents();
}
