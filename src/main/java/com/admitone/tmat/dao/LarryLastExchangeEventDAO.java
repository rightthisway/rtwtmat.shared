package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.LarryLastExchangeEvent;


public interface LarryLastExchangeEventDAO extends RootDAO<Integer, LarryLastExchangeEvent> {

	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByEventId(Integer eventId);
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByVenueId(Integer venueId);
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByArtistId(Integer artistId);
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId);
	Collection<LarryLastExchangeEvent> getAllLarryLastExchangeEventsEligibleForUpdate(Long minute);
	public LarryLastExchangeEvent getLarryLastExchangeEventsByEventId(Integer eventId);
}
