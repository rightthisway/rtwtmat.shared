package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TMATDependentGrandChildCategory;

public interface TMATDependentGrandChildCategoryDAO extends RootDAO<Integer, TMATDependentGrandChildCategory> {
	List<TMATDependentGrandChildCategory> getTmatDependentsByGrandChildCategoryId(Integer tourId);

}
