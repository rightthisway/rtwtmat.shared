package com.admitone.tmat.dao;

import com.admitone.tmat.data.StubHubApiTracking;

public interface StubHubApiTrackingDAO extends RootDAO<Integer, StubHubApiTracking>{

	public int updateStubHubStatus(int crawlId);
}
