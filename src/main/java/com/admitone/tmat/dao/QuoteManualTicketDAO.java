package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.QuoteManualTicket;

public interface QuoteManualTicketDAO extends RootDAO<Integer, QuoteManualTicket> {
	
	QuoteManualTicket getQuoteManualTicket(Integer eventId,Integer quoteId);
	Collection<QuoteManualTicket> getAllQuoteManualTicket(Integer eventId,Integer quoteId);
	public void deleteQuoteManualTicketByQuoteId(Integer quoteId);
	List<QuoteManualTicket> getAllQuoteManualTicketByQuoteID(List<Integer> ids);
}
