package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.ValuationFactor;

public interface ValuationFactorDAO extends RootDAO<Integer, ValuationFactor> {
	Collection<ValuationFactor> getAllValuationFactorsFromEvent(Integer eventId);
	//Collection<ValuationFactor> getAllValuationFactorsFromTour(Integer tourId);
	Collection<ValuationFactor> getAllValuationFactorsFromArtist(Integer artistId);
}