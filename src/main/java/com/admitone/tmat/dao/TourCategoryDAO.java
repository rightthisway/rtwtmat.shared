package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TourCategory;

public interface TourCategoryDAO extends RootDAO<Integer, TourCategory>{
	public TourCategory getTourCategoryByName(String name);
	public List<TourCategory> getAllTourCategoryOrderByName();
}
