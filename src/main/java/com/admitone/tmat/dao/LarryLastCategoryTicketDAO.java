package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.LarryLastCategoryTicket;

public interface LarryLastCategoryTicketDAO extends RootDAO<Integer, LarryLastCategoryTicket> {

	public Collection<LarryLastCategoryTicket> getAllActiveLarryLastCategoryTickets();
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsByAll(Integer eventId, String section, String row,String quantity);
	public List<LarryLastCategoryTicket> getAllLarryLastCategoryTicketsById(Integer Id);
	public List<String> getPriceHistoryById(Integer Id);
}