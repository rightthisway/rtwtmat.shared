package com.admitone.tmat.dao;

import com.admitone.tmat.data.TicketMasterEventsArtist;

public interface TicketMasterEventsArtistDAO extends RootDAO<Integer, TicketMasterEventsArtist> {
	
}
