package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.Synonym;

public interface SynonymDAO extends RootDAO<String, Synonym> {
	List<Synonym> getSynonymByType(String Type);
	List<Synonym> getSynonymByNameAndType(String name, String type);
	public List<Synonym> getSynonymByNameAndTypeNew(String name, String type);
	void deleteSynonymByNameAndType(String name, String type);
	public void updateSynonymByNameAndType(Synonym synonym);
	public List<Synonym> getSynonymByName(String name);
	
}