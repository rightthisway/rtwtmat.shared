package com.admitone.tmat.dao;

import java.util.ArrayList;
import java.util.List;

import com.admitone.tmat.data.TnExchageEventZoneFloorCap;
import com.admitone.tmat.data.ZonesPricingFloorCap;

public interface ZonesPricingFloorCapDAO extends RootDAO<Integer, ZonesPricingFloorCap> {
	ZonesPricingFloorCap getEventZoneFloorCapByEventByZone(Integer eventId,String zone);
	List<ZonesPricingFloorCap> getAllZonesFloorCapByEventId(ArrayList<Integer> eventIds);
	public void deleteExpiredEventsByIds(List<Integer> eventIds);
	public void deleteFloorCapByVenue(String venueName,List<String> parentCategoryList);
	public List<TnExchageEventZoneFloorCap> getAllFloorCapByVenue(String venueName,List<String> parentCategoryList);
}
