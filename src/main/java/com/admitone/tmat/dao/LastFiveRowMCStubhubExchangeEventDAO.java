package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.LastFiveRowMCStubhubExchangeEvent;

public interface LastFiveRowMCStubhubExchangeEventDAO extends RootDAO<Integer, LastFiveRowMCStubhubExchangeEvent> {
	
	public Collection<LastFiveRowMCStubhubExchangeEvent> getAllLastFiveRowMCStubhubExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllLastFiveRowMCStubhubExchangeEventsByEvent(List<Integer> eventIds);
	//public List<LastFiveRowMCStubhubExchangeEvent> getAllActiveLastFiveRowMCStubhubExchangeEvent();
	public Collection<LastFiveRowMCStubhubExchangeEvent> getAllActiveLastFiveRowMCStubhubExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<LastFiveRowMCStubhubExchangeEvent> getAllDeletedLastFiveRowMCStubhubExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllLastFiveRowMCStubhubExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<LastFiveRowMCStubhubExchangeEvent> tnEvents) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
	
}
