package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.VipCatsCategoryTicket;

public interface VipCatsCategoryTicketDAO extends RootDAO<Integer, VipCatsCategoryTicket> {

	public Collection<VipCatsCategoryTicket> getAllActiveVipCatsCategoryTickets();
	public List<VipCatsCategoryTicket> getAllVipCatsCategoryTicketsByAll(Integer eventId, String section, String row,String quantity);
	
	public List<VipCatsCategoryTicket> getAllVipCatsCategoryTicketsById(Integer Id);
}
