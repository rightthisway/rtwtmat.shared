package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TMATDependentArtistMerge;

public interface TMATDependentTourMergeDAO extends RootDAO<Integer,TMATDependentArtistMerge> {
	public List<TMATDependentArtistMerge> getTMATDependentTourMergeByTourIdAndMergeTourId(Integer tourId,Integer toMergedTourId);
}
