package com.admitone.tmat.dao;

import java.util.ArrayList;
import java.util.List;

import com.admitone.tmat.data.TnExchageEventZoneFloorCap;

public interface TnExchangeEventZoneFloorCapDAO extends RootDAO<Integer, TnExchageEventZoneFloorCap> {
	TnExchageEventZoneFloorCap getEventZoneFloorCapByEventByZone(Integer eventId,Integer zoneId,Integer brokerId);
	List<TnExchageEventZoneFloorCap> getAllZonesFloorCapByEventId(ArrayList<Integer> eventIds,Integer brokerId);
	public void deleteExpiredEventsByIds(List<Integer> eventIds);
	public void deleteFloorCapByVenue(String venueName,List<String> parentCategoryList);
	public List<TnExchageEventZoneFloorCap> getAllFloorCapByVenue(String venueName,List<String> parentCategoryList);
}
