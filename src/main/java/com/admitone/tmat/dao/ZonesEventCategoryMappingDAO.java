package com.admitone.tmat.dao;

import com.admitone.tmat.data.ZonesEventCategoryMapping;


public interface ZonesEventCategoryMappingDAO extends RootDAO<Integer, ZonesEventCategoryMapping> {

	public ZonesEventCategoryMapping getZonesEventCategoryMappingByEvent(Integer eventId);
	public void deleteZonesEventCategoryMappingByEvent(Integer eventId);
}
