package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.ChildTourCategory;

public interface ChildTourCategoryDAO extends RootDAO<Integer, ChildTourCategory> {
	public List<ChildTourCategory> getChildTourCategoryByTourCategory(Integer id);
	public ChildTourCategory getChildTourCategoryByNameAndTourCategoryName(String name, String tourCategoryName);
	public List<ChildTourCategory> getChildTourCategoryOrderByName();
	public Collection<ChildTourCategory> getChildTourCategoryByTourCategories(List<Integer> ids);
	public Collection<ChildTourCategory> getChildTourCategoriesByName(String name);
}
