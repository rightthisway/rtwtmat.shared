package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.ZonedLastRowMiniTNExchangeEvent;

public interface ZonedLastRowMiniTNExchangeEventDAO extends RootDAO<Integer, ZonedLastRowMiniTNExchangeEvent> {
	
	public Collection<ZonedLastRowMiniTNExchangeEvent> getAllZonedLastRowMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllZonedLastRowMiniTNExchangeEventsByEvent(List<Integer> eventIds);
	public List<ZonedLastRowMiniTNExchangeEvent> getAllActiveZonedLastRowMiniTNExchangeEvent();
	public Collection<ZonedLastRowMiniTNExchangeEvent> getAllActiveZonedLastRowMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<ZonedLastRowMiniTNExchangeEvent> getAllDeletedZonedLastRowMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllZonedLastRowMiniTNExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<ZonedLastRowMiniTNExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String mini_exposure_ticketnetwork);
	public void updateRptFactor(String rpt_ticketnetwork);
	public void updatePriceBreakup(String pricebreakup_ticketnetwork);
	public void updateUpperMarkup(String upper_markpu_ticketnetwork);
	public void updateLowerMarkup(String lower_markpu_ticketnetwork);
	public void updateUpperShippingFees(String upper_shippingfees_ticketnetwork);
	public void updateLowerShippingFees(String lower_shippingfees_ticketnetwork);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
