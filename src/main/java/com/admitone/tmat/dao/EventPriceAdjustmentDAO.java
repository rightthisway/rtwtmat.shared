package com.admitone.tmat.dao;

import java.util.Map;

import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.EventPriceAdjustmentPk;

public interface EventPriceAdjustmentDAO extends RootDAO<EventPriceAdjustmentPk, EventPriceAdjustment> {
	EventPriceAdjustment get(int eventId, String siteId);
	Map<String, EventPriceAdjustment> getPriceAdjustments(int eventId);
	void deletePriceAdjustments(int eventId);
	EventPriceAdjustmentPk save(EventPriceAdjustment priceAdjustment);
	void update(EventPriceAdjustment priceAdjustment);
}
