package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.ZoneLastRowMiniExchangeEvent;


public interface ZoneLastRowMiniExchangeEventDAO extends RootDAO<Integer, ZoneLastRowMiniExchangeEvent> {

	Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByEventId(Integer eventId);
	Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByVenueId(Integer venueId);
	Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByArtistId(Integer artistId);
	Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId);
	public Collection<ZoneLastRowMiniExchangeEvent> getAllZoneLastRowMiniExchangeEventsEligibleForUpdate(Long minute);
	public ZoneLastRowMiniExchangeEvent getZoneLastRowMiniExchangeEventsByEventId(Integer eventId);
}
