package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.LongTransaction;

public interface LongTransactionDAO extends RootDAO<Integer, LongTransaction> {
	Collection<LongTransaction> getLongTransactionsByEventId(int eventId);
}
