package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.AutoVividSeatExchangeEvent;
import com.admitone.tmat.data.GlobalAutoPricingAudit;

public interface AutoVividSeatExchangeEventDAO extends RootDAO<Integer, AutoVividSeatExchangeEvent> {
	
	public Collection<AutoVividSeatExchangeEvent> getAllAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds);
	public List<AutoVividSeatExchangeEvent> getAllActiveAutoVividSeatExchangeEvent();
	public Collection<AutoVividSeatExchangeEvent> getAllActiveAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<AutoVividSeatExchangeEvent> getAllDeletedAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllAutoVividSeatExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<AutoVividSeatExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String auto_exposure_vivid);
	public void updateRptFactor(String rpt_vivid);
	public void updatePriceBreakup(String pricebreakup_vivid);
	public void updateUpperMarkup(String upper_markpu_vivid);
	public void updateLowerMarkup(String lower_markpu_vivid);
	public void updateUpperShippingFees(String upper_shippingfees_vivid);
	public void updateLowerShippingFees(String lower_shippingfees_vivid);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
	
}
