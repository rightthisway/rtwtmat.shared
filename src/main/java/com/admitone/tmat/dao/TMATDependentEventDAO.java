package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.TMATDependentEvent;

public interface TMATDependentEventDAO extends RootDAO<Integer, TMATDependentEvent> {
	List<TMATDependentEvent> getTmatDependentsByEventId(Integer eventId);
	void saveorUpdateTMATDependentEvent(Collection<Event> events);

}
