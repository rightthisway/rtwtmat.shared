package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.VenueCategory;

public interface VenueCategoryDAO extends RootDAO<Integer, VenueCategory> {
	public VenueCategory getVenueCategoryByVenueAndCategoryGroup(Integer venueId,String categroyGroup);
	public List<VenueCategory> getVenueCategoriesByVenueId(Integer venueId);
	public List<VenueCategory> getVenueCategoriesByVenueIdOrderByCategory(Integer venueId);
	public List<String> getCategoryGroupByVenueId(Integer venueId);
	public void deleteVenueCategory(List venueCategoryId);
	public List<String> getCategoryGroupByVenueCategoryId(Integer venuCatergoryGroupId );
	public List<VenueCategory> getAllVenueCategoriesForUSandCA();
	public List<VenueCategory> getVenueCategoriesForPresaleEvent(Integer venueId);
}
