package com.admitone.tmat.dao;

import com.admitone.tmat.data.TicketMasterEvent;

public interface TicketMasterEventDAO extends RootDAO<Integer, TicketMasterEvent> {
	
}
