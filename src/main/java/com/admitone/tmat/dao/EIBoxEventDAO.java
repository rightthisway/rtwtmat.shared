package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.EIBoxEvent;

public interface EIBoxEventDAO extends RootDAO<Integer, EIBoxEvent> {
	Collection<EIBoxEvent> getEIBoxEvents(Date fromDate, Date toDate);
	Collection<EIBoxEvent> getEIBoxEventsByNameFilter(String eventFilter);
}
