package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TickPickExchangeEvent;

public interface TickPickExchangeEventDAO extends RootDAO<Integer, TickPickExchangeEvent> {
	
	public Collection<TickPickExchangeEvent> getAllTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllTickPickExchangeEventsByEvent(List<Integer> eventIds);
	public List<TickPickExchangeEvent> getAllActiveTickPickExchangeEvent();
	public Collection<TickPickExchangeEvent> getAllActiveTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<TickPickExchangeEvent> getAllDeletedTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllTickPickExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<TickPickExchangeEvent> tnEvents) throws Exception;
	
}
