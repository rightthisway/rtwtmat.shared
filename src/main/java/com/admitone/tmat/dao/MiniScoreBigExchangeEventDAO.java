package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.MiniScoreBigExchangeEvent;

public interface MiniScoreBigExchangeEventDAO extends RootDAO<Integer, MiniScoreBigExchangeEvent> {
	
	public Collection<MiniScoreBigExchangeEvent> getAllMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds);
	public List<MiniScoreBigExchangeEvent> getAllActiveMiniScoreBigExchangeEvent();
	public Collection<MiniScoreBigExchangeEvent> getAllActiveMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<MiniScoreBigExchangeEvent> getAllDeletedMiniScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllMiniScoreBigExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<MiniScoreBigExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String mini_exposure_scorebig);
	public void updateRptFactor(String rpt_scorebig);
	public void updatePriceBreakup(String pricebreakup_scorebig);
	public void updateUpperMarkup(String upper_markpu_scorebig);
	public void updateLowerMarkup(String lower_markpu_scorebig);
	public void updateUpperShippingFees(String upper_shippingfees_scorebig);
	public void updateLowerShippingFees(String lower_shippingfees_scorebig);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
