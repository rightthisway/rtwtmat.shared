package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.DuplicateTicketMap;

public interface DuplicateTicketMapDAO extends RootDAO<Integer, DuplicateTicketMap> {
	Collection<DuplicateTicketMap> getDuplicateTicketMap(Integer eventId);
	void deleteByGroupId(Long groupId);
	void deleteGroupsWithOneEntry(Integer eventId);
	void delete(Integer ticketId, boolean deleteGroup);
}