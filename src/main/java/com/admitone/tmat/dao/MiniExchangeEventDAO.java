package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.MiniExchangeEvent;

public interface MiniExchangeEventDAO extends RootDAO<Integer, MiniExchangeEvent>{

	
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByArtistId(Integer artistId);
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId);
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsByVenueId(Integer venueId);
	public MiniExchangeEvent getMiniExchangeEventsByEventId(Integer eventId);
	public Collection<MiniExchangeEvent> getAllMiniExchangeEventsEligibleForUpdate(Long minute);
}
