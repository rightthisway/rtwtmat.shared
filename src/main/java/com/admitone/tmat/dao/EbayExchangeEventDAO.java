package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.EbayExchangeEvent;

public interface EbayExchangeEventDAO extends RootDAO<Integer, EbayExchangeEvent> {
	
	public Collection<EbayExchangeEvent> getAllEbayExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllEbayExchangeEventsByEvent(List<Integer> eventIds);
	public List<EbayExchangeEvent> getAllActiveEbayExchangeEvent();
	public Collection<EbayExchangeEvent> getAllActiveEbayExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<EbayExchangeEvent> getAllDeletedEbayExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllEbayExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<EbayExchangeEvent> tnEvents) throws Exception;
	
}
