package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.VipMiniTNExchangeEvent;

public interface VipMiniTNExchangeEventDAO extends RootDAO<Integer, VipMiniTNExchangeEvent> {
	
	public Collection<VipMiniTNExchangeEvent> getAllVipMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVipMiniTNExchangeEventsByEvent(List<Integer> eventIds);
	public List<VipMiniTNExchangeEvent> getAllActiveVipMiniTNExchangeEvent();
	public Collection<VipMiniTNExchangeEvent> getAllActiveVipMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VipMiniTNExchangeEvent> getAllDeletedVipMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVipMiniTNExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VipMiniTNExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String vip_exposure_ticketnetwork);
	public void updateRptFactor(String rpt_ticketnetwork);
	public void updatePriceBreakup(String pricebreakup_ticketnetwork);
	public void updateUpperMarkup(String upper_markpu_ticketnetwork);
	public void updateLowerMarkup(String lower_markpu_ticketnetwork);
	public void updateUpperShippingFees(String upper_shippingfees_ticketnetwork);
	public void updateLowerShippingFees(String lower_shippingfees_ticketnetwork);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
