package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.EIBoxTicket;

public interface EIBoxTicketDAO extends RootDAO<Integer, EIBoxTicket> {
	Collection<EIBoxTicket> getEIBoxTicketsByEventId(Integer eventId, Integer venueId, Date eventDate);
}
