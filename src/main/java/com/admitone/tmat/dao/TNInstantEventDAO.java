package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.TNInstantEvent;
//import com.admitone.tmat.data.Tour;

public interface TNInstantEventDAO extends RootDAO<Integer,TNInstantEvent>{

	Collection<Event> getAllInstantEventsByTour(Integer tourId);
	Collection<TNInstantEvent> getAllTNInstantEventsByTour(Integer tourId);
	int getInstantEventCountByTour(Integer tourId);
	public boolean deleteEventsByIds(List<Integer> eventIds);
	public TNInstantEvent getTNInstantEventByEventId(Integer eventId);
//	public Collection<Tour> getAllTours();
	
}
