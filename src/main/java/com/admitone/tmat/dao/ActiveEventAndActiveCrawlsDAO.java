package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.ActiveEventAndActiveCrawls;

public interface ActiveEventAndActiveCrawlsDAO extends RootDAO<Integer, ActiveEventAndActiveCrawls>{
   public  List<ActiveEventAndActiveCrawls> getActiveEventAndActiveCrawl(String applyDate);
}
