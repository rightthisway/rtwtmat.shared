package com.admitone.tmat.dao;

import com.admitone.tmat.data.Privilege;

public interface PrivilegeDAO extends RootDAO<Integer, Privilege> {
}
