package com.admitone.tmat.dao;

import com.admitone.tmat.data.VenueSynonym;

public interface VenueSynonymDAO extends RootDAO<String, VenueSynonym> {
	 
	public VenueSynonym getSynonymByVenueId(Integer venueId);
}