package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.ConcertSynonym;

public interface ConcertSynonymDAO extends RootDAO<String, ConcertSynonym> {
	Collection<ConcertSynonym> getValuesByName(String name);
}
