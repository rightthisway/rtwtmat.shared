package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.EventAudit;

public interface EventAuditDAO extends RootDAO<Integer,EventAudit> {

	public List<EventAudit> getEventAuditByEventId(Integer eventId);
	public List<EventAudit> getEventRemovedByAction();
	public List<EventAudit> getAllEventsByTourId(Integer tourId);
	public List<EventAudit> getAllEventsByVenueId(Integer venueId);
}
