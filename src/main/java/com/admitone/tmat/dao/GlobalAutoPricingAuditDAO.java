package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;

public interface GlobalAutoPricingAuditDAO extends RootDAO<Integer, GlobalAutoPricingAudit> {

	public List<GlobalAutoPricingAudit> getAllAuditsOrderByCreatedDate();
}
