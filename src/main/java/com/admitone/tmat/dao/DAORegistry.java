package com.admitone.tmat.dao;

import com.admitone.tmat.dao.hibernate.AutoScoreBigExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.AutoTNExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.AutoTickPickExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.AutoVividSeatExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.CrawlerHistoryManagementDAO;
import com.admitone.tmat.dao.hibernate.LastFiveRowMiniCategoryTicketDAO;
import com.admitone.tmat.dao.hibernate.LastRowMiniCategoryTicketDAO;
import com.admitone.tmat.dao.hibernate.MiniScoreBigExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.MiniTNExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.MiniTickPickExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.MiniVividSeatExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.QueryManagerDAO;
import com.admitone.tmat.dao.hibernate.QuoteManualTicketDAO;
import com.admitone.tmat.dao.hibernate.VipMiniScoreBigExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.VipMiniTNExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.VipMiniTickPickExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.VipMiniVividSeatExchangeEventDAO;
import com.admitone.tmat.dao.hibernate.ZonedLastRowMiniCategoryTicketDAO;
import com.admitone.tmat.dao.hibernate.ZonesPricingCategoryTicketDAO;

public class DAORegistry {
	private static AdmitoneEventDAO admitoneEventDAO;
	private static AdmitoneEventLocalDAO admitoneEventLocalDAO;
	private static AdmitoneInventoryDAO admitoneInventoryDAO;
	private static AdmitoneInventoryLocalDAO admitoneInventoryLocalDAO;
	private static EventDAO eventDAO;
	private static HarvestEventDAO harvestEventDAO;
	private static HarvestTicketDAO harvestTicketDAO;
	
	private static CrawlerHistoryManagementDAO crawlerHistoryManagementDAO;
	private static QueryManagerDAO queryManagerDAO;
	
	private static AdmitoneTicketMarkDAO admitoneTicketMarkDAO;
	private static ArtistDAO artistDAO;
	private static UserDAO userDAO;
	private static UserBrokerDetailsDAO userBrokerDetailsDAO;
	private static LiveUserDAO liveUserDAO;
	private static UserActionDAO userActionDAO;
	private static UserAlertDAO userAlertDAO;
	private static UserOutlierDAO userOutlierDAO;
	private static UserOutlierCategoryDetailsDAO userOutlierCategoryDetailsDAO;
	private static MMEventMapDAO mMEventMapDAO;
	private static RoleDAO roleDAO;
	private static VenueDAO venueDAO;
	private static VenueCategoryDAO venueCategoryDAO;
	private static SynonymDAO synonymDAO;
	private static VenueSynonymDAO venueSynonymDAO;
	private static TheaterSynonymDAO theaterSynonymDAO;
	private static ConcertSynonymDAO concertSynonymDAO;
	private static CategorySynonymDAO categorySynonymDAO;
	private static SiteDAO siteDAO;
//	private static TourDAO tourDAO;
	private static TicketDAO ticketDAO;
	private static TicketListingCrawlDAO TicketListingCrawlDAO;
	private static BookmarkDAO bookmarkDAO;
	private static CategoryDAO categoryDAO;
	private static CategoryMappingDAO categoryMappingDAO;
//	private static CategoryNewDAO categoryNewDAO;
//	private static CategoryMappingNewDAO categoryMappingNewDAO;
	private static PropertyDAO propertyDAO;
	private static EventPriceAdjustmentDAO eventPriceAdjustmentDAO;
//	private static TourPriceAdjustmentDAO tourPriceAdjustmentDAO;
	private static ArtistPriceAdjustmentDAO artistPriceAdjustmentDAO;
	private static ShortBroadcastDAO shortBroadcastDAO;
	private static ShortTransactionDAO shortTransactionDAO;
	private static LongTransactionDAO longTransactionDAO;
	private static EventSynonymDAO eventSynonymDAO;
	private static ValuationFactorDAO valuationFactorDAO;
	private static ExcludeTicketMapDAO excludeTicketMapDAO;
	private static PreferenceDAO preferenceDAO;
	private static DuplicateTicketMapDAO duplicateTicketMapDAO;
	private static EbayInventoryDAO ebayInventoryDAO;
	private static EbayInventoryGroupDAO ebayInventoryGroupDAO;
//	private static EbayInventoryEventDAO ebayInventoryEventDAO;
//	private static EbayInventoryTourDAO ebayInventoryTourDAO;
	private static SeatWaveEventDAO seatWaveEventDAO;
	private static SeatWaveSeasonDAO seatWaveSeasonDAO;	
	private static PrivilegeDAO privilegeDAO;
	private static QuoteCustomerDAO quoteCustomerDAO;
	private static TicketQuoteDAO ticketQuoteDAO;
	private static QuoteDAO quoteDAO;
	private static QuoteManualTicketDAO quoteManualTicketDAO;
	private static MarketMakerEventTrackingDAO marketMakerEventTrackingDAO;
//	private static InstantTicketsQueryManager instantTicketsQueryManager;
//	private static MercuryTicketsQueryManager mercuryTicketsQueryManager;
//	private static EITicketsQueryManager eiTicketsQueryManager;
//	private static InstantEventDAO instantEventDAO; 
//	private static InstantTourDAO instantTourDAO;
//	private static EIInstantEventDAO eiinstantEventDAO; 
//	private static EIInstantTourDAO eiinstantTourDAO;
//	private static TNInstantEventDAO tninstantEventDAO; 
//	private static TNInstantTourDAO tninstantTourDAO;
//	private static TnowInstantEventDAO tnowInstantEventDAO;
	private static TourCategoryDAO tourCategoryDAO;
	private static ChildTourCategoryDAO childTourCategoryDAO;
	private static GrandChildTourCategoryDAO grandChildTourCategoryDAO;
	private static ProxyDAO proxyDAO;
	private static MXPZonesTicketDAO mxpZonesTicketDAO;
	// history daos
	private static HistoricalTicketDAO historicalTicketDAO;
	
//	private static HistoricalTourDAO historicalTourDAO;
//	private static HistoricalVenueDAO historicalVenueDAO;
//	private static HistoricalEventDAO historicalEventDAO;
	
	private static EIBoxTicketDAO eiBoxTicketDAO;
	private static EIBoxEventDAO eiBoxEventDAO;
	private static EiMarketPlaceEventDAO eiMarketPlaceEventDAO;
	
	private static ZonesEventCategoryMappingDAO zoneEventCategoryMappingDAO;
	
	private static ExcludedCheapestTicketDAO excludedCheapestTicketDAO;
	private static ManagePurchasePriceDAO managePurchasePriceDAO;
	private static DefaultPurchasePriceDAO defaultPurchasePriceDAO;
	
	private static ZonesPricingFloorCapDAO zonesPricingFloorCapDAO;
	private static ZonesPricingFloorCapAuditDAO zonesPricingFloorCapAuditDAO;
	private static JKTZonesPricingCategoryTicketDAO jktZonesPricingCategoryTicketDAO;
	private static TnExchangeEventZoneGroupTicketDAO tnExchangeEventZoneGroupTicketDAO;
	private static ZonePricingErrorLogDAO zonePricingErrorLogDAO;
	private static TnExchangeEventZoneFloorCapDAO tnExchangeEventZoneFloorCapDAO;
	private static TnExchangeTheatreEventZonesDAO tnExchangeTheatreEventZonesDAO;
	//private static ExchangeEventCategoryGroupDAO exchangeEventCategoryGroupDAO;
	private static TnFloorCapEventVenueDAO tnFloorCapEventVenueDAO;
	private static TnFloorCapEventVenueAuditDAO tnFloorCapEventVenueAuditDAO;
	private static TnExchangeEventZoneFloorCapAuditDAO tnExchangeEventZoneFloorCapAuditDAO;
	

	private static TMATDependentArtistDAO tmatDependentArtistDAO;
	private static TMATDependentEventDAO tmatDependentEventDAO;
	private static TMATDependentVenueMergeDAO tmatDependentVenueMergeDAO;
	private static TMATDependentArtistMergeDAO tmatDependentArtistMergeDAO;
	private static OpenOrderStatusDAO openOrderStatusDAO;
	private static TMATDependentChildCategoryDAO tmatDependentChildCategoryDAO;
	private static TMATDependentGrandChildCategoryDAO tmatDependentGrandChildCategoryDAO;
	private static TMATDependentZonesCategoryDAO tmatDependentZonesCategoryDAO;
	private static AlertTicketGroupDAO alertTicketGroupDAO;
	private static NewPOSEventDAO newPOSEventDAO;
	private static DefaultExchangeCrawlFrequencyDAO defaultExchangeCrawlFrequencyDAO;
	private static TNTGCatsCategoryEventDAO tnTGCatsCategoryEventDAO;
	private static TNTGCatsCategoryEventAuditDAO tnTGCatsCategoryEventAuditDAO;
	private static TNExchangeEventDAO tnExchangeEventDAO;
	private static VividExchangeEventDAO vividExchangeEventDAO;
	private static TickPickExchangeEventDAO tickPickExchangeEventDAO;
	private static EbayExchangeEventDAO ebayExchangeEventDAO;
	private static ScoreBigExchangeEventDAO scoreBigExchangeEventDAO;
	private static LarryMiniTNExchangeEventDAO larryMiniTNExchangeEventDAO;
	private static LastFiveRowMCStubhubExchangeEventDAO lastFiveRowMCStubhubExchangeEventDAO;
	private static ExchangeFourExchangeEventDAO exchangeFourExchangeEventDAO;
	private static ExchangeFiveExchangeEventDAO exchangeFiveExchangeEventDAO;
	private static ExchangeEventAutoPricingAuditDAO exchangeEventAutoPricingAuditDAO;
	private static ExchangeEventAuditDAO exchangeEventAuditDAO;
	private static CsvUploadRemoveAuditDAO csvUploadRemoveAuditDAO;
	private static TNErrorDAO tnErrorDAO;
	private static CategoryTicketDAO categoryTicketDAO;
	private static TGCatsCategoryTicketDAO  tgCatsCategoryTicketDAO;
	private static AutoCatsCategoryTicketDAO  autoCatsCategoryTicketDAO;
	private static LastFiveRowMiniCategoryTicketDAO lastFiveRowMiniCategoryTicketDAO;
	private static LastRowMiniCategoryTicketDAO lastRowMiniCategoryTicketDAO;
	private static LarryLastCategoryTicketDAO larryLastCategoryTicketDAO;
	private static VipAutoCategoryTicketDAO vipAutoCategoryTicketDAO;
	private static VipCatsCategoryTicketDAO  vipCatsCategoryTicketDAO;
	private static TgCatsEbayListingDAO tgCatsEbayListingDAO;
	private static TGCatsCategoryEventDAO tgCatsCategoryEventDAO;
	private static TGCatsErrorDAO tgCatsErrorDAO;
	private static TNDataFeedEventDAO tnDataFeedEventDAO;
	private static VWTGCatsCategoryTicketDAO  vwTgCatsCategoryTicketDAO;
	private static VividTheaterCategoryTicketDAO vividTheaterCategoryTicketDAO;
	private static DefaultAutoPricingAuditDAO defaultAutoPricingAuditDAO;
	private static DefaultExchangeMarkupAuditDAO defaultExchangeMarkupAuditDAO;
	private static EventAuditDAO eventAuditDAO;
	private static VWVipCatsCategoryTicketDAO  vwVipCatsCategoryTicketDAO;
	private static AutoTNExchangeEventDAO autoTNExchangeEventDAO;
	private static AutoTickPickExchangeEventDAO autoTickPickExchangeEventDAO;
	private static AutoVividSeatExchangeEventDAO autoVividSeatExchangeEventDAO;
	private static AutoScoreBigExchangeEventDAO autoScoreBigExchangeEventDAO;
	private static MiniTNExchangeEventDAO miniTNExchangeEventDAO;
	private static MiniTickPickExchangeEventDAO miniTickPickExchangeEventDAO;
	private static MiniVividSeatExchangeEventDAO miniVividSeatExchangeEventDAO;
	private static MiniScoreBigExchangeEventDAO miniScoreBigExchangeEventDAO;
	private static VipMiniTNExchangeEventDAO vipMiniTNExchangeEventDAO;
	private static VipMiniTickPickExchangeEventDAO vipMiniTickPickExchangeEventDAO;
	private static VipMiniVividSeatExchangeEventDAO vipMiniVividSeatExchangeEventDAO;
	private static VipMiniScoreBigExchangeEventDAO vipMiniScoreBigExchangeEventDAO;
	private static VipAutoTNExchangeEventDAO vipAutoTNExchangeEventDAO;
	private static VipAutoVividSeatExchangeEventDAO vipAutoVividSeatExchangeEventDAO;
	private static VipAutoScoreBigExchangeEventDAO vipAutoScoreBigExchangeEventDAO;
	private static VipAutoCatsErrorDAO vipAutoCatsErrorDAO;
	private static VwVipAutoCategoryTicketDAO vwVipAutoCategoryTicketDAO;
	private static GlobalAutoPricingAuditDAO globalAutoPricingAuditDAO;
	private static LastRowMiniTNExchangeEventDAO lastRowMiniTNExchangeEventDAO;
	private static ZonesPricingTNExchangeEventDAO zonesPricingTNExchangeEventDAO;
	private static ZonedLastRowMiniTNExchangeEventDAO zonedLastRowMiniTNExchangeEventDAO;
	private static VWLastRowMiniCatsCategoryTicketDAO vwLastRowMiniCatsCategoryTicketDAO;
	private static LastRowMiniCatsErrorDAO lastRowMiniCatsErrorDAO;
	private static AuditTnApiCallLimitDAO auditTnApiCallLimitDAO;
	private static ZonedLastRowMiniCategoryTicketDAO zonedLastRowMiniCategoryTicketDAO;
	private static ZonesPricingCategoryTicketDAO zonesPricingCategoryTicketDAO;	
	private static VipCatsErrorDAO vipCatsErrorDAO;
	private static BrokerDAO brokerDAO;
	private static ManageBrokerAuditDAO manageBrokerAuditDAO;
	private static ZoneEventDAO zoneEventDAO;
	private static PresaleEventDAO presaleEventDAO;
	private static SoldTicketDetailDAO soldTicketDetailDAO;
	private static AutopricingProductDAO autopricingProductDAO;
	private static MiniExchangeEventDAO miniExchangeEventDAO;
	private static VipMiniExchangeEventDAO vipMiniExchangeEventDAO;
	private static LastRowMiniExchangeEventDAO lastRowMiniExchangeEventDAO;
	private static LarryLastExchangeEventDAO larryLastExchangeEventDAO;
	private static ZoneLastRowMiniExchangeEventDAO zoneLastRowMiniExchangeEventDAO;
	private static ZonePricingExchangeEventDAO zonePricingExchangeEventDAO;
	private static ZoneTicketsProcessorExchangeEventDAO zoneTicketsProcessorExchangeEventDAO;
	private static ActiveEventAndActiveCrawlsDAO activeEventAndActiveCrawlsDAO;
	private static StubHubApiTrackingDAO stubHubApiTrackingDAO;
	private static TicketMasterEventDAO ticketMasterEventDAO;
	private static TicketMasterEventsArtistDAO ticketMasterEventsArtistDAO;
	private static VenueMapZonesDAO venueMapZonesDAO;
	private static EventHitAuditDAO eventHitAuditDAO;
	private static ProbableVenueDAO probableVenueDAO;
	private static TixcityUnfilledOrderSummaryDAO tixcityUnfilledOrderSummaryDAO;
	private static VividSeatsTicketInventoryDAO vividSeatsTicketInventoryDAO;
	
	public static ManageBrokerAuditDAO getManageBrokerAuditDAO() {
		return manageBrokerAuditDAO;
	}

	public void setManageBrokerAuditDAO(ManageBrokerAuditDAO manageBrokerAuditDAO) {
		DAORegistry.manageBrokerAuditDAO = manageBrokerAuditDAO;
	}
	
	

	public static BrokerDAO getBrokerDAO() {
		return brokerDAO;
	}

	public void setBrokerDAO(BrokerDAO brokerDAO) {
		DAORegistry.brokerDAO = brokerDAO;
	}

	public static AuditTnApiCallLimitDAO getAuditTnApiCallLimitDAO() {
		return auditTnApiCallLimitDAO;
	}

	public void setAuditTnApiCallLimitDAO(
			AuditTnApiCallLimitDAO auditTnApiCallLimitDAO) {
		DAORegistry.auditTnApiCallLimitDAO = auditTnApiCallLimitDAO;
	}
	
	public static CsvUploadRemoveAuditDAO getCsvUploadRemoveAuditDAO() {
		return csvUploadRemoveAuditDAO;
	}

	public void setCsvUploadRemoveAuditDAO(CsvUploadRemoveAuditDAO csvUploadRemoveAuditDAO) {
		DAORegistry.csvUploadRemoveAuditDAO = csvUploadRemoveAuditDAO;
	}

	public static AdmitoneEventDAO getAdmitoneEventDAO() {
		return admitoneEventDAO;
	}
	
	public void setAdmitoneEventDAO(AdmitoneEventDAO admitoneEventDAO) {
		DAORegistry.admitoneEventDAO = admitoneEventDAO;
	}
	public static AdmitoneEventLocalDAO getAdmitoneEventLocalDAO() {
		return admitoneEventLocalDAO;
	}
	
	public void setAdmitoneEventLocalDAO(AdmitoneEventLocalDAO admitoneEventLocalDAO) {
		DAORegistry.admitoneEventLocalDAO = admitoneEventLocalDAO;
	}
	
	public static AdmitoneInventoryDAO getAdmitoneInventoryDAO() {
		return admitoneInventoryDAO;
	}
	
	public void setAdmitoneInventoryDAO(AdmitoneInventoryDAO admitoneInventoryDAO) {
		DAORegistry.admitoneInventoryDAO = admitoneInventoryDAO;
	}
	
	public static AdmitoneInventoryLocalDAO getAdmitoneInventoryLocalDAO() {
		return admitoneInventoryLocalDAO;
	}
	
	public void setAdmitoneInventoryLocalDAO(AdmitoneInventoryLocalDAO admitoneInventoryLocalDAO) {
		DAORegistry.admitoneInventoryLocalDAO = admitoneInventoryLocalDAO;
	}
	

	
	public static EventDAO getEventDAO() {
		return eventDAO;
	}
	
	public void setEventDAO(EventDAO eventDAO) {
		DAORegistry.eventDAO = eventDAO;
	}
		
	public static HarvestEventDAO getHarvestEventDAO() {
		return harvestEventDAO;
	}
	
	public void setHarvestEventDAO(HarvestEventDAO harvestEventDAO) {
		DAORegistry.harvestEventDAO = harvestEventDAO;
	}
			
	public static HarvestTicketDAO getHarvestTicketDAO() {
		return harvestTicketDAO;
	}
	
	public void setHarvestTicketDAO(HarvestTicketDAO harvestTicketDAO) {
		DAORegistry.harvestTicketDAO = harvestTicketDAO;
	}
	
	public static ArtistDAO getArtistDAO() {
		return artistDAO;
	}
	
	public void setArtistDAO(ArtistDAO artistDAO) {
		DAORegistry.artistDAO = artistDAO;
	}
	
	public static UserDAO getUserDAO() {
		return userDAO;
	}
	
	public void setUserDAO(UserDAO userDAO) {
		DAORegistry.userDAO = userDAO;
	}
	
	public static LiveUserDAO getLiveUserDAO() {
		return liveUserDAO;
	}
	
	public void setLiveUserDAO(LiveUserDAO liveUserDAO) {
		DAORegistry.liveUserDAO = liveUserDAO;
	}
	
	public static UserActionDAO getUserActionDAO() {
		return userActionDAO;
	}
	
	public void setUserActionDAO(UserActionDAO userActionDAO) {
		DAORegistry.userActionDAO = userActionDAO;
	}
	
	
	
	public static UserOutlierDAO getUserOutlierDAO() {
		return userOutlierDAO;
	}

	public void setUserOutlierDAO(UserOutlierDAO userOutlierDAO) {
		DAORegistry.userOutlierDAO = userOutlierDAO;
	}
	
	public static UserOutlierCategoryDetailsDAO getUserOutlierCategoryDetailsDAO() {
		return userOutlierCategoryDetailsDAO;
	}

	public void setUserOutlierCategoryDetailsDAO(UserOutlierCategoryDetailsDAO userOutlierCategoryDetailsDAO) {
		DAORegistry.userOutlierCategoryDetailsDAO = userOutlierCategoryDetailsDAO;
	}

	
	public static UserAlertDAO getUserAlertDAO() {
		return userAlertDAO;
	}
	
	public void setUserAlertDAO(UserAlertDAO userAlertDAO) {
		DAORegistry.userAlertDAO = userAlertDAO;
	}
	
	
	public static MMEventMapDAO getMMEventMapDAO() {
		return mMEventMapDAO;
	}
	
	public void setMMEventMapDAO(MMEventMapDAO mMEventMapDAO) {
		DAORegistry.mMEventMapDAO = mMEventMapDAO;
	}
	
	public static RoleDAO getRoleDAO() {
		return roleDAO;
	}
	
	public void setRoleDAO(RoleDAO roleDAO) {
		DAORegistry.roleDAO = roleDAO;
	}
	
	public static VenueDAO getVenueDAO() {
		return venueDAO;
	}
	
	public void setVenueDAO(VenueDAO venueDAO) {
		DAORegistry.venueDAO = venueDAO;
	}
	
	public static SynonymDAO getSynonymDAO() {
		return synonymDAO;
	}
	
	public void setSynonymDAO(SynonymDAO synonymDAO) {
		DAORegistry.synonymDAO = synonymDAO;
	}
	
	
	public static TheaterSynonymDAO getTheaterSynonymDAO() {
		return theaterSynonymDAO;
	}
	
	public void setTheaterSynonymDAO(TheaterSynonymDAO theaterSynonymDAO) {
		DAORegistry.theaterSynonymDAO = theaterSynonymDAO;
	}
	
	public static ConcertSynonymDAO getConcertSynonymDAO() {
		return concertSynonymDAO;
	}

	public void setConcertSynonymDAO(ConcertSynonymDAO concertSynonymDAO) {
		DAORegistry.concertSynonymDAO = concertSynonymDAO;
	}

	public static CategorySynonymDAO getCategorySynonymDAO() {
		return categorySynonymDAO;
	}
	
	public void setCategorySynonymDAO(CategorySynonymDAO categorySynonymDAO) {
		DAORegistry.categorySynonymDAO = categorySynonymDAO;
	}
	
	public static SiteDAO getSiteDAO() {
		return siteDAO;
	}

	public void setSiteDAO(SiteDAO siteDAO) {
		DAORegistry.siteDAO = siteDAO;
	}

	/*public static TourDAO getTourDAO() {
		return tourDAO;
	}
	
	public void setTourDAO(TourDAO tourDAO) {
		DAORegistry.tourDAO = tourDAO;
	}*/
	
	public static TicketDAO getTicketDAO() {
		return ticketDAO;
	}
	
	public static PreferenceDAO getPreferenceDAO() {
		return preferenceDAO;
	}

	public void setPreferenceDAO(PreferenceDAO preferenceDAO) {
		DAORegistry.preferenceDAO = preferenceDAO;
	}	

	public void setTicketDAO(TicketDAO ticketDAO) {
		DAORegistry.ticketDAO = ticketDAO;
	}
	
	public static TicketListingCrawlDAO getTicketListingCrawlDAO() {
		return TicketListingCrawlDAO;
	}
	
	public void setTicketListingCrawlDAO(
			TicketListingCrawlDAO ticketListingCrawlDAO) {
		TicketListingCrawlDAO = ticketListingCrawlDAO;
	}
	
	public static BookmarkDAO getBookmarkDAO() {
		return bookmarkDAO;
	}
	
	public void setBookmarkDAO(BookmarkDAO bookmarkDAO) {
		DAORegistry.bookmarkDAO = bookmarkDAO;
	}
	
	public static CategoryDAO getCategoryDAO() {
		return categoryDAO;
	}
	
	public void setCategoryDAO(CategoryDAO categoryDAO) {
		DAORegistry.categoryDAO = categoryDAO;
	}
	
	public static CategoryMappingDAO getCategoryMappingDAO() {
		return categoryMappingDAO;
	}
	
	public void setCategoryMappingDAO(CategoryMappingDAO categoryMappingDAO) {
		DAORegistry.categoryMappingDAO = categoryMappingDAO;
	}
	
	public static PropertyDAO getPropertyDAO() {
		return propertyDAO;
	}
	
	public void setPropertyDAO(PropertyDAO propertyDAO) {
		DAORegistry.propertyDAO = propertyDAO;
	}
	
	public static EventPriceAdjustmentDAO getEventPriceAdjustmentDAO() {
		return eventPriceAdjustmentDAO;
	}
	
	public void setEventPriceAdjustmentDAO(
			EventPriceAdjustmentDAO eventPriceAdjustmentDAO) {
		DAORegistry.eventPriceAdjustmentDAO = eventPriceAdjustmentDAO;
	}
	
	/*public static TourPriceAdjustmentDAO getTourPriceAdjustmentDAO() {
		return tourPriceAdjustmentDAO;
	}
	
	public void setTourPriceAdjustmentDAO(
			TourPriceAdjustmentDAO tourPriceAdjustmentDAO) {
		DAORegistry.tourPriceAdjustmentDAO = tourPriceAdjustmentDAO;
	}*/
	
	public static ArtistPriceAdjustmentDAO getArtistPriceAdjustmentDAO() {
		return artistPriceAdjustmentDAO;
	}
	
	public void setArtistPriceAdjustmentDAO(
			ArtistPriceAdjustmentDAO artistPriceAdjustmentDAO) {
		DAORegistry.artistPriceAdjustmentDAO = artistPriceAdjustmentDAO;
	}
	
	public static ShortBroadcastDAO getShortBroadcastDAO() {
		return shortBroadcastDAO;
	}
	
	public void setShortBroadcastDAO(ShortBroadcastDAO shortBroadcastDAO) {
		DAORegistry.shortBroadcastDAO = shortBroadcastDAO;
	}
	
	public static ShortTransactionDAO getShortTransactionDAO() {
		return shortTransactionDAO;
	}
	
	public void setShortTransactionDAO(
			ShortTransactionDAO shortTransactionDAO) {
		DAORegistry.shortTransactionDAO = shortTransactionDAO;
	}

	public static LongTransactionDAO getLongTransactionDAO() {
		return longTransactionDAO;
	}
	
	public void setLongTransactionDAO(
			LongTransactionDAO longTransactionDAO) {
		DAORegistry.longTransactionDAO = longTransactionDAO;
	}

	public static EventSynonymDAO getEventSynonymDAO() {
		return eventSynonymDAO;
	}
	
	public void setEventSynonymDAO(EventSynonymDAO eventSynonymDAO) {
		DAORegistry.eventSynonymDAO = eventSynonymDAO;
	}
	
	public static ValuationFactorDAO getValuationFactorDAO() {
		return valuationFactorDAO;
	}

	public void setValuationFactorDAO(ValuationFactorDAO valuationFactorDAO) {
		DAORegistry.valuationFactorDAO = valuationFactorDAO;
	}
	
	public static ExcludeTicketMapDAO getExcludeTicketMapDAO() {
		return excludeTicketMapDAO;
	}

	public void setExcludeTicketMapDAO(ExcludeTicketMapDAO excludeTicketMapDAO) {
		DAORegistry.excludeTicketMapDAO = excludeTicketMapDAO;
	}	

	public static HistoricalTicketDAO getHistoricalTicketDAO() {
		return historicalTicketDAO;
	}

	public void setHistoricalTicketDAO(
			HistoricalTicketDAO historicalTicketDAO) {
		DAORegistry.historicalTicketDAO = historicalTicketDAO;
	}

	public static DuplicateTicketMapDAO getDuplicateTicketMapDAO() {
		return duplicateTicketMapDAO;
	}

	public void setDuplicateTicketMapDAO(
			DuplicateTicketMapDAO duplicateTicketMapDAO) {
		DAORegistry.duplicateTicketMapDAO = duplicateTicketMapDAO;
	}
	
	public static EbayInventoryDAO getEbayInventoryDAO() {
		return ebayInventoryDAO;
	}

	public void setEbayInventoryDAO(EbayInventoryDAO ebayInventoryDAO) {
		DAORegistry.ebayInventoryDAO = ebayInventoryDAO;
	}
	
	public static EbayInventoryGroupDAO getEbayInventoryGroupDAO() {
		return ebayInventoryGroupDAO;
	}

	public void setEbayInventoryGroupDAO(EbayInventoryGroupDAO ebayInventoryGroupDAO) {
		DAORegistry.ebayInventoryGroupDAO = ebayInventoryGroupDAO;
	}		

	/*public static EbayInventoryEventDAO getEbayInventoryEventDAO() {
		return ebayInventoryEventDAO;
	}

	public void setEbayInventoryEventDAO(
			EbayInventoryEventDAO ebayInventoryEventDAO) {
		DAORegistry.ebayInventoryEventDAO = ebayInventoryEventDAO;
	}

	public static EbayInventoryTourDAO getEbayInventoryTourDAO() {
		return ebayInventoryTourDAO;
	}

	public void setEbayInventoryTourDAO(EbayInventoryTourDAO ebayInventoryTourDAO) {
		DAORegistry.ebayInventoryTourDAO = ebayInventoryTourDAO;
	}	*/
	
	public static EIBoxTicketDAO getEiBoxTicketDAO() {
		return eiBoxTicketDAO;
	}

	public void setEiBoxTicketDAO(EIBoxTicketDAO eiBoxTicketDAO) {
		DAORegistry.eiBoxTicketDAO = eiBoxTicketDAO;
	}
	
	public static EIBoxEventDAO getEiBoxEventDAO() {
		return eiBoxEventDAO;
	}

	public void setEiBoxEventDAO(EIBoxEventDAO eiBoxEventDAO) {
		DAORegistry.eiBoxEventDAO = eiBoxEventDAO;
	}
	
	public static SeatWaveEventDAO getSeatWaveEventDAO() {
		return seatWaveEventDAO;
	}

	public void setSeatWaveEventDAO(SeatWaveEventDAO seatWaveEventDAO) {
		DAORegistry.seatWaveEventDAO = seatWaveEventDAO;
	}
	
	public static SeatWaveSeasonDAO getSeatWaveSeasonDAO() {
		return seatWaveSeasonDAO;
	}
	
	public void setSeatWaveSeasonDAO(SeatWaveSeasonDAO seatWaveSeasonDAO) {
		DAORegistry.seatWaveSeasonDAO = seatWaveSeasonDAO;
	}
	
	public static PrivilegeDAO getPrivilegeDAO() {
		return privilegeDAO;
	}
	
	public void setPrivilegeDAO(PrivilegeDAO privilegeDAO) {
		DAORegistry.privilegeDAO = privilegeDAO;
	}	
	
	public static TicketQuoteDAO getTicketQuoteDAO() {
		return ticketQuoteDAO;
	}

	public void setTicketQuoteDAO(TicketQuoteDAO ticketQuoteDAO) {
		DAORegistry.ticketQuoteDAO = ticketQuoteDAO;
	}

	public static QuoteDAO getQuoteDAO() {
		return quoteDAO;
	}

	public void setQuoteDAO(QuoteDAO quoteDAO) {
		DAORegistry.quoteDAO = quoteDAO;
	}

	public static QuoteCustomerDAO getQuoteCustomerDAO() {
		return quoteCustomerDAO;
	}

	public void setQuoteCustomerDAO(
			QuoteCustomerDAO quoteCustomerDAO) {
		DAORegistry.quoteCustomerDAO = quoteCustomerDAO;
	}

	public static EiMarketPlaceEventDAO getEiMarketPlaceEventDAO() {
		return eiMarketPlaceEventDAO;
	}

	public void setEiMarketPlaceEventDAO(EiMarketPlaceEventDAO eiMarketPlaceEventDAO) {
		DAORegistry.eiMarketPlaceEventDAO = eiMarketPlaceEventDAO;
	}	
	

	public static AdmitoneTicketMarkDAO getAdmitoneTicketMarkDAO() {
		return admitoneTicketMarkDAO;
	}

	public void setAdmitoneTicketMarkDAO(
			AdmitoneTicketMarkDAO admitOneTicketMapDAO) {
		DAORegistry.admitoneTicketMarkDAO = admitOneTicketMapDAO;
	}

	public static MarketMakerEventTrackingDAO getMarketMakerEventTrackingDAO() {
		return marketMakerEventTrackingDAO;
	}

	public void setMarketMakerEventTrackingDAO(
			MarketMakerEventTrackingDAO marketMakerEventTrackingDAO) {
		DAORegistry.marketMakerEventTrackingDAO = marketMakerEventTrackingDAO;
	}
	
	/*public static InstantTicketsQueryManager getInstantTicketsQueryManager() {
		return instantTicketsQueryManager;
	}

	public void setInstantTicketsQueryManager(InstantTicketsQueryManager instantTicketsQueryManager) {
		DAORegistry.instantTicketsQueryManager = instantTicketsQueryManager;
	}	
	
	public static MercuryTicketsQueryManager getMercuryTicketsQueryManager() {
		return mercuryTicketsQueryManager;
	}

	public void setMercuryTicketsQueryManager(MercuryTicketsQueryManager mercuryTicketsQueryManager) {
		DAORegistry.mercuryTicketsQueryManager = mercuryTicketsQueryManager;
	}
	
	public static EITicketsQueryManager getEiTicketsQueryManager() {
		return eiTicketsQueryManager;
	}

	public void setEiTicketsQueryManager(EITicketsQueryManager eiTicketsQueryManager) {
		DAORegistry.eiTicketsQueryManager = eiTicketsQueryManager;
	}
	
	public static InstantEventDAO getInstantEventDAO() {
		return instantEventDAO;
	}

	public void setInstantEventDAO(InstantEventDAO instantEventDAO) {
		DAORegistry.instantEventDAO = instantEventDAO;
	}
	

	public static InstantTourDAO getInstantTourDAO() {
		return instantTourDAO;
	}

	public void setInstantTourDAO(InstantTourDAO instantTourDAO) {
		DAORegistry.instantTourDAO = instantTourDAO;
	}
	
	public static EIInstantEventDAO getEiinstantEventDAO() {
		return eiinstantEventDAO;
	}

	public void setEiinstantEventDAO(EIInstantEventDAO eiinstantEventDAO) {
		DAORegistry.eiinstantEventDAO = eiinstantEventDAO;
	}

	public static EIInstantTourDAO getEiinstantTourDAO() {
		return eiinstantTourDAO;
	}

	public void setEiinstantTourDAO(EIInstantTourDAO eiinstantTourDAO) {
		DAORegistry.eiinstantTourDAO = eiinstantTourDAO;
	}

	public static TNInstantEventDAO getTninstantEventDAO() {
		return tninstantEventDAO;
	}

	public void setTninstantEventDAO(TNInstantEventDAO tninstantEventDAO) {
		DAORegistry.tninstantEventDAO = tninstantEventDAO;
	}

	public static TNInstantTourDAO getTninstantTourDAO() {
		return tninstantTourDAO;
	}

	public void setTninstantTourDAO(TNInstantTourDAO tninstantTourDAO) {
		DAORegistry.tninstantTourDAO = tninstantTourDAO;
	}*/

	public static CrawlerHistoryManagementDAO getCrawlerHistoryManagementDAO() {
		return crawlerHistoryManagementDAO;
	}

	public void setCrawlerHistoryManagementDAO(
			CrawlerHistoryManagementDAO crawlerHistoryManagementDAO) {
		DAORegistry.crawlerHistoryManagementDAO = crawlerHistoryManagementDAO;
	}

	public static QueryManagerDAO getQueryManagerDAO() {
		return queryManagerDAO;
	}

	public void setQueryManagerDAO(QueryManagerDAO queryManagerDAO) {
		DAORegistry.queryManagerDAO = queryManagerDAO;
	}
	
	public static ZonesEventCategoryMappingDAO getZoneEventCategoryMappingDAO() {
		return zoneEventCategoryMappingDAO;
	}

	public void setZoneEventCategoryMappingDAO(
			ZonesEventCategoryMappingDAO zoneEventCategoryMappingDAO) {
		DAORegistry.zoneEventCategoryMappingDAO = zoneEventCategoryMappingDAO;
	}

	public static TourCategoryDAO getTourCategoryDAO() {
		return tourCategoryDAO;
	}

	public void setTourCategoryDAO(TourCategoryDAO tourCategoryDAO) {
		DAORegistry.tourCategoryDAO = tourCategoryDAO;
	}

	public static ChildTourCategoryDAO getChildTourCategoryDAO() {
		return childTourCategoryDAO;
	}

	public void setChildTourCategoryDAO(
			ChildTourCategoryDAO childTourCategoryDAO) {
		DAORegistry.childTourCategoryDAO = childTourCategoryDAO;
	}

	public static GrandChildTourCategoryDAO getGrandChildTourCategoryDAO() {
		return grandChildTourCategoryDAO;
	}

	public void setGrandChildTourCategoryDAO(
			GrandChildTourCategoryDAO grandChildTourCategoryDAO) {
		DAORegistry.grandChildTourCategoryDAO = grandChildTourCategoryDAO;
	}

	public static ProxyDAO getProxyDAO() {
		return proxyDAO;
	}

	public void setProxyDAO(ProxyDAO proxyDAO) {
		DAORegistry.proxyDAO = proxyDAO;
	}

	/*public static TnowInstantEventDAO getTnowInstantEventDAO() {
		return tnowInstantEventDAO;
	}

	public void setTnowInstantEventDAO(
			TnowInstantEventDAO tnowInstantEventDAO) {
		DAORegistry.tnowInstantEventDAO = tnowInstantEventDAO;
	}*/

	public static MXPZonesTicketDAO getMxpZonesTicketDAO() {
		return mxpZonesTicketDAO;
	}

	public void setMxpZonesTicketDAO(MXPZonesTicketDAO mxpZonesTicketDAO) {
		DAORegistry.mxpZonesTicketDAO = mxpZonesTicketDAO;
	}

	

	public static ExcludedCheapestTicketDAO getExcludedCheapestTicketDAO() {
		return excludedCheapestTicketDAO;
	}

	public void setExcludedCheapestTicketDAO(
			ExcludedCheapestTicketDAO excludedCheapestTicketDAO) {
		DAORegistry.excludedCheapestTicketDAO = excludedCheapestTicketDAO;
	}	

	public static ManagePurchasePriceDAO getManagePurchasePriceDAO() {
		return managePurchasePriceDAO;
	}
	
	public void setManagePurchasePriceDAO(ManagePurchasePriceDAO managePurchasePriceDAO) {
		DAORegistry.managePurchasePriceDAO = managePurchasePriceDAO;
	}

	public static DefaultPurchasePriceDAO getDefaultPurchasePriceDAO() {
		return defaultPurchasePriceDAO;
	}

	public void setDefaultPurchasePriceDAO(
			DefaultPurchasePriceDAO defaultPurchasePriceDAO) {
		DAORegistry.defaultPurchasePriceDAO = defaultPurchasePriceDAO;
	}
	
	public static TMATDependentArtistDAO getTmatDependentArtistDAO() {
		return tmatDependentArtistDAO;
	}

	public void setTmatDependentArtistDAO(TMATDependentArtistDAO tmatDependentArtistDAO) {
		DAORegistry.tmatDependentArtistDAO = tmatDependentArtistDAO;
	}

	public static TMATDependentEventDAO getTmatDependentEventDAO() {
		return tmatDependentEventDAO;
	}

	public void setTmatDependentEventDAO(TMATDependentEventDAO tmatDependentEventDAO) {
		DAORegistry.tmatDependentEventDAO = tmatDependentEventDAO;
	}

	public static TMATDependentVenueMergeDAO getTmatDependentVenueMergeDAO() {
		return tmatDependentVenueMergeDAO;
	}

	public void setTmatDependentVenueMergeDAO(
			TMATDependentVenueMergeDAO tmatDependentVenueMergeDAO) {
		DAORegistry.tmatDependentVenueMergeDAO = tmatDependentVenueMergeDAO;
	}
	
	public static TMATDependentArtistMergeDAO getTmatDependentArtistMergeDAO() {
		return tmatDependentArtistMergeDAO;
	}

	public void setTmatDependentArtistMergeDAO(
			TMATDependentArtistMergeDAO tmatDependentArtistMergeDAO) {
		DAORegistry.tmatDependentArtistMergeDAO = tmatDependentArtistMergeDAO;
	}

	public static OpenOrderStatusDAO getOpenOrderStatusDAO() {
		return openOrderStatusDAO;
	}

	public void setOpenOrderStatusDAO(OpenOrderStatusDAO openOrderStatusDAO) {
		DAORegistry.openOrderStatusDAO = openOrderStatusDAO;
	}

	public static TMATDependentChildCategoryDAO getTmatDependentChildCategoryDAO() {
		return tmatDependentChildCategoryDAO;
	}

	public void setTmatDependentChildCategoryDAO(TMATDependentChildCategoryDAO tmatDependentChildCategoryDAO) {
		DAORegistry.tmatDependentChildCategoryDAO = tmatDependentChildCategoryDAO;
	}

	public static TMATDependentGrandChildCategoryDAO getTmatDependentGrandChildCategoryDAO() {
		return tmatDependentGrandChildCategoryDAO;
	}

	public void setTmatDependentGrandChildCategoryDAO(TMATDependentGrandChildCategoryDAO tmatDependentGrandChildCategoryDAO) {
		DAORegistry.tmatDependentGrandChildCategoryDAO = tmatDependentGrandChildCategoryDAO;
	}

	public static TMATDependentZonesCategoryDAO getTmatDependentZonesCategoryDAO() {
		return tmatDependentZonesCategoryDAO;
	}

	public void setTmatDependentZonesCategoryDAO(TMATDependentZonesCategoryDAO tmatDependentZonesCategoryDAO) {
		DAORegistry.tmatDependentZonesCategoryDAO = tmatDependentZonesCategoryDAO;
	}

	public static AlertTicketGroupDAO getAlertTicketGroupDAO() {
		return alertTicketGroupDAO;
	}

	public void setAlertTicketGroupDAO(
			AlertTicketGroupDAO alertTicketGroupDAO) {
		DAORegistry.alertTicketGroupDAO = alertTicketGroupDAO;
	}

	
	
	public static NewPOSEventDAO getNewPOSEventDAO() {
		return newPOSEventDAO;
	}

	public void setNewPOSEventDAO(NewPOSEventDAO newPOSEventDAO) {
		DAORegistry.newPOSEventDAO = newPOSEventDAO;
	}

	public static VenueCategoryDAO getVenueCategoryDAO() {
		return venueCategoryDAO;
	}

	public void setVenueCategoryDAO(VenueCategoryDAO venueCategoryDAO) {
		DAORegistry.venueCategoryDAO = venueCategoryDAO;
	}

	/*public static CategoryNewDAO getCategoryNewDAO() {
		return categoryNewDAO;
	}

	public void setCategoryNewDAO(CategoryNewDAO categoryNewDAO) {
		DAORegistry.categoryNewDAO = categoryNewDAO;
	}

	public static CategoryMappingNewDAO getCategoryMappingNewDAO() {
		return categoryMappingNewDAO;
	}

	public void setCategoryMappingNewDAO(
			CategoryMappingNewDAO categoryMappingNewDAO) {
		DAORegistry.categoryMappingNewDAO = categoryMappingNewDAO;
	}*/
	
	public static QuoteManualTicketDAO getQuoteManualTicketDAO() {
		return quoteManualTicketDAO;
	}

	public void setQuoteManualTicketDAO(
			QuoteManualTicketDAO quoteManualTicketDAO) {
		DAORegistry.quoteManualTicketDAO = quoteManualTicketDAO;
	}
	public static DefaultExchangeCrawlFrequencyDAO getDefaultExchangeCrawlFrequencyDAO() {
		return defaultExchangeCrawlFrequencyDAO;
	}

	public void setDefaultExchangeCrawlFrequencyDAO(
			DefaultExchangeCrawlFrequencyDAO defaultExchangeCrawlFrequencyDAO) {
		DAORegistry.defaultExchangeCrawlFrequencyDAO = defaultExchangeCrawlFrequencyDAO;
	}

	public static TNTGCatsCategoryEventDAO getTnTGCatsCategoryEventDAO() {
		return tnTGCatsCategoryEventDAO;
	}

	public void setTnTGCatsCategoryEventDAO(
			TNTGCatsCategoryEventDAO tnTGCatsCategoryEventDAO) {
		DAORegistry.tnTGCatsCategoryEventDAO = tnTGCatsCategoryEventDAO;
	}

	public static TNTGCatsCategoryEventAuditDAO getTnTGCatsCategoryEventAuditDAO() {
		return tnTGCatsCategoryEventAuditDAO;
	}

	public  void setTnTGCatsCategoryEventAuditDAO(
			TNTGCatsCategoryEventAuditDAO tnTGCatsCategoryEventAuditDAO) {
		DAORegistry.tnTGCatsCategoryEventAuditDAO = tnTGCatsCategoryEventAuditDAO;
	}

	public static TNExchangeEventDAO getTnExchangeEventDAO() {
		return tnExchangeEventDAO;
	}

	public void setTnExchangeEventDAO(TNExchangeEventDAO tnExchangeEventDAO) {
		DAORegistry.tnExchangeEventDAO = tnExchangeEventDAO;
	}

	public static VividExchangeEventDAO getVividExchangeEventDAO() {
		return vividExchangeEventDAO;
	}

	public  void setVividExchangeEventDAO(
			VividExchangeEventDAO vividExchangeEventDAO) {
		DAORegistry.vividExchangeEventDAO = vividExchangeEventDAO;
	}

	public static ExchangeEventAutoPricingAuditDAO getExchangeEventAutoPricingAuditDAO() {
		return exchangeEventAutoPricingAuditDAO;
	}

	public void setExchangeEventAutoPricingAuditDAO(
			ExchangeEventAutoPricingAuditDAO exchangeEventAutoPricingAuditDAO) {
		DAORegistry.exchangeEventAutoPricingAuditDAO = exchangeEventAutoPricingAuditDAO;
	}

	public static EbayExchangeEventDAO getEbayExchangeEventDAO() {
		return ebayExchangeEventDAO;
	}

	public void setEbayExchangeEventDAO(
			EbayExchangeEventDAO ebayExchangeEventDAO) {
		DAORegistry.ebayExchangeEventDAO = ebayExchangeEventDAO;
	}

	public static LarryMiniTNExchangeEventDAO getLarryMiniTNExchangeEventDAO() {
		return larryMiniTNExchangeEventDAO;
	}

	public void setLarryMiniTNExchangeEventDAO(
			LarryMiniTNExchangeEventDAO larryMiniTNExchangeEventDAO) {
		DAORegistry.larryMiniTNExchangeEventDAO = larryMiniTNExchangeEventDAO;
	}

	public static LastFiveRowMCStubhubExchangeEventDAO getLastFiveRowMCStubhubExchangeEventDAO() {
		return lastFiveRowMCStubhubExchangeEventDAO;
	}

	public void setLastFiveRowMCStubhubExchangeEventDAO(
			LastFiveRowMCStubhubExchangeEventDAO lastFiveRowMCStubhubExchangeEventDAO) {
		DAORegistry.lastFiveRowMCStubhubExchangeEventDAO = lastFiveRowMCStubhubExchangeEventDAO;
	}

	public static ExchangeFourExchangeEventDAO getExchangeFourExchangeEventDAO() {
		return exchangeFourExchangeEventDAO;
	}

	public void setExchangeFourExchangeEventDAO(
			ExchangeFourExchangeEventDAO exchangeFourExchangeEventDAO) {
		DAORegistry.exchangeFourExchangeEventDAO = exchangeFourExchangeEventDAO;
	}

	public static ExchangeFiveExchangeEventDAO getExchangeFiveExchangeEventDAO() {
		return exchangeFiveExchangeEventDAO;
	}

	public void setExchangeFiveExchangeEventDAO(
			ExchangeFiveExchangeEventDAO exchangeFiveExchangeEventDAO) {
		DAORegistry.exchangeFiveExchangeEventDAO = exchangeFiveExchangeEventDAO;
	}

	public static TNErrorDAO getTnErrorDAO() {
		return tnErrorDAO;
	}

	public  void setTnErrorDAO(TNErrorDAO tnErrorDAO) {
		DAORegistry.tnErrorDAO = tnErrorDAO;
	}

	public static CategoryTicketDAO getCategoryTicketDAO() {
		return categoryTicketDAO;
	}

	public  void setCategoryTicketDAO(CategoryTicketDAO categoryTicketDAO) {
		DAORegistry.categoryTicketDAO = categoryTicketDAO;
	}

	public static TGCatsCategoryTicketDAO getTgCatsCategoryTicketDAO() {
		return tgCatsCategoryTicketDAO;
	}

	public  void setTgCatsCategoryTicketDAO(
			TGCatsCategoryTicketDAO tgCatsCategoryTicketDAO) {
		
		DAORegistry.tgCatsCategoryTicketDAO = tgCatsCategoryTicketDAO;
	}
	
	
	
	
	public static LastFiveRowMiniCategoryTicketDAO getLastFiveRowMiniCategoryTicketDAO() {
		return lastFiveRowMiniCategoryTicketDAO;
	}

	public  void setLastFiveRowMiniCategoryTicketDAO(
			LastFiveRowMiniCategoryTicketDAO lastFiveRowMiniCategoryTicketDAO) {
		DAORegistry.lastFiveRowMiniCategoryTicketDAO = lastFiveRowMiniCategoryTicketDAO;
	}
	
	public static LastRowMiniCategoryTicketDAO getLastRowMiniCategoryTicketDAO() {
		return lastRowMiniCategoryTicketDAO;
	}

	public  void setLastRowMiniCategoryTicketDAO(
			LastRowMiniCategoryTicketDAO lastRowMiniCategoryTicketDAO) {
		DAORegistry.lastRowMiniCategoryTicketDAO = lastRowMiniCategoryTicketDAO;
	}
	
	
	
	
	public static LarryLastCategoryTicketDAO getLarryLastCategoryTicketDAO() {
		return larryLastCategoryTicketDAO;
	}

	public  void setLarryLastCategoryTicketDAO(
			LarryLastCategoryTicketDAO larryLastCategoryTicketDAO) {
		DAORegistry.larryLastCategoryTicketDAO = larryLastCategoryTicketDAO;
	}
	
	public static VipAutoCategoryTicketDAO getVipAutoCategoryTicketDAO() {
		return vipAutoCategoryTicketDAO;
	}

	public  void setVipAutoCategoryTicketDAO(
			VipAutoCategoryTicketDAO vipAutoCategoryTicketDAO) {
		DAORegistry.vipAutoCategoryTicketDAO = vipAutoCategoryTicketDAO;
	}
	
	
	public static AutoCatsCategoryTicketDAO getAutoCatsCategoryTicketDAO() {
		return autoCatsCategoryTicketDAO;
	}

	public  void setAutoCatsCategoryTicketDAO(
			AutoCatsCategoryTicketDAO autoCatsCategoryTicketDAO) {
		DAORegistry.autoCatsCategoryTicketDAO = autoCatsCategoryTicketDAO;
	}

	public static TGCatsCategoryEventDAO getTgCatsCategoryEventDAO() {
		return tgCatsCategoryEventDAO;
	}

	public  void setTgCatsCategoryEventDAO(
			TGCatsCategoryEventDAO tgCatsCategoryEventDAO) {
		DAORegistry.tgCatsCategoryEventDAO = tgCatsCategoryEventDAO;
	}

	public static TGCatsErrorDAO getTgCatsErrorDAO() {
		return tgCatsErrorDAO;
	}

	public void setTgCatsErrorDAO(TGCatsErrorDAO tgCatsErrorDAO) {
		DAORegistry.tgCatsErrorDAO = tgCatsErrorDAO;
	}

	public static VWTGCatsCategoryTicketDAO getVwTgCatsCategoryTicketDAO() {
		return vwTgCatsCategoryTicketDAO;
	}

	public  void setVwTgCatsCategoryTicketDAO(
			VWTGCatsCategoryTicketDAO vwTgCatsCategoryTicketDAO) {
		DAORegistry.vwTgCatsCategoryTicketDAO = vwTgCatsCategoryTicketDAO;
	}
	
	public static TNDataFeedEventDAO getTnDataFeedEventDAO() {
		return tnDataFeedEventDAO;
	}

	public void setTnDataFeedEventDAO(TNDataFeedEventDAO tnDataFeedEventDAO) {
		DAORegistry.tnDataFeedEventDAO = tnDataFeedEventDAO;
	}
	
	public static VividTheaterCategoryTicketDAO getVividTheaterCategoryTicketDAO() {
		return vividTheaterCategoryTicketDAO;
	}

	public  void setVividTheaterCategoryTicketDAO(
			VividTheaterCategoryTicketDAO vividTheaterCategoryTicketDAO) {
		DAORegistry.vividTheaterCategoryTicketDAO = vividTheaterCategoryTicketDAO;
	}

	public static DefaultAutoPricingAuditDAO getDefaultAutoPricingAuditDAO() {
		return defaultAutoPricingAuditDAO;
	}

	public void setDefaultAutoPricingAuditDAO(
			DefaultAutoPricingAuditDAO defaultAutoPricingAuditDAO) {
		DAORegistry.defaultAutoPricingAuditDAO = defaultAutoPricingAuditDAO;
	}

	public static DefaultExchangeMarkupAuditDAO getDefaultExchangeMarkupAuditDAO() {
		return defaultExchangeMarkupAuditDAO;
	}

	public void setDefaultExchangeMarkupAuditDAO(
			DefaultExchangeMarkupAuditDAO defaultExchangeMarkupAuditDAO) {
		DAORegistry.defaultExchangeMarkupAuditDAO = defaultExchangeMarkupAuditDAO;
	}

	public static TickPickExchangeEventDAO getTickPickExchangeEventDAO() {
		return tickPickExchangeEventDAO;
	}

	public void setTickPickExchangeEventDAO(
			TickPickExchangeEventDAO tickPickExchangeEventDAO) {
		DAORegistry.tickPickExchangeEventDAO = tickPickExchangeEventDAO;
	}

	public static ScoreBigExchangeEventDAO getScoreBigExchangeEventDAO() {
		return scoreBigExchangeEventDAO;
	}

	public void setScoreBigExchangeEventDAO(
			ScoreBigExchangeEventDAO scoreBigExchangeEventDAO) {
		DAORegistry.scoreBigExchangeEventDAO = scoreBigExchangeEventDAO;
	}

	public static EventAuditDAO getEventAuditDAO() {
		return eventAuditDAO;
	}

	public void setEventAuditDAO(EventAuditDAO eventAuditDAO) {
		DAORegistry.eventAuditDAO = eventAuditDAO;
	}

	public static VWVipCatsCategoryTicketDAO getVwVipCatsCategoryTicketDAO() {
		return vwVipCatsCategoryTicketDAO;
	}

	public void setVwVipCatsCategoryTicketDAO(
			VWVipCatsCategoryTicketDAO vwVipCatsCategoryTicketDAO) {
		DAORegistry.vwVipCatsCategoryTicketDAO = vwVipCatsCategoryTicketDAO;
	}

	public static VipCatsErrorDAO getVipCatsErrorDAO() {
		return vipCatsErrorDAO;
	}

	public void setVipCatsErrorDAO(VipCatsErrorDAO vipCatsErrorDAO) {
		DAORegistry.vipCatsErrorDAO = vipCatsErrorDAO;
	}

	public static VipCatsCategoryTicketDAO getVipCatsCategoryTicketDAO() {
		return vipCatsCategoryTicketDAO;
	}

	public void setVipCatsCategoryTicketDAO(
			VipCatsCategoryTicketDAO vipCatsCategoryTicketDAO) {
		DAORegistry.vipCatsCategoryTicketDAO = vipCatsCategoryTicketDAO;
	}

	public static TgCatsEbayListingDAO getTgCatsEbayListingDAO() {
		return tgCatsEbayListingDAO;
	}

	public void setTgCatsEbayListingDAO(
			TgCatsEbayListingDAO tgCatsEbayListingDAO) {
		DAORegistry.tgCatsEbayListingDAO = tgCatsEbayListingDAO;
	}


	public static MMEventMapDAO getmMEventMapDAO() {
		return mMEventMapDAO;
	}

	public void setmMEventMapDAO(MMEventMapDAO mMEventMapDAO) {
		DAORegistry.mMEventMapDAO = mMEventMapDAO;
	}

	public static AutoTNExchangeEventDAO getAutoTNExchangeEventDAO() {
		return autoTNExchangeEventDAO;
	}

	public void setAutoTNExchangeEventDAO(
			AutoTNExchangeEventDAO autoTNExchangeEventDAO) {
		DAORegistry.autoTNExchangeEventDAO = autoTNExchangeEventDAO;
	}

	public static AutoTickPickExchangeEventDAO getAutoTickPickExchangeEventDAO() {
		return autoTickPickExchangeEventDAO;
	}

	public void setAutoTickPickExchangeEventDAO(
			AutoTickPickExchangeEventDAO autoTickPickExchangeEventDAO) {
		DAORegistry.autoTickPickExchangeEventDAO = autoTickPickExchangeEventDAO;
	}

	public static AutoVividSeatExchangeEventDAO getAutoVividSeatExchangeEventDAO() {
		return autoVividSeatExchangeEventDAO;
	}

	public void setAutoVividSeatExchangeEventDAO(
			AutoVividSeatExchangeEventDAO autoVividSeatExchangeEventDAO) {
		DAORegistry.autoVividSeatExchangeEventDAO = autoVividSeatExchangeEventDAO;
	}

	public static AutoScoreBigExchangeEventDAO getAutoScoreBigExchangeEventDAO() {
		return autoScoreBigExchangeEventDAO;
	}

	public void setAutoScoreBigExchangeEventDAO(
			AutoScoreBigExchangeEventDAO autoScoreBigExchangeEventDAO) {
		DAORegistry.autoScoreBigExchangeEventDAO = autoScoreBigExchangeEventDAO;
	}

	public static MiniTNExchangeEventDAO getMiniTNExchangeEventDAO() {
		return miniTNExchangeEventDAO;
	}

	public void setMiniTNExchangeEventDAO(
			MiniTNExchangeEventDAO miniTNExchangeEventDAO) {
		DAORegistry.miniTNExchangeEventDAO = miniTNExchangeEventDAO;
	}

	public static MiniTickPickExchangeEventDAO getMiniTickPickExchangeEventDAO() {
		return miniTickPickExchangeEventDAO;
	}

	public void setMiniTickPickExchangeEventDAO(
			MiniTickPickExchangeEventDAO miniTickPickExchangeEventDAO) {
		DAORegistry.miniTickPickExchangeEventDAO = miniTickPickExchangeEventDAO;
	}

	public static MiniVividSeatExchangeEventDAO getMiniVividSeatExchangeEventDAO() {
		return miniVividSeatExchangeEventDAO;
	}

	public void setMiniVividSeatExchangeEventDAO(
			MiniVividSeatExchangeEventDAO miniVividSeatExchangeEventDAO) {
		DAORegistry.miniVividSeatExchangeEventDAO = miniVividSeatExchangeEventDAO;
	}

	public static MiniScoreBigExchangeEventDAO getMiniScoreBigExchangeEventDAO() {
		return miniScoreBigExchangeEventDAO;
	}

	public void setMiniScoreBigExchangeEventDAO(
			MiniScoreBigExchangeEventDAO miniScoreBigExchangeEventDAO) {
		DAORegistry.miniScoreBigExchangeEventDAO = miniScoreBigExchangeEventDAO;
	}

	public static VipMiniTNExchangeEventDAO getVipMiniTNExchangeEventDAO() {
		return vipMiniTNExchangeEventDAO;
	}

	public void setVipMiniTNExchangeEventDAO(
			VipMiniTNExchangeEventDAO vipMiniTNExchangeEventDAO) {
		DAORegistry.vipMiniTNExchangeEventDAO = vipMiniTNExchangeEventDAO;
	}

	public static VipMiniTickPickExchangeEventDAO getVipMiniTickPickExchangeEventDAO() {
		return vipMiniTickPickExchangeEventDAO;
	}

	public void setVipMiniTickPickExchangeEventDAO(
			VipMiniTickPickExchangeEventDAO vipMiniTickPickExchangeEventDAO) {
		DAORegistry.vipMiniTickPickExchangeEventDAO = vipMiniTickPickExchangeEventDAO;
	}

	public static VipMiniVividSeatExchangeEventDAO getVipMiniVividSeatExchangeEventDAO() {
		return vipMiniVividSeatExchangeEventDAO;
	}

	public void setVipMiniVividSeatExchangeEventDAO(
			VipMiniVividSeatExchangeEventDAO vipMiniVividSeatExchangeEventDAO) {
		DAORegistry.vipMiniVividSeatExchangeEventDAO = vipMiniVividSeatExchangeEventDAO;
	}

	public static VipMiniScoreBigExchangeEventDAO getVipMiniScoreBigExchangeEventDAO() {
		return vipMiniScoreBigExchangeEventDAO;
	}

	public void setVipMiniScoreBigExchangeEventDAO(
			VipMiniScoreBigExchangeEventDAO vipMiniScoreBigExchangeEventDAO) {
		DAORegistry.vipMiniScoreBigExchangeEventDAO = vipMiniScoreBigExchangeEventDAO;
	}
	
	public static VipAutoTNExchangeEventDAO getVipAutoTNExchangeEventDAO() {
		return vipAutoTNExchangeEventDAO;
	}

	public void setVipAutoTNExchangeEventDAO(
			VipAutoTNExchangeEventDAO vipAutoTNExchangeEventDAO) {
		DAORegistry.vipAutoTNExchangeEventDAO = vipAutoTNExchangeEventDAO;
	}

	public static VipAutoVividSeatExchangeEventDAO getVipAutoVividSeatExchangeEventDAO() {
		return vipAutoVividSeatExchangeEventDAO;
	}

	public void setVipAutoVividSeatExchangeEventDAO(
			VipAutoVividSeatExchangeEventDAO vipAutoVividSeatExchangeEventDAO) {
		DAORegistry.vipAutoVividSeatExchangeEventDAO = vipAutoVividSeatExchangeEventDAO;
	}

	public static VipAutoScoreBigExchangeEventDAO getVipAutoScoreBigExchangeEventDAO() {
		return vipAutoScoreBigExchangeEventDAO;
	}

	public void setVipAutoScoreBigExchangeEventDAO(
			VipAutoScoreBigExchangeEventDAO vipAutoScoreBigExchangeEventDAO) {
		DAORegistry.vipAutoScoreBigExchangeEventDAO = vipAutoScoreBigExchangeEventDAO;
	}

	public static ExchangeEventAuditDAO getExchangeEventAuditDAO() {
		return exchangeEventAuditDAO;
	}

	public void setExchangeEventAuditDAO(
			ExchangeEventAuditDAO exchangeEventAuditDAO) {
		DAORegistry.exchangeEventAuditDAO = exchangeEventAuditDAO;
	}

	public static VipAutoCatsErrorDAO getVipAutoCatsErrorDAO() {
		return vipAutoCatsErrorDAO;
	}

	public void setVipAutoCatsErrorDAO(
			VipAutoCatsErrorDAO vipAutoCatsErrorDAO) {
		DAORegistry.vipAutoCatsErrorDAO = vipAutoCatsErrorDAO;
	}

	public static VwVipAutoCategoryTicketDAO getVwVipAutoCategoryTicketDAO() {
		return vwVipAutoCategoryTicketDAO;
	}

	public void setVwVipAutoCategoryTicketDAO(
			VwVipAutoCategoryTicketDAO vwVipAutoCategoryTicketDAO) {
		DAORegistry.vwVipAutoCategoryTicketDAO = vwVipAutoCategoryTicketDAO;
	}

	public static GlobalAutoPricingAuditDAO getGlobalAutoPricingAuditDAO() {
		return globalAutoPricingAuditDAO;
	}

	public void setGlobalAutoPricingAuditDAO(
			GlobalAutoPricingAuditDAO globalAutoPricingAuditDAO) {
		DAORegistry.globalAutoPricingAuditDAO = globalAutoPricingAuditDAO;
	}

	public static LastRowMiniTNExchangeEventDAO getLastRowMiniTNExchangeEventDAO() {
		return lastRowMiniTNExchangeEventDAO;
	}

	public void setLastRowMiniTNExchangeEventDAO(
			LastRowMiniTNExchangeEventDAO lastRowMiniTNExchangeEventDAO) {
		DAORegistry.lastRowMiniTNExchangeEventDAO = lastRowMiniTNExchangeEventDAO;
	}
	

	public static ZonesPricingTNExchangeEventDAO getZonesPricingTNExchangeEventDAO() {
		return zonesPricingTNExchangeEventDAO;
	}

	public void setZonesPricingTNExchangeEventDAO(
			ZonesPricingTNExchangeEventDAO zonesPricingTNExchangeEventDAO) {
		DAORegistry.zonesPricingTNExchangeEventDAO = zonesPricingTNExchangeEventDAO;
	}
	

	public static ZonedLastRowMiniTNExchangeEventDAO getZonedLastRowMiniTNExchangeEventDAO() {
		return zonedLastRowMiniTNExchangeEventDAO;
	}

	public void setZonedLastRowMiniTNExchangeEventDAO(
			ZonedLastRowMiniTNExchangeEventDAO zonedLastRowMiniTNExchangeEventDAO) {
		DAORegistry.zonedLastRowMiniTNExchangeEventDAO = zonedLastRowMiniTNExchangeEventDAO;
	}

	public static VWLastRowMiniCatsCategoryTicketDAO getVwLastRowMiniCatsCategoryTicketDAO() {
		return vwLastRowMiniCatsCategoryTicketDAO;
	}

	public void setVwLastRowMiniCatsCategoryTicketDAO(
			VWLastRowMiniCatsCategoryTicketDAO vwLastRowMiniCatsCategoryTicketDAO) {
		DAORegistry.vwLastRowMiniCatsCategoryTicketDAO = vwLastRowMiniCatsCategoryTicketDAO;
	}

	public static LastRowMiniCatsErrorDAO getLastRowMiniCatsErrorDAO() {
		return lastRowMiniCatsErrorDAO;
	}

	public void setLastRowMiniCatsErrorDAO(
			LastRowMiniCatsErrorDAO lastRowMiniCatsErrorDAO) {
		DAORegistry.lastRowMiniCatsErrorDAO = lastRowMiniCatsErrorDAO;
	}
	
	public static TnExchangeEventZoneGroupTicketDAO getTnExchangeEventZoneGroupTicketDAO() {
		return tnExchangeEventZoneGroupTicketDAO;
	}

	public void setTnExchangeEventZoneGroupTicketDAO(
			TnExchangeEventZoneGroupTicketDAO tnExchangeEventZoneGroupTicketDAO) {
		DAORegistry.tnExchangeEventZoneGroupTicketDAO = tnExchangeEventZoneGroupTicketDAO;
	}

	public static ZonePricingErrorLogDAO getZonePricingErrorLogDAO() {
		return zonePricingErrorLogDAO;
	}

	public void setZonePricingErrorLogDAO(
			ZonePricingErrorLogDAO zonePricingErrorLogDAO) {
		DAORegistry.zonePricingErrorLogDAO = zonePricingErrorLogDAO;
	}

	public static TnExchangeEventZoneFloorCapDAO getTnExchangeEventZoneFloorCapDAO() {
		return tnExchangeEventZoneFloorCapDAO;
	}

	public void setTnExchangeEventZoneFloorCapDAO(
			TnExchangeEventZoneFloorCapDAO tnExchangeEventZoneFloorCapDAO) {
		DAORegistry.tnExchangeEventZoneFloorCapDAO = tnExchangeEventZoneFloorCapDAO;
	}

	public static TnExchangeTheatreEventZonesDAO getTnExchangeTheatreEventZonesDAO() {
		return tnExchangeTheatreEventZonesDAO;
	}

	public void setTnExchangeTheatreEventZonesDAO(
			TnExchangeTheatreEventZonesDAO tnExchangeTheatreEventZonesDAO) {
		DAORegistry.tnExchangeTheatreEventZonesDAO = tnExchangeTheatreEventZonesDAO;
	}

	public static TnFloorCapEventVenueDAO getTnFloorCapEventVenueDAO() {
		return tnFloorCapEventVenueDAO;
	}

	public void setTnFloorCapEventVenueDAO(
			TnFloorCapEventVenueDAO tnFloorCapEventVenueDAO) {
		DAORegistry.tnFloorCapEventVenueDAO = tnFloorCapEventVenueDAO;
	}

	public static TnFloorCapEventVenueAuditDAO getTnFloorCapEventVenueAuditDAO() {
		return tnFloorCapEventVenueAuditDAO;
	}

	public void setTnFloorCapEventVenueAuditDAO(
			TnFloorCapEventVenueAuditDAO tnFloorCapEventVenueAuditDAO) {
		DAORegistry.tnFloorCapEventVenueAuditDAO = tnFloorCapEventVenueAuditDAO;
	}

	public static ZonedLastRowMiniCategoryTicketDAO getZonedLastRowMiniCategoryTicketDAO() {
		return zonedLastRowMiniCategoryTicketDAO;
	}

	public void setZonedLastRowMiniCategoryTicketDAO(
			ZonedLastRowMiniCategoryTicketDAO zonedLastRowMiniCategoryTicketDAO) {
		DAORegistry.zonedLastRowMiniCategoryTicketDAO = zonedLastRowMiniCategoryTicketDAO;
	}

	public static ZonesPricingCategoryTicketDAO getZonesPricingCategoryTicketDAO() {
		return zonesPricingCategoryTicketDAO;
	}

	public void setZonesPricingCategoryTicketDAO(
			ZonesPricingCategoryTicketDAO zonesPricingCategoryTicketDAO) {
		DAORegistry.zonesPricingCategoryTicketDAO = zonesPricingCategoryTicketDAO;
	}


	
	public static TnExchangeEventZoneFloorCapAuditDAO getTnExchangeEventZoneFloorCapAuditDAO() {
		return tnExchangeEventZoneFloorCapAuditDAO;
	}

	public void setTnExchangeEventZoneFloorCapAuditDAO(
			TnExchangeEventZoneFloorCapAuditDAO tnExchangeEventZoneFloorCapAuditDAO) {
		DAORegistry.tnExchangeEventZoneFloorCapAuditDAO = tnExchangeEventZoneFloorCapAuditDAO;
	}
	
	public static ZonesPricingFloorCapDAO getZonesPricingFloorCapDAO() {
		return zonesPricingFloorCapDAO;
	}

	public void setZonesPricingFloorCapDAO(
			ZonesPricingFloorCapDAO zonesPricingFloorCapDAO) {
		DAORegistry.zonesPricingFloorCapDAO = zonesPricingFloorCapDAO;
	}

	public static ZonesPricingFloorCapAuditDAO getZonesPricingFloorCapAuditDAO() {
		return zonesPricingFloorCapAuditDAO;
	}

	public void setZonesPricingFloorCapAuditDAO(
			ZonesPricingFloorCapAuditDAO zonesPricingFloorCapAuditDAO) {
		DAORegistry.zonesPricingFloorCapAuditDAO = zonesPricingFloorCapAuditDAO;
	}

	public static JKTZonesPricingCategoryTicketDAO getJktZonesPricingCategoryTicketDAO() {
		return jktZonesPricingCategoryTicketDAO;
	}

	public void setJktZonesPricingCategoryTicketDAO(
			JKTZonesPricingCategoryTicketDAO jktZonesPricingCategoryTicketDAO) {
		DAORegistry.jktZonesPricingCategoryTicketDAO = jktZonesPricingCategoryTicketDAO;
	}

	public static UserBrokerDetailsDAO getUserBrokerDetailsDAO() {
		return userBrokerDetailsDAO;
	}

	public void setUserBrokerDetailsDAO(
			UserBrokerDetailsDAO userBrokerDetailsDAO) {
		DAORegistry.userBrokerDetailsDAO = userBrokerDetailsDAO;
	}

	public static ZoneEventDAO getZoneEventDAO() {
		return zoneEventDAO;
	}

	public void setZoneEventDAO(ZoneEventDAO zoneEventDAO) {
		DAORegistry.zoneEventDAO = zoneEventDAO;
	}

	public static PresaleEventDAO getPresaleEventDAO() {
		return presaleEventDAO;
	}

	public void setPresaleEventDAO(PresaleEventDAO presaleEventDAO) {
		DAORegistry.presaleEventDAO = presaleEventDAO;
	}

	public static ActiveEventAndActiveCrawlsDAO getActiveEventAndActiveCrawlsDAO() {
		return activeEventAndActiveCrawlsDAO;
	}

	public  void setActiveEventAndActiveCrawlsDAO(
			ActiveEventAndActiveCrawlsDAO activeEventAndActiveCrawlsDAO) {
		DAORegistry.activeEventAndActiveCrawlsDAO = activeEventAndActiveCrawlsDAO;
	}

	public static SoldTicketDetailDAO getSoldTicketDetailDAO() {
		return soldTicketDetailDAO;
	}

	public void setSoldTicketDetailDAO(
			SoldTicketDetailDAO soldTicketDetailDAO) {
		DAORegistry.soldTicketDetailDAO = soldTicketDetailDAO;
	}

	public static AutopricingProductDAO getAutopricingProductDAO() {
		return autopricingProductDAO;
	}

	public void setAutopricingProductDAO(
			AutopricingProductDAO autopricingProductDAO) {
		DAORegistry.autopricingProductDAO = autopricingProductDAO;
	}

	public static MiniExchangeEventDAO getMiniExchangeEventDAO() {
		return miniExchangeEventDAO;
	}

	public void setMiniExchangeEventDAO(MiniExchangeEventDAO miniExchangeEventDAO) {
		DAORegistry.miniExchangeEventDAO = miniExchangeEventDAO;
	}

	public static VipMiniExchangeEventDAO getVipMiniExchangeEventDAO() {
		return vipMiniExchangeEventDAO;
	}

	public void setVipMiniExchangeEventDAO(VipMiniExchangeEventDAO vipMiniExchangeEventDAO) {
		DAORegistry.vipMiniExchangeEventDAO = vipMiniExchangeEventDAO;
	}

	public static LastRowMiniExchangeEventDAO getLastRowMiniExchangeEventDAO() {
		return lastRowMiniExchangeEventDAO;
	}

	public void setLastRowMiniExchangeEventDAO(LastRowMiniExchangeEventDAO lastRowMiniExchangeEventDAO) {
		DAORegistry.lastRowMiniExchangeEventDAO = lastRowMiniExchangeEventDAO;
	}

	public static LarryLastExchangeEventDAO getLarryLastExchangeEventDAO() {
		return larryLastExchangeEventDAO;
	}

	public void setLarryLastExchangeEventDAO(LarryLastExchangeEventDAO larryLastExchangeEventDAO) {
		DAORegistry.larryLastExchangeEventDAO = larryLastExchangeEventDAO;
	}

	public static ZoneLastRowMiniExchangeEventDAO getZoneLastRowMiniExchangeEventDAO() {
		return zoneLastRowMiniExchangeEventDAO;
	}

	public void setZoneLastRowMiniExchangeEventDAO(ZoneLastRowMiniExchangeEventDAO zoneLastRowMiniExchangeEventDAO) {
		DAORegistry.zoneLastRowMiniExchangeEventDAO = zoneLastRowMiniExchangeEventDAO;
	}

	public static ZonePricingExchangeEventDAO getZonePricingExchangeEventDAO() {
		return zonePricingExchangeEventDAO;
	}

	public void setZonePricingExchangeEventDAO(ZonePricingExchangeEventDAO zonePricingExchangeEventDAO) {
		DAORegistry.zonePricingExchangeEventDAO = zonePricingExchangeEventDAO;
	}

	public static ZoneTicketsProcessorExchangeEventDAO getZoneTicketsProcessorExchangeEventDAO() {
		return zoneTicketsProcessorExchangeEventDAO;
	}

	public void setZoneTicketsProcessorExchangeEventDAO(ZoneTicketsProcessorExchangeEventDAO zoneTicketsProcessorExchangeEventDAO) {
		DAORegistry.zoneTicketsProcessorExchangeEventDAO = zoneTicketsProcessorExchangeEventDAO;
	}

	public static StubHubApiTrackingDAO getStubHubApiTrackingDAO() {
		return stubHubApiTrackingDAO;
	}

	public void setStubHubApiTrackingDAO(
			StubHubApiTrackingDAO stubHubApiTrackingDAO) {
		DAORegistry.stubHubApiTrackingDAO = stubHubApiTrackingDAO;
	}

	public static TicketMasterEventDAO getTicketMasterEventDAO() {
		return ticketMasterEventDAO;
	}

	public  void setTicketMasterEventDAO(
			TicketMasterEventDAO ticketMasterEventDAO) {
		DAORegistry.ticketMasterEventDAO = ticketMasterEventDAO;
	}

	public static TicketMasterEventsArtistDAO getTicketMasterEventsArtistDAO() {
		return ticketMasterEventsArtistDAO;
	}

	public  void setTicketMasterEventsArtistDAO(
			TicketMasterEventsArtistDAO ticketMasterEventsArtistDAO) {
		DAORegistry.ticketMasterEventsArtistDAO = ticketMasterEventsArtistDAO;
	}

	public static VenueMapZonesDAO getVenueMapZonesDAO() {
		return venueMapZonesDAO;
	}

	public void setVenueMapZonesDAO(VenueMapZonesDAO venueMapZonesDAO) {
		DAORegistry.venueMapZonesDAO = venueMapZonesDAO;
	}

	public static EventHitAuditDAO getEventHitAuditDAO() {
		return eventHitAuditDAO;
	}

	public void setEventHitAuditDAO(EventHitAuditDAO eventHitAuditDAO) {
		DAORegistry.eventHitAuditDAO = eventHitAuditDAO;
	}

	public static VenueSynonymDAO getVenueSynonymDAO() {
		return venueSynonymDAO;
	}

	public void setVenueSynonymDAO(VenueSynonymDAO venueSynonymDAO) {
		DAORegistry.venueSynonymDAO = venueSynonymDAO;
	}

	public static ProbableVenueDAO getProbableVenueDAO() {
		return probableVenueDAO;
	}

	public void setProbableVenueDAO(ProbableVenueDAO probableVenueDAO) {
		DAORegistry.probableVenueDAO = probableVenueDAO;
	}

	public static TixcityUnfilledOrderSummaryDAO getTixcityUnfilledOrderSummaryDAO() {
		return tixcityUnfilledOrderSummaryDAO;
	}

	public void setTixcityUnfilledOrderSummaryDAO(
			TixcityUnfilledOrderSummaryDAO tixcityUnfilledOrderSummaryDAO) {
		DAORegistry.tixcityUnfilledOrderSummaryDAO = tixcityUnfilledOrderSummaryDAO;
	}

	public static VividSeatsTicketInventoryDAO getVividSeatsTicketInventoryDAO() {
		return vividSeatsTicketInventoryDAO;
	}

	public void setVividSeatsTicketInventoryDAO(VividSeatsTicketInventoryDAO vividSeatsTicketInventoryDAO) {
		DAORegistry.vividSeatsTicketInventoryDAO = vividSeatsTicketInventoryDAO;
	}
	
	
}