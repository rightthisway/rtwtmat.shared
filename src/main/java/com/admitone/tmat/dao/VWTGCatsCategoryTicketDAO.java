package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.VWTGCatsCategoryTicket;

public interface VWTGCatsCategoryTicketDAO extends RootDAO<Integer, VWTGCatsCategoryTicket> {

	public Collection<VWTGCatsCategoryTicket> getAllActiveTnTgCatsCategoryTickets();
}
