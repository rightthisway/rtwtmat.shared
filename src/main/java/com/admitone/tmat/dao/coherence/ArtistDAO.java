package com.admitone.tmat.dao.coherence;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.Artist;
import com.admitone.tmat.enums.EventStatus;
import com.tangosol.net.CacheFactory;

public class ArtistDAO extends CoherenceDAO<Integer, Artist> implements com.admitone.tmat.dao.ArtistDAO {
	public ArtistDAO() {
		super();
		cache = CacheFactory.getCache("ticketCache");
	}
	
	public void deleteEmptyArtists() {
		// TODO Auto-generated method stub
		
	}

	public Collection<Artist> filterByName(String pattern) {
		// TODO Auto-generated method stub
		return null;
	}

	public int getArtistCount() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Artist getArtistByStubhubId(Integer stubhubId) {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<Artist> searchArtists(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<Artist> filterByName(String pattern,
			EventStatus eventStatus) {
		// TODO Auto-generated method stub
		return null;
	}

    public Artist getArtistByName(String name) {
    	return null;
    }

    public Collection<Artist> getAllActiveArtists() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Artist> searchArtistsByWord(String name) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Artist> getAllActiveArtistsByGrandChildCategoryId(
			Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Artist> getAllActiveArtistsByGrandChildCategorys(
			List<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Artist> getArtistsByIds(List<Integer> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void bulkDelete(List<Integer> ids) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public Collection<Artist> getAllArtistsBySqlQuery(){
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Artist> getAllArtistsWithGrandChildBySqlQuery() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<Artist> getAllActiveArtistsWithEvents() {
		// TODO Auto-generated method stub
		return null;
	}
}
