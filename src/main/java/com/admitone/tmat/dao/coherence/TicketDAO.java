package com.admitone.tmat.dao.coherence;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.data.UserOutlier;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.pojo.SimpleTicket;
import com.tangosol.net.CacheFactory;


/*
 * Please use this DAO only to write to the cache (Since we use write behind scheme
 * the data will be written to the db after the specified delay.
 */

public class TicketDAO extends CoherenceDAO<Integer, Ticket> implements com.admitone.tmat.dao.TicketDAO{
	
	public TicketDAO() {
		super();
		cache = CacheFactory.getCache("com.admitone.tmat.data.Ticket");
	}

	
	public Collection<Ticket> getAllTicketsByEvent(int event_id) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllTicketsMatchingUserAlert(UserAlert userAlert) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllTicketsByEventAndSiteId(int event_id,
			String siteId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllTicketsByEvent(int event_id,
			TicketStatus status) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllBuyItNowTickets(TicketStatus status) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllOutDatedExpiredTickets() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllOutDatedExpiredTicketsFromCrawl(
			Integer crawlId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void updateStatusOfOutdatedExpiredTicketsFromCrawl(Integer crawlId,
			TicketStatus ticketStatus) {
		// TODO Auto-generated method stub
		
	}

	
	public Collection<Ticket> getAllActiveTicketsUpdatedBefore(Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsUpdatedBeforeFromCrawl(
			Integer crawl_id, Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void updateStatusOfTicketsUpdatedBeforeFromCrawl(Integer crawlId,
			Date date, TicketStatus ticketStatus) {
		// TODO Auto-generated method stub
		
	}

	
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveRegularTicketsByEvent(Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsBySiteId(String siteId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsByEventAndSiteIds(
			Integer eventId, String siteId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId,
			Integer categoryId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllCompletedTicketsByEvent(Integer eventId,
			List<Integer> categoryIds) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId,
			Integer categoryId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsByEvent(Integer eventId,
			List<Integer> categoryIds) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllTicketsByEvent(Integer eventId,
			Integer categoryId, String day) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getUncategorizedCompletedTicketsByEvent(
			Integer eventId, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getUncategorizedActiveTicketsByEvent(
			Integer eventId, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getCategorizedCompletedTicketsByEvent(
			Integer eventId, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getCategorizedActiveTicketsByEvent(
			Integer eventId, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Object[]> getSectionsAndRowsFromEventId(Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsByTour(Integer tourId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public void deleteTicketsByCrawlId(int crawlId) {
		// TODO Auto-generated method stub
		
	}

	
	public void deleteTicketsByCrawlId(int crawlId, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		
	}

	
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(
			Integer eventId, Integer categoryId, String[] siteIds,
			Integer quantity, Integer lotSize, String section, String row,
			Date startDate, Date endDate,
			RemoveDuplicatePolicy removeDuplicatePolicy,
			boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(
			Integer eventId, Integer categoryId, String[] siteIds,
			List<Integer> quantities, Integer lotSize, String section,
			String row, Date startDate, Date endDate,
			RemoveDuplicatePolicy removeDuplicatePolicy,
			boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getEventCategorySiteQuantityLotTickets(
			Integer eventId, List<Integer> categoryId, String[] siteIds,
			List<Integer> quantities, Integer lotSize, String section,
			String row, Date startDate, Date endDate,
			RemoveDuplicatePolicy removeDuplicatePolicy,
			boolean keepOnlyActive, boolean removeA1Tix, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(
			Integer eventId, Integer categoryId, Integer lotSize,
			String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public ArrayList<SimpleTicket> getEventCategoryLotSimpleTickets(
			Integer eventId, Integer categoryId, Integer lotSize,
			boolean removeA1Tix, String catScheme) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public int getTicketCount(TicketStatus status) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public int getTicketCountByEvent(int event_id, TicketStatus status) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public int getTicketQuantitySumByEvent(int event_id, TicketStatus status) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public int getTicketCountByCrawl(int crawl_id, TicketStatus status) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public int getTicketCountByCrawl(int crawl_id, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public int getTicketQuantitySumByCrawl(int crawl_id, TicketStatus status) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public int getTicketCountByCrawl(int crawl_id) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public int getTicketQuantitySumByCrawl(int crawl_id) {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public void deleteTicketsByEventId(Integer eventId) {
		// TODO Auto-generated method stub
		
	}

	
	public void enableTickets(Integer crawlerId) {
		// TODO Auto-generated method stub
		
	}

	
	public void disableTickets(Integer crawlerId) {
		// TODO Auto-generated method stub
		
	}
	
	public void disableTicketsByCrawlId(int crawlId, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
	}

	
	public void disableAndUnassignCrawlTickets(Integer crawlId) {
		// TODO Auto-generated method stub
		
	}

	
	public void recomputeTicketRemainingQuantities() {
		// TODO Auto-generated method stub
		
	}

	
	public void saveOrUpdateAllTicketsWithSameEventAndSite(
			Collection<Ticket> tickets) {
		// TODO Auto-generated method stub
		
	}

	
	public Collection<Ticket> getAllTicketsByEventAndDate(Integer eventId,
			Date date) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllTicketsByEventUpdatedAfter(int event_id,
			Date afterDate) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveTicketsByEventAndSiteId(int event_id,
			String siteId) {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Collection<Ticket> getAllActiveNonZonesTicketsByEvent(Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}

	public Collection<Ticket> getAllTicketsMatchingUserOutlier(UserOutlier userOutlier) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public List<Ticket> getAllTmatTicketsById(Integer Id){
		return null;
	}


	@Override
	public String getTicketDeliveryType(String exchangeName) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Collection<Ticket> getAllActiveTicketForAutoCatsByEventId(
			Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Collection<Ticket> getAllActiveInstantTicketForAutoCatByEventId(
			Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Collection<Ticket> getAllActiveTicketForAutoCatSalesByEventId(
			Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public Collection<Ticket> getAllActiveInstantTicketForAutoCatSalesByEventId(
			Integer eventId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public Collection<Ticket> getAllActiveTicketsBySectionAndEventId(Integer eventId, String section){
		return null;
	}
	

	public Collection<Ticket> getAllActiveTicketsByNormalizedSectionAndEventId(Integer eventId, String section){
		return null;
	}

}
