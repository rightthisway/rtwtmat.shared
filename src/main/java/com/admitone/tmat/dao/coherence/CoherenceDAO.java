package com.admitone.tmat.dao.coherence;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;

public abstract class CoherenceDAO<K extends Serializable, T extends Serializable> {
    protected Class<T> domainClass = getDomainClass();
    protected NamedCache cache;

	public CoherenceDAO() {
		this.cache = CacheFactory.getCache(domainClass.getName());
	}

    public T get(K id) {
    	return (T)cache.get((Serializable)id);
    }

    public Collection<T> getAll() {
    	return null;
    }
    
    public void flush() {
    	
    }

    public K save(T entity) {
    	Object id = getIdFromEntity(entity);
    	cache.put((Serializable)id, entity);
    	return (K)id;
    }

    public void saveOrUpdate(T entity) {
    	save(entity);
    }

    public void update(T entity) {
    	save(entity);
    }

    public void delete(T entity) {
    	Object id = getIdFromEntity(entity);
		cache.remove(id);
    }
    
    public void saveOrUpdateAll(Collection<T> list) {
    	if(list != null){
    		System.out.println("In CoherenceDAO, Savingorupdateall into the cache. The size of the list is: " + list.size());
    	}
    	Map<Object, Object> map = new HashMap<Object, Object>();
    	for (T entity: list) {
        	Object id = getIdFromEntity(entity);
        	map.put(id, entity);
    	}
    	cache.putAll(map);
    }

    public void deleteById(K id) {
    	cache.remove(id);
    }
    
    protected Class getDomainClass() {
        if (domainClass == null) {
            ParameterizedType thisType = (ParameterizedType) getClass().getGenericSuperclass();

            // get T, so the 2nd class
            domainClass = (Class) thisType.getActualTypeArguments()[1];
        }
        return domainClass;
    }
    
    private Object getIdFromEntity(Object entity) {
		try {
			return entity.getClass().getMethod("getItemId", null).invoke(entity, null);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
    }

    public void saveAll(Collection<T> entities) {
    	//throw new RuntimeException("Not implemented yet!");
    	Map<Object, Object> map = new HashMap<Object, Object>();
    	for (T entity: entities) {
        	Object id = getIdFromEntity(entity);
        	map.put(id, entity);
    	}
    	cache.putAll(map);
    }

    public boolean updateAll(Collection<T> entities) {
    	//throw new RuntimeException("Not implemented yet!");
    	try{
	    	Map<Object, Object> map = new HashMap<Object, Object>();
	    	for (T entity: entities) {
	        	Object id = getIdFromEntity(entity);
	        	map.put(id, entity);
	    	}
	    	cache.putAll(map);
    	}catch (Exception e) {
    		return false;
    	}
    	return true;
    }

    public void deleteAll(Collection<T> entities) {
    	//throw new RuntimeException("Not implemented yet!");

    	for (T entity: entities) {
        	Object id = getIdFromEntity(entity);
        	cache.remove(id);
    	}
   	
    }

}
