package com.admitone.tmat.dao.coherence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.Transaction;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.tangosol.net.GuardSupport;
import com.tangosol.net.cache.CacheStore;

/**
 * This class is invoked when the followings occur on the cache:
 * - entry not found in the cache => get()
 * - entry deleted (write behind)
 * - entry updated (write behind)
 *
 */
public class HibernateCacheStore implements CacheStore {
	private String entityName;
	private Class entityClass; 
	private static HibernateTemplate hibernateTemplate;

	public HibernateCacheStore() {}
	
	public HibernateCacheStore(String entityName) {
		this.entityName = entityName;
		
		try {
			this.entityClass = Class.forName(entityName);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public void erase(Object id) {
		Object entity = getHibernateTemplate().get(entityClass, (Serializable)id);
		getHibernateTemplate().delete(entity);
	}

	// Can probably be optimized
	public void eraseAll(Collection keyCollection) {
		Collection<Object> entities = new ArrayList<Object>();
		for (Object id: keyCollection) {
			Object entity = getHibernateTemplate().get(entityClass, (Serializable)id);
			if (entity != null) {
				entities.add(entity);
			}
		}
		
		getHibernateTemplate().deleteAll(entities);
	}

	public void store(Object id, Object entity) {
		GuardSupport.heartbeat();
		try{
			Transaction transaction = getHibernateTemplate().getSessionFactory().getCurrentSession().getTransaction();
			transaction.setTimeout(30);
			transaction.begin();
			getHibernateTemplate().saveOrUpdate(entity);
			transaction.commit();
		}catch(Exception e){
			System.out.println("----------------Hibernate Cache store exception--------------------");
			System.out.println("Exception in store method of HibernateCacheStore while saving entity");
			e.printStackTrace();
			System.out.println("----------------Hibernate Cache store exception--------------------");
		}
	}

	public void storeAll(Map map) {
		//getHibernateTemplate().saveOrUpdateAll(map.values());
		for(Object entity : map.values()){
			GuardSupport.heartbeat();
			try{
				Transaction transaction = getHibernateTemplate().getSessionFactory().getCurrentSession().getTransaction();
				transaction.setTimeout(30);
				transaction.begin();
				getHibernateTemplate().saveOrUpdate(entity);
				transaction.commit();
			}catch(Exception e){
				System.out.println("----------------Hibernate Cache store exception--------------------");
				System.out.println("Exception in storeAll method of HibernateCacheStore while saving entity");
				e.printStackTrace();
				System.out.println("----------------Hibernate Cache store exception--------------------");
			}
		}
	}

	public Object load(Object id) {
		return getHibernateTemplate().get(entityClass, (Serializable)id);
	}

	public Map loadAll(Collection keyCollection) {
		Map<Object, Object> entityMap = new HashMap<Object, Object>();
		
		for (Object id: keyCollection) {
			Object entity = getHibernateTemplate().get(entityClass, (Serializable)id);
			if (entity != null) {
				entityMap.put(id, entity);
			}
		}
		return entityMap;
	}
	
	public void setHibernateTemplate(HibernateTemplate template) {
		HibernateCacheStore.hibernateTemplate = template;
	}
	
	public HibernateTemplate getHibernateTemplate() {
		return hibernateTemplate;
	}
	
    public void updateAll(Collection entities) {
    	//throw new RuntimeException("Not implemented yet!");
    	getHibernateTemplate().saveOrUpdateAll(entities);
    }

    public void deleteAll(Collection entities) {
    	//throw new RuntimeException("Not implemented yet!");
    	getHibernateTemplate().deleteAll(entities);
    }

}
