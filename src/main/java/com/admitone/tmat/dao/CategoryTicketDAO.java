package com.admitone.tmat.dao;

import com.admitone.tmat.data.CategoryTicket;

public interface CategoryTicketDAO extends RootDAO<Integer, CategoryTicket> {

}

