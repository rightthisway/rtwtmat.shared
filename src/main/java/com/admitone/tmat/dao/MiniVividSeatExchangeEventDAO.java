package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.MiniVividSeatExchangeEvent;

public interface MiniVividSeatExchangeEventDAO extends RootDAO<Integer, MiniVividSeatExchangeEvent> {
	
	public Collection<MiniVividSeatExchangeEvent> getAllMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds);
	public List<MiniVividSeatExchangeEvent> getAllActiveMiniVividSeatExchangeEvent();
	public Collection<MiniVividSeatExchangeEvent> getAllActiveMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<MiniVividSeatExchangeEvent> getAllDeletedMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllMiniVividSeatExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<MiniVividSeatExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String mini_exposure_vivid);
	public void updateRptFactor(String rpt_vivid);
	public void updatePriceBreakup(String pricebreakup_vivid);
	public void updateUpperMarkup(String upper_markpu_vivid);
	public void updateLowerMarkup(String lower_markpu_vivid);
	public void updateUpperShippingFees(String upper_shippingfees_vivid);
	public void updateLowerShippingFees(String lower_shippingfees_vivid);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
	
}
