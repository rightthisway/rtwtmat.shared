package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TNTGCatsCategoryEventAudit;

public interface TNTGCatsCategoryEventAuditDAO extends RootDAO<Integer, TNTGCatsCategoryEventAudit> {

	public List<TNTGCatsCategoryEventAudit> getTGCatsCategoryEventAuditByEventId(Integer eId);
	
}
