package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.UserOutlier;
import com.admitone.tmat.enums.UserOutlierStatus;

public interface UserOutlierDAO extends RootDAO<Integer, UserOutlier> {
	Collection<UserOutlier> getOutliersByMarketMakerAndArtistId(String username, Integer tourId, UserOutlierStatus userOutlierStatus);
	public Collection<UserOutlier> getOutliersByUsernameAndArtistId(String username, Integer tourId, UserOutlierStatus userOutlierStatus);
	Collection<UserOutlier> getOutliersByUsernameAndEventId(String username, Integer eventId, UserOutlierStatus userOutlierStatus);
	Collection<UserOutlier> getOutliersByMarketMakerAndEventId(String username, Integer eventId,  UserOutlierStatus userOutlierStatus); 
	Collection<UserOutlier> getAllOutliers(UserOutlierStatus outlierStatus);
}