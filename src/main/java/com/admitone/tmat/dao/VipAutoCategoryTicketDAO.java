package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.VipAutoCategoryTicket;

public interface VipAutoCategoryTicketDAO extends RootDAO<Integer, VipAutoCategoryTicket> {

	public Collection<VipAutoCategoryTicket> getAllActiveVipAutoCategoryTickets();
	public List<VipAutoCategoryTicket> getAllVipAutoCategoryTicketsByAll(Integer eventId, String section, String row, String quantity);
	public List<VipAutoCategoryTicket> getAllVipAutoCategoryTicketsById(Integer Id);
}
