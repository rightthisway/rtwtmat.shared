package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.EbayInventory;

public interface EbayInventoryDAO extends RootDAO<Integer, EbayInventory> {
	Collection<EbayInventory> getEbayInventoriesByEventUsername(Integer eventId, String username);
	Collection<EbayInventory> getEbayInventoriesByGroupId(Integer groupId);
	EbayInventory getEbayInventoriesByGroupIdAndRank(Integer groupId, Integer rank);
	void deleteByGroupId(Integer groupId);
	void deleteEbayInventoryByEventId(Integer eventId);
}