package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TMATDependentArtist;

public interface TMATDependentArtistDAO extends RootDAO<Integer, TMATDependentArtist> {
	List<TMATDependentArtist> getTmatDependentsByArtistId(Integer artistId);

}
