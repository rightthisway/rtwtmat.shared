package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.Venue;

public interface VenueDAO extends RootDAO<Integer, Venue> {
	Collection<Venue> getVenues(String name);
	Venue getVenue(String name);
	Venue getVenueByStubhubId(Integer stubhubId);
	void deleteEmptyVenues();
	Collection<Venue> filterByVenue(String pattern);
	Collection<Venue> filterVenue(String venue,String city, String state, String country, String venueType);
	Collection<Venue> getVenueByTour(Integer tourId);
	Collection<Venue> getVenueById(Integer id);
	public void deleteVenue(Integer venueId);
	public Collection<Venue> getAllVenuesForUSandCA();
	public Collection<Venue> getAllVenuesByEventActiveStatus();
}