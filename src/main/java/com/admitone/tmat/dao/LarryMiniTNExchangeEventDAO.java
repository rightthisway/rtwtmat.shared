package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.LarryMiniTNExchangeEvent;

public interface LarryMiniTNExchangeEventDAO extends RootDAO<Integer, LarryMiniTNExchangeEvent> {
	
	public Collection<LarryMiniTNExchangeEvent> getAllLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds);
	public List<LarryMiniTNExchangeEvent> getAllActiveLarryMiniTNExchangeEvent();
	public Collection<LarryMiniTNExchangeEvent> getAllActiveLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<LarryMiniTNExchangeEvent> getAllDeletedLarryMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllLarryMiniTNExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<LarryMiniTNExchangeEvent> tnEvents) throws Exception;
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
	
}
