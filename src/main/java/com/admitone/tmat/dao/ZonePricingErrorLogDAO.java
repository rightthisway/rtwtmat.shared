package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.ZonePricingErrorLog;

public interface ZonePricingErrorLogDAO extends RootDAO<Integer, ZonePricingErrorLog> {

	List<String> getDistinctVenuesByErrorType(String errorType);
	List<ZonePricingErrorLog> getAllFailedZonesByErrorType(String errorType);
	List<ZonePricingErrorLog> getAllFailedFCByErrorType(String errorType1,String errorType2);
}
