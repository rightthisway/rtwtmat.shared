package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TNTGCatsCategoryEvent;

public interface TNTGCatsCategoryEventDAO extends RootDAO<Integer, TNTGCatsCategoryEvent> {
	
	public Collection<TNTGCatsCategoryEvent> getAllTGCatsCategoryEventByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllTGCatsCategoryEventByEvent(List<Integer> eventIds);
	public List<TNTGCatsCategoryEvent> getAllActiveTGCatsCategoryEvent();
	
}
