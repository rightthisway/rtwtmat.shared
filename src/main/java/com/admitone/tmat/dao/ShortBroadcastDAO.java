package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.ShortBroadcast;

public interface ShortBroadcastDAO extends RootDAO<Integer, ShortBroadcast> {
	Collection<ShortBroadcast> getAllBroadcastsByEvent(int eventId);
	void deleteBroadcastsByEventId(Integer eventId);
	Collection<ShortBroadcast> getAllLinkedShortBroadcasts();
}
