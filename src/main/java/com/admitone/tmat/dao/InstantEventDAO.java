package com.admitone.tmat.dao;
import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.InstantEvent;

public interface InstantEventDAO extends RootDAO<Integer,InstantEvent>{

	Collection<InstantEvent> getAllInstantEventsByTour(Integer tourId);
	Collection<InstantEvent> getInstantEventsByTour(Integer tourId);
	int getInstantEventCountByTour(Integer tourId);
	void deleteEventsByIds(List<Integer> eventIds);
	InstantEvent getInstantEventByEventId(Integer eventId);
}
