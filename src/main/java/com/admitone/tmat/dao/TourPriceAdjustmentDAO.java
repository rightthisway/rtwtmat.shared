package com.admitone.tmat.dao;

import java.util.Map;

import com.admitone.tmat.data.TourPriceAdjustment;
import com.admitone.tmat.data.TourPriceAdjustmentPk;

public interface TourPriceAdjustmentDAO extends RootDAO<TourPriceAdjustmentPk, TourPriceAdjustment> {
	Map<String, TourPriceAdjustment> getPriceAdjustments(int tourId);
	void deletePriceAdjustments(int tourId);
}