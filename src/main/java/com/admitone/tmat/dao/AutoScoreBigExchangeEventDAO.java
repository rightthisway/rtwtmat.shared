package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.AutoScoreBigExchangeEvent;
import com.admitone.tmat.data.GlobalAutoPricingAudit;

public interface AutoScoreBigExchangeEventDAO extends RootDAO<Integer, AutoScoreBigExchangeEvent> {
	
	public Collection<AutoScoreBigExchangeEvent> getAllAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds);
	public List<AutoScoreBigExchangeEvent> getAllActiveAutoScoreBigExchangeEvent();
	public Collection<AutoScoreBigExchangeEvent> getAllActiveAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<AutoScoreBigExchangeEvent> getAllDeletedAutoScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllAutoScoreBigExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<AutoScoreBigExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String auto_exposure_scorebig);
	public void updateRptFactor(String rpt_scorebig);
	public void updatePriceBreakup(String pricebreakup_scorebig);
	public void updateUpperMarkup(String upper_markpu_scorebig);
	public void updateLowerMarkup(String lower_markpu_scorebig);
	public void updateUpperShippingFees(String upper_shippingfees_scorebig);
	public void updateLowerShippingFees(String lower_shippingfees_scorebig);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
