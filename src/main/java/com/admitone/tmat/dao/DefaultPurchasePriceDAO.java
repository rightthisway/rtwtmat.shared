package com.admitone.tmat.dao;


import java.util.List;

import com.admitone.tmat.data.DefaultPurchasePrice;

public interface DefaultPurchasePriceDAO extends RootDAO<Integer , DefaultPurchasePrice> {
	DefaultPurchasePrice getAllDefaultTourPriceByTourId(Integer Id);
	List<DefaultPurchasePrice> getAllDefaultTourPrice();
	DefaultPurchasePrice getDefaultTourPriceByExchangeAndTicketType(String exchange,String ticketType);
}
