package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.MMEventMap;

public interface MMEventMapDAO extends RootDAO<Integer, MMEventMap> {
	Collection<MMEventMap> getMapsByUsername(String username);
	Collection<MMEventMap> getMapsByEventId(Integer eventId);
	Collection<Integer> getEventIdsByUsername(String username);
	Collection<String> getUsernamesByEventId(Integer eventId);
	MMEventMap getMap(String username, Integer eventId);
	void deleteMapsByEventId(Integer eventId);
}