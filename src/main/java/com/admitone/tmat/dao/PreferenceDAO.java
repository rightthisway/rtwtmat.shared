package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.Preference;
import com.admitone.tmat.data.PreferencePK;

public interface PreferenceDAO extends RootDAO<PreferencePK, Preference> {
	Preference getPreference(String username, String name);
	Collection<Preference> getAllPreferences(String username);
	void deletePreference(String username, String name);
	void deleteAllPreferences(String username);
	void deleteAllPreferences();
}
