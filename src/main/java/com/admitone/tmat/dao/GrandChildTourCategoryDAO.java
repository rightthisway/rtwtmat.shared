package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GrandChildTourCategory;

public interface GrandChildTourCategoryDAO extends RootDAO<Integer, GrandChildTourCategory> {
	public Collection<GrandChildTourCategory> getGrandChildTourCategoryByChildTourCategory(Integer id);
	public Collection<GrandChildTourCategory> getGrandChildTourCategoryByChildTourCategoryName(String name);
	public GrandChildTourCategory getGrandChildTourCategoryByNameAndChildTourCategoryNameAndGrandChildTourCategoryName(String name,String categoryName,String partentCategoryName);
	public void deleteAllById(List<Integer> ids);
	public Collection<GrandChildTourCategory> getAllGrandChildTourOrderByName();
	public Collection<GrandChildTourCategory> getGrandChildTourCategoryByChildTourCategorys(List<Integer> ids);
	public Collection<GrandChildTourCategory> getGrandChildCategoriesByName(String name);
}
