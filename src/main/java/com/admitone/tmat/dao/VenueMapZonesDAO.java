package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.VenueMapZones;

public interface VenueMapZonesDAO extends RootDAO<Integer, VenueMapZones> {

	public List<VenueMapZones> getAllVenueMapZonesByVenueCategoryId(Integer venueCategoryId) throws Exception;
}
