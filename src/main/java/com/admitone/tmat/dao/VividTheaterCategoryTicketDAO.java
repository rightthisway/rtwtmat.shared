package com.admitone.tmat.dao;

import com.admitone.tmat.data.VividTheaterCategoryTicket;

public interface VividTheaterCategoryTicketDAO extends RootDAO<Integer, VividTheaterCategoryTicket> {

}

