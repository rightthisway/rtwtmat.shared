package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.User;

public interface UserDAO extends RootDAO<Integer, User> {
	User getUserByUsernameOrEmail(String username, String email);
	Collection<User> getAllUsersByRole(String role);
	void removePasswordFromIdleUser(Integer period);
	boolean checkCode(String username, String code);
	int getUserCount();
	User getUserByUsername(String username);
//	Collection<User> getUsersByBrokerid(String brokerid);
	User getUserByUsernameAndBrokerid(String username , Integer brokerId);
	public Collection<User> getUsersByBrokerId(Integer brokerId);
	public Collection<User> getBrowseLiteUsersByBrokerId(Integer brokerId);
	public List<String> getBrokers(); 
}