package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.VipAutoVividSeatExchangeEvent;

public interface VipAutoVividSeatExchangeEventDAO extends RootDAO<Integer, VipAutoVividSeatExchangeEvent> {
	
	public Collection<VipAutoVividSeatExchangeEvent> getAllVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds);
	public List<VipAutoVividSeatExchangeEvent> getAllActiveVipAutoVividSeatExchangeEvent();
	public Collection<VipAutoVividSeatExchangeEvent> getAllActiveVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VipAutoVividSeatExchangeEvent> getAllDeletedVipAutoVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVipAutoVividSeatExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VipAutoVividSeatExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String vip_exposure_vivid);
	public void updateRptFactor(String rpt_vivid);
	public void updatePriceBreakup(String pricebreakup_vivid);
	public void updateUpperMarkup(String upper_markpu_vivid);
	public void updateLowerMarkup(String lower_markpu_vivid);
	public void updateUpperShippingFees(String upper_shippingfees_vivid);
	public void updateLowerShippingFees(String lower_shippingfees_vivid);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);	
}
