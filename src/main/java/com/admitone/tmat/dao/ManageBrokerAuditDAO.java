package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.ManageBrokerAudit;

public interface ManageBrokerAuditDAO extends RootDAO<Integer, ManageBrokerAudit>{
	
	public List<ManageBrokerAudit> getManageBrokerAuditByEventId(Integer eId);
	public void saveBulkAudits(List<ManageBrokerAudit> tnEvents) throws Exception;

}
