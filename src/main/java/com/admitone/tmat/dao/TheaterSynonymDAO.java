package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.TheaterSynonym;

public interface TheaterSynonymDAO extends RootDAO<String, TheaterSynonym>{
	Collection<TheaterSynonym> getValuesByName(String name);
}
