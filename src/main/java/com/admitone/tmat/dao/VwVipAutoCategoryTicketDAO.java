package com.admitone.tmat.dao;

import com.admitone.tmat.data.VwVipAutoCategoryTicket;

public interface VwVipAutoCategoryTicketDAO extends RootDAO<Integer, VwVipAutoCategoryTicket> {

}

