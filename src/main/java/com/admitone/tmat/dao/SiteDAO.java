package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Map;

import com.admitone.tmat.data.Site;

public interface SiteDAO extends RootDAO<String, Site> {
	public Collection<Site> getAll();
	public Site get(String id);
	public void update(Site site);
	public String save(Site site);
	public Map<String, Site> getSiteByIdMap();
}