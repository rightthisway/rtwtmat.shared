package com.admitone.tmat.dao;

import java.util.Date;

import com.admitone.tmat.data.SeatWaveSeason;

public interface SeatWaveSeasonDAO extends RootDAO<Integer, SeatWaveSeason> {	
	public void deleteSeasonsNotUpdatedAfter(Date updatedDate);
}
