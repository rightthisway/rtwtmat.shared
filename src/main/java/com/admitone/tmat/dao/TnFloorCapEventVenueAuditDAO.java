package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TnFloorCapEventVenueAudit;

public interface TnFloorCapEventVenueAuditDAO extends RootDAO<Integer, TnFloorCapEventVenueAudit> {
	
	public List<TnFloorCapEventVenueAudit> getTmFloorCapVenueAuditsByVenueName(String venueName);
}
