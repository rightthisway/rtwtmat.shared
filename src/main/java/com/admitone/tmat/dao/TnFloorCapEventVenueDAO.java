package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TnFloorCapEventVenue;

public interface TnFloorCapEventVenueDAO extends RootDAO<Integer, TnFloorCapEventVenue> {
	
	public void deleteTnFloorVenueByVenue(String venue);
	List<String> getAllTnFloorCapVenues(Collection<String> parentCategoryList);
}
