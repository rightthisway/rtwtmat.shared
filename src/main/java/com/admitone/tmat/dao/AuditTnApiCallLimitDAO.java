package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.AuditTnApiCallLimit;

public interface AuditTnApiCallLimitDAO extends RootDAO<Integer, AuditTnApiCallLimit>{

	public List<AuditTnApiCallLimit> getByProjectName(String project);
	
}
