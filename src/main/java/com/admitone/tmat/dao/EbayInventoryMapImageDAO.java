package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.EbayInventoryMapImage;

public interface EbayInventoryMapImageDAO extends RootDAO<Integer, EbayInventoryMapImage> {
	public Collection<EbayInventoryMapImage> getAllEbayInventoryTourMapImages(Integer tourId);
	public Collection<EbayInventoryMapImage> getAllEbayInventoryEventMapImages(Integer eventId);
}
