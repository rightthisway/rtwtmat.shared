package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.VWVipCatsCategoryTicket;

public interface VWVipCatsCategoryTicketDAO extends RootDAO<Integer, VWVipCatsCategoryTicket> {

	public Collection<VWVipCatsCategoryTicket> getAllActiveTnVipCatsCategoryTickets();
	
}