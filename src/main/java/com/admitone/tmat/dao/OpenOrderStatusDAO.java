package com.admitone.tmat.dao;

import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.OpenOrderStatus;
import com.admitone.tmat.data.Venue;
import com.admitone.tmat.enums.ProfitLossSign;

public interface OpenOrderStatusDAO extends RootDAO<Integer, OpenOrderStatus> {

	public List<OpenOrderStatus> getAllActiveRTWOpenOrders();
	public List<OpenOrderStatus> getSoldTicketDetailsByBrokerId(Integer eventId);
	public List<OpenOrderStatus> getSoldTicketDetailsByBrokerIdAndEventId(Integer brokerId,Integer eventId);
	public List<OpenOrderStatus> getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(Integer brokerId,Integer artistId,Integer eventId,Date startDate,Date endDate,
			ProfitLossSign profitLossSign);
	public List<Venue> getAllActiveOpenOrdersVenues(String param);
	public List<Artist> getAllActiveOpenOrderArtist(String param);
}
