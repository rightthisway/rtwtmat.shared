package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.LiveUser;

public interface LiveUserDAO extends RootDAO<String, LiveUser> {
	Collection<LiveUser> getLiveUserByUsername(String username);
	void updateLiveUserByUserName(String userName,String applicationName,Character isLoggedIn);
}