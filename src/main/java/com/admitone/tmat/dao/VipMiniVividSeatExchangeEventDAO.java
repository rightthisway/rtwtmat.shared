package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.VipMiniVividSeatExchangeEvent;

public interface VipMiniVividSeatExchangeEventDAO extends RootDAO<Integer, VipMiniVividSeatExchangeEvent> {
	
	public Collection<VipMiniVividSeatExchangeEvent> getAllVipMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVipMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds);
	public List<VipMiniVividSeatExchangeEvent> getAllActiveVipMiniVividSeatExchangeEvent();
	public Collection<VipMiniVividSeatExchangeEvent> getAllActiveVipMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VipMiniVividSeatExchangeEvent> getAllDeletedVipMiniVividSeatExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVipMiniVividSeatExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VipMiniVividSeatExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String vip_exposure_vivid);
	public void updateRptFactor(String rpt_vivid);
	public void updatePriceBreakup(String pricebreakup_vivid);
	public void updateUpperMarkup(String upper_markpu_vivid);
	public void updateLowerMarkup(String lower_markpu_vivid);
	public void updateUpperShippingFees(String upper_shippingfees_vivid);
	public void updateLowerShippingFees(String lower_shippingfees_vivid);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
