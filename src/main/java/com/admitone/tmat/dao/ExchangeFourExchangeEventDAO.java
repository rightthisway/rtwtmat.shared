package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.ExchangeFourExchangeEvent;

public interface ExchangeFourExchangeEventDAO extends RootDAO<Integer, ExchangeFourExchangeEvent> {
	
	public Collection<ExchangeFourExchangeEvent> getAllExchangeFourExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllExchangeFourExchangeEventsByEvent(List<Integer> eventIds);
	public List<ExchangeFourExchangeEvent> getAllActiveExchangeFourExchangeEvent();
	public Collection<ExchangeFourExchangeEvent> getAllActiveExchangeFourExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<ExchangeFourExchangeEvent> getAllDeletedExchangeFourExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllExchangeFourExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<ExchangeFourExchangeEvent> tnEvents) throws Exception;
	
}
