package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.VividExchangeEvent;

public interface VividExchangeEventDAO extends RootDAO<Integer, VividExchangeEvent> {
	
	public Collection<VividExchangeEvent> getAllVividExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVividExchangeEventsByEvent(List<Integer> eventIds);
	public List<VividExchangeEvent> getAllActiveVividExchangeEvent();
	public Collection<VividExchangeEvent> getAllActiveVividExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VividExchangeEvent> getAllDeletedVividExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVividExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VividExchangeEvent> tnEvents) throws Exception;
	public List<Object[]> getAllFrequentEvents();
	
}
