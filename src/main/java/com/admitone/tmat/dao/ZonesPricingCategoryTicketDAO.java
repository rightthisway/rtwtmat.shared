package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.ZonesPricingCategoryTicket;

public interface ZonesPricingCategoryTicketDAO extends RootDAO<Integer, ZonesPricingCategoryTicket> {

	public Collection<ZonesPricingCategoryTicket> getAllActiveZonesPricingCategoryTickets();
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketByAll(Integer eventId,String section,String row,String quantity );
	
	public List<ZonesPricingCategoryTicket> getAllZonesPricingCategoryTicketById(Integer Id);
	public List<String> getPriceHistoryById(Integer Id);
}
