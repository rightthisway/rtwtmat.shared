package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.ZonesPricingFloorCapAudit;

public interface ZonesPricingFloorCapAuditDAO extends RootDAO<Integer, ZonesPricingFloorCapAudit> {
	
	public List<ZonesPricingFloorCapAudit> getZoneFloorCapAuditByEvntIdAndZoneId(Integer eId,  String zone);
}
