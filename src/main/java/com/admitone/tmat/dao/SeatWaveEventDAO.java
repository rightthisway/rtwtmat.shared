package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.SeatWaveEvent;

public interface SeatWaveEventDAO extends RootDAO<Integer, SeatWaveEvent> {	
	public Collection<SeatWaveEvent> getSeatWaveEvents(int seasonId);
	public Collection<SeatWaveEvent> getSeatWaveEvents(Date fromDate, Date toDate);
	public void deleteEventsNotUpdatedAfter(Date updatedDate);	
}
