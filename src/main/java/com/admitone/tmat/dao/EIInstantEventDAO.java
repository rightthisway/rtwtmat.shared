package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.EIInstantEvent;
import com.admitone.tmat.data.Event;

public interface EIInstantEventDAO extends RootDAO<Integer,EIInstantEvent>{

	Collection<Event> getAllInstantEventsByTour(Integer tourId);
	Collection<EIInstantEvent> getInstantEventsByTour(Integer tourId);
	int getInstantEventCountByTour(Integer tourId);
}
