package com.admitone.tmat.dao;

import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.CrawlerHistoryManagement;

public interface CrawlerHistoryManagementDAO extends RootDAO<Integer,CrawlerHistoryManagement> {

	public List<CrawlerHistoryManagement> getCountersOfAllSiteIds(Date startDate,Date endDate);
	public  List<CrawlerHistoryManagement> getCrawlerHistory(Integer crawlId,Date startDate,Date endDate,String system);
	
}
