package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.VipMiniTickPickExchangeEvent;

public interface VipMiniTickPickExchangeEventDAO extends RootDAO<Integer, VipMiniTickPickExchangeEvent> {
	
	public Collection<VipMiniTickPickExchangeEvent> getAllVipMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVipMiniTickPickExchangeEventsByEvent(List<Integer> eventIds);
	public List<VipMiniTickPickExchangeEvent> getAllActiveVipMiniTickPickExchangeEvent();
	public Collection<VipMiniTickPickExchangeEvent> getAllActiveVipMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VipMiniTickPickExchangeEvent> getAllDeletedVipMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVipMiniTickPickExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VipMiniTickPickExchangeEvent> tnEvents) throws Exception;
	
}
