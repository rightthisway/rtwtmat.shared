package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.LastRowMiniCategoryTicket;

public interface LastRowMiniCategoryTicketDAO extends RootDAO<Integer, LastRowMiniCategoryTicket> {

	public Collection<LastRowMiniCategoryTicket> getAllActiveLastRowMiniCategoryTickets();
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsByAll(Integer eventId,String section,String row,String quantity);
	public List<LastRowMiniCategoryTicket> getAllLastRowMiniCategoryTicketsById(Integer Id);
	public List<String> getPriceHistoryById(Integer Id);
}