package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.User;
import com.admitone.tmat.data.UserAction;

public interface UserActionDAO extends RootDAO<Integer, UserAction> {
	Collection<UserAction> getActionsByUsernameAndDate(String username,Date startDate,Date endDate);
	Collection<UserAction> getActionsByUsername(String username);
	void logUserAction(User user, String action, String page,String ipAddress,Date timeStamp);
	void deleteUserActionByUserName(String userName);
	void deleteUserActionByUserId(Integer id);
}