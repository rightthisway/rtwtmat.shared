package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TGCatsCategoryTicket;

public interface TGCatsCategoryTicketDAO extends RootDAO<Integer, TGCatsCategoryTicket> {

	public Collection<TGCatsCategoryTicket> getAllActiveTgCatsCategoryTickets();
	public List<TGCatsCategoryTicket> getAllTgCatsCategoryTicketsByAll(Integer eventId,String section,String row,String quantity );
	
	public List<TGCatsCategoryTicket> getAllTgCatsCategoryTicketsById(Integer Id);
	public List<String> getPriceHistoryById(Integer Id);
}
