package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.ArtistBookmark;
import com.admitone.tmat.data.Bookmark;
import com.admitone.tmat.data.BookmarkPk;
import com.admitone.tmat.data.EventBookmark;
import com.admitone.tmat.data.TicketBookmark;
import com.admitone.tmat.data.TourBookmark;
import com.admitone.tmat.data.VenueBookmark;
import com.admitone.tmat.enums.BookmarkType;

public interface BookmarkDAO extends RootDAO<BookmarkPk, Bookmark> {
	void deleteBookmarksFromUser(String username);
	Collection<ArtistBookmark> getAllArtistBookmarks(String username);
	Collection<TourBookmark> getAllTourBookmarks(String username);
	Collection<EventBookmark> getAllEventBookmarks(String username);
	Collection<TicketBookmark> getAllTicketBookmarks(String username);
	Collection<VenueBookmark> getAllVenueBookmarks(String username);
	
	//Bookmark getBookmark(String username, BookmarkType type, String objectId);
	Bookmark getBookmark(String username, BookmarkType type, Integer objectId);
	Collection<EventBookmark> getOutlier(String username);
	void deleteArtistBookmarks(Integer artistId);
	void deleteTourBookmarks(Integer tourId);
	void deleteVenueBookmarks(Integer venueId);
	void deleteTourBookmarksWithArtistId(Integer artistId);
	void deleteEventBookmarks(Integer eventId);
	void deleteEventBookmarksWithTourId(Integer tourId);
	void deleteTicketBookmarks(Integer ticketId);
	void deleteTicketBookmarksWithEventId(Integer eventId);
	void deleteTicketBookmarksByCrawlId(Integer crawlId);
	void deleteTicketBookmarksByCrawlId(Integer crawlId, Date startDate, Date endDate);
	void cleanBookmarks();
}