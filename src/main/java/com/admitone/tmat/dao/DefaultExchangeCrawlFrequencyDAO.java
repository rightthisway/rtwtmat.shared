package com.admitone.tmat.dao;


import java.util.List;

import com.admitone.tmat.data.DefaultExchangeCrawlFrequency;

public interface DefaultExchangeCrawlFrequencyDAO extends RootDAO<Integer , DefaultExchangeCrawlFrequency> {
	
	public List<DefaultExchangeCrawlFrequency> getAllDefaultExchangeCrawlFrequency();
	public DefaultExchangeCrawlFrequency getDefaultExchangeCrawlFrequencyByExchange(String exchange);
}
