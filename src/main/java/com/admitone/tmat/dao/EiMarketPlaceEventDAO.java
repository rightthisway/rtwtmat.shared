package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.EiMarketPlaceEvent;

public interface EiMarketPlaceEventDAO extends RootDAO<Integer, EiMarketPlaceEvent> {
	Collection<EiMarketPlaceEvent> getEiMarketPlaceEvents(Date fromDate, Date toDate);
	void deleteEventsNotUpdatedAfter(Date updatedDate);
	Collection<Integer> getEiMarketPlaceEventIds();
	void updateDate(Integer eventId, Date date);
}
