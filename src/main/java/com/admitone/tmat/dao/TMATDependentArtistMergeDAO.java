package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TMATDependentArtistMerge;

public interface TMATDependentArtistMergeDAO extends RootDAO<Integer,TMATDependentArtistMerge> {
	public List<TMATDependentArtistMerge> getTMATDependentArtistMergeByArtistIdAndMergeArtistId(Integer tourId,Integer toMergedTourId);
}
