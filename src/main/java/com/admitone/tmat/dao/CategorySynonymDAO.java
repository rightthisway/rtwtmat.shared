package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.CategorySynonym;

public interface CategorySynonymDAO extends RootDAO<Integer, CategorySynonym> {
	Collection<CategorySynonym> getCategorySynonyms(String name);
	Collection<CategorySynonym> getSynonyms(Integer catId);
	Collection<CategorySynonym> getTourCategorySynonyms(Integer tourId);
}
