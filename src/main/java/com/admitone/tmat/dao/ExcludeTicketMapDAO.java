package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.ExcludeTicketMap;

public interface ExcludeTicketMapDAO extends RootDAO<Integer, ExcludeTicketMap> {
	Collection<ExcludeTicketMap> getAllMapsByTicketId(Integer ticketId);
	Collection<ExcludeTicketMap> getAllMapsByEventId(String eventId);
	Collection<ExcludeTicketMap> getAllShortMapsByEventId(String eventId);
	Collection<ExcludeTicketMap> getAllLongMapsByEventId(String eventId);
	Collection<ExcludeTicketMap> getAllMapsByShortId(Integer shortId);
	Collection<ExcludeTicketMap> getAllMapsByLongId(Integer shortId);
	Collection<ExcludeTicketMap> getAllShortMapsByEventAndCategory(Integer eventId, Integer categoryId);
	Collection<ExcludeTicketMap> getAllLongMapsByEventAndCategory(Integer eventId, Integer categoryId);
	Collection<ExcludeTicketMap> getAllMapsByTransactionIdAndType(Integer transactionId, Integer transactionType);
	Collection<ExcludeTicketMap> getAllMapsByEventId(Integer eventId);

	void deleteEventMapForTicket(Integer ticketId);
	void deleteAllCatMapsForTicket(Integer ticketId);
	void deleteAllCatMapsForTicket(Integer ticketId, Integer categoryId);
	void deleteAllTxMapsForTicket(Integer ticketId);
	void deleteAllTxMapsForTicket(Integer ticketId, Integer categoryId);
}