package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.InstantTickets;

public interface InstantTicketsDAO extends RootDAO<Integer, InstantTickets> {
List<InstantTickets> getAllInstantTickets();
List<InstantTickets> getAllInstantTickets(Integer eventId);
}
