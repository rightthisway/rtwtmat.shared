package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.UserOutlierCategoryDetails;

public interface UserOutlierCategoryDetailsDAO extends RootDAO<Integer, UserOutlierCategoryDetails> {
	
	Collection<UserOutlierCategoryDetails> getAllOutlierCategoryPriceDetailsByOutlierId(Integer outlierId);
	void deleteAllOutlierCatPriceDtlByOutlierID(Integer outlierId);
}