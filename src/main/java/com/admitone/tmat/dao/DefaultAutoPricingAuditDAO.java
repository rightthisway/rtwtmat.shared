package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.DefaultAutoPricingAudit;

public interface DefaultAutoPricingAuditDAO extends RootDAO<Integer,DefaultAutoPricingAudit> {

	public List<DefaultAutoPricingAudit> getDefaultAutoPricingAuditByCategory(String exCategory);

}
