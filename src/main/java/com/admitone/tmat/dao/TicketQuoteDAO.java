package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TicketQuote;

public interface TicketQuoteDAO extends RootDAO<Integer, TicketQuote> {
	void deleteByQuoteId(Integer quoteId);
	TicketQuote getMostRecentTicketQuote(String username);
	Collection<TicketQuote> getTicketQuotesByQuoteId(Integer quoteId);
	List<TicketQuote> getTicketQuotesByQuoteIds(List<Integer> ids);	
	void deleteByIds(List<Integer> ids);
}
