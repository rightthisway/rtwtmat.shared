package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.TGCatsCategoryEvent;

public interface TGCatsCategoryEventDAO extends RootDAO<Integer, TGCatsCategoryEvent> {
	
	public Collection<TGCatsCategoryEvent> getAllTGCatsCategoryEventByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<TGCatsCategoryEvent> getAllActiveTGCatsCategoryEventByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllTGCatsCategoryEventAsDeleteByEvent(List<Integer> eventIds);
	public void deleteAllTGCatsCategoryEventByEvent(List<Integer> eventIds);
	public List<TGCatsCategoryEvent> getAllActiveTGCatsCategoryEvent();
	public Collection<TGCatsCategoryEvent> getAllDeletedTGCatsCategoryEventByEvent(List<Integer> eventIds, boolean isEventInclusive);
	
}
