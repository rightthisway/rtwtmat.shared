package com.admitone.tmat.dao;

import com.admitone.tmat.data.User;

public interface UserBrokerDetailsDAO extends RootDAO<Integer, User> {
    void deleteBrokerDetailsByUserId(Integer userId);
}
