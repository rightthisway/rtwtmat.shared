package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.MXPZonesTicket;


public interface MXPZonesTicketDAO extends RootDAO<Integer, MXPZonesTicket> {
	public List<MXPZonesTicket> getMXPZonesTicketByTNEventId(Integer eventId);
}
