package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.ProbableVenue;

public interface ProbableVenueDAO extends RootDAO<String, ProbableVenue> {
	 
	public List<ProbableVenue> getSynonymByVenueId(Integer venueId);
	public List<ProbableVenue> getAllAddedVenueSynonymsByMarkedAsSynonym(Boolean markedAsSynonym);
	public List<ProbableVenue> getAllAddedVenueSynonymsByVenue(Boolean markedAsSynonym, Integer venueId);
}