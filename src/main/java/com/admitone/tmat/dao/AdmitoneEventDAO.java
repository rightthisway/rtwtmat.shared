package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.AdmitoneEvent;

public interface AdmitoneEventDAO extends RootDAO<Integer, AdmitoneEvent> {
	List<AdmitoneEvent> getAllEventsByEventNamesAndVenueName(String[] eventNames,String venueName,List<String> parentCategoryList);
	List<String> getAllDistinctZoneEventsByVenueName(String venueName,List<String> parentCategoryList,Integer brokerId);
	List<AdmitoneEvent> getAllZoneEventsByEventNamesAndVenueName(String[] eventNames,String venueName,List<String> parentCategoryList,Integer brokerId);
	List<AdmitoneEvent> getAllZoneEventsByVenueName(String venueName,List<String> parentCategoryList);
	public List<Integer> getAllEventIdsByVenue(String venue,List<String> parentCategoryList);
	List<AdmitoneEvent> getAllEventsByEventIds(List<Integer> eventIds);
	List<String> getDistinctVenues(List<String> parentCategoryList);
	Collection<Integer> getAllEventIds();
	Collection<AdmitoneEvent> getAllEventsNearDate(Date eventDate);
	AdmitoneEvent getEventByEventId(Integer eventId);
	public List<Integer> getAllEventIdsByParentCategoryList(List<String> parentCategoryList);
	public List<AdmitoneEvent> getEventsByName(String eventName);
	List<String> getAllDistinctZonedVenues(Integer brokerId);
}
