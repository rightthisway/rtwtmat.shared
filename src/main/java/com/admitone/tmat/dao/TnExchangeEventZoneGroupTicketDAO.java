package com.admitone.tmat.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.TnExchangeEventZoneGroupTicket;

public interface TnExchangeEventZoneGroupTicketDAO extends RootDAO<Integer, TnExchangeEventZoneGroupTicket> {

	TnExchangeEventZoneGroupTicket getZoneTicketsByEventIdAndCategoryTicketGroupId(Integer eventId,Integer ticketGrpId);
	List<TnExchangeEventZoneGroupTicket> getAllZonesAutoPricesedTicketsByEventId(ArrayList<Integer> eventIds);
	List<String> getDistinctVenues();
	List<TnExchangeEventZoneGroupTicket> getAllProcessedLisitngs();
	List<TnExchangeEventZoneGroupTicket> getAllProcessedLisitngsByDate(Date fromDate,Date toDate);
	List<TnExchangeEventZoneGroupTicket> getAllFailedLisitngs();
}
