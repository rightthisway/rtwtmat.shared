package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.MarketMakerEventTracking;
import com.admitone.tmat.data.User;

public interface MarketMakerEventTrackingDAO extends RootDAO<Integer, MarketMakerEventTracking> {
	User getCurrentMarketMakerForEvent(Integer eventId);
	MarketMakerEventTracking getCurrentMarketMakerEventTracking(Integer eventId);
	Collection<Event> getCurrentEventsManagedByMarketMaker(String username);
	Collection<Event> getAllEventsManagedByMarketMaker(String username);
}
