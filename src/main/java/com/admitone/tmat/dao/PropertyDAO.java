package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Map;

import com.admitone.tmat.data.Property;

//this is a class for property dao

public interface PropertyDAO extends RootDAO<String, Property> {
	Map<String, String> getPropertyMap();
	Collection<Property> getPropertyByPrefix(String prefix);
	public int addStubhubMinuteHitCount(int count) throws Exception;
}
