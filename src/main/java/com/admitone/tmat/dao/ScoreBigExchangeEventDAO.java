package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.ScoreBigExchangeEvent;

public interface ScoreBigExchangeEventDAO extends RootDAO<Integer, ScoreBigExchangeEvent> {
	
	public Collection<ScoreBigExchangeEvent> getAllScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllScoreBigExchangeEventsByEvent(List<Integer> eventIds);
	public List<ScoreBigExchangeEvent> getAllActiveScoreBigExchangeEvent();
	public Collection<ScoreBigExchangeEvent> getAllActiveScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<ScoreBigExchangeEvent> getAllDeletedScoreBigExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllScoreBigExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<ScoreBigExchangeEvent> tnEvents) throws Exception;
	
}
