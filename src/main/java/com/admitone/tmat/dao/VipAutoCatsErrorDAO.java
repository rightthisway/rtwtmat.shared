package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.VipAutoCatsError;

	public interface VipAutoCatsErrorDAO extends RootDAO<Integer, VipAutoCatsError>{
		public Collection<VipAutoCatsError> getAllVipAutoCatsErrorByDate(Date date);
	}
