package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.MiniTickPickExchangeEvent;

public interface MiniTickPickExchangeEventDAO extends RootDAO<Integer, MiniTickPickExchangeEvent> {
	
	public Collection<MiniTickPickExchangeEvent> getAllMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllMiniTickPickExchangeEventsByEvent(List<Integer> eventIds);
	public List<MiniTickPickExchangeEvent> getAllActiveMiniTickPickExchangeEvent();
	public Collection<MiniTickPickExchangeEvent> getAllActiveMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<MiniTickPickExchangeEvent> getAllDeletedMiniTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllMiniTickPickExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<MiniTickPickExchangeEvent> tnEvents) throws Exception;
	
}
