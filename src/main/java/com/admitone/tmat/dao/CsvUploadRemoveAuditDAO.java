package com.admitone.tmat.dao;

import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.CsvUploadRemoveAudit;

public interface CsvUploadRemoveAuditDAO extends RootDAO<String, CsvUploadRemoveAudit>  {
	
	public List<CsvUploadRemoveAudit> getAllByVenueAndCategoryGroup(String venuename, String catScheme);
	public List<CsvUploadRemoveAudit> getAllAuditsbyFilter(String action,Date fromDate,Date toDate);

}
