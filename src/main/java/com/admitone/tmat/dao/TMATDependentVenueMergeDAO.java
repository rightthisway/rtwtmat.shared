package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TMATDependentVenueMerge;

public interface TMATDependentVenueMergeDAO extends RootDAO<Integer,TMATDependentVenueMerge> {
	public List<TMATDependentVenueMerge> getTMATDependentVenueMergeByVenueIdAndMergeVenueId(Integer venueId,Integer toMergedVenueId);
}
