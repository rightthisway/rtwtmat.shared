package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.AdmitoneEventLocal;

public interface AdmitoneEventLocalDAO extends RootDAO<Integer, AdmitoneEventLocal> {
	Collection<Integer> getAllEventIds();
	Collection<AdmitoneEventLocal> getAllEventsNearDate(Date eventDate);
	AdmitoneEventLocal getEventByEventId(Integer eventId);
	AdmitoneEventLocal getEventByVenueCityStateDateTime(String venueName,String cityName,String stateName, Date eventDate, Date eventTime);
	
}
