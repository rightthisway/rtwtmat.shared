package com.admitone.tmat.dao;


import java.util.List;

import com.admitone.tmat.data.ManagePurchasePrice;

public interface ManagePurchasePriceDAO extends RootDAO<Integer , ManagePurchasePrice> {

	List<Integer> getDistinctArtistIds();

	List<ManagePurchasePrice> getAllByArtistIds(List<Integer> ids);

//	Collection<Artist> getDistinctArtists();

	List<ManagePurchasePrice> getAllByArtistExchangeTicketTypes(List<Integer> ids,List<String> exchangeIds, List<String> ticketTypeIds);
	List<ManagePurchasePrice> getAllByManagePurchasePriceByArtistId(Integer Id);
	List<ManagePurchasePrice> getAllManagePurchasePriceByEventId(Integer Id);

}
