package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.AdmitoneInventory;

public interface AdmitoneInventoryDAO extends RootDAO<Integer, AdmitoneInventory> {
	// FIXME: not really sure what this method's purpose is. Remove it?
	@Deprecated
	Collection<AdmitoneInventory> getAllInventoryByDate(Date eventDate);
	
	Collection<AdmitoneInventory> getAllInventoryByAdmitOneEventId(Integer eventId);
	Collection<AdmitoneInventory> getAllInventoryByEventId(Integer eventId);
}
