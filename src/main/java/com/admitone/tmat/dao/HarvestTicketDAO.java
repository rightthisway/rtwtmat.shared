package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.HarvestTicket;

public interface HarvestTicketDAO extends RootDAO<Integer, HarvestTicket> {

	void delete(HarvestTicket Ticket);
	void deleteById(Integer id);
	void update(HarvestTicket Ticket);
	Collection<HarvestTicket> getTicketsForEvent(Integer eventId);
}
