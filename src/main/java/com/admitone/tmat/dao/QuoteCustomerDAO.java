package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.QuoteCustomer;

public interface QuoteCustomerDAO extends RootDAO<Integer, QuoteCustomer> {
	public QuoteCustomer getCustomerByName(String name);
	public Collection<String> getCreators();
	public Collection<QuoteCustomer> getCustomersByUsername(String username);
	public Collection<QuoteCustomer> getCustomersByCreator(String creator);	
	public Collection<QuoteCustomer> getAllCustomersByBrokerId(Integer brokerId);
	public QuoteCustomer getCustomerByNameandBrokerId(String name,Integer brokerId);
}
