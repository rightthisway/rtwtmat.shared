package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.ExchangeEventAudit;

public interface ExchangeEventAuditDAO extends RootDAO<Integer, ExchangeEventAudit> {

	public List<ExchangeEventAudit> getExchangeEventAuditByEventId(Integer eId);
	public void saveBulkAudits(List<ExchangeEventAudit> tnEvents) throws Exception;
}
