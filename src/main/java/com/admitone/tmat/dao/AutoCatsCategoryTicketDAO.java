package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.AutoCatsCategoryTicket;

public interface AutoCatsCategoryTicketDAO extends RootDAO<Integer, AutoCatsCategoryTicket> {

	public Collection<AutoCatsCategoryTicket> getAllActiveAutoCatsCategoryTickets();
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByAll(Integer eventId, String section, String row, String quantity);
	
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsById(Integer Id);
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByTicketId(int ticketId);
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByBaseoneticketId(int ticketId);
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByBasetwoticketId(int ticketId);
	public List<AutoCatsCategoryTicket> getAllAutoCatsCategoryTicketsByBasethreeticketId(int ticketId);
}
