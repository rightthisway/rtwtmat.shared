package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.PresaleEvent;

public interface PresaleEventDAO extends RootDAO<Integer, PresaleEvent> {

	public Collection<PresaleEvent> getAllEligiblePresaleEvents(Date createdDateParam);
	public Collection<PresaleEvent> getAllPresaleEventsByArtistIdAndVenueId(Integer artistId,Integer venueId);
	public PresaleEvent getActivePresaleEventByEventId(Integer eventId);
	
}
