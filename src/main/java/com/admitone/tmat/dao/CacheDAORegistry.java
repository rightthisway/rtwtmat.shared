package com.admitone.tmat.dao;

import java.io.Serializable;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public final class CacheDAORegistry extends DAORegistry implements ApplicationContextAware, Serializable {
	private static ApplicationContext context;

	private static Object getDAO(String name) {
		return context.getBean(name);
	}

//	public static EventDAO getEventDAO() {
//		return (EventDAO) getDAO("cacheEventDAO");
//	}
//
//	public static ArtistDAO getArtistDAO() {
//		return (ArtistDAO) getDAO("cacheArtistDAO");
//	}
//
//	public static UserDAO getUserDAO() {
//		return (UserDAO) getDAO("cacheUserDAO");
//	}
//
//	public static UserAlertDAO getUserAlertDAO() {
//		return (UserAlertDAO) getDAO("cacheUserAlertDAO");
//	}
//
//	public static UserActionDAO getUserActionDAO() {
//		return (UserActionDAO) getDAO("cacheUserActionDAO");
//	}
//
//	public static MMEventMapDAO getMMEventMapDAO() {
//		return (MMEventMapDAO) getDAO("cacheMMEventMapDAO");
//	}
//
//	public static RoleDAO getRoleDAO() {
//		return (RoleDAO) getDAO("cacheRoleDAO");
//	}
//
//	public static VenueDAO getVenueDAO() {
//		return (VenueDAO) getDAO("cacheVenueDAO");
//	}
//
//	public static SynonymDAO getSynonymDAO() {
//		return (SynonymDAO) getDAO("cacheSynonymDAO");
//	}
//
//	public static CategorySynonymDAO getCategorySynonymDAO() {
//		return (CategorySynonymDAO) getDAO("cacheCategorySynonymDAO");
//	}
//
//	public static TourDAO getTourDAO() {
//		return (TourDAO) getDAO("cacheTourDAO");
//	}
//
//	public static TicketDAO getTicketDAO() {
//		return (TicketDAO) getDAO("cacheTicketDAO");
//	}
//
//	public static TicketListingCrawlDAO getTicketListingCrawlDAO() {
//		return (TicketListingCrawlDAO) getDAO("cacheTicketListingCrawlDAO");
//	}
//
//	public static BookmarkDAO getBookmarkDAO() {
//		return (BookmarkDAO) getDAO("cacheBookmarkDAO");
//	}
//
//	public static CategoryDAO getCategoryDAO() {
//		return (CategoryDAO) getDAO("cacheCategoryDAO");
//	}
//
//	public static CategoryMappingDAO getCategoryMappingDAO() {
//		return (CategoryMappingDAO) getDAO("cacheCategoryMappingDAO");
//	}
//
//	public static PropertyDAO getPropertyDAO() {
//		return (PropertyDAO) getDAO("cachePropertyDAO");
//	}
//
//	public static EventPriceAdjustmentDAO getEventPriceAdjustmentDAO() {
//		return (EventPriceAdjustmentDAO) getDAO("cacheEventPriceAdjustmentDAO");
//	}
//
//	public static TourPriceAdjustmentDAO getTourPriceAdjustmentDAO() {
//		return (TourPriceAdjustmentDAO) getDAO("cacheTourPriceAdjustmentDAO");
//	}
//
//	public static ArtistPriceAdjustmentDAO getArtistPriceAdjustmentDAO() {
//		return (ArtistPriceAdjustmentDAO) getDAO("cacheArtistPriceAdjustmentDAO");
//	}
//
//	public static SiteDAO getSiteDAO() {
//		return (SiteDAO) getDAO("cacheSiteDAO");
//	}
//
//	public static ShortTransactionDAO getShortTransactionDAO() {
//		return (ShortTransactionDAO) getDAO("cacheShortTransactionDAO");
//	}
//
//	public static EventSynonymDAO getEventSynonymDAO() {
//		return (EventSynonymDAO) getDAO("cacheEventSynonymDAO");
//	}

	/*
	 * Since caching for other object in not used, i have commented it.
	 * Only cacheTicketDAO is enabled.
	 */
	public static com.admitone.tmat.dao.coherence.TicketDAO getTicketDAO() {
		return (com.admitone.tmat.dao.coherence.TicketDAO) getDAO("cacheTicketDAO");
	}
	
    public void setApplicationContext(ApplicationContext context)
    throws BeansException {
    	CacheDAORegistry.context = context;
    }
}