package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.MiniTNExchangeEvent;

public interface MiniTNExchangeEventDAO extends RootDAO<Integer, MiniTNExchangeEvent> {
	
	public Collection<MiniTNExchangeEvent> getAllMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllMiniTNExchangeEventsByEvent(List<Integer> eventIds);
	public List<MiniTNExchangeEvent> getAllActiveMiniTNExchangeEvent();
	public Collection<MiniTNExchangeEvent> getAllActiveMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<MiniTNExchangeEvent> getAllDeletedMiniTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllMiniTNExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<MiniTNExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String mini_exposure_ticketnetwork);
	public void updateRptFactor(String rpt_ticketnetwork);
	public void updatePriceBreakup(String pricebreakup_ticketnetwork);
	public void updateUpperMarkup(String upper_markpu_ticketnetwork);
	public void updateLowerMarkup(String lower_markpu_ticketnetwork);
	public void updateUpperShippingFees(String upper_shippingfees_ticketnetwork);
	public void updateLowerShippingFees(String lower_shippingfees_ticketnetwork);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
