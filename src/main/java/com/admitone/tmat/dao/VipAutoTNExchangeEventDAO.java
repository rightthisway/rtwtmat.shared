package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.GlobalAutoPricingAudit;
import com.admitone.tmat.data.VipAutoTNExchangeEvent;

public interface VipAutoTNExchangeEventDAO extends RootDAO<Integer, VipAutoTNExchangeEvent> {
	
	public Collection<VipAutoTNExchangeEvent> getAllVipAutoTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllVipAutoTNExchangeEventsByEvent(List<Integer> eventIds);
	public List<VipAutoTNExchangeEvent> getAllActiveVipAutoTNExchangeEvent();
	public Collection<VipAutoTNExchangeEvent> getAllActiveVipAutoTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<VipAutoTNExchangeEvent> getAllDeletedVipAutoTNExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllVipAutoTNExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<VipAutoTNExchangeEvent> tnEvents) throws Exception;
	public void updateExposure(String vip_exposure_ticketnetwork);
	public void updateRptFactor(String rpt_ticketnetwork);
	public void updatePriceBreakup(String pricebreakup_ticketnetwork);
	public void updateUpperMarkup(String upper_markpu_ticketnetwork);
	public void updateLowerMarkup(String lower_markpu_ticketnetwork);
	public void updateUpperShippingFees(String upper_shippingfees_ticketnetwork);
	public void updateLowerShippingFees(String lower_shippingfees_ticketnetwork);
	public void updateGlobalAutoPricing(GlobalAutoPricingAudit globalAudit,String parentType);
}
