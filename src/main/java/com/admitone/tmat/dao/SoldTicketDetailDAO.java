package com.admitone.tmat.dao;

import java.util.Date;
import java.util.List;

import com.admitone.tmat.data.SoldTicketDetail;

public interface SoldTicketDetailDAO extends RootDAO<Integer, SoldTicketDetail> {
	public List<SoldTicketDetail> getSoldUnfilledTicketDetailsByBrokerId(Integer brokerId);
	public List<SoldTicketDetail> getSoldTicketDetailsByEventId(Integer eventId);
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerId(Integer eventId);
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndEventId(Integer brokerId,Integer eventId);
	public List<SoldTicketDetail> getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(Integer brokerId,Integer artistId,Integer eventId,Date startDate,Date endDate);
	public List<SoldTicketDetail> getAllBrokersSoldUnfilledTicketDetails();
}

