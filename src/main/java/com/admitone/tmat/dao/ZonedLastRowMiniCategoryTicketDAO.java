package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.ZonedLastRowMiniCategoryTicket;

public interface ZonedLastRowMiniCategoryTicketDAO extends RootDAO<Integer, ZonedLastRowMiniCategoryTicket> {

	public Collection<ZonedLastRowMiniCategoryTicket> getAllActiveZonedLastRowMiniCategoryTickets();
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsByAll(Integer eventId,String section,String row,String quantity);
	public List<ZonedLastRowMiniCategoryTicket> getAllZonedLastRowMiniCategoryTicketsById(Integer Id);
	public List<String> getPriceHistoryById(Integer Id);
}