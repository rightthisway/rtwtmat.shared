package com.admitone.tmat.dao;

import com.admitone.tmat.data.TNDataFeedEvent;

public interface TNDataFeedEventDAO extends RootDAO<Integer, TNDataFeedEvent>{

}
