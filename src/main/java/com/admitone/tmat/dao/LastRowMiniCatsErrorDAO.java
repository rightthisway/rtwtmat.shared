package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.LastRowMiniCatsError;

public interface LastRowMiniCatsErrorDAO extends RootDAO<Integer, LastRowMiniCatsError>{
	public Collection<LastRowMiniCatsError> getAllLastRowMiniCatsErrorErrorByDate(Date date);
}
