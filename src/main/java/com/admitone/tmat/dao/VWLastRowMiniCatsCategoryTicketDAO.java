package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.VWLastRowMiniCatsCategoryTicket;

public interface VWLastRowMiniCatsCategoryTicketDAO extends RootDAO<Integer, VWLastRowMiniCatsCategoryTicket> {

	public Collection<VWLastRowMiniCatsCategoryTicket> getAllActiveTnLastRowMiniCatsCatsCategoryTickets();
}
