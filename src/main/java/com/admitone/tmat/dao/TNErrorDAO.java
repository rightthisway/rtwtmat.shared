package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.data.TNError;

	public interface TNErrorDAO extends RootDAO<Integer, TNError>{
		public Collection<TNError> getAllTNErrorByDate(Date date);
	}
