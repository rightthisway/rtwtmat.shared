package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.LastFiveRowMiniCategoryTicket;

public interface LastFiveRowMiniCategoryTicketDAO extends RootDAO<Integer, LastFiveRowMiniCategoryTicket> {

	public Collection<LastFiveRowMiniCategoryTicket> getAllActiveLastFiveRowMiniCategoryTickets();
	public List<LastFiveRowMiniCategoryTicket> getAllLastFiveRowMiniCategoryTicketsByAll(Integer eventId, String section, String row, String quantity);
	public List<LastFiveRowMiniCategoryTicket> getAllLastFiveRowMiniCategoryTicketsById(Integer Id);
	public List<String> getPriceHistoryById(Integer Id);
}