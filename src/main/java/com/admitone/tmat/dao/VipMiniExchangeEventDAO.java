package com.admitone.tmat.dao;

import java.util.Collection;

import com.admitone.tmat.data.VipMiniExchangeEvent;


public interface VipMiniExchangeEventDAO extends RootDAO<Integer, VipMiniExchangeEvent>{

	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByEventId(Integer eventId);
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByVenueId(Integer venueId);
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByArtistId(Integer artistId);
	public Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsByGrandChildCategoryId(Integer grandChildCategoryId);
	Collection<VipMiniExchangeEvent> getAllVipMiniExchangeEventsEligibleForUpdate(Long minute);
	public VipMiniExchangeEvent getVipMiniExchangeEventsByEventId(Integer eventId);
}
