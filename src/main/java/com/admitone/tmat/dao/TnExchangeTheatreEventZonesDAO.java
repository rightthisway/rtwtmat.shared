package com.admitone.tmat.dao;

import java.util.List;

import com.admitone.tmat.data.TnExchangeTheatreEventZones;

public interface TnExchangeTheatreEventZonesDAO extends RootDAO<Integer, TnExchangeTheatreEventZones> {
	
	 TnExchangeTheatreEventZones getTnExchangeTheatreEventZones(Integer zoneId);
	 List<TnExchangeTheatreEventZones> getAllTnExchangeTheatreEventZones();
}
