package com.admitone.tmat.dao;

import java.util.Collection;
import java.util.List;

import com.admitone.tmat.data.AutoTickPickExchangeEvent;

public interface AutoTickPickExchangeEventDAO extends RootDAO<Integer, AutoTickPickExchangeEvent> {
	
	public Collection<AutoTickPickExchangeEvent> getAllAutoTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void deleteAllAutoTickPickExchangeEventsByEvent(List<Integer> eventIds);
	public List<AutoTickPickExchangeEvent> getAllActiveAutoTickPickExchangeEvent();
	public Collection<AutoTickPickExchangeEvent> getAllActiveAutoTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public Collection<AutoTickPickExchangeEvent> getAllDeletedAutoTickPickExchangeEventsByEvent(List<Integer> eventIds, boolean isEventInclusive);
	public void updateAllAutoTickPickExchangeEventAsDeleteByEvent(List<Integer> eventIds);
	public void saveBulkEvents(List<AutoTickPickExchangeEvent> tnEvents) throws Exception;
	
}
