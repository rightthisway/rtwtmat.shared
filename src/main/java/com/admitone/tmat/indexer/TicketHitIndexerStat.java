package com.admitone.tmat.indexer;

import java.io.Serializable;

public class TicketHitIndexerStat implements Serializable {
	public long totalIndexingTime = 0;
	public long totalDBUpdateTime = 0;
	public long totalDBInsertTime = 0;
	public long updatedTicketCount = 0;
	public long insertedTicketCount = 0;
	public long updatedTicketErrorCount = 0;
	public long insertedTicketErrorCount = 0;
	public long updatedTicketWarningCount = 0;
	public long insertedTicketWarningCount = 0;
	public long totalDBLoadTime = 0; 
	
	public long getTotalIndexingTime() {
		return totalIndexingTime;
	}
	
	public void setTotalIndexingTime(long totalIndexingTime) {
		this.totalIndexingTime = totalIndexingTime;
	}
	
	public void incrementTotalIndexingTime(long value) {
		this.totalIndexingTime += value;
	}

	public long getTotalDBLoadTime() {
		return totalDBLoadTime;
	}

	public void setTotalDBLoadTime(long totalDBLoadTime) {
		this.totalDBLoadTime = totalDBLoadTime;
	}

	public void incrementTotalDBLoadTime(long value) {
		this.totalDBLoadTime += value;
	}

	public long getTotalDBUpdateTime() {
		return totalDBUpdateTime;
	}
	
	public void setTotalDBUpdateTime(long totalDBUpdateTime) {
		this.totalDBUpdateTime = totalDBUpdateTime;
	}

	public void incrementTotalDBUpdateTime(long value) {
		this.totalDBUpdateTime += value;
	}

	public long getTotalDBInsertTime() {
		return totalDBInsertTime;
	}
	
	public void setTotalDBInsertTime(long totalDBInsertTime) {
		this.totalDBInsertTime = totalDBInsertTime;
	}

	public void incrementTotalDBInsertTime(long value) {
		this.totalDBInsertTime += value;
	}

	public long getUpdatedTicketCount() {
		return updatedTicketCount;
	}
	
	public void setUpdatedTicketCount(long updatedTicketCount) {
		this.updatedTicketCount = updatedTicketCount;
	}

	public void incrementUpdatedTicketCount(long value) {
		this.updatedTicketCount += value;
	}
	
	public long getInsertedTicketCount() {
		return insertedTicketCount;
	}
	
	public void setInsertedTicketCount(long insertedTicketCount) {
		this.insertedTicketCount = insertedTicketCount;
	}

	public void incrementInsertedTicketCount(long value) {
		this.insertedTicketCount += value;
	}

	public long getUpdatedTicketErrorCount() {
		return updatedTicketErrorCount;
	}
	
	public void setUpdatedTicketErrorCount(long updatedTicketErrorCount) {
		this.updatedTicketErrorCount = updatedTicketErrorCount;
	}

	public void incrementUpdatedTicketErrorCount(long value) {
		this.updatedTicketErrorCount += value;
	}

	public long getInsertedTicketErrorCount() {
		return insertedTicketErrorCount;
	}
	
	public void setInsertedTicketErrorCount(int insertedTicketErrorCount) {
		this.insertedTicketErrorCount = insertedTicketErrorCount;
	}
	
	public void incrementInsertedTicketErrorCount(long value) {
		insertedTicketErrorCount += value;
	}

	public long getUpdatedTicketWarningCount() {
		return updatedTicketWarningCount;
	}

	public void setUpdatedTicketWarningCount(long updatedTicketWarningCount) {
		this.updatedTicketWarningCount = updatedTicketWarningCount;
	}

	public void incrementUpdatedTicketWarningCount(long value) {
		updatedTicketWarningCount += value;
	}

	public long getInsertedTicketWarningCount() {
		return insertedTicketWarningCount;
	}

	public void setInsertedTicketWarningCount(long insertedTicketWarningCount) {
		this.insertedTicketWarningCount = insertedTicketWarningCount;
	}

	public void incrementInsertedTicketWarningCount(long value) {
		insertedTicketWarningCount += value;
	}

	public Double getAverageIndexingTime() {
		if (insertedTicketCount + updatedTicketCount == 0) {
			return 0D;
		}
		return (double) totalIndexingTime / (double) (insertedTicketCount + updatedTicketCount);
	}
	
	public long getIndexedTicketCount() {
		return insertedTicketCount + updatedTicketCount;
	}
	
	public double getAverageDBUpdateTicketTime() {
		if ((insertedTicketCount + updatedTicketCount) == 0) {
			return 0;
		}
		return (double)totalDBUpdateTime / (double)(updatedTicketCount);
	}

	public double getAverageDBInsertTicketTime() {
		if (insertedTicketCount == 0) {
			return 0;
		}
		return (double)totalDBInsertTime / (double)insertedTicketCount;
	}

	public double getAverageDBStoreTicketTime() {
		if (insertedTicketCount + updatedTicketCount == 0) {
			return 0;
		}
		return (double)(totalDBInsertTime + totalDBUpdateTime) / (double)(insertedTicketCount + updatedTicketCount);
	}

	public double getAverageDBLoadTicketTime() {
		if (insertedTicketCount + updatedTicketCount == 0) {
			return 0;
		}
		return (double)(totalDBLoadTime) / (double)(insertedTicketCount + updatedTicketCount);
	}


	public void reset() {
		totalIndexingTime = 0;
		totalDBUpdateTime = 0;
		totalDBInsertTime = 0;
		totalDBLoadTime = 0;
		updatedTicketCount = 0;
		insertedTicketCount = 0;
		updatedTicketErrorCount = 0;
		insertedTicketErrorCount = 0;		
		updatedTicketWarningCount = 0;
		insertedTicketWarningCount = 0;
	}	
}