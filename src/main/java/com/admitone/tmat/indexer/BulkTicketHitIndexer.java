package com.admitone.tmat.indexer;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.DuplicateTicketMap;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.pojo.WSTicketWrapper;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.utils.PostBackURL;
import com.admitone.tmat.utils.TicketPersistenceTracker;
import com.thoughtworks.xstream.XStream;

/**
 * Bulk Ticket Hit Indexer
 * Resolve event if needed and persist the ticket into the database.
 */
public class BulkTicketHitIndexer extends TicketHitIndexer {
	private Semaphore concurrentFlushes = new Semaphore(3);
	
	// crawlId => TicketHitContainer
	private Map<Integer, TicketHitContainer> ticketHitContainerMap = new ConcurrentHashMap<Integer, TicketHitContainer>(); 
	private TicketListingCrawler ticketListingCrawler;
	private final Logger logger = LoggerFactory.getLogger(BulkTicketHitIndexer.class);
	private PostBackURL postBackUrl;
	private TicketHitIndexerStat stat = new TicketHitIndexerStat();

	public BulkTicketHitIndexer() {
		super();
	}
	
	public void clear(TicketListingCrawl crawl) {
		ticketHitContainerMap.remove(crawl.getId());
	}	

	/**
	 * @param ticketHit
	 * @return true if it is a new entry, false otherwise
	 * TODO: save/update the tickets in batch
	 */
	public int indexTicketHit(TicketHit ticketHit) {
		if (ticketHit == null) {
			return ERROR;
		}
		
		TicketHitContainer container = ticketHitContainerMap.get(ticketHit.getTicketListingCrawl().getId());
		if (container == null) {
			container = new TicketHitContainer(ticketHit.getTicketListingCrawl());
			ticketHitContainerMap.put(ticketHit.getTicketListingCrawl().getId(), container);
		}
		
		container.addTicket(ticketHit);		
		return NEW_ITEM;
	}

	public boolean flush(TicketListingCrawl ticketListingCrawl) {
		try {
			concurrentFlushes.acquire();
			if (ticketHitContainerMap.get(ticketListingCrawl.getId()) != null) {
				Collection<TicketHit> ticketHits = ticketHitContainerMap.get(ticketListingCrawl.getId()).getTicketHits();
				ticketHits = resolveTicketHitEventId(ticketListingCrawl, ticketHits);		
				
				long startIndexationTime = System.currentTimeMillis();
				__flush(ticketListingCrawl);
				ticketListingCrawl.setIndexationTime(ticketListingCrawl.getIndexationTime() + System.currentTimeMillis() - startIndexationTime);
			}else{
				__flush(ticketListingCrawl);
				XStream xstream = new XStream();
				xstream.autodetectAnnotations(false);
				xstream.alias("WSTicketWrapper", WSTicketWrapper.class);
				xstream.alias("Ticket", Ticket.class);
				WSTicketWrapper wsTicketWrapper = new WSTicketWrapper();
				ArrayList<Ticket> list = new ArrayList<Ticket>();
				wsTicketWrapper.setNewTickets(list);
				wsTicketWrapper.setUpdateTickets(list);
				wsTicketWrapper.setCrawlerId(ticketListingCrawl.getId());
				wsTicketWrapper.setEventId(ticketListingCrawl.getEventId());
				String newTicketsXml = xstream.toXML(wsTicketWrapper);
				Map<Integer, List<String>> urlMap= postBackUrl.getPostBackUrlMap();
				if(urlMap!= null && urlMap.size()!=0){
					List<String> urls=urlMap.remove(ticketListingCrawl.getId());
					if(urls!=null){
						try{
							HttpClient hc = new DefaultHttpClient();
							for(String url:urls){
								HttpPost hp = new HttpPost(url);
								NameValuePair nameValuePair = new BasicNameValuePair("ticketdata", newTicketsXml);
								List<NameValuePair> parameters = new ArrayList<NameValuePair>();
								parameters.add(nameValuePair);
								
								nameValuePair = new BasicNameValuePair("isLast", "true");
								parameters.add(nameValuePair);

								UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
								hp.setEntity(entity);
								SendRequest sr= new SendRequest(hc,hp);
								sr.setPriority(Thread.MAX_PRIORITY);
								sr.start();
							}
						}catch (Exception e) {
							e.fillInStackTrace();
							return false;
						}
					}
				}
				TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(), CrawlState.PERSISTED);
			}
			
			ticketListingCrawl.setCrawlPhase(CrawlPhase.EXPIRATION);
			Long startExpirationTime = System.currentTimeMillis();
	
			/*try {
				ticketUpdater.updateOutDatedExpiredTicketsFromCrawl(ticketListingCrawl);
				ticketUpdater.updateTicketsUpdatedBeforeFromCrawl(ticketListingCrawl, ticketListingCrawl.getStartCrawl());
			} catch (Exception e) {
				ticketListingCrawl.setCrawlState(CrawlState.EXCEPTION);
				String errorMessage = e.getMessage();
				if (errorMessage == null) {
					errorMessage = e.getClass().getName();
				}
				ticketListingCrawl.setErrorMessage(errorMessage);
				ticketListingCrawl.setException(e);			
				logger.warn("Error while flushing tickets to DB", e);
				return false;
			}*/
	
			ticketListingCrawl.setExpirationTime(System.currentTimeMillis() - startExpirationTime);
			return true;
		} catch(InterruptedException e) {
			logger.warn("Error while flushing tickets to DB", e);
			return false;
		} finally {
			concurrentFlushes.release();			
		}
	}

	private void __flush(TicketListingCrawl ticketListingCrawl) {
		String siteId;

		if (ticketListingCrawl.getSiteId().equals(Site.SEATWAVE_FEED)) {
			 siteId = Site.SEATWAVE;			
		} else {
			 siteId = ticketListingCrawl.getSiteId();
		}
		
		TicketHitContainer ticketHitContainer = ticketHitContainerMap.get(ticketListingCrawl.getId());
		
		Event event = DAORegistry.getEventDAO().get(ticketListingCrawl.getEventId());
		Collection<Ticket> expiredTickets = new ArrayList<Ticket>();
		Collection<DuplicateTicketMap> updatedMaps = new ArrayList<DuplicateTicketMap>();
		
		Map<Integer, Collection<TicketHit>> ticketHitsbyEventIdMap = null;
		if(ticketHitContainer == null || ticketHitContainer.getTicketHits() == null || ticketHitContainer.getTicketHits().isEmpty()){
			ticketHitsbyEventIdMap = new HashMap<Integer, Collection<TicketHit>>();
			ticketHitsbyEventIdMap.put(ticketListingCrawl.getEventId(), new ArrayList<TicketHit>());
		}else{
			ticketHitsbyEventIdMap = getTicketHitsByEventIdMap(resolveTicketHitEventId(ticketHitContainer.getTicketListingCrawl(), ticketHitContainer.getTicketHits()));
		}
		Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
		Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
		Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
		for(Synonym synonym:synonyms){
			if(synonym.getType().equals("Theater")){
				theaterSynonymMap.put(synonym.getName(), synonym);
			}else{
				concertSynonymMap.put(synonym.getName(), synonym);
			}
		}
		
		Map<String,Synonym> synonymMap = null;
		if(event.getEventType().equals(TourType.THEATER)){
			synonymMap = theaterSynonymMap;
		}else if(event.getEventType().equals(TourType.CONCERT)|| event.getEventType().equals(TourType.SPORT)){
			synonymMap = concertSynonymMap;
		}
		else{
			synonymMap =  new HashMap<String, Synonym>();
		}
		for(Map.Entry<Integer,Collection<TicketHit>>entry: ticketHitsbyEventIdMap.entrySet()) {
			
			int eventId = entry.getKey();
			Collection<TicketHit> ticketHits = entry.getValue();
			
			Map<String, Ticket> existingTicketMap = new HashMap<String, Ticket>();
			Map<String, Ticket> existingTicketsMap = new HashMap<String, Ticket>();
			
			Collection<Ticket> updatedTickets = new ArrayList<Ticket>();
			Collection<Ticket> newTickets = new ArrayList<Ticket>();
			
			Map<Integer, DuplicateTicketMap> eventExpiredTicketsMap = new HashMap<Integer, DuplicateTicketMap>();
			
			Long beforeLoadingFromDBTime = System.currentTimeMillis();
			Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllTicketsByEventAndSiteId(eventId, siteId);
			if (tickets != null) {
				for(Ticket ticket: tickets) {
					if(ticket.getTicketStatus().equals(TicketStatus.DISABLED)){
						continue;
					}
					existingTicketsMap.put(ticket.getTicketKey(), ticket);
					existingTicketMap.put(ticket.getTicketKey(), ticket);
				}
			}
	
			Long afterLoadingFromDBTime = System.currentTimeMillis();
			stat.incrementTotalDBLoadTime(afterLoadingFromDBTime - beforeLoadingFromDBTime);
			
			
			TourType tourType = event.getEventType();
	
			Collection<DuplicateTicketMap> dupTicketMaps = DAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(event.getId());
			
			Long startTime = System.currentTimeMillis();
			DateFormat df= new SimpleDateFormat("MM-dd-yy");
			Date now=new Date();
			for (TicketHit ticketHit: ticketHits) {
				// compute adjusted current price
				// check if the ticket is already in the DB
				String ticketKey = ticketHit.getSiteId() + "-" + ticketHit.getItemId();
				Ticket ticket = existingTicketsMap.remove(ticketKey); 
				if (ticket == null) {
					ticket = new Ticket();
					
					ticket.setInsertionDate(now);
					ticket.setPriceHistory(df.format(now)+":"+ticketHit.getCurrentPrice());
					newTickets.add(ticket);
					stat.incrementInsertedTicketCount(1);
				} else {
					if(dupTicketMaps != null && !dupTicketMaps.isEmpty()){
						for(DuplicateTicketMap dupTicketMap : dupTicketMaps){
							if(dupTicketMap.getTicketId().equals(ticket.getId())){
								eventExpiredTicketsMap.put(ticket.getId(), dupTicketMap);
							}
						}
					}
					try{
						if(ticket.getCurrentPrice().equals(ticketHit.getCurrentPrice()) 
								&& ticket.getQuantity().equals(ticketHit.getQuantity()) 
								&& ticketHit.getSection().equalsIgnoreCase(ticket.getSection()) && ticketHit.getRow().equalsIgnoreCase(ticket.getRow())){
							
							if(ticketHit.getTicketDeliveryType()==null && ticket.getTicketDeliveryType()==null ){ 
								continue;
							}else if(ticketHit.getTicketDeliveryType() == null || ticket.getTicketDeliveryType() == null){
								
							}else if(ticketHit.getTicketDeliveryType().equals(ticket.getTicketDeliveryType())){
								continue;
							}
						}
							
					}catch (Exception e) {
						e.printStackTrace();
						System.out.println(e.fillInStackTrace());
					}
					if (ticketHit.getCurrentPrice() != ticket.getCurrentPrice()) {
						int updateCount = (ticket.getPriceUpdateCount() != null)?ticket.getPriceUpdateCount():0;
						ticket.setPriceUpdateCount(updateCount + 1);
//						ticket.setLastUpdate(new Date());
						String priceHistory=ticket.getPriceHistory();
						if(priceHistory!=null){
							if(priceHistory.contains(df.format(now))){
								String []temp=priceHistory.split(",");
								String history="";
								for(String price:temp){
									if(!price.contains(df.format(now))){
										history=history + "," + price;
									}else{
										history=history + "," + df.format(now)+":"+ticket.getCurrentPrice();
									}
								}
								ticket.setPriceHistory(history);
							}else{
								ticket.setPriceHistory(priceHistory + "," + df.format(now)+":"+ticket.getCurrentPrice());
							}
						}else{
							ticket.setPriceHistory(df.format(now)+":"+ticket.getCurrentPrice());
						}
					}
					updatedTickets.add(ticket);
					stat.incrementUpdatedTicketCount(1);
				}

				updateTicketFromTicketHit(ticket, ticketHit, tourType,synonymMap);
				if (!ticketHit.getTicketStatus().equals(TicketStatus.ACTIVE)) {
					expiredTickets.add(ticket);
				}
			}
			if(!existingTicketsMap.values().isEmpty()){
				for(Ticket ticket:existingTicketsMap.values()){
					if(ticket.getTicketStatus().equals(TicketStatus.SOLD)){
						continue;
					}
					ticket.setTicketStatus(TicketStatus.DISABLED);
					ticket.setLastUpdate(now);
					tickets.remove(ticket);
				}
			}
			long finishedIndexingTime = System.currentTimeMillis();
//			ArrayList<Ticket> ticketsXmlList = new ArrayList<Ticket>();
//			TicketData ticketData = new TicketData();
//			ticketData.setCrawlerId(ticketListingCrawl.getId());
			try {
				//Note: Commented by pratap to use cache write behind.
				//DAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);
				//CacheDAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);
//				ticketData.setNewTickets((List<Ticket>)newTickets);
//				ticketsXmlList.addAll(newTickets);
//				
//				XStream xstream = new XStream();
//				String newTicketsXml = xstream.toXML(newTickets);
			} catch(Exception e) {
				try {
					stat.incrementInsertedTicketWarningCount(1);
					//Note: Commented by pratap to use cache write behind.
					//DAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);
					
//					CacheDAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);
//					ticketData.setNewTickets((List<Ticket>)newTickets);
//					ticketsXmlList.addAll(newTickets);
				} catch (Exception e2) {
					stat.incrementInsertedTicketErrorCount(1);
				    e2.printStackTrace();
					//throw new RuntimeException(e);				
				}
			}
	
			long finishedDBInsertTime = System.currentTimeMillis();
			
			try {
				//Note: Commented by pratap to use cache write behind.
				//DAORegistry.getTicketDAO().updateAll(updatedTickets);
				
//				CacheDAORegistry.getTicketDAO().saveOrUpdateAll(updatedTickets);
//				ticketData.setUpdateTickets((List<Ticket>)updatedTickets);
//				ticketsXmlList.addAll(updatedTickets);
			} catch(Exception e) {
				stat.incrementUpdatedTicketErrorCount(1);
				throw new RuntimeException(e);
			}
			synchronized (postBackUrl) {
				Map<Integer, List<String>> urlMap= postBackUrl.getPostBackUrlMap();
				if(urlMap!= null && urlMap.size()!=0){
//				if(ticketListingCrawl.getXmlPostBack() != null && ticketListingCrawl.getXmlPostBack()){
					List<String> urls=urlMap.remove(ticketListingCrawl.getId());
					if(urls!=null){
						try{
							XStream xstream = new XStream();
							xstream.autodetectAnnotations(false);
							xstream.alias("WSTicketWrapper", WSTicketWrapper.class);
							xstream.alias("Ticket", Ticket.class);
							WSTicketWrapper wsTicketWrapper = new WSTicketWrapper();
							Map<Integer, Integer> idmap = new HashMap<Integer, Integer>();
							for(Ticket tic : updatedTickets){
								Integer cnt = idmap.get(tic.getId());
								if(cnt == null){
									cnt = 1;
								}else{
									cnt++;
								}
								idmap.put(tic.getId(), cnt);					
							}
							int newi=0;
							int updatedi=0;
							while(newi<=newTickets.size() || updatedi<=updatedTickets.size()){
								int prevNewi=newi;
								int prevUpdatei=updatedi;
								if(newTickets.size()>newi+1000){
									newi+=1000;
								}else{
									int temp= newTickets.size()-newi;
//									if(temp==0){
//										temp=1;
//									}
									newi+=temp;
								}
								
								List<Ticket> newSubList= null;
								newSubList =new ArrayList<Ticket>(((ArrayList<Ticket>)(newTickets)).subList(prevNewi, newi));
								
								if(updatedTickets.size()>updatedi+1000){
									updatedi+=1000;
								}else{
									int temp= updatedTickets.size()-updatedi;
//									if(temp==0){
//										temp=1;
//									}
									updatedi+=temp;
								}
								
								List<Ticket> updatedSubList= null;
								updatedSubList =new ArrayList<Ticket>(((ArrayList<Ticket>)updatedTickets).subList(prevUpdatei, updatedi));
								
								String isLast;
								if(newi==newTickets.size() && updatedi==updatedTickets.size()){
									isLast="true";
									newi+=1;
									updatedi+=1;
								}else{
									isLast="false";
								}
								wsTicketWrapper.setNewTickets((ArrayList<Ticket>)newSubList);
								wsTicketWrapper.setUpdateTickets((ArrayList<Ticket>)updatedSubList);
								wsTicketWrapper.setCrawlerId(ticketListingCrawl.getId());
								wsTicketWrapper.setEventId(eventId);
								String newTicketsXml = xstream.toXML(wsTicketWrapper);
									
								for(String url:urls){
//										String postbackurl = urls.get(ticketListingCrawl.getId());
									//postbackurl = "http://192.168.0.77:8080/zoneplatform-1/zp/WSPostTickets";
									
									HttpClient hc = new DefaultHttpClient();
				//					HttpGet hg = new HttpGet(postbackurl);
				//					HttpParams httpParams = new BasicHttpParams();
				//					httpParams.setParameter("eventId", eventId);
				//					httpParams.setParameter("postBackUrl", postbackurl);
				//					hg.setParams(httpParams);
				//					HttpResponse resp = hc.execute(hg);
									HttpPost hp = new HttpPost(url);
									NameValuePair nameValuePair = new BasicNameValuePair("ticketdata", newTicketsXml);
									
									List<NameValuePair> parameters = new ArrayList<NameValuePair>();
									parameters.add(nameValuePair);
									nameValuePair = new BasicNameValuePair("isLast", isLast);
									parameters.add(nameValuePair);
									
									UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
									hp.setEntity(entity);
									SendRequest sr= new SendRequest(hc,hp);
									sr.setPriority(Thread.MAX_PRIORITY);
									sr.start();
									/*HttpResponse response = hc.execute(hp);
									BufferedReader in = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
									StringBuffer sb = new StringBuffer();
									String line = "";
									String lineSeparator = System.getProperty("line.separator");
									while((line=in.readLine()) != null){
										sb.append(line + lineSeparator);
									}
									
									String responseString = sb.toString();
									*/
								}
								
							}
							urls=null;
//							}
//						}
							
					}catch(Exception e1){
						e1.printStackTrace();
					}finally{
//						TicketListingCrawl crawl= ticketListingCrawler.getTicketListingCrawlById(ticketListingCrawl.getId());
//						crawl.setPostBackUrl(null);
//						crawl.setXmlPostBack(false);
					}
					
					
				}
			}
			}

//				ticketListingCrawl.setPostBackUrl(null);
//				ticketListingCrawl.setXmlPostBack(false);
//				DAORegistry.getTicketListingCrawlDAO().saveOrUpdate(ticketListingCrawl);
			
			try{
				/*
				NamedCache ticketCache = CacheFactory.getCache("ticketCache");
				Long timeStamp = UniqueLongIDGenerator.getInstance().getUniqueTimestamp();
				Long uuid = UniqueLongIDGenerator.getInstance().getUID();
				TicketCacheKey ticketCacheKey = new TicketCacheKey(timeStamp, uuid);
				ticketCache.put(ticketCacheKey, ticketData);
				*/
				DAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);
				DAORegistry.getTicketDAO().saveOrUpdateAll(updatedTickets);
				DAORegistry.getTicketDAO().saveOrUpdateAll(existingTicketsMap.values());
				TicketPersistenceTracker.getInstance().setTicketCrawlStatus(ticketListingCrawl.getId(),CrawlState.PERSISTED);
			}catch(Exception e){
				e.printStackTrace();
			}
			

			long finishedDBUpdateTime = System.currentTimeMillis();
	
			if(!eventExpiredTicketsMap.isEmpty() && !newTickets.isEmpty()){
				for(Ticket newTicket : newTickets){
					for(Integer removedTicketId : eventExpiredTicketsMap.keySet()){
						
						//look for new tickets that match expired tickets with duplicate mappings
						if(this.matchDuplicateTicket(existingTicketMap.get(removedTicketId), newTicket)) {
							eventExpiredTicketsMap.get(removedTicketId).setTicketId(newTicket.getId());
							updatedMaps.add(eventExpiredTicketsMap.remove(removedTicketId));
							
							//break the inner loop
							break;
						}
					}
				}
			}
			
			stat.incrementTotalDBInsertTime(finishedDBInsertTime - finishedIndexingTime);
			stat.incrementTotalDBUpdateTime(finishedDBUpdateTime - finishedDBInsertTime);
			stat.incrementTotalIndexingTime(finishedIndexingTime - startTime);
		
		}
		
		ticketHitContainerMap.remove(ticketListingCrawl.getId());
		
		if(!updatedMaps.isEmpty()) {
			try {
				DAORegistry.getDuplicateTicketMapDAO().updateAll(updatedMaps);
			} catch(Exception e) {
				logger.error("Exception updating duplicate ticket maps: " + e.getMessage());
				//throw new RuntimeException(e);
			}
		}
		
		for (Ticket ticket: expiredTickets) {
			DAORegistry.getBookmarkDAO().deleteTicketBookmarks(ticket.getId());
			DAORegistry.getDuplicateTicketMapDAO().delete(ticket.getId(), true);
		}
	}
	
	private boolean matchDuplicateTicket(Ticket ticket1, Ticket ticket2){
		
		if(ticket1 == null || ticket2 == null){
			return false;
		}
		
		//if section row seat qty price are the same
		if(compare(ticket1.getSection(), ticket2.getSection()) 
				&& compare(ticket1.getRow(), ticket2.getRow())
				&& compare(ticket1.getSeat(), ticket2.getSeat())
				&& ticket1.getRemainingQuantity().equals(ticket2.getRemainingQuantity())
				&& ticket1.getCurrentPrice().equals(ticket2.getCurrentPrice())){
			return true;
		}
		return false;
	}
	
	private boolean compare(String string1, String string2){
		if(string1 == null && string2 == null){
			return true;
		}
		if((string1 != null && string2 == null) || (string1 == null && string2 != null)){
			return false;
		}
		return string1.equals(string2);
	}
	
	public TicketHitIndexerStat getStat() {
		return stat;
	}
		
	public void resetStat() {
		stat.reset();
	}

	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}

	public PostBackURL getPostBackUrl() {
		return postBackUrl;
	}

	public void setPostBackUrl(PostBackURL postBackUrl) {
		this.postBackUrl = postBackUrl;
	}
	
	public class SendRequest extends Thread{
		HttpClient hc;
		HttpPost hp;
		public SendRequest(HttpClient hc,HttpPost hp) {
			this.hc=hc;
			this.hp=hp;
		}
		@Override
		public void run() {
			try {
				hc.execute(hp);
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		
	}
}
