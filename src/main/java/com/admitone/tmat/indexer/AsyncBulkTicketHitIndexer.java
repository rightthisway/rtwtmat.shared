package com.admitone.tmat.indexer;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.PriceRounding;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.ticketfetcher.TicketHit;

/**
 * Async Bulk Ticket Hit Indexer
 * Resolve event if needed and persist the ticket into the database.
 */

@Deprecated
public class AsyncBulkTicketHitIndexer extends TicketHitIndexer {
	// eventId-siteId => TicketHitContainer
	private Map<Integer, TicketHitContainer> ticketHitContainerMap = new HashMap<Integer, TicketHitContainer>();
	
	private final Logger logger = LoggerFactory.getLogger(AsyncBulkTicketHitIndexer.class);

	private TicketHitIndexerStat stat = new TicketHitIndexerStat();
	
	public AsyncBulkTicketHitIndexer() {
		super();
		Thread thread = new Thread(new BulkTicketHitIndexerThread(ticketHitContainerMap, stat));
		thread.start();
	}
	

	public void clear(TicketListingCrawl crawl) {
		ticketHitContainerMap.remove(crawl.getId());
	}

	/**
	 * @param ticketHit
	 * @return true if it is a new entry, false otherwise
	 * FIXME: made the method synchronized to prevent the threads to write simultaneously in the DB
	 * TODO: save/update the tickets in batch
	 */
	public int indexTicketHit(TicketHit ticketHit) {
		if (ticketHit == null) {
			return ERROR;
		}
		
		// if it becomes too complicate here, we can write a resolver class
		// to resolve the incorrect fields
		
		// check validity of the result
		// TODO: this might be done in another class that allow some rules
		// for now to find the venue,we look at the date
		
		Integer eventId = ticketHit.getEventId();
		if (eventId == null) {
			eventId = ticketHit.getTicketListingCrawl().getEventId();
		}
		
		if (eventId == null) {
			Event event = null;
			if (ticketHit.getEventDate() == null) {
				logger.info("Ticket has no date,skip it: " + ticketHit);
				return ERROR;
			}

//			Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByTour(ticketHit.getTicketListingCrawl().getTourId());
			Event eventDb = DAORegistry.getEventDAO().get(eventId);
			// find the event based on the date
//			for (Event e:events) {
				if(eventDb.getDate()!=null){
					Calendar dbCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
					dbCalendar.setTime(eventDb.getDate());
					
					Calendar hitCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
					hitCalendar.setTime(ticketHit.getEventDate());
					
					if (dbCalendar.get(Calendar.DAY_OF_YEAR) == hitCalendar.get(Calendar.DAY_OF_YEAR)
							&& dbCalendar.get(Calendar.YEAR) == hitCalendar.get(Calendar.YEAR)) {
						event = eventDb;
	//					break;
					}
				}
				// TODO: check city,province, etc ....
//			}
			if (event == null) {
				logger.info("NOMATCH - Skipped " + ticketHit);
				return ERROR;
			}
			eventId = event.getId();
		}

		ticketHit.setEventId(eventId);

		TicketHitContainer container = ticketHitContainerMap.get(ticketHit.getTicketListingCrawl().getId());
		if (container == null) {
			container = new TicketHitContainer(ticketHit.getTicketListingCrawl());
			ticketHitContainerMap.put(ticketHit.getTicketListingCrawl().getId(), container);
		}
		
		container.addTicket(ticketHit);
		
		return NEW_ITEM;
	}
	
	public boolean flush(TicketListingCrawl crawl) {
		// format data
		TicketHitContainer ticketHitContainer = ticketHitContainerMap.get(crawl.getId());
		if (ticketHitContainer == null) {
			return false;
		}
		
		ticketHitContainer.setFlushed(true);
		return true;
	}
	
	public TicketHitIndexerStat getStat() {
		return stat;
	}		

	public void resetStat() {
		stat.reset();
	}
	
	public class BulkTicketHitIndexerThread extends Thread {
		private Map<Integer, TicketHitContainer> ticketHitContainerMap = new HashMap<Integer, TicketHitContainer>(); 
		private TicketHitIndexerStat stat;
		
		public BulkTicketHitIndexerThread(Map<Integer, TicketHitContainer> ticketHitContainerMap, TicketHitIndexerStat stat) {
			this.ticketHitContainerMap = ticketHitContainerMap;
			this.stat = stat;
		}

		public void flushAll(Collection<TicketHitContainer> containers) {
			_flushAll(containers);
		}

		private void _flushAll(Collection<TicketHitContainer> containers) {
			if (containers.isEmpty()) {
				return;
			}
			
			Map<String, Ticket> existingTicketMap = new HashMap<String, Ticket>();
			
			Collection<Ticket> updatedTickets = new ArrayList<Ticket>();
			Collection<Ticket> newTickets = new ArrayList<Ticket>();
			Collection<Ticket> expiredTickets = new ArrayList<Ticket>();
			
			Long beforeLoadingFromDBTime = System.currentTimeMillis();
			
			// TODO: maybe can do a bulk load if it's slow

			// eventId-siteId => price adjustment
			Map<Integer, Double> percentAdjustmentMap = new HashMap<Integer, Double>();
			
			// siteId => PriceRounding
			Map<String, PriceRounding> priceRoundingMap = new HashMap<String, PriceRounding>();

			// eventId => TourType
			Map<Integer, TourType> tourTypeMap = new HashMap<Integer, TourType>();

			for (TicketHitContainer container: containers) {
				String siteId;
				if (container.getTicketListingCrawl().getSiteId().equals(Site.SEATWAVE_FEED)) {
					 siteId = Site.SEATWAVE;			
				} else {
					 siteId = container.getTicketListingCrawl().getSiteId();
				}
				
				Map<Integer, Collection<TicketHit>> ticketHitsByEventIdMap = getTicketHitsByEventIdMap(container.getTicketHits());
				
				for (Map.Entry<Integer, Collection<TicketHit>> entry: ticketHitsByEventIdMap.entrySet()) {
					Integer eventId = entry.getKey();
					
					
					// following line is not working with mssql
					// for(Ticket ticket: DAORegistry.getTicketDAO().getAllSimpleTicketsByEventAndSiteId(eventId, siteId)) {
					for(Ticket ticket: DAORegistry.getTicketDAO().getAllTicketsByEventAndSiteId(eventId, siteId)) {
						existingTicketMap.put(ticket.getTicketKey(), ticket);
					}
	
					EventPriceAdjustment priceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(eventId, siteId);
					if (priceAdjustment != null) {
						percentAdjustmentMap.put(container.getTicketListingCrawl().getId(), priceAdjustment.getPercentAdjustment());
					}
	
					if (priceRoundingMap.get(siteId) == null) {
						PriceRounding priceRounding = PriceRounding.NONE;
						Site site = DAORegistry.getSiteDAO().get(siteId);
						if (site != null) {
							priceRounding = site.getPriceRounding();
						}		
						priceRoundingMap.put(siteId, priceRounding);
					}
					
					if (tourTypeMap.get(eventId) == null) {
						Event event = DAORegistry.getEventDAO().get(eventId);
						TourType tourType = event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getTourType();
						tourTypeMap.put(eventId, tourType);
					}
				}

			}

			Long afterLoadingFromDBTime = System.currentTimeMillis();
			stat.incrementTotalDBLoadTime(afterLoadingFromDBTime - beforeLoadingFromDBTime);			
			
			Long startTime = System.currentTimeMillis();
			Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
			Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
			Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
			for(Synonym synonym:synonyms){
				if(synonym.getType().equals("Theater")){
					theaterSynonymMap.put(synonym.getName(), synonym);
				}else{
					concertSynonymMap.put(synonym.getName(), synonym);
				}
			}
			for (TicketHitContainer container: containers) {
				String siteId;
				if (container.getTicketListingCrawl().getSiteId().equals(Site.SEATWAVE_FEED)) {
					 siteId = Site.SEATWAVE;			
				} else {
					 siteId = container.getTicketListingCrawl().getSiteId();
				}					
				
				Map<Integer, Collection<TicketHit>> ticketHitsByEventIdMap = getTicketHitsByEventIdMap(resolveTicketHitEventId(container.getTicketListingCrawl(), container.getTicketHits()));
				
				for (Map.Entry<Integer, Collection<TicketHit>> entry: ticketHitsByEventIdMap.entrySet()) {
					
					Integer eventId = entry.getKey();
					Collection<TicketHit> ticketHits = entry.getValue();
					
					TourType tourType = tourTypeMap.get(eventId);
//					Double percentAdjustment = percentAdjustmentMap.get(container.getTicketListingCrawl().getId());
//					PriceRounding priceRounding = priceRoundingMap.get(siteId); 
					
					for (TicketHit ticketHit: ticketHits) {
		
						// compute adjusted current price
						// check if the ticket is already in the DB
						String ticketKey = ticketHit.getSiteId() + "-" + ticketHit.getItemId();
						Ticket ticket = existingTicketMap.get(ticketKey); 
						if (ticket == null) {
							ticket = new Ticket();
							ticket.setInsertionDate(new Date());
							boolean alreadyPresent = false;
							for (Ticket tix: newTickets) {
								if (ticket.getId().equals(tix.getId())) {
									alreadyPresent = true;
									break;
								}
							}
							if (!alreadyPresent) {
								newTickets.add(ticket);
								stat.incrementInsertedTicketCount(1);
							}
						} else {
							updatedTickets.add(ticket);
							if (ticketHit.getCurrentPrice() != ticket.getCurrentPrice()) {
								int updateCount = (ticket.getPriceUpdateCount() != null)?ticket.getPriceUpdateCount():0;
								ticket.setPriceUpdateCount(updateCount + 1);
							}
							stat.incrementUpdatedTicketCount(1);
						}
						if(ticket.getEvent().getEventType().equals(TourType.THEATER)){
							updateTicketFromTicketHit(ticket, ticketHit, tourType,theaterSynonymMap);
						}else if(ticket.getEvent().getEventType().equals(TourType.CONCERT)){
							updateTicketFromTicketHit(ticket, ticketHit, tourType,concertSynonymMap);
						}else{
							updateTicketFromTicketHit(ticket, ticketHit, tourType,null);
						}
												
						if (!ticketHit.getTicketStatus().equals(TicketStatus.ACTIVE)) {
							expiredTickets.add(ticket);
						}
		
						ticket.setTicketStatus(ticketHit.getTicketStatus());
						if ((ticketHit.getSeller() != null) && (ticketHit.getSeller() != null)) {
							ticket.setSeller(ticketHit.getSeller());
						}
		
						if (ticketHit.getTicketListingCrawl() != null) {
							ticket.setTicketListingCrawlId(ticketHit.getTicketListingCrawl().getId());
						}
					}
				}
			}

			long finishedIndexingTime = System.currentTimeMillis();

			try {
				DAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);
			} catch(Exception e) {
				/*try {
					stat.incrementInsertedTicketWarningCount(1);
					DAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);				
				} catch (Exception e2) {*/
					stat.incrementInsertedTicketErrorCount(1);
					// e2.printStackTrace();
					throw new RuntimeException(e);				
//				}
			}

			long finishedDBInsertTime = System.currentTimeMillis();
			
			try {
				DAORegistry.getTicketDAO().updateAll(updatedTickets);
			} catch(Exception e) {
				stat.incrementUpdatedTicketErrorCount(1);
				//e.printStackTrace();
				throw new RuntimeException(e);
			}
			
			long finishedDBUpdateTime = System.currentTimeMillis();

			stat.incrementTotalDBInsertTime(finishedDBInsertTime - finishedIndexingTime);
			stat.incrementTotalDBUpdateTime(finishedDBUpdateTime - finishedDBInsertTime);
			stat.incrementTotalIndexingTime(finishedIndexingTime - startTime);
			
			for (Ticket ticket: expiredTickets) {
				DAORegistry.getBookmarkDAO().deleteTicketBookmarks(ticket.getId());
			}
			
			for (TicketHitContainer container: containers) {
				TicketListingCrawl ticketListingCrawl = container.getTicketListingCrawl();
				ticketListingCrawl.setCrawlPhase(CrawlPhase.EXPIRATION);
				Long startExpirationTime = System.currentTimeMillis();

				try {
					ticketUpdater.updateOutDatedExpiredTicketsFromCrawl(ticketListingCrawl);
					ticketUpdater.updateTicketsUpdatedBeforeFromCrawl(ticketListingCrawl, ticketListingCrawl.getStartCrawl());		
				} catch (Exception e) {
					ticketListingCrawl.setCrawlState(CrawlState.EXCEPTION);
					String errorMessage = e.getMessage();
					if (errorMessage == null) {
						errorMessage = e.getClass().getName();
					}
					ticketListingCrawl.setErrorMessage(errorMessage);
					ticketListingCrawl.setException(e);
					// TODO Auto-generated catch block
					e.printStackTrace();
				}			
				ticketListingCrawl.setExpirationTime(System.currentTimeMillis() - startExpirationTime);
			}
		}
		
		public void run() {
			Collection<TicketHitContainer> containers = new ArrayList<TicketHitContainer>();
			while(true) {
				try {
					containers.clear();
					for (Entry<Integer, TicketHitContainer> entry: ticketHitContainerMap.entrySet()) {
						TicketHitContainer container = entry.getValue();
						if (container.isFlushed()) {
							containers.add(container);
						}
					}
					
					flushAll(containers);
					
					for (TicketHitContainer container: containers) {
						ticketHitContainerMap.remove(container.getTicketListingCrawl().getId());
					}
					
					try {
						// one second delay, it has to be small to avoid stale data
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}