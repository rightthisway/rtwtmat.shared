package com.admitone.tmat.indexer;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.CrawlPhase;
import com.admitone.tmat.enums.CrawlState;
import com.admitone.tmat.enums.PriceRounding;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.ticketfetcher.TicketHit;

/**
 * Simple Ticket Hit Indexer
 * Resolve event if needed and persist the ticket into the database.
 */
@Deprecated
public class SimpleTicketHitIndexer extends TicketHitIndexer {
	private final Logger logger = LoggerFactory.getLogger(SimpleTicketHitIndexer.class);
	private TicketHitIndexerStat stat = new TicketHitIndexerStat();
	
	public SimpleTicketHitIndexer() {
		super();
	}

	/**
	 * @param ticketHit
	 * @return true if it is a new entry, false otherwise
	 * FIXME: made the method synchronized to prevent the threads to write simultaneously in the DB
	 * TODO: save/update the tickets in batch
	 */
	public synchronized int indexTicketHit(TicketHit ticketHit) {		
		if (ticketHit == null) {
			return ERROR;
		}
		
		Date startDate = new Date();
				
		ticketHit = resolveTicketHitEventId(ticketHit.getTicketListingCrawl(), ticketHit);
		if (ticketHit == null) {
			return ERROR;
		}
		
		try {
			// compute adjusted current price
			
			Double percentAdjustment = null;
			EventPriceAdjustment priceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(ticketHit.getEventId(), ticketHit.getTicketListingCrawl().getSiteId());
			if (priceAdjustment != null) {
				percentAdjustment = priceAdjustment.getPercentAdjustment();
			}
	
			PriceRounding priceRounding = PriceRounding.NONE;
			Site site = DAORegistry.getSiteDAO().get(ticketHit.getSiteId());
			if (site != null) {
				priceRounding = site.getPriceRounding();
			}		
			
			boolean ticketExists; 
			// check if the ticket is already in the DB
			String ticketKey = ticketHit.getSiteId() + "-" + ticketHit.getItemId();
			Ticket ticket = null;
			
			for (Ticket curTicket: DAORegistry.getTicketDAO().getAllTicketsByEvent(ticketHit.getEventId())) {
				if (curTicket.getTicketKey().equals(ticketKey)) {
					ticket = curTicket;
					break;
				}
			}
			if (ticket == null) {
				logger.info("Indexing " + ticketHit);
				
				// create it
				ticket = new Ticket(ticketHit.getSiteId(), ticketHit.getItemId());
				ticket.setInsertionDate(new Date());
				ticketExists = false;
			} else {
				ticketExists = true;
			}
			
			updateTicketFromTicketHit(ticket, ticketHit, ticket.getEvent().getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getTourType(),null);
		
			if (ticketExists) {
				if (ticketHit.getCurrentPrice() != ticket.getCurrentPrice()) {
					int updateCount = (ticket.getPriceUpdateCount() != null)?ticket.getPriceUpdateCount():0;
					ticket.setPriceUpdateCount(updateCount + 1);
				}
	
				if (!ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
					DAORegistry.getBookmarkDAO().deleteTicketBookmarks(ticket.getId());
				}
			}
	
			Date finishedIndexingDate = new Date();

			int result;
			
			if (!ticketExists) {
				try {
					DAORegistry.getTicketDAO().save(ticket);
				} catch(Exception e) {
					stat.incrementInsertedTicketErrorCount(1);
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				result = NEW_ITEM;
				stat.incrementInsertedTicketCount(1);
			} else {
				try {
				
					DAORegistry.getTicketDAO().update(ticket);
				} catch(Exception e) {
					stat.incrementUpdatedTicketErrorCount(1);
					e.printStackTrace();
					throw new RuntimeException(e);
				}
				result = EXISTING_ITEM;
				stat.incrementUpdatedTicketCount(1);
			}

			Date finishedDBUpdateDate = new Date();

			stat.incrementTotalIndexingTime(finishedIndexingDate.getTime() - startDate.getTime());
			
			if (!ticketExists) {
				stat.incrementTotalDBInsertTime(finishedDBUpdateDate.getTime() - finishedIndexingDate.getTime());
			} else {
				stat.incrementTotalDBUpdateTime(finishedDBUpdateDate.getTime() - finishedIndexingDate.getTime());
			}
			
			return result;
		} catch (Exception e) {
			logger.error("*******************  ERROR WHILE INDEXING: " + ticketHit);
			e.printStackTrace();
			return ERROR;
		}
	}
	
	public TicketHitIndexerStat getStat() {
		return stat;
	}
	
	public void resetStat() {
		stat.reset();
	}
	
	public boolean flush(TicketListingCrawl ticketListingCrawl) {
		ticketListingCrawl.setCrawlPhase(CrawlPhase.EXPIRATION);
		Long startExpirationTime = System.currentTimeMillis();

		try {
			ticketUpdater.updateOutDatedExpiredTicketsFromCrawl(ticketListingCrawl);
			ticketUpdater.updateTicketsUpdatedBeforeFromCrawl(ticketListingCrawl, ticketListingCrawl.getStartCrawl());		
		} catch (Exception e) {
			ticketListingCrawl.setCrawlState(CrawlState.EXCEPTION);
			String errorMessage = e.getMessage();
			if (errorMessage == null) {
				errorMessage = e.getClass().getName();
			}
			ticketListingCrawl.setErrorMessage(errorMessage);
			ticketListingCrawl.setException(e);			
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		
		ticketListingCrawl.setExpirationTime(System.currentTimeMillis() - startExpirationTime);
		return true;
	}
}
