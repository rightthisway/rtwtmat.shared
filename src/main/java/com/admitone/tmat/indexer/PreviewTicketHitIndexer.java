package com.admitone.tmat.indexer;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.PriceRounding;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.ticketfetcher.TicketHit;

/**
 * Preview Ticket Hit Indexer.
 * This class is used to preview the ticket hit that would be indexed for a crawl. 
 */
public class PreviewTicketHitIndexer extends TicketHitIndexer {
	private Collection<Ticket> indexedTickets = new ArrayList<Ticket>();
	private Integer previewLimitSize;
	
	public PreviewTicketHitIndexer(Integer previewLimitSize) {
		this.previewLimitSize = previewLimitSize;
	}

	@Override
	public int indexTicketHit(TicketHit ticketHit) {
		if (ticketHit == null) {
			return ERROR;
		}
				
		String ticketKey = ticketHit.getSiteId() + "-" + ticketHit.getItemId();

		if (previewLimitSize != null && indexedTickets.size() >= previewLimitSize) {
			throw new InterruptedTicketListingCrawlException("Preview Limit has been reached.");
		}
		
		ticketHit = resolveTicketHitEventId(ticketHit.getTicketListingCrawl(), ticketHit);
		
		if (ticketHit == null) {			
			return ERROR;
		}
		
		Double percentAdjustment = null;
		EventPriceAdjustment priceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(ticketHit.getEventId(), ticketHit.getTicketListingCrawl().getSiteId());
		if (priceAdjustment != null) {
			percentAdjustment = priceAdjustment.getPercentAdjustment();
		}

		PriceRounding priceRounding = PriceRounding.NONE;
		Site site = DAORegistry.getSiteDAO().get(ticketHit.getSiteId());
		if (site != null) {
			priceRounding = site.getPriceRounding();
		}
		
		Event event = DAORegistry.getEventDAO().get(ticketHit.getEventId());
		TourType tourType = event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getTourType();
		
		Ticket ticket = new Ticket(ticketHit.getSiteId(), ticketHit.getItemId());
		updateTicketFromTicketHit(ticket, ticketHit, tourType,null);
		
		indexedTickets.add(ticket);
		return NEW_ITEM;
	}
	
	public Collection<Ticket> getIndexedTickets() {
		return indexedTickets;
	}

}
