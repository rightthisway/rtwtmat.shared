package com.admitone.tmat.indexer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.ticketfetcher.TicketHit;

public class TicketHitContainer {
	private Date lastUpdate;
	private Boolean flushed = false;
	private TicketListingCrawl crawl;
	// private Map<Integer, Collection<TicketHit>>ticketHitsByEventId = new HashMap<Integer, Collection<TicketHit>>();

	public TicketHitContainer(TicketListingCrawl crawl) {
		this.crawl = crawl;
		this.lastUpdate = new Date();
	}
	
	private Collection<TicketHit> ticketHits = new ArrayList<TicketHit>();

	public synchronized void addTicket(TicketHit ticketHit) {
		for (TicketHit hit: ticketHits) {
			if (ticketHit.getItemId().equals(hit.getItemId())) {
				return;
			}
		}
		
		ticketHits.add(ticketHit);
	
		/*
		Collection<TicketHit> ticketHitList = ticketHitsByEventId.get(ticketHit.getEventId());
		if (ticketHitList == null) {
			ticketHitList = new ArrayList<TicketHit>();
			ticketHitsByEventId.put(ticketHit.getEventId(), ticketHitList);
		}
		ticketHitList.add(ticketHit);
		*/
		
		this.lastUpdate = new Date();
	}
	
	public Collection<TicketHit> getTicketHits() {
		return ticketHits;
	}

	public void setTicketHits(Collection<TicketHit> ticketHits) {
		this.ticketHits = ticketHits;
	}
	
	public Date getLastUpdate() {
		return lastUpdate;
	}
	
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
	public void setFlushed(boolean flushed) {
		this.flushed = flushed;
	}
	
	public boolean isFlushed() {
		return flushed;
	}

	public TicketListingCrawl getTicketListingCrawl() {
		return crawl;
	}
	
	/*
	public Map<Integer, Collection<TicketHit>> getTicketHitsByEventId() {
		return ticketHitsByEventId;
	}
	*/
}
