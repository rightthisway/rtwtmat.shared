package com.admitone.tmat.indexer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketUpdater;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.utils.SectionRowStripper;

/**
 * Abstract Ticket Hit Indexer class.
 */
public abstract class TicketHitIndexer implements Serializable{
	private final Logger logger = LoggerFactory.getLogger(TicketHitIndexer.class);	
	
	protected TicketUpdater ticketUpdater;
	
	/**
	 * Returned value when the indexed ticket is new.
	 */
	public final static int NEW_ITEM = 1;

	/**
	 * Returned value when the indexed ticket was already in the database.
	 */
	public final static int EXISTING_ITEM = 2;

	/**
	 * Returned value when the indexed ticket is incorrect.
	 */
	public final static int ERROR = -1;

	public TicketHitIndexer() {
	}

	/**
	 * Index ticket hit.
	 * @param ticketHit
	 * @return
	 */
	
	public abstract int indexTicketHit(TicketHit ticketHit);

	public boolean flush(TicketListingCrawl crawl) {
		return false;
	}
	
	public void clear(TicketListingCrawl crawl) {
	}
	
	public TicketHitIndexerStat getStat() {
		return new TicketHitIndexerStat();
	}
	
	public void resetStat() {
	}

	public TicketUpdater getTicketUpdater() {
		return ticketUpdater;
	}

	public void setTicketUpdater(TicketUpdater ticketUpdater) {
		this.ticketUpdater = ticketUpdater;
	}

	protected Map<Integer, Collection<TicketHit>> getTicketHitsByEventIdMap(Collection<TicketHit> ticketHits) {
		Map<Integer, Collection<TicketHit>> ticketHitsByEventIdMap = new HashMap<Integer, Collection<TicketHit>>();
		try{
			for(TicketHit ticketHit: ticketHits) {
				Collection<TicketHit> ticketHitsForEventId = ticketHitsByEventIdMap.get(ticketHit.getEventId());
				if (ticketHitsForEventId == null) {
					ticketHitsForEventId = new ArrayList<TicketHit>();
					ticketHitsByEventIdMap.put(ticketHit.getEventId(), ticketHitsForEventId);
				}
				ticketHitsForEventId.add(ticketHit);
			}
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("TICKETHITINDEXER EXCEPTION");
		}
		
		
		return ticketHitsByEventIdMap;
	}
	
	
	protected void updateTicketFromTicketHit(Ticket ticket, TicketHit ticketHit, TourType tourType,Map<String,Synonym> synonymsMap) {
		try{
		ticket.setSiteId(ticketHit.getSiteId());
		ticket.setItemId(ticketHit.getItemId());

		ticket.setEventId(ticketHit.getEventId());
		ticket.setTicketType(ticketHit.getTicketType());

		ticket.setTicketDeliveryType(ticketHit.getTicketDeliveryType());
		Integer soldQty =0;
		if(ticket.getId()!=null){
			soldQty = ticket.getSoldQuantity() + ticket.getQuantity() - ticketHit.getQuantity();
		}
		ticket.setSoldQuantity(soldQty);
		ticket.setQuantity(ticketHit.getQuantity());
		
		ticket.setRemainingQuantity(ticketHit.getQuantity() - ticketHit.getSoldQuantity());
		
		String section = cleanString(ticketHit.getSection());
		if (section != null) {
			section = section.replaceAll("[^\\w/-]", " ");
		}
		ticket.setSection(section.trim());
		
		String row = cleanString(ticketHit.getRow());
		if (row != null) {
			row = row.replaceAll("[^\\w/-]", " ");
		}
		ticket.setRow(row);
		ticket.setSeat(cleanString(ticketHit.getSeat()));					
		ticket.setCurrentPrice(ticketHit.getCurrentPrice());
		ticket.setBuyItNowPrice(ticketHit.getBuyItNowPrice());
		ticket.setEndDate(ticketHit.getEndDate());
		ticket.setLastUpdate(new Date());
		ticket.setLotSize(ticketHit.getLotSize());
		ticket.setNormalizedSection(SectionRowStripper.strip(tourType, section,synonymsMap));
		ticket.setInHand(ticketHit.isInHand());

		ticket.setTicketStatus(ticketHit.getTicketStatus());
		if ((ticket.getSeller() == null) && (ticketHit.getSeller() != null)) {
	//	if (ticketHit.getSeller() != null) {
			ticket.setSeller(ticketHit.getSeller());
		}

		if (ticketHit.getTicketListingCrawl() != null) {
			ticket.setTicketListingCrawlId(ticketHit.getTicketListingCrawl().getId());
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	private String cleanString(String text) {
		if (text == null) {
			return null;
		}
		
		return text.trim().replaceAll("\n", " ").replaceAll("\r", "");
	}

	protected TicketHit resolveTicketHitEventId(TicketListingCrawl ticketListingCrawl, TicketHit ticketHit) {
		Collection<TicketHit> ticketHits = new ArrayList<TicketHit>();
		ticketHits.add(ticketHit);
		Collection<TicketHit> resultTicketHits = resolveTicketHitEventId(ticketListingCrawl, ticketHits);
		if (resultTicketHits.size() == 0) {
			return null;
		}
		return resultTicketHits.iterator().next();
	}

	
	protected Collection<TicketHit> resolveTicketHitEventId(TicketListingCrawl ticketListingCrawl, Collection<TicketHit> ticketHits) {
		Collection<TicketHit> resultTicketHits = new ArrayList<TicketHit>();

		if (ticketListingCrawl.getEventId() != null) {
     			for(TicketHit ticketHit: ticketHits) {
          				ticketHit.setEventId(ticketListingCrawl.getEventId());
        				resultTicketHits.add(ticketHit);
			}
			return ticketHits;
		}

		// FIXME: use a hashmap with the date as the key to be faster
		/*Collection<Event> events = new ArrayList<Event>();
		if (ticketListingCrawl.getTourId() != null) {
			events = DAORegistry.getEventDAO().getAllEventsByTour(ticketListingCrawl.getTourId());
	    }*/
		Event eventDb = DAORegistry.getEventDAO().get(ticketListingCrawl.getEventId());
		for(TicketHit ticketHit: ticketHits) {
			if (ticketHit.getEventId() != null) {
				resultTicketHits.add(ticketHit);
				continue;
			}
	
			if (ticketHit.getEventDate() == null) {
				logger.info("Ticket has no date,skip it: " + ticketHit);
				continue;
			}
			
			
			
			// find the event based on the date
			Event event = null;
//			for (Event e:events) {
//				  TBD issue.. Need to discuss with business
				if (eventDb.getLocalDate() == null) {
					logger.info("Need to fix TBD Events: " + ticketHit);
					continue;
				}

				Calendar dbCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				dbCalendar.setTime(eventDb.getLocalDate());
				
				Calendar hitCalendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
				hitCalendar.setTime(ticketHit.getEventDate());
				
				if (dbCalendar.get(Calendar.DAY_OF_YEAR) == hitCalendar.get(Calendar.DAY_OF_YEAR)
						&& dbCalendar.get(Calendar.YEAR) == hitCalendar.get(Calendar.YEAR)) {
					if (event == null || dbCalendar.get(Calendar.HOUR_OF_DAY) == hitCalendar.get(Calendar.HOUR_OF_DAY)) {
						event = eventDb;
					}
				}
//			}
	
			if (event == null) {
				logger.info("NOMATCH - Skipped " + ticketHit);
				continue;
			}
			ticketHit.setEventId(event.getId());
			resultTicketHits.add(ticketHit);			
		}
		return resultTicketHits;
	}
}
