package com.admitone.tmat.pojo;

import java.io.Serializable;

import com.admitone.tmat.data.Event;

public class ExchangeEventAutoPricing implements Serializable {
	 private Integer id;
	 private Event event;
	 private Double lowerMarkup;
	 private Double upperMarkup;
	 private Double lowerShippingFees;
	 private Double upperShippingFees;
	 private String autoExposure;
	 private String miniExposure;
	 private String vipMiniExposure;
	 private String vipAutoExposure;
	 private Double rptFactor;
	 private Boolean isFrequentEvent;
	 private Double priceBreakup;
	 private String status;
	 private Integer shippingMethod;
	 private Integer nearTermDisplayOption;
	 private Boolean allowSectionRange;
	 private String lastRowMiniExposure;
	 private String lastFiveRowMiniExposure;
	 private String zonesPricingExposure;
	 private String zonedLastRowMiniExposure;
	 
	private Boolean isTicketNetworkEvent;
	private Boolean isVividSeatsEvent;
	private Boolean isTickPickEvent;
	private Boolean isEbayEvent;
	private Boolean isScoreBigEvent;
	private Boolean isLarryMiniTNEvent;
	private Boolean isStubhubEvent;
	private Boolean isExchangeFourEvent;
	private Boolean isExchangeFiveEvent;
	private Boolean isZonesPricingTNEvent;
	private Boolean isZonedLastRowMiniTNEvent;
	private Integer shippingDays;
	
	public void setAutoPricingValues(Double rptFactor,Double priceBreakup,
			Double lMarkup,Double uMarkup,Double lShippingFees,Double uShippingFees, Boolean isFrequent,Integer shippingMethod,Integer nearTermDisplayOption,
			Boolean allowSectionRange,Integer shippingDays) {
		
		this.shippingMethod = shippingMethod;
		this.nearTermDisplayOption = nearTermDisplayOption;
		this.allowSectionRange = allowSectionRange;
		this.rptFactor = rptFactor;
		this.priceBreakup = priceBreakup;
		this.lowerMarkup = lMarkup;
		this.upperMarkup = uMarkup;
		this.lowerShippingFees = lShippingFees;
		this.upperShippingFees = uShippingFees;
		this.isFrequentEvent = isFrequent;
		this.shippingDays=shippingDays;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	public Event getEvent() {
		return event;
	}
	public void setEvent(Event event) {
		this.event = event;
	}


	
	/*@Transient
	public int compareTo(TNTGCatsCategoryEvent obj) {
		return this.getEvent().getId()- obj.getEvent().getId();
	}*/
	
	
	
	public String getStatus() {
		return status;
	}
	public Boolean getIsFrequentEvent() {
		return isFrequentEvent;
	}
	public void setIsFrequentEvent(Boolean isFrequentEvent) {
		this.isFrequentEvent = isFrequentEvent;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Double getRptFactor() {
		return rptFactor;
	}
	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}
	
	
	public Boolean getIsTicketNetworkEvent() {
		return isTicketNetworkEvent;
	}
	public void setIsTicketNetworkEvent(Boolean isTicketNetworkEvent) {
		this.isTicketNetworkEvent = isTicketNetworkEvent;
	}
	public Boolean getIsVividSeatsEvent() {
		return isVividSeatsEvent;
	}
	public void setIsVividSeatsEvent(Boolean isVividSeatsEvent) {
		this.isVividSeatsEvent = isVividSeatsEvent;
	}
	public Boolean getIsTickPickEvent() {
		return isTickPickEvent;
	}
	public void setIsTickPickEvent(Boolean isTickPickEvent) {
		this.isTickPickEvent = isTickPickEvent;
	}
	public Double getLowerMarkup() {
		return lowerMarkup;
	}
	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}
	public Double getUpperMarkup() {
		return upperMarkup;
	}
	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}
	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}
	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}
	public Double getUpperShippingFees() {
		return upperShippingFees;
	}
	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}
	public Double getPriceBreakup() {
		return priceBreakup;
	}
	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}


	public Boolean getIsEbayEvent() {
		return isEbayEvent;
	}


	public void setIsEbayEvent(Boolean isEbayEvent) {
		this.isEbayEvent = isEbayEvent;
	}


	public Boolean getIsScoreBigEvent() {
		return isScoreBigEvent;
	}


	public void setIsScoreBigEvent(Boolean isScoreBigEvent) {
		this.isScoreBigEvent = isScoreBigEvent;
	}

	public Boolean getIsLarryMiniTNEvent() {
		return isLarryMiniTNEvent;
	}


	public void setIsLarryMiniTNEvent(Boolean isLarryMiniTNEvent) {
		this.isLarryMiniTNEvent = isLarryMiniTNEvent;
	}


	public Boolean getIsExchangeFourEvent() {
		return isExchangeFourEvent;
	}


	public void setIsExchangeFourEvent(Boolean isExchangeFourEvent) {
		this.isExchangeFourEvent = isExchangeFourEvent;
	}


	public Boolean getIsExchangeFiveEvent() {
		return isExchangeFiveEvent;
	}


	public void setIsExchangeFiveEvent(Boolean isExchangeFiveEvent) {
		this.isExchangeFiveEvent = isExchangeFiveEvent;
	}


	public String getAutoExposure() {
		return autoExposure;
	}


	public void setAutoExposure(String autoExposure) {
		this.autoExposure = autoExposure;
	}


	public String getMiniExposure() {
		return miniExposure;
	}


	public void setMiniExposure(String miniExposure) {
		this.miniExposure = miniExposure;
	}


	public String getVipMiniExposure() {
		return vipMiniExposure;
	}


	public void setVipMiniExposure(String vipMiniExposure) {
		this.vipMiniExposure = vipMiniExposure;
	}


	public String getVipAutoExposure() {
		return vipAutoExposure;
	}


	public void setVipAutoExposure(String vipAutoExposure) {
		this.vipAutoExposure = vipAutoExposure;
	}


	public Integer getShippingMethod() {
		return shippingMethod;
	}


	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}


	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}


	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}


	public Boolean getAllowSectionRange() {
		return allowSectionRange;
	}


	public void setAllowSectionRange(Boolean allowSectionRange) {
		this.allowSectionRange = allowSectionRange;
	}


	public String getLastRowMiniExposure() {
		return lastRowMiniExposure;
	}


	public void setLastRowMiniExposure(String lastRowMiniExposure) {
		this.lastRowMiniExposure = lastRowMiniExposure;
	}


	public String getLastFiveRowMiniExposure() {
		return lastFiveRowMiniExposure;
	}


	public void setLastFiveRowMiniExposure(String lastFiveRowMiniExposure) {
		this.lastFiveRowMiniExposure = lastFiveRowMiniExposure;
	}


	public Boolean getIsStubhubEvent() {
		return isStubhubEvent;
	}


	public void setIsStubhubEvent(Boolean isStubhubEvent) {
		this.isStubhubEvent = isStubhubEvent;
	}


	public String getZonesPricingExposure() {
		return zonesPricingExposure;
	}


	public void setZonesPricingExposure(String zonesPricingExposure) {
		this.zonesPricingExposure = zonesPricingExposure;
	}


	public Boolean getIsZonesPricingTNEvent() {
		return isZonesPricingTNEvent;
	}


	public void setIsZonesPricingTNEvent(Boolean isZonesPricingTNEvent) {
		this.isZonesPricingTNEvent = isZonesPricingTNEvent;
	}


	public String getZonedLastRowMiniExposure() {
		return zonedLastRowMiniExposure;
	}


	public void setZonedLastRowMiniExposure(String zonedLastRowMiniExposure) {
		this.zonedLastRowMiniExposure = zonedLastRowMiniExposure;
	}


	public Boolean getIsZonedLastRowMiniTNEvent() {
		return isZonedLastRowMiniTNEvent;
	}


	public void setIsZonedLastRowMiniTNEvent(Boolean isZonedLastRowMiniTNEvent) {
		this.isZonedLastRowMiniTNEvent = isZonedLastRowMiniTNEvent;
	}


	public Integer getShippingDays() {
		return shippingDays;
	}


	public void setShippingDays(Integer shippingDays) {
		this.shippingDays = shippingDays;
	}


}
