package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.List;

public class AOZoneTickets implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private Integer quantity;
	private Double broadcastPrice;
	private Integer marketPercentage;
	private Integer tixInZone;
	private Integer marketPerBelowAO;
	private Long daysOfExpiration;
	private Double totalMarket;
	private Double PGM;
	private Integer tixBelowAOZone;
	private Double marketPrice;
	private String date;
	private String time;
	private String section;
	private List<AOZoneTickets> children;
	private String eventName;
	private String eventDate;
	private int eventid;
	private Double lowESL;
	private boolean aoInfusedTix = false;
	private String category;
	private String venue;
	
	
	
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public int getEventid() {
		return eventid;
	}
	public void setEventid(int eventid) {
		this.eventid = eventid;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getBroadcastPrice() {
		return broadcastPrice;
	}
	public void setBroadcastPrice(Double broadcastPrice) {
		this.broadcastPrice = broadcastPrice;
	}
	public Integer getMarketPercentage() {
		return marketPercentage;
	}
	public void setMarketPercentage(Integer marketPercentage) {
		this.marketPercentage = marketPercentage;
	}
	public Integer getTixInZone() {
		return tixInZone;
	}
	public void setTixInZone(Integer tixInZone) {
		this.tixInZone = tixInZone;
	}
	public Integer getMarketPerBelowAO() {
		return marketPerBelowAO;
	}
	public void setMarketPerBelowAO(Integer marketPerBelowAO) {
		this.marketPerBelowAO = marketPerBelowAO;
	}
	public Long getDaysOfExpiration() {
		return daysOfExpiration;
	}
	public void setDaysOfExpiration(Long daysOfExpiration) {
		this.daysOfExpiration = daysOfExpiration;
	}
	public Double getTotalMarket() {
		return totalMarket;
	}
	public void setTotalMarket(Double totalMarket) {
		this.totalMarket = totalMarket;
	}
	public Double getPGM() {
		return PGM;
	}
	public void setPGM(Double pGM) {
		PGM = pGM;
	}
	public Integer getTixBelowAOZone() {
		return tixBelowAOZone;
	}
	public void setTixBelowAOZone(Integer tixBelowAOZone) {
		this.tixBelowAOZone = tixBelowAOZone;
	}
	public Double getMarketPrice() {
		return marketPrice;
	}
	public void setMarketPrice(Double marketPrice) {
		this.marketPrice = marketPrice;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public List<AOZoneTickets> getChildren() {
		return children;
	}
	public void setChildren(List<AOZoneTickets> children) {
		this.children = children;
	}
	public Double getLowESL() {
		return lowESL;
	}
	public boolean isAoInfusedTix() {
		return aoInfusedTix;
	}
	public void setAoInfusedTix(boolean aoInfusedTix) {
		this.aoInfusedTix = aoInfusedTix;
	}
	public void setLowESL(Double lowESL) {
		this.lowESL = lowESL;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	
	
}
