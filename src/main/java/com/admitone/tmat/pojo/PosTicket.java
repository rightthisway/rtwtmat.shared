package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.Date;


public class PosTicket implements Serializable {
	
	private Integer ticketId; 
	private Integer ticketGroupId; 
	private Integer invoiceId; 
	private Integer purchaseOrderId; 
	private Integer statusId; 
	private String row;
	private String section;
	private String originalSection;
	private Integer seatNumber;
	private Integer seatOrder;
	
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Double wholesalePrice;
	private Double actualSoldPrice;
	private String generalDesc;
	private Date expectedArrivalDate;
	private Integer systemUserId;
	private Integer modSystemUserId;
	private Integer ticketOnHandStausId;
	private Date createDate;
	private String internalNotes;
	private String seatRange;
	private Integer quantity;
	
	private Date purchaseOrderDate;
	
	public Integer getTicketId() {
		return ticketId;
	}
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Integer getStatusId() {
		return statusId;
	}
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getOriginalSection() {
		return originalSection;
	}
	public void setOriginalSection(String originalSection) {
		this.originalSection = originalSection;
	}
	public Integer getSeatNumber() {
		return seatNumber;
	}
	public void setSeatNumber(Integer seatNumber) {
		this.seatNumber = seatNumber;
	}
	public Integer getSeatOrder() {
		return seatOrder;
	}
	public void setSeatOrder(Integer seatOrder) {
		this.seatOrder = seatOrder;
	}
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public Double getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}
	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}
	public String getGeneralDesc() {
		return generalDesc;
	}
	public void setGeneralDesc(String generalDesc) {
		this.generalDesc = generalDesc;
	}
	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	public Integer getTicketOnHandStausId() {
		return ticketOnHandStausId;
	}
	public void setTicketOnHandStausId(Integer ticketOnHandStausId) {
		this.ticketOnHandStausId = ticketOnHandStausId;
	}
	public Integer getModSystemUserId() {
		return modSystemUserId;
	}
	public void setModSystemUserId(Integer modSystemUserId) {
		this.modSystemUserId = modSystemUserId;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}
	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}
	public String getSeatRange() {
		return seatRange;
	}
	public void setSeatRange(String seatRange) {
		this.seatRange = seatRange;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	
}
