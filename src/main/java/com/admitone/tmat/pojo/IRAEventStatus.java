package com.admitone.tmat.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.data.Event;

public class IRAEventStatus {
	private Map<Long, String> eventStringMap = new HashMap<Long, String>();
	private Map<Long, List<Event>> eventMap = new HashMap<Long, List<Event>>();
	
	public Map<Long, String> getEventStringMap() {
		return eventStringMap;
	}

	public void setEventStringMap(Map<Long, String> eventStringMap) {
		this.eventStringMap = eventStringMap;
	}

	public Map<Long, List<Event>> getEventMap() {
		return eventMap;
	}

	public void setEventMap(Map<Long, List<Event>> eventMap) {
		this.eventMap = eventMap;
	}	
	
	public List<Event> getEventFromMapById(Long id){
		return eventMap.remove(id);
	}
}
