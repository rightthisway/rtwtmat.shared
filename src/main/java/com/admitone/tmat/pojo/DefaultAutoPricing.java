package com.admitone.tmat.pojo;


public class DefaultAutoPricing {
//	 private Integer id;
	 private Double lowerMarkup;
	 private Double upperMarkup;
	 private Double lowerShippingFees;
	 private Double upperShippingFees;
	 private String autoExposure;
	 private String miniExposure;
	 private String vipMiniExposure;
	 private String vipAutoExposure;
	 private Double rptFactor;
	 private Double priceBreakup;
	 private Integer shippingMethod;
	 private Integer nearTermDisplayOption;
	 private String lastRowMiniExposure;
	 private String lastFiveRowMiniExposure;
	 private String ZonesPricingExposure;
	 private String ZonedLastRowMiniExposure;
	
	
	public void setAutoPricingValues(Double rptFactor,Double priceBreakup,
			Double lMarkup,Double uMarkup,Double lShippingFees,Double uShippingFees, Boolean isFrequent,
			Integer shippingMethod,Integer nearTermDisplayOption) {
		
		this.rptFactor = rptFactor;
		this.priceBreakup = priceBreakup;
		this.lowerMarkup = lMarkup;
		this.upperMarkup = uMarkup;
		this.lowerShippingFees = lShippingFees;
		this.upperShippingFees = uShippingFees;
		this.shippingMethod = shippingMethod;
		this.nearTermDisplayOption = nearTermDisplayOption;
	}
	
	
//	public Integer getId() {
//		return id;
//	}
//	public void setId(Integer id) {
//		this.id = id;
//	}


	public Double getLowerMarkup() {
		return lowerMarkup;
	}


	public void setLowerMarkup(Double lowerMarkup) {
		this.lowerMarkup = lowerMarkup;
	}


	public Double getUpperMarkup() {
		return upperMarkup;
	}


	public void setUpperMarkup(Double upperMarkup) {
		this.upperMarkup = upperMarkup;
	}


	public Double getLowerShippingFees() {
		return lowerShippingFees;
	}


	public void setLowerShippingFees(Double lowerShippingFees) {
		this.lowerShippingFees = lowerShippingFees;
	}


	public Double getUpperShippingFees() {
		return upperShippingFees;
	}


	public void setUpperShippingFees(Double upperShippingFees) {
		this.upperShippingFees = upperShippingFees;
	}


	/*public String getExposure() {
		return exposure;
	}


	public void setExposure(String exposure) {
		this.exposure = exposure;
	}*/


	public Double getRptFactor() {
		return rptFactor;
	}


	public void setRptFactor(Double rptFactor) {
		this.rptFactor = rptFactor;
	}


	public Double getPriceBreakup() {
		return priceBreakup;
	}


	public void setPriceBreakup(Double priceBreakup) {
		this.priceBreakup = priceBreakup;
	}


	public String getAutoExposure() {
		return autoExposure;
	}


	public void setAutoExposure(String autoExposure) {
		this.autoExposure = autoExposure;
	}


	public String getMiniExposure() {
		return miniExposure;
	}


	public void setMiniExposure(String miniExposure) {
		this.miniExposure = miniExposure;
	}


	public String getVipMiniExposure() {
		return vipMiniExposure;
	}


	public void setVipMiniExposure(String vipMiniExposure) {
		this.vipMiniExposure = vipMiniExposure;
	}


	public String getVipAutoExposure() {
		return vipAutoExposure;
	}


	public void setVipAutoExposure(String vipAutoExposure) {
		this.vipAutoExposure = vipAutoExposure;
	}


	public Integer getShippingMethod() {
		return shippingMethod;
	}


	public void setShippingMethod(Integer shippingMethod) {
		this.shippingMethod = shippingMethod;
	}


	public Integer getNearTermDisplayOption() {
		return nearTermDisplayOption;
	}


	public void setNearTermDisplayOption(Integer nearTermDisplayOption) {
		this.nearTermDisplayOption = nearTermDisplayOption;
	}


	public String getLastRowMiniExposure() {
		return lastRowMiniExposure;
	}


	public void setLastRowMiniExposure(String lastRowMiniExposure) {
		this.lastRowMiniExposure = lastRowMiniExposure;
	}


	public String getLastFiveRowMiniExposure() {
		return lastFiveRowMiniExposure;
	}


	public void setLastFiveRowMiniExposure(String lastFiveRowMiniExposure) {
		this.lastFiveRowMiniExposure = lastFiveRowMiniExposure;
	}


	public String getZonesPricingExposure() {
		return ZonesPricingExposure;
	}


	public void setZonesPricingExposure(String zonesPricingExposure) {
		ZonesPricingExposure = zonesPricingExposure;
	}


	public String getZonedLastRowMiniExposure() {
		return ZonedLastRowMiniExposure;
	}


	public void setZonedLastRowMiniExposure(String zonedLastRowMiniExposure) {
		ZonedLastRowMiniExposure = zonedLastRowMiniExposure;
	}
	
		
}
