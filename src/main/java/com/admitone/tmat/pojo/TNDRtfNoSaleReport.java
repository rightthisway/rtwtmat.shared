package com.admitone.tmat.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;


public class TNDRtfNoSaleReport {
	
	private Integer eventId;
	private String eventName;
	private Date eventDate;
	private String eventDateStr;
	private String eventTimeStr;
	private String venue;
	private String city;
	private String state;
	private String country;
	private Integer tnSalesCount;
	private Integer tixCitySalesCount;
	private Integer rtfTicketsCount;
	private Integer tixcityTicketsCount;
	private String tmatEvent;
	private String ticketNetworkDirectCrawl;
	private String ticketEvolutionCrawl;
	private String stubhubCrawl;
	private String flashSeatsCrawl;
	private String vividSeatsCrawl;
	private String csvUploaded;
	private String mapUploaded;
	private String autocats96Enabled;
	private String larryLastEnabled;
	private String zoneLastRowEnabled;
	private String rtfListingEnabled;
	
	
	
	public static SimpleDateFormat dtFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	public static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
	
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		if(eventDateStr == null) {
			if(eventDate != null) {
				eventDateStr = dateFormat.format(eventDate);
			} else {
				return "TBD";
			}
		}
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		if(eventTimeStr == null) {
			if(eventDate != null) {
				eventTimeStr = timeFormat.format(eventDate);
			} else {
				return "TBD";
			}
		}
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	
	public Integer getTnSalesCount() {
		return tnSalesCount;
	}
	public void setTnSalesCount(Integer tnSalesCount) {
		this.tnSalesCount = tnSalesCount;
	}
	public Integer getTixCitySalesCount() {
		return tixCitySalesCount;
	}
	public void setTixCitySalesCount(Integer tixCitySalesCount) {
		this.tixCitySalesCount = tixCitySalesCount;
	}
	public Integer getRtfTicketsCount() {
		return rtfTicketsCount;
	}
	public void setRtfTicketsCount(Integer rtfTicketsCount) {
		this.rtfTicketsCount = rtfTicketsCount;
	}
	public Integer getTixcityTicketsCount() {
		return tixcityTicketsCount;
	}
	public void setTixcityTicketsCount(Integer tixcityTicketsCount) {
		this.tixcityTicketsCount = tixcityTicketsCount;
	}
	public String getTmatEvent() {
		return tmatEvent;
	}
	public void setTmatEvent(String tmatEvent) {
		this.tmatEvent = tmatEvent;
	}
	public String getTicketNetworkDirectCrawl() {
		return ticketNetworkDirectCrawl;
	}
	public void setTicketNetworkDirectCrawl(String ticketNetworkDirectCrawl) {
		this.ticketNetworkDirectCrawl = ticketNetworkDirectCrawl;
	}
	public String getTicketEvolutionCrawl() {
		return ticketEvolutionCrawl;
	}
	public void setTicketEvolutionCrawl(String ticketEvolutionCrawl) {
		this.ticketEvolutionCrawl = ticketEvolutionCrawl;
	}
	public String getStubhubCrawl() {
		return stubhubCrawl;
	}
	public void setStubhubCrawl(String stubhubCrawl) {
		this.stubhubCrawl = stubhubCrawl;
	}
	public String getFlashSeatsCrawl() {
		return flashSeatsCrawl;
	}
	public void setFlashSeatsCrawl(String flashSeatsCrawl) {
		this.flashSeatsCrawl = flashSeatsCrawl;
	}
	public String getVividSeatsCrawl() {
		return vividSeatsCrawl;
	}
	public void setVividSeatsCrawl(String vividSeatsCrawl) {
		this.vividSeatsCrawl = vividSeatsCrawl;
	}
	public String getCsvUploaded() {
		return csvUploaded;
	}
	public void setCsvUploaded(String csvUploaded) {
		this.csvUploaded = csvUploaded;
	}
	public String getMapUploaded() {
		return mapUploaded;
	}
	public void setMapUploaded(String mapUploaded) {
		this.mapUploaded = mapUploaded;
	}
	public String getAutocats96Enabled() {
		return autocats96Enabled;
	}
	public void setAutocats96Enabled(String autocats96Enabled) {
		this.autocats96Enabled = autocats96Enabled;
	}
	public String getLarryLastEnabled() {
		return larryLastEnabled;
	}
	public void setLarryLastEnabled(String larryLastEnabled) {
		this.larryLastEnabled = larryLastEnabled;
	}
	public String getZoneLastRowEnabled() {
		return zoneLastRowEnabled;
	}
	public void setZoneLastRowEnabled(String zoneLastRowEnabled) {
		this.zoneLastRowEnabled = zoneLastRowEnabled;
	}
	public String getRtfListingEnabled() {
		return rtfListingEnabled;
	}
	public void setRtfListingEnabled(String rtfListingEnabled) {
		this.rtfListingEnabled = rtfListingEnabled;
	}
	
	
	
}
