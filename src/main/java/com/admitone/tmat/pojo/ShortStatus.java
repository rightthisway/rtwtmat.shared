package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.data.Site;

// FIXME: rename this to ShortOrLongStatus
// it's a short if longTransaction = false 
public class ShortStatus implements Serializable {
	//private double grossMargin;
	//private int tourId;

	private int eventId;
	private int shortAlertId;
	private String category;
	private int categoryId = 0;
	private String description;
	private String venue;
	private String date;
	private String time;
	private String alertMinPrice;
	private String alertMaxPrice;

	private String section;
	private String normalizedSection;
	private String row;
	private String normalizedRow;
	
	// section/row of the lowest ticket price in TMAT matching the category and quantity of
	// this short/long.
	private String ticketSection;
	private String ticketRow;
	
	private static final long serialVersionUID = 1L;
	private static Map<String, String> siteToColor ;
	  
	static {
		Map<String, String> map = new HashMap<String, String>();
		map.put(Site.STUB_HUB, "#ffcc66");
		map.put(Site.TICKETS_NOW, "#6699cc");
		map.put(Site.RAZOR_GATOR, "#6699cc");
		siteToColor = Collections.unmodifiableMap(map);
	}

	private int transactionId;
	private int qtySold;
	private double projGrossMargin;
	private double priceSold;
	private double minPrice;
	private double totalMinPrice;
	private double exposure;
	
	private String customer;
	private Integer ticketId;
	private String siteId;
	private String siteColor;
	private Integer invoice;
	
	private boolean longTransaction = false;
	
	private double marketMover;
	private double totalMarketMover;
	
	//This is to identify if a Cat/Event level ShortStatus is long short or Neutral.
	private Integer longShortNeutral;
	private double priceAtDate90;
	private double priceAtDate60;
	private double priceAtDate30;
	private double priceAtDate14;
	private double priceAtDate7;
	private double priceAtDate4;
	private double priceAtDate1;
	private double sellingPrice;
	private Date invoiceDate;
	private Date poDate;
	private Integer DataPointDiff;
	private double buyingPrice;
	
	/* If this is an aggregate Status, this contains the
	 * statuses that make up this status */
	private List<ShortStatus> children; 
	
	//for serialization
	public ShortStatus() { }
		
	public int getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(int transactionId) {
		this.transactionId = transactionId;
	}
	public int getQtySold() {
		return qtySold;
	}
	public void setQtySold(int qtySold) {
		this.qtySold = qtySold;
	}

	public double getProjGrossMargin() {
		return projGrossMargin;
	}
	
	public void setProjGrossMargin(double projGrossMargin) {
		this.projGrossMargin = projGrossMargin;
	}

	public double getPriceSold() {
		return priceSold;
	}
	
	public void setPriceSold(double priceSold) {
		this.priceSold = priceSold;
	}
	
	public double getExposure() {
		return exposure;
	}
	
	public void setExposure(double exposure) {
		this.exposure = exposure;
	}
	
	public double getMinPrice() {
		return minPrice;
	}
	
	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	public double getTotalMinPrice() {
		return totalMinPrice;
	}
	
	public void setTotalMinPrice(double totalMinPrice) {
		this.totalMinPrice = totalMinPrice;
	}

	public List<ShortStatus> getChildren() {
		return children;
	}
	
	public void setChildren(List<ShortStatus> children) {
		this.children = children;
	}
	
	public Integer getTicketId() {
		return ticketId;
	}
	
	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}
	
	public String getSiteId() {
		return siteId;
	}
	
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	
	public Integer getInvoice() {
		return invoice;
	}
	
	public void setInvoice(Integer invoice) {
		this.invoice = invoice;
	}

	public String getSiteColor() {
		return siteColor;
	}
	public void setSiteColor(String siteColor) {
		this.siteColor = siteColor;
	}
	
	public String getCustomer() {
		return customer;
	}
	
	public void setCustomer(String customer) {
		this.customer = customer;
		String color = null;

		if (customer != null) {
			for(String site : siteToColor.keySet()){
				if(customer.toLowerCase().contains(site)) {
					color = siteToColor.get(site);
				}
			}
		}
		if(color == null) {
			color = "gray";
		}
		this.setSiteColor(color);
	}

	public boolean getLongTransaction() {
		return longTransaction;
	}
	
	public void setLongTransaction(boolean longTransaction) {
		this.longTransaction= longTransaction;
	}

	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public int getCategoryId() {
		return categoryId;
	}
	
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}
	
	public int getEventId() {
		return eventId;
	}
	
	public void setEventId(int eventId) {
		this.eventId = eventId;
	}
	
	public String getDate() {
		return date;
	}
	
	public void setDate(String date) {
		this.date = date;
	}
	
	public String getSection() {
		return section;
	}
	
	public void setSection(String section) {
		this.section = section;
	}
	
	public String getRow() {
		return row;
	}
	
	public void setRow(String row) {
		this.row = row;
	}

	public String getTicketSection() {
		return ticketSection;
	}

	public void setTicketSection(String ticketSection) {
		this.ticketSection = ticketSection;
	}

	public String getTicketRow() {
		return ticketRow;
	}

	public void setTicketRow(String ticketRow) {
		this.ticketRow = ticketRow;
	}
	
	
	
	public String toString() {
	String returnString = "ShortStatus["
			+ " qtySold: " + qtySold
			+ " projGrossMargin: " + projGrossMargin
			+ " exposure: " + exposure
			+ " description: " + getDescription()
			+ " eventId: " + getEventId()
			+ " date: " + getDate()
			+ "children[ \n";
			if(children != null){
				for(ShortStatus child : children){
					returnString += child + ", \n";
				}
			}
			returnString += "]";
	
	return returnString;
	}

	public String getNormalizedSection() {
		return normalizedSection;
	}

	public void setNormalizedSection(String normalizedSection) {
		this.normalizedSection = normalizedSection;
	}

	public String getNormalizedRow() {
		return normalizedRow;
	}

	public void setNormalizedRow(String normalizedRow) {
		this.normalizedRow = normalizedRow;
	}
	
	//According to the formula provide the marketMover value is 75% of the minPrice(Market Price) 
    public double getMarketMover() {
    	if(this.getChildren() == null || this.getChildren().isEmpty()){
	    	if(this.longTransaction){
	    		return this.minPrice*0.75D;
	    	}else{
	    		return this.priceSold*0.75D;
	    	}
    	}else{
    		if(this.getLongShortNeutral() == 1){
    			return this.minPrice*0.75D;
    		}else if(this.getLongShortNeutral() == -1){
    			return this.priceSold*0.75D;
    		}else{
    			return 0.0D;
    		}
    	}
	}

	public void setMarketMover(double marketMover) {
		this.marketMover = marketMover;
	}

	public Integer getLongShortNeutral() {
		return longShortNeutral;
	}

	public void setLongShortNeutral(Integer longShortNeutral) {
		this.longShortNeutral = longShortNeutral;
	}

	public double getPriceAtDate90() {
		return priceAtDate90;
	}

	public void setPriceAtDate90(double priceAtDate90) {
		this.priceAtDate90 = priceAtDate90;
	}

	public double getPriceAtDate60() {
		return priceAtDate60;
	}

	public void setPriceAtDate60(double priceAtDate60) {
		this.priceAtDate60 = priceAtDate60;
	}

	public double getPriceAtDate30() {
		return priceAtDate30;
	}

	public void setPriceAtDate30(double priceAtDate30) {
		this.priceAtDate30 = priceAtDate30;
	}

	public double getPriceAtDate14() {
		return priceAtDate14;
	}

	public void setPriceAtDate14(double priceAtDate14) {
		this.priceAtDate14 = priceAtDate14;
	}

	public double getPriceAtDate7() {
		return priceAtDate7;
	}

	public void setPriceAtDate7(double priceAtDate7) {
		this.priceAtDate7 = priceAtDate7;
	}

	public double getPriceAtDate4() {
		return priceAtDate4;
	}

	public void setPriceAtDate4(double priceAtDate4) {
		this.priceAtDate4 = priceAtDate4;
	}

	public double getPriceAtDate1() {
		return priceAtDate1;
	}

	public void setPriceAtDate1(double priceAtDate1) {
		this.priceAtDate1 = priceAtDate1;
	}
	
	public Date getInvoiceDate() {
		return invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public Date getPoDate() {
		return poDate;
	}

	public void setPoDate(Date poDate) {
		this.poDate = poDate;
	}

	public double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}
	
	public Integer getDataPointDiff() {
		return DataPointDiff;
	}

	public void setDataPointDiff(Integer dataPointDiff) {
		DataPointDiff = dataPointDiff;
	}

	public double getBuyingPrice() {
		return buyingPrice;
	}

	public void setBuyingPrice(double buyingPrice) {
		this.buyingPrice = buyingPrice;
	}
	
	public double getTotalMarketMover() {
		return totalMarketMover;
	}

	public void setTotalMarketMover(double totalMarketMover) {
		this.totalMarketMover = totalMarketMover;
	}

	public int getShortAlertId() {
		return shortAlertId;
	}

	public void setShortAlertId(int shortAlertId) {
		this.shortAlertId = shortAlertId;
	}

	

	public String getAlertMinPrice() {
		return alertMinPrice;
	}

	public void setAlertMinPrice(String alertMinPrice) {
		this.alertMinPrice = alertMinPrice;
	}

	public String getAlertMaxPrice() {
		return alertMaxPrice;
	}

	public void setAlertMaxPrice(String alertMaxPrice) {
		this.alertMaxPrice = alertMaxPrice;
	}
	
	/*
	public int getTourId() {
		return tourId;
	}
	
	public void setTourId(int tourId) {
		this.tourId = tourId;
	}

	public double getGrossMargin() {
		return grossMargin;
	}
	
	public void setGrossMargin(double grossMargin) {
		this.grossMargin = grossMargin;
	}
	*/
	
	
}
