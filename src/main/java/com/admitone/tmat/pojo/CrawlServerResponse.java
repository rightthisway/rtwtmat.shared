package com.admitone.tmat.pojo;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("CrawlServerResponse")
public class CrawlServerResponse {
	private String status;
	private String msg;
	private Integer numberofcrawls;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Integer getNumberofcrawls() {
		return numberofcrawls;
	}
	public void setNumberofcrawls(Integer numberofcrawls) {
		this.numberofcrawls = numberofcrawls;
	}
	
}
