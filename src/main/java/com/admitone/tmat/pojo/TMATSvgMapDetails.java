package com.admitone.tmat.pojo;

import java.util.Collection;


public class TMATSvgMapDetails {
	private String svgText;
	private String svgMapPath;
	private boolean isMapWithSvg;
	Collection<ZoneRGBColor> zoneRGBColors;
	
	private Error error;
	private Integer status;
	
	public String getSvgText() {
		return svgText;
	}
	public void setSvgText(String svgText) {
		this.svgText = svgText;
	}
	public String getSvgMapPath() {
		return svgMapPath;
	}
	public void setSvgMapPath(String svgMapPath) {
		this.svgMapPath = svgMapPath;
	}
	public boolean isMapWithSvg() {
		return isMapWithSvg;
	}
	public void setMapWithSvg(boolean isMapWithSvg) {
		this.isMapWithSvg = isMapWithSvg;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Collection<ZoneRGBColor> getZoneRGBColors() {
		return zoneRGBColors;
	}
	public void setZoneRGBColors(Collection<ZoneRGBColor> zoneRGBColors) {
		this.zoneRGBColors = zoneRGBColors;
	}
	
	
}
