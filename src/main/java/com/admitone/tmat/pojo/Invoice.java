package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.Date;


public class Invoice implements Serializable {
	
	  private Integer invoiceId;
	  private Integer clientId;
	  private String brokerType;
	  private Double invoiceTotal;
	  private Double invoiceTotalDue;
	  private Double invoiceTotalExpense;
	  private Double invoiceTotalShippingCost;
	  private Double invoiceTotalTaxes;
	  private Double invoiceBalanceDue;
	  private Date invoiceDueDate;
	  private String invoiceDateStr;
	  private Integer systemUserId;
	  private String salesPersonName;
	  private Integer shippingTrackingId;
	  private Integer dropShipped;
	  private String notes;
	  private String externalPO;
	  private Integer invoiceStatusId;
	  private String displayedNotes;
	  private Date createDate;
	  private Date invoiceVoidDate;
	  private Integer ticketRequestId;
	  private Integer reprint;
	  private Integer invoiceTypeId;
	  private Integer isDwon;
	   private Date isDwonUpdateTime;
	   private Integer sentIn;
	   private Date sentInUpdateTime;
	 private Integer isEmailed;
	 private String emaiInfo;
	  private Integer isWillCallEmailed;
	  private Integer isAutoProcessed;
	  private Integer transOfficeId;
	  private Integer userOfficeId;
	  private Integer mztixInvoiceId;
	  private String systemUserFirstName;
	  private String systemUserLastName;
	  
	  
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Double getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public Double getInvoiceTotalDue() {
		return invoiceTotalDue;
	}
	public void setInvoiceTotalDue(Double invoiceTotalDue) {
		this.invoiceTotalDue = invoiceTotalDue;
	}
	public Double getInvoiceTotalExpense() {
		return invoiceTotalExpense;
	}
	public void setInvoiceTotalExpense(Double invoiceTotalExpense) {
		this.invoiceTotalExpense = invoiceTotalExpense;
	}
	public Double getInvoiceTotalShippingCost() {
		return invoiceTotalShippingCost;
	}
	public void setInvoiceTotalShippingCost(Double invoiceTotalShippingCost) {
		this.invoiceTotalShippingCost = invoiceTotalShippingCost;
	}
	public Double getInvoiceTotalTaxes() {
		return invoiceTotalTaxes;
	}
	public void setInvoiceTotalTaxes(Double invoiceTotalTaxes) {
		this.invoiceTotalTaxes = invoiceTotalTaxes;
	}
	public Double getInvoiceBalanceDue() {
		return invoiceBalanceDue;
	}
	public void setInvoiceBalanceDue(Double invoiceBalanceDue) {
		this.invoiceBalanceDue = invoiceBalanceDue;
	}
	public Date getInvoiceDueDate() {
		return invoiceDueDate;
	}
	public void setInvoiceDueDate(Date invoiceDueDate) {
		this.invoiceDueDate = invoiceDueDate;
	}
	public String getInvoiceDateStr() {
		return invoiceDateStr;
	}
	public void setInvoiceDateStr(String invoiceDateStr) {
		this.invoiceDateStr = invoiceDateStr;
	}
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	public Integer getShippingTrackingId() {
		return shippingTrackingId;
	}
	public void setShippingTrackingId(Integer shippingTrackingId) {
		this.shippingTrackingId = shippingTrackingId;
	}
	public Integer getDropShipped() {
		return dropShipped;
	}
	public void setDropShipped(Integer dropShipped) {
		this.dropShipped = dropShipped;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getExternalPO() {
		return externalPO;
	}
	public void setExternalPO(String externalPO) {
		this.externalPO = externalPO;
	}
	public Integer getInvoiceStatusId() {
		return invoiceStatusId;
	}
	public void setInvoiceStatusId(Integer invoiceStatusId) {
		this.invoiceStatusId = invoiceStatusId;
	}
	public String getDisplayedNotes() {
		return displayedNotes;
	}
	public void setDisplayedNotes(String displayedNotes) {
		this.displayedNotes = displayedNotes;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	public Date getInvoiceVoidDate() {
		return invoiceVoidDate;
	}
	public void setInvoiceVoidDate(Date invoiceVoidDate) {
		this.invoiceVoidDate = invoiceVoidDate;
	}
	public Integer getTicketRequestId() {
		return ticketRequestId;
	}
	public void setTicketRequestId(Integer ticketRequestId) {
		this.ticketRequestId = ticketRequestId;
	}
	public Integer getReprint() {
		return reprint;
	}
	public void setReprint(Integer reprint) {
		this.reprint = reprint;
	}
	public Integer getInvoiceTypeId() {
		return invoiceTypeId;
	}
	public void setInvoiceTypeId(Integer invoiceTypeId) {
		this.invoiceTypeId = invoiceTypeId;
	}
	public Integer getIsDwon() {
		return isDwon;
	}
	public void setIsDwon(Integer isDwon) {
		this.isDwon = isDwon;
	}
	public Date getIsDwonUpdateTime() {
		return isDwonUpdateTime;
	}
	public void setIsDwonUpdateTime(Date isDwonUpdateTime) {
		this.isDwonUpdateTime = isDwonUpdateTime;
	}
	public Integer getSentIn() {
		return sentIn;
	}
	public void setSentIn(Integer sentIn) {
		this.sentIn = sentIn;
	}
	public Date getSentInUpdateTime() {
		return sentInUpdateTime;
	}
	public void setSentInUpdateTime(Date sentInUpdateTime) {
		this.sentInUpdateTime = sentInUpdateTime;
	}
	public Integer getIsEmailed() {
		return isEmailed;
	}
	public void setIsEmailed(Integer isEmailed) {
		this.isEmailed = isEmailed;
	}
	public String getEmaiInfo() {
		return emaiInfo;
	}
	public void setEmaiInfo(String emaiInfo) {
		this.emaiInfo = emaiInfo;
	}
	public Integer getIsWillCallEmailed() {
		return isWillCallEmailed;
	}
	public void setIsWillCallEmailed(Integer isWillCallEmailed) {
		this.isWillCallEmailed = isWillCallEmailed;
	}
	public Integer getIsAutoProcessed() {
		return isAutoProcessed;
	}
	public void setIsAutoProcessed(Integer isAutoProcessed) {
		this.isAutoProcessed = isAutoProcessed;
	}
	public Integer getTransOfficeId() {
		return transOfficeId;
	}
	public void setTransOfficeId(Integer transOfficeId) {
		this.transOfficeId = transOfficeId;
	}
	public Integer getUserOfficeId() {
		return userOfficeId;
	}
	public void setUserOfficeId(Integer userOfficeId) {
		this.userOfficeId = userOfficeId;
	}
	public Integer getMztixInvoiceId() {
		return mztixInvoiceId;
	}
	public void setMztixInvoiceId(Integer mztixInvoiceId) {
		this.mztixInvoiceId = mztixInvoiceId;
	}
	public String getSalesPersonName() {
		return salesPersonName;
	}
	public void setSalesPersonName(String salesPersonName) {
		this.salesPersonName = salesPersonName;
	}
	public Integer getClientId() {
		return clientId;
	}
	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}
	public String getBrokerType() {
		return brokerType;
	}
	public void setBrokerType(String brokerType) {
		this.brokerType = brokerType;
	}
	public String getSystemUserFirstName() {
		return systemUserFirstName;
	}
	public void setSystemUserFirstName(String systemUserFirstName) {
		this.systemUserFirstName = systemUserFirstName;
	}
	public String getSystemUserLastName() {
		return systemUserLastName;
	}
	public void setSystemUserLastName(String systemUserLastName) {
		this.systemUserLastName = systemUserLastName;
	}
	
	  
	  
	  
}
