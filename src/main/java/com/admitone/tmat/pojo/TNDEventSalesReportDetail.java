package com.admitone.tmat.pojo;

import java.text.SimpleDateFormat;
import java.util.Date;


public class TNDEventSalesReportDetail {
	
	private Integer eventId;
	private String eventName;
	private Date eventDate;
	private Date eventTime;
	private String eventDateStr;
	private String eventTimeStr;
	private String venue;
	private String city;
	private String state;
	private String country;
	private Integer salesCount;
	private String recentSaleDateStr;
	private Date recentSaleDate;
	private String tmatEvent;
	private String ticketNetworkDirectCrawl;
	private String ticketEvolutionCrawl;
	private String stubhubCrawl;
	private String csvUploaded;
	private Date tnEventCreatedDate;
	private String tnEventCreatedDateStr;
	
	
	
	public static SimpleDateFormat dtFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	public static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
	
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDateStr() {
		if(eventDateStr == null) {
			if(eventDate != null) {
				eventDateStr = dateFormat.format(eventDate);
			} else {
				return "TBD";
			}
		}
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		if(eventTimeStr == null) {
			if(eventTime != null) {
				eventTimeStr = timeFormat.format(eventTime);
			} else {
				return "TBD";
			}
		}
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getSalesCount() {
		return salesCount;
	}
	public void setSalesCount(Integer salesCount) {
		this.salesCount = salesCount;
	}
	public String getRecentSaleDateStr() {
		if(recentSaleDateStr == null) {
			if(recentSaleDate != null) {
				recentSaleDateStr = dtFormat.format(recentSaleDate);
			}
		}
		return recentSaleDateStr;
	}
	public void setRecentSaleDateStr(String recentSaleDateStr) {
		this.recentSaleDateStr = recentSaleDateStr;
	}
	public Date getRecentSaleDate() {
		return recentSaleDate;
	}
	public void setRecentSaleDate(Date recentSaleDate) {
		this.recentSaleDate = recentSaleDate;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public String getTmatEvent() {
		return tmatEvent;
	}
	public void setTmatEvent(String tmatEvent) {
		this.tmatEvent = tmatEvent;
	}
	public String getTicketNetworkDirectCrawl() {
		return ticketNetworkDirectCrawl;
	}
	public void setTicketNetworkDirectCrawl(String ticketNetworkDirectCrawl) {
		this.ticketNetworkDirectCrawl = ticketNetworkDirectCrawl;
	}
	public String getTicketEvolutionCrawl() {
		return ticketEvolutionCrawl;
	}
	public void setTicketEvolutionCrawl(String ticketEvolutionCrawl) {
		this.ticketEvolutionCrawl = ticketEvolutionCrawl;
	}
	public String getStubhubCrawl() {
		return stubhubCrawl;
	}
	public void setStubhubCrawl(String stubhubCrawl) {
		this.stubhubCrawl = stubhubCrawl;
	}
	public String getCsvUploaded() {
		return csvUploaded;
	}
	public void setCsvUploaded(String csvUploaded) {
		this.csvUploaded = csvUploaded;
	}
	public Date getTnEventCreatedDate() {
		return tnEventCreatedDate;
	}
	public void setTnEventCreatedDate(Date tnEventCreatedDate) {
		this.tnEventCreatedDate = tnEventCreatedDate;
	}
	public String getTnEventCreatedDateStr() {
		if(tnEventCreatedDateStr == null){
			if(tnEventCreatedDate != null) {
				tnEventCreatedDateStr = dtFormat.format(tnEventCreatedDate);
			}
		}
		return tnEventCreatedDateStr;
	}
	public void setTnEventCreatedDateStr(String tnEventCreatedDateStr) {
		this.tnEventCreatedDateStr = tnEventCreatedDateStr;
	}
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	
	
}
