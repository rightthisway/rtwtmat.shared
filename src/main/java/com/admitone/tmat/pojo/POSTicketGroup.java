package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.Date;

public class POSTicketGroup implements Serializable {

	private Integer invoiceId;
	private Integer ticketGroupId;
	private Integer ticketId;
	private Integer ticketGroupExposureId;
	private Date arrivedOnDate;
	private String arrivedOnDateStr;
	private Integer originalTicketCount;
	private Integer remainingTicketCount;
	private Integer mzTixTicketGroupId;
	private Integer mzTixQty;
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Double wholesalePrice;
	private Double actualWholeSalePrice;
	private Double actualSoldPrice;
	private String row;
	private String section;
	private String notes;
	private Date createdDate;
	private Integer systemUserId;
	private Integer eventId;
	private Integer statusId;
	private Integer ticketGroupSeatingTypeId;
	private Integer ticketGroupTypeId;
	private Integer clientBrokerId;
	private Integer clientBrokerEmployeeId;
	private String internalNotes;
	private Date actualPurchaseDate;
	private Integer autoProcessWebRequests;
	private Boolean taxExempt;
	private Boolean ebayAuction;
	private String ebayAuctionNumber;
	private Date updateDateTime;
	private String notOnHandMessage;
	private Integer showTndNonTnd;
	private Integer officeId;
	private Boolean isDropSale;
	private Integer exchangeEventId;
	private Integer exchangeTicketGroupId;
	private Integer unbroadcastDaysBeforeEvent;
	private Integer ticketGroupStockTypeId;
	private Integer shippingMethodSpecialId;
	private Integer showNearTermOptionId;

	private Integer tgNoteGrandFathered;
	private Date priceUpdateDatetime;
	private Boolean eticketInstantDownload;
	private Boolean eticketStoredLocally;
	private Boolean overrideInstantDownloadPause;
	
	private String commissionPerc;
	private String eventName;
	private String eventDateStr;
	private String simpleEventDateStr;
	private Date eventDate;
	private String venueName;
	private Integer seatLow;
	private Integer seatHigh;
	private Integer qty;
	private Double totalCost;
	private Double tgCommission;
	private Integer parentCatId;
	private Integer zoneEvent;
	
	
	private String eventParentCategory;
	private Integer ticketOnHandStatusId;
	private Integer mzTixpurchaseOrderId;
	private Integer accountId;
	private Integer paymentTypeId;
	private Integer shippingAccountTypeId;
	private Integer shippingAccountDeliveryMethodId;
	private Integer shippingTrackingStatusId;
	private String ticketGroupCodeDesc;
	private String mzTixpurchaseOrderIds;
	private Integer ticketGroupCodeId;
	private Integer jktPoId;
	
	private String catSeatLow;
	private String catSeatHigh;
	private String seat;
	private String salesPerson;
	private String isVoidedInvoice;
	private String invoiceDateStr;
	private String invoiceVoidDateStr;
	
	private Integer clientId;
	private String clientFirstName;
	private String clientLastName;
	private String clientCompanyName;
	private String clientMail;
	private String referredBy;
	private String streetAddress1;
	private String streetAddress2;
	private String city;
	private String state;
	private String country;
	private String phoneNumber;
	private String clientType;
	
	private Boolean isCats;
	private Boolean isZones;
	private Boolean isLong;
	private Boolean isSH;
	private Boolean isTE;
	private Boolean isTnow;
	private Boolean isRetail;
	private Boolean isWholeSaleCsr;
	private Double invoiceTotal;
	
	private String invoiceNotes;
	private Boolean isUnfilled;
	private Date fillDate;
	private String fillDateStr;
	private Boolean isRewardTheFanOrder;
	private Boolean isRightThisWayOrder;
	
	private String externalNotes;
	private String tmatBrokerName;
	private PosTicket posTicket;
	private Integer ticketRequestId;
	private Integer fillAge;
	
	//Created for RTW P&L Filled Report
	private String purchaseOrderDateStr;
	private Date purchaseOrderDate;
	
	
	private Boolean isTixcityOrder;
	private Integer ticketGroupOfficeId;
	
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}

	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}

	public Integer getTicketGroupExposureId() {
		return ticketGroupExposureId;
	}

	public void setTicketGroupExposureId(Integer ticketGroupExposureId) {
		this.ticketGroupExposureId = ticketGroupExposureId;
	}

	public Date getArrivedOnDate() {
		return arrivedOnDate;
	}

	public void setArrivedOnDate(Date arrivedOnDate) {
		this.arrivedOnDate = arrivedOnDate;
	}

	public Integer getOriginalTicketCount() {
		return originalTicketCount;
	}

	public void setOriginalTicketCount(Integer originalTicketCount) {
		this.originalTicketCount = originalTicketCount;
	}

	public Integer getRemainingTicketCount() {
		return remainingTicketCount;
	}

	public void setRemainingTicketCount(Integer remainingTicketCount) {
		this.remainingTicketCount = remainingTicketCount;
	}

	public Double getRetailPrice() {
		return retailPrice;
	}

	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}

	public Double getFacePrice() {
		return facePrice;
	}

	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}

	public Double getCost() {
		return cost;
	}

	public void setCost(Double cost) {
		this.cost = cost;
	}

	public Double getWholesalePrice() {
		return wholesalePrice;
	}

	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getSystemUserId() {
		return systemUserId;
	}

	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}

	public Integer getEventId() {
		return eventId;
	}

	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}

	public Integer getStatusId() {
		return statusId;
	}

	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	public Integer getTicketGroupSeatingTypeId() {
		return ticketGroupSeatingTypeId;
	}

	public void setTicketGroupSeatingTypeId(Integer ticketGroupSeatingTypeId) {
		this.ticketGroupSeatingTypeId = ticketGroupSeatingTypeId;
	}

	public Integer getTicketGroupTypeId() {
		return ticketGroupTypeId;
	}

	public void setTicketGroupTypeId(Integer ticketGroupTypeId) {
		this.ticketGroupTypeId = ticketGroupTypeId;
	}

	public Integer getClientBrokerId() {
		return clientBrokerId;
	}

	public void setClientBrokerId(Integer clientBrokerId) {
		this.clientBrokerId = clientBrokerId;
	}

	public Integer getClientBrokerEmployeeId() {
		return clientBrokerEmployeeId;
	}

	public void setClientBrokerEmployeeId(Integer clientBrokerEmployeeId) {
		this.clientBrokerEmployeeId = clientBrokerEmployeeId;
	}

	public String getInternalNotes() {
		return internalNotes;
	}

	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}

	public Date getActualPurchaseDate() {
		return actualPurchaseDate;
	}

	public void setActualPurchaseDate(Date actualPurchaseDate) {
		this.actualPurchaseDate = actualPurchaseDate;
	}

	public Integer getAutoProcessWebRequests() {
		return autoProcessWebRequests;
	}

	public void setAutoProcessWebRequests(Integer autoProcessWebRequests) {
		this.autoProcessWebRequests = autoProcessWebRequests;
	}

	public Boolean getTaxExempt() {
		return taxExempt;
	}

	public void setTaxExempt(Boolean taxExempt) {
		this.taxExempt = taxExempt;
	}

	public Boolean getEbayAuction() {
		return ebayAuction;
	}

	public void setEbayAuction(Boolean ebayAuction) {
		this.ebayAuction = ebayAuction;
	}

	public String getEbayAuctionNumber() {
		return ebayAuctionNumber;
	}

	public void setEbayAuctionNumber(String ebayAuctionNumber) {
		this.ebayAuctionNumber = ebayAuctionNumber;
	}

	public Date getUpdateDateTime() {
		return updateDateTime;
	}

	public void setUpdateDateTime(Date updateDateTime) {
		this.updateDateTime = updateDateTime;
	}

	public String getNotOnHandMessage() {
		return notOnHandMessage;
	}

	public void setNotOnHandMessage(String notOnHandMessage) {
		this.notOnHandMessage = notOnHandMessage;
	}

	public Integer getShowTndNonTnd() {
		return showTndNonTnd;
	}

	public void setShowTndNonTnd(Integer showTndNonTnd) {
		this.showTndNonTnd = showTndNonTnd;
	}

	public Integer getOfficeId() {
		return officeId;
	}

	public void setOfficeId(Integer officeId) {
		this.officeId = officeId;
	}

	public boolean isDropSale() {
		return isDropSale;
	}

	public void setDropSale(boolean isDropSale) {
		this.isDropSale = isDropSale;
	}

	public Integer getExchangeEventId() {
		if(null == exchangeEventId){
			exchangeEventId = 0;
		}
		return exchangeEventId;
	}

	public void setExchangeEventId(Integer exchangeEventId) {
		this.exchangeEventId = exchangeEventId;
	}

	public Integer getExchangeTicketGroupId() {
		return exchangeTicketGroupId;
	}

	public void setExchangeTicketGroupId(Integer exchangeTicketGroupId) {
		this.exchangeTicketGroupId = exchangeTicketGroupId;
	}

	public Integer getUnbroadcastDaysBeforeEvent() {
		return unbroadcastDaysBeforeEvent;
	}

	public void setUnbroadcastDaysBeforeEvent(Integer unbroadcastDaysBeforeEvent) {
		this.unbroadcastDaysBeforeEvent = unbroadcastDaysBeforeEvent;
	}

	public Integer getTicketGroupStockTypeId() {
		return ticketGroupStockTypeId;
	}

	public void setTicketGroupStockTypeId(Integer ticketGroupStockTypeId) {
		this.ticketGroupStockTypeId = ticketGroupStockTypeId;
	}

	public Integer getShippingMethodSpecialId() {
		return shippingMethodSpecialId;
	}

	public void setShippingMethodSpecialId(Integer shippingMethodSpecialId) {
		this.shippingMethodSpecialId = shippingMethodSpecialId;
	}

	public Integer getShowNearTermOptionId() {
		return showNearTermOptionId;
	}

	public void setShowNearTermOptionId(Integer showNearTermOptionId) {
		this.showNearTermOptionId = showNearTermOptionId;
	}

	public Integer getTgNoteGrandFathered() {
		return tgNoteGrandFathered;
	}

	public void setTgNoteGrandFathered(Integer tgNoteGrandFathered) {
		this.tgNoteGrandFathered = tgNoteGrandFathered;
	}

	public Date getPriceUpdateDatetime() {
		return priceUpdateDatetime;
	}

	public void setPriceUpdateDatetime(Date priceUpdateDatetime) {
		this.priceUpdateDatetime = priceUpdateDatetime;
	}

	public Boolean getEticketInstantDownload() {
		return eticketInstantDownload;
	}

	public void setEticketInstantDownload(Boolean eticketInstantDownload) {
		this.eticketInstantDownload = eticketInstantDownload;
	}

	public Boolean getEticketStoredLocally() {
		return eticketStoredLocally;
	}

	public void setEticketStoredLocally(Boolean eticketStoredLocally) {
		this.eticketStoredLocally = eticketStoredLocally;
	}

	public Boolean getOverrideInstantDownloadPause() {
		return overrideInstantDownloadPause;
	}

	public void setOverrideInstantDownloadPause(
			Boolean overrideInstantDownloadPause) {
		this.overrideInstantDownloadPause = overrideInstantDownloadPause;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getEventDateStr() {
		return eventDateStr;
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}

	public Integer getSeatLow() {
		return seatLow;
	}

	public void setSeatLow(Integer seatLow) {
		this.seatLow = seatLow;
	}

	public Integer getSeatHigh() {
		return seatHigh;
	}

	public void setSeatHigh(Integer seatHigh) {
		this.seatHigh = seatHigh;
	}

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Double getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}

	public String getEventParentCategory() {
		if(parentCatId == 1){
			eventParentCategory="SPORTS";
		 }else if(parentCatId == 2){
			 eventParentCategory="CONCERTS";
		 }else if(parentCatId == 3){
			 eventParentCategory="THEATER";
		 }else{
			 eventParentCategory="OTHERS";
		 }
		return eventParentCategory;
	}

	public void setEventParentCategory(String eventParentCategory) {
		this.eventParentCategory = eventParentCategory;
	}

	public Integer getTicketOnHandStatusId() {
		return ticketOnHandStatusId;
	}

	public void setTicketOnHandStatusId(Integer ticketOnHandStatusId) {
		this.ticketOnHandStatusId = ticketOnHandStatusId;
	}


	public Integer getMzTixpurchaseOrderId() {
		return mzTixpurchaseOrderId;
	}

	public void setMzTixpurchaseOrderId(Integer mzTixpurchaseOrderId) {
		this.mzTixpurchaseOrderId = mzTixpurchaseOrderId;
	}

	public Integer getAccountId() {
		return accountId;
	}

	public void setAccountId(Integer accountId) {
		this.accountId = accountId;
	}

	public Integer getPaymentTypeId() {
		return paymentTypeId;
	}

	public void setPaymentTypeId(Integer paymentTypeId) {
		this.paymentTypeId = paymentTypeId;
	}

	public Integer getShippingAccountTypeId() {
		return shippingAccountTypeId;
	}

	public void setShippingAccountTypeId(Integer shippingAccountTypeId) {
		this.shippingAccountTypeId = shippingAccountTypeId;
	}

	public Integer getShippingAccountDeliveryMethodId() {
		return shippingAccountDeliveryMethodId;
	}

	public void setShippingAccountDeliveryMethodId(
			Integer shippingAccountDeliveryMethodId) {
		this.shippingAccountDeliveryMethodId = shippingAccountDeliveryMethodId;
	}

	public Integer getShippingTrackingStatusId() {
		return shippingTrackingStatusId;
	}

	public void setShippingTrackingStatusId(Integer shippingTrackingStatusId) {
		this.shippingTrackingStatusId = shippingTrackingStatusId;
	}

	public String getSimpleEventDateStr() {
		return simpleEventDateStr;
	}

	public void setSimpleEventDateStr(String simpleEventDateStr) {
		this.simpleEventDateStr = simpleEventDateStr;
	}

	public String getTicketGroupCodeDesc() {
		return ticketGroupCodeDesc;
	}

	public void setTicketGroupCodeDesc(String ticketGroupCodeDesc) {
		this.ticketGroupCodeDesc = ticketGroupCodeDesc;
	}

	public Integer getMzTixTicketGroupId() {
		return mzTixTicketGroupId;
	}

	public void setMzTixTicketGroupId(Integer mzTixTicketGroupId) {
		this.mzTixTicketGroupId = mzTixTicketGroupId;
	}

	public Integer getMzTixQty() {
		return mzTixQty;
	}

	public void setMzTixQty(Integer mzTixQty) {
		this.mzTixQty = mzTixQty;
	}

	public String getMzTixpurchaseOrderIds() {
		return mzTixpurchaseOrderIds;
	}

	public void setMzTixpurchaseOrderIds(String mzTixpurchaseOrderIds) {
		this.mzTixpurchaseOrderIds = mzTixpurchaseOrderIds;
	}

	public Integer getTicketGroupCodeId() {
		return ticketGroupCodeId;
	}

	public void setTicketGroupCodeId(Integer ticketGroupCodeId) {
		this.ticketGroupCodeId = ticketGroupCodeId;
	}

	public String getArrivedOnDateStr() {
		return arrivedOnDateStr;
	}

	public void setArrivedOnDateStr(String arrivedOnDateStr) {
		this.arrivedOnDateStr = arrivedOnDateStr;
	}

	public Integer getJktPoId() {
		return jktPoId;
	}

	public void setJktPoId(Integer jktPoId) {
		this.jktPoId = jktPoId;
	}

	public String getCatSeatLow() {
		return catSeatLow;
	}

	public void setCatSeatLow(String catSeatLow) {
		this.catSeatLow = catSeatLow;
	}

	public String getCatSeatHigh() {
		return catSeatHigh;
	}

	public void setCatSeatHigh(String catSeatHigh) {
		this.catSeatHigh = catSeatHigh;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public Integer getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Double getActualSoldPrice() {
		return actualSoldPrice;
	}

	public void setActualSoldPrice(Double actualSoldPrice) {
		this.actualSoldPrice = actualSoldPrice;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public Double getTgCommission() {
		return tgCommission;
	}

	public void setTgCommission(Double tgCommission) {
		this.tgCommission = tgCommission;
	}

	public String getSalesPerson() {
		return salesPerson;
	}

	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}

	public String getInvoiceDateStr() {
		return invoiceDateStr;
	}

	public void setInvoiceDateStr(String invoiceDateStr) {
		this.invoiceDateStr = invoiceDateStr;
	}

	public Integer getClientId() {
		return clientId;
	}

	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}

	public String getClientFirstName() {
		return clientFirstName;
	}

	public void setClientFirstName(String clientFirstName) {
		this.clientFirstName = clientFirstName;
	}

	public String getClientLastName() {
		return clientLastName;
	}

	public void setClientLastName(String clientLastName) {
		this.clientLastName = clientLastName;
	}

	public String getClientCompanyName() {
		return clientCompanyName;
	}

	public void setClientCompanyName(String clientCompanyName) {
		this.clientCompanyName = clientCompanyName;
	}

	public String getClientMail() {
		return clientMail;
	}

	public void setClientMail(String clientMail) {
		this.clientMail = clientMail;
	}

	public String getReferredBy() {
		return referredBy;
	}

	public void setReferredBy(String referredBy) {
		this.referredBy = referredBy;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhoneNumber() {
		if(null == phoneNumber){
			phoneNumber="";
		}
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getInvoiceVoidDateStr() {
		return invoiceVoidDateStr;
	}

	public void setInvoiceVoidDateStr(String invoiceVoidDateStr) {
		this.invoiceVoidDateStr = invoiceVoidDateStr;
	}

	public String getClientType() {
		return clientType;
	}

	public void setClientType(String clientType) {
		this.clientType = clientType;
	}

	public Double getActualWholeSalePrice() {
		return actualWholeSalePrice;
	}

	public void setActualWholeSalePrice(Double actualWholeSalePrice) {
		this.actualWholeSalePrice = actualWholeSalePrice;
	}

	public String getIsVoidedInvoice() {
		return isVoidedInvoice;
	}

	public void setIsVoidedInvoice(String isVoidedInvoice) {
		this.isVoidedInvoice = isVoidedInvoice;
	}

	public String getCommissionPerc() {
		return commissionPerc;
	}

	public void setCommissionPerc(String commissionPerc) {
		this.commissionPerc = commissionPerc;
	}

	public Boolean getIsCats() {
		if(isCats ==  null){
			isCats = false;
		}
		return isCats;
	}

	public void setIsCats(Boolean isCats) {
		this.isCats = isCats;
	}

	public Boolean getIsLong() {
		if(isLong ==  null){
			isLong = false;
		}
		return isLong;
	}

	public void setIsLong(Boolean isLong) {
		this.isLong = isLong;
	}

	public Boolean getIsSH() {
		if(isSH ==  null){
			isSH = false;
		}
		return isSH;
	}

	public void setIsSH(Boolean isSH) {
		this.isSH = isSH;
	}

	public Boolean getIsTnow() {
		if(isTnow ==  null){
			isTnow = false;
		}
		return isTnow;
	}

	public void setIsTnow(Boolean isTnow) {
		this.isTnow = isTnow;
	}

	public Boolean getIsTE() {
		if(isTE ==  null){
			isTE = false;
		}
		return isTE;
	}

	public void setIsTE(Boolean isTE) {
		this.isTE = isTE;
	}

	public Boolean getIsRetail() {
		if(isRetail ==  null){
			isRetail = false;
		}
		return isRetail;
	}
	
	
	public Boolean getIsWholeSaleCsr() {
		if(isWholeSaleCsr ==  null){
			isWholeSaleCsr = false;
		}
		return isWholeSaleCsr;
	}

	public void setIsWholeSaleCsr(Boolean isWholeSaleCsr) {
		this.isWholeSaleCsr = isWholeSaleCsr;
	}

	public Boolean getIsUnfilled() {
		if(isUnfilled ==  null){
			isUnfilled = false;
		}
		return isUnfilled;
	}

	public void setIsUnfilled(Boolean isUnfilled) {
		this.isUnfilled = isUnfilled;
	}
	
	

	public void setIsRetail(Boolean isRetail) {
		this.isRetail = isRetail;
	}

	public Double getInvoiceTotal() {
		return invoiceTotal;
	}

	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}

	public Date getFillDate() {
		return fillDate;
	}

	public void setFillDate(Date fillDate) {
		this.fillDate = fillDate;
	}

	public String getFillDateStr() {
		if(fillDateStr == null || fillDateStr.isEmpty()){
			fillDateStr="-";
		}
		return fillDateStr;
	}

	public void setFillDateStr(String fillDateStr) {
		this.fillDateStr = fillDateStr;
	}

	public Boolean getIsZones() {
		if(isZones ==  null){
			isZones = false;
		}
		return isZones;
	}

	public void setIsZones(Boolean isZones) {
		this.isZones = isZones;
	}

	public Integer getParentCatId() {
		return parentCatId;
	}

	public void setParentCatId(Integer parentCatId) {
		this.parentCatId = parentCatId;
	}

	public Integer getZoneEvent() {
		return zoneEvent;
	}

	public void setZoneEvent(Integer zoneEvent) {
		this.zoneEvent = zoneEvent;
	}

	public String getTmatBrokerName() {
		return tmatBrokerName;
	}

	public void setTmatBrokerName(String tmatBrokerName) {
		this.tmatBrokerName = tmatBrokerName;
	}

	public String getPurchaseOrderDateStr() {
		return purchaseOrderDateStr;
	}

	public void setPurchaseOrderDateStr(String purchaseOrderDateStr) {
		this.purchaseOrderDateStr = purchaseOrderDateStr;
	}

	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}

	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}

	public PosTicket getPosTicket() {
		return posTicket;
	}

	public void setPosTicket(PosTicket posTicket) {
		this.posTicket = posTicket;
	}

	public String getExternalNotes() {
		return externalNotes;
	}

	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}

	public String getInvoiceNotes() {
		return invoiceNotes;
	}

	public void setInvoiceNotes(String invoiceNotes) {
		this.invoiceNotes = invoiceNotes;
	}

	public Integer getTicketRequestId() {
		return ticketRequestId;
	}

	public void setTicketRequestId(Integer ticketRequestId) {
		this.ticketRequestId = ticketRequestId;
	}

	public Integer getFillAge() {
		return fillAge;
	}

	public void setFillAge(Integer fillAge) {
		this.fillAge = fillAge;
	}

	public Boolean getIsRewardTheFanOrder() {
		return isRewardTheFanOrder;
	}

	public void setIsRewardTheFanOrder(Boolean isRewardTheFanOrder) {
		this.isRewardTheFanOrder = isRewardTheFanOrder;
	}

	public Boolean getIsRightThisWayOrder() {
		return isRightThisWayOrder;
	}

	public void setIsRightThisWayOrder(Boolean isRightThisWayOrder) {
		this.isRightThisWayOrder = isRightThisWayOrder;
	}

	public Boolean getIsTixcityOrder() {
		if(isTixcityOrder ==  null){
			isTixcityOrder = false;
		}
		return isTixcityOrder;
	}

	public void setIsTixcityOrder(Boolean isTixcityOrder) {
		this.isTixcityOrder = isTixcityOrder;
	}

	public Integer getTicketGroupOfficeId() {
		return ticketGroupOfficeId;
	}

	public void setTicketGroupOfficeId(Integer ticketGroupOfficeId) {
		this.ticketGroupOfficeId = ticketGroupOfficeId;
	}

	
	
}
