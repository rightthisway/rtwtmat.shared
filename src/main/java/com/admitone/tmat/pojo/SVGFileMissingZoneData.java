package com.admitone.tmat.pojo;

import java.io.Serializable;

import com.admitone.tmat.data.Venue;

public class SVGFileMissingZoneData implements Serializable {
	
	
	protected Integer venueId;
	protected String building;
	protected String city;
	protected String state;
	protected String country;
	protected Integer venueCategoryId;
	protected String categoryGroupName;
	protected String zone;
	
	public SVGFileMissingZoneData() {
		
	}
	public SVGFileMissingZoneData(Venue venue) {
		this.venueId= venue.getId();
		this.building = venue.getBuilding();
		this.city = venue.getCity();
		this.state=venue.getState();
		this.country = venue.getCountry();
	}
	
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	public String getBuilding() {
		return building;
	}
	public void setBuilding(String building) {
		this.building = building;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	public String getCategoryGroupName() {
		return categoryGroupName;
	}
	public void setCategoryGroupName(String categoryGroupName) {
		this.categoryGroupName = categoryGroupName;
	}
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}


}