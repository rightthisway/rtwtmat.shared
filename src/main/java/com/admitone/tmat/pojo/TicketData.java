package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.List;

import com.admitone.tmat.data.Ticket;

public class TicketData implements Serializable{
	
	private Integer crawlerId;
	private List<Ticket> newTickets;
	private List<Ticket> updateTickets;
	
	
	public Integer getCrawlerId() {
		return crawlerId;
	}
	public void setCrawlerId(Integer crawlerId) {
		this.crawlerId = crawlerId;
	}
	public List<Ticket> getNewTickets() {
		return newTickets;
	}
	public void setNewTickets(List<Ticket> newTickets) {
		this.newTickets = newTickets;
	}
	public List<Ticket> getUpdateTickets() {
		return updateTickets;
	}
	public void setUpdateTickets(List<Ticket> updateTickets) {
		this.updateTickets = updateTickets;
	}
	

}
