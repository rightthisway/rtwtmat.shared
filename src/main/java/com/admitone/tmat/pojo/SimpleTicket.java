package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.Map;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.Ticket;



public class SimpleTicket implements Serializable {

	private int categoryId;
	private int qty;
	private int remainingQty;
	private double price;
	private String siteId;
	private Integer ticketId;
	private String section;
	private String normalizedSection;
	private String row;
	private String normalizedRow;

	public SimpleTicket(int category, int qty, double price, String siteId, Integer ticketId, String section, String row) {
		this.categoryId = category;
		this.qty = qty;
		this.remainingQty = qty;
		this.price = price;
		this.siteId = siteId;
		this.ticketId = ticketId;
		this.section = section;
		this.row = row;
	}
	
	/*public SimpleTicket(Ticket ticket, String catScheme) {
		
		if(ticket != null && !"".equals(ticket)){
//			Integer catId = ticket.getCategoryId(ticket.getEvent().getVenueCategory(),catScheme,catMap,catMappingList);
			Integer catId = null;
			if(ticket.getCategory()!=null){
				catId = ticket.getCategory().getId(); 
			}
			if(catId != null) {
				this.categoryId = catId.intValue();
			}
			if(ticket.getLotSize() != null)
				this.qty =  ticket.getLotSize().intValue();
			this.remainingQty = ticket.getRemainingQuantity();
			this.siteId = ticket.getSiteId();
			this.ticketId = ticket.getId();
			this.section = ticket.getSection();
			this.normalizedSection = ticket.getNormalizedSection();
			this.row = ticket.getRow();
			this.normalizedRow = ticket.getNormalizedRow().replaceAll("\\W", "");
	
			Double realPrice = ticket.getBuyItNowPrice();
	
			if(realPrice == null){
				
				realPrice = ticket.getCurrentPrice();
			}
			
			EventPriceAdjustment ePriceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(ticket.getEventId().intValue(), ticket.getSiteId());
			
			if(ePriceAdjustment != null){
				Double eShortPercent = ePriceAdjustment.getShortPercent();
				if(eShortPercent != null) {
					realPrice = new Double((realPrice.doubleValue() * (1.0 + (eShortPercent.doubleValue()) / 100.0)));
				}
				Double eShortFlat = ePriceAdjustment.getShortFlatFee();
				if(eShortFlat != null) {
					realPrice = new Double(realPrice.doubleValue() + eShortFlat.doubleValue());
				}
			} else {
	
				TourPriceAdjustment tPriceAdjustment = DAORegistry.getTourPriceAdjustmentDAO().get(new TourPriceAdjustmentPk(ticket.getEvent().getTourId().intValue(), ticket.getSiteId()));
	
				if(tPriceAdjustment != null){
					Double tShortPercent = tPriceAdjustment.getShortPercent();
					if(tShortPercent != null) {
						realPrice = new Double(realPrice.doubleValue() * (1.0 + (tShortPercent.doubleValue() / 100.0)));
					}
					Double tShortFlat = tPriceAdjustment.getShortFlatFee();
					if(tShortFlat != null) {
						realPrice = new Double(realPrice.doubleValue() + tShortFlat.doubleValue());
					}
				}
			}
		
			this.price = realPrice.doubleValue();
		}
	}*/
	
	public SimpleTicket(Ticket ticket, String catScheme,Map<String, ManagePurchasePrice> tourPriceMap) {
		
		if(ticket != null && !"".equals(ticket)){
			Integer catId = null;
			if(ticket.getCategory()!=null){
				catId = ticket.getCategory().getId(); 
			}
			if(catId != null) {
				this.categoryId = catId.intValue();
			}
			if(ticket.getLotSize() != null)
				this.qty =  ticket.getLotSize().intValue();
			this.remainingQty = ticket.getRemainingQuantity();
			this.siteId = ticket.getSiteId();
			this.ticketId = ticket.getId();
			this.section = ticket.getSection();
			this.normalizedSection = ticket.getNormalizedSection();
			this.row = ticket.getRow();
			this.normalizedRow = ticket.getNormalizedRow().replaceAll("\\W", "");
	
			Double realPrice = ticket.getBuyItNowPrice();
	
			/* Purchase price calculation from online price- Begins*/
			ManagePurchasePrice managePurchasePrice =tourPriceMap.get(ticket.getSiteId()+"-"+(null == ticket.getTicketType()?"REGULAR":ticket.getTicketType()));
			if( null != managePurchasePrice && managePurchasePrice.getCurrencyType().equals(1)){
				realPrice =(realPrice *(1+Double.valueOf(managePurchasePrice.getServiceFee())/100)) +
				(Double.valueOf(managePurchasePrice.getShipping()) /ticket.getRemainingQuantity());
			}else if(null != managePurchasePrice){
				realPrice =(realPrice + Double.valueOf(managePurchasePrice.getServiceFee())) +
				(Double.valueOf(managePurchasePrice.getShipping()) /ticket.getRemainingQuantity());
			}
			/* Purchase price calculation from online price- Ends*/
			
			if(realPrice == null || realPrice ==0){
				realPrice = ticket.getCurrentPrice();
			}
			
			EventPriceAdjustment ePriceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(ticket.getEventId().intValue(), ticket.getSiteId());
			
			if(ePriceAdjustment != null){
				Double eShortPercent = ePriceAdjustment.getShortPercent();
				if(eShortPercent != null) {
					realPrice = new Double((realPrice.doubleValue() * (1.0 + (eShortPercent.doubleValue()) / 100.0)));
				}
				Double eShortFlat = ePriceAdjustment.getShortFlatFee();
				if(eShortFlat != null) {
					realPrice = new Double(realPrice.doubleValue() + eShortFlat.doubleValue());
				}
			} /*else {
	
				TourPriceAdjustment tPriceAdjustment = DAORegistry.getTourPriceAdjustmentDAO().get(new TourPriceAdjustmentPk(ticket.getEvent().getTourId().intValue(), ticket.getSiteId()));
	
				if(tPriceAdjustment != null){
					Double tShortPercent = tPriceAdjustment.getShortPercent();
					if(tShortPercent != null) {
						realPrice = new Double(realPrice.doubleValue() * (1.0 + (tShortPercent.doubleValue() / 100.0)));
					}
					Double tShortFlat = tPriceAdjustment.getShortFlatFee();
					if(tShortFlat != null) {
						realPrice = new Double(realPrice.doubleValue() + tShortFlat.doubleValue());
					}
				}
			}*/
		
			this.price = realPrice.doubleValue();
		}
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int category) {
		this.categoryId = category;
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getRemainingQty() {
		return remainingQty;
	}

	public void setRemainingQty(int remainingQty) {
		this.remainingQty = remainingQty;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public String toString(){
		String returnString = "SimpleTicket["
				+ "Category: " + categoryId
				+ " Qty: " + qty 
				+ " Price: " + price + "]";
		
		return returnString;
	}

	public String getNormalizedSection() {
		return normalizedSection;
	}

	public void setNormalizedSection(String normalizedSection) {
		this.normalizedSection = normalizedSection;
	}

	public String getNormalizedRow() {
		return normalizedRow;
	}

	public void setNormalizedRow(String normalizedRow) {
		this.normalizedRow = normalizedRow;
	}
}
