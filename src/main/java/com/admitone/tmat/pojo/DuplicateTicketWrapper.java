package com.admitone.tmat.pojo;

import java.util.ArrayList;

import com.admitone.tmat.data.DuplicateTicketMap;

public class DuplicateTicketWrapper {
	private String status;
	private String msg;
	private ArrayList<DuplicateTicketMap> duplicateTicketMap;
	
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public ArrayList<DuplicateTicketMap> getDuplicateTicketMap() {
		return duplicateTicketMap;
	}
	public void setDuplicateTicketMap(
			ArrayList<DuplicateTicketMap> duplicateTicketMap) {
		this.duplicateTicketMap = duplicateTicketMap;
	}
	

}
