package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class CastStatus implements Serializable {
	
	private static final long serialVersionUID = 1L;
	  
	private int castId;
	private int eventId;
	private int categoryId;
	private String groupName;
	private String eventName;
	private String date;
	private int quantity;
	private double price;
	private double exposure;
	private String section;
	private String normalizedSection;
	private String row;
	private String normalizedRow;
	private String venue;
//	private String siteId;
	/** siteToQty is the number of tickets broadcast by AdmitOne **/
	private Map<String, Integer> qtyBySite;
	/** siteToTotalQty is the number of tickets broadcast by all Vendors **/
	private Map<String, Integer> totalQtyBySite;
	
        /* If this is an aggregate Status, this contains the
	 * statuses that make up this status */
	private List<CastStatus> children; 

	//for serialization
	public CastStatus() { }
		
	public int getCastId() {
		return castId;
	}

	public void setCastId(int castId) {
		this.castId = castId;
	}

	public int getEventId() {
		return eventId;
	}

	public void setEventId(int eventId) {
		this.eventId = eventId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getExposure() {
		return exposure;
	}

	public void setExposure(double exposure) {
		this.exposure = exposure;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getRow() {
		return row;
	}

	public void setRow(String row) {
		this.row = row;
	}

	public List<CastStatus> getChildren() {
		return children;
	}

	public void setChildren(List<CastStatus> children) {
		this.children = children;
	}

	/*
	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	*/

	public Map<String, Integer> getQtyBySite() {
		return qtyBySite;
	}

	public void setQtyBySite(Map<String, Integer> qtyBySite) {
		this.qtyBySite = qtyBySite;
	}

	public Map<String, Integer> getTotalQtyBySite() {
		return totalQtyBySite;
	}

	public void setTotalQtyBySite(Map<String, Integer> totalQtyBySite) {
		this.totalQtyBySite = totalQtyBySite;
	}

	public String toString() {
	String returnString = "CastStatus["
			+ " castId: " + castId
			+ " eventId: " + eventId
			+ " categoryId: " + categoryId
			+ " groupName: " + groupName
			+ " eventName: " + eventName
			+ " date: " + date
			+ " quantity: " + quantity
			+ " price: " + price
			+ " exposure: " + exposure 
			+ " section: " + section 
			+ " row: " + row 
//			+ " siteId: " + siteId 
			+ "children[ \n";
			if(children != null){
				for(CastStatus child : children){
					returnString = returnString + child + ", \n";
				}
			}
			returnString = returnString + "]";
	
	return returnString;
	}

	public String getNormalizedSection() {
		return normalizedSection;
	}

	public void setNormalizedSection(String normalizedSection) {
		this.normalizedSection = normalizedSection;
	}

	public String getNormalizedRow() {
		return normalizedRow;
	}

	public void setNormalizedRow(String normalizedRow) {
		this.normalizedRow = normalizedRow;
	}

	public String getVenue() {
		return venue;
	}

	public void setVenue(String venue) {
		this.venue = venue;
	}
	
	
}
