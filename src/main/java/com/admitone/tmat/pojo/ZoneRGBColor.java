package com.admitone.tmat.pojo;

import java.io.Serializable;

/**
 * class to represent a country 
 * @author hamin
 *
 */

public class ZoneRGBColor implements Serializable {
	
	private Integer id;
	private String zone;
	private String color;
	private String rgbColor;
	
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	
	public String getZone() {
		return zone;
	}
	public void setZone(String zone) {
		this.zone = zone;
	}
	
	
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	
	
	public String getRgbColor() {
		return rgbColor;
	}
	public void setRgbColor(String rgbColor) {
		this.rgbColor = rgbColor;
	}
	
	
	
	

}
