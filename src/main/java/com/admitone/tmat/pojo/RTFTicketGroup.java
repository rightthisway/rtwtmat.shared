package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RTFTicketGroup implements Serializable {

	private Integer invoiceId;
	private Integer orderId;
	private Date invoiceDate;
	private String invoiceDateStr;
	private String invoiceStatus;
	private Integer ticketGroupId;
	private Double price;
	private Double retailPrice;
	private Double facePrice;
	private Double cost;
	private Double totalCost;
	private Double wholesalePrice;
	private Double soldPrice;
	private Double totalSoldPrice;
	private String row;
	private String section;
	private String notes;
	private Integer systemUserId;
	private Integer eventId;
	private Integer customerId;
	private String internalNotes;
	private String commissionPerc;
	private String eventName;
	private Date eventDate;
	private Date eventTime;
	private String eventDateStr;
	private String eventTimeStr;
	private String venue;
	private String seatLow;
	private String seatHigh;
	private Integer quantity;
	private Double rtwCommission;
	private Integer parentCatId;
	private String eventParentCategory;
	private String salesPerson;
	private Boolean isLongSale;
	private String sectionRange;
	private String rowRange;
	private String deliveryMethod;
	
	private String isVoidedInvoice;
	private String invoiceVoidDate;
	private String invoiceVoidDateStr;
	
	private String customerName;
	private String customerEmail;
	private String streetAddress1;
	private String streetAddress2;
	private String city;
	private String state;
	private String country;
	private String customerPhone;
	private String customerType;
	
	
	private Boolean isCats;
	private Boolean isZones;
	private Boolean isLong;
	private Double invoiceTotal;
	
	private String invoiceNotes;
	private Boolean isUnfilled;
	private Date fillDate;
	private String fillDateStr;
	
	private String externalNotes;
	private String tmatBrokerName;
	
	//Created for RTW P&L Filled Report
	private String purchaseOrderDateStr;
	private Date purchaseOrderDate;
	private Integer purchaseOrderId;
	private String isRealTixUploaded;
	
	
	public static SimpleDateFormat dtFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	public static SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
	public static SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
	
	public Integer getInvoiceId() {
		return invoiceId;
	}
	public void setInvoiceId(Integer invoiceId) {
		this.invoiceId = invoiceId;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getInvoiceDateStr() {
		if(invoiceDateStr == null) {
			if(invoiceDate != null) {
				invoiceDateStr = dtFormat.format(invoiceDate);
			}
		}
		return invoiceDateStr;
	}
	public void setInvoiceDateStr(String invoiceDateStr) {
		this.invoiceDateStr = invoiceDateStr;
	}
	public String getInvoiceStatus() {
		return invoiceStatus;
	}
	public void setInvoiceStatus(String invoiceStatus) {
		this.invoiceStatus = invoiceStatus;
	}
	public Integer getTicketGroupId() {
		return ticketGroupId;
	}
	public void setTicketGroupId(Integer ticketGroupId) {
		this.ticketGroupId = ticketGroupId;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getRetailPrice() {
		return retailPrice;
	}
	public void setRetailPrice(Double retailPrice) {
		this.retailPrice = retailPrice;
	}
	public Double getFacePrice() {
		return facePrice;
	}
	public void setFacePrice(Double facePrice) {
		this.facePrice = facePrice;
	}
	public Double getCost() {
		return cost;
	}
	public void setCost(Double cost) {
		this.cost = cost;
	}
	public Double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(Double totalCost) {
		this.totalCost = totalCost;
	}
	public Double getWholesalePrice() {
		return wholesalePrice;
	}
	public void setWholesalePrice(Double wholesalePrice) {
		this.wholesalePrice = wholesalePrice;
	}
	public Double getSoldPrice() {
		return soldPrice;
	}
	public void setSoldPrice(Double soldPrice) {
		this.soldPrice = soldPrice;
	}
	public Double getTotalSoldPrice() {
		return totalSoldPrice;
	}
	public void setTotalSoldPrice(Double totalSoldPrice) {
		this.totalSoldPrice = totalSoldPrice;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getInternalNotes() {
		return internalNotes;
	}
	public void setInternalNotes(String internalNotes) {
		this.internalNotes = internalNotes;
	}
	public String getCommissionPerc() {
		return commissionPerc;
	}
	public void setCommissionPerc(String commissionPerc) {
		this.commissionPerc = commissionPerc;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	public String getEventDateStr() {
		if(eventDateStr == null) {
			if(eventDate != null) {
				eventDateStr = dateFormat.format(eventDate);
			} else {
				return "TBD";
			}
		}
		return eventDateStr;
	}
	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}
	public String getEventTimeStr() {
		if(eventTimeStr == null) {
			if(eventTime != null) {
				eventTimeStr = timeFormat.format(eventTime);
			} else {
				return "TBD";
			}
		}
		return eventTimeStr;
	}
	public void setEventTimeStr(String eventTimeStr) {
		this.eventTimeStr = eventTimeStr;
	}
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getSeatLow() {
		return seatLow;
	}
	public void setSeatLow(String seatLow) {
		this.seatLow = seatLow;
	}
	public String getSeatHigh() {
		return seatHigh;
	}
	public void setSeatHigh(String seatHigh) {
		this.seatHigh = seatHigh;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Double getRtwCommission() {
		return rtwCommission;
	}
	public void setRtwCommission(Double rtwCommission) {
		this.rtwCommission = rtwCommission;
	}
	public Integer getParentCatId() {
		return parentCatId;
	}
	public void setParentCatId(Integer parentCatId) {
		this.parentCatId = parentCatId;
	}
	public String getEventParentCategory() {
		return eventParentCategory;
	}
	public void setEventParentCategory(String eventParentCategory) {
		this.eventParentCategory = eventParentCategory;
	}
	public String getSalesPerson() {
		return salesPerson;
	}
	public void setSalesPerson(String salesPerson) {
		this.salesPerson = salesPerson;
	}
	public Boolean getIsLongSale() {
		return isLongSale;
	}
	public void setIsLongSale(Boolean isLongSale) {
		this.isLongSale = isLongSale;
	}
	public String getSectionRange() {
		return sectionRange;
	}
	public void setSectionRange(String sectionRange) {
		this.sectionRange = sectionRange;
	}
	public String getRowRange() {
		return rowRange;
	}
	public void setRowRange(String rowRange) {
		this.rowRange = rowRange;
	}
	public String getDeliveryMethod() {
		return deliveryMethod;
	}
	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}
	public String getIsVoidedInvoice() {
		return isVoidedInvoice;
	}
	public void setIsVoidedInvoice(String isVoidedInvoice) {
		this.isVoidedInvoice = isVoidedInvoice;
	}
	public String getInvoiceVoidDate() {
		return invoiceVoidDate;
	}
	public void setInvoiceVoidDate(String invoiceVoidDate) {
		this.invoiceVoidDate = invoiceVoidDate;
	}
	public String getInvoiceVoidDateStr() {
		if(invoiceVoidDateStr == null) {
			if(invoiceVoidDate != null) {
				invoiceVoidDateStr = dtFormat.format(invoiceVoidDate);
			}
		}
		return invoiceVoidDateStr;
	}
	public void setInvoiceVoidDateStr(String invoiceVoidDateStr) {
		this.invoiceVoidDateStr = invoiceVoidDateStr;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getCustomerEmail() {
		return customerEmail;
	}
	public void setCustomerEmail(String customerEmail) {
		this.customerEmail = customerEmail;
	}
	public String getStreetAddress1() {
		return streetAddress1;
	}
	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = streetAddress1;
	}
	public String getStreetAddress2() {
		return streetAddress2;
	}
	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = streetAddress2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCustomerPhone() {
		return customerPhone;
	}
	public void setCustomerPhone(String customerPhone) {
		this.customerPhone = customerPhone;
	}
	public String getCustomerType() {
		return customerType;
	}
	public void setCustomerType(String customerType) {
		this.customerType = customerType;
	}
	public Boolean getIsCats() {
		return isCats;
	}
	public void setIsCats(Boolean isCats) {
		this.isCats = isCats;
	}
	public Boolean getIsZones() {
		return isZones;
	}
	public void setIsZones(Boolean isZones) {
		this.isZones = isZones;
	}
	public Boolean getIsLong() {
		return isLong;
	}
	public void setIsLong(Boolean isLong) {
		this.isLong = isLong;
	}
	public Double getInvoiceTotal() {
		return invoiceTotal;
	}
	public void setInvoiceTotal(Double invoiceTotal) {
		this.invoiceTotal = invoiceTotal;
	}
	public String getInvoiceNotes() {
		return invoiceNotes;
	}
	public void setInvoiceNotes(String invoiceNotes) {
		this.invoiceNotes = invoiceNotes;
	}
	public Boolean getIsUnfilled() {
		return isUnfilled;
	}
	public void setIsUnfilled(Boolean isUnfilled) {
		this.isUnfilled = isUnfilled;
	}
	public Date getFillDate() {
		return fillDate;
	}
	public void setFillDate(Date fillDate) {
		this.fillDate = fillDate;
	}
	public String getFillDateStr() {
		if(fillDateStr == null) {
			if(fillDate != null) {
				fillDateStr = dtFormat.format(fillDate);
			}
		}
		return fillDateStr;
	}
	public void setFillDateStr(String fillDateStr) {
		this.fillDateStr = fillDateStr;
	}
	public String getExternalNotes() {
		return externalNotes;
	}
	public void setExternalNotes(String externalNotes) {
		this.externalNotes = externalNotes;
	}
	public String getTmatBrokerName() {
		return tmatBrokerName;
	}
	public void setTmatBrokerName(String tmatBrokerName) {
		this.tmatBrokerName = tmatBrokerName;
	}
	public String getPurchaseOrderDateStr() {
		if(purchaseOrderDateStr == null) {
			if(purchaseOrderDate != null) {
				purchaseOrderDateStr = dtFormat.format(purchaseOrderDate);
			}
		}
		return purchaseOrderDateStr;
	}
	public void setPurchaseOrderDateStr(String purchaseOrderDateStr) {
		this.purchaseOrderDateStr = purchaseOrderDateStr;
	}
	public Date getPurchaseOrderDate() {
		return purchaseOrderDate;
	}
	public void setPurchaseOrderDate(Date purchaseOrderDate) {
		this.purchaseOrderDate = purchaseOrderDate;
	}
	public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public String getIsRealTixUploaded() {
		return isRealTixUploaded;
	}
	public void setIsRealTixUploaded(String isRealTixUploaded) {
		this.isRealTixUploaded = isRealTixUploaded;
	}
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	
	

}
