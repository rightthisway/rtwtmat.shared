package com.admitone.tmat.pojo;


import java.sql.Time;
import java.util.Date;

import com.admitone.tmat.data.Event;


public class EventDTO {

	protected Integer id;
	protected String name;
	protected Date eventDate;
	protected Date eventTime;
	protected String eventStatus;
	protected Integer venueId;
	protected String venueName;
	protected String city;
	protected String state;
	protected String country;
	protected String postalCode;
	protected Integer admitoneId;
	protected Integer venueCategoryId;
	protected String venueCategoryName;
	protected Integer noOfCrawls;
	protected Integer artistId;
	protected String artistName;
	protected Integer grandChildCategoryId;
	protected String grandChildCategoryName;
	protected Integer childCategoryId;
	protected String childCategoryName;
	protected Integer parentCategoryId;
	protected String parentCategoryName;
	protected Boolean isZoneEvent;
	protected Event event;
	protected String process;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getEventDate() {
		return eventDate;
	}
	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}
	public Date getEventTime() {
		return eventTime;
	}
	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}
	public String getEventStatus() {
		return eventStatus;
	}
	public void setEventStatus(String eventStatus) {
		this.eventStatus = eventStatus;
	}
	public Integer getVenueId() {
		return venueId;
	}
	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}
	public Integer getAdmitoneId() {
		return admitoneId;
	}
	public void setAdmitoneId(Integer admitoneId) {
		this.admitoneId = admitoneId;
	}
	public Integer getVenueCategoryId() {
		return venueCategoryId;
	}
	public void setVenueCategoryId(Integer venueCategoryId) {
		this.venueCategoryId = venueCategoryId;
	}
	public Integer getNoOfCrawls() {
		return noOfCrawls;
	}
	public void setNoOfCrawls(Integer noOfCrawls) {
		this.noOfCrawls = noOfCrawls;
	}
	public Integer getGrandChildCategoryId() {
		return grandChildCategoryId;
	}
	public void setGrandChildCategoryId(Integer grandChildCategoryId) {
		this.grandChildCategoryId = grandChildCategoryId;
	}
	public Event getEvent() {
		
		if(event == null) {
			event = new Event();
			event.setId(this.id);
			event.setName(this.name);
			event.setVenueCategoryId(this.venueCategoryId);
			
			if(this.eventDate !=null){
				event.setLocalDate(new Date(this.eventDate.getTime()));
			}
			if(this.eventTime !=null){
				event.setLocalTime(new Time(this.eventTime.getTime()));	
			}
		}
		return event;
	}
	public Boolean getIsZoneEvent() {
		return isZoneEvent;
	}
	public void setIsZoneEvent(Boolean isZoneEvent) {
		this.isZoneEvent = isZoneEvent;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getVenueCategoryName() {
		return venueCategoryName;
	}
	public void setVenueCategoryName(String venueCategoryName) {
		this.venueCategoryName = venueCategoryName;
	}
	public Integer getArtistId() {
		return artistId;
	}
	public void setArtistId(Integer artistId) {
		this.artistId = artistId;
	}
	public String getArtistName() {
		return artistName;
	}
	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}
	public String getGrandChildCategoryName() {
		return grandChildCategoryName;
	}
	public void setGrandChildCategoryName(String grandChildCategoryName) {
		this.grandChildCategoryName = grandChildCategoryName;
	}
	public Integer getChildCategoryId() {
		return childCategoryId;
	}
	public void setChildCategoryId(Integer childCategoryId) {
		this.childCategoryId = childCategoryId;
	}
	public String getChildCategoryName() {
		return childCategoryName;
	}
	public void setChildCategoryName(String childCategoryName) {
		this.childCategoryName = childCategoryName;
	}
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	public String getParentCategoryName() {
		return parentCategoryName;
	}
	public void setParentCategoryName(String parentCategoryName) {
		this.parentCategoryName = parentCategoryName;
	}
	public String getProcess() {
		return process;
	}
	public void setProcess(String process) {
		this.process = process;
	}
	public void setEvent(Event event) {
		this.event = event;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	
	
}
