package com.admitone.tmat.pojo;

public class EventCrawlerDetails {

	protected Integer eventId;
	protected String eventName;
	protected String eventDate;
	protected String eventTime;
	protected String venueName;
	protected String city;
	protected String state;
	protected String country;
	protected String tourName;
	protected String siteId;
	protected Integer crawlCount=0;
	protected String activeCrawlSites;
	protected String eventCreatedDate;
	protected String parentCategory;
	
	
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getEventTime() {
		return eventTime;
	}
	public void setEventTime(String eventTime) {
		this.eventTime = eventTime;
	}
	public String getVenueName() {
		return venueName;
	}
	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getTourName() {
		return tourName;
	}
	public void setTourName(String tourName) {
		this.tourName = tourName;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public Integer getCrawlCount() {
		return crawlCount;
	}
	public void setCrawlCount(Integer crawlCount) {
		this.crawlCount = crawlCount;
	}
	public String getActiveCrawlSites() {
		return activeCrawlSites;
	}
	public void setActiveCrawlSites(String activeCrawlSites) {
		this.activeCrawlSites = activeCrawlSites;
	}
	public String getEventCreatedDate() {
		return eventCreatedDate;
	}
	public void setEventCreatedDate(String eventCreatedDate) {
		this.eventCreatedDate = eventCreatedDate;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	
		
}
