package com.admitone.tmat.pojo;


import java.util.List;

public class CheapestZone {

	private Integer eventId;
	private String eventName;
	private String eventDate;
	private String venue;
	
	
	private Integer categoryId;
	private String category;
	private Integer quantity;
	private String section;
	private String row;
	private double purchasePrice;
	private double onlinePrice;
	private String seller;
	private String siteId;
	
	private double nextPurchasePrice;
	private double difference;
	private String nextSection;
	private String nextRow;
	private Integer nextQuantity;
	
	private List<CheapestZone> children;
	
	
	
	
	
	
	
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	
	
	
	
	
	
	
	public double getNextPurchasePrice() {
		return nextPurchasePrice;
	}
	public void setNextPurchasePrice(double nextPurchasePrice) {
		this.nextPurchasePrice = nextPurchasePrice;
	}
	public double getDifference() {
		return difference;
	}
	public void setDifference(double difference) {
		this.difference = difference;
	}
	public String getNextSection() {
		return nextSection;
	}
	public void setNextSection(String nextSection) {
		this.nextSection = nextSection;
	}
	public String getNextRow() {
		return nextRow;
	}
	public void setNextRow(String nextRow) {
		this.nextRow = nextRow;
	}
	public Integer getNextQuantity() {
		return nextQuantity;
	}
	public void setNextQuantity(Integer nextQuantity) {
		this.nextQuantity = nextQuantity;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	
	public String getVenue() {
		return venue;
	}
	public void setVenue(String venue) {
		this.venue = venue;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getRow() {
		return row;
	}
	public void setRow(String row) {
		this.row = row;
	}
	public double getPurchasePrice() {
		return purchasePrice;
	}
	public void setPurchasePrice(double purchasePrice) {
		this.purchasePrice = purchasePrice;
	}
	public double getOnlinePrice() {
		return onlinePrice;
	}
	public void setOnlinePrice(double onlinePrice) {
		this.onlinePrice = onlinePrice;
	}
	
	
	public String getSeller() {
		return seller;
	}
	public void setSeller(String seller) {
		this.seller = seller;
	}
	
	public List<CheapestZone> getChildren() {
		return children;
	}
	public void setChildren(List<CheapestZone> children) {
		this.children = children;
	}
	
	
	
	
}
