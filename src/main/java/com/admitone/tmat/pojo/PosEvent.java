package com.admitone.tmat.pojo;


import java.io.Serializable;
import java.util.Date;



public final class PosEvent implements Serializable  {
	protected static final long serialVersionUID = -7402732543016575381L;
	
	protected Integer id;
	protected Integer venueId;
	protected String eventName;
	protected String venueName;
	protected Integer exchangeEventId;
	protected Date eventDate;
	protected String eventDateStr;
	protected Integer exchangeVenueId;
	protected Integer parentCatId;
	protected Integer zoneEvent;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getVenueId() {
		return venueId;
	}

	public void setVenueId(Integer venueId) {
		this.venueId = venueId;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	public String getVenueName() {
		return venueName;
	}

	public void setVenueName(String venueName) {
		this.venueName = venueName;
	}
	public String getEventDateStr() {
		return eventDateStr;
	}

	public void setEventDateStr(String eventDateStr) {
		this.eventDateStr = eventDateStr;
	}

	public Integer getExchangeEventId() {
		return exchangeEventId;
	}

	public void setExchangeEventId(Integer exchangeEventId) {
		this.exchangeEventId = exchangeEventId;
	}

	public Date getEventDate() {
		return eventDate;
	}

	public void setEventDate(Date eventDate) {
		this.eventDate = eventDate;
	}

	public Integer getExchangeVenueId() {
		return exchangeVenueId;
	}

	public void setExchangeVenueId(Integer exchangeVenueId) {
		this.exchangeVenueId = exchangeVenueId;
	}

	public Integer getParentCatId() {
		return parentCatId;
	}

	public void setParentCatId(Integer parentCatId) {
		this.parentCatId = parentCatId;
	}

	public Integer getZoneEvent() {
		return zoneEvent;
	}

	public void setZoneEvent(Integer zoneEvent) {
		this.zoneEvent = zoneEvent;
	}
	
	
	
}