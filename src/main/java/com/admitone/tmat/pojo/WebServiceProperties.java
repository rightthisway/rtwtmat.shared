package com.admitone.tmat.pojo;

public class WebServiceProperties {

	private String webServiceUrl;
	private String configId;
	
	public String getWebServiceUrl() {
		return webServiceUrl;
	}
	public void setWebServiceUrl(String webServiceUrl) {
		this.webServiceUrl = webServiceUrl;
	}
	
	public String getConfigId() {
		return configId;
	}
	public void setConfigId(String configId) {
		this.configId = configId;
	}
}
