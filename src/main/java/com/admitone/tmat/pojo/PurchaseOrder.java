package com.admitone.tmat.pojo;

import java.io.Serializable;
import java.util.Date;


public class PurchaseOrder implements Serializable {
	
	  private Integer purchaseOrderId;
	  private Double purchaseOrderTotal;
	  private Integer systemUserId;
	  private Date createDate;
	
	  
	  public Integer getPurchaseOrderId() {
		return purchaseOrderId;
	}
	public void setPurchaseOrderId(Integer purchaseOrderId) {
		this.purchaseOrderId = purchaseOrderId;
	}
	public Integer getSystemUserId() {
		return systemUserId;
	}
	public void setSystemUserId(Integer systemUserId) {
		this.systemUserId = systemUserId;
	}
	public Double getPurchaseOrderTotal() {
		return purchaseOrderTotal;
	}
	public void setPurchaseOrderTotal(Double purchaseOrderTotal) {
		this.purchaseOrderTotal = purchaseOrderTotal;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
		  
	  
	  
	  
}
