package com.admitone.tmat.security;

import java.util.Collection;
import java.util.HashSet;

import org.acegisecurity.GrantedAuthority;
import org.acegisecurity.GrantedAuthorityImpl;
import org.acegisecurity.userdetails.UserDetails;
import org.acegisecurity.userdetails.UserDetailsService;
import org.acegisecurity.userdetails.UsernameNotFoundException;
import org.springframework.dao.DataAccessException;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Privilege;
import com.admitone.tmat.data.Role;
import com.admitone.tmat.data.User;
import com.admitone.tmat.utils.PreferenceManager;

public class AuthenticationService implements UserDetailsService {
	private PreferenceManager preferenceManager;

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException, DataAccessException {
		
		User user = DAORegistry.getUserDAO().getUserByUsernameAndBrokerid(username, 1);
		if (user == null) {
			return null;
		}
		
		// if user was not already logged in, delete his filters
		if (!SessionListener.isLoggedIn(username)) {
			preferenceManager.resetFilterPreferenceForUser(username);
		}
		
		
		Collection<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
		for (Role role: user.getRoles()) {
			authorities.add(new GrantedAuthorityImpl(role.getName()));
			for (Privilege privilege: role.getPrivileges()) {
				authorities.add(new GrantedAuthorityImpl(privilege.getName()));
			}
		}
		
		authorities.add(new GrantedAuthorityImpl("PRIV_LOGGED_IN"));

		return new org.acegisecurity.userdetails.User(user.getUsername(),
					user.getPassword(), true, true, true, !user.getLocked(), authorities.toArray(new GrantedAuthority[authorities.size()]));
	}

	public void setPreferenceManager(PreferenceManager preferenceManager) {
		this.preferenceManager = preferenceManager;
	}

}
