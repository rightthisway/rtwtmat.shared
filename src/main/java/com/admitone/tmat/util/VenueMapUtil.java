package com.admitone.tmat.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.data.VenueMapZones;
import com.admitone.tmat.utils.mail.MailManager;

public class VenueMapUtil extends QuartzJobBean implements StatefulJob{
	
	public static Date lastUpdateTime;
	public static Date nextRunTime;
	private static Boolean running=false;
	private static Boolean stopped;
	private static MailManager mailManager = null;

	public static SimpleDateFormat dtFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		
		try{
			updateVenueMapStatus();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
/*public static  void updateVenueMapStatusOldd() throws Exception {
		
		try {
			System.out.println("VENUEMAPSTATUS : RTW Venue Map Status Updater: Job Started....."+new Date());
			
			Calendar cal =  Calendar.getInstance();
			Date now = cal.getTime();
			setLastUpdateTime(now);
			
			//cal.add(Calendar.MINUTE,15);
			//setNextRunTime(cal.getTime());
			if(isStopped() || isRunning()){
				return ;
			}
			setRunning(true);
			int cnt = 0;
			Collection<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getAll();
			List<VenueCategory> toBeUpdateList = new ArrayList<VenueCategory>();
			for (VenueCategory venueCategory : venueCategories) {
				if(isStopped()){
					 break;
				 }
				File svgTxtFile = new File("////C://TMATIMAGESFINAL//SvgText//" + venueCategory.getVenue().getId()+"_"+venueCategory.getCategoryGroup()+".txt");
				if(svgTxtFile != null && svgTxtFile.exists()){
					
					if(!venueCategory.getIsVenuMap()) {
						venueCategory.setIsVenuMap(true);
						toBeUpdateList.add(venueCategory);
					}
				}else{
					if(venueCategory.getIsVenuMap()) {
						venueCategory.setIsVenuMap(false);
						toBeUpdateList.add(venueCategory);
					}
					
				}
				
				if(cnt % 100 == 0) {
					System.out.println(" processed : "+cnt);
				}
				cnt++;
			}
			System.out.println("PRESALEEVENTJOB Update Size : "+toBeUpdateList.size());
			DAORegistry.getVenueCategoryDAO().saveOrUpdateAll(toBeUpdateList);
			
			System.out.println("VENUEMAPSTATUS : RTW Venue Map Status Updater: Job Finished....."+new Date());
			
			setRunning(false);
		}catch (Exception e) {
			setRunning(false);
			e.printStackTrace();
			
			System.out.println("PRESALEEVENTJOB : Error in Presale Event Job..."+new Date());
			e.printStackTrace();
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "Presale Event Converter Job Failed : ";
			String template = "common-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("bodyContent", "Following error occured in Presale Event Converter Job:-");
			map.put("error", e);
			
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			
			throw e;
		}		
	}*/
	public static  void updateVenueMapStatus() throws Exception {
		
		try {
			System.out.println("VENUEMAPSTATUS : RTW Venue Map Status Updater: Job Started....."+new Date());
			
			Calendar cal =  Calendar.getInstance();
			Date now = cal.getTime();
			setLastUpdateTime(now);
			
			//cal.add(Calendar.MINUTE,15);
			//setNextRunTime(cal.getTime());
			if(isStopped() || isRunning()){
				return ;
			}
			setRunning(true);
			
			Collection<VenueMapZones> venueMapZoneListFromDb = DAORegistry.getVenueMapZonesDAO().getAll();
			Map<Integer,Map<String,VenueMapZones>> venueMapZonesMap = new HashMap<Integer,Map<String,VenueMapZones>>();
			for (VenueMapZones venueMapZones : venueMapZoneListFromDb) {
				
				Map<String,VenueMapZones> tempMap = venueMapZonesMap.get(venueMapZones.getVenueCategoryId());
				if(tempMap == null) {
					tempMap = new HashMap<String, VenueMapZones>();
				}
				tempMap.put(venueMapZones.getZone(),venueMapZones);
				venueMapZonesMap.put(venueMapZones.getVenueCategoryId(), tempMap);
			}
			Collection<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getAll();
			List<VenueCategory> toBeUpdateList = new ArrayList<VenueCategory>();
			List<VenueCategory> errorVenueCats = new ArrayList<VenueCategory>();
			
			List<VenueMapZones> updateVenueMapZoneList = new ArrayList<VenueMapZones>();
			List<VenueMapZones> deleteVenueMapZoneList = new ArrayList<VenueMapZones>();
			int total=venueCategories.size();
			int processed=0;
			
			for (VenueCategory venueCategory : venueCategories) {
				if(isStopped()){
					 break;
				 }
				File svgTxtFile = new File("////C://TMATIMAGESFINAL//SvgText//" + venueCategory.getVenue().getId()+"_"+venueCategory.getCategoryGroup()+".txt");
				if(svgTxtFile != null && svgTxtFile.exists()){
					if(!venueCategory.getIsVenuMap()) {
						venueCategory.setIsVenuMap(true);
						toBeUpdateList.add(venueCategory);
					}
				}else{
					if(venueCategory.getIsVenuMap()) {
						venueCategory.setIsVenuMap(false);
						toBeUpdateList.add(venueCategory);
					}
					
				}
				
				Map<String,VenueMapZones> venuemapZoneMapDb = venueMapZonesMap.get(venueCategory.getId());
				
				 //FileReader fReader = null;
				 //BufferedReader reader = null;
				 String svgText = "",str="";
				 Map<String, Boolean> svgZoneMap = new HashMap<String, Boolean>();
				 if(svgTxtFile != null && svgTxtFile.exists()) {
					/* fReader = new FileReader(svgTxtFile);
					 reader = new BufferedReader(fReader);
					 while((str = reader.readLine()) != null) {
						 svgText += str;
					 }*/
					 svgText = FileUtils.readFileToString(svgTxtFile);
					 svgText = svgText.replace("\n", "").replace("\r", "");
					 try {
						 svgZoneMap = getAllZonesFromText(svgText);
					 } catch(Exception e) {
						 System.out.println("Error : "+ venueCategory.getVenue().getId()+"_"+venueCategory.getCategoryGroup()+" : "+venueCategory.getId());
						 errorVenueCats.add(venueCategory);
						 //continue;
					 }
				 }
				 if(svgZoneMap== null) {
					 svgZoneMap =new HashMap<String, Boolean>();
				 }
				 if(venuemapZoneMapDb== null) {
					 venuemapZoneMapDb =new HashMap<String, VenueMapZones>();
				 }
				 List<String> keyListFromSvgMap = new ArrayList<String>(svgZoneMap.keySet());
				 for (String key : keyListFromSvgMap) {
					 VenueMapZones venueMapZone = venuemapZoneMapDb.remove(key);
					 if(venueMapZone == null) {
						 venueMapZone = new VenueMapZones();
						 venueMapZone.setZone(key);
						 venueMapZone.setVenueCategoryId(venueCategory.getId());
						 //venueMapZone.setVenueId(venueCategory.getVenue().getId());
						 updateVenueMapZoneList.add(venueMapZone);
					 }
				}
				 for (String key : venuemapZoneMapDb.keySet()) {
					 deleteVenueMapZoneList.add(venuemapZoneMapDb.get(key));
				}
				 
				/*if(processed % 1000 == 0) {
					System.out.println(processed+"/"+total+" : "+new Date());
				}*/
				processed++;
			}
			System.out.println("VENUEMAPSTATUS Update Size : "+toBeUpdateList.size()+" : Zones Create Size : "+updateVenueMapZoneList.size()+" : Zones Delete Size : "+deleteVenueMapZoneList.size()+" : error venuecats Size : "+errorVenueCats.size());
			
			DAORegistry.getVenueCategoryDAO().saveOrUpdateAll(toBeUpdateList);
			
			DAORegistry.getVenueMapZonesDAO().saveOrUpdateAll(updateVenueMapZoneList);
			DAORegistry.getVenueMapZonesDAO().deleteAll(deleteVenueMapZoneList);
			
			System.out.println("VENUEMAPSTATUS : RTW Venue Map Status Updater: Job Finished....."+new Date());
			
			setRunning(false);
		}catch (Exception e) {
			setRunning(false);
			e.printStackTrace();
			
			System.out.println("PRESALEEVENTJOB : Error in Presale Event Job..."+new Date());
			e.printStackTrace();
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "Presale Event Converter Job Failed : ";
			String template = "common-mail-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("bodyContent", "Following error occured in Presale Event Converter Job:-");
			map.put("error", e);
			
			//String toAddress = "pmehul@rightthisway.com";
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			
			throw e;
		}		
	}
	
	public static Map<String, Boolean> getAllZonesFromText(String svgText) throws Exception {
		
		Map<String, Boolean> allZonesMap = new HashMap<String, Boolean>();
		try {
		
			Pattern pattern = Pattern.compile("<g id=\"zone_letters\" display=\"none\">(.*?)</g>");
			Matcher matcher = pattern.matcher(svgText);
						
			String value = new String();
			
			if(matcher.find()){
				//System.out.println("ZONE :" + dayMatcher.group(1));
				value = matcher.group(1);
			}
			
			String[] strings = value.split("transform=");
			//System.out.println(value);
			for (String text : strings) {
				
				Pattern dayPattern = Pattern.compile("<text id=\"text_(.*?)\" font-weight");
				Matcher dayMatcher = dayPattern.matcher(text);
				
				if(dayMatcher.find()){
					//System.out.println("ZONE :" + dayMatcher.group(1));
					String zone = dayMatcher.group(1);
					allZonesMap.put(zone.toUpperCase(), true);
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
		
		return allZonesMap;
		 
	 }
	public static void updateVenueMapStatusByVenueAndCategoryGroup(Integer venueId,List<String> categoryGroups) throws Exception {
		
		try {
			Collection<VenueCategory> venueCategories = DAORegistry.getVenueCategoryDAO().getVenueCategoriesByVenueId(venueId);

			List<VenueCategory> toBeUpdateList = new ArrayList<VenueCategory>();
			for (VenueCategory venueCategory : venueCategories) {
				
				if(categoryGroups.contains(venueCategory.getCategoryGroup())) {
					File svgTxtFile = new File("////C://TMATIMAGESFINAL//SvgText//" + venueCategory.getVenue().getId()+"_"+venueCategory.getCategoryGroup()+".txt");
					if(svgTxtFile != null && svgTxtFile.exists()){
						venueCategory.setIsVenuMap(true);
					}else{
						venueCategory.setIsVenuMap(false);
					}
					toBeUpdateList.add(venueCategory);
					
					List<VenueMapZones> updateVenueMapZoneList = new ArrayList<VenueMapZones>();
					List<VenueMapZones> deleteVenueMapZoneList = new ArrayList<VenueMapZones>();
					
					List<VenueMapZones> venueMapZonesList = DAORegistry.getVenueMapZonesDAO().getAllVenueMapZonesByVenueCategoryId(venueCategory.getId());
					Map<String,VenueMapZones> venuemapZoneMapDb = new HashMap<String, VenueMapZones>();
					for (VenueMapZones venueMapZones : venueMapZonesList) {
						venuemapZoneMapDb.put(venueMapZones.getZone(), venueMapZones);
					}
					
					 //FileReader fReader = null;
					 //BufferedReader reader = null;
					 String svgText = "",str="";
					 Map<String, Boolean> svgZoneMap = new HashMap<String, Boolean>();
					 if(svgTxtFile != null && svgTxtFile.exists()) {
						/* fReader = new FileReader(svgTxtFile);
						 reader = new BufferedReader(fReader);
						 while((str = reader.readLine()) != null) {
							 svgText += str;
						 }*/
						 svgText = FileUtils.readFileToString(svgTxtFile);
						 svgText = svgText.replace("\n", "").replace("\r", "");
						 try {
							 svgZoneMap = getAllZonesFromText(svgText);
						 } catch(Exception e) {
							 System.out.println("Error : "+ venueCategory.getVenue().getId()+"_"+venueCategory.getCategoryGroup()+" : "+venueCategory.getId());
							 //errorVenueCats.add(venueCategory);
							 //continue;
						 }
					 }
					 if(svgZoneMap== null) {
						 svgZoneMap =new HashMap<String, Boolean>();
					 }
					 List<String> keyListFromSvgMap = new ArrayList<String>(svgZoneMap.keySet());
					 for (String key : keyListFromSvgMap) {
						 VenueMapZones venueMapZone = venuemapZoneMapDb.remove(key);
						 if(venueMapZone == null) {
							 venueMapZone = new VenueMapZones();
							 venueMapZone.setZone(key);
							 venueMapZone.setVenueCategoryId(venueCategory.getId());
							 //venueMapZone.setVenueId(venueCategory.getVenue().getId());
							 updateVenueMapZoneList.add(venueMapZone);
						 }
					}
					 for (String key : venuemapZoneMapDb.keySet()) {
						 deleteVenueMapZoneList.add(venuemapZoneMapDb.get(key));
					}
					 DAORegistry.getVenueMapZonesDAO().saveOrUpdateAll(updateVenueMapZoneList);
					 DAORegistry.getVenueMapZonesDAO().deleteAll(deleteVenueMapZoneList);
				}
			}
			DAORegistry.getVenueCategoryDAO().saveOrUpdateAll(toBeUpdateList);
			
			/*if(!toBeUpdateList.isEmpty()) {
				for (VenueCategory venueCategory : toBeUpdateList) {
					if(venueCategory.getIsVenuMap()) {
					DAORegistry.getQueryManagerDAO().updateRTFVenueMapStatus(venueCategory.getId(), connection);
					}
				}	
			}*/
			
			
		}catch (Exception e) {
			e.printStackTrace();
			throw e;
		}		
	}
	
	public static Boolean isStopped() {
		if(stopped==null){
			return false;
		}
		return stopped;
	}


	public static void setStopped(Boolean stopped) {
		VenueMapUtil.stopped = stopped;
	}


	public static Boolean isRunning() {
		return running;
	}


	public static void setRunning(Boolean running) {
		VenueMapUtil.running = running;
	}


	public static Date getLastUpdateTime() {
		if(lastUpdateTime==null){
			Property lastRunTimeProperty = DAORegistry.getPropertyDAO().get("venue.map.status.last.run.time");
			if(lastRunTimeProperty != null && lastRunTimeProperty.getValue() != null) {
				try {
					lastUpdateTime = dtFormat.parse(lastRunTimeProperty.getValue());
				}catch (Exception e) {
					// TODO: handle exception
				}
			}
		}
		return lastUpdateTime;
	}


	public static void setLastUpdateTime(Date lastUpdateTime) {
		VenueMapUtil.lastUpdateTime = lastUpdateTime;
	}


	public static Date getNextRunTime() {
		return nextRunTime;
	}


	public static void setNextRunTime(Date nextRunTime) {
		VenueMapUtil.nextRunTime = nextRunTime;
	}
	
	public MailManager getMailManager() {
		return VenueMapUtil.mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		VenueMapUtil.mailManager = mailManager;
	}

	public static String getLastUpdateTimeStr() {
		Date lastUpdateTime = getLastUpdateTime();
		if(lastUpdateTime != null) {
			return dtFormat.format(lastUpdateTime);
		}
		return null;
	}
	public static String getNextRunTimeStr() {
		Calendar cal = Calendar.getInstance();
		if(cal.get(Calendar.MINUTE) >= 45) {
			cal.add(Calendar.HOUR_OF_DAY, 1);
		}
		cal.set(Calendar.MINUTE, 45);
		Date nextRunTime = cal.getTime();
		
		return dtFormat.format(nextRunTime);
	}
}
