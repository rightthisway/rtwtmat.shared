package com.admitone.tmat.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.SoldTicketDetail;
import com.admitone.tmat.dwr.CrawlerDwr;

public class RTWOpenOrdersAutomaticCrawlUpdater extends QuartzJobBean implements StatefulJob{

	public static final int DEFAULT_TIMEOUT = 30000;
	public static Date lastRunTime = new Date();
	public static DateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss");

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		System.out.println("RTWOOCU : RTW Open Order Automcatic Crawl Updater: Uploader Job Started....."+new Date());
		try{
			getRTWOpenOrdersCrawlUpdater();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("RTWOOCU : RTW Open Order Automcatic Crawl Updater: Uploader Job Finished....."+new Date());
	}
	
	public void getRTWOpenOrdersCrawlUpdater(){
		
		Collection<SoldTicketDetail> ticketDetail =  new ArrayList<SoldTicketDetail>();
		try{
			 //ticketDetail = DAORegistry.getSoldTicketDetailDAO().getSoldUnfilledTicketDetailsByBrokerId(5);
			 ticketDetail = DAORegistry.getSoldTicketDetailDAO().getAllBrokersSoldUnfilledTicketDetails();
		}catch(Exception e){
			e.printStackTrace();
		}
		Collection<Event> events = DAORegistry.getEventDAO().getAllEventByAdmitoneId();
		Map<Integer, Event> tmatEventIdMap = new HashMap<Integer, Event>();
		for (Event event : events) {
			tmatEventIdMap.put(event.getAdmitoneId() , event);
		}
		if(null != ticketDetail && !ticketDetail.isEmpty()){
			System.out.println("RTWCU : Total No Of open Orders : "+ticketDetail.size());
			String eventIDs = "";
			for (SoldTicketDetail soldTicketDetail : ticketDetail) {
				Event event = tmatEventIdMap.get(soldTicketDetail.getEventId());
				if(null == event){
					continue;
				}
				eventIDs = eventIDs +","+event.getId();
			}
			System.out.println("RTWCU : RTW CRAWL UPDATER BEGINS-"+new Date());
			CrawlerDwr crawlerDwr = new CrawlerDwr();
			crawlerDwr.rtwOpenOrderforceCrawlerByEvent(eventIDs);
			System.out.println("RTWCU : RTW CRAWL UPDATER ENDS-"+new Date());
		}
	}
}
