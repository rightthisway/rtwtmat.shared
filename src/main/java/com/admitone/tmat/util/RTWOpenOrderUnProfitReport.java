package com.admitone.tmat.util;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

import com.admitone.tmat.common.MailIdProperty;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.OpenOrderStatus;
import com.admitone.tmat.enums.ProfitLossSign;
import com.admitone.tmat.utils.mail.MailAttachment;
import com.admitone.tmat.utils.mail.MailManager;

public class RTWOpenOrderUnProfitReport extends QuartzJobBean implements StatefulJob{
	
	static MailManager mailManager = null;
	private MailIdProperty mailIdProperty;

	public MailIdProperty getMailIdProperty() {
		return mailIdProperty;
	}

	public void setMailIdProperty(MailIdProperty mailIdProperty) {
		this.mailIdProperty = mailIdProperty;
	}
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		this.mailManager = mailManager;
	}

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		
		System.out.println("RTWOOSU : RTW NON-PROFIT Open Order Email Updater: Job Started....."+new Date());
		try{
			rtwOpenOrderUnProfitEmail();
		}catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("RTWOOSU : RTW NON-PROFIT  Open Order Email Updater: Job Finished....."+new Date());
	}
	
	public void rtwOpenOrderUnProfitEmail() throws Exception{
		
		try {

			SXSSFWorkbook workbook = new SXSSFWorkbook();
			getRtwNegativeProfitOpenOrderDetails(workbook);
			getRtwZeroProfitOpenOrderDetails(workbook);
			
			Sheet ssSheet = workbook.getSheetAt(0);
			if(ssSheet != null && ssSheet.getLastRowNum() >= 1) {
			    ByteArrayOutputStream bos = new ByteArrayOutputStream();
			    try {
			        workbook.write(bos);
			    } finally {
			        bos.close();
			    }
			    byte[] bytes = bos.toByteArray();
			    MailAttachment[] mailAttachments = new MailAttachment[1];
			    mailAttachments[0] = new MailAttachment(bytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "RTWNonProfitOpenOrders.xlsx");
					
			    String resource = "mail-rtw-non-profit-order-details.html";
				String subject = "RTW Non Profit Open Orders.";
				String mimeType = "text/html";
				String toAddress ="leor@rewardthefan.com,amit.raut@rightthisway.com,ezippo@rightthisway.com,bmanusama@rightthisway.com,jreardon@rightthisway.com";
				String fromName = "tmatmanagers@rightthisway.com";
				String cc = null;
				String bccAddress = "AODev@rightthisway.com";
				
				Map<String, Object> map = new HashMap<String, Object>();
				
				mailManager.sendMail(fromName, toAddress, null, bccAddress, subject, resource, map, mimeType, mailAttachments);
			}
				
		} catch (Exception e) {
			e.printStackTrace();
			// send an email with error
			String fromName  = "tmatmanagers@rightthisway.com";
			String mimeType = "text/html";
			String subject = "Error while downloading RTW NON-PROFIT Orders .";
			String template = "mail-rtw-non-profit-order-error.html";
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("error", e);
			String toAddress = "AODev@rightthisway.com";
			mailManager.sendMail(fromName, toAddress, null, null, subject, template, map, mimeType, null);
			throw e;
		}		
	}
	
	public static SXSSFWorkbook getRtwNegativeProfitOpenOrderDetails(SXSSFWorkbook workbook) throws Exception {


		List<OpenOrderStatus> negativeProfitOrders = DAORegistry.getOpenOrderStatusDAO().getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(null, null, null, null, null, ProfitLossSign.NEGATIVE);

		Sheet ssSheet =  workbook.createSheet("RTW_Negative_Profit_Open_Orders");
		int j = 0;
		
		Row rowhead =   ssSheet.createRow((int)0);
		rowhead.createCell((int) j).setCellValue("Broker");
		j++;
		rowhead.createCell((int) j).setCellValue("Invoice No");
		j++;
		rowhead.createCell((int) j).setCellValue("Event Name");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Event Date");
	    j++;
    	rowhead.createCell((int) j).setCellValue("Venue Name");
    	j++;
    	rowhead.createCell((int) j).setCellValue("Zone");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("section");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Row");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Quantity");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Section Tix Count");
	    j++;
 	   	rowhead.createCell((int) j).setCellValue("Zone Tix Count");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("Event Tix Count");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Net Sold Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Market Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Net Total Sold Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("P/L Value");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Section Margin(%)");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone Cheapest Price");
 	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone Total Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone P/L Value");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone Margin(%)");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Actual Sold Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Total Market price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Total Actual Sold Price");
	    j++;
 	    rowhead.createCell((int) j).setCellValue("Zone Tix Group Count");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Invoice Date");
	    j++;
    	rowhead.createCell((int) j).setCellValue("Internal Notes");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Last Updated Price");
	    j++;
	    
	    CellStyle cellamountStyle = workbook.createCellStyle();
	    cellamountStyle.setDataFormat(
	    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
	    
	    DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	    DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    int rowvariable = 1;
	    for (OpenOrderStatus obj : negativeProfitOrders) {
	    	if(null == obj ){
	    		continue;
	    	}
	    	int column = 0;
			Row row = ssSheet.createRow(rowvariable);
			row.createCell((int) column).setCellValue(obj.getBrokerName().toString().replaceAll(",", "-"));//Broker Name
	        column++;
			row.createCell((int) column).setCellValue(obj.getInvoiceNo().toString().replaceAll(",", "-"));//Invoice No
	        column++;
	        row.createCell((int) column).setCellValue(obj.getEventName().toString().replaceAll(",", "-"));//Event Name
	        column++;
	        row.createCell((int) column).setCellValue(obj.getEventDateStr().toString());//Event Date
	        column++;
	    	row.createCell((int) column).setCellValue(obj.getVenueName().toString().replaceAll(",", "-"));//Venue
	    	column++;
	    	Cell shortZoneCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	    	shortZoneCell.setCellValue(obj.getZone().toString().replaceAll(",", "-"));//Zone
	        column++;
	    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	    	shortSectionCell.setCellValue(obj.getSection().toString().replaceAll(",", "-"));//Section
	        column++;
	        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	        shortRowCell.setCellValue(obj.getRow().toString().replaceAll(",", "-"));//row
	        column++;
	    	row.createCell((int) column).setCellValue(obj.getSoldQty().toString().replaceAll(",", "-"));//Quantity
	    	column++;
	    	row.createCell((int) column).setCellValue(obj.getSectionTixQty().toString());//Section Tix Count
	        column++;
	        row.createCell((int) column).setCellValue(obj.getZoneTixQty().toString());//Availabe Zone Tix Count
	        column++;
	        row.createCell((int) column).setCellValue(obj.getEventTixQty().toString());//Event Tix Count
	        column++;
	        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getNetSoldPrice().toString()));//Net Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getMarketPrice().toString()));//Market Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getNetTotalSoldPrice().toString()));//Net Total Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getProfitAndLoss().toString()));//P/L Value
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getSectionMargin().toString()));//Section MArgin
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        Cell zoneCheapestPriceCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        zoneCheapestPriceCell.setCellValue(Double.valueOf(obj.getZoneCheapestPrice().toString()));//Cheapest Zone Price
	        zoneCheapestPriceCell.setCellStyle(cellamountStyle);
	        column++;	        
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getZoneTotalPrice().toString()));//Zone Total Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getZoneProfitAndLoss().toString()));//Zone P/L Value
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getZoneMargin().toString()));//Zone Margin
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getActualSoldPrice().toString()));//Actual Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getTotalMarketPrice().toString()));//Total Market Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getTotalActualSoldPrice().toString()));//Total Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        row.createCell((int) column).setCellValue(obj.getZoneTixGroupCount().toString());//Availabe Zone Tix Group Count
	        column++;
	        row.createCell((int) column).setCellValue(obj.getInvoiceDateStr().toString().replaceAll(",", "-"));//Invoice Date
	        column++;
	    	row.createCell((int) column).setCellValue(obj.getInternalNotes().toString().replaceAll(",", "-"));//Internal Notes
	    	column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getLastUpdatedPrice().toString()));//Last Updated Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        
	        
	        
	       /* String salesDate = obj[15].toString();//Sales Date
	        if(salesDate != null && !salesDate.isEmpty()) {
	        	 row.createCell((int) column).setCellValue(xlDateFormat.format(dbDateTimeFormat.parse(salesDate)));
	        	 column++;
	        } else {
	        	 row.createCell((int) column).setCellValue("");
	        	 column++;
	        }*/

	        
	       
	        rowvariable++;
		}
		return workbook;
	
	}
	
	public static SXSSFWorkbook getRtwZeroProfitOpenOrderDetails(SXSSFWorkbook workbook) throws Exception {


		List<OpenOrderStatus> zeroProfitOrders = DAORegistry.getOpenOrderStatusDAO().getSoldTicketDetailsByBrokerIdAndArtistIdAndEventIdAndBySalesDateRange(null, null, null, null, null, ProfitLossSign.ZERO);

		Sheet ssSheet =  workbook.createSheet("RTW_Zero_Profit_Open_Orders");
		int j = 0;
		
		Row rowhead =   ssSheet.createRow((int)0);
		rowhead.createCell((int) j).setCellValue("Broker");
		j++;
		rowhead.createCell((int) j).setCellValue("Invoice No");
		j++;
		rowhead.createCell((int) j).setCellValue("Event Name");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Event Date");
	    j++;
    	rowhead.createCell((int) j).setCellValue("Venue Name");
    	j++;
    	rowhead.createCell((int) j).setCellValue("Zone");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("section");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Row");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Quantity");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Section Tix Count");
	    j++;
 	   	rowhead.createCell((int) j).setCellValue("Zone Tix Count");
 	    j++;
 	    rowhead.createCell((int) j).setCellValue("Event Tix Count");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Net Sold Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Market Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Net Total Sold Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("P/L Value");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Section Margin(%)");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone Cheapest Price");
 	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone Total Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone P/L Value");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Zone Margin(%)");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Actual Sold Price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Total Market price");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Total Actual Sold Price");
	    j++;
 	    rowhead.createCell((int) j).setCellValue("Zone Tix Group Count");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Invoice Date");
	    j++;
    	rowhead.createCell((int) j).setCellValue("Internal Notes");
	    j++;
	    rowhead.createCell((int) j).setCellValue("Last Updated Price");
	    j++;
	    
	    
	    
	    CellStyle cellamountStyle = workbook.createCellStyle();
	    cellamountStyle.setDataFormat(
	    		workbook.getCreationHelper().createDataFormat().getFormat("#0.00"));
	    
	    DateFormat xlDateFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	    DateFormat dbDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	    int rowvariable = 1;
	    for (OpenOrderStatus obj : zeroProfitOrders) {
	    	if(null == obj ){
	    		continue;
	    	}
	    	int column = 0;
			Row row = ssSheet.createRow(rowvariable);
			row.createCell((int) column).setCellValue(obj.getBrokerName().toString().replaceAll(",", "-"));//Broker Name
	        column++;
			row.createCell((int) column).setCellValue(obj.getInvoiceNo().toString().replaceAll(",", "-"));//Invoice No
	        column++;
	        row.createCell((int) column).setCellValue(obj.getEventName().toString().replaceAll(",", "-"));//Event Name
	        column++;
	        row.createCell((int) column).setCellValue(obj.getEventDateStr().toString());//Event Date
	        column++;
	    	row.createCell((int) column).setCellValue(obj.getVenueName().toString().replaceAll(",", "-"));//Venue
	    	column++;
	    	Cell shortZoneCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	    	shortZoneCell.setCellValue(obj.getZone().toString().replaceAll(",", "-"));//Zone
	        column++;
	    	Cell shortSectionCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	    	shortSectionCell.setCellValue(obj.getSection().toString().replaceAll(",", "-"));//Section
	        column++;
	        Cell shortRowCell = row.createCell(column,Cell.CELL_TYPE_STRING);
	        shortRowCell.setCellValue(obj.getRow().toString().replaceAll(",", "-"));//row
	        column++;
	    	row.createCell((int) column).setCellValue(obj.getSoldQty().toString().replaceAll(",", "-"));//Quantity
	    	column++;
	    	row.createCell((int) column).setCellValue(obj.getSectionTixQty().toString());//Section Tix Count
	        column++;
	        row.createCell((int) column).setCellValue(obj.getZoneTixQty().toString());//Availabe Zone Tix Count
	        column++;
	        row.createCell((int) column).setCellValue(obj.getEventTixQty().toString());//Event Tix Count
	        column++;
	        Cell amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getNetSoldPrice().toString()));//Net Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getMarketPrice().toString()));//Market Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getNetTotalSoldPrice().toString()));//Net Total Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getProfitAndLoss().toString()));//P/L Value
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getSectionMargin().toString()));//Section MArgin
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        Cell zoneCheapestPriceCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        zoneCheapestPriceCell.setCellValue(Double.valueOf(obj.getZoneCheapestPrice().toString()));//Cheapest Zone Price
	        zoneCheapestPriceCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getZoneTotalPrice().toString()));//Zone Total Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getZoneProfitAndLoss().toString()));//Zone P/L Value
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getZoneMargin().toString()));//Zone Margin
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getActualSoldPrice().toString()));//Actual Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getTotalMarketPrice().toString()));//Total Market Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getTotalActualSoldPrice().toString()));//Total Sold Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        row.createCell((int) column).setCellValue(obj.getZoneTixGroupCount().toString());//Availabe Zone Tix Group Count
	        column++;
	        row.createCell((int) column).setCellValue(obj.getInvoiceDateStr().toString().replaceAll(",", "-"));//Invoice Date
	        column++;
	    	row.createCell((int) column).setCellValue(obj.getInternalNotes().toString().replaceAll(",", "-"));//Internal Notes
	    	column++;
	        amountCell = row.createCell(column,Cell.CELL_TYPE_NUMERIC);
	        amountCell.setCellValue(Double.valueOf(obj.getLastUpdatedPrice().toString()));//Last Updated Price
	        amountCell.setCellStyle(cellamountStyle);
	        column++;
	        
	        
	        
	       /* String salesDate = obj[15].toString();//Sales Date
	        if(salesDate != null && !salesDate.isEmpty()) {
	        	 row.createCell((int) column).setCellValue(xlDateFormat.format(dbDateTimeFormat.parse(salesDate)));
	        	 column++;
	        } else {
	        	 row.createCell((int) column).setCellValue("");
	        	 column++;
	        }*/

	        
	       
	        rowvariable++;
		}
		return workbook;
	
	}

}
