package com.admitone.tmat.enums;

public enum UserOutlierStatus {
	ACTIVE, DISABLED, EXPIRED, DELETED
}
