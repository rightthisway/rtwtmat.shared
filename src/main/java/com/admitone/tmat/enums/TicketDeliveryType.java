package com.admitone.tmat.enums;

public enum TicketDeliveryType {
	INSTANT, EDELIVERY, MERCURY, MERCURYPOS, ETICKETS
}
