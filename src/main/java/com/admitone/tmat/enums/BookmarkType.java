package com.admitone.tmat.enums;

public enum BookmarkType {
	ARTIST, TOUR, EVENT, TICKET,VENUE;
}
