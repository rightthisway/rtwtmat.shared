package com.admitone.tmat.enums;

public enum TourStatus {
	ACTIVE, EXPIRED, DELETED
}