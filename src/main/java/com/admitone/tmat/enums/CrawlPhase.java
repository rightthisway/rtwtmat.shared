package com.admitone.tmat.enums;

public enum CrawlPhase {
	STOPPED,DISPATCHED, EXTRACTION, FLUSH, EXPIRATION, REQUEUING
}
