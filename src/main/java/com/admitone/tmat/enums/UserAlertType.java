package com.admitone.tmat.enums;

public enum UserAlertType {
	IN_OUT, IN, OUT
}
