package com.admitone.tmat.enums;

import java.io.Serializable;

public enum TourType implements Serializable {
	SPORT, THEATER, CONCERT, OTHER
}
