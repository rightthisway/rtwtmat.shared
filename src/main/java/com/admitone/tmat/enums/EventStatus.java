package com.admitone.tmat.enums;

public enum EventStatus {
	ACTIVE, EXPIRED, DELETED,DELETEDEXPIRED
}
