package com.admitone.tmat.enums;

public enum ProfitLossSign {
	ALL,POSITIVE,NEGATIVE,ZERO
}
