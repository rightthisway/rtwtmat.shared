package com.admitone.tmat.enums;

public enum ArtistStatus {
	ACTIVE, EXPIRED, DELETED
}
