package com.admitone.tmat.enums;

public enum CrawlState {
	STOPPED, RUNNING, PAUSED, NET_ERROR, NO_TIX, TIMED_OUT, EXCEPTION, RESTRICTED, QUEUED, PERSISTED;
	
	public boolean isError() {
	//	return (this.equals(NET_ERROR) || this.equals(NO_TIX) || this.equals(TIMED_OUT) || this.equals(EXCEPTION));
		return (this.equals(NET_ERROR) || this.equals(TIMED_OUT) || this.equals(EXCEPTION));
	}
}
