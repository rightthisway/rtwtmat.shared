package com.admitone.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.TicketMasterEvent;
import com.admitone.tmat.data.TicketMasterEventsArtist;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;
import com.admitone.tmat.utils.mail.MailManager;

public class TicketMasterEventCreationManager implements InitializingBean {
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(TicketMasterEventCreationManager.class);
	
	static MailManager mailManager = null;
	
	public MailManager getMailManager() {
		return mailManager;
	}

	public void setMailManager(MailManager mailManager) {
		TicketMasterEventCreationManager.mailManager = mailManager;
	}


	public void afterPropertiesSet() throws Exception {
		Timer timer = new Timer();
		Calendar calender = Calendar.getInstance();
		calender.set(Calendar.MINUTE, 8);
		calender.add(Calendar.HOUR, 0);
		timer.schedule(new TimerTask() {			
			@Override
			public void run() {
					try {
						getEventsFromStubhub(null);
					} catch (Exception e) {
						e.printStackTrace();
					}
			}
		}, calender.getTime(), 1000L * 60 * 60); //1 hour
	}
	
	
	public static Collection<TicketMasterEvent> getEventsFromStubhub(SimpleHttpClient httpClient) throws Exception{
		
		if(httpClient == null) {
			httpClient = HttpClientStore.createHttpClient("default");
		}
		int eventsCount=0;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
		Map<Integer,Event> eventMap = new HashMap<Integer, Event>();
		Integer pageNo=0;//2;
		Date startDate = new Date();//new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").parse("09/26/2018 21:00:00");
		
		try {
		
		Date lastProcessedEventdate = null;
		Date lastProcessedEventdateTime = null;
		int duplicateEventCount=0;
		int duplicateEventArtistCount=0;
		
		Collection<TicketMasterEvent> eventsList = DAORegistry.getTicketMasterEventDAO().getAll();
		Map<String,TicketMasterEvent> eventsMapFromDb = new HashMap<String, TicketMasterEvent>();
		for (TicketMasterEvent tmEvent : eventsList) {
			eventsMapFromDb.put(tmEvent.getEventId(), tmEvent);
		}
		Collection<TicketMasterEventsArtist> eventArtistsList = DAORegistry.getTicketMasterEventsArtistDAO().getAll();
		Map<String,Map<String,TicketMasterEventsArtist>> eventArtistsMapFromDb = new HashMap<String, Map<String,TicketMasterEventsArtist>>();
		for (TicketMasterEventsArtist tmEventArtist : eventArtistsList) {
			Map<String,TicketMasterEventsArtist> artistMap = eventArtistsMapFromDb.get(tmEventArtist.getEventId());
			if(artistMap == null) {
				artistMap = new HashMap<String, TicketMasterEventsArtist>();
			}
			artistMap.put(tmEventArtist.getArtistId(), tmEventArtist);
			eventArtistsMapFromDb.put(tmEventArtist.getEventId(),artistMap);
		}
		
		while(true){
			
			Collection<TicketMasterEvent> tmEvents = new ArrayList<TicketMasterEvent>();
			List<TicketMasterEventsArtist> tmArtistList = new ArrayList<TicketMasterEventsArtist>();
			List<TicketMasterEventsArtist> tobeDeletedTmArtistList = new ArrayList<TicketMasterEventsArtist>();
			
			try {
			//http://www.stubhub.com/shape/search/catalog/events/v3?tld=1&geoExpansion=true&status=active+%7Ccontingent&start=0&rows=20&sort=eventDateLocal+asc&spellCheck=true&fieldList=id%2Cname%2CeventDateLocal%2Cvenue%2CeventInfoUrl&q=Mets
			//System.out.println(feedUrl);
			HttpGet httpPost = new HttpGet(
					//"https://app.ticketmaster.com/discovery/v2/attractions.json?apikey=JXVjEnAbwyaYjxkVWiAMiw3cZWAEFVfC&keyword=mets" +
					//"https://app.ticketmaster.com/partners/v1/events/G5dIZfREN6Svi/availability?apikey=GkB8Z037ZfqbLCNtZViAgrEegbsrZ6Ne"+
					//"https://app.ticketmaster.com/commerce/v2/events/G5vVZf9eSwvYs/offers.json?apikey=HhuDAsQGdx50qbUng1IXCgrqGVrxWGGY" +
					
					"https://app.ticketmaster.com/discovery/v2/events.json?apikey=HhuDAsQGdx50qbUng1IXCgrqGVrxWGGY" +
					//"&attractionId=K8vZ9175w1f" +
					
					//"&keyword=mets" +
					"&countryCode=US,CA" +
					"&source=ticketmaster,tmr" +//universe,frontgate,
					"&startDateTime=" +dateTimeFormat.format(startDate)+
					//"&endDateTime=2017-08-20T00:06:00Z" +
					//"&includeTBA=yes" +
					//"&includeTBD=yes" +
					"&sort=date,name,asc" +
					"&size=200" +
					"&page="+pageNo+"" +
					"");
			// System.out.println("stubhub>" + feedUrl);
			httpPost.addHeader("Host", "app.ticketmaster.com");
			//httpPost.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0");
			//httpPost.addHeader("Accept", "application/json, text/javascript, */*; q=0.01");
			//httpPost.addHeader("Accept-Encoding", "gzip, deflate");
			//httpPost.addHeader("Accept-Language", "en-us");
			//httpPost.addHeader("Referer", "http://www.stubhub.com/an-american-in-paris-tempe-tickets-an-american-in-paris-tempe-asu-gammage-4-19-2017/event/9623006/");
			//httpPost.addHeader("Cookie", httpClient.getStubhubCookie());
			
//			httpClient = HttpClientStore.createHttpClient(Site.STUB_HUB);

			String result = null;
			CloseableHttpResponse response = null;
			try{
				httpClient = HttpClientStore.createHttpClient("default");
				response = httpClient.execute(httpPost);
				HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
				//ensure it is fully consumed
				if(entity != null){
					result = EntityUtils.toString(entity).trim();
					//System.out.println(result);
					EntityUtils.consumeQuietly(entity);
				}
			}catch(Exception e){
				e.printStackTrace();
			}finally{
				//to reuse connection we need to close response stream associated with that
				if(response != null)
					response.close();
			}
			
			//System.out.println(result);
			//result  = result.replaceAll("\\s++", " ");
			Date now = new Date();
			JSONObject jsonObject = new JSONObject(result);
			if(jsonObject.has("errors")){
				JSONArray jsonArray = jsonObject.getJSONArray("errors");
				Boolean isError = false;
				for (int i = 0 ; i < jsonArray.length() ; i++) {
					JSONObject errorJson = jsonArray.getJSONObject(i); 
					String errorCode = errorJson.getString("code");
					String errorDetail = errorJson.getString("detail");
					isError = true;
				}
				if (isError) {
					System.out.println("Error : "+jsonObject.toString());
					throw new Exception(jsonObject.toString());
				}
			}
			
			Collection<String> eventIds = new ArrayList<String>();
			JSONObject eventsJson = jsonObject.getJSONObject("_embedded");
			if(eventsJson.has("events")){
				JSONArray docsJSONArray = eventsJson.getJSONArray("events");
				for (int i = 0 ; i < docsJSONArray.length() ; i++) {
					eventsCount++;
					
					TicketMasterEvent tmEvent = new TicketMasterEvent();
					JSONObject eventJSONObject = docsJSONArray.getJSONObject(i);
					
					String eventId = eventJSONObject.getString("id");
					if(eventIds.contains(eventId)) {
						System.out.println("Event Id already Exist : "+eventId);
						duplicateEventCount++;
						continue;
					}
					eventIds.add(eventId);
					tmEvent.setEventId(eventJSONObject.getString("id"));
					tmEvent.setEventName(eventJSONObject.getString("name"));
					tmEvent.setIsTest(eventJSONObject.getBoolean("test"));
					if(eventJSONObject.has("url")) {
						tmEvent.setUrl(eventJSONObject.getString("url"));
					}
					
					JSONObject datJson = eventJSONObject.getJSONObject("dates");
					JSONObject startDateJson = datJson.getJSONObject("start");
					
					if(startDateJson.getBoolean("dateTBD") || startDateJson.getBoolean("dateTBA") ||
							startDateJson.getBoolean("timeTBA") || startDateJson.getBoolean("noSpecificTime")) {
						//System.out.println("eventId : "+eventId+" : localDate : "+ localDate+ " : localTime : "+ localTime + " : localDateTime : "+localDateTime);
						System.out.println("eventId : "+eventId+" : STR : "+ startDateJson);
					}
					
					Date eventDate = null;
					if(startDateJson.has("localDate")) {
						String localDate = startDateJson.getString("localDate");
						if(localDate != null) {
							eventDate = dateFormat.parse(localDate);
							tmEvent.setEventDate(eventDate);
							lastProcessedEventdate = eventDate;
						}
					}
					if(startDateJson.has("localTime")) {
						String localTime = startDateJson.getString("localTime");
						if(localTime != null) {
							tmEvent.setEventTime(timeFormat.parse(localTime));
						}
					}
					if(startDateJson.has("dateTime")) {
						String localDateTime = startDateJson.getString("dateTime");
						if(localDateTime != null) {
							eventDate = dateTimeFormat.parse(localDateTime);
							tmEvent.setEventDateTime(eventDate);
							lastProcessedEventdateTime = eventDate;
						}
					}
					if(eventDate != null && eventDate.before(now)) {
						System.out.println("Event is Expired Event : "+eventId);
						continue;
					}
					if(datJson.has("timezone")) {
						tmEvent.setTimeZone(datJson.getString("timezone"));
					}
					tmEvent.setSpanMultipleDays(datJson.getString("spanMultipleDays"));
					
					
					JSONObject statusJson = datJson.getJSONObject("status");
					tmEvent.setEventStatus(statusJson.getString("code"));
					
					JSONObject embeddedeDocJson = eventJSONObject.getJSONObject("_embedded");
					JSONArray venueArrayJson = embeddedeDocJson.getJSONArray("venues");
					if(venueArrayJson.length() >1) {
						System.out.println("event with multiple venue : "+eventId+" : "+venueArrayJson.length());
					}
					for (int j = 0 ; j < venueArrayJson.length() ; j++) {
						JSONObject venueJSONObject = venueArrayJson.getJSONObject(j);
						
						if(!venueJSONObject.has("name")) {
						System.out.println("name : "+venueJSONObject);	
						}
						tmEvent.setVenueId(venueJSONObject.getString("id"));
						tmEvent.setVenueName(venueJSONObject.getString("name"));
						tmEvent.setIsTestVenue(venueJSONObject.getBoolean("test"));
						tmEvent.setPostalCode(venueJSONObject.getString("postalCode"));
						if(venueJSONObject.has("timezone")) {
							tmEvent.setVenueTimeZone(venueJSONObject.getString("timezone"));	
						}
						
						
						JSONObject cityJson = venueJSONObject.getJSONObject("city");
						tmEvent.setCity(cityJson.getString("name"));
						
						if(venueJSONObject.has("state")) {
							JSONObject stateJson = venueJSONObject.getJSONObject("state");
							tmEvent.setStateCode(stateJson.getString("stateCode"));
							if(stateJson.has("name")) {
								tmEvent.setState(stateJson.getString("name"));	
							} else {
								System.out.println("state Obj : "+eventId+" : state : "+stateJson);
							}
							
						}
						if(venueJSONObject.has("country")) {
							JSONObject countryJson = venueJSONObject.getJSONObject("country");
							tmEvent.setCountryCode(countryJson.getString("countryCode"));
							tmEvent.setCountry(countryJson.getString("name"));
						}
						
						if(venueJSONObject.has("address")) {
							JSONObject addressJson = venueJSONObject.getJSONObject("address");
							if(addressJson.has("line1")) {
								tmEvent.setVenueAddress1(addressJson.getString("line1"));
							}
							
						}
					}
					Map<String, TicketMasterEventsArtist> artistMapFromDb = eventArtistsMapFromDb.get(eventId);
					Map<String,TicketMasterEventsArtist> artistMap = new HashMap<String, TicketMasterEventsArtist>();
					Set<String> artistIdList = new HashSet<String>();
					if(embeddedeDocJson.has("attractions")) {
						JSONArray attractionsArrayJson = embeddedeDocJson.getJSONArray("attractions");
						if(attractionsArrayJson.length() >1) {
							//System.out.println("event with multiple attractions : "+eventId+" : "+attractionsArrayJson.length());
						}
						//List<TicketMasterEventsArtist> tmArtistList = new ArrayList<TicketMasterEventsArtist>(); 
						for (int j = 0 ; j < attractionsArrayJson.length() ; j++) {
							JSONObject attractionsJSONObject = attractionsArrayJson.getJSONObject(j);
							
							TicketMasterEventsArtist tmEventArtist = new TicketMasterEventsArtist();
							tmEventArtist.setEventId(eventId);
							
							String artistId = attractionsJSONObject.getString("id");
							tmEventArtist.setArtistId(artistId);
							if(!artistIdList.add(artistId)) {
								continue;
							}
							
							if(attractionsJSONObject.has("name")) {
								tmEventArtist.setArtistName(attractionsJSONObject.getString("name"));	
							} else {
								System.out.println("Artist Name Null : "+attractionsJSONObject);
							}
							
							tmEventArtist.setIsTest(attractionsJSONObject.getBoolean("test"));
							
							if(attractionsJSONObject.has("classifications")) {
								JSONArray classificationsArrayJson = attractionsJSONObject.getJSONArray("classifications");
								if(classificationsArrayJson.length() >1) {
									//System.out.println("event artist with multiple classificationsArrayJson : "+eventId+" : "+classificationsArrayJson.length());
								}
								for (int k = 0 ; k < classificationsArrayJson.length() ; k++) {
									JSONObject classificationJSONObject = classificationsArrayJson.getJSONObject(k);
									
									Boolean isPrimary = false;
									if(classificationJSONObject.has("primary")) {
										isPrimary = classificationJSONObject.getBoolean("primary");
									}
									if(!isPrimary) {
										//System.out.println("Event with non primary Classification : "+eventId);
										continue;
									}
									
									JSONObject segmentJson = classificationJSONObject.getJSONObject("segment");
									tmEventArtist.setSegmentName(segmentJson.getString("name"));
									tmEventArtist.setSegmentId(segmentJson.getString("id"));
									
									if(classificationJSONObject.has("genre")) {
										JSONObject genreJson = classificationJSONObject.getJSONObject("genre");
										tmEventArtist.setGenreName(genreJson.getString("name"));
										tmEventArtist.setGenreId(genreJson.getString("id"));
									}
									if(classificationJSONObject.has("subGenre")) {
										JSONObject subGenreJson = classificationJSONObject.getJSONObject("subGenre");
										tmEventArtist.setSubGenreName(subGenreJson.getString("name"));
										tmEventArtist.setSubGenreId(subGenreJson.getString("id"));									
									}
									
									if(classificationJSONObject.has("type")) {
										JSONObject typeJson = classificationJSONObject.getJSONObject("type");
										tmEventArtist.setTypeName(typeJson.getString("name"));
										tmEventArtist.setTypeId(typeJson.getString("id"));
									}
									if(classificationJSONObject.has("subType")) {
										JSONObject subTypeJson = classificationJSONObject.getJSONObject("subType");
										tmEventArtist.setSubTypeName(subTypeJson.getString("name"));
										tmEventArtist.setSubTypeId(subTypeJson.getString("id"));
									}
									
									tmEventArtist.setCreationDate(now);
									tmEventArtist.setLastUpdate(now);
								}
							} else {
								//System.out.println("event artist with no clasification : "+eventId+" : "+attractionsJSONObject.toString());
							}
							TicketMasterEventsArtist tmEventArtistDb = artistMapFromDb.remove( tmEventArtist.getArtistId());
							if(tmEventArtistDb != null) {
								if(!tmEventArtistDb.getCompareString().equals(tmEventArtist.getCompareString())) {
									tmEventArtistDb.updateObjectValues(tmEventArtistDb);
									tmEventArtist.setLastUpdate(now);
									
									tmArtistList.add(tmEventArtistDb);
									artistMap.put(tmEventArtistDb.getArtistId(), tmEventArtistDb);
								}
							} else {
								tmArtistList.add(tmEventArtist);
								artistMap.put(tmEventArtist.getArtistId(), tmEventArtist);
							}
							/*if(tmEventArtistDb == null) {
								//tmArtistList.add(tmEventArtist);	
								eventArtistsMapFromDb.put(eventId+"_"+tmEventArtist.getArtistId(), tmEventArtistDb);
							} else {
								System.out.println("Event artist Exist in DB : "+eventId+"_"+tmEventArtist.getArtistId());
								duplicateEventArtistCount++;
							}*/
							
						}
					} else {
						//System.out.println("Event without attraction : "+eventId);
					}
					if(!artistMapFromDb.isEmpty()) {
						tobeDeletedTmArtistList.addAll(artistMapFromDb.values());
					}
					
					if(eventJSONObject.has("classifications")) {
						JSONArray classificationsArrayJson = eventJSONObject.getJSONArray("classifications");
						if(classificationsArrayJson.length() >1) {
							//System.out.println("event with multiple classificationsArrayJson : "+eventId+" : "+classificationsArrayJson.length());
						}
						for (int j = 0 ; j < classificationsArrayJson.length() ; j++) {
							JSONObject classificationJSONObject = classificationsArrayJson.getJSONObject(j);
							Boolean isPrimary = classificationJSONObject.getBoolean("primary");
							if(!isPrimary) {
								System.out.println("Event with non primary Classification : "+eventId);
								continue;
							}
							
							JSONObject segmentJson = classificationJSONObject.getJSONObject("segment");
							tmEvent.setSegmentName(segmentJson.getString("name"));
							tmEvent.setSegmentId(segmentJson.getString("id"));
							
							if(classificationJSONObject.has("genre")) {
								JSONObject genreJson = classificationJSONObject.getJSONObject("genre");
								tmEvent.setGenreName(genreJson.getString("name"));
								tmEvent.setGenreId(genreJson.getString("id"));
							}
							if(classificationJSONObject.has("subGenre")) {
								JSONObject subGenreJson = classificationJSONObject.getJSONObject("subGenre");
								tmEvent.setSubGenreName(subGenreJson.getString("name"));
								tmEvent.setSubGenreId(subGenreJson.getString("id"));
							}
							if(classificationJSONObject.has("type")) {
								JSONObject typeJson = classificationJSONObject.getJSONObject("type");
								tmEvent.setTypeName(typeJson.getString("name"));
								tmEvent.setTypeId(typeJson.getString("id"));	
							}
							if(classificationJSONObject.has("subType")) {
								JSONObject subTypeJson = classificationJSONObject.getJSONObject("subType");
								tmEvent.setSubTypeName(subTypeJson.getString("name"));
								tmEvent.setSubTypeId(subTypeJson.getString("id"));							
							}
						}
					} else {
						//System.out.println("event with no clasification : "+eventId+" : "+eventJSONObject.toString());
					}
					tmEvent.setCreationDate(now);
					tmEvent.setLastUpdate(now);
					tmEvent.setEventStatus("ACTIVE");
					
					TicketMasterEvent tmEventDb = eventsMapFromDb.get(eventId);
					if(tmEventDb != null) {
						if(!tmEventDb.getCompareString().equals(tmEvent.getCompareString())) {
							tmEventDb.updateObjectValues(tmEvent);
							tmEventDb.setLastUpdate(now);
							
							tmEvents.add(tmEventDb);
							eventsMapFromDb.put(eventId, tmEventDb);
						}
					} else {
						tmEvents.add(tmEvent);
						eventsMapFromDb.put(eventId, tmEvent);
					}
					
				}
			}
			DAORegistry.getTicketMasterEventDAO().saveOrUpdateAll(tmEvents);
			DAORegistry.getTicketMasterEventsArtistDAO().saveOrUpdateAll(tmArtistList);
			DAORegistry.getTicketMasterEventsArtistDAO().deleteAll(tobeDeletedTmArtistList);
			
			System.out.println("Event Dup : "+duplicateEventCount+" : art : "+duplicateEventArtistCount+" : page : "+pageNo);
			
			JSONObject pageJson = jsonObject.getJSONObject("page");
			Integer totalEvents = pageJson.getInt("totalElements");
			Integer totalPages = pageJson.getInt("totalPages");
			Integer number = pageJson.getInt("number");
			Integer size = pageJson.getInt("size");
			
			pageNo++;
			if(totalPages<pageNo) {
				//break;
			}
			if(size == 0) {
				break;
			}
			if(pageNo == 4) {
				pageNo = 0;
				if(lastProcessedEventdate.before(lastProcessedEventdateTime)) {
					startDate = lastProcessedEventdateTime;
				} else {
					startDate = lastProcessedEventdate;
				}
			}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("err startDate : "+startDate+" : "+pageNo);
			}
			
			
		}
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("final err startDate : "+startDate+" : "+pageNo);
		}
		
		return null;
	}
	
	
	
	public static void main(String[] args) throws Exception {
		TicketMasterEventCreationManager fetcher = new TicketMasterEventCreationManager();
		Calendar cal1 = new GregorianCalendar(2016, 9, 4);
		Calendar cal2 = new GregorianCalendar(2016, 9, 28);
		List<Event> eventList = new ArrayList<Event>();
		Event e = new Event();
		e.setLocalDate(new Date(new GregorianCalendar(2016, 9, 4).getTimeInMillis()));
		eventList.add(e);
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient("default");
		getEventsFromStubhub(httpClient);
	}
}
