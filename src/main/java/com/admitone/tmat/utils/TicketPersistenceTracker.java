package com.admitone.tmat.utils;

import com.admitone.tmat.enums.CrawlState;
import com.tangosol.net.CacheFactory;

public class TicketPersistenceTracker {
	
	private static TicketPersistenceTracker instance = null;
	
	public static synchronized TicketPersistenceTracker getInstance() {
		if(instance == null){
			instance = new TicketPersistenceTracker();
		}
		return instance;
	}

	public synchronized void setCrawlPersisitenceStatusToQueued(Integer crawlerId){
		try{
			String crawlerStatus = (String)CacheFactory.getCache("crawlerPersistenceStatusCache").get(crawlerId);
			crawlerStatus = "QUEUED";
			CacheFactory.getCache("crawlerPersistenceStatusCache").put(crawlerId, crawlerStatus);
		}catch(Exception e){
			System.out.println("Exception while setting crawl persisitence status to queued");
			e.printStackTrace();
		}
	}
	
	public synchronized void setCrawlPersisitenceStatusToPersisted(Integer crawlerId){
		try{
			String crawlerStatus = (String)CacheFactory.getCache("crawlerPersistenceStatusCache").get(crawlerId);
			crawlerStatus = "PERSISTED";
			CacheFactory.getCache("crawlerPersistenceStatusCache").put(crawlerId, crawlerStatus);
		}catch(Exception e){
			System.out.println("Exception while setting crawl persisitence status to persisted");
			e.printStackTrace();
		}
	}
	
	public synchronized String getCrawlPesistenceStatus(Integer crawlerId){
		try{
			String crawlerStatus = (String)CacheFactory.getCache("crawlerPersistenceStatusCache").get(crawlerId);
			return crawlerStatus;
		}catch(Exception e){
			System.out.println("Exception while getting crawl persisitence status");
			e.printStackTrace();
			return null;
		}
		
	}
	
	public synchronized void setTicketCrawlStatus(Integer crawlerId,CrawlState crawlerStatus){
		try{
//			String crawlerStatus = (String)CacheFactory.getCache("crawlerPersistenceStatusCache").get(crawlerId);
//			crawlerStatus = "PERSISTED";
			CacheFactory.getCache("crawlerPersistenceStatusCache").put(crawlerId, crawlerStatus);
		}catch(Exception e){
			System.err.println("Exception while setting crawl persisitence status to persisted");
			System.err.println(e.fillInStackTrace());
		}
	}
	
	public synchronized CrawlState getTicketCrawlStatus(Integer crawlerId){
		try{
			CrawlState crawlerStatus = (CrawlState)CacheFactory.getCache("crawlerPersistenceStatusCache").get(crawlerId);
			return crawlerStatus;
		}catch(Exception e){
			System.out.println("Exception while getting crawl persisitence status");
			e.printStackTrace();
			return null;
		}
		
	}
	
}
