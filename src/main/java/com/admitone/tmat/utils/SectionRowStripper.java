package com.admitone.tmat.utils;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ConcertSynonym;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.TheaterSynonym;
import com.admitone.tmat.data.Ticket;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.enums.TourType;

public final class SectionRowStripper {
	private static Logger logger = org.slf4j.LoggerFactory.getLogger(SectionRowStripper.class);	
//	private static final Pattern pattern = Pattern.compile("^(?:[^\\d]*[^a-zA-Z0-9])?(\\d+)(?:[^a-zA-Z0-9][^\\d]*)?$");
	private static final Pattern hasNumberPattern = Pattern.compile("^([^\\d]*)([\\d]+)$");
	private static final Pattern hasNumberSpacePattern = Pattern.compile("^([^\\d]*)(\\s+)([\\d]+)$");
	private static final Pattern sportingEventPattern = Pattern.compile("^[^\\d]*([\\d]+.*$)");
	private static final Map<String, String> synonymMap = new HashMap<String, String>();
	
	public static void init() {
	//	synonymMap.clear();
		Collection<TheaterSynonym> tsynonyms = DAORegistry.getTheaterSynonymDAO().getAll();
		Collection<ConcertSynonym> csynonyms = DAORegistry.getConcertSynonymDAO().getAll();
		if (tsynonyms!=null){
			DAORegistry.getTheaterSynonymDAO().deleteAll(tsynonyms);
		}
		if(csynonyms != null){
			DAORegistry.getConcertSynonymDAO().deleteAll(csynonyms);
		}
		for (Synonym synonym: DAORegistry.getSynonymDAO().getAll()) {
			if(synonym.getType().equalsIgnoreCase("Theater")){
				updateTheaterSynonyms(synonym);
			}
			else if(synonym.getType().equalsIgnoreCase("Concert")){
				updateConcertSynonym(synonym);
			}
		}
	/*		for (String value: synonym.getValue().split(",")) {
				value = value.trim().toLowerCase();
				synonymMap.put(value, synonym.getName().toLowerCase().trim());
				
			}
			synonymMap.put(synonym.getName().toLowerCase().trim(), synonym.getName().toLowerCase().trim());
	*/
		}
	
	public static void updateConcertSynonym(Synonym synonym){
		for (String value: synonym.getValue().split(",")) {
			value = value.trim().toLowerCase();
			if (!value.isEmpty()||value!=null){
				ConcertSynonym csynonym = new ConcertSynonym();
				csynonym.setValue(value);
				csynonym.setName(synonym.getName().toLowerCase().trim());
				try{
					DAORegistry.getConcertSynonymDAO().saveOrUpdate(csynonym);
				}catch(Exception e){
					logger.error("The error is: " + e.getMessage());
				}
			}
		}
	}
	
	
	public static void updateTheaterSynonyms(Synonym synonym){
		for (String value: synonym.getValue().split(",")) {
			value = value.trim().toLowerCase();
				if (!value.isEmpty()||value!=null){
				TheaterSynonym tsynonym = new TheaterSynonym();
				tsynonym.setValue(value);
				tsynonym.setName(synonym.getName().toLowerCase().trim());
				try{
					DAORegistry.getTheaterSynonymDAO().saveOrUpdate(tsynonym);
				}catch(Exception e){
					logger.error("The error is: " + e.getMessage());
				}
			}
		}
	}
	
	// concert takes part in a stadium so same rule than sporting event
	public static String stripConcertEvent(String name,Map<String,String> synonymsMap) {
		return stripSportingEvent(name,synonymsMap);
	}

	//if name contains a number, remove text before it
	public static String stripSportingEvent(String name,Map<String,String> synonymsMap) {
		// quick fix to prevent SRO[number] from being scrubbed
		if (name == null) {
			return null;
		}

		name = name.toLowerCase().trim();
		if (name.matches(".*[sS][rR][oO][\\d]+.*")) {
			return name;
		}
		Matcher matcher = sportingEventPattern.matcher(name);
		if (matcher.find()) {
			return matcher.group(1);
		}else{
			String result = "";
			/*ConcertSynonym csynonym = DAORegistry.getConcertSynonymDAO().get(name);
			if(csynonym!=null){ 
				result = csynonym.getName().trim();
			}*/
			
			result = getNormalizedSectionFromSynonyms(name, synonymsMap);
			if (!result.isEmpty()) {
				return result;
			}
		}
		return name;
		}

	// replace synonyms, get rid of other things
	public static String stripTheaterEvent(String name,Map<String,String> synonymsMap) {
		if (name == null) {
			return null;
		}
		name = name.toLowerCase().trim();
		String result = "";
		result = getNormalizedSectionFromSynonyms(name, synonymsMap);
/*		StringTokenizer tokenizer = new StringTokenizer(name);
		while (tokenizer.hasMoreTokens()) {
			String word = tokenizer.nextToken(".,-_");
	
			String word = name;
			String synonym = synonymMap.get(word); */
		
			// Commented out by Chirag on 06/02/2014
			/*TheaterSynonym tsynonym = DAORegistry.getTheaterSynonymDAO().get(name);
			if(tsynonym!=null){ 
				result += tsynonym.getName();
			}*/
		   // Commented till here
		
/*			if (synonym != null) {
				result += synonym;
			} 
			else {
				Matcher matcher = hasNumberPattern.matcher(word);
				Matcher matcher2 = hasNumberSpacePattern.matcher(word);
				if (matcher.find()) {
					String subWord = matcher.group(1);
					String number = matcher.group(2);
					synonym = synonymMap.get(subWord);
					if (synonym != null) {
					//	result += synonym + " " + number;
						result += synonym;
					} else if (subWord.isEmpty()){
						result += number;						
					}
				}
				if (matcher2.find()){
					String subWord = matcher2.group(1);
					String number = matcher2.group(3);
					synonym = synonymMap.get(subWord);
					if (synonym != null) {
					//	result += synonym + " " + number;
						result += synonym;
					} else if (subWord.isEmpty()){
						result += number;						
					}
				}
			}
		} */
		result = result.trim();
		
		if (result.isEmpty()) {
			return name;
		}
		return result.trim();
	}

	public static String stripOtherEvent(String name) {
		if(name.contains("-")){  //Dirty trick to avoid 210-212 OR 222-220 section..
			return name;
		}
		return name.replaceAll("[^\\w\\s]", "");
	}

	public static String strip(TourType type, String name,Map<String,Synonym> synonymsMap) {
		if(name.contains("ZONE")){
			return name;
		}
		
		if(name ==null || name.isEmpty()){
			return "";
		}
		if(synonymsMap==null){
			synonymsMap = new HashMap<String, Synonym>();
		}
//		Map<String,String> concertSynonymsMap = new HashMap<String, String>();
		Map<String,String> synonymsStringMap = new HashMap<String, String>();
		for(Synonym synonym:synonymsMap.values()){
			for(String temp:synonym.getValue().split(",")){
				if(temp.isEmpty()){
					continue;
				}
				synonymsStringMap.put(temp.trim().toUpperCase(), synonym.getName());
			}
			synonymsStringMap.put(synonym.getName().toUpperCase(), synonym.getName());
		}
		if (type == null) {
			return stripOtherEvent(name);			
		}
		
		if (type.equals(TourType.SPORT)) {
			return stripSportingEvent(name,synonymsStringMap);
		}

		if (type.equals(TourType.CONCERT)) {
			return stripConcertEvent(name,synonymsStringMap);
		}
		if (type.equals(TourType.THEATER)) {
			return stripTheaterEvent(name,synonymsStringMap);			
		}

		return stripOtherEvent(name);
	}
	
	/*public static void recomputeTourShortSectionsAndRows(Integer tourId) {
		Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
		Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
		Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
		for(Synonym synonym:synonyms){
			if(synonym.getType().equals("Theater")){
				theaterSynonymMap.put(synonym.getName(), synonym);
			}else{
				concertSynonymMap.put(synonym.getName(), synonym);
			}
		}
		
		try {
			Tour tour = DAORegistry.getTourDAO().get(tourId);
			Map<String,Synonym> synonymMap = null;
			if(tour.getTourType().equals(TourType.THEATER)){
				synonymMap = theaterSynonymMap;
			}else if(tour.getTourType().equals(TourType.CONCERT) || tour.getTourType().equals(TourType.SPORT)){
				synonymMap = concertSynonymMap;
			}
			else{
				synonymMap =  new HashMap<String, Synonym>();
			}
			Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByTour(tourId);
			
			for (Ticket ticket: tickets) {
				ticket.setNormalizedSection(strip(tour.getTourType(), ticket.getSection(),synonymMap));
	//			logger.info("TICKET " + ticket.getNormalizedSection() + " " 
	//					+ ticket.getNormalizedRow() + " " 
	//					+ ticket.getNormalizedSeat());
				DAORegistry.getTicketDAO().update(ticket);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	public static void recomputeEventShortSectionsAndRows(Integer eventId) {
		try {
			Event event = DAORegistry.getEventDAO().get(eventId);
			Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
			Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
			Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
			for(Synonym synonym:synonyms){
				if(synonym.getType().equals("Theater")){
					theaterSynonymMap.put(synonym.getName(), synonym);
				}else{
					concertSynonymMap.put(synonym.getName(), synonym);
				}
			}
			Map<String,Synonym> synonymMap = null;
			if(event.getEventType().equals(TourType.THEATER)){
				synonymMap = theaterSynonymMap;
			}else if(event.getEventType().equals(TourType.CONCERT) || event.getEventType().equals(TourType.SPORT)){
				synonymMap = concertSynonymMap;
			}
			else{
				synonymMap =  new HashMap<String, Synonym>();
			}
			Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
			if(tickets!=null && !tickets.isEmpty()){
				for (Ticket ticket: tickets) {
					ticket.setNormalizedSection(strip(event.getEventType(), ticket.getSection(),synonymMap));
		//			logger.info("TICKET " + ticket.getNormalizedSection() + " " 
		//					+ ticket.getNormalizedRow() + " " 
		//					+ ticket.getNormalizedSeat());
					
				}
				DAORegistry.getTicketDAO().updateAll(tickets);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getNormalizedSectionFromSynonyms(String section,Map<String,String> synonymsMap) {

		section = section.trim();
		String value= synonymsMap.get(section.toUpperCase());
		if(value!=null){
			return value;
		}
		if(section.contains("-")){
			String finalResult="";
			for(String temp:section.split("-")){
				finalResult+=getNormalizedSectionFromSynonyms(temp, synonymsMap)+"-";
			}
			return finalResult.substring(0,finalResult.length()-1);
		}
		Pattern pattern = Pattern.compile("^(\\d+)(\\D+)(\\d+)$"); 
		Pattern pattern1 = Pattern.compile("^(\\D+)(\\d+)(\\D+)$");
		Pattern pattern2 = Pattern.compile("^(\\D+)(\\d+)\\s+(\\d+)$");
		Pattern pattern3 = Pattern.compile("^(\\d+)\\s+(\\d+)(\\D+)$");
		Pattern pattern4 = Pattern.compile("^(\\D+)(\\d+)$");
		Pattern pattern5 = Pattern.compile("^(\\d+\\D+)\\s+(\\D+)$");
		Pattern pattern6 = Pattern.compile("^(\\d+)(\\D+)$");
		Pattern pattern7 = Pattern.compile("^(\\d+)$");
		
		Matcher matcher = pattern.matcher(section);
		Matcher matcher1 = pattern1.matcher(section);
		Matcher matcher2 = pattern2.matcher(section);
		Matcher matcher3 = pattern3.matcher(section);
		Matcher matcher4 = pattern4.matcher(section);
		Matcher matcher5 = pattern5.matcher(section);
		Matcher matcher6 = pattern6.matcher(section);
		Matcher matcher7 = pattern7.matcher(section);
		if(matcher5.find()){
			String result="";
			for(String temp:matcher5.group(1).trim().split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					Pattern pattern8 = Pattern.compile("^(\\d+)(\\D+)$");
					Matcher matcher8 = pattern8.matcher(temp);
					if(matcher8.find()){
						value = synonymsMap.get(matcher8.group(2).trim().toUpperCase());
						if(value==null){
							value=matcher8.group(1).trim();
						}
						result+= Integer.parseInt(matcher8.group(1))+ " " + value;
					}else{
						String val="";
						try{
							val = Integer.parseInt(temp.trim())+"";
						}catch (Exception e) {
							val=temp.trim();
						}
						result+=  val + " ";
					}
				}
			}
			for(String temp:matcher5.group(2).trim().split("\\s+")){
				value= synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					result+= temp + " ";
				}
			}
			return result.trim();
		}else if(matcher.find()){
			String result="";
			for(String temp:matcher.group(2).trim().split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					result+= temp + " ";
				}
			}
			return (Integer.parseInt(matcher.group(1).trim()) + " " + result.trim() + " " + Integer.parseInt(matcher.group(3).trim())).trim();
		}else if(matcher1.find()){
			String result1="";
			for(String temp:matcher1.group(1).trim().split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result1+= value + " ";
				}
				else{
					result1+= temp + " ";
				}
			}
			String space = "";
			String result3="";
			String group3=matcher1.group(3).trim();
			boolean isNumber= false;
			if(group3.trim().equalsIgnoreCase("st") ||group3.trim().equalsIgnoreCase("th") ||group3.trim().equalsIgnoreCase("rd") || group3.trim().equalsIgnoreCase("nd")){
				result3 = matcher1.group(2).trim() + group3;
				value = synonymsMap.get(result3.toUpperCase());
				result3 = value;
				isNumber = true;
			}else{
				for(String temp:matcher1.group(3).split("\\s+")){
					if(temp.trim().isEmpty()){
						space =" ";
					}
					value = synonymsMap.get(temp.toUpperCase());
					if(value!=null){
						result3+= value + " ";
					}
					else{
						result3+= temp + " ";
					}
				}
			}
			if(isNumber){
				return result1.trim() + " " + result3.trim();
			}
			return (result1.trim() + " " + Integer.parseInt(matcher1.group(2).trim())  + space + result3.trim()).trim();
		}else if(matcher2.find()){
			String result="";
			for(String temp:matcher2.group(1).trim().split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					result+= temp + " ";
				}
			}
			return (result.trim()  + " " +  Integer.parseInt(matcher2.group(2).trim()) + " " + Integer.parseInt(matcher2.group(3).trim())).trim();
		}else if(matcher3.find()){
			String result="";
			for(String temp:matcher3.group(3).trim().split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					result+= temp + " ";
				}
			}
			return (Integer.parseInt(matcher3.group(1).trim()) + " " + Integer.parseInt(matcher3.group(2).trim()) + " " + result.trim()).trim() ;
		}else if(matcher4.find()){
			String result="";
			for(String temp:matcher4.group(1).trim().split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					result+= temp + " ";
				}
			}
			return (result.trim() + " " + Integer.parseInt(matcher4.group(2).trim())).trim();
		}else if(matcher6.find()){
			String result="";
			for(String temp:matcher6.group(2).trim().split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					result+= temp + " ";
				}
			}
			return (result.trim() + " " + Integer.parseInt(matcher6.group(1).trim())).trim();
		}else if(matcher7.find()){
			return Integer.parseInt(matcher7.group(1))+"";
		}else{
			String result="";
			for(String temp:section.split("\\s+")){
				value = synonymsMap.get(temp.toUpperCase());
				if(value!=null){
					result+= value + " ";
				}
				else{
					result+= temp + " ";
				}
			}
			return result.trim();
		}
		
	}
	
	public static void main(String[] args) {
//		String test = "10 Orch 100,Right Orchesrtra 10,10 Right Orch 100,100 Right Orchesrtra,10 ORCH100,10ORCH 10,ORCH100";  // Problem
		String test = "ORCH 0100,ORCH0100,0100 ORCH,0100ORCH,0100 ORCH0200,0100 ORCH 0200,0100 0200 ORCH,0100 0200ORCH,ORCH 0200 0100,ORCH0200 0100," +
			 "ORCH Center 0100,ORCH Center 0100,0100 ORCHe  Center,0100ORCH Center,0100 ORCH Center0200,0100 ORCH Center 0200,0100 0200 ORCH,0100 0200ORCH Center,ORCH Center 0200 0100,ORCH Center0200 0100," + 
			 "ORCH Center,ORCH,clb c 0100," +
			 "1ST-BALCONY,first ORCH," +
			 "Stadium I 3 L,ORCH 0100 Center,All-Star Clb," +
			 "Orchestra 01A,Orch 1A,Orch 01 A,ORCH 0100";  

//		Pattern pattern = Pattern.compile("^(\\d+)(\\D+)(\\d+)$"); // 100 level 200
//		Pattern pattern1 = Pattern.compile("^(\\D+)(\\d+)(\\D+)$"); // level 100 floor
//		Pattern pattern2 = Pattern.compile("^(\\D+)(\\d+)\\s+(\\d+)$"); // level 100 200
//		Pattern pattern3 = Pattern.compile("^(\\d+)\\s+(\\d+)(\\D+)$"); // 200 100 level
//		Pattern pattern4 = Pattern.compile("^(\\D+)(\\d+)$"); // level 100
//		Pattern pattern5 = Pattern.compile("^(\\d+)(\\D+)$"); // 100 level
		Map<String, String> map= new HashMap<String, String>();
		Synonym syn = new Synonym("Orchestra","Orchestra,ORC,ORCH");
		Synonym syn1 = new Synonym("Club","clb,club");
		Synonym syn3 = new Synonym("Pavilion","pav,pv");
		Synonym syn2 = new Synonym("first","1st,first");
		for(String temp:syn.getValue().split(",")){
			map.put(temp.toUpperCase(), syn.getName());
		}
		map.put(syn.getName().toUpperCase(), syn.getName());
		
		for(String temp:syn1.getValue().split(",")){
			map.put(temp.toUpperCase(), syn1.getName());
		}
		map.put(syn1.getName().toUpperCase(), syn1.getName());
		
		for(String temp:syn2.getValue().split(",")){
			map.put(temp.toUpperCase(), syn2.getName());
		}
		map.put(syn2.getName().toUpperCase(), syn2.getName());
		
		for(String temp:syn3.getValue().split(",")){
			map.put(temp.toUpperCase(), syn3.getName());
		}
		map.put(syn3.getName().toUpperCase(), syn3.getName());
		
		for(String test1:test.split(",")){
//			Matcher matcher = pattern.matcher(test1);
//			Matcher matcher1 = pattern1.matcher(test1);
//			Matcher matcher2 = pattern2.matcher(test1);
//			Matcher matcher3 = pattern3.matcher(test1);
//			Matcher matcher4 = pattern4.matcher(test1);
//			Matcher matcher5 = pattern5.matcher(test1);
//			if(matcher.find()){
//				System.out.println(test1 + 
////						" -- Found at 0: " + matcher.group(1).trim() + " " + matcher.group(2).trim() + " " + matcher.group(3).trim() + 
//						":::::: " + getNormalizedSectionFromSynonyms(test1, map));
//			}else if(matcher1.find()){
//				System.out.println(test1 + 
////						" -- Found at 1: " + matcher1.group(1).trim() + " " + matcher1.group(2).trim() + " " + matcher1.group(3).trim() + 
//						":::::: " + getNormalizedSectionFromSynonyms(test1, map));
//			}else if(matcher2.find()){
//				System.out.println(test1 + 
////						" -- Found at 2: " + matcher2.group(1).trim() +  " " + matcher2.group(2).trim() + " " + matcher2.group(3).trim() + 
//						":::::: " + getNormalizedSectionFromSynonyms(test1, map));
//			}else if(matcher3.find()){
//				System.out.println(test1 + 
////						" -- Found at 3: " + matcher3.group(1).trim() +  " " + matcher3.group(2).trim() + " " + matcher3.group(3).trim() + 
//						":::::: " + getNormalizedSectionFromSynonyms(test1, map));
//			}else if(matcher4.find()){
//				System.out.println(test1 + 
////						" -- Found at 4: " + matcher4.group(1).trim() +  " " + matcher4.group(2).trim() + 
//						":::::: " + getNormalizedSectionFromSynonyms(test1, map));
//			}else if(matcher5.find()){
				System.out.println(test1 + 
//						" -- Found at 5: " + matcher5.group(2).trim() +  " " + matcher5.group(1).trim() + 
						":::::: " + getNormalizedSectionFromSynonyms(test1, map));
//			}else{
//				System.out.println(test1 + 
////						" -- No luck" + 
//						":::::: " + getNormalizedSectionFromSynonyms(test1, map));
//			}
		}
	}
}