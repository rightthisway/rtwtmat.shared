package com.admitone.tmat.utils;

public class InstantEventManager {
	
//private static Double defaultMarkupPercent = 20.00;
//private static Integer defaultExpiryTime = 24;
//private static Double defaultMinThreshold = 100.00;
//private static Double defaultMaxThreshold = 20000.00;
//private static Double defaultSalesPercent = 0.00;
//private static Double defaultShippingFee = 0.00;

/*public static void saveInstantEvents(Collection<Event> eventList){
		
//	InstantTour instantTour = null;
	InstantEvent instantEvent = null;
	Integer defaultExpiryTime = 0;
	Double defaultMarkupPercent = 0.0;
	Double defaultSalesPercent=0.0;
	Double defaultShippingFee=0.0;
	Double defaultMinThreshold=0.0;
	Double defaultMaxThreshold=0.0;
	Property shExpiryProperty = DAORegistry.getPropertyDAO().get("sh.expiry");
	Property shMarkupProperty = DAORegistry.getPropertyDAO().get("sh.markup.percent");
	Property shSalesProperty = DAORegistry.getPropertyDAO().get("sh.sales");
	Property shShippingProperty = DAORegistry.getPropertyDAO().get("sh.shipping");
	Property shMaxThresholdProperty = DAORegistry.getPropertyDAO().get("sh.maxthreshold");
	Property shMinThresholdProperty = DAORegistry.getPropertyDAO().get("sh.minthreshold");
	String shExpiryStr = shExpiryProperty.getValue();
	try {
		defaultExpiryTime = Integer.valueOf(shExpiryStr);
	}catch(NumberFormatException nfe){}
	
	
	String shMarkupStr = shMarkupProperty.getValue();
	try{
		defaultMarkupPercent = Double.valueOf(shMarkupStr);
	}catch(NumberFormatException nfe){}
	
	
	String shSalesStr = shSalesProperty.getValue();
	try{
		defaultSalesPercent = Double.valueOf(shSalesStr);
	}catch(NumberFormatException nfe){}
	
	
	String shShippingStr = shShippingProperty.getValue();
	try{
		defaultShippingFee = Double.valueOf(shShippingStr);
	}catch(NumberFormatException nfe){}
	String shMinThresholdStr = shMinThresholdProperty.getValue();
	try{
		defaultMinThreshold = Double.valueOf(shMinThresholdStr);
	}catch(NumberFormatException nfe){}
	
	
	String shMaxThresholdStr = shMaxThresholdProperty.getValue();
	try{
		defaultMaxThreshold = Double.valueOf(shMaxThresholdStr);
	}catch(NumberFormatException nfe){}
	
	List<InstantEvent> instantEvents = new ArrayList<InstantEvent>();
	for (Event event : eventList) {
		instantEvent = new InstantEvent(event);
		instantEvent.setMarkupPercent(defaultMarkupPercent);
		instantEvent.setExpiryTime(defaultExpiryTime);
		instantEvent.setMinThreshold(defaultMinThreshold);
		instantEvent.setMaxThreshold(defaultMaxThreshold);
		instantEvent.setSalesPercent(defaultSalesPercent);
		instantEvent.setShippingFee(defaultShippingFee);
		instantEvent.setTour(event.getTour());
		instantEvents.add(instantEvent);
		//	instantEvent.setTourId(event.getTourId());
			
			instantTour = DAORegistry.getInstantTourDAO().get(event.getTourId());
			if (instantTour == null) {
				instantTour = new InstantTour(event.getTourId());
				instantTour.setMarkupPercent(defaultMarkupPercent);
				instantTour.setExpiryTime(defaultExpiryTime);
				instantTour.setMinThreshold(defaultMinThreshold);
				instantTour.setMaxThreshold(defaultMaxThreshold);
				instantTour.setSalesPercent(defaultSalesPercent);
				instantTour.setShippingFee(defaultShippingFee);
				DAORegistry.getInstantTourDAO().save(instantTour);
			}
			else {
				instantEvent.setExpiryTime(instantTour.getExpiryTime());
				instantEvent.setMinThreshold(instantTour.getMinThreshold());
				instantEvent.setMaxThreshold(instantTour.getMaxThreshold());
				instantEvent.setMarkupPercent(instantTour.getMarkupPercent());
				instantEvent.setSalesPercent(instantTour.getSalesPercent());
				instantEvent.setShippingFee(instantTour.getShippingFee());
			}
			
		

	//	getInstantTicketsInventory(eventId);
	}
	DAORegistry.getInstantEventDAO().saveAll(instantEvents);
	}


public static void deleteInstantEvents(Integer eventId){
	Event event= DAORegistry.getEventDAO().get(eventId);
	Integer tourId = event.getTourId();
	DAORegistry.getInstantEventDAO().deleteById(eventId);
	int eventCount = DAORegistry.getInstantEventDAO().getInstantEventCountByTour(tourId);
	if (eventCount ==0){
		DAORegistry.getInstantTourDAO().deleteById(tourId);
	}
	}
	

public static void deleteInstantTour(Integer tourId){
	Collection<InstantEvent> ievents = DAORegistry.getInstantEventDAO().getInstantEventsByTour(tourId);
	for (InstantEvent ievent:ievents){
		DAORegistry.getInstantEventDAO().deleteById(ievent.getEvent().getId());
	}
	int eventCount = DAORegistry.getInstantEventDAO().getInstantEventCountByTour(tourId);
	if (eventCount ==0){
		DAORegistry.getInstantTourDAO().deleteById(tourId);
	}
}

public static void updateInstantEventsandTours(){
	
	Integer defaultExpiryTime = 0;
	Double defaultMarkupPercent = 0.0;
	Double defaultSalesPercent=0.0;
	Double defaultShippingFee=0.0;
	Double defaultMinThreshold=0.0;
	Double defaultMaxThreshold=0.0;
	
	Collection<InstantEvent> iEvents = DAORegistry.getInstantEventDAO().getAll();
	Collection<InstantTour> iTours = DAORegistry.getInstantTourDAO().getAll();
	
	Property shExpiryProperty = DAORegistry.getPropertyDAO().get("sh.expiry");
	String shExpiryStr = shExpiryProperty.getValue();
	
	try {
		defaultExpiryTime = Integer.valueOf(shExpiryStr);
	}catch(NumberFormatException nfe){}
	
	Property shMarkupProperty = DAORegistry.getPropertyDAO().get("sh.markup.percent");
	String shMarkupStr = shMarkupProperty.getValue();
	try{
		defaultMarkupPercent = Double.valueOf(shMarkupStr);
	}catch(NumberFormatException nfe){}
	
	Property shSalesProperty = DAORegistry.getPropertyDAO().get("sh.sales");
	String shSalesStr = shSalesProperty.getValue();
	try{
		defaultSalesPercent = Double.valueOf(shSalesStr);
	}catch(NumberFormatException nfe){}
	
	Property shShippingProperty = DAORegistry.getPropertyDAO().get("sh.shipping");
	String shShippingStr = shShippingProperty.getValue();
	try{
		defaultShippingFee = Double.valueOf(shShippingStr);
	}catch(NumberFormatException nfe){}
	
	Property shMinThresholdProperty = DAORegistry.getPropertyDAO().get("sh.minthreshold");
	String shMinThresholdStr = shMinThresholdProperty.getValue();
	try{
		defaultMinThreshold = Double.valueOf(shMinThresholdStr);
	}catch(NumberFormatException nfe){}
	
	Property shMaxThresholdProperty = DAORegistry.getPropertyDAO().get("sh.maxthreshold");
	String shMaxThresholdStr = shMaxThresholdProperty.getValue();
	try{
		defaultMaxThreshold = Double.valueOf(shMaxThresholdStr);
	}catch(NumberFormatException nfe){}
	
	for(InstantEvent instantEvent:iEvents){
	instantEvent.setMarkupPercent(defaultMarkupPercent);
	instantEvent.setExpiryTime(defaultExpiryTime);
	instantEvent.setMinThreshold(defaultMinThreshold);
	instantEvent.setMaxThreshold(defaultMaxThreshold);
	instantEvent.setSalesPercent(defaultSalesPercent);
	instantEvent.setShippingFee(defaultShippingFee);
	}
	DAORegistry.getInstantEventDAO().saveOrUpdateAll(iEvents);
	
	for(InstantTour instantTour: iTours){
	instantTour.setMarkupPercent(defaultMarkupPercent);
	instantTour.setExpiryTime(defaultExpiryTime);
	instantTour.setMinThreshold(defaultMinThreshold);
	instantTour.setMaxThreshold(defaultMaxThreshold);
	instantTour.setSalesPercent(defaultSalesPercent);
	instantTour.setShippingFee(defaultShippingFee);
	}
	DAORegistry.getInstantTourDAO().saveOrUpdateAll(iTours);
}
*/
}

/*
public static Collection<Ticket> getInstantTicketsInventory(Integer eventId) {
	
			Collection<Ticket> filteredInstantTickets = new ArrayList<Ticket>();
		
				Event event = DAORegistry.getEventDAO().get(eventId);
//				if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
//					if (event.getDate().getTime()- new Date().getTime()>60L*60L*60L*1000L){
			//	Collection<Ticket> instantTickets = DAORegistry.getInstantTicketsQueryManager().getInstantTickets(eventId);
				Collection<Ticket> instantTickets = DAORegistry.getTicketDAO().getAllInstantTickets(eventId, TicketDeliveryType.INSTANT, TicketStatus.ACTIVE);
				List<String> catScheme = Categorizer.getCategoryGroupsByEvent(eventId);
		//		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		//		List<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
		//		tmpFilteredTickets = TicketUtil.removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		//		Collection<AdmitoneInventory> aotickets = DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByEventId(eventId);
				for (String catscheme : catScheme) {
					Collection<Category> categories = DAORegistry.getCategoryDAO()
					.getAllCategories(event.getTourId(), catscheme);
					Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
					for (Category category : categories) {
						categoryMap.put(category.getId(), category);
					}
		//			for (Ticket ticket:tmpFilteredTickets){	
						for (Ticket instantTicket : instantTickets) {
						//	if (instantTicket.getDupRefTicketId()==null){
		//					if(!TicketUtil.isInInventory(instantTicket, aotickets)){
		//						if (ticket.getId().equals(instantTicket.getTicketId())){
									int quantity = instantTicket.getRemainingQuantity();
									double price = instantTicket.getCurrentPrice()*1.2;
									if(quantity*price>100 && price<20000){
										Integer catId = Categorizer.computeCategory(eventId, instantTicket.getNormalizedSection(),instantTicket.getRow(), catscheme);	
										if (catId != null) {
									//		instantTicket.setCategory(categoryMap.get(catId));
											filteredInstantTickets.add(instantTicket);
										//	filteredUniqueInstantTickets = getUniqueValues (filteredInstantTickets);
										}
									}
								}
							//}
						}
			//		}
//				}
//		}	
//			}	
		//	System.out.println("Instant tix filtered coll size" +filteredInstantTickets.size());
			return filteredInstantTickets;
		//	return filteredUniqueInstantTickets;
		}*/

