package com.admitone.tmat.utils.eimarketplace;

public interface EIMPCredential {
	String getUsername();
	String getPassword();
}
