package com.admitone.tmat.utils.eimarketplace;

public class EIMPSimpleCredential implements EIMPCredential {
	private String username;
	private String password;
	
	public EIMPSimpleCredential(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public String getPassword() {
		return password;
	}

}
