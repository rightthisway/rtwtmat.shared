package com.admitone.tmat.utils.eimarketplace;

import java.net.URI;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EIMPRedirectHandler implements RedirectHandler {
	private Logger logger = LoggerFactory.getLogger(EIMPRedirectHandler.class);
	
	private String redirectUrl;

	public EIMPRedirectHandler() {
	}

	public URI getLocationURI(HttpResponse response, HttpContext context)
			throws ProtocolException {
		if (redirectUrl == null) {
			return null;
		}
		
		// sometime we get /brokers/pubsearch.cfm?e=444&v=521&s=1&d={ts '2009-12-04 19:30:00'}
		// need to escape the special  characters.
		redirectUrl = redirectUrl
				.replace("{", "%7B")
				.replace("}", "%7D")
				.replace("'", "%27")
				.replace(" ", "+");
		
		logger.info("REDIRECTURL=" + redirectUrl);		

		if (redirectUrl.startsWith("http")) {
			return URI.create(redirectUrl);
		} else {
			return URI.create("https://www.eimarketplace.com" + redirectUrl);
		}
	}

	public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
		Header[] headers = response.getAllHeaders();
		for (Header header: headers) {
			if (header.getName().equalsIgnoreCase("Location")) {
				redirectUrl = header.getValue();
				return true;
			}
		}
		return false;
	}

	public String getRedirectUrl() {
		return redirectUrl;
	}

	public void reset() {
		redirectUrl = null;
	}
}
