package com.admitone.tmat.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.pojo.AOZoneTickets;

public final class AOTicketUtil {
	
	public static AOZoneTickets computeAOZoneTickets(Event event){
		
		String DATE_FORMAT = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		String TIME_FORMAT = "HH:mm";
		SimpleDateFormat stf = new SimpleDateFormat(TIME_FORMAT);
//		Event event = DAORegistry.getEventDAO().get(eventId);    
		if(event.getAdmitoneId()== null){
			return null;
		}
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
		TicketUtil.computeAdmitOneTickets(tickets,null);
		tickets =  TicketUtil.removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		String scheme = "";//Categorizer.getCategoryGroupsByEvent(event.getId()).get(0);
		if(event.getVenueCategory()!=null){
			scheme = event.getVenueCategory().getCategoryGroup();
		}
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), scheme);
		Collection<Category> categories = null;
		if(event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		}else{
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), scheme);
		}
		TicketUtil.preAssignCategoriesToTickets(tickets, categories);
		List<AOZoneTickets> aoZoneTickets = null;
		
		
		List<Ticket> aoTickets= new ArrayList<Ticket>();
		HashMap<String,List<Ticket>> nonAOTickets = new HashMap<String, List<Ticket>>();
		for(Ticket ticket: tickets){
			if(ticket.getCategory() != null){
				if(ticket.isAdmitOneNonTMTicket() || ticket.isAdmitOneTicket()){
					aoTickets.add(ticket);
				}else{
					if(nonAOTickets.get(ticket.getCategory().getSymbol()) !=null){
						List<Ticket> nonAOTicketsList = nonAOTickets.get(ticket.getCategory().getSymbol());
						nonAOTicketsList.add(ticket);
					}else {
						List<Ticket> nonAOTicketsList = new ArrayList<Ticket>();
						nonAOTicketsList.add(ticket);
			    		nonAOTickets.put(ticket.getCategory().getSymbol(),nonAOTicketsList);
					}
				}
			}
		}
		aoZoneTickets = calculateAOZone(nonAOTickets,aoTickets);
		if(aoZoneTickets != null)
		{
			AOZoneTickets aoZoneTicket = new AOZoneTickets();
			aoZoneTicket.setEventName(event.getName());
			String dateTime ="TBD";
			if(event.getDate()==null){
				dateTime=event.getDate().toString();
			}
			aoZoneTicket.setEventDate(dateTime);
			aoZoneTicket.setVenue(event.getVenue().getLocation());
			aoZoneTicket.setEventid(event.getId());
			aoZoneTicket.setChildren(aoZoneTickets);
			aoZoneTicket.setTime(stf.format(event.getLocalDateTime()));
			aoZoneTicket.setDate(sdf.format(event.getLocalDateTime()));
			return aoZoneTicket;
		}else
			return null;
		
		
	}

		//replica of computeAOZoneTickets
		public static AOZoneTickets computeAOZoneInventoryTickets(Event event){
		
		String DATE_FORMAT = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		String TIME_FORMAT = "HH:mm";
		SimpleDateFormat stf = new SimpleDateFormat(TIME_FORMAT);
//		Event event = DAORegistry.getEventDAO().get(eventId);    
		if(event.getAdmitoneId()== null){
			return null;
		}
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
		TicketUtil.computeAdmitOneInventoryTickets(tickets,null);
		tickets =  TicketUtil.removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		String scheme = "";//Categorizer.getCategoryGroupsByEvent(event.getId()).get(0);
		if(event.getVenueCategory()!=null){
			scheme = event.getVenueCategory().getCategoryGroup();
		}
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), scheme);
		Collection<Category> categories = null;
		if(event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		}else{
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), scheme);
		}
		TicketUtil.preAssignCategoriesToTickets(tickets, categories);
		List<AOZoneTickets> aoZoneTickets = null;
		
		
		List<Ticket> aoTickets= new ArrayList<Ticket>();
		HashMap<String,List<Ticket>> nonAOTickets = new HashMap<String, List<Ticket>>();
		for(Ticket ticket: tickets){
			if(ticket.getCategory() != null){
				if(ticket.isAdmitOneNonTMTicket() || ticket.isAdmitOneTicket()){
					aoTickets.add(ticket);
				}else{
					if(nonAOTickets.get(ticket.getCategory().getSymbol()) !=null){
						List<Ticket> nonAOTicketsList = nonAOTickets.get(ticket.getCategory().getSymbol());
						nonAOTicketsList.add(ticket);
					}else {
						List<Ticket> nonAOTicketsList = new ArrayList<Ticket>();
						nonAOTicketsList.add(ticket);
			    		nonAOTickets.put(ticket.getCategory().getSymbol(),nonAOTicketsList);
					}
				}
			}
		}
		aoZoneTickets = calculateAOZone(nonAOTickets,aoTickets);
		if(aoZoneTickets != null)
		{
			AOZoneTickets aoZoneTicket = new AOZoneTickets();
			aoZoneTicket.setEventName(event.getName());
			String dateTime ="TBD";
			if(event.getDate()==null){
				dateTime=event.getDate().toString();
			}
			aoZoneTicket.setEventDate(dateTime);
			aoZoneTicket.setVenue(event.getVenue().getLocation());
			aoZoneTicket.setEventid(event.getId());
			aoZoneTicket.setChildren(aoZoneTickets);
			aoZoneTicket.setTime(stf.format(event.getLocalDateTime()));
			aoZoneTicket.setDate(sdf.format(event.getLocalDateTime()));
			return aoZoneTicket;
		}else
			return null;
		
		
	}
	
	
	private  static List<AOZoneTickets> calculateAOZone(HashMap<String,List<Ticket>> nonAOTickets,List<Ticket> aoTickets)
	{
		List<AOZoneTickets> aoZoneTickets = new ArrayList<AOZoneTickets>();
		for(Ticket ticket: aoTickets)
		{
			if(ticket.getSection().toLowerCase().contains("zone"))
			{
				AOZoneTickets aoZoneTicket = new AOZoneTickets();
				aoZoneTicket.setSection(ticket.getSection());
				Integer tixInZone= 0; 
				Double zoneMarketPrice = ticket.getCurrentPrice();
				Integer tixBelowAOZone = 0;
				Double PGM = new Double(0);
				List<Ticket> nonAOTicketList = nonAOTickets.get(ticket.getCategory().getSymbol());
				Double lowESL =new Double(0);
				boolean aoInfusedTix = false;
				if(nonAOTicketList != null)
				{
					for(Ticket nonAOTicket: nonAOTicketList)
					{
						if(nonAOTicket.isAdmitOneNonTMTicket() || ticket.isAdmitOneNonTMTicket())
							aoInfusedTix = true;
						tixInZone = tixInZone + nonAOTicket.getRemainingQuantity();
						if(!nonAOTicket.getSection().contains("ZONE") && nonAOTicket.getRemainingQuantity() >1)
						{
							if((lowESL >0 && nonAOTicket.getCurrentPrice() < lowESL) || (lowESL ==0))
								lowESL = nonAOTicket.getCurrentPrice();
						}
						if((nonAOTicket.getCurrentPrice()< zoneMarketPrice) && nonAOTicket.getSection().contains("ZONE"))
						{
							zoneMarketPrice = nonAOTicket.getCurrentPrice();
						}
						if(nonAOTicket.getCurrentPrice()<= ticket.getCurrentPrice())
						{
							tixBelowAOZone = tixBelowAOZone+nonAOTicket.getRemainingQuantity();
						}
					}
				}
				if(!aoInfusedTix)
				{
					tixInZone = tixInZone + ticket.getRemainingQuantity();
				}
				int noOfDays = (int)((ticket.getEvent().getDate().getTime() - new Date().getTime())/ (1000* 60*60*24) );
				aoZoneTicket.setTixInZone(tixInZone);
				aoZoneTicket.setBroadcastPrice(ticket.getCurrentPrice());
				aoZoneTicket.setQuantity(ticket.getRemainingQuantity());
				aoZoneTicket.setMarketPrice(zoneMarketPrice);
				aoZoneTicket.setTixBelowAOZone(tixBelowAOZone);
				if(!aoInfusedTix)
				{
					aoZoneTicket.setMarketPercentage(Math.round((ticket.getRemainingQuantity().floatValue()/(tixInZone.floatValue()))*100));
					aoZoneTicket.setMarketPerBelowAO(Math.round((tixBelowAOZone.floatValue()/(tixInZone.floatValue()))* 100));
				}
				if(ticket.getCurrentPrice() > zoneMarketPrice)
					PGM = ticket.getCurrentPrice() -  zoneMarketPrice;
				aoZoneTicket.setPGM(PGM);
				aoZoneTicket.setDaysOfExpiration(new Long(noOfDays));
				aoZoneTicket.setLowESL(lowESL);
				aoZoneTicket.setAoInfusedTix(aoInfusedTix);
				aoZoneTicket.setCategory(ticket.getCategory().getDescription());
				aoZoneTickets.add(aoZoneTicket);
			}
		}
		//Collections.sort(aoZoneTickets, new Comparator<AOZoneTickets>() {
		
			try {
					aoZoneTickets.sort(new Comparator<AOZoneTickets>() {
					@Override
					public int compare(AOZoneTickets ticket1, AOZoneTickets ticket2) {
						if(ticket2.getSection().toLowerCase().contains("premium")){
							return 1;
						}
						if(ticket2.getSection().compareTo(ticket1.getSection()) > 0){
							return -1;
						}else if(ticket2.getSection().compareTo(ticket1.getSection()) == 0){
							return ticket1.getSection().compareTo(ticket2.getSection());					
						}else{
							return 1;
						}
					}
				});
		}catch(Exception ex) {
			System.out.println("Exception sorting in AOTicketutil " + ex);
		}
		return aoZoneTickets;
	}
}
