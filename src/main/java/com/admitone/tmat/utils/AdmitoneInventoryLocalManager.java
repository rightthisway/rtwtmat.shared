package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneInventoryLocal;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ShortTransaction;
//import com.admitone.tmat.data.Tour;

/**
 * Admitone InventoryLocal Manager.
 * 
 *  TODO:
 *  - make direct access to database instead of putting that in memory.
 */
public class AdmitoneInventoryLocalManager {
	
	protected static AdmitoneInventoryLocalManager instance = null;

	private final Logger log = LoggerFactory.getLogger(AdmitoneInventoryLocalManager.class);
	
	// every 10 mn
	protected static final int DEFAULT_INV_TIMER_WAIT = 10 * 60 * 1000;
		
	/* Map<AOneEventId, AdmitoneInventoryLocal> */
	
	// admit one localEvent id => list of inventories
	private Map<Integer, List<AdmitoneInventoryLocal>> localTicketsByAdmitoneId = new HashMap<Integer, List<AdmitoneInventoryLocal>>();

	// admit one localEvent id + qty => list of inventories
	private Map<String, List<AdmitoneInventoryLocal>> localTicketsByAdmitoneIdQty = new HashMap<String, List<AdmitoneInventoryLocal>>();
	
	// tmat localEvent id => list of inventories
	private Map<Integer, List<AdmitoneInventoryLocal>> localTicketsByEventId = new HashMap<Integer, List<AdmitoneInventoryLocal>>();
	
	// admit one localEvent id => tmat localEvent id
	private Map<Integer, Integer> localEventByAdmitoneId = new HashMap<Integer, Integer>();
	
	// tmat localEvent id => admitone localEvent id
	private Map<Integer, Integer> admitoneIdByEventId = new HashMap<Integer, Integer>();
	
	// id of localEvents which has inventoryLocal
	private HashSet<Integer> localEventIdsWithInventoryLocal = new HashSet<Integer>();
	
	private AdmitoneInventoryLocalManager() {
		Timer timer = new Timer();
		//run every 15 minutes by default
		timer.scheduleAtFixedRate(
				new TimerTask() {
					@Override
					public void run(){
						try {
							System.out.println(";;;;;;;;;;START TO LOAD ADMITONE INVENTORY LOCAL " + new Date());
							instance.reinit();
							System.out.println(";;;;;;;;;;FINISH TO LOAD ADMITONE INVENTORY LOCAL " + new Date());
						} catch (Exception e) {
							System.out.println(";;;;;;;;;;ERROR WHILE LOADING ADMITONEINVENTORY LOCAL " + new Date());
							e.printStackTrace();
						}
					}
				}, new Date(), DEFAULT_INV_TIMER_WAIT);
	}

	public static AdmitoneInventoryLocalManager getInstance() {
		if (instance == null) {
			//instance = new AdmitoneInventoryLocalManager();
		}
		return instance;		
	}

	/**
	 * addInventoryLocal
	 * 
	 * This method will add the parameter newInventoryLocal to Memory ONLY
	 * 
	 * @param newInventoryLocal - inventoryLocal to add
	 * @return void
	 */
	private void addInventoryLocal(AdmitoneInventoryLocal newInventoryLocal) {
		//log.info("adding InventoryLocal: " + newInventoryLocal.toString());
		List<AdmitoneInventoryLocal> inventories = localTicketsByAdmitoneId.get(newInventoryLocal.getEventId());
		
		if(inventories == null){
			inventories = new ArrayList<AdmitoneInventoryLocal>();
			localTicketsByAdmitoneId.put(newInventoryLocal.getEventId(), inventories);
		} 
		
		inventories.add(newInventoryLocal);

		String key = newInventoryLocal.getEventId() + "-" + newInventoryLocal.getQuantity();

		List<AdmitoneInventoryLocal> inventories2 = localTicketsByAdmitoneIdQty.get(key);
		if (inventories2 == null) {
			inventories2 = new ArrayList<AdmitoneInventoryLocal>();
			localTicketsByAdmitoneIdQty.put(key, inventories2);
		}
		inventories2.add(newInventoryLocal);
		
		Integer localEventId = this.getEventIdByAdmitoneId(newInventoryLocal.getEventId());


		if(localEventId != null){
			localEventIdsWithInventoryLocal.add(localEventId);
			
			List<AdmitoneInventoryLocal> inventories3 = localTicketsByEventId.get(localEventId);
			if(inventories3 == null){
				inventories3 = new ArrayList<AdmitoneInventoryLocal>();
				localTicketsByEventId.put(localEventId, inventories3);
			} 
			inventories3.add(newInventoryLocal);
		}
	}

	/**
	 * getInventoryLocalByAdmitoneEvent
	 * 
	 * @param localEventId - Admitone Integer Id to get inventoryLocal for
	 * @return ArrayList<AdmitoneInventoryLocal> - all A1 InventoryLocal for this localEvent
	 */
	public List<AdmitoneInventoryLocal> getInventoryLocalByAdmitoneEvent(Integer localEventId) {
		return localTicketsByAdmitoneId.get(localEventId);
	}
	
	/**
	 * getInventoryLocalByAdmitoneEvent
	 * 
	 * @param localEventId - Admitone Integer Id to get inventoryLocal for
	 * @return ArrayList<AdmitoneInventoryLocal> - all A1 InventoryLocal for this localEvent
	 */
	public List<AdmitoneInventoryLocal> getInventoryLocalByAdmitoneEventQuantity(Integer localEventId, Integer quantity) {
		return localTicketsByAdmitoneIdQty.get(localEventId + "-" + quantity);
	}


	public List<AdmitoneInventoryLocal> getInventoryLocalByEvent(Integer localEventId) {
		/*if(admitoneIdByEventId.get(localEventId) == null){
			return new ArrayList<AdmitoneInventoryLocal>();
		}*/
		
		List<AdmitoneInventoryLocal> result = localTicketsByAdmitoneId.get(localEventId);
		if (result == null) {
			return new ArrayList<AdmitoneInventoryLocal>();
		}
		
		return result;
	}

	/**
	 * getAllInventoryLocalEventIds
	 * 
	 * @return Set<Integer> - all of the localEventIds that have AdmitoneInventoryLocal
	 */
	public Set<Integer> getAllInventoryLocalEventIds() {
		return admitoneIdByEventId.keySet();
	}
	
	/**
	 * getAllInventoryLocalAOneIds
	 * 
	 * @return Set<Integer> - all of the localEventIds that have AdmitoneInventoryLocal
	 */
	public Set<Integer> getAllInventoryLocalAOneIds() {
		return localTicketsByAdmitoneId.keySet();
	}

	/**
	 * getAllInventoryLocalAOneIds
	 * 
	 * @return Set<Integer> - all of the localEventIds that have AdmitoneInventoryLocal AND cost is not 0.00
	 */
	public Set<Integer> getAllTransactionInventoryLocalEventIds() {
		return localEventIdsWithInventoryLocal;
	}

	/**
	 * getEventIdByAdmitoneId
	 * 
	 * @param localEventId - Admitone Integer Id to get inventoryLocal for
	 * @return ArrayList<AdmitoneInventoryLocal> - all A1 InventoryLocal for this localEvent
	 */
	public Integer getEventIdByAdmitoneId(Integer localEventId) {
		return localEventByAdmitoneId.get(localEventId);
	}
	
	public Integer getAdmitoneIdByEventId(Integer localEventId) {
		return admitoneIdByEventId.get(localEventId);
	}

	protected void reinit(){
		System.out.println("REINIT ADMITONE INVENTORY LOCAL");
		Collection<Integer> admitoneIds = DAORegistry.getAdmitoneEventDAO().getAllEventIds();
		// if it is null, that means that the replication is going on and during that time
		// all the inventoryLocal records are removed from DB.
		// skip it for now and will update the inventoryLocal in memory later.
		if (admitoneIds == null || admitoneIds.isEmpty()) {
			System.out.println("Admit One Event IDS is empty");
			return;
		}
		
		Collection<AdmitoneInventoryLocal> inventoryLocal = DAORegistry.getAdmitoneInventoryLocalDAO().getAll();
		if (inventoryLocal == null || inventoryLocal.isEmpty()) {
			System.out.println("Admit One inventoryLocal is empty");
			return;
		}
		
		localTicketsByAdmitoneId = new HashMap<Integer, List<AdmitoneInventoryLocal>>();
		localTicketsByAdmitoneIdQty = new HashMap<String, List<AdmitoneInventoryLocal>>();
		localTicketsByEventId = new HashMap<Integer, List<AdmitoneInventoryLocal>>();
		localEventByAdmitoneId = new HashMap<Integer, Integer>();
		admitoneIdByEventId = new HashMap<Integer, Integer>();
		localEventIdsWithInventoryLocal = new HashSet<Integer>();

		Set<Integer> admitoneEventIds = new HashSet<Integer>(admitoneIds); 			
		
		Collection<Event> localEvents = DAORegistry.getEventDAO().getAllActiveEvents();
		for(Event localEvent: localEvents) {
			Integer admitoneEventId = localEvent.getAdmitoneId();
			if(admitoneEventId != null && admitoneEventIds.contains(admitoneEventId)){
				localEventByAdmitoneId.put(admitoneEventId, localEvent.getId());
				admitoneIdByEventId.put(localEvent.getId(), admitoneEventId);
			}
		}

		for(AdmitoneInventoryLocal inv : inventoryLocal){
			this.addInventoryLocal(inv);
		}	
//		System.out.println("Finished adding inventoryLocal: " + localEventByAdmitoneId);
	}
	
	/*public List<Integer> getTourAdmitoneInventoryLocalEventIds(Tour tour) {
		Collection<Event> localEvents = tour.getEvents();
		List<Integer> returnIds = new ArrayList<Integer>();
		for(Event localEvent : localEvents){
			if(localEventIdsWithInventoryLocal.contains(localEvent.getId())){
				returnIds.add(localEvent.getId());
			}
		}
		return returnIds;
	}*/

	@Deprecated
	public Collection<ShortTransaction> getShortsByEvent(Integer localEventId) {
		Collection<ShortTransaction> shortTransactions = new ArrayList<ShortTransaction>();
		
		Collection<AdmitoneInventoryLocal> inventoryLocal = getInventoryLocalByEvent(localEventId);
		System.out.println(">>>>>>>>> INVENTORY LOCAL=" + inventoryLocal);
		if(inventoryLocal == null || inventoryLocal.isEmpty()){
			inventoryLocal = getInventoryLocalByAdmitoneEvent(localEventId);
		} 
		if(inventoryLocal != null){				
			for(AdmitoneInventoryLocal inv : inventoryLocal){
				if(inv.getCost() != null && inv.getCost() > 0.00) {						
					ShortTransaction longTransaction = new ShortTransaction(inv);
					shortTransactions.add(longTransaction);
				}
			}
		}
		return shortTransactions;
	}
}
