package com.admitone.tmat.utils;

public class TNEventManager {
//	private static Double defaultMarkupPercent = 30.00;
//	private static Integer defaultExpiryTime = 60;
//	private static Double defaultMinThreshold = 1000.00;
//	private static Double defaultMaxThreshold = 200000.00;
//	private static Double defaultSalesPercent = 80.00;
//	private static Double defaultShippingFee = 90.00;

	/*public static void saveTNInstantEvents(Collection<Event> events){
			
//		TNInstantTour instantTour = null;
		TNInstantEvent instantEvent = null;
		
		Integer defaultExpiryTime = 0;
		Double defaultMarkupPercent = 0.0;
		Double defaultSalesPercent=0.0;
		Double defaultShippingFee=0.0;
		Double defaultMinThreshold=0.0;
		Double defaultMaxThreshold=0.0;
		List<TNInstantEvent> instantEvents = new ArrayList<TNInstantEvent>();
		
		Property tnExpiryProperty = DAORegistry.getPropertyDAO().get("tn.expiry");
		String tnExpiryStr = tnExpiryProperty.getValue();
		try {
			defaultExpiryTime = Integer.valueOf(tnExpiryStr);
		}catch(NumberFormatException nfe){}
		
		Property tnMarkupProperty = DAORegistry.getPropertyDAO().get("tn.markup.percent");
		String tnMarkupStr = tnMarkupProperty.getValue();
		try{
			defaultMarkupPercent = Double.valueOf(tnMarkupStr);
		}catch(NumberFormatException nfe){}
		
		Property tnSalesProperty = DAORegistry.getPropertyDAO().get("tn.sales");
		String tnSalesStr = tnSalesProperty.getValue();
		try{
			defaultSalesPercent = Double.valueOf(tnSalesStr);
		}catch(NumberFormatException nfe){}
		
		Property tnShippingProperty = DAORegistry.getPropertyDAO().get("tn.shipping");
		String tnShippingStr = tnShippingProperty.getValue();
		try{
			defaultShippingFee = Double.valueOf(tnShippingStr);
		}catch(NumberFormatException nfe){}
		
		Property tnMinThresholdProperty = DAORegistry.getPropertyDAO().get("tn.minthreshold");
		String tnMinThresholdStr = tnMinThresholdProperty.getValue();
		try{
			defaultMinThreshold = Double.valueOf(tnMinThresholdStr);
		}catch(NumberFormatException nfe){}
		
		Property tnMaxThresholdProperty = DAORegistry.getPropertyDAO().get("tn.maxthreshold");
		String tnMaxThresholdStr = tnMaxThresholdProperty.getValue();
		try{
			defaultMaxThreshold = Double.valueOf(tnMaxThresholdStr);
		}catch(NumberFormatException nfe){}
		
		for (Event event : events) {
//			Event event = DAORegistry.getEventDAO().get(eventId);
			if (event == null) {
				continue;
			}

			instantEvent = new TNInstantEvent(event);
		
			instantEvent.setMarkupPercent(defaultMarkupPercent);
			instantEvent.setExpiryTime(defaultExpiryTime);
			instantEvent.setMinThreshold(defaultMinThreshold);
			instantEvent.setMaxThreshold(defaultMaxThreshold);
			instantEvent.setSalesPercent(defaultSalesPercent);
			instantEvent.setShippingFee(defaultShippingFee);
			instantEvent.setTour(event.getTour());

			instantEvents.add(instantEvent);
		}
		DAORegistry.getTninstantEventDAO().saveAll(instantEvents);
	}


	public static void deleteInstantEvents(Integer eventId){
		Event event= DAORegistry.getEventDAO().get(eventId);
		Integer tourId = event.getTourId();
		DAORegistry.getTninstantEventDAO().deleteById(eventId);
		int eventCount = DAORegistry.getTninstantEventDAO().getInstantEventCountByTour(tourId);
		if (eventCount ==0){
			DAORegistry.getTninstantTourDAO().deleteById(tourId);
		}
		}
	
	public static void deleteInstantTour(Integer tourId){
		Collection<TNInstantEvent> ievents = DAORegistry.getTninstantEventDAO().getAllTNInstantEventsByTour(tourId);
		for (TNInstantEvent ievent:ievents){
			DAORegistry.getTninstantEventDAO().deleteById(ievent.getEventId());
		}
		int eventCount = DAORegistry.getTninstantEventDAO().getInstantEventCountByTour(tourId);
		if (eventCount ==0){
			DAORegistry.getTninstantTourDAO().deleteById(tourId);
		}
	}
	
	public static void updateInstantEventsandTours(){
		Integer defaultExpiryTime = 0;
		Double defaultMarkupPercent = 0.0;
		Double defaultSalesPercent=0.0;
		Double defaultShippingFee=0.0;
		Double defaultMinThreshold=0.0;
		Double defaultMaxThreshold=0.0;
		
		Collection<TNInstantEvent> iEvents = DAORegistry.getTninstantEventDAO().getAll();
		Collection<TNInstantTour> iTours = DAORegistry.getTninstantTourDAO().getAll();
		
		Property tnExpiryProperty = DAORegistry.getPropertyDAO().get("tn.expiry");
		String tnExpiryStr = tnExpiryProperty.getValue();
		try {
			defaultExpiryTime = Integer.valueOf(tnExpiryStr);
		}catch(NumberFormatException nfe){}
		
		Property tnMarkupProperty = DAORegistry.getPropertyDAO().get("tn.markup.percent");
		String tnMarkupStr = tnMarkupProperty.getValue();
		try{
			defaultMarkupPercent = Double.valueOf(tnMarkupStr);
		}catch(NumberFormatException nfe){}
		
		Property tnSalesProperty = DAORegistry.getPropertyDAO().get("tn.sales");
		String tnSalesStr = tnSalesProperty.getValue();
		try{
			defaultSalesPercent = Double.valueOf(tnSalesStr);
		}catch(NumberFormatException nfe){}
		
		Property tnShippingProperty = DAORegistry.getPropertyDAO().get("tn.shipping");
		String tnShippingStr = tnShippingProperty.getValue();
		try{
			defaultShippingFee = Double.valueOf(tnShippingStr);
		}catch(NumberFormatException nfe){}
		
		Property tnMinThresholdProperty = DAORegistry.getPropertyDAO().get("tn.minthreshold");
		String tnMinThresholdStr = tnMinThresholdProperty.getValue();
		try{
			defaultMinThreshold = Double.valueOf(tnMinThresholdStr);
		}catch(NumberFormatException nfe){}
		
		Property tnMaxThresholdProperty = DAORegistry.getPropertyDAO().get("tn.maxthreshold");
		String tnMaxThresholdStr = tnMaxThresholdProperty.getValue();
		try{
			defaultMaxThreshold = Double.valueOf(tnMaxThresholdStr);
		}catch(NumberFormatException nfe){}
		
		
		for(TNInstantEvent instantEvent:iEvents){
		instantEvent.setMarkupPercent(defaultMarkupPercent);
		instantEvent.setExpiryTime(defaultExpiryTime);
		instantEvent.setMinThreshold(defaultMinThreshold);
		instantEvent.setMaxThreshold(defaultMaxThreshold);
		instantEvent.setSalesPercent(defaultSalesPercent);
		instantEvent.setShippingFee(defaultShippingFee);
		}
		DAORegistry.getTninstantEventDAO().saveOrUpdateAll(iEvents);
			
		for(TNInstantTour instantTour: iTours){
		instantTour.setMarkupPercent(defaultMarkupPercent);
		instantTour.setExpiryTime(defaultExpiryTime);
		instantTour.setMinThreshold(defaultMinThreshold);
		instantTour.setMaxThreshold(defaultMaxThreshold);
		instantTour.setSalesPercent(defaultSalesPercent);
		instantTour.setShippingFee(defaultShippingFee);
		}
		DAORegistry.getTninstantTourDAO().saveOrUpdateAll(iTours);
		
	}*/
}
