package com.admitone.tmat.utils.redmine;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ProtocolException;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MIME;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.AbstractContentBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

public class WebRedmineClient {
	public final static String BUG_TRACKER_ID = "1";
	public final static String FEATURE_TRACKER_ID = "2";
	public final static String SUPPORT_TRACKER_ID = "3";
	
	public String postRedmineIssue(final String trackerId, final String title, final String message, final int priority) throws Exception {
		return postRedmineIssue(null, trackerId, title, message, null, priority);
	}

	public String postRedmineIssue(final String trackerId, final String title, final String message, final RedmineAttachment[] attachments, final int priority) throws Exception {
		return postRedmineIssue(null, trackerId, title, message, attachments, priority);
	}
	
	public String postRedmineIssue(String project, final String trackerId, final String title, final String message, final RedmineAttachment[] attachments, final int priority) throws Exception {
		if (project == null || project.isEmpty()) {
			Property feedbackRedmineProjectProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.project");
			project = feedbackRedmineProjectProperty.getValue();
		}
		
		Property feedbackRedmineUriProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.uri");
		Property feedbackRedmineUsernameProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.username");
		Property feedbackRedminePasswordProperty = DAORegistry.getPropertyDAO().get("feedback.redmine.password");
		
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient();
		
		HttpPost httpPost = new HttpPost(feedbackRedmineUriProperty.getValue() + "/login");
		httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
 		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");			
		httpPost.addHeader("Referer", feedbackRedmineUriProperty.getValue() + "/login");

		String postData = 
				"username=" + URLEncoder.encode(feedbackRedmineUsernameProperty.getValue(), "ISO8859-1")
				+ "&password=" + URLEncoder.encode(feedbackRedminePasswordProperty.getValue(), "ISO8859-1")
				+ "&Login=Login+%C2%BB";
		
		httpPost.setEntity(new StringEntity(postData));
		
		RedmineRedirectHandler redirectHandler = new RedmineRedirectHandler(feedbackRedmineUriProperty.getValue());
		httpClient.setRedirectHandler(redirectHandler);
		
		CloseableHttpResponse response = null;
		try{
			response = httpClient.execute(httpPost);
			EntityUtils.consumeQuietly(response.getEntity());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
		if (redirectHandler == null) {
			throw new Exception("Cannot log in");
		}

		redirectHandler.reset();

		httpPost = new HttpPost(feedbackRedmineUriProperty.getValue() + "/projects/" + project.toLowerCase() + "/issues/new");
		httpPost.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
		httpPost.addHeader("Accept-Language", "en-us,en;q=0.5");
		httpPost.addHeader("Accept-Encoding", "gzip,deflate");
		httpPost.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");
		
		System.out.println(feedbackRedmineUriProperty.getValue() + "/projects/" + project.toLowerCase() + "/issues/new");
		MultipartEntity postEntity = new MultipartEntity();
		postEntity.addPart("issue[tracker_id]", new StringBody(trackerId));
		postEntity.addPart("issue[subject]", new StringBody(title));
		postEntity.addPart("issue[description]", new StringBody(message));
		postEntity.addPart("issue[status_id]", new StringBody("1"));
		postEntity.addPart("issue[priority_id]", new StringBody(priority + ""));
		postEntity.addPart("issue[assigned_to_id]", new StringBody(""));
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		postEntity.addPart("issue[start_date]", new StringBody(dateFormat.format(new Date())));
		postEntity.addPart("issue[due_date]", new StringBody(""));
		postEntity.addPart("issue[estimated_hours]", new StringBody(""));
		postEntity.addPart("issue[done_ratio]", new StringBody("0"));
		
		if (attachments != null) {
			int i = 1;		
			for(final RedmineAttachment attachment: attachments) {
				RedmineBody attachmentBody = new RedmineBody(attachment.getContent(), attachment.getMimeType(), attachment.getFilename());
				postEntity.addPart("attachments[" + (i++) + "][file]", attachmentBody);
			}
		}
		
		
		httpPost.setEntity(postEntity);
	
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				System.out.println("CONTENT=" + EntityUtils.toString(entity));
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
				
		String url = redirectHandler.getRedirectUrl();
		String[] tokens = url.split("/");
		
		return tokens[tokens.length - 1];
	}


	class RedmineRedirectHandler implements RedirectHandler {
		private String redirectUrl;
		private String baseUrl;
		
		public RedmineRedirectHandler(String baseUrl) {
			this.baseUrl = baseUrl;
		}

		public URI getLocationURI(HttpResponse response, HttpContext context)
				throws ProtocolException {
			System.out.println("REDIRECT TO URL=" + redirectUrl);
			if (redirectUrl == null) {
				return null;
			}

			if (redirectUrl.startsWith("http")) {
				return URI.create(redirectUrl);
			} else {
				return URI.create(baseUrl + redirectUrl);
			}
		}

		public boolean isRedirectRequested(HttpResponse response, HttpContext context) {
			Header[] headers = response.getAllHeaders();
			for (Header header: headers) {
				if (header.getName().equalsIgnoreCase("Location")) {
					redirectUrl = header.getValue();
					return true;
				}
			}
			return false;
		}

		public void reset() {
			redirectUrl = null;
		}
		
		public String getRedirectUrl() {
			return redirectUrl;
		}
		
	}

	class RedmineBody extends AbstractContentBody {

		private byte[] content;
		private String filename;

		public RedmineBody(final byte[] content, final String mimeType, final String filename) {
			super(mimeType);
			this.filename = filename;
			this.content = content;
		}

		@Deprecated
		public void writeTo(final OutputStream out, int mode) throws IOException {
			writeTo(out);
		}

		public void writeTo(final OutputStream out) throws IOException {
			out.write(content);
			out.flush();
		}

		public String getTransferEncoding() {
			return MIME.ENC_BINARY;
		}

		public String getCharset() {
			return null;
		}

		public long getContentLength() {
			return this.content.length;
		}

		public String getFilename() {
			return this.filename;
		}

	}

}
