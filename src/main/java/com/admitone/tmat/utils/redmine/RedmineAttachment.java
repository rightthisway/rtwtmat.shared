package com.admitone.tmat.utils.redmine;

public class RedmineAttachment {
	private byte[] content;
	private String filename;
	private String mimeType;
	
	public RedmineAttachment(byte[] content, String mimeType, String filename) {
		this.content = content;
		this.mimeType = mimeType;
		this.filename = filename;
	}
	
	public byte[] getContent() {
		return content;
	}
	
	public String getFilename() {
		return filename;
	}

	public String getMimeType() {
		return mimeType;
	}	
}
