package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Preference;

/**
 * Manage preferences of users (e.g., way tickets are displayed, ...) 
 *
 */
public class PreferenceManager implements InitializingBean {
	// map username => preferences
	private Map<String, Collection<Preference>> preferencesByUsernameMap = new HashMap<String, Collection<Preference>>();
	private Map<String, Preference> preferenceMap = new HashMap<String, Preference>();
	
	public PreferenceManager() {
	}
	
    // create a new preference if it does not exist
    public void updatePreference(String username, String name, String value) {
       /* Preference preference = preferenceMap.get(username + "-" + name);
        if (preference == null) {
              preference = new Preference(username, name, value);
              DAORegistry.getPreferenceDAO().save(preference);
        } else {
              preference.setValue(value);
              DAORegistry.getPreferenceDAO().update(preference);
        }*/
    	preferenceMap.remove(username + "-" + name);
    	Preference preference = new Preference(username, name, value);
    	DAORegistry.getPreferenceDAO().saveOrUpdate(preference);
        updatePreferenceMaps(preference);
    }
    
	public String getPreferenceValue(String username, String name, String defaultValue) {
		String value = getPreferenceValue(username, name);
		if (value == null) {
			updatePreference(username, name, defaultValue);
			return defaultValue;
		}
		
		return value;
	}

	public String getPreferenceValue(String username, String name) {
		Preference preference = preferenceMap.get(username + "-" + name);
		if (preference == null) {
			return null;
		}
		
		return preference.getValue();
	}
	
	private void updatePreferenceMaps(Preference preference) {
		Collection<Preference> preferences = preferencesByUsernameMap.get(preference.getUsername());
		if (preferences == null) {
			preferences = new ArrayList<Preference>();
			preferencesByUsernameMap.put(preference.getUsername(), preferences);
		} else {
			for (Preference cPref: preferences) {
				if (cPref.getName().equals(preference.getName())) {
					preferences.remove(cPref);
					break;
				}
			}
		}
		
		preferences.add(preference);
		
		preferenceMap.put(preference.getUsername() + "-" + preference.getName(), preference);		
	}
	
	public void removePreferences() {
		preferenceMap.clear();
		preferencesByUsernameMap.clear();
		DAORegistry.getPreferenceDAO().deleteAllPreferences();
	}
	
	public void removePreference(String username, String name) {
		preferenceMap.remove(username + "-" + name);
		DAORegistry.getPreferenceDAO().deletePreference(username, name);
	}
	
	public void removePreferenceForUser(String username) {
		for (Map.Entry<String, Preference> entry: new ArrayList<Map.Entry<String, Preference>>(preferenceMap.entrySet())) {
			if (entry.getValue().getUsername().equals(username)) {
				preferenceMap.remove(entry.getKey());
			}
		}
		preferencesByUsernameMap.remove(username);
		DAORegistry.getPreferenceDAO().deleteAllPreferences(username);
	}
	
	public void afterPropertiesSet() throws Exception {
		for (Preference preference: DAORegistry.getPreferenceDAO().getAll()) {
			updatePreferenceMaps(preference);
		}
	}
	
	public void resetFilterPreferenceForUser(String username) {
	
		String[] filterPreferenceNames = {
				"browseTicketCategory", "browseTicketCatGroup", "browseTicketWholesale", "browseTicketShow",
				"browseTicketSection", "browseTicketRow", "browseTicketStartPrice", "browseTicketEndPrice",
				"browseTicketQuantity", "browseTicketSites","browseZoneStartPrice", "browseZoneEndPrice"
				
				// "browseTicketShowDuplicates", "browseTicketRemoveDuplicatePolicy", "browseTicketNumTicketRows", "browseTicketMultisort", "browseTicketMultisortFields"
		};
		
		for(String name: filterPreferenceNames) {
			removePreference(username, name);
		}
	}
	

}
