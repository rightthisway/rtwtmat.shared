package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.ticketfetcher.TicketHit;
import com.admitone.tmat.ticketfetcher.TicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkDirectTicketListingFetcher;
import com.admitone.tmat.ticketfetcher.TicketNetworkTicketListingFetcher;
import com.admitone.tmat.web.pojo.FeedAndCrawlComparatorVO;

public class TNWebAndWSTicketListingComparator {
	
	private static TNWebAndWSTicketListingComparator instance = null;
	

	public static TNWebAndWSTicketListingComparator  getInstance(){
		
		if(instance==null){
			instance = new TNWebAndWSTicketListingComparator();
		}
		return instance;
	}

	public FeedAndCrawlComparatorVO compare(Integer zeroMarkupEventId){
		ComparatorTicketHitIndexer wsTicketHitIndexer = new ComparatorTicketHitIndexer(10000);
		ComparatorTicketHitIndexer webTicketHitIndexer = new ComparatorTicketHitIndexer(10000);
		TicketListingFetcher wsTicketListingFetcher = new TicketNetworkDirectTicketListingFetcher();
		TicketListingFetcher webTicketListingFetcher = new TicketNetworkTicketListingFetcher();
		
		String url = "http://zeromarkup.com/ResultsTicket.aspx?evtid="+zeroMarkupEventId+"&";
		
		
		Set<String> wsTicketIds = new HashSet<String>();
		Set<String> webTicketIds = new HashSet<String>();
		
		TicketListingCrawl webTicketListingCrawl = new TicketListingCrawl();
		webTicketListingCrawl.setSiteId(Site.TICKET_NETWORK);
		webTicketListingCrawl.setQueryUrl(url);
		webTicketListingCrawl.resetStats();

		TicketListingCrawl wsTicketListingCrawl = new TicketListingCrawl();
		wsTicketListingCrawl.setSiteId(Site.TICKET_NETWORK_DIRECT);
		wsTicketListingCrawl.setQueryUrl(url);
		wsTicketListingCrawl.resetStats();

		try {
			wsTicketListingFetcher.fetchTicketListing(wsTicketHitIndexer, wsTicketListingCrawl);
			webTicketListingFetcher.fetchTicketListing(webTicketHitIndexer, webTicketListingCrawl);
		} catch (InterruptedTicketListingCrawlException e) {
			// do nothing
			
			System.out.println("InterruptedTicketListingCrawlException : " + e.getMessage());
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Error while fetching tickets : " + e.getMessage());
			e.printStackTrace();
		}
		
		Collection<TicketHit> wsTicketHits = wsTicketHitIndexer.getFetchedTicketHits();
		Map<String, TicketHit> wsTicketById = new HashMap<String, TicketHit>();
		for(TicketHit wsTicketHit: wsTicketHits) {
			// we are not using ticket id for the comparison as they are different on zeromarkup
			// and TND
			// we use <SECTION>-<ROW> combination instead
			wsTicketHit.setItemID(wsTicketHit.getSection() + "-" + wsTicketHit.getRow());
			wsTicketIds.add(wsTicketHit.getItemId());
			wsTicketById.put(wsTicketHit.getItemId(), wsTicketHit);
		}
		
		Collection<TicketHit> webTicketHits = webTicketHitIndexer.getFetchedTicketHits();
		Map<String, TicketHit> webTicketById = new HashMap<String, TicketHit>();
		for(TicketHit webTicketHit: webTicketHits) {
			// we are not using ticket id for the comparison as they are different on zeromarkup
			// and TND
			// we use <SECTION>-<ROW> combination instead
			webTicketHit.setItemID(webTicketHit.getSection() + "-" + webTicketHit.getRow());
			webTicketIds.add(webTicketHit.getItemId());
			webTicketById.put(webTicketHit.getItemId(), webTicketHit);
		}
		
		Set<String> diffWebTicketIds = new HashSet<String>(webTicketIds);
		Set<String> diffWsTicketIds = new HashSet<String>(wsTicketIds);
		

		String eventId = zeroMarkupEventId.toString();

		System.out.println("The number of tickets indexed in TND: " + wsTicketHits.size());
		System.out.println("The number of tickets indexed in ZEROMARKUP: " + webTicketHits.size());
		// see which ids appears in feed but not in web
		for(String ticketId: wsTicketIds) {
			diffWebTicketIds.remove(ticketId);
		}
		
		ArrayList<TicketHit> extraTicketInFeed = new ArrayList<TicketHit>();
		for(String ticketId: diffWebTicketIds) {
			TicketHit ticketHit = webTicketById.get(ticketId);
			extraTicketInFeed.add(ticketHit);
			System.out.println("TICKET HIT=" + ticketHit);
		}		
		
		
		System.out.println("The number of tickets which appear in feed but not in web : " + diffWebTicketIds.size());
		
		
		// see which ids appears in web but not in ws
		for(String ticketId: webTicketIds) {
			diffWsTicketIds.remove(ticketId);
		}
		
		ArrayList<TicketHit> missingTicketInFeed = new ArrayList<TicketHit>();
		for(String ticketId: diffWsTicketIds) {
			TicketHit ticketHit = wsTicketById.get(ticketId);
			missingTicketInFeed.add(ticketHit);
			System.out.println("TICKET HIT=" + ticketHit);
		}		

		System.out.println("The number of tickets which appear in feed but not in web : " + diffWsTicketIds.size());
		
		FeedAndCrawlComparatorVO feedAndCrawlComparatorVO = new FeedAndCrawlComparatorVO();
		
		feedAndCrawlComparatorVO.setTicketIndexedFromFeed(wsTicketHits.size());
		feedAndCrawlComparatorVO.setTicketIndexedFromWeb(webTicketHits.size());
		feedAndCrawlComparatorVO.setExtraTicketInFeed(extraTicketInFeed);
		feedAndCrawlComparatorVO.setMissingTicketInFeed(missingTicketInFeed);		
		
		wsTicketHitIndexer.reset();
		webTicketHitIndexer.reset();		
		
		return feedAndCrawlComparatorVO;
		
	}
}
