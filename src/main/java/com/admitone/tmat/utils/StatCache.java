package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Artist;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.web.Constants;

public class StatCache {
	
	private boolean empty = true;
	
	private Date startUpdate = null;
	private Date endUpdate = null;
	
	private boolean refreshingTourStatCache = false;

	private Map<Integer, Map<String, Integer>> tourStatsByQuantity = null;
	private Map<Integer, Map<String, Integer>> nonDuplicateTourStatsByQuantity = null;
	private Map<Integer, Map<String, Integer>> tourStatsBySiteId = null;
	private Map<Integer, Map<String, Integer>> nonDuplicateTourStatsBySiteId = null;
	private Map<Integer, Map<String, Integer>> eventStatsByQuantity = null;
	private Map<Integer, Map<String, Integer>> nonDuplicateEventStatsByQuantity = null;
	private Map<Integer, Map<String, Integer>> eventStatsBySiteId = null;
	private Map<Integer, Map<String, Integer>> nonDuplicateEventStatsBySiteId = null;
	private int progressPercent = 0;
	
	private Map<String, Integer> totalStatsBySiteId = null;
	private Map<String, Integer> nonDuplicateTotalStatsBySiteId = null;
	private Map<String, Integer> totalStatsByQuantity = null;
	private Map<String, Integer> nonDuplicateTotalStatsByQuantity = null;
	
	private int ticketCount = 0;
	private int nonDuplicateTicketCount = 0;
	private int ticketEntryCount = 0;		
	private int nonDuplicateTicketEntryCount = 0;		
	private int eventCount = 0;
	
	Timer timer = new Timer();

	public void refreshTourStatCacheEntry() {
		if (refreshingTourStatCache) {
			return;
		}
		
		refreshingTourStatCache = true;
		
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				try {
					updateTourStatCacheEntry();
				} catch (Exception e) {
					e.printStackTrace();
				}
				refreshingTourStatCache = false;
			}
			
		}, 0);
	}

	
	private void updateTourStatCacheEntry() {
		startUpdate = new Date();
		
		Map<Integer, Map<String, Integer>> newEventStatsBySiteId = new HashMap<Integer, Map<String,Integer>>();
		Map<Integer, Map<String, Integer>> newEventStatsByQuantity = new HashMap<Integer, Map<String,Integer>>();
		Map<Integer, Map<String, Integer>> newTourStatsBySiteId = new HashMap<Integer, Map<String,Integer>>();
		Map<Integer, Map<String, Integer>> newTourStatsByQuantity = new HashMap<Integer, Map<String,Integer>>();

		Map<Integer, Map<String, Integer>> newNonDuplicateEventStatsBySiteId = new HashMap<Integer, Map<String,Integer>>();
		Map<Integer, Map<String, Integer>> newNonDuplicateEventStatsByQuantity = new HashMap<Integer, Map<String,Integer>>();
		Map<Integer, Map<String, Integer>> newNonDuplicateTourStatsBySiteId = new HashMap<Integer, Map<String,Integer>>();
		Map<Integer, Map<String, Integer>> newNonDuplicateTourStatsByQuantity = new HashMap<Integer, Map<String,Integer>>();

		int newTicketCount = 0;
		int newNonDuplicateTicketCount = 0;
		int newTicketEntryCount = 0;
		int newNonDuplicateTicketEntryCount = 0;
		int newEventCount = 0;
		
		Map<String, Integer> newTotalStatsByQuantity = new HashMap<String, Integer>();
		Map<String, Integer> newNonDuplicateTotalStatsByQuantity = new HashMap<String, Integer>();
		for(String qty: new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9+"}) {
			newTotalStatsByQuantity.put(qty, 0);
			newNonDuplicateTotalStatsByQuantity.put(qty, 0);
		}		

		Map<String, Integer> newTotalStatsBySiteId = new HashMap<String, Integer>();
		Map<String, Integer> newNonDuplicateTotalStatsBySiteId = new HashMap<String, Integer>();
		for(String siteId: Constants.getInstance().getSiteIds()) {
			newTotalStatsBySiteId.put(siteId, 0);			
			newNonDuplicateTotalStatsBySiteId.put(siteId, 0);			
		}

		Collection<Event> events = DAORegistry.getEventDAO().getAllEventsByStatus(EventStatus.ACTIVE); 
		Collection<Artist> artists = DAORegistry.getArtistDAO().getAll();
		Map <Integer, Collection<Event>> eventsByTourId = new HashMap<Integer, Collection<Event>>();
		for(Artist artist: artists) {
			eventsByTourId.put(artist.getId(), new ArrayList<Event>());
		}

		for(Event event: events) {
			eventsByTourId.get(event.getArtistId()).add(event);
		}

		int i = 0;

		for (Artist artist: artists) {

			Map<String,Integer> currentTourStatsByQuantity = new HashMap<String,Integer>();
			Map<String,Integer> nonDuplicateCurrentTourStatsByQuantity = new HashMap<String,Integer>();
			for(String qty: new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9+"}) {
				currentTourStatsByQuantity.put(qty, 0);
				nonDuplicateCurrentTourStatsByQuantity.put(qty, 0);
			}
			newTourStatsByQuantity.put(artist.getId(), currentTourStatsByQuantity);
			newNonDuplicateTourStatsByQuantity.put(artist.getId(), nonDuplicateCurrentTourStatsByQuantity);
			
			Map<String,Integer> currentTourStatsBySiteId = new HashMap<String,Integer>();
			Map<String,Integer> nonDuplicateCurrentTourStatsBySiteId = new HashMap<String,Integer>();
			for(String siteId: Constants.getInstance().getSiteIds()) {
				currentTourStatsBySiteId.put(siteId, 0);
				nonDuplicateCurrentTourStatsBySiteId.put(siteId, 0);
			}
			newTourStatsBySiteId.put(artist.getId(), currentTourStatsBySiteId);
			newNonDuplicateTourStatsBySiteId.put(artist.getId(), nonDuplicateCurrentTourStatsBySiteId);

			for (Event event: eventsByTourId.get(artist.getId())) {
				progressPercent =  (i * 100) / events.size();
				eventCount++;
				Map<String,Integer> currentEventStatsByQuantity = new HashMap<String,Integer>();
				Map<String,Integer> nonDuplicateCurrentEventStatsByQuantity = new HashMap<String,Integer>();
				for(String qty: new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9+"}) {
					currentEventStatsByQuantity.put(qty, 0);
					nonDuplicateCurrentEventStatsByQuantity.put(qty, 0);
				}
				newEventStatsByQuantity.put(event.getId(), currentEventStatsByQuantity);
				newNonDuplicateEventStatsByQuantity.put(event.getId(), nonDuplicateCurrentEventStatsByQuantity);

				Map<String,Integer> currentEventStatsBySiteId = new HashMap<String,Integer>();
				Map<String,Integer> nonDuplicateCurrentEventStatsBySiteId = new HashMap<String,Integer>();
				for(String siteId: Constants.getInstance().getSiteIds()) {
					currentEventStatsBySiteId.put(siteId, 0);
					nonDuplicateCurrentEventStatsBySiteId.put(siteId, 0);
				}
				newEventStatsBySiteId.put(event.getId(), currentEventStatsBySiteId);
				newNonDuplicateEventStatsBySiteId.put(event.getId(), nonDuplicateCurrentEventStatsBySiteId);

				Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
				
				for(Ticket ticket: tickets) {
					newTicketCount += ticket.getRemainingQuantity();
					newTicketEntryCount ++;
					currentEventStatsBySiteId.put(ticket.getSiteId(), currentEventStatsBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
					currentTourStatsBySiteId.put(ticket.getSiteId(), currentTourStatsBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
					newTotalStatsBySiteId.put(ticket.getSiteId(), newTotalStatsBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
					
					String quantityStr;
					if (ticket.getRemainingQuantity() > 8) {
						quantityStr = "9+";
					} else {
						quantityStr = Integer.toString(ticket.getRemainingQuantity());
					}
					currentEventStatsByQuantity.put(quantityStr, currentEventStatsByQuantity.get(quantityStr) + 1);					
					currentTourStatsByQuantity.put(quantityStr, currentTourStatsByQuantity.get(quantityStr) + 1);
					newTotalStatsByQuantity.put(quantityStr, newTotalStatsByQuantity.get(quantityStr) + 1);
				}
				
				//
				// NON DUPLICATE KEYS
				//

				Collection<Ticket> nonDuplicateTickets = TicketUtil.removeDuplicateTickets(DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId()));
				
				for(Ticket ticket: nonDuplicateTickets) {
					newNonDuplicateTicketCount += ticket.getRemainingQuantity();
					newNonDuplicateTicketEntryCount ++;
					
					nonDuplicateCurrentEventStatsBySiteId.put(ticket.getSiteId(), nonDuplicateCurrentEventStatsBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
					nonDuplicateCurrentTourStatsBySiteId.put(ticket.getSiteId(), currentTourStatsBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
					newNonDuplicateTotalStatsBySiteId.put(ticket.getSiteId(), newNonDuplicateTotalStatsBySiteId.get(ticket.getSiteId()) + ticket.getRemainingQuantity());
					
					String quantityStr;
					if (ticket.getRemainingQuantity() > 8) {
						quantityStr = "9+";
					} else {
						quantityStr = Integer.toString(ticket.getRemainingQuantity());
					}
					nonDuplicateCurrentEventStatsByQuantity.put(quantityStr, nonDuplicateCurrentEventStatsByQuantity.get(quantityStr) + 1);					
					nonDuplicateCurrentTourStatsByQuantity.put(quantityStr, nonDuplicateCurrentTourStatsByQuantity.get(quantityStr) + 1);
					newNonDuplicateTotalStatsByQuantity.put(quantityStr, newNonDuplicateTotalStatsByQuantity.get(quantityStr) + 1);
				}
				i++;
			}
		}

		tourStatsByQuantity = newTourStatsByQuantity;
		nonDuplicateTourStatsByQuantity = newNonDuplicateTourStatsByQuantity;
		tourStatsBySiteId = newTourStatsBySiteId;
		nonDuplicateTourStatsBySiteId = newNonDuplicateTourStatsBySiteId;
		eventStatsByQuantity = newEventStatsByQuantity;
		nonDuplicateEventStatsByQuantity = newNonDuplicateEventStatsByQuantity;
		eventStatsBySiteId = newEventStatsBySiteId;
		nonDuplicateEventStatsBySiteId = newNonDuplicateEventStatsBySiteId;
		
		totalStatsBySiteId = newTotalStatsBySiteId;		
		nonDuplicateTotalStatsBySiteId = newNonDuplicateTotalStatsBySiteId;
		totalStatsByQuantity = newTotalStatsByQuantity;
		nonDuplicateTotalStatsByQuantity = newNonDuplicateTotalStatsByQuantity;
		
		ticketCount = newTicketCount;
		nonDuplicateTicketCount = newNonDuplicateTicketCount;
		ticketEntryCount = newTicketEntryCount;
		nonDuplicateTicketEntryCount = newNonDuplicateTicketEntryCount;
		eventCount = newEventCount;

		empty = false;
		
		endUpdate = new Date();
	}


	public Date getStartUpdate() {
		return startUpdate;
	}


	public Date getEndUpdate() {
		return endUpdate;
	}


	public boolean isRefreshingTourStatCache() {
		return refreshingTourStatCache;
	}


	public Map<Integer, Map<String, Integer>> getTourStatsByQuantity() {
		return tourStatsByQuantity;
	}


	public Map<Integer, Map<String, Integer>> getTourStatsBySiteId() {
		return tourStatsBySiteId;
	}


	public Map<Integer, Map<String, Integer>> getEventStatsByQuantity() {
		return eventStatsByQuantity;
	}


	public Map<Integer, Map<String, Integer>> getEventStatsBySiteId() {
		return eventStatsBySiteId;
	}


	public Map<String, Integer> getTotalStatsBySiteId() {
		return totalStatsBySiteId;
	}


	public Map<String, Integer> getNonDuplicateTotalStatsBySiteId() {
		return nonDuplicateTotalStatsBySiteId;
	}


	public Map<String, Integer> getTotalStatsByQuantity() {
		return totalStatsByQuantity;
	}

	public Map<String, Integer> getNonDuplicateTotalStatsByQuantity() {
		return nonDuplicateTotalStatsByQuantity;
	}


	public int getTicketCount() {
		return ticketCount;
	}


	public int getNonDuplicateTicketCount() {
		return nonDuplicateTicketCount;
	}


	public int getTicketEntryCount() {
		return ticketEntryCount;
	}


	public int getNonDuplicateTicketEntryCount() {
		return nonDuplicateTicketEntryCount;
	}


	public int getEventCount() {
		return eventCount;
	}


	public Timer getTimer() {
		return timer;
	}


	public boolean isEmpty() {
		return empty;
	}


	public Map<Integer, Map<String, Integer>> getNonDuplicateTourStatsByQuantity() {
		return nonDuplicateTourStatsByQuantity;
	}


	public Map<Integer, Map<String, Integer>> getNonDuplicateTourStatsBySiteId() {
		return nonDuplicateTourStatsBySiteId;
	}


	public Map<Integer, Map<String, Integer>> getNonDuplicateEventStatsByQuantity() {
		return nonDuplicateEventStatsByQuantity;
	}


	public Map<Integer, Map<String, Integer>> getNonDuplicateEventStatsBySiteId() {
		return nonDuplicateEventStatsBySiteId;
	}


	public int getProgressPercent() {
		return progressPercent;
	}

	
}

