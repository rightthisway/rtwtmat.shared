package com.admitone.tmat.utils;

import com.admitone.tmat.dao.DAORegistry;

public class CacheTicketProcessingThreadRunner {
	public final int DEFAULT_MAX_THREADES = 6;
	
	public CacheTicketProcessingThreadRunner(){
		Integer maxThreads = DEFAULT_MAX_THREADES;
		try{
			maxThreads = Integer.parseInt(DAORegistry.getPropertyDAO().get("crawler.thread.count").getValue()) * 2;
		}catch(Exception e){
			System.out.println("Error getting the crawler.thread.count");
			e.printStackTrace();
			maxThreads = DEFAULT_MAX_THREADES;
		}
		for(int i = 0; i< maxThreads; i++){
			CacheTicketProcessor thread = new CacheTicketProcessor(i);
			thread.start();
		}
	}

}
