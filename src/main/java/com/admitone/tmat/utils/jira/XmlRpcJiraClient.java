package com.admitone.tmat.utils.jira;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;

/**
 * Jira Client using XML RPC protocol.
 * 
 * The XML RPC protocol does not support attachments. (to use attachment, use the Soap client)
 * 
 *  @author frederic
 *
 */
public class XmlRpcJiraClient extends JiraClient{
	
	public XmlRpcJiraClient() {
	}

	public String postJiraIssue(String projectName, String issueType, String title, String message) {
		return postJiraIssue(projectName, issueType, title, message, null);
	}

	/**
	 * 
	 * @param title
	 * @param message
	 * @return issue key.
	 */
	@Override
	public String postJiraIssue(String projectName, String issueType, String title, String message, JiraAttachment[] attachments) {
		Property feedbackJiraUriProperty = DAORegistry.getPropertyDAO().get("feedback.jira.uri");
		Property feedbackJiraRpcPathProperty = DAORegistry.getPropertyDAO().get("feedback.jira.rpcPath");
		Property feedbackJiraProjectProperty = DAORegistry.getPropertyDAO().get("feedback.jira.project");
		Property feedbackJiraComponentsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.components");
		Property feedbackJiraAffectsVersionsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.affectsVersions");
		Property feedbackJiraUsernameProperty = DAORegistry.getPropertyDAO().get("feedback.jira.username");
		Property feedbackJiraPasswordProperty = DAORegistry.getPropertyDAO().get("feedback.jira.password");
		
		try {
			XmlRpcClientConfigImpl xmlRpcConfig = new XmlRpcClientConfigImpl();
			xmlRpcConfig.setServerURL(new URL(feedbackJiraUriProperty.getValue() + feedbackJiraRpcPathProperty.getValue()));
			
			XmlRpcClient rpcClient = new XmlRpcClient();
			rpcClient.setConfig(xmlRpcConfig);
			
			Object[] loginParams = {
					feedbackJiraUsernameProperty.getValue(),
					feedbackJiraPasswordProperty.getValue()					
			};
			String loginToken = (String)rpcClient.execute("jira1.login", loginParams);
						
			Object[] componentsResult = (Object[])rpcClient.execute("jira1.getComponents", new Object[] {
					loginToken, feedbackJiraProjectProperty.getValue()
			});
			Map<String, String> componentIdByName = new HashMap<String, String>();
			for(Object result: componentsResult) {
				Map<String, String> componentMap = (Map<String, String>)result; 
				componentIdByName.put(componentMap.get("name").toUpperCase(), componentMap.get("id"));
			}				

			Object[] versionsResult = (Object[])rpcClient.execute("jira1.getVersions", new Object[] {
					loginToken, feedbackJiraProjectProperty.getValue()
			});
			Map<String, String> versionIdByName = new HashMap<String, String>();
			for(Object result: versionsResult) {
				Map<String, String> versionMap = (Map<String, String>)result; 
				versionIdByName.put(versionMap.get("name").toUpperCase(), versionMap.get("id"));
			}				
						
			Map<String, Object> issueMap = new HashMap<String, Object>();
			if (projectName == null || projectName.isEmpty()) {
				issueMap.put("project", feedbackJiraProjectProperty.getValue());								
			} else {
				issueMap.put("project", projectName);				
			}
			issueMap.put("summary", title);
			issueMap.put("type", issueType);
			issueMap.put("priority", "3");
			issueMap.put("description", message);
			
			List<Map<String, String>> components = new ArrayList<Map<String,String>>();
			for(String componentName: feedbackJiraComponentsProperty.getValue().split(",")) {
				componentName = componentName.trim().toUpperCase();
				if (componentIdByName.get(componentName) == null) {
					continue;
				}
				Map<String, String> c = new HashMap<String, String>();
				c.put("id", componentIdByName.get(componentName));
				components.add(c);
			}
			
			issueMap.put("components", components);

			List<Map<String, String>> versions = new ArrayList<Map<String,String>>();
			for(String versionName: feedbackJiraAffectsVersionsProperty.getValue().split(",")) {
				versionName = versionName.trim().toUpperCase();
				if (versionIdByName.get(versionName) == null) {
					continue;
				}
				Map<String, String> c = new HashMap<String, String>();
				c.put("id", versionIdByName.get(versionName));
				versions.add(c);
			}
			issueMap.put("affectsVersions", versions);
											
			Object[] issueParams = {
				loginToken,
				issueMap
			};
			
			Map<String, Object> issueTokens = (HashMap<String, Object>)rpcClient.execute("jira1.createIssue", issueParams);				
			String issueKey = (String)issueTokens.get("key");
			
			// Log out
			Boolean logoutToken = (Boolean) rpcClient.execute("jira1.logout", new Object[]{loginToken});
			System.out.println("Logout successful: " + logoutToken);
			return issueKey;	
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
	}
}
