package com.admitone.tmat.utils.jira;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.axis.encoding.Base64;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.utils.jira.soap.JiraSoapService;
import com.admitone.tmat.utils.jira.soap.JiraSoapServiceService;
import com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator;
import com.admitone.tmat.utils.jira.soap.RemoteComponent;
import com.admitone.tmat.utils.jira.soap.RemoteIssue;
import com.admitone.tmat.utils.jira.soap.RemoteVersion;

/**
 * SOAP Jira Client.
 * @author frederic
 *
 */
public class SoapJiraClient extends JiraClient{
	
	public SoapJiraClient() {
	}

	public String postJiraIssue(String projectName, String issueType, String title, String message) throws Exception {
		return postJiraIssue(projectName, issueType, title, message, null);
	}

	/**
	 * @param title
	 * @param message
	 * @return issue key.
	 */
	public String postJiraIssue(final String projectName, final String issueType, final String title, final String message, final JiraAttachment[] attachments) throws Exception {
		Property feedbackJiraUriProperty = DAORegistry.getPropertyDAO().get("feedback.jira.uri");
		Property feedbackJiraSoapPathProperty = DAORegistry.getPropertyDAO().get("feedback.jira.soapPath");
		Property feedbackJiraRpcPathProperty = DAORegistry.getPropertyDAO().get("feedback.jira.rpcPath");
		Property feedbackJiraProjectProperty = DAORegistry.getPropertyDAO().get("feedback.jira.project");
		Property feedbackJiraComponentsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.components");
		Property feedbackJiraAffectsVersionsProperty = DAORegistry.getPropertyDAO().get("feedback.jira.affectsVersions");
		Property feedbackJiraUsernameProperty = DAORegistry.getPropertyDAO().get("feedback.jira.username");
		Property feedbackJiraPasswordProperty = DAORegistry.getPropertyDAO().get("feedback.jira.password");
		
		JiraSoapServiceService jiraSoapServiceGetter = new JiraSoapServiceServiceLocator();		
		
		final JiraSoapService jiraSoapService = jiraSoapServiceGetter.getJirasoapserviceV2(new URL(feedbackJiraUriProperty.getValue() + "/" + feedbackJiraSoapPathProperty.getValue()));
		final String token = jiraSoapService.login(feedbackJiraUsernameProperty.getValue(), feedbackJiraPasswordProperty.getValue());		

		Map<String, RemoteComponent> componentByName = new HashMap<String, RemoteComponent>();
		RemoteComponent[] components = jiraSoapService.getComponents(token, feedbackJiraProjectProperty.getValue());
		for(RemoteComponent component: components) {			
			componentByName.put(component.getName().toUpperCase(), component);
		}

		Map<String, RemoteVersion> versionByName = new HashMap<String, RemoteVersion>();
		RemoteVersion[] versions = jiraSoapService.getVersions(token, feedbackJiraProjectProperty.getValue());
		for(RemoteVersion version: versions) {			
			versionByName.put(version.getName().toUpperCase(), version);
		}
		
		RemoteIssue issue = new RemoteIssue();
		if (projectName == null || projectName.isEmpty()) {
			issue.setProject(feedbackJiraProjectProperty.getValue());
		} else {
			issue.setProject(projectName);			
		}
		issue.setSummary(title);
		issue.setType(issueType);
		issue.setPriority("3");
		issue.setDescription(message);
		
		List<RemoteComponent> affectComponents = new ArrayList<RemoteComponent>();
		for(String componentName: feedbackJiraComponentsProperty.getValue().split(",")) {
			RemoteComponent component = componentByName.get(componentName.trim().toUpperCase());
			if (component == null) {
				continue;
			}
			affectComponents.add(component);
		}		
		RemoteComponent[] affectComponentArray = new RemoteComponent[affectComponents.size()];
		affectComponents.toArray(affectComponentArray);
		issue.setComponents(affectComponentArray);
		
		List<RemoteVersion> affectsVersions = new ArrayList<RemoteVersion>();
		for(String versionName: feedbackJiraAffectsVersionsProperty.getValue().split(",")) {
			RemoteVersion version = versionByName.get(versionName.trim().toUpperCase());  
			if (version == null) {
				continue;
			}
			affectsVersions.add(version);
		}
		RemoteVersion[] affectVersionArray = new RemoteVersion[affectsVersions.size()];
		affectsVersions.toArray(affectVersionArray);
		issue.setAffectsVersions(affectVersionArray);
				

		final RemoteIssue resultIssue = jiraSoapService.createIssue(token, issue);
		
		if (attachments != null && attachments.length > 0) {
			String[] attachmentContents = new String[attachments.length];
			String[] attachmentFilenames = new String[attachments.length];
						
			for (int i = 0 ; i < attachments.length ; i++) {
				JiraAttachment attachment = attachments[i];
				attachmentContents[i] = Base64.encode(attachment.getContent()); 
				attachmentFilenames[i] = attachment.getFilename(); 
			}
			
			try {
				jiraSoapService.addBase64EncodedAttachmentsToIssue(token, resultIssue.getKey(), attachmentFilenames, attachmentContents);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		jiraSoapService.logout(token);
		
		
		return resultIssue.getKey();

	}
}
