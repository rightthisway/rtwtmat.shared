/**
 * JiraSoapServiceService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.admitone.tmat.utils.jira.soap;

public interface JiraSoapServiceService extends javax.xml.rpc.Service {
    public java.lang.String getJirasoapserviceV2Address();

    public com.admitone.tmat.utils.jira.soap.JiraSoapService getJirasoapserviceV2() throws javax.xml.rpc.ServiceException;

    public com.admitone.tmat.utils.jira.soap.JiraSoapService getJirasoapserviceV2(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
