package com.admitone.tmat.utils.jira;

/**
 * Jira attachment.
 * @author frederic
 *
 */
public class JiraAttachment {
	private byte[] content;
	private String mimeType;
	private String filename;
	
	public JiraAttachment(byte[] content, String mimeType, String filename) {
		this.content = content;
		this.mimeType = mimeType;
		this.filename = filename;
	}
	
	public byte[] getContent() {
		return content;
	}

	public String getMimeType() {
		return mimeType;
	}

	public String getFilename() {
		return filename;
	}
	
	
}
