package com.admitone.tmat.utils.jira;

/**
 * JIRA CLIENT.
 * @author frederic
 */
public abstract class JiraClient {
	public static final String ISSUE_TYPE_BUG = "1";
	public static final String ISSUE_TYPE_ENHANCEMENT = "2";
	public static final String ISSUE_TYPE_TASK = "3";
	public static final String ISSUE_TYPE_IMPROVEMENT = "4";
	
	public JiraClient() {
	}

	public String postJiraIssue(String projectName, String issueType, String title, String message) throws Exception {
		return postJiraIssue(projectName, issueType, title, message, null);
	}

	/**
	 * @param title
	 * @param message
	 * @return issue key.
	 */
	public abstract String postJiraIssue(String projectName, String issueType, String title, String message, JiraAttachment[] attachments) throws Exception;
}
