/**
 * JiraSoapServiceServiceTestCase.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.admitone.tmat.utils.jira.soap;

public class JiraSoapServiceServiceTestCase extends junit.framework.TestCase {
    public JiraSoapServiceServiceTestCase(java.lang.String name) {
        super(name);
    }

    public void testJirasoapserviceV2WSDL() throws Exception {
        javax.xml.rpc.ServiceFactory serviceFactory = javax.xml.rpc.ServiceFactory.newInstance();
        java.net.URL url = new java.net.URL(new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2Address() + "?WSDL");
        javax.xml.rpc.Service service = serviceFactory.createService(url, new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getServiceName());
        assertTrue(service != null);
    }

    public void test1JirasoapserviceV2GetComment() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteComment value = null;
            value = binding.getComment(new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test2JirasoapserviceV2CreateGroup() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteGroup value = null;
            value = binding.createGroup(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteUser());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test3JirasoapserviceV2GetServerInfo() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        com.admitone.tmat.utils.jira.soap.RemoteServerInfo value = null;
        value = binding.getServerInfo(new java.lang.String());
        // TBD - validate results
    }

    public void test4JirasoapserviceV2GetGroup() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteGroup value = null;
            value = binding.getGroup(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test5JirasoapserviceV2Login() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.lang.String value = null;
            value = binding.login(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e1) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test6JirasoapserviceV2GetUser() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteUser value = null;
            value = binding.getUser(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test7JirasoapserviceV2GetIssue() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue value = null;
            value = binding.getIssue(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test8JirasoapserviceV2Logout() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        boolean value = false;
        value = binding.logout(new java.lang.String());
        // TBD - validate results
    }

    public void test9JirasoapserviceV2GetComponents() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteComponent[] value = null;
            value = binding.getComponents(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test10JirasoapserviceV2CreateUser() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteUser value = null;
            value = binding.createUser(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test11JirasoapserviceV2CreateIssue() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue value = null;
            value = binding.createIssue(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteIssue());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test12JirasoapserviceV2UpdateIssue() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue value = null;
            value = binding.updateIssue(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteFieldValue[0]);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test13JirasoapserviceV2DeleteIssue() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteIssue(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test14JirasoapserviceV2GetAvailableActions() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteNamedObject[] value = null;
            value = binding.getAvailableActions(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test15JirasoapserviceV2GetSubTaskIssueTypes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssueType[] value = null;
            value = binding.getSubTaskIssueTypes(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test16JirasoapserviceV2GetConfiguration() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteConfiguration value = null;
            value = binding.getConfiguration(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test17JirasoapserviceV2CreateProject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProject value = null;
            value = binding.createProject(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemotePermissionScheme(), new com.admitone.tmat.utils.jira.soap.RemoteScheme(), new com.admitone.tmat.utils.jira.soap.RemoteScheme());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test18JirasoapserviceV2UpdateProject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProject value = null;
            value = binding.updateProject(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProject());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test19JirasoapserviceV2GetProjectByKey() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProject value = null;
            value = binding.getProjectByKey(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test20JirasoapserviceV2RemoveAllRoleActorsByProject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.removeAllRoleActorsByProject(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProject());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test21JirasoapserviceV2GetPriorities() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemotePriority[] value = null;
            value = binding.getPriorities(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test22JirasoapserviceV2GetResolutions() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteResolution[] value = null;
            value = binding.getResolutions(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test23JirasoapserviceV2GetIssueTypes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssueType[] value = null;
            value = binding.getIssueTypes(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test24JirasoapserviceV2GetStatuses() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteStatus[] value = null;
            value = binding.getStatuses(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test25JirasoapserviceV2GetProjectRoles() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProjectRole[] value = null;
            value = binding.getProjectRoles(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test26JirasoapserviceV2GetProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProjectRole value = null;
            value = binding.getProjectRole(new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test27JirasoapserviceV2GetProjectRoleActors() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProjectRoleActors value = null;
            value = binding.getProjectRoleActors(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProjectRole(), new com.admitone.tmat.utils.jira.soap.RemoteProject());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test28JirasoapserviceV2GetDefaultRoleActors() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteRoleActors value = null;
            value = binding.getDefaultRoleActors(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProjectRole());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test29JirasoapserviceV2RemoveAllRoleActorsByNameAndType() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.removeAllRoleActorsByNameAndType(new java.lang.String(), new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test30JirasoapserviceV2DeleteProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteProjectRole(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProjectRole(), true);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test31JirasoapserviceV2UpdateProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.updateProjectRole(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProjectRole());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test32JirasoapserviceV2CreateProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProjectRole value = null;
            value = binding.createProjectRole(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProjectRole());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test33JirasoapserviceV2IsProjectRoleNameUnique() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            boolean value = false;
            value = binding.isProjectRoleNameUnique(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test34JirasoapserviceV2AddActorsToProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.addActorsToProjectRole(new java.lang.String(), new java.lang.String[0], new com.admitone.tmat.utils.jira.soap.RemoteProjectRole(), new com.admitone.tmat.utils.jira.soap.RemoteProject(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test35JirasoapserviceV2RemoveActorsFromProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.removeActorsFromProjectRole(new java.lang.String(), new java.lang.String[0], new com.admitone.tmat.utils.jira.soap.RemoteProjectRole(), new com.admitone.tmat.utils.jira.soap.RemoteProject(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test36JirasoapserviceV2AddDefaultActorsToProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.addDefaultActorsToProjectRole(new java.lang.String(), new java.lang.String[0], new com.admitone.tmat.utils.jira.soap.RemoteProjectRole(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test37JirasoapserviceV2RemoveDefaultActorsFromProjectRole() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.removeDefaultActorsFromProjectRole(new java.lang.String(), new java.lang.String[0], new com.admitone.tmat.utils.jira.soap.RemoteProjectRole(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test38JirasoapserviceV2GetAssociatedNotificationSchemes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteScheme[] value = null;
            value = binding.getAssociatedNotificationSchemes(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProjectRole());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test39JirasoapserviceV2GetAssociatedPermissionSchemes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteScheme[] value = null;
            value = binding.getAssociatedPermissionSchemes(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProjectRole());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test40JirasoapserviceV2DeleteProject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteProject(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test41JirasoapserviceV2GetProjectById() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProject value = null;
            value = binding.getProjectById(new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test42JirasoapserviceV2GetVersions() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteVersion[] value = null;
            value = binding.getVersions(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test43JirasoapserviceV2GetCustomFields() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteField[] value = null;
            value = binding.getCustomFields(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test44JirasoapserviceV2GetComments() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteComment[] value = null;
            value = binding.getComments(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test45JirasoapserviceV2GetFavouriteFilters() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteFilter[] value = null;
            value = binding.getFavouriteFilters(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test46JirasoapserviceV2ReleaseVersion() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.releaseVersion(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteVersion());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test47JirasoapserviceV2ArchiveVersion() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.archiveVersion(new java.lang.String(), new java.lang.String(), new java.lang.String(), true);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test48JirasoapserviceV2GetFieldsForEdit() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteField[] value = null;
            value = binding.getFieldsForEdit(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test49JirasoapserviceV2GetIssueTypesForProject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssueType[] value = null;
            value = binding.getIssueTypesForProject(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test50JirasoapserviceV2GetSubTaskIssueTypesForProject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssueType[] value = null;
            value = binding.getSubTaskIssueTypesForProject(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test51JirasoapserviceV2AddUserToGroup() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.addUserToGroup(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteGroup(), new com.admitone.tmat.utils.jira.soap.RemoteUser());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test52JirasoapserviceV2RemoveUserFromGroup() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.removeUserFromGroup(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteGroup(), new com.admitone.tmat.utils.jira.soap.RemoteUser());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test53JirasoapserviceV2GetSecurityLevel() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteSecurityLevel value = null;
            value = binding.getSecurityLevel(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test54JirasoapserviceV2AddComment() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.addComment(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteComment());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test55JirasoapserviceV2GetProjectWithSchemesById() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProject value = null;
            value = binding.getProjectWithSchemesById(new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test56JirasoapserviceV2GetSecurityLevels() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteSecurityLevel[] value = null;
            value = binding.getSecurityLevels(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test57JirasoapserviceV2GetProjectAvatars() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteAvatar[] value = null;
            value = binding.getProjectAvatars(new java.lang.String(), new java.lang.String(), true);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test58JirasoapserviceV2SetProjectAvatar() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.setProjectAvatar(new java.lang.String(), new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test59JirasoapserviceV2GetProjectAvatar() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteAvatar value = null;
            value = binding.getProjectAvatar(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test60JirasoapserviceV2DeleteProjectAvatar() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteProjectAvatar(new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test61JirasoapserviceV2GetNotificationSchemes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteScheme[] value = null;
            value = binding.getNotificationSchemes(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test62JirasoapserviceV2GetPermissionSchemes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemotePermissionScheme[] value = null;
            value = binding.getPermissionSchemes(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test63JirasoapserviceV2GetAllPermissions() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemotePermission[] value = null;
            value = binding.getAllPermissions(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test64JirasoapserviceV2CreatePermissionScheme() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemotePermissionScheme value = null;
            value = binding.createPermissionScheme(new java.lang.String(), new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test65JirasoapserviceV2AddPermissionTo() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemotePermissionScheme value = null;
            value = binding.addPermissionTo(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemotePermissionScheme(), new com.admitone.tmat.utils.jira.soap.RemotePermission(), new com.admitone.tmat.utils.jira.soap.RemoteEntity());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test66JirasoapserviceV2DeletePermissionFrom() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemotePermissionScheme value = null;
            value = binding.deletePermissionFrom(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemotePermissionScheme(), new com.admitone.tmat.utils.jira.soap.RemotePermission(), new com.admitone.tmat.utils.jira.soap.RemoteEntity());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test67JirasoapserviceV2DeletePermissionScheme() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deletePermissionScheme(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test68JirasoapserviceV2CreateIssueWithSecurityLevel() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue value = null;
            value = binding.createIssueWithSecurityLevel(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteIssue(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test69JirasoapserviceV2AddAttachmentsToIssue() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            boolean value = false;
            value = binding.addAttachmentsToIssue(new java.lang.String(), new java.lang.String(), new java.lang.String[0], new byte[0][0]);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test70JirasoapserviceV2GetAttachmentsFromIssue() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteAttachment[] value = null;
            value = binding.getAttachmentsFromIssue(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test71JirasoapserviceV2HasPermissionToEditComment() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            boolean value = false;
            value = binding.hasPermissionToEditComment(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteComment());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test72JirasoapserviceV2EditComment() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteComment value = null;
            value = binding.editComment(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteComment());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test73JirasoapserviceV2GetFieldsForAction() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteField[] value = null;
            value = binding.getFieldsForAction(new java.lang.String(), new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test74JirasoapserviceV2ProgressWorkflowAction() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue value = null;
            value = binding.progressWorkflowAction(new java.lang.String(), new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteFieldValue[0]);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test75JirasoapserviceV2GetIssueById() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue value = null;
            value = binding.getIssueById(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test76JirasoapserviceV2AddWorklogWithNewRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteWorklog value = null;
            value = binding.addWorklogWithNewRemainingEstimate(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteWorklog(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test77JirasoapserviceV2AddWorklogAndAutoAdjustRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteWorklog value = null;
            value = binding.addWorklogAndAutoAdjustRemainingEstimate(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteWorklog());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test78JirasoapserviceV2AddWorklogAndRetainRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteWorklog value = null;
            value = binding.addWorklogAndRetainRemainingEstimate(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteWorklog());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test79JirasoapserviceV2DeleteWorklogWithNewRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteWorklogWithNewRemainingEstimate(new java.lang.String(), new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test80JirasoapserviceV2DeleteWorklogAndAutoAdjustRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteWorklogAndAutoAdjustRemainingEstimate(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test81JirasoapserviceV2DeleteWorklogAndRetainRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteWorklogAndRetainRemainingEstimate(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test82JirasoapserviceV2UpdateWorklogWithNewRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.updateWorklogWithNewRemainingEstimate(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteWorklog(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test83JirasoapserviceV2UpdateWorklogAndAutoAdjustRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.updateWorklogAndAutoAdjustRemainingEstimate(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteWorklog());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test84JirasoapserviceV2UpdateWorklogAndRetainRemainingEstimate() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.updateWorklogAndRetainRemainingEstimate(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteWorklog());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test85JirasoapserviceV2GetWorklogs() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteWorklog[] value = null;
            value = binding.getWorklogs(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test86JirasoapserviceV2HasPermissionToCreateWorklog() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            boolean value = false;
            value = binding.hasPermissionToCreateWorklog(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e1) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test87JirasoapserviceV2HasPermissionToDeleteWorklog() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            boolean value = false;
            value = binding.hasPermissionToDeleteWorklog(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e1) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test88JirasoapserviceV2HasPermissionToUpdateWorklog() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            boolean value = false;
            value = binding.hasPermissionToUpdateWorklog(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e1) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

    public void test89JirasoapserviceV2GetResolutionDateByKey() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.util.Calendar value = null;
            value = binding.getResolutionDateByKey(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test90JirasoapserviceV2GetResolutionDateById() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            java.util.Calendar value = null;
            value = binding.getResolutionDateById(new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test91JirasoapserviceV2GetIssueCountForFilter() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            long value = -3;
            value = binding.getIssueCountForFilter(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test92JirasoapserviceV2GetIssuesFromTextSearch() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue[] value = null;
            value = binding.getIssuesFromTextSearch(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test93JirasoapserviceV2GetIssuesFromTextSearchWithProject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue[] value = null;
            value = binding.getIssuesFromTextSearchWithProject(new java.lang.String(), new java.lang.String[0], new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test94JirasoapserviceV2GetIssuesFromJqlSearch() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue[] value = null;
            value = binding.getIssuesFromJqlSearch(new java.lang.String(), new java.lang.String(), 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test95JirasoapserviceV2DeleteUser() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteUser(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test96JirasoapserviceV2UpdateGroup() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteGroup value = null;
            value = binding.updateGroup(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteGroup());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test97JirasoapserviceV2DeleteGroup() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.deleteGroup(new java.lang.String(), new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test98JirasoapserviceV2RefreshCustomFields() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.refreshCustomFields(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test99JirasoapserviceV2GetSavedFilters() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteFilter[] value = null;
            value = binding.getSavedFilters(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test100JirasoapserviceV2AddBase64EncodedAttachmentsToIssue() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            boolean value = false;
            value = binding.addBase64EncodedAttachmentsToIssue(new java.lang.String(), new java.lang.String(), new java.lang.String[0], new java.lang.String[0]);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test101JirasoapserviceV2CreateProjectFromObject() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProject value = null;
            value = binding.createProjectFromObject(new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteProject());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteValidationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteValidationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e3) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e3);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e4) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e4);
        }
            // TBD - validate results
    }

    public void test102JirasoapserviceV2GetSecuritySchemes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteScheme[] value = null;
            value = binding.getSecuritySchemes(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test103JirasoapserviceV2AddVersion() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteVersion value = null;
            value = binding.addVersion(new java.lang.String(), new java.lang.String(), new com.admitone.tmat.utils.jira.soap.RemoteVersion());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test104JirasoapserviceV2GetIssuesFromFilter() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue[] value = null;
            value = binding.getIssuesFromFilter(new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test105JirasoapserviceV2GetIssuesFromFilterWithLimit() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue[] value = null;
            value = binding.getIssuesFromFilterWithLimit(new java.lang.String(), new java.lang.String(), 0, 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test106JirasoapserviceV2GetIssuesFromTextSearchWithLimit() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteIssue[] value = null;
            value = binding.getIssuesFromTextSearchWithLimit(new java.lang.String(), new java.lang.String(), 0, 0);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e1) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e1);
        }
            // TBD - validate results
    }

    public void test107JirasoapserviceV2GetProjectsNoSchemes() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            com.admitone.tmat.utils.jira.soap.RemoteProject[] value = null;
            value = binding.getProjectsNoSchemes(new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteAuthenticationException e2) {
            throw new junit.framework.AssertionFailedError("RemoteAuthenticationException Exception caught: " + e2);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e3) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e3);
        }
            // TBD - validate results
    }

    public void test108JirasoapserviceV2SetNewProjectAvatar() throws Exception {
        com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub binding;
        try {
            binding = (com.admitone.tmat.utils.jira.soap.JirasoapserviceV2SoapBindingStub)
                          new com.admitone.tmat.utils.jira.soap.JiraSoapServiceServiceLocator().getJirasoapserviceV2();
        }
        catch (javax.xml.rpc.ServiceException jre) {
            if(jre.getLinkedCause()!=null)
                jre.getLinkedCause().printStackTrace();
            throw new junit.framework.AssertionFailedError("JAX-RPC ServiceException caught: " + jre);
        }
        assertNotNull("binding is null", binding);

        // Time out after a minute
        binding.setTimeout(60000);

        // Test operation
        try {
            binding.setNewProjectAvatar(new java.lang.String(), new java.lang.String(), new java.lang.String(), new java.lang.String());
        }
        catch (com.admitone.tmat.utils.jira.soap.RemotePermissionException e1) {
            throw new junit.framework.AssertionFailedError("RemotePermissionException Exception caught: " + e1);
        }
        catch (com.admitone.tmat.utils.jira.soap.RemoteException e2) {
            throw new junit.framework.AssertionFailedError("RemoteException Exception caught: " + e2);
        }
            // TBD - validate results
    }

}
