package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimerTask;

import com.admitone.tmat.crawler.TicketListingCrawl;
import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.dao.DAORegistry;

public class CrawlUtils extends TimerTask {

	TicketListingCrawler ticketListingCrawler;
	
	public TicketListingCrawler getTicketListingCrawler() {
		return ticketListingCrawler;
	}

	public void setTicketListingCrawler(
			TicketListingCrawler ticketListingCrawler) {
		this.ticketListingCrawler = ticketListingCrawler;
	}

	public void removeExpiredAndDisabledCrawls() {
		
		Collection<TicketListingCrawl> disblaedCrawls = DAORegistry.getTicketListingCrawlDAO().getAllDisabled();
		Map<Integer, Boolean> disableCrawlMap = new HashMap<Integer, Boolean>();
		if(null != disblaedCrawls && !disblaedCrawls.isEmpty()){
			for (TicketListingCrawl crawl : disblaedCrawls) {
				disableCrawlMap.put(crawl.getId(), Boolean.TRUE);
			}
			System.out.println("CU: Total Disabled Crawls :"+disableCrawlMap.size());
		}
		
		Collection<TicketListingCrawl> list  = new ArrayList<TicketListingCrawl>(ticketListingCrawler.getTicketListingCrawls());
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		Date now = new Date();
		now.setTime(cal.getTimeInMillis());
		int i = 0,errCnt=0;
		for(TicketListingCrawl crawl:list){
			try {
				if((crawl.getEvent().getLocalDate() != null && crawl.getEvent().getLocalDate().before(now)) || 
						(null != disableCrawlMap.get(crawl.getId()) && disableCrawlMap.get(crawl.getId()))){
					//ticketListingCrawler.disableCrawl(crawl.getId());
					crawl.setEnabled(false);
					crawl.setLastUpdated(new Date());
					crawl.setLastUpdater("AUTO UPDATE");
					DAORegistry.getTicketDAO().disableTickets(crawl.getId());
					ticketListingCrawler.removeTicketListingCrawl(crawl);
					i++;
				}
			} catch (Exception e) {
				errCnt++;
				e.printStackTrace();
				System.out.println("ERROR Removing outdated CRL : "+crawl.getId()+" : "+ new Date());
				// TODO: handle exception
			}
		}
		System.out.println("CU: Total Removed Crawls From Cache :"+i+" : "+errCnt+" : "+ new Date());
	}
	
	
	@Override
	public void run() {
		System.out.println("CU: CrawlUtils:Removing Expired Event and Disblaed Crawls-- Job Started...."+new Date());
		removeExpiredAndDisabledCrawls();
		System.out.println("CU: CrawlUtils:Removing Expired Event and Disblaed Crawls-- Job Finished...."+new Date());
	}
	
}
