package com.admitone.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AutopricingProduct;
import com.admitone.tmat.data.Broker;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.Ticket;



public class CategoryGroupManager {
	public static Logger logger = LoggerFactory.getLogger(CategoryGroupManager.class);
	static DateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	static DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	static DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
	public static int PURCHASE_PRICE_THRESHHOLD = 10;
	
	public static List<CategoryTicket> computeCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,AutopricingProduct product) throws Exception {
		
		Date now = new Date();
//		now = df.parse(df.format(now));
		
		
		List<CategoryTicket> catTixList = new ArrayList<CategoryTicket>();
		
		Collection<Ticket> ticketList = new ArrayList<Ticket>();
		try{
			if(event.getLocalDate()!=null && ((event.getLocalDate().getTime() - now.getTime())  <= 120 * 60 * 60 * 1000 )){
				ticketList = DAORegistry.getTicketDAO().getAllActiveInstantTicketForAutoCatSalesByEventId(event.getId());
			} else {
				ticketList = DAORegistry.getTicketDAO().getAllActiveTicketForAutoCatSalesByEventId(event.getId());
			}
			
		}catch (Exception e) {
			logger.error("105.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			System.out.println("105.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			e.printStackTrace();
		}
		logger.info("NF cheapest tickets calculation started for Event: "+event.getId()+" : " + new Date());
		
		Collection<Category> categoryList = null;
		try {
			categoryList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategoryId());
//			for(Ticket t:ticketList){
//				System.out.println(t.getId()+ "::" + t.getNormalizedSection()+"::"+t.getCurrentPrice());
//			}
//			System.out.println("==========================================");
			ticketList = AutoPricingTicketUtil.removeDuplicateTickets(ticketList);
//			for(Ticket t:ticketList){
//				System.out.println(t.getId()+ ":" + t.getNormalizedSection()+":"+t.getCurrentPrice());
//			}
			ticketList = AutoPricingTicketUtil.removeAdmitoneTickets(ticketList, event, event.getAdmitoneId(),false);
//			System.out.println("==========================================");
//			for(Ticket t:ticketList){
//				System.out.println(t.getId()+ ":" + t.getNormalizedSection()+":"+t.getCurrentPrice());
//			}
			/*if(product.getName().equalsIgnoreCase("ZonesPricing")){
				if(exEvent.getTicketNetworkBrokerId().equals(5)) {//for TIX City we have to consider all tickets
					ticketList = AutoPricingTicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
				}else{
					ticketList = AutoPricingTicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
				}
			} else {
			*/	ticketList = AutoPricingTicketUtil.preAssignCategoriesToTickets(ticketList, event, categoryList);
			/*}*/
			
			//Purchase tour = DAORegistry.getPurchaseDAO().getPurchaseByEventId(event.getId());
			Collection<DefaultPurchasePrice> defaultPurchasePrices = DAORegistry.getDefaultPurchasePriceDAO().getAll();
			Map<String, DefaultPurchasePrice> defaultPurchasePriceMap = new HashMap<String, DefaultPurchasePrice>();
			for(DefaultPurchasePrice defaultPurchasePrice:defaultPurchasePrices){
				defaultPurchasePriceMap.put(defaultPurchasePrice.getExchange() + "-" + defaultPurchasePrice.getTicketType(), defaultPurchasePrice);
			}
			Collection<ManagePurchasePrice> managePurchasePricelist = DAORegistry.getManagePurchasePriceDAO().getAllManagePurchasePriceByEventId(event.getId());//tour.getTmatPurchaseId());
			Map<String, ManagePurchasePrice> tourPriceMap = new HashMap<String, ManagePurchasePrice>();
			for(ManagePurchasePrice tourPrice:managePurchasePricelist){
				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
			}
			Map<String, Set<Ticket>> sectionTicketMap = new HashMap<String, Set<Ticket>>();
//			Collection<Ticket> tempTicketList = new ArrayList<Ticket>();
			Set<Ticket> sectionTickets = null;
			for (Ticket ticket : ticketList) {
				AutoPricingTicketUtil.getPurchasePrice(ticket, defaultPurchasePriceMap, null, tourPriceMap);

				if(ticket.getPurchasePrice() < PURCHASE_PRICE_THRESHHOLD) {
//					eTicketSkipByThresHold++;
					continue;
				}
				
				CategoryMapping catmapping = ticket.getCategoryMapping();
				
				if(product.getName().equalsIgnoreCase("RewardTheFan Listings")){
					String section = ticket.getCategory().getSymbol().replaceAll("\\s+", " ").toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
					
				} else if((product.getName().equalsIgnoreCase("larrylast") && catmapping != null && catmapping.getLarrySection() != null && catmapping.getLarrySection().length() > 0 &&
						 catmapping.getLastRow() != null && catmapping.getLastRow().length() > 0) ) {
				
					String section = catmapping.getLarrySection().trim().toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>(); 
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
				} else if(product.getName().equalsIgnoreCase("minicats") || product.getName().equalsIgnoreCase("vip minicats")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
//					System.out.println(section);
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
				} else if(product.getName().equalsIgnoreCase("LastRow MiniCats") || product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase();
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					sectionTickets.add(ticket);
					
				} else if(product.getName().equalsIgnoreCase("ZonesPricing")){
					String section = ticket.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase();
					if(!exEvent.getTicketNetworkBrokerId().equals(5)) {//for TIX City we have to consider all sections
						if(!section.contains("zone")){
							continue;
						}
					}
					sectionTickets = sectionTicketMap.get(section);
					
					if(null == sectionTickets) {
						sectionTickets = new HashSet<Ticket>();
						sectionTicketMap.put(section, sectionTickets);
					}
					
					sectionTickets.add(ticket);
				}
//				tempTicketList.add(ticket);
			}
			
			if(product.getName().equalsIgnoreCase("RewardTheFan Listings")){
				getZoneTicketsCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
					vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,sectionTicketMap,catTixList );
				
			}else if(product.getName().equalsIgnoreCase("larrylast")){
				getCategoryTicketGroups(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
					vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,sectionTicketMap,catTixList );
			}else if(product.getName().equalsIgnoreCase("minicats")){
				getSectionBasedCheapestMiniCats(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,sectionTicketMap,catTixList );
			} else if(product.getName().equalsIgnoreCase("LastRow MiniCats") || product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")){
				getLastRowMiniCatCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("Vip MiniCats")){
				getSectionBasedCheapestVipCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,sectionTicketMap,catTixList,product);
			} else if(product.getName().equalsIgnoreCase("ZonesPricing")){
				getZonesPricingCategoryTickets(event,exEvent,defaultExpectedArrivialDatePriorDays,sectionEligibleMinEntries,tnExAddMarkup,
						vividExAddMarkup,tickPickExAddMarkup,scoreBigExAddMarkup,sectionTicketMap,catTixList,product);
			}
			
			
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("106.NF Event:" + event.getId() + ":-:" + event.getName() +":"+ event.getLocalDate() + e.fillInStackTrace());
			System.out.println("106.NF Event:" + event.getId() + e.fillInStackTrace());
		}
		
		return catTixList;
	}
	
	public static CategoryTicket computecategoryTicket(Set<Ticket> sectionTickets,Event event,ExchangeEvent exEvent,int quantity,
			int sectionEligibleMinEntries,Date expectedArrivalDate,double tnExAddMarkup,double vividExAddMarkup,double tickPickExAddMarkup,
			double scoreBigExAddMarkup,CategoryTicket catTicket) throws Exception {
		
//		CategoryTicket catTicket =  new CategoryTicket();
		AutoPricingTicketUtil.getCheapestTicketByQty(sectionTickets, exEvent, quantity, sectionEligibleMinEntries,catTicket);
		
		if(catTicket.getPurPrice()!=null) {
			catTicket.setQuantity(quantity);
			/*catTicket.setPosExchangeEventId(event.getAdmitoneId());*/
//			catTicket.setRowRange(rowRange);
//			catTicket.setLastRow(lastRow);
			/*catTicket.setExpectedArrivalDate(expectedArrivalDate);
			catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
			catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());*/
			//catTicket.setSection(larrySection);
			
			computecategoryTicketPrices(catTicket, exEvent, tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup);
		} else {
			catTicket = null;
//			return null;
		}
		return catTicket;
	}
	
	public static void computecategoryTicketPrices(CategoryTicket catTicket,ExchangeEvent exEvent,double tnExAddMarkup,
			double vividExAddMarkup,double tickPickExAddMarkup,double scoreBigExAddMarkup) throws Exception {
		
		
			double roundedPrice;
			double eventMarkup,eventShippingFees;
			
			if(catTicket.getPurPrice() >= exEvent.getPriceBreakup()) {
				eventMarkup = exEvent.getUpperMarkup();
				eventShippingFees = exEvent.getUpperShippingFees();
			} else {
				eventMarkup = exEvent.getLowerMarkup();
				eventShippingFees = exEvent.getLowerShippingFees();
			}
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setActualPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setZoneTicketPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tnExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTnPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+vividExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setVividPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+tickPickExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setTickpickPrice(roundedPrice);
			
			roundedPrice = (double) Math.ceil(((catTicket.getPurPrice()*(1+(eventMarkup+scoreBigExAddMarkup)/100)) + eventShippingFees/catTicket.getQuantity()) * 100 / 100);
			catTicket.setScoreBigPrice(roundedPrice);
			
	}
	
	public static List<CategoryTicket> getZoneTicketsCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList) throws Exception {

		//Map<String,CategoryMapping> rowRangeMap = Categorizer.computeRowRangeForSectionsByVenueCategoryIdOne(event);
		List<Ticket> finalSectionTickets = new ArrayList<Ticket>();
		
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			
			finalSectionTickets.clear();
			finalSectionTickets.addAll(sectionTickets);
			
			
			//Collections.sort(finalSectionTickets, AutoPricingTicketUtil.ticketSortingComparator);
			
			try {					
				finalSectionTickets.sort(new Comparator<Ticket>() {
		    @Override
		    public int compare(Ticket ticket1, Ticket ticket2) {
				int cmp = ticket1.getPurchasePrice().compareTo(
						ticket2.getPurchasePrice());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				// if same price get the one with less quantity first
				int cmp2 = ticket1.getQuantity().compareTo(
						ticket2.getQuantity());
				if (cmp2 < 0) {
					return -1;
				}

				if (cmp2 > 0) {
					return 1;
				}
				return ticket1.getId().compareTo(ticket2.getId());
		    }
		});
		}catch(Exception ex) {
			System.out.println("Error Sorting in CategoryGroupManager line 284 " + ex);
		}			
			
			
			//logger.info("NF eventId...."+event.getId()+"..section.."+key+"..rowRange.."+rowRange);
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
				
			for (int i = 1; i <= 9; i++) {
				if(i != 9) {
					int quantity = i;
					CategoryTicketGroup catTicket = new CategoryTicketGroup();
					catTicket= (CategoryTicketGroup) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
					
					if(catTicket != null) {
						catTicket.setQuantity(quantity);
						catTicket.setSection(key);
						/*catTicket.setPosExchangeEventId(event.getAdmitoneId());
						//catTicket.setRowRange(rowRange);
						//catTicket.setLastRow(lastRow);
						catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
						catTicket.setPriority(1);*/
						//catTicket.setSection(key);
						
						catTixList.add(catTicket);
					}
					if(catTicket != null){
						
						List<Ticket> nextCheapestTickets = finalSectionTickets;
						Set<Ticket> cheapestTickets = null;
						
						int fromIndex = nextCheapestTickets.indexOf(catTicket.getTicket());
						int ticketSize = nextCheapestTickets.size();
						int priority=2,j=2;
						
						while(j<ticketSize){
							
							if(priority > 5){
								break;	
							}
							cheapestTickets = new HashSet<Ticket>(nextCheapestTickets.subList(fromIndex+1, ticketSize));
							
							catTicket = new CategoryTicketGroup();
							catTicket= (CategoryTicketGroup) computecategoryTicket(cheapestTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
									tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
							
							if(catTicket != null) {
								catTicket.setSection(key);
								catTicket.setQuantity(quantity);
								/*catTicket.setPosExchangeEventId(event.getAdmitoneId());
								//catTicket.setRowRange(rowRange);
								//catTicket.setLastRow(lastRow);
								catTicket.setExpectedArrivalDate(expectedArrivalDate);
								catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
								catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());*/
								catTicket.setPriority(priority);
								//catTicket.setSection(key);
								
								catTixList.add(catTicket);
							
								fromIndex = nextCheapestTickets.indexOf(catTicket.getTicket());
								ticketSize = nextCheapestTickets.size();
								priority++;
							}else{
								ticketSize = 1;
								break;
							}
						}
					}
				}
			}
		}
		return catTixList;
	}
	
		
	public static List<CategoryTicket> getCategoryTicketGroups(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList){
		try{
			Map<String,Set<String>> larrySectionMap = DAORegistry.getQueryManagerDAO().getAllLarrySectionAndLastRowByVenueCategoryId(event.getVenueCategoryId());
			Set<Ticket> sectionTickets = null;
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			for (String  key : sectionTicketMap.keySet()) {
				sectionTickets = sectionTicketMap.get(key);
				int quantity;
//				CategoryTicket catTicket;
				
				Set<String> lastRows = larrySectionMap.get(key.toUpperCase());
				
				
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 || i == 6) {
						quantity = i;
						CategoryTicketGroup catTicket = new CategoryTicketGroup();		
						catTicket = (CategoryTicketGroup)computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
						
						if(catTicket != null) {
							
							//If LarrySection ends with SPECIAL then we have to create duplicate ticket with Sections like LEFT and RIGHT
							String sectionPrefix = null,section1 = null,section2=null;
							if(catTicket.getSection() != null && catTicket.getSection().toUpperCase().contains("SPECIAL")) {
								sectionPrefix = catTicket.getSection().trim().toUpperCase().replaceAll("SPECIAL", "");
								section1 = sectionPrefix.trim() + " LEFT";
								section2 = sectionPrefix.trim() + " RIGHT";
							} else {
								section1 = catTicket.getSection();
							}
							catTicket.setSection(section1);
							catTixList.add(catTicket);
							
							if(section2 != null) {
//								CategoryTicketGroup newCatTicket = new CategoryTicketGroup(catTicket);
								catTicket.setSection(section2);
								catTixList.add(catTicket);
							}
							// Need to work for other products..
							//If more than one different last row exist in CSV for single larrySection(CSV - G Column) we have to create create duplicate listings with all last row values.
							for (String lastRow : lastRows) {
								if(!catTicket.getLastRow().equalsIgnoreCase(lastRow)) {
//									CategoryTicketGroup newCatTicket = new CategoryTicketGroup(catTicket);
									catTicket.setLastRow(lastRow);
									catTicket.setSection(section1);
									catTixList.add(catTicket);
									
									if(section2 != null) {
//										newCatTicket = new CategoryTicketGroup(newCatTicket);
										catTicket.setSection(section2);
										catTixList.add(catTicket);
									}	
								}
							}
						}
					}
				}
			}
		}catch (Exception e) {
			System.out.println(e.fillInStackTrace());
			return null;
		}
		
		return catTixList;
	}
	
	
	public static List<CategoryTicket> getSectionBasedCheapestVipCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {
		
		
		Date expectedArrivalDate = null;
		if(event.getLocalDate()!=null) {
			Calendar cal = Calendar.getInstance();
			cal.setTime(event.getLocalDate());
			cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
			expectedArrivalDate = cal.getTime();
		}
		Map<String ,Map<String,CategoryMapping>> alternateRowMap = AutoPricingTicketUtil.getSectionBasedAlternateRows(event);
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);

			Map<String,CategoryMapping> rowMap = alternateRowMap.get(key);
			if(rowMap == null ||  rowMap.isEmpty()) {
				continue;	
			}
				
			for (String  alRow : rowMap.keySet()) {
				CategoryMapping categoryMapping = rowMap.get(alRow);
				if(categoryMapping == null) {
					continue;
				}
				List<String>  rowList = categoryMapping.getRowList();
				if(rowList == null ||  rowList.isEmpty()) {
					continue;
				}
				Set<Ticket> rowTicketList =  new HashSet<Ticket>();
				
				for (Ticket ticket : sectionTickets) {
					if(ticket.getRow()!= null && rowList.contains(ticket.getRow().replaceAll("\\s+", " ").toLowerCase())) {
						rowTicketList.add(ticket);
					}
					
					for (int i = 1; i <= 9; i++) {
						if( i ==2 || i== 4 || i == 6) {
							int quantity = i;
							CategoryTicket catTicket = new CategoryTicketGroup();
							catTicket = computecategoryTicket(rowTicketList, event, exEvent, quantity, sectionEligibleMinEntries,expectedArrivalDate,
									tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
							
							if(catTicket != null) {
								catTicket.setSection(key);
								catTicket.setQuantity(quantity);
								/*catTicket.setPosExchangeEventId(event.getAdmitoneId());*/
								catTicket.setRowRange(categoryMapping.getAlternateRow().toUpperCase());
								//catTicket.setExpectedArrivalDate(expectedArrivalDate);
								/*catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
								catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());*/
								
								catTixList.add(catTicket);
							}
						}
					}
				}
			}
		}
		return catTixList;
	}
	
	public static List<CategoryTicket> getSectionBasedCheapestMiniCats(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, List<CategoryTicket> catTixList) throws Exception {

		int eventLevelsectionEligibleCount = sectionEligibleMinEntries;

//		Date now = new Date();
//		now = df.parse(df.format(now));
		
		if(exEvent.getAllowSectionRange()) {
			sectionTicketMap = AutoPricingTicketUtil.getSectionRangeTicketsFromUniqueSections(sectionTicketMap);
		}
		
		Map<String,CategoryMapping> rowRangeMap = Categorizer.computeRowRangeForSectionsByVenueCategoryIdOne(event);
		CategoryMapping categoryMapping = null;
		
		for (String  key : sectionTicketMap.keySet()) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(key);
			
			categoryMapping = rowRangeMap.get(key);
			String rowRange = "",lastRow = ""; 
			
			if(categoryMapping != null) {
				rowRange = categoryMapping.getRowRange();
				lastRow = categoryMapping.getLastRow();
				
				if(lastRow == null) {
					lastRow = "";
				}
			}
			
			if(exEvent.getAllowSectionRange() && key.contains("-")) {
				sectionEligibleMinEntries = 1;
//				isAllowRangeSection = true;
			} else {
				sectionEligibleMinEntries = eventLevelsectionEligibleCount;
//				isAllowRangeSection = false;
			}
			
			if(rowRange.equals("")) {
				if(exEvent.getAllowSectionRange() && key.contains("-")) {
					Iterator<Ticket> iterator = sectionTickets.iterator();
					Ticket tix = null;
						
					while(iterator.hasNext()) {
						tix = iterator.next();
						if(key.equals(tix.getNormalizedSection().replaceAll("\\s+", " ").toLowerCase())) {
							break;
						}
					}
						
					if(null != tix && tix.getCategoryMapping() != null) {
						categoryMapping = rowRangeMap.get(tix.getCategoryMapping().getStartSection().replaceAll("\\s+", " ").toLowerCase());	
					}
					if(categoryMapping != null) {
						rowRange = categoryMapping.getRowRange();
						lastRow = categoryMapping.getLastRow();
							
						if(lastRow == null) {
							lastRow = "";
						}
					}
				} else {
					continue;
				}
			}
			//logger.info("NF eventId...."+event.getId()+"..section.."+key+"..rowRange.."+rowRange);
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
				
			for (int i = 1; i <= 9; i++) {
				if(i ==2 || i== 4 || i == 6) {
					int quantity = i;
					CategoryTicketGroup catTicket = new CategoryTicketGroup();
					catTicket= (CategoryTicketGroup) computecategoryTicket(sectionTickets, event, exEvent,quantity, sectionEligibleMinEntries,expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
					
					if(catTicket != null) {
						catTicket.setQuantity(quantity);
						/*catTicket.setPosExchangeEventId(event.getAdmitoneId());*/
						catTicket.setRowRange(rowRange);
						catTicket.setLastRow(lastRow);
						/*catTicket.setExpectedArrivalDate(expectedArrivalDate);
						catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
						catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());*/
						catTicket.setSection(key);
						
						catTixList.add(catTicket);
					}
				}
			}
		}
		return catTixList;
	}
	
	public static List<CategoryTicket> getLastRowMiniCatCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {

		
		try{
		
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			CategoryMapping categoryMapping = null;
			
			for (String  key : sectionTicketMap.keySet()) {
				
				categoryMapping = eligibleRowListMap.get(key);
				if(categoryMapping == null) {
					continue;
				}
				Set<String> eligibleRowList = categoryMapping.getMiniLastEligibleRows();
				String rowRange = categoryMapping.getRowRange();
				//String lastRow = categoryMapping.getLastRow();
				String miniLastRow = categoryMapping.getMiniLastRow();
					
				if(rowRange == null) {
					rowRange = "";
				}
				
				Set<Ticket> tempSectionTickets = sectionTicketMap.get(key);
				Set<Ticket> sectionTickets = new HashSet<Ticket>();
				
				for (Ticket ticket : tempSectionTickets) {
					if(ticket.getRow()!= null && eligibleRowList.contains(ticket.getRow().replaceAll("\\s+", " ").toLowerCase())) {
						sectionTickets.add(ticket);
					}
				}
				
				for (int i = 1; i <= 9; i++) {
					if(i ==2 || i== 4 || i == 6) {
						int quantity = i;
						
						CategoryTicket catTicket = null;
						if(product.getName().equalsIgnoreCase("LastRow MiniCats")) {
							catTicket = new CategoryTicketGroup();
							
						} else if(product.getName().equalsIgnoreCase("Zoned LastRow MiniCats")) {
							catTicket = new CategoryTicketGroup();
						}
						
						
						catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
								tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
						
						if(catTicket != null) {
							catTicket.setLastRow(miniLastRow);
							catTicket.setRowRange(rowRange);
							catTicket.setSection(key);
							/*catTicket.setPosExchangeEventId(event.getAdmitoneId());*/
							//catTicket.setExpectedArrivalDate(expectedArrivalDate);
							/*catTicket.setNearTermOptionId(exEvent.getNearTermDisplayOption()); 
							catTicket.setShippingMethodSpecialId(exEvent.getShippingMethod());
							*/
							catTixList.add(catTicket);
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return catTixList;
	}
	public static List<CategoryTicket> getZonesPricingCategoryTickets(Event event,ExchangeEvent exEvent,
			int defaultExpectedArrivialDatePriorDays,int sectionEligibleMinEntries,Double tnExAddMarkup,
			Double vividExAddMarkup,Double tickPickExAddMarkup,Double scoreBigExAddMarkup,Map<String, Set<Ticket>> sectionTicketMap, 
			List<CategoryTicket> catTixList,AutopricingProduct product) throws Exception {
		
		try{
			
			Date expectedArrivalDate = null;
			if(event.getLocalDate()!=null) {
				Calendar cal = Calendar.getInstance();
				cal.setTime(event.getLocalDate());
				cal.add(Calendar.DAY_OF_MONTH, - defaultExpectedArrivialDatePriorDays);
				expectedArrivalDate = cal.getTime();
			}
			
			//Map<String,CategoryMapping> eligibleRowListMap = Categorizer.getLastRowMinicatsSectionBasedEligibleTMATTicketRowsFromLastRow(event);
			//CategoryMapping categoryMapping = null;
			
			Broker broker = exEvent.getTicketNetworkBroker();
			
			if(broker.getName().equals("Joe Knows Tickets")) {
				for (String  key : sectionTicketMap.keySet()) {
					Set<Ticket> sectionTickets = sectionTicketMap.get(key);
						
					int quantity = 8;
					CategoryTicket catTicket = new CategoryTicketGroup();
					catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
							tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
					
					if(catTicket != null) {
						catTicket.setSection(key);
						catTixList.add(catTicket);
						
					}
				}
			} else if(broker.getName().equals("Reserve One Tickets")) {
				for (String  key : sectionTicketMap.keySet()) {
					Set<Ticket> sectionTickets =  sectionTicketMap.get(key);
					
					for (int i = 1; i <= 10; i++) {
						if((key.contains("premium") && i ==4) || i== 8 || i== 10) {
							int quantity = i;
							CategoryTicket catTicket = new CategoryTicketGroup();
							catTicket = computecategoryTicket(sectionTickets, event, exEvent, quantity, sectionEligibleMinEntries, expectedArrivalDate,
									tnExAddMarkup, vividExAddMarkup, tickPickExAddMarkup, scoreBigExAddMarkup,catTicket);
							if(catTicket != null) {
								catTicket.setSection(key);
								catTixList.add(catTicket);
							}
						}
					}
				}	
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return catTixList;
	}
}
