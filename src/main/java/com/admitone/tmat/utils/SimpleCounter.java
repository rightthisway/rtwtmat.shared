package com.admitone.tmat.utils;

import java.io.Serializable;

public class SimpleCounter implements Serializable{

	private static Integer count = 0;

	public static Integer getCount() {
		return count;
	}

	public static void increment(){
		count++;
	}
}
