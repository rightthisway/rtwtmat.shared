package com.admitone.tmat.utils.httpclient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.net.ssl.SSLContext;

import org.apache.commons.httpclient.HttpException;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

/**
 * This class will get latest Cookies Information from Vivid
 * @author Ulaganathan
 *
 */
public class VividSeatCookieFetcher {
	public static String staticEventId = "9530269";
	
	public static String getCookies(CloseableHttpClient httpClient, String eventId){
		String D_IID="",D_UID="",D_ZID="",D_ZUID="",D_HID="",D_SID="",ak_bmsc="", AKA_A2="",DC="",SH_AT="",STUB_SESS="",TLTSID="",TLTHID="",_abck="",bm_sz="",shDyeID="",shDyePath="";
		String originalCookies = "",SH_VI= "",SH6_USER_PREF= "",bffactivity= "";
    	String COOKIES1 = "";
    	String content = "";
		CloseableHttpResponse response1 = null;
		org.apache.http.Header[] responseHeaders = null;
		//System.out.println("=======================================================HOME - BEGINS===============================================================================");
    	HttpGet request1 = new HttpGet("https://www.vividseats.com/");
    	request1.addHeader(":authority", "www.vividseats.com");
    	request1.addHeader(":path", "/b");
    	request1.addHeader(":method", "GET");
    	request1.addHeader(":scheme", "https");
    	request1.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
    	request1.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    	request1.addHeader("Accept-Encoding", "gzip, deflate, br");
    	request1.addHeader("Accept-Language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
    	request1.addHeader("Upgrade-Insecure-Requests", "1");
    	request1.addHeader("dnt", "1");
    	try{
    		response1 = httpClient.execute(request1);
			//System.out.println("RESPONSE 1: "+response1);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response1.getEntity());
			content = EntityUtils.toString(entity);
			responseHeaders = response1.getAllHeaders();
			EntityUtils.consumeQuietly(response1.getEntity());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			try{
				if(response1 != null)
					response1.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
    	
    	int i = 0;
    	for (org.apache.http.Header header : responseHeaders) {
    		//System.out.println(header.getName() + " : " + header.getValue());
    		if(header.getName().equals("Set-Cookie")){
    			String tempArray[] = header.getValue().split(";");
    			if(null != tempArray && tempArray.length > 0){
    				if(i == 0){
    					COOKIES1 = tempArray[0];
        			}else{
        				COOKIES1 = COOKIES1+";"+tempArray[0];
        			}
    				/*if(tempArray[0].contains("D_IID")){
    					D_IID = tempArray[0];
    				}else if(tempArray[0].contains("D_UID")){
    					D_UID = tempArray[0];
    				} else if(tempArray[0].contains("D_ZID")){
    					D_ZID = tempArray[0];
    				} else if(tempArray[0].contains("D_ZUID")){
    					D_ZUID = tempArray[0];
    				} else if(tempArray[0].contains("D_HID")){
    					D_HID = tempArray[0];
    				} else if(tempArray[0].contains("D_SID")){
    					D_SID = tempArray[0];
    				} else if(tempArray[0].contains("ak_bmsc")){
    					ak_bmsc = tempArray[0];
    				} else if(tempArray[0].contains("AKA_A2")){
    					AKA_A2 = tempArray[0];
    				} else if(tempArray[0].contains("DC")){
    					DC = tempArray[0];
    				} else if(tempArray[0].contains("SH_AT")){
    					SH_AT = tempArray[0];
    				} else if(tempArray[0].contains("STUB_SESS")){
    					STUB_SESS = tempArray[0];
    				} else if(tempArray[0].contains("TLTSID")){
    					TLTSID = tempArray[0];
    				} else if(tempArray[0].contains("TLTHID")){
    					TLTHID = tempArray[0];
    				} else if(tempArray[0].contains("_abck")){
    					_abck = tempArray[0];
    				} else if(tempArray[0].contains("bm_sz")){
    					bm_sz = tempArray[0];
    				} else if(tempArray[0].contains("bff-activity")){
    					bffactivity = tempArray[0];
    				} else if(tempArray[0].contains("SH_VI")){
    					SH_VI = tempArray[0];
    				} else if(tempArray[0].contains("SH6_USER_PREF")){
    					SH6_USER_PREF = tempArray[0];
    				}*/
        			i++;
    			}
    			
    		} 
    	}
    	
    	/*System.out.println("TLTSID: "+TLTSID);
    	System.out.println("TLTHID: "+TLTHID);
    	System.out.println("_abck: "+_abck);
    	System.out.println("bm_sz: "+bm_sz);
    	System.out.println("ak_bmsc: "+ak_bmsc);
    	System.out.println("AKA_A2: "+AKA_A2);
    	System.out.println("DC: "+DC);
    	System.out.println("SH_AT: "+SH_AT);
    	System.out.println("D_IID: "+D_IID);
    	System.out.println("D_UID: "+D_UID);
    	System.out.println("D_ZID: "+D_ZID);
    	System.out.println("D_ZUID: "+D_ZUID);
    	System.out.println("D_HID: "+D_HID);
    	System.out.println("D_SID: "+D_SID);
    	System.out.println("STUB_SESS: "+STUB_SESS);
    	System.out.println("bffactivity: "+bffactivity);
    	System.out.println("SH_VI: "+SH_VI);
    	System.out.println("SH6_USER_PREF: "+SH6_USER_PREF);*/
    	
    	//System.out.println("COOKIES1: "+COOKIES1);
    	
    	originalCookies = COOKIES1;
		//System.out.println("=======================================================DISTIL JS - BEGINS===============================================================================");
		
		HttpGet request2 = new HttpGet("https://www.vividseats.com/vvdstsdstl.js");
		request2.addHeader(":authority", "www.vividseats.com");
		request2.addHeader(":path", "/vvdstsdstl.js");
		request2.addHeader(":method", "GET");
		request2.addHeader(":scheme", "https");
		request2.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
		request2.addHeader("Accept", "*/*");
		request2.addHeader("Accept-Encoding", "gzip, deflate, br");
		request2.addHeader("Accept-Language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
    	request2.addHeader("referer", "https://www.vividseats.com/");
    	request2.addHeader("dnt", "1");
    	request2.addHeader("cookies", originalCookies);
    	content = "";
    	CloseableHttpResponse response2 = null;
		responseHeaders = null;
		try{
			response2 = httpClient.execute(request2);
			//System.out.println("RESPONSE 3: "+response2);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response2.getEntity());
			content = EntityUtils.toString(entity);
			responseHeaders = response2.getAllHeaders();
			EntityUtils.consumeQuietly(response2.getEntity());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			try{
				if(response2 != null)
					response2.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		Pattern pattern = null;
		Matcher matcher = null;
    	
		pattern = Pattern.compile("path:\"(.*?)\"");
		matcher = pattern.matcher(content);
		String PID = "";
		while(matcher.find()){
			PID=matcher.group(1);
		}
		
		pattern = Pattern.compile("ajax_header:\"(.*?)\"");
		matcher = pattern.matcher(content);
		String ajax_header = "";
		while(matcher.find()){
			ajax_header=matcher.group(1);
		} 
		
		String COOKIES2 = "";
    	for (org.apache.http.Header header : responseHeaders) {
    		/*System.out.println(header.getName() 
    		      + " : " + header.getValue());  */
    		if(header.getName().equals("Set-Cookie")){
    			String tempArray[] = header.getValue().split(";");
    			if(null != tempArray && tempArray.length > 0){
    				COOKIES2 = COOKIES2+";"+tempArray[0];
    				
    				/*if(tempArray[0].contains("D_IID")){
    					D_IID = tempArray[0];
    				}else if(tempArray[0].contains("D_UID")){
    					D_UID = tempArray[0];
    				} else if(tempArray[0].contains("D_ZID")){
    					D_ZID = tempArray[0];
    				} else if(tempArray[0].contains("D_ZUID")){
    					D_ZUID = tempArray[0];
    				} else if(tempArray[0].contains("D_HID")){
    					D_HID = tempArray[0];
    				} else if(tempArray[0].contains("D_SID")){
    					D_SID = tempArray[0];
    				} else if(tempArray[0].contains("ak_bmsc")){
    					ak_bmsc = tempArray[0];
    				} else if(tempArray[0].contains("AKA_A2")){
    					AKA_A2 = tempArray[0];
    				} else if(tempArray[0].contains("DC")){
    					DC = tempArray[0];
    				} else if(tempArray[0].contains("SH_AT")){
    					SH_AT = tempArray[0];
    				} else if(tempArray[0].contains("STUB_SESS")){
    					STUB_SESS = tempArray[0];
    				} else if(tempArray[0].contains("TLTSID")){
    					TLTSID = tempArray[0];
    				} else if(tempArray[0].contains("_abck")){
    					_abck = tempArray[0];
    				} else if(tempArray[0].contains("bm_sz")){
    					bm_sz = tempArray[0];
    				} else if(tempArray[0].contains("bff-activity")){
    					bffactivity = tempArray[0];
    				} else if(tempArray[0].contains("SH_VI")){
    					SH_VI = tempArray[0];
    				} else if(tempArray[0].contains("SH6_USER_PREF")){
    					SH6_USER_PREF = tempArray[0];
    				}  else if(tempArray[0].contains("TLTHID")){
    					TLTHID = tempArray[0];
    				} */
    			}
    		} 
    	}
    	
    	/*System.out.println("TLTSID: "+TLTSID);
    	System.out.println("TLTHID: "+TLTHID);
    	System.out.println("_abck: "+_abck);
    	System.out.println("bm_sz: "+bm_sz);
    	System.out.println("ak_bmsc: "+ak_bmsc);
    	System.out.println("AKA_A2: "+AKA_A2);
    	System.out.println("DC: "+DC);
    	System.out.println("SH_AT: "+SH_AT);
    	System.out.println("D_IID: "+D_IID);
    	System.out.println("D_UID: "+D_UID);
    	System.out.println("D_ZID: "+D_ZID);
    	System.out.println("D_ZUID: "+D_ZUID);
    	System.out.println("D_HID: "+D_HID);
    	System.out.println("D_SID: "+D_SID);
    	System.out.println("STUB_SESS: "+STUB_SESS);
    	System.out.println("bffactivity: "+bffactivity);
    	System.out.println("SH_VI: "+SH_VI);
    	System.out.println("SH6_USER_PREF: "+SH6_USER_PREF);
        System.out.println("COOKIES2: "+COOKIES2);*/
        
        originalCookies = originalCookies+";"+ COOKIES2;
        
        //System.out.println("=======================================================DISTIL JS - ENDS===============================================================================");
        	
         
		/*System.out.println("PID : "+PID);
		System.out.println("ajax_header : "+ajax_header);
		
		System.out.println("=======================================================DISTIL JS PID - BEGINS===============================================================================");
    	
    	*/
		HttpPost post2 = new HttpPost("https://www.vividseats.com"+PID);
		post2.addHeader(":authority", "www.vividseats.com");
		post2.addHeader(":path", PID);
		post2.addHeader(":method", "POST");
		post2.addHeader(":scheme", "https");
    	post2.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
    	post2.addHeader("Accept", "*/*");
    	post2.addHeader("Accept-Encoding", "gzip, deflate, br");
    	post2.addHeader("Accept-Language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
    	post2.addHeader("cookies", originalCookies);
    	post2.addHeader("content-type", "text/plain;charset=UTF-8");
    	post2.addHeader("origin", "https://www.vividseats.com/");
    	post2.addHeader("x-distil-ajax", ajax_header);
    	post2.addHeader("referer", "https://www.vividseats.com/");
    	post2.addHeader("dnt", "1");
    	String q="p=%7B%22proof%22%3A%22bc%3A1533322128163%3AfX07gVJXkedSAnPQsv2n%22%2C%22fp2%22%3A%7B%22userAgent%22%3A%22Mozilla%2F5.0(WindowsNT6.1%3BWin64%3Bx64)AppleWebKit%2F537.36(KHTML%2ClikeGecko)Chrome%2F67.0.3396.99Safari%2F537.36%22%2C%22language%22%3A%22en-US%22%2C%22screen%22%3A%7B%22width%22%3A1366%2C%22height%22%3A768%2C%22availHeight%22%3A728%2C%22availWidth%22%3A1366%2C%22innerHeight%22%3A150%2C%22innerWidth%22%3A1366%2C%22outerHeight%22%3A728%2C%22outerWidth%22%3A1366%7D%2C%22timezone%22%3A5.5%2C%22indexedDb%22%3Atrue%2C%22addBehavior%22%3Afalse%2C%22openDatabase%22%3Atrue%2C%22cpuClass%22%3A%22unknown%22%2C%22platform%22%3A%22Win32%22%2C%22doNotTrack%22%3A%221%22%2C%22plugins%22%3A%22ChromePDFPlugin%3A%3APortableDocumentFormat%3A%3Aapplication%2Fx-google-chrome-pdf~pdf%3BChromePDFViewer%3A%3A%3A%3Aapplication%2Fpdf~pdf%3BNativeClient%3A%3A%3A%3Aapplication%2Fx-nacl~%2Capplication%2Fx-pnacl~%22%2C%22canvas%22%3A%7B%22winding%22%3A%22yes%22%2C%22towebp%22%3Atrue%2C%22blending%22%3Atrue%2C%22img%22%3A%2208355e8d2361a41de9a62aa60c60d1ff0126e9c9%22%7D%2C%22webGL%22%3A%7B%22img%22%3A%229debeeee077f7cfdc834330fe26be86e27393b6f%22%2C%22extensions%22%3A%22ANGLE_instanced_arrays%3BEXT_blend_minmax%3BEXT_color_buffer_half_float%3BEXT_texture_filter_anisotropic%3BWEBKIT_EXT_texture_filter_anisotropic%3BOES_element_index_uint%3BOES_standard_derivatives%3BOES_texture_float%3BOES_texture_float_linear%3BOES_texture_half_float%3BOES_texture_half_float_linear%3BOES_vertex_array_object%3BWEBGL_color_buffer_float%3BWEBGL_compressed_texture_etc1%3BWEBGL_compressed_texture_s3tc%3BWEBKIT_WEBGL_compressed_texture_s3tc%3BWEBGL_debug_renderer_info%3BWEBGL_debug_shaders%3BWEBGL_depth_texture%3BWEBKIT_WEBGL_depth_texture%3BWEBGL_draw_buffers%3BWEBGL_lose_context%3BWEBKIT_WEBGL_lose_context%22%2C%22aliasedlinewidthrange%22%3A%22%5B1%2C1%5D%22%2C%22aliasedpointsizerange%22%3A%22%5B0.125%2C8192%5D%22%2C%22alphabits%22%3A8%2C%22antialiasing%22%3A%22yes%22%2C%22bluebits%22%3A8%2C%22depthbits%22%3A24%2C%22greenbits%22%3A8%2C%22maxanisotropy%22%3A16%2C%22maxcombinedtextureimageunits%22%3A32%2C%22maxcubemaptexturesize%22%3A8192%2C%22maxfragmentuniformvectors%22%3A224%2C%22maxrenderbuffersize%22%3A8192%2C%22maxtextureimageunits%22%3A16%2C%22maxtexturesize%22%3A8192%2C%22maxvaryingvectors%22%3A32%2C%22maxvertexattribs%22%3A32%2C%22maxvertextextureimageunits%22%3A16%2C%22maxvertexuniformvectors%22%3A256%2C%22maxviewportdims%22%3A%22%5B8192%2C8192%5D%22%2C%22redbits%22%3A8%2C%22renderer%22%3A%22WebKitWebGL%22%2C%22shadinglanguageversion%22%3A%22WebGLGLSLES1.0(OpenGLESGLSLES1.0Chromium)%22%2C%22stencilbits%22%3A0%2C%22vendor%22%3A%22WebKit%22%2C%22version%22%3A%22WebGL1.0(OpenGLES2.0Chromium)%22%2C%22vertexshaderhighfloatprecision%22%3A23%2C%22vertexshaderhighfloatprecisionrangeMin%22%3A127%2C%22vertexshaderhighfloatprecisionrangeMax%22%3A127%2C%22vertexshadermediumfloatprecision%22%3A23%2C%22vertexshadermediumfloatprecisionrangeMin%22%3A127%2C%22vertexshadermediumfloatprecisionrangeMax%22%3A127%2C%22vertexshaderlowfloatprecision%22%3A23%2C%22vertexshaderlowfloatprecisionrangeMin%22%3A127%2C%22vertexshaderlowfloatprecisionrangeMax%22%3A127%2C%22fragmentshaderhighfloatprecision%22%3A23%2C%22fragmentshaderhighfloatprecisionrangeMin%22%3A127%2C%22fragmentshaderhighfloatprecisionrangeMax%22%3A127%2C%22fragmentshadermediumfloatprecision%22%3A23%2C%22fragmentshadermediumfloatprecisionrangeMin%22%3A127%2C%22fragmentshadermediumfloatprecisionrangeMax%22%3A127%2C%22fragmentshaderlowfloatprecision%22%3A23%2C%22fragmentshaderlowfloatprecisionrangeMin%22%3A127%2C%22fragmentshaderlowfloatprecisionrangeMax%22%3A127%2C%22vertexshaderhighintprecision%22%3A0%2C%22vertexshaderhighintprecisionrangeMin%22%3A31%2C%22vertexshaderhighintprecisionrangeMax%22%3A30%2C%22vertexshadermediumintprecision%22%3A0%2C%22vertexshadermediumintprecisionrangeMin%22%3A31%2C%22vertexshadermediumintprecisionrangeMax%22%3A30%2C%22vertexshaderlowintprecision%22%3A0%2C%22vertexshaderlowintprecisionrangeMin%22%3A31%2C%22vertexshaderlowintprecisionrangeMax%22%3A30%2C%22fragmentshaderhighintprecision%22%3A0%2C%22fragmentshaderhighintprecisionrangeMin%22%3A31%2C%22fragmentshaderhighintprecisionrangeMax%22%3A30%2C%22fragmentshadermediumintprecision%22%3A0%2C%22fragmentshadermediumintprecisionrangeMin%22%3A31%2C%22fragmentshadermediumintprecisionrangeMax%22%3A30%2C%22fragmentshaderlowintprecision%22%3A0%2C%22fragmentshaderlowintprecisionrangeMin%22%3A31%2C%22fragmentshaderlowintprecisionrangeMax%22%3A30%7D%2C%22touch%22%3A%7B%22maxTouchPoints%22%3A0%2C%22touchEvent%22%3Afalse%2C%22touchStart%22%3Afalse%7D%2C%22video%22%3A%7B%22ogg%22%3A%22probably%22%2C%22h264%22%3A%22probably%22%2C%22webm%22%3A%22probably%22%7D%2C%22audio%22%3A%7B%22ogg%22%3A%22probably%22%2C%22mp3%22%3A%22probably%22%2C%22wav%22%3A%22probably%22%2C%22m4a%22%3A%22maybe%22%7D%2C%22vendor%22%3A%22GoogleInc.%22%2C%22product%22%3A%22Gecko%22%2C%22productSub%22%3A%2220030107%22%2C%22browser%22%3A%7B%22ie%22%3Afalse%2C%22chrome%22%3Atrue%2C%22webdriver%22%3Afalse%7D%2C%22fonts%22%3A%22Batang%3BCalibri%3BCentury%3BHaettenschweiler%3BLeelawadee%3BMarlett%3BPMingLiU%3BPristina%3BSimHei%3BVrinda%22%7D%2C%22cookies%22%3A1%2C%22setTimeout%22%3A0%2C%22setInterval%22%3A0%2C%22appName%22%3A%22Netscape%22%2C%22platform%22%3A%22Win32%22%2C%22syslang%22%3A%22en-US%22%2C%22userlang%22%3A%22en-US%22%2C%22cpu%22%3A%22%22%2C%22productSub%22%3A%2220030107%22%2C%22plugins%22%3A%7B%220%22%3A%22ChromePDFPlugin%22%2C%221%22%3A%22ChromePDFViewer%22%2C%222%22%3A%22NativeClient%22%7D%2C%22mimeTypes%22%3A%7B%220%22%3A%22application%2Fpdf%22%2C%221%22%3A%22PortableDocumentFormatapplication%2Fx-google-chrome-pdf%22%2C%222%22%3A%22NativeClientExecutableapplication%2Fx-nacl%22%2C%223%22%3A%22PortableNativeClientExecutableapplication%2Fx-pnacl%22%7D%2C%22screen%22%3A%7B%22width%22%3A1366%2C%22height%22%3A768%2C%22colorDepth%22%3A24%7D%2C%22fonts%22%3A%7B%220%22%3A%22Calibri%22%2C%221%22%3A%22Cambria%22%2C%222%22%3A%22Constantia%22%2C%223%22%3A%22LucidaBright%22%2C%224%22%3A%22DejaVuSerif%22%2C%225%22%3A%22Georgia%22%2C%226%22%3A%22SegoeUI%22%2C%227%22%3A%22Candara%22%2C%228%22%3A%22DejaVuSans%22%2C%229%22%3A%22TrebuchetMS%22%2C%2210%22%3A%22Verdana%22%2C%2211%22%3A%22Consolas%22%2C%2212%22%3A%22LucidaConsole%22%2C%2213%22%3A%22LucidaSansTypewriter%22%2C%2214%22%3A%22DejaVuSansMono%22%2C%2215%22%3A%22CourierNew%22%2C%2216%22%3A%22Courier%22%7D%7D";
    	try {
    		post2.setEntity(new StringEntity(q));
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		Integer leng = q.length();
    	System.out.println(leng);
    	post2.addHeader("content-length", leng.toString());
    	CloseableHttpResponse response8 = null;
    	content = "";
		responseHeaders = null;
		try{
			response8 = httpClient.execute(post2);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response8.getEntity());
			content = EntityUtils.toString(entity);
			//System.out.println("RESPONSE 8: "+response8);
			responseHeaders = response8.getAllHeaders();
			EntityUtils.consumeQuietly(response8.getEntity());
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response8 != null)
				try {
					response8.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		
		String COOKIES8 = "";
		i = 0;
    	for (org.apache.http.Header header : responseHeaders) {
    		
    		/*System.out.println(header.getName() 
    		      + " : " + header.getValue());  */
    		
    		if(header.getName().equals("Set-Cookie")){
    			
    			String tempArray[] = header.getValue().split(";");
    			
    			if(null != tempArray && tempArray.length > 0){
    				/*if(tempArray[0].contains("D_IID")){
    					D_IID = tempArray[0];
    				}else if(tempArray[0].contains("D_UID")){
    					D_UID = tempArray[0];
    				} else if(tempArray[0].contains("D_ZID")){
    					D_ZID = tempArray[0];
    				} else if(tempArray[0].contains("D_ZUID")){
    					D_ZUID = tempArray[0];
    				} else if(tempArray[0].contains("D_HID")){
    					D_HID = tempArray[0];
    				} else if(tempArray[0].contains("D_SID")){
    					D_SID = tempArray[0];
    				} else if(tempArray[0].contains("ak_bmsc")){
    					ak_bmsc = tempArray[0];
    				} else if(tempArray[0].contains("AKA_A2")){
    					AKA_A2 = tempArray[0];
    				} else if(tempArray[0].contains("DC")){
    					DC = tempArray[0];
    				} else if(tempArray[0].contains("SH_AT")){
    					SH_AT = tempArray[0];
    				} else if(tempArray[0].contains("STUB_SESS")){
    					STUB_SESS = tempArray[0];
    				} else if(tempArray[0].contains("TLTSID")){
    					TLTSID = tempArray[0];
    				} else if(tempArray[0].contains("_abck")){
    					_abck = tempArray[0];
    				} else if(tempArray[0].contains("bm_sz")){
    					bm_sz = tempArray[0];
    				} else if(tempArray[0].contains("bff-activity")){
    					bffactivity = tempArray[0];
    				} else if(tempArray[0].contains("SH_VI")){
    					SH_VI = tempArray[0];
    				} else if(tempArray[0].contains("SH6_USER_PREF")){
    					SH6_USER_PREF = tempArray[0];
    				}   else if(tempArray[0].contains("TLTHID")){
    					TLTHID = tempArray[0];
    				}*/
    				COOKIES8 = COOKIES8+";"+tempArray[0];
        			i++;
    			}
    		} 
    	}
    	
    	/*System.out.println("TLTSID: "+TLTSID);
    	System.out.println("TLTHID: "+TLTHID);
    	System.out.println("_abck: "+_abck);
    	System.out.println("bm_sz: "+bm_sz);
    	System.out.println("ak_bmsc: "+ak_bmsc);
    	System.out.println("AKA_A2: "+AKA_A2);
    	System.out.println("DC: "+DC);
    	System.out.println("SH_AT: "+SH_AT);
    	System.out.println("D_IID: "+D_IID);
    	System.out.println("D_UID: "+D_UID);
    	System.out.println("D_ZID: "+D_ZID);
    	System.out.println("D_ZUID: "+D_ZUID);
    	System.out.println("D_HID: "+D_HID);
    	System.out.println("D_SID: "+D_SID);
    	System.out.println("STUB_SESS: "+STUB_SESS);
    	System.out.println("bffactivity: "+bffactivity);
    	System.out.println("SH_VI: "+SH_VI);
    	System.out.println("SH6_USER_PREF: "+SH6_USER_PREF);
    	System.out.println("COOKIES8 : "+COOKIES8);
    	
    	originalCookies = originalCookies  + COOKIES8;
    	
    	System.out.println("=======================================================DISTIL JS PID - ENDS===============================================================================");
    	
    	
    	 */
      /*  System.out.println("=======================================================INVENTORY - BEGINS===============================================================================");
		
    	System.out.println("TLTSID: "+TLTSID);
    	System.out.println("TLTHID: "+TLTHID);
    	System.out.println("_abck: "+_abck);
    	System.out.println("bm_sz: "+bm_sz);
    	System.out.println("ak_bmsc: "+ak_bmsc);
    	System.out.println("AKA_A2: "+AKA_A2);
    	System.out.println("DC: "+DC);
    	System.out.println("SH_AT: "+SH_AT);
    	System.out.println("D_IID: "+D_IID);
    	System.out.println("D_UID: "+D_UID);
    	System.out.println("D_ZID: "+D_ZID);
    	System.out.println("D_ZUID: "+D_ZUID);
    	System.out.println("D_HID: "+D_HID);
    	System.out.println("D_SID: "+D_SID);
    	System.out.println("STUB_SESS: "+STUB_SESS);
    	System.out.println("bffactivity: "+bffactivity);
    	System.out.println("SH_VI: "+SH_VI);
    	System.out.println("SH6_USER_PREF: "+SH6_USER_PREF);
    	
    	String invCookie = bffactivity+";"+SH_VI+";"+SH6_USER_PREF+";"+TLTSID+";"+TLTHID+";"+_abck+";"+bm_sz+";"+ak_bmsc+";"+AKA_A2+";"+DC+";"+SH_AT+";"+D_IID+";"+D_UID+";"+ D_ZID+";"+D_ZUID+";"+ D_HID+";"+D_SID+";"+STUB_SESS;
    	
    	System.out.println("INVENTORY COOKIES:  "+invCookie);
    	System.out.println("com-stubhub-dye : "+shDyeID);
    	System.out.println("com-stubhub-dye-path : "+shDyePath);
    	
    	
    	String url = "https://www.vividseats.com/rest/v2/web/listings/2378130";
    	HttpGet request11 = new HttpGet(url);
    	request11.addHeader("Host", "www.vividseats.com");
    	request11.addHeader("user-agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
    	request11.addHeader(":authority", "www.vividseats.com");
    	request11.addHeader(":method", "GET");
    	request11.addHeader(":path", "/rest/v2/web/listings/2607783");
    	request11.addHeader(":scheme", "https");
    	request11.addHeader("accept", "application/json, text/plain");
    	request11.addHeader("accept-encoding", "gzip, deflate, br");
    	request11.addHeader("accept-language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
    	request11.addHeader("Content-Type", "application/json");
    	request11.addHeader("referer", "www.vividseat.com/theatre/on-your-feet-tickets/on-your-feet-9-26-2378130.html");
    	request11.addHeader("x-distil-ajax", "uurrvyqyyqxrtytrzw");
    	//request11.addHeader("x-requested-with", "XMLHttpRequest");
    	request11.addHeader("cookie",originalCookies);
    	request11.addHeader("dnt", "1");
    	JSONObject jsonObject = null;
    	CloseableHttpResponse response11 = null;
    	
		try{
			response11 = httpClient.execute(request11);
			System.out.println("INVETORY--->"+response11);
			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response11.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				content = EntityUtils.toString(entity);
				System.out.println(content);
				responseHeaders = response11.getAllHeaders();
				EntityUtils.consumeQuietly(entity);
			}
			
			jsonObject = new JSONObject(content);
			success++;
		}catch(Exception e){
			e.printStackTrace();
			failure++;
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response11 != null)
				try {
					response11.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}*/
		 
    	//System.out.println("=======================================================INVENTORY - ENDS===============================================================================");
        return originalCookies;
    }
	 
	
	public static void main(String[] args) throws Exception {
		
		
		SSLContext sslcontext= SSLContexts.custom().loadTrustMaterial(null,
	            new TrustSelfSignedStrategy()).build();
		SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
		        new String[] { "TLSv1" , "TLSv1.1", "TLSv1.2"  }, null, null);
		
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		HttpRequestRetryHandler myReTryHandler = new HttpRequestRetryHandler() {
			@Override
			public boolean retryRequest(IOException exception, int executionCount,
					HttpContext context) {
				if(exception!=null){
				}
				return false;
			}
		};
		
		for(int i=50;i<=60;i++){
			String host = "us-s"+i+".netnut.io";
			Integer port = 33128;
			///host = "64.120.85.238";
			//port = 19051;
			
			int j = 10;
			j = j + i;
			HttpHost proxy = new HttpHost(host,port);
			credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials("Rightthisway!a"+j, "Qpox9d"));
			RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).setSocketTimeout(60*1000).build();
	        CloseableHttpClient client = HttpClients.custom().setProxy(proxy).
			setRetryHandler(myReTryHandler).
			setDefaultCredentialsProvider(credsProvider).
			setDefaultRequestConfig(config).
			setSSLSocketFactory(sslsf).
			addInterceptorFirst(new RemoveSoapHeadersInterceptor()).
			
			build();
	        try{
	        	//getJsonTicketListingsResponse(client);
	        	
	        	getCookies(client, null);
	        	//getCookies(client, null);
	        }catch(Exception e){
	        	e.printStackTrace();
	        }
	        client.close();
		}
		
	}
	 
	
	private static class RemoveSoapHeadersInterceptor implements HttpRequestInterceptor {

        public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
            if (request instanceof HttpEntityEnclosingRequest) {
                if (request.containsHeader(HTTP.TRANSFER_ENCODING)) {
                    request.removeHeaders(HTTP.TRANSFER_ENCODING);
                }
                if (request.containsHeader(HTTP.CONTENT_LEN)) {
                    request.removeHeaders(HTTP.CONTENT_LEN);
                }
            }
        }
    }
	}
