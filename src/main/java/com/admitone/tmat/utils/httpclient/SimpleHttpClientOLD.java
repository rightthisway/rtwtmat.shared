package com.admitone.tmat.utils.httpclient;

import java.io.IOException;
import java.util.Date;

import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.http.params.HttpParams;

public class SimpleHttpClientOLD extends DefaultHttpClient {
	public static final String DEFAULT = "default";
	
	private Date created;
	private boolean used = true;
	
	// true if the httpClient is valid
	private boolean valid = true;
	private String siteId;
	
	public SimpleHttpClientOLD(String siteId) {
		super();
		this.created = new Date();
		this.siteId = siteId;
		
		setUsed(true);

		reset();
	}
	
	public SimpleHttpClientOLD(String siteId, ClientConnectionManager cm, HttpParams hp) {
		super(cm,hp);
		this.created = new Date();
		this.siteId = siteId;
		
		setUsed(true);

		reset();
	}
	
	public void reset() {
		/*
		// force to not keep connection alive
		
		setKeepAliveStrategy(new ConnectionKeepAliveStrategy() {
			@Override
			public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
				return 300000;
			}			
		});
	
		// force to not reuse connections
		setReuseStrategy(new ConnectionReuseStrategy() {
			@Override
			public boolean keepAlive(HttpResponse response, HttpContext context) {
				return true;
			}
		});	
		*/
		
		// TODO: matybe some other parameter to tune:
		// http://wiki.apache.org/HttpComponents/HttpClientTutorial
		
		// set timeout to 30s
		getParams().setIntParameter(CoreConnectionPNames.SO_TIMEOUT, 30000);
		getParams().setIntParameter(CoreConnectionPNames.CONNECTION_TIMEOUT, 30000);
		
		// Determines whether Nagle's algorithm is to be used. The Nagle's algorithm tries to conserve
		// bandwidth by minimizing the number of segments that are sent. When applications wish to
		// decrease network latency and increase performance, they can disable Nagle's algorithm (that is
		// enable TCP_NODELAY). Data will be sent earlier, at the cost of an increase in bandwidth consumption. 
		// getParams().setBooleanParameter(CoreConnectionPNames.TCP_NODELAY, true);		
		// getParams().setBooleanParameter(CoreConnectionPNames.STALE_CONNECTION_CHECK, false);		
		
		getCookieStore().clear();
		getCredentialsProvider().clear();
		setRedirectHandler(null);				
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public String getSiteId() {
		return siteId;
	}
	
	private void addExtraHttpRequestParameters(HttpRequest request) {
		request.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		request.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");	
		request.addHeader("Accept-Language", "en-us,en;q=0.5");
	}
	
	public HttpResponse execute(HttpGet request) throws IOException, ClientProtocolException {
		return execute(request, true);
	}
	
	public HttpResponse execute(HttpPost request) throws IOException, ClientProtocolException {
		return execute(request, true);
	}

	public HttpResponse __execute(HttpUriRequest request, boolean addExtraHeaders) throws IOException, ClientProtocolException {
		try {
			if (addExtraHeaders) {
				addExtraHttpRequestParameters(request);
			}
			return super.execute(request);
		} catch (IOException e) {
			valid = false;
			System.out.println("HTTP CLIENT ERROR: " + e.getMessage() + "for url=" + request.getRequestLine());
			throw e;
		}		
	}

	// dirty trick to override the execute method as the execute method of AbstractHttpClient is final.
	// use HttpGet and HttpPost instead of HttpRequestURI
	public HttpResponse execute(HttpGet request, boolean addExtraHeaders)
			throws IOException, ClientProtocolException {
		return __execute(request, addExtraHeaders);
	}

	public HttpResponse execute(HttpPost request, boolean addExtraHeaders)
			throws IOException, ClientProtocolException {
		return __execute(request, addExtraHeaders);
	}	
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public boolean isValid() {
		return valid;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}
	
}

