package com.admitone.tmat.utils.httpclient;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.Inflater;
import java.util.zip.InflaterInputStream;

import org.apache.http.HttpEntity;
import org.apache.http.entity.HttpEntityWrapper;

/**
 * This class provides an uncompressed http entity content.
 * It handles gzip and deflate compression.
 */
public class UncompressedHttpEntity extends HttpEntityWrapper {
	private InputStreamWrapper wrappedInputStream;
	private String codec;

	public UncompressedHttpEntity(HttpEntity entity) {
		super(entity);
	}

	public void setCodec(String codec) {
		this.codec = codec;
	}
	
    public InputStream getContent()
    		throws IOException, IllegalStateException {

	    // the wrapped entity's getContent() decides about repeatability
	    wrappedInputStream = new InputStreamWrapper(wrappedEntity.getContent());
	    
	    if ("gzip".equalsIgnoreCase(codec)) {
	    	return new GZIPInputStream(wrappedInputStream);
	    } else if ("deflate".equalsIgnoreCase(codec)) {
            return new InflaterInputStream(wrappedInputStream, new Inflater(true));		    	
	    } else if ("zlip".equalsIgnoreCase(codec)) {
	    	return new InflaterInputStream(wrappedInputStream);
	    } else {
	    	return wrappedInputStream;
	    }
    }

    public long getContentLength() {
    	return -1;
    }
    
    public int getReadByteCount() {
    	if (wrappedInputStream == null) {
    		return 0;
    	}
    	return wrappedInputStream.getReadByteCount();
    }	
}

/**
 * Input Stream Wrapper to count read bytes. 
 */
class InputStreamWrapper extends InputStream {
	private InputStream inputStream;
	private int readByteCount;
	
	public InputStreamWrapper(InputStream inputStream) {
		this.inputStream = inputStream;
		readByteCount = 0;
	}

	@Override
	public int read() throws IOException {
		int ch =  inputStream.read();
		readByteCount++;
		return ch;
	}


	public int getReadByteCount() {
		return readByteCount;
	}
	
}
