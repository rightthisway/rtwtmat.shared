package com.admitone.tmat.utils.httpclient;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.SSLContext;

import org.apache.commons.httpclient.HttpException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Proxy;
import com.admitone.tmat.data.Site;

/**
 * This class will get latest proxies list from db every 24 hrs
 * @author cshah
 *
 */
public class ChangeProxy extends TimerTask implements InitializingBean{
//		private HttpClientStore httpClientStore;
		private static Logger logger = LoggerFactory.getLogger(ChangeProxy.class);
		private static Collection<Proxy> ipPool= DAORegistry.getProxyDAO().getAllProxy();
		//private static Collection<Proxy> ipPool= new ArrayList<Proxy>();
		private static String NETNUT_DEFAULT_USERNAME="Rightthisway";
		private static String NETNUT_USERNAME="Rightthisway";
		private static String NETNUT_PASSWORD = "Qpox9d";
		private static Map<String, Integer> netNutStickyMap = new HashMap<String, Integer>();;
		private static final int TIMER_INTERVAL = 24 * 60 * 60 * 1000;  // 24*60  minutes
		private static Iterator<Proxy> it=ipPool.iterator();
		private static String currentStickyCountry = null;
		private static Integer currentStickyNum = null;
		private static boolean forceStickyIndex = false;
		private static Integer currentStickyUserIndex = null;
		private static String currentStickyHost = null;
		
		public ChangeProxy() {

		}
		
		@Override
		public void run() {
			ipPool= DAORegistry.getProxyDAO().getAllProxy();
			it=ipPool.iterator();
			
			try{
				getNetNutStickyIps();
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
		
		
		
		public static void getNetNutStickyIps() throws Exception{
			
			SimpleHttpClient client = HttpClientStore.createHttpClient();
			
    		HttpGet request = new HttpGet(String.format(
	                "https://dashboard.netnut.io/DashboardDataServices.asmx/GetProxyRanges?userName=%s&password=%s",
	                "Rightthisway","Qpox9d")); 
			try {
				HttpResponse response = client.execute(request); 
				String result = EntityUtils.toString(response.getEntity());
				//System.out.println(result);
				JSONObject jsonObject = XML.toJSONObject(result);
				System.out.println(jsonObject);
				Map<String, Integer> netNutStickyMapTemp = new HashMap<String, Integer>();;
				if(jsonObject.has("ProxyRange") && jsonObject.has("ProxyRange")){
		 			JSONArray docsJSONArray =  jsonObject.getJSONObject("ProxyRange").getJSONObject("Countries").getJSONArray("Country");
		 			for (int i = 0 ; i < docsJSONArray.length() ; i++) {
		 				JSONObject docJSONObject = docsJSONArray.getJSONObject(i);
		 				String countryCode = docJSONObject.getString("CountryCode");
		 				if(countryCode.equalsIgnoreCase("US") /*|| countryCode.equalsIgnoreCase("IN")
		 						|| countryCode.equalsIgnoreCase("NP") || countryCode.equalsIgnoreCase("DE")
		 						|| countryCode.equalsIgnoreCase("ca") || countryCode.equalsIgnoreCase("ca")*/){
		 					String countryName = docJSONObject.getString("CountryName");
			 				String maxCountryS = docJSONObject.getString("MaxCountryS");
			 				netNutStickyMapTemp.put(countryCode.toLowerCase(), Integer.parseInt(maxCountryS));
			 				//netNutStickyMapTemp.put(countryCode.toLowerCase(), 5);
		 				}
		 			}
		 			netNutStickyMap = new HashMap<String, Integer>();
		 			netNutStickyMap.putAll(netNutStickyMapTemp);
				}
				
			} catch (ClientProtocolException e) {
				if(e.getMessage().equals("Unauthorized")){
				}
			} catch (IOException e) {
				System.err.println("Error while getting Proxy from NetNut : " + e.getMessage());
				System.err.println("Complete Message : " );
//					e.printStackTrace();
				logger.info("Error while getting Proxy from NetNut : " + e.getMessage());
			}finally{
//					if(request != null){
//						request. = null;
//					}
			}
		}
		
		public static String getNetNutHostNameOld(){
			String host="",userName="";
			
			try{
				if(currentStickyCountry == null || currentStickyCountry.isEmpty()){
					currentStickyNum = 2;
					currentStickyCountry = "us"; 
					host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
				}else{
					
						if(null == netNutStickyMap || netNutStickyMap.isEmpty()){
							netNutStickyMap.clear();
							netNutStickyMap = new HashMap<String, Integer>();
							try{
								getNetNutStickyIps();
							}catch(Exception e){
								e.printStackTrace();
							}
						}
						Integer maxNum = netNutStickyMap.get(currentStickyCountry);
						if(maxNum == null){
							maxNum =0;
							//currentStickyNum = 2;
						}
						if(currentStickyNum < maxNum){
							currentStickyNum++;
							if(currentStickyNum == 5){
								currentStickyNum = 6;
							}
							host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
						}else{
							netNutStickyMap.remove(currentStickyCountry);
							if(null == netNutStickyMap || netNutStickyMap.isEmpty() || netNutStickyMap.size() <= 0){
								try{
									getNetNutStickyIps();
								}catch(Exception e){
									e.printStackTrace();
								}
							}
							for (String key : netNutStickyMap.keySet()) {
								currentStickyCountry = key;
								break;
							}
							currentStickyNum =2; 
							host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
						}
				}
			}catch(Exception e){
				e.printStackTrace();
				host = "us-s70.netnut.io";
				userName= "Rightthisway";
			}
			
			
			if(host == null || host.isEmpty()){
				host = "us-s70.netnut.io";
				userName= "Rightthisway";
			}
			
			//NETNUT_USERNAME = userName;
			currentStickyHost= host;
			return host;
		}
		
		public static String getNetNutHostNameForAllCountry(){
			String host="",userName="";
			
			try{
				if(currentStickyCountry == null || currentStickyCountry.isEmpty()){
					currentStickyNum = 2;
					currentStickyCountry = "us";
					currentStickyUserIndex = 2;
					host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
					userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
				}else{
					if(currentStickyUserIndex < 254){
						currentStickyUserIndex++;
						host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
						userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
					}else{
						if(null == netNutStickyMap || netNutStickyMap.isEmpty()){
							try{
								getNetNutStickyIps();
							}catch(Exception e){
								e.printStackTrace();
							}
							currentStickyNum = 2;
							currentStickyCountry = "us";
							currentStickyUserIndex = 2;
							host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
							userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
						}else{
							Integer maxNum = netNutStickyMap.get(currentStickyCountry);
							if(maxNum == null){
								currentStickyNum = 2;
								currentStickyUserIndex=2;
								host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
								userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
							}else if(currentStickyNum < maxNum){
								currentStickyNum++;
								currentStickyUserIndex=2;
								host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
								userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
							}else{
								netNutStickyMap.remove(currentStickyCountry);
								if(null == netNutStickyMap || netNutStickyMap.isEmpty() || netNutStickyMap.size() <= 0){
									netNutStickyMap.clear();
									netNutStickyMap = new HashMap<String, Integer>();
									try{
										getNetNutStickyIps();
									}catch(Exception e){
										e.printStackTrace();
									}
								}
								for (String key : netNutStickyMap.keySet()) {
									currentStickyCountry = key;
									break;
								}
								currentStickyNum =2;
								currentStickyUserIndex=2; 
								host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
								userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				host = "us-s70.netnut.io";
				userName= "Rightthisway";
			}
			
			
			if(host == null || host.isEmpty()){
				host = "us-s70.netnut.io";
				userName= "Rightthisway";
			}
			
			NETNUT_USERNAME = userName;
			System.out.println("NETNUT_USERNAME----->"+NETNUT_USERNAME);
			currentStickyHost= host;
			return host;
		}
		
		public static void forcefullyIncreaseStickyServerNum(){
			//currentStickyNum = currentStickyNum + 1;
			//currentStickyUserIndex = 2;
			forceStickyIndex = true;
		}
		
		public static void main(String[] args) throws Exception {
			
			getNetNutStickyIps();
			
			for (int i=1;i<100;i++) {
				if(i==5){
					forcefullyIncreaseStickyServerNum();
				}
				System.out.println("ForceStickyIndix------>"+forceStickyIndex+", Previous Index: "+currentStickyNum);
				
				getNetNutStickyHost();
				
				System.out.println("currentStickyCountry------>"+currentStickyCountry);
				System.out.println("currentStickyNum------>"+currentStickyNum);
				System.out.println("currentStickyUserIndex------>"+currentStickyUserIndex);
				System.out.println("NETNUT_USERNAME------>"+NETNUT_USERNAME);
				System.out.println("currentStickyHost------>"+currentStickyHost);
				
				System.out.println(i+"==============================================================="+i);
			}
		}
		
		
		
		public static String getNetNutStickyHost(){
			String host="",userName="";
			try{
				if(currentStickyCountry == null || currentStickyCountry.isEmpty()){
					try{
						getNetNutStickyIps();
					}catch(Exception e){
						e.printStackTrace();
						netNutStickyMap = new HashMap<String, Integer>();
			 			netNutStickyMap.put("us",100);
					}
					currentStickyNum = 3;
					currentStickyCountry = "us";
					currentStickyUserIndex = 4;
					host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
					userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
					NETNUT_USERNAME = userName;
					currentStickyHost= host;
					return host;
				}else{
					
					if(forceStickyIndex){
						Integer maxNum = netNutStickyMap.get(currentStickyCountry);
						forceStickyIndex= false;
						if(currentStickyNum < maxNum){
							currentStickyNum++;
							currentStickyUserIndex=2;
							host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
							userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
						}else{
							currentStickyCountry = null;
							currentStickyUserIndex = null;
							return getNetNutStickyHost();
						}
						
					}else{
						
						if(currentStickyUserIndex < 254){
							try{
								Integer maxNum = netNutStickyMap.get(currentStickyCountry);
								if(currentStickyNum > maxNum){
									currentStickyCountry = null;
									currentStickyUserIndex = null;
									return getNetNutStickyHost();
								}
							}catch(Exception e){
								currentStickyCountry = null;
								currentStickyUserIndex = null;
								return getNetNutStickyHost();
							}
							
							currentStickyUserIndex++;
							host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
							userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
							
						}else{
							Integer maxNum = netNutStickyMap.get(currentStickyCountry);
							if(currentStickyNum < maxNum){
								currentStickyNum++;
								currentStickyUserIndex=2;
								host = currentStickyCountry+"-s"+currentStickyNum+".netnut.io";
								userName = NETNUT_DEFAULT_USERNAME+"!a"+currentStickyUserIndex;
							}else{
								currentStickyCountry = null;
								currentStickyUserIndex = null;
								return getNetNutStickyHost();
							}
						}
					}
				}
			}catch(Exception e){
				e.printStackTrace();
				currentStickyCountry = null;
				currentStickyUserIndex = null;
				return getNetNutStickyHost();
			}
			
			
			if(host == null || host.isEmpty()){
				currentStickyCountry = null;
				currentStickyUserIndex = null;
				return getNetNutStickyHost();
			}
			
			NETNUT_USERNAME = userName;
			currentStickyHost= host;
			return host;
		}
		
		public static void changeLuminatiProxy(SimpleHttpClient httpClient,boolean isBestBroxy) throws Exception {
			boolean flag =false;
			String result =null;
			int i = 0;
			HttpHost proxy = null;
	    	while(i<5 && !flag && !isBestBroxy){
	    		i++;
				try {
					proxy = new HttpHost(getNetNutStickyHost(), 33128);
					flag = true;
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    	}
	    	if(flag && !isBestBroxy){
	    		httpClient.setUserName(ChangeProxy.getNETNUT_USERNAME());
	    		httpClient.setPassword(ChangeProxy.getNETNUT_PASSWORD());
	    		
	    		httpClient.setHost(proxy.getHostName());
	    		httpClient.setPort(33128);  // NetNut uses static port
	    		CredentialsProvider credsProvider = new BasicCredentialsProvider();
	    		HttpRequestRetryHandler myReTryHandler = new HttpRequestRetryHandler() {
	    			@Override
	    			public boolean retryRequest(IOException exception, int executionCount,
	    					HttpContext context) {
	    				if(exception!=null){
	    					logger.info("IO Exception while executing request : " + exception.fillInStackTrace());
	    				}
	    				return false;
	    			}
	    		};
	    		if(httpClient.getSiteId().equalsIgnoreCase("stubhub")) {
	    			SSLContext sslcontext= SSLContexts.custom().loadTrustMaterial(null,
	    		            new TrustSelfSignedStrategy()).build();
	    			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
	    			        new String[] { "TLSv1" , "TLSv1.1", "TLSv1.2"  }, null, null);
	    			
	    			credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(httpClient.getUserName(), httpClient.getPassword()));
	    			RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).setSocketTimeout(60*1000).build();
	    	        httpClient.setCloseableHttpClient(HttpClients.custom().setProxy(proxy).
	    			setRetryHandler(myReTryHandler).
	    			setDefaultCredentialsProvider(credsProvider).
	    			setDefaultRequestConfig(config).
	    			setSSLSocketFactory(sslsf).
	    			addInterceptorFirst(new RemoveSoapHeadersInterceptor()).
	    			build());
	    			
	    		} else if(httpClient.getSiteId().equalsIgnoreCase("vividseat")) {
	    			SSLContext sslcontext= SSLContexts.custom().loadTrustMaterial(null,
	    		            new TrustSelfSignedStrategy()).build();
	    			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
	    			        new String[] { "TLSv1" , "TLSv1.1", "TLSv1.2"  }, null, null);
	    			
	    			credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(httpClient.getUserName(), httpClient.getPassword()));
	    			RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).setSocketTimeout(60*1000).build();
	    	        httpClient.setCloseableHttpClient(HttpClients.custom().setProxy(proxy).
	    			setRetryHandler(myReTryHandler).
	    			setDefaultCredentialsProvider(credsProvider).
	    			setDefaultRequestConfig(config).
	    			setSSLSocketFactory(sslsf).
	    			addInterceptorFirst(new RemoveSoapHeadersInterceptor()).
	    			build());
	    			
	    		} else {

		    		credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(httpClient.getUserName(), httpClient.getPassword()));
		            RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).build();
		    		httpClient.setCloseableHttpClient(HttpClients.custom().setConnectionManager(new PoolingHttpClientConnectionManager()).setProxy(proxy).setRetryHandler(myReTryHandler).setDefaultCredentialsProvider(credsProvider).setDefaultRequestConfig(config).build());
	    	
	    		}
	    		
	    		
	    	}else {
	    		Proxy rem ;
	    		if(it.hasNext()){
	    	
					rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					httpClient.setHost(temp[0]);
					httpClient.setPort(Integer.parseInt(temp[1]));
	//				ipPool.remove(rem);
				}else{
					it=ipPool.iterator();
					rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					httpClient.setHost(temp[0]);
					httpClient.setPort(Integer.parseInt(temp[1]));
				}
	    		
	    	}
		}

		public static String getCurrentProxyForTM(){
			if(null == currentStickyHost || currentStickyHost.isEmpty()){
				getLuminatiProxy(false);
			}
			return currentStickyHost+":33128";
		}
		
		public static String getLuminatiProxy(boolean isBestBroxy){
			boolean flag =false;
			String result =null;
			int i = 0;
			HttpHost proxy = null;
	    	while(i<5 && !flag && !isBestBroxy){
	    		i++;
				try {
					proxy = new HttpHost(getNetNutStickyHost(), 33128);
					result = proxy.getHostName();
					flag = true;
					
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
	    	}
	    	if(flag && !isBestBroxy){
				return result + ":33128";
	    	}else{
	    		if(it.hasNext()){
			    	
					Proxy rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					HttpClientStore.setProxyIp(temp[0]);
					HttpClientStore.setPort(Integer.parseInt(temp[1]));
					return temp[0] + ":" + Integer.parseInt(temp[1]);
				}else{
					it=ipPool.iterator();
					Proxy rem =  it.next();
					String[] temp=rem.getUrl().split(":");
					return temp[0] + ":" + Integer.parseInt(temp[1]);
				}
	    	}
		}
		
		public static void changeBestProxy(){
			if(it.hasNext()){
		    	
				Proxy rem =  it.next();
				String[] temp=rem.getUrl().split(":");
				HttpClientStore.setProxyIp(temp[0]);
				HttpClientStore.setPort(Integer.parseInt(temp[1]));
//				ipPool.remove(rem);
			}else{
				it=ipPool.iterator();
				Proxy rem =  it.next();
				String[] temp=rem.getUrl().split(":");
				HttpClientStore.setProxyIp(temp[0]);
				HttpClientStore.setPort(Integer.parseInt(temp[1]));
			}
		}
		
		public void afterPropertiesSet() throws Exception {
			Timer timer= new Timer();
			timer.scheduleAtFixedRate(new ChangeProxy(),new Date(),TIMER_INTERVAL);
		}

		public static String getNETNUT_USERNAME() {
			return NETNUT_USERNAME;
		}

		public static String getNETNUT_PASSWORD() {
			return NETNUT_PASSWORD;
		}
		
		
		public static void mainold(String[] args) {
			
			/*HttpHost proxy = new HttpHost("us-s1.netnut.io",33128);
            Credentials credentials = new UsernamePasswordCredentials("Rightthisway", "Qpox9d");
            AuthScope authScope = new AuthScope("us-s1.netnut.io", 33128);
            CredentialsProvider credsProvider = new BasicCredentialsProvider();
            credsProvider.setCredentials(authScope, credentials);
            HttpClient client = HttpClientBuilder.create().setProxy(proxy).setDefaultCredentialsProvider(credsProvider).build();
            try {
                   HttpResponse response=client.execute(new HttpGet("https://www.msn.com"));
                   HttpEntity entity = response.getEntity();
                   String responseString = EntityUtils.toString(entity, "UTF-8");
                   System.out.println(responseString);
            } catch (IOException e) {
                   // TODO Auto-generated catch block
                   e.printStackTrace();
            }*/
			
			SimpleHttpClient httpClient = new SimpleHttpClient(Site.TICKET_NETWORK_DIRECT);
			
			String hostName = "us.netnut.io";
			String userName = "Rightthisway";
			
			HttpHost proxy = new HttpHost(hostName,33128);			
			System.out.println("HOST: "+hostName);
			System.out.println("USER NAME: "+userName);
			System.out.println("PASSWORD: "+ChangeProxy.getNETNUT_PASSWORD());
			httpClient.setUserName(userName);
    		httpClient.setPassword(ChangeProxy.getNETNUT_PASSWORD());
    		
    		httpClient.setHost(proxy.getHostName());
    		httpClient.setPort(33128);
    		
            
            CredentialsProvider credsProvider = new BasicCredentialsProvider();
    		HttpRequestRetryHandler myReTryHandler = new HttpRequestRetryHandler() {
    			@Override
    			public boolean retryRequest(IOException exception, int executionCount,
    					HttpContext context) {
    				if(exception!=null){
    					logger.info("IO Exception while executing request : " + exception.fillInStackTrace());
    				}
    				return false;
    			}
    		};
    		credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(httpClient.getUserName(), httpClient.getPassword()));
            RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).build();
    		httpClient.setCloseableHttpClient(HttpClients.custom().setConnectionManager(new PoolingHttpClientConnectionManager()).setProxy(proxy).setRetryHandler(myReTryHandler).setDefaultCredentialsProvider(credsProvider).setDefaultRequestConfig(config).build());
    		
    		int k= 0;
    		for(int i=1;i<10;i++){
    			try {
    				System.out.println(i+"---------->"+new Date());
        			HttpGet httpGet = new HttpGet("http://www.ticketsnow.com/ResaleOrder/Hamilton-Tickets-at-Sarofim-Hall--Hobby-Center-in-Houston-4-24-2018/tickets/2051210");
        			httpGet.addHeader("Host", "www.ticketsnow.com");				
        			httpGet.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        			httpGet.addHeader("Accept-Encoding", "gzip,deflate");
        			httpGet.addHeader("Accept-Language", "en-US,en;q=0.9");
                    HttpResponse response=httpClient.execute(httpGet);
                    HttpEntity entity = response.getEntity();
                    String responseString = EntityUtils.toString(entity, "UTF-8");
                    System.out.println(responseString);
                    System.out.println(i+"---------->"+new Date());
                    break;
             } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    k++;
             }
    		}
    		System.out.println(k);
    		
		}
		
		private static class RemoveSoapHeadersInterceptor implements HttpRequestInterceptor {

	        public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
	            if (request instanceof HttpEntityEnclosingRequest) {
	                if (request.containsHeader(HTTP.TRANSFER_ENCODING)) {
	                    request.removeHeaders(HTTP.TRANSFER_ENCODING);
	                }
	                if (request.containsHeader(HTTP.CONTENT_LEN)) {
	                    request.removeHeaders(HTTP.CONTENT_LEN);
	                }
	            }
	        }
	    }

		
	}
