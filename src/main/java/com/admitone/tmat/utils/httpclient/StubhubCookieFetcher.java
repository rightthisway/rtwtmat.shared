package com.admitone.tmat.utils.httpclient;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.util.EntityUtils;

/**
 * This class will get latest Cookies Information from Stubhub
 * @author Ulaganathan
 *
 */
public class StubhubCookieFetcher {
	
	public static String staticEventId = "9530269";
	
	public static String getCookies(SimpleHttpClient httpClient, String eventId){
    	String cookies = "";
    	eventId = (null != eventId && !eventId.isEmpty())?eventId:staticEventId;
    	HttpGet request = new HttpGet("https://www.stubhub.com/metallica-tickets-metallica-nashville-bridgestone-arena-1-24-2019/event/103491778/?sort=quality+desc");
    	request.addHeader("Host", "www.stubhub.com");
    	request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; rv:46.0) Gecko/20100101 Firefox/46.0");
    	request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
    	request.addHeader("Accept-Encoding", "gzip, deflate, br");
    	request.addHeader("Accept-Language", "en-US,en;q=0.5");
    	request.addHeader("Connection", "keep-alive");
    	request.addHeader("Cookie", "fsr.s={\"f\":1462010947398}");
    	request.addHeader("Upgrade-Insecure-Requests", "1");
    	request.addHeader("Referer", "https://www.stubhub.com/metallica-tickets-metallica-el-paso-don-haskins-center-2-28-2019/event/103491786/");
        try{
    		CloseableHttpResponse response = null;
    		org.apache.http.Header[] responseHeaders = null;
    		try{
    			response = httpClient.execute(request);
    			responseHeaders = response.getAllHeaders();
    			EntityUtils.consumeQuietly(response.getEntity());
    		}catch(Exception e){
    			e.printStackTrace();
    		}finally{
    			//to reuse connection we need to close response stream associated with that
    			if(response != null)
    				response.close();
    		}
        	
        	int i = 0;
        	for (org.apache.http.Header header : responseHeaders) {
        		
        		if(header.getName().equals("Set-Cookie")){
        			
        			/*System.out.println(header.getName() 
        		      + " : " + header.getValue());*/
        			
        			String tempArray[] = header.getValue().split(";");
        			
        			if(null != tempArray && tempArray.length > 0){
        				
        				if(i == 0){
	        				cookies = tempArray[0];
	        			}else{
	        				cookies = cookies+";"+tempArray[0];
	        			}
        				
        				if(tempArray[0].contains("TLTHID")){
        					String tempVariable = tempArray[0];
        					tempVariable= tempVariable.replaceAll("TLTHID", "TLTSID");
        					cookies =  cookies+";"+tempVariable;
        				}
	        			i++;
        			}
        		}
        	}
        	
        	Calendar calendar = new GregorianCalendar();
        	String month = "", date = "", year = ""+calendar.get(Calendar.YEAR);
        	
        	if(calendar.get(Calendar.MONTH) <= 9){
        		month = 0+"" +calendar.get(Calendar.MONTH);
        	}else{
        		month = ""+calendar.get(Calendar.MONTH);
        	}
        	
        	if(calendar.get(Calendar.DATE) <= 9){
        		month = 0+"" +calendar.get(Calendar.DATE);
        	}else{
        		month = ""+calendar.get(Calendar.DATE);
        	}
        	
        	cookies = cookies+";"+"STUB_SESS=filler%7E%5E%7E0%7Capp_token%7E%5E%7EBImXAmYKv7MZjdJqQiBaUOcoa2HXA3Bgr5nl%2Fie9i9Y%3D%7E%5E%7E"+month+"%2F"+date+"%2F"+year+";";
        	//System.out.println("Cookie : "+cookies);
        }catch(Exception e){
        	e.printStackTrace();
        	return null;
        }
        
        return cookies;
    }
	
		public static void main(String[] args) throws Exception {
		/*Calendar calendar = new GregorianCalendar();
    	int month = calendar.get(Calendar.MONTH);
    	int date = calendar.get(Calendar.DATE);
    	int year = calendar.get(Calendar.YEAR);
    	System.out.println(month+":"+date+":"+year);*/
    	SimpleHttpClient httpClient = HttpClientStore.createHttpClient("stubhub", "", "");
    	getCookies(httpClient, null);
	}
	
	
	}
