package com.admitone.tmat.utils.httpclient;

import java.io.IOException;
import java.util.Date;

import javax.net.ssl.SSLContext;

import org.apache.commons.httpclient.HttpException;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.RedirectHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class SimpleHttpClient extends CloseableHttpClient {
	private final Logger logger = LoggerFactory.getLogger(SimpleHttpClient.class);
	CloseableHttpClient closeableHttpClient;
	public static final String DEFAULT = "default";
	public String userName ;
	public String password ;
	DefaultRedirectStrategy defaultRedirectStrategy;
	RedirectHandler redirectHandler;
	private Date created;
	private boolean used = true;
	private String host;
	private int port;
	// true if the httpClient is valid
	private boolean valid = true;
	private String siteId;
	private boolean isLuminati;
	private boolean isBestProxy;
	private String stubhubCookie;
	private String vividCookie;
	public SimpleHttpClient(String siteId) {
		this.created = new Date();
		this.siteId = siteId;
		setUsed(true);
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(200);
		cm.setDefaultMaxPerRoute(200);
		RequestConfig config = RequestConfig.custom()
		  .setConnectTimeout(6 * 1000)
		  .setConnectionRequestTimeout(6 * 1000)
		  .setSocketTimeout(6 * 1000).build();
		this.closeableHttpClient = HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(config).build();
		this.created = new Date();
		this.setValid(true);
		this.setUsed(false);
		// reset();
	}
	public void changeCloseableHttpClient(){
		try {
			this.closeableHttpClient.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.created = new Date();
		setUsed(true);
		PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
		cm.setMaxTotal(200);
		cm.setDefaultMaxPerRoute(200);
		RequestConfig config = RequestConfig.custom()
		  .setConnectTimeout(6 * 1000)
		  .setConnectionRequestTimeout(6 * 1000)
		  .setSocketTimeout(6 * 1000).build();
		this.closeableHttpClient = HttpClients.custom().setConnectionManager(cm).setDefaultRequestConfig(config).build();
		this.created = new Date();
		this.setValid(true);
		this.setUsed(false);
	}
	

	public SimpleHttpClient(String siteId, HttpHost proxy , boolean isBestProxy) throws Exception {
		this.created = new Date();
		this.siteId = siteId;
		setUsed(true);
		CredentialsProvider credsProvider = new BasicCredentialsProvider();
		this.host = proxy.getHostName();
		this.port = proxy.getPort();
		HttpRequestRetryHandler myReTryHandler = new HttpRequestRetryHandler() {
			@Override
			public boolean retryRequest(IOException exception, int executionCount,
					HttpContext context) {
				if(exception!=null){
					logger.info("IO Exception while executing request : " + exception.fillInStackTrace());
				}
				return false;
			}
		};
		
		this.userName = ChangeProxy.getNETNUT_USERNAME();
		this.password = ChangeProxy.getNETNUT_PASSWORD();
		
		logger.info("Proxy:" + host + "::" + port+" Credentials: "+userName+":-:"+password);
//		closeableHttpClient = HttpClients.custom().setConnectionManager(cm).setDefaultCredentialsProvider(credsProvider).setRoutePlanner(routePlanner).build();
		
		//System.out.println("Proxy:" + host + "::" + port+"::"+ siteId+":: isBestProxy::"+isBestProxy);
		if(!isBestProxy && siteId.equalsIgnoreCase("stubhub")) {
			SSLContext sslcontext= SSLContexts.custom().loadTrustMaterial(null,
		            new TrustSelfSignedStrategy()).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
			        new String[] { "TLSv1" , "TLSv1.1", "TLSv1.2"  }, null, null);
			
			credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(userName, password));
	        RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).setSocketTimeout(60*1000).build();
			closeableHttpClient = HttpClients.custom().setProxy(proxy).
			setRetryHandler(myReTryHandler).
			setDefaultCredentialsProvider(credsProvider).
			setDefaultRequestConfig(config).
			setSSLSocketFactory(sslsf).
			addInterceptorFirst(new RemoveSoapHeadersInterceptor()).
			build();
			
		}else if(!isBestProxy && siteId.equalsIgnoreCase("vividseat")) {
			SSLContext sslcontext= SSLContexts.custom().loadTrustMaterial(null,
		            new TrustSelfSignedStrategy()).build();
			SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext,
			        new String[] { "TLSv1" , "TLSv1.1", "TLSv1.2"  }, null, null);
			
			credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(userName, password));
	        RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).setSocketTimeout(60*1000).build();
			closeableHttpClient = HttpClients.custom().setProxy(proxy).
			setRetryHandler(myReTryHandler).
			setDefaultCredentialsProvider(credsProvider).
			setDefaultRequestConfig(config).
			setSSLSocketFactory(sslsf).
			addInterceptorFirst(new RemoveSoapHeadersInterceptor()).
			build();
			
		} else if(!isBestProxy){
			credsProvider.setCredentials(new AuthScope(proxy),new UsernamePasswordCredentials(userName, password));
	        RequestConfig config = RequestConfig.custom().setConnectTimeout(60*1000).setConnectionRequestTimeout(60*1000).setSocketTimeout(60*1000).build();
			closeableHttpClient = HttpClients.custom().setConnectionManager(new PoolingHttpClientConnectionManager()).setProxy(proxy).
			setRetryHandler(myReTryHandler).
			setDefaultCredentialsProvider(credsProvider).
			setDefaultRequestConfig(config).
			build();
			
		}else{
			//This will allow best proxy
			RequestConfig config = RequestConfig.custom()
			  .setConnectTimeout(6 * 1000)
			  .setConnectionRequestTimeout(6 * 1000)
			  .setSocketTimeout(6 * 1000).build();
			closeableHttpClient = HttpClients.custom().setConnectionManager(new PoolingHttpClientConnectionManager())
			.setProxy(proxy)
			.setRetryHandler(myReTryHandler)
			.setDefaultCredentialsProvider(credsProvider)
			.setDefaultRequestConfig(config)
			.build();
		}
		
		this.created = new Date();
		this.setUsed(false);
        
		// reset();
	}
	
	private void addExtraHttpRequestParameters(HttpRequest request) {
		request.addHeader("User-Agent", "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.5) Gecko/2008120122 Firefox/3.0.5");
		request.addHeader("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.7");	
		request.addHeader("Accept-Language", "en-us,en;q=0.5");
	}
	
	
	@Override
	public CloseableHttpResponse execute(HttpUriRequest request) throws IOException,
			ClientProtocolException {
 		return execute(request, true);
	}
	
	public CloseableHttpResponse execute(HttpPost request) throws IOException, ClientProtocolException {
		return execute(request, true);
	}

	public CloseableHttpResponse __execute(HttpUriRequest request, boolean addExtraHeaders) throws IOException, ClientProtocolException {
		try {
			if (addExtraHeaders) {
				addExtraHttpRequestParameters(request);
				//request.removeHeaders("Cookie");
			}
			
				if (this.host != null) {
					int i =0;
					CloseableHttpResponse response = null;
					while(i<2){
						response = handleResponse(request);
						if (response != null
								&& !statusCodeRequiresExitNodeSwitch(response.getStatusLine()
										.getStatusCode())) {
							this.setUsed(false);
							return response;
						}else {
							//System.out.println("CLIENTEX : "+response+" : "+request.getRequestLine());
							try {
								ChangeProxy.changeLuminatiProxy(this,this.isBestProxy());
								assignHttpClient();
							} catch (Exception e) {
								logger.info("HTTP CLIENT ERROR: while assigning client" + e.getMessage() + "for url=" + request.getRequestLine());
							}
						}
						i++;
					}
				} else {
					int i =0;
					CloseableHttpResponse response = null;
					while(i<2){
						try{
							response =  closeableHttpClient.execute(request);
							this.setUsed(false);
							return response;
						}catch (Exception e) {
							i++;
							this.changeCloseableHttpClient();
						}
					}
					
				}
			
			return null;
		} catch (IOException e) {
			valid = false;
			this.setValid(false);
			System.out.println("HTTP CLIENT ERROR: " + e.getMessage() + "for url=" + request.getRequestLine());
			throw e;
		}		
	}
	
	public CloseableHttpResponse executeWithOneHit(HttpUriRequest request, boolean addExtraHeaders) throws IOException, ClientProtocolException {
		try {
			if (addExtraHeaders) {
				addExtraHttpRequestParameters(request);
				//request.removeHeaders("Cookie");
			}
			
				if (this.host != null) {
					int i =0;
					CloseableHttpResponse response = null;
					while(i<2){
						response = handleResponse(request);
						if (response != null
								&& !statusCodeRequiresExitNodeSwitch(response.getStatusLine()
										.getStatusCode())) {
							this.setUsed(false);
							return response;
						}else {
							//System.out.println("CLIENTEX : "+response+" : "+request.getRequestLine());
							try {
								ChangeProxy.changeLuminatiProxy(this,this.isBestProxy());
								assignHttpClient();
							} catch (Exception e) {
								logger.info("HTTP CLIENT ERROR: while assigning client" + e.getMessage() + "for url=" + request.getRequestLine());
							}
						}
						i++;
					}
				} else {
					//int i =0;
					CloseableHttpResponse response = null;
					//while(i<2){
						try{
							response =  closeableHttpClient.execute(request);
							this.setUsed(false);
							return response;
						}catch (Exception e) {
							logger.info("HTTP CLIENT ERROR: " + e.getMessage() + "for url=" + request.getRequestLine());
							//i++;
							//this.changeCloseableHttpClient();
							e.printStackTrace();
						}
					//}
					
				}
			
			return null;
		} catch (IOException e) {
			valid = false;
			this.setValid(false);
			System.out.println("HTTP CLIENT ERROR: " + e.getMessage() + "for url=" + request.getRequestLine());
			throw e;
		}		
	}

	public CloseableHttpResponse handleResponse(HttpUriRequest request) throws ClientProtocolException, IOException {
		CloseableHttpResponse response = null;
		
			if(!this.isLuminati()){
				response = closeableHttpClient.execute(request);
				return response;
			}else{
				int i = 0;
				
				while(i<2){
					try{
						
						
						response = closeableHttpClient.execute(request);
						//System.out.println("REQUEST Execution:" + response.getStatusLine() + ":" + request.getURI() + " By " + this.host +":" + this.port+ " and Credentials " + this.userName+":"+this.password );
	//					response = Executor.newInstance().auth(proxy, LUMATI_USER_NAME, LUMATI_PASWORD).execute(Request.Get(request.getURI()).viaProxy(proxy)).returnResponse();
						if (response != null
								&& !statusCodeRequiresExitNodeSwitch(response.getStatusLine()
										.getStatusCode())) {
							// success or other client/website error like 404...
							break;
						}else{
							logger.info("Code:" + response.getStatusLine() + ":" + request.getURI() + " By " + this.host +":" + this.port+ " and Credentials " + this.userName+":"+this.password );
							try{
								ChangeProxy.forcefullyIncreaseStickyServerNum();
							}catch(Exception e3){
								e3.printStackTrace();
							}
							ChangeProxy.changeLuminatiProxy(this,this.isBestProxy());
							assignHttpClient();
	//						this.host = proxy.getHostName();
	//						this.port = proxy.getPort();
						}
					}catch (Exception e) {
						logger.info("Error while proecessing request "+i+" : " + request.getURI() + " By " + this.host +":" + this.port + " and Credentials " + this.userName +":"+this.password+" :: " + e.fillInStackTrace());
						//System.out.println("Error while proecessing request "+i + request.getURI() + " By " + this.host +":" + this.port + " :: " + e.fillInStackTrace());
						try {
							try{
								ChangeProxy.forcefullyIncreaseStickyServerNum();
							}catch(Exception e3){
								e3.printStackTrace();
							}
							
							ChangeProxy.changeLuminatiProxy(this,this.isBestProxy());
							assignHttpClient();
						} catch (Exception e1) {
						}
					}
					i++;
				}
			}	
		
				
		return response;
	}

	public boolean statusCodeRequiresExitNodeSwitch(int code) {
		boolean flag = (code == 403 || code == 429 || code == 502 || code == 503 || code == 405);
		return flag;
	}

	// dirty trick to override the execute method as the execute method of
	// AbstractHttpClient is final.
	// use HttpGet and HttpPost instead of HttpRequestURI
	public CloseableHttpResponse execute(HttpUriRequest request, boolean addExtraHeaders)
			throws IOException, ClientProtocolException {
		return __execute(request, addExtraHeaders);
//		handleResponse(request);
//		return response;
	}
	
	public void assignHttpClient() throws Exception{
		SimpleHttpClient httpClient= HttpClientStore.createHttpClient(siteId);
		this.setCloseableHttpClient(httpClient.getCloseableHttpClient());
	}
	
	/*
	 * public HttpResponse execute(HttpPost request, boolean addExtraHeaders)
	 * throws IOException, ClientProtocolException { return __execute(request,
	 * addExtraHeaders); }
	 */

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public boolean isUsed() {
		return used;
	}

	public void setUsed(boolean used) {
		this.used = used;
	}

	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getStubhubCookie() {
		return stubhubCookie;
	}
	public void setStubhubCookie(String stubhubCookie) {
		this.stubhubCookie = stubhubCookie;
	}
	
	public String getVividCookie() {
		return vividCookie;
	}
	public void setVividCookie(String vividCookie) {
		this.vividCookie = vividCookie;
	}
	public DefaultRedirectStrategy getDefaultRedirectStrategy() {
		return defaultRedirectStrategy;
	}

	public void setDefaultRedirectStrategy(
			DefaultRedirectStrategy defaultRedirectStrategy) {
		this.defaultRedirectStrategy = defaultRedirectStrategy;
	}

	public RedirectHandler getRedirectHandler() {
		return redirectHandler;
	}

	public void setRedirectHandler(RedirectHandler redirectHandler) {
		this.redirectHandler = redirectHandler;
	}

	public CloseableHttpClient getCloseableHttpClient() {
		return closeableHttpClient;
	}

	public void setCloseableHttpClient(CloseableHttpClient closeableHttpClient) {
		this.closeableHttpClient = closeableHttpClient;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean isLuminati() {
		return isLuminati;
	}

	public void setLuminati(boolean isLuminati) {
		this.isLuminati = isLuminati;
	}

	public boolean isBestProxy() {
		isBestProxy = null !=siteId && siteId.equals("stubhubapi")?true:false;
		return isBestProxy;
	}
	public void setBestProxy(boolean isBestProxy) {
		this.isBestProxy = isBestProxy;
	}
	
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	@Override
	public HttpParams getParams() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public ClientConnectionManager getConnectionManager() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public void close() throws IOException {
		// TODO Auto-generated method stub
		
	}
	@Override
	protected CloseableHttpResponse doExecute(HttpHost paramHttpHost, HttpRequest paramHttpRequest,
			HttpContext paramHttpContext) throws IOException, ClientProtocolException {
		// TODO Auto-generated method stub
		return null;
	}
	
	private static class RemoveSoapHeadersInterceptor implements HttpRequestInterceptor {

        public void process(HttpRequest request, HttpContext context) throws HttpException, IOException {
            if (request instanceof HttpEntityEnclosingRequest) {
                if (request.containsHeader(HTTP.TRANSFER_ENCODING)) {
                    request.removeHeaders(HTTP.TRANSFER_ENCODING);
                }
                if (request.containsHeader(HTTP.CONTENT_LEN)) {
                    request.removeHeaders(HTTP.CONTENT_LEN);
                }
            }
        }
    }

}
