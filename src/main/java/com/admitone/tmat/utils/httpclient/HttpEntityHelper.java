package com.admitone.tmat.utils.httpclient;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;

public class HttpEntityHelper {
	
	public static UncompressedHttpEntity getUncompressedEntity(HttpEntity entity) {
        Header ceheader = entity.getContentEncoding();
        UncompressedHttpEntity resultEntity = new UncompressedHttpEntity(entity);
        if (ceheader != null) {
            HeaderElement[] codecs = ceheader.getElements();
            for (int i = 0; i < codecs.length; i++) {
            	// System.out.println("CODECS=" + codecs);
                String codecName = codecs[i].getName();
                if ("gzip".equalsIgnoreCase(codecName) || "deflate".equalsIgnoreCase(codecName)) {
                	resultEntity.setCodec(codecName);
                } 
            }
        }
        return resultEntity;
	}	
}


