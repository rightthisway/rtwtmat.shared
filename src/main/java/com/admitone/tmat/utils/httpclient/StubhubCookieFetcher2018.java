package com.admitone.tmat.utils.httpclient;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

/**
 * This class will get latest Cookies Information from Stubhub
 * @author Ulaganathan
 *
 */
public class StubhubCookieFetcher2018 {
	
	public static String staticEventId = "9530269";
	
	public static String getCookies(SimpleHttpClient httpClient, String eventId){
    	String cookies = "";
    	eventId = (null != eventId && !eventId.isEmpty())?eventId:staticEventId;
    	HttpGet request = new HttpGet("https://www.stubhub.com/notifications/");
    	request.addHeader(":authority", "www.stubhub.com");
    	request.addHeader(":path", "/notifications/");
    	request.addHeader(":method", "GET");
    	request.addHeader(":scheme", "https");
    	request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
    	request.addHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
    	request.addHeader("Accept-Encoding", "gzip, deflate, br");
    	request.addHeader("Accept-Language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
    	request.addHeader("Upgrade-Insecure-Requests", "1");
    	request.addHeader("dnt", "1");
    	String content = "";
        try{
    		CloseableHttpResponse response = null;
    		org.apache.http.Header[] responseHeaders = null;
    		try{
    			response = httpClient.execute(request);
    			
    			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
    			content = EntityUtils.toString(entity);
				//System.out.println("content----->"+content);
    			
    			responseHeaders = response.getAllHeaders();
    			EntityUtils.consumeQuietly(response.getEntity());
    		}catch(Exception e){
    			e.printStackTrace();
    		}finally{
    			//to reuse connection we need to close response stream associated with that
    			if(response != null)
    				response.close();
    		}
        	
        	int i = 0;
        	for (org.apache.http.Header header : responseHeaders) {
        		
        		System.out.println(header.getName() 
          		      + " : " + header.getValue());
        		
        		if(header.getName().equals("Set-Cookie")){
        			
        			String tempArray[] = header.getValue().split(";");
        			
        			if(null != tempArray && tempArray.length > 0){
        				
        				if(i == 0){
	        				cookies = tempArray[0];
	        			}else{
	        				cookies = cookies+";"+tempArray[0];
	        			}
        				
        				if(tempArray[0].contains("TLTHID")){
        					String tempVariable = tempArray[0];
        					tempVariable= tempVariable.replaceAll("TLTHID", "TLTSID");
        					cookies =  cookies+";"+tempVariable;
        				}
	        			i++;
        			}
        		}
        	}
        	
        	System.out.println("COOKIES1: "+cookies);
        	
        	System.out.println("===========================================");
        	
        	
        	Pattern pattern = Pattern.compile("d__inj_delayed\" src=\"(.*?)\"");
			Matcher matcher = pattern.matcher(content);
			String processId = "";
			while(matcher.find()){
				processId=matcher.group(1);
			}
			System.out.println(processId);
			
			request = new HttpGet("https://www.stubhub.com"+processId);
	    	request.addHeader(":authority", "www.stubhub.com");
	    	request.addHeader(":path", processId);
	    	request.addHeader(":method", "GET");
	    	request.addHeader(":scheme", "https");
	    	request.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
	    	request.addHeader("Accept", "*/*");
	    	request.addHeader("Accept-Encoding", "gzip, deflate, br");
	    	request.addHeader("Accept-Language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
	    	request.addHeader("referer", "https://www.stubhub.com/notifications/");
	    	request.addHeader("dnt", "1");
	    	request.addHeader("cookies", cookies);
			 
	    	content = "";
	        try{
	    		response = null;
	    		responseHeaders = null;
	    		try{
	    			response = httpClient.execute(request);
	    			
	    			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
	    			content = EntityUtils.toString(entity);
					System.out.println("PID content----->"+content);
	    			
	    			responseHeaders = response.getAllHeaders();
	    			EntityUtils.consumeQuietly(response.getEntity());
	    		}catch(Exception e){
	    			e.printStackTrace();
	    		}finally{
	    			//to reuse connection we need to close response stream associated with that
	    			if(response != null)
	    				response.close();
	    		}
	        	
	    		pattern = Pattern.compile("path:\"(.*?)\"");
				matcher = pattern.matcher(content);
				String PID = "";
				while(matcher.find()){
					PID=matcher.group(1);
				}
				
				pattern = Pattern.compile("ajax_header:\"(.*?)\"");
				matcher = pattern.matcher(content);
				String ajax_header = "";
				while(matcher.find()){
					ajax_header=matcher.group(1);
				}
				i = 0;
				
				System.out.println("===========================================");
				
	        	for (org.apache.http.Header header : responseHeaders) {
	        		
	        		System.out.println(header.getName() 
	        		      + " : " + header.getValue());  
	        		
	        		if(header.getName().equals("Set-Cookie")){
	        			
	        			String tempArray[] = header.getValue().split(";");
	        			
	        			if(null != tempArray && tempArray.length > 0){
	        				
	        				cookies = cookies+";"+tempArray[0];
	        				
	        				if(tempArray[0].contains("TLTHID")){
	        					String tempVariable = tempArray[0];
	        					tempVariable= tempVariable.replaceAll("TLTHID", "TLTSID");
	        					cookies =  cookies+";"+tempVariable;
	        				}
		        			i++;
	        			}
	        		}
	        	}
	        	
	        	System.out.println("COOKIES2: "+cookies);
	        	
	        	System.out.println("+==============================================");
	        	
	        	Calendar calendar = new GregorianCalendar();
	        	String month = "", date = "", year = ""+calendar.get(Calendar.YEAR);
	        	
	        	if(calendar.get(Calendar.MONTH) <= 9){
	        		month = 0+"" +calendar.get(Calendar.MONTH);
	        	}else{
	        		month = ""+calendar.get(Calendar.MONTH);
	        	}
	        	
	        	if(calendar.get(Calendar.DATE) <= 9){
	        		month = 0+"" +calendar.get(Calendar.DATE);
	        	}else{
	        		month = ""+calendar.get(Calendar.DATE);
	        	}
	        	
	        	//cookies = cookies+";"+"STUB_SESS=filler%7E%5E%7E0%7Capp_token%7E%5E%7EBImXAmYKv7MZjdJqQiBaUOcoa2HXA3Bgr5nl%2Fie9i9Y%3D%7E%5E%7E"+month+"%2F"+date+"%2F"+year+";";
	     
	        	

				System.out.println("PROCESS ID : "+processId);
				System.out.println("PID : "+PID);
				System.out.println("ajax_header : "+ajax_header);
	        	System.out.println("cookies > "+cookies);
	        	
	        	HttpPost post = new HttpPost("https://www.stubhub.com"+PID);
	        	post.addHeader(":authority", "www.stubhub.com");
	        	post.addHeader(":path", PID);
	        	post.addHeader(":method", "POST");
	        	post.addHeader(":scheme", "https");
	        	post.addHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36");
	        	post.addHeader("Accept", "*/*");
	        	post.addHeader("Accept-Encoding", "gzip, deflate, br");
	        	post.addHeader("Accept-Language", "en-US,en;q=0.9,hi;q=0.8,ta;q=0.7");
	        	post.addHeader("cookies", cookies);
	        	post.addHeader("content-length", "6410");
	        	post.addHeader("content-type", "text/plain;charset=UTF-8");
	        	post.addHeader("origin", "https://www.stubhub.com");
	        	post.addHeader("x-distil-ajax", ajax_header);
	        	post.addHeader("referer", "https://www.stubhub.com/notifications/");
	        	post.addHeader("dnt", "1");
	        	
	        	List<NameValuePair> params = new ArrayList<NameValuePair>();
	        	PID = PID.split("PID=")[1];
	        	//params.add(new BasicNameValuePair("PID", PID));
	        	params.add(new BasicNameValuePair("p", "%7B%22proof%22%3A%2270%3A1528918392869%3AlRCyApC4gopDaw7k2s01%22%2C%22fp2%22%3A%7B%22userAgent%22%3A%22Mozilla%2F5.0(WindowsNT6.1%3BWin64%3Bx64)AppleWebKit%2F537.36(KHTML%2ClikeGecko)Chrome%2F67.0.3396.87Safari%2F537.36%22%2C%22language%22%3A%22en-US%22%2C%22screen%22%3A%7B%22width%22%3A1366%2C%22height%22%3A768%2C%22availHeight%22%3A728%2C%22availWidth%22%3A1366%2C%22innerHeight%22%3A150%2C%22innerWidth%22%3A1366%2C%22outerHeight%22%3A728%2C%22outerWidth%22%3A1366%7D%2C%22timezone%22%3A5.5%2C%22indexedDb%22%3Atrue%2C%22addBehavior%22%3Afalse%2C%22openDatabase%22%3Atrue%2C%22cpuClass%22%3A%22unknown%22%2C%22platform%22%3A%22Win32%22%2C%22doNotTrack%22%3A%221%22%2C%22plugins%22%3A%22ChromePDFPlugin%3A%3APortableDocumentFormat%3A%3Aapplication%2Fx-google-chrome-pdf~pdf%3BChromePDFViewer%3A%3A%3A%3Aapplication%2Fpdf~pdf%3BNativeClient%3A%3A%3A%3Aapplication%2Fx-nacl~%2Capplication%2Fx-pnacl~%22%2C%22canvas%22%3A%7B%22winding%22%3A%22yes%22%2C%22towebp%22%3Atrue%2C%22blending%22%3Atrue%2C%22img%22%3A%223f79b75d9af6c722b60838222ccf41927cbd2b08%22%7D%2C%22webGL%22%3A%7B%22img%22%3A%229debeeee077f7cfdc834330fe26be86e27393b6f%22%2C%22extensions%22%3A%22ANGLE_instanced_arrays%3BEXT_blend_minmax%3BEXT_color_buffer_half_float%3BEXT_texture_filter_anisotropic%3BWEBKIT_EXT_texture_filter_anisotropic%3BOES_element_index_uint%3BOES_standard_derivatives%3BOES_texture_float%3BOES_texture_float_linear%3BOES_texture_half_float%3BOES_texture_half_float_linear%3BOES_vertex_array_object%3BWEBGL_color_buffer_float%3BWEBGL_compressed_texture_etc1%3BWEBGL_compressed_texture_s3tc%3BWEBKIT_WEBGL_compressed_texture_s3tc%3BWEBGL_debug_renderer_info%3BWEBGL_debug_shaders%3BWEBGL_depth_texture%3BWEBKIT_WEBGL_depth_texture%3BWEBGL_draw_buffers%3BWEBGL_lose_context%3BWEBKIT_WEBGL_lose_context%22%2C%22aliasedlinewidthrange%22%3A%22%5B1%2C1%5D%22%2C%22aliasedpointsizerange%22%3A%22%5B0.125%2C8192%5D%22%2C%22alphabits%22%3A8%2C%22antialiasing%22%3A%22yes%22%2C%22bluebits%22%3A8%2C%22depthbits%22%3A24%2C%22greenbits%22%3A8%2C%22maxanisotropy%22%3A16%2C%22maxcombinedtextureimageunits%22%3A32%2C%22maxcubemaptexturesize%22%3A8192%2C%22maxfragmentuniformvectors%22%3A224%2C%22maxrenderbuffersize%22%3A8192%2C%22maxtextureimageunits%22%3A16%2C%22maxtexturesize%22%3A8192%2C%22maxvaryingvectors%22%3A32%2C%22maxvertexattribs%22%3A32%2C%22maxvertextextureimageunits%22%3A16%2C%22maxvertexuniformvectors%22%3A256%2C%22maxviewportdims%22%3A%22%5B8192%2C8192%5D%22%2C%22redbits%22%3A8%2C%22renderer%22%3A%22WebKitWebGL%22%2C%22shadinglanguageversion%22%3A%22WebGLGLSLES1.0(OpenGLESGLSLES1.0Chromium)%22%2C%22stencilbits%22%3A0%2C%22vendor%22%3A%22WebKit%22%2C%22version%22%3A%22WebGL1.0(OpenGLES2.0Chromium)%22%2C%22vertexshaderhighfloatprecision%22%3A23%2C%22vertexshaderhighfloatprecisionrangeMin%22%3A127%2C%22vertexshaderhighfloatprecisionrangeMax%22%3A127%2C%22vertexshadermediumfloatprecision%22%3A23%2C%22vertexshadermediumfloatprecisionrangeMin%22%3A127%2C%22vertexshadermediumfloatprecisionrangeMax%22%3A127%2C%22vertexshaderlowfloatprecision%22%3A23%2C%22vertexshaderlowfloatprecisionrangeMin%22%3A127%2C%22vertexshaderlowfloatprecisionrangeMax%22%3A127%2C%22fragmentshaderhighfloatprecision%22%3A23%2C%22fragmentshaderhighfloatprecisionrangeMin%22%3A127%2C%22fragmentshaderhighfloatprecisionrangeMax%22%3A127%2C%22fragmentshadermediumfloatprecision%22%3A23%2C%22fragmentshadermediumfloatprecisionrangeMin%22%3A127%2C%22fragmentshadermediumfloatprecisionrangeMax%22%3A127%2C%22fragmentshaderlowfloatprecision%22%3A23%2C%22fragmentshaderlowfloatprecisionrangeMin%22%3A127%2C%22fragmentshaderlowfloatprecisionrangeMax%22%3A127%2C%22vertexshaderhighintprecision%22%3A0%2C%22vertexshaderhighintprecisionrangeMin%22%3A31%2C%22vertexshaderhighintprecisionrangeMax%22%3A30%2C%22vertexshadermediumintprecision%22%3A0%2C%22vertexshadermediumintprecisionrangeMin%22%3A31%2C%22vertexshadermediumintprecisionrangeMax%22%3A30%2C%22vertexshaderlowintprecision%22%3A0%2C%22vertexshaderlowintprecisionrangeMin%22%3A31%2C%22vertexshaderlowintprecisionrangeMax%22%3A30%2C%22fragmentshaderhighintprecision%22%3A0%2C%22fragmentshaderhighintprecisionrangeMin%22%3A31%2C%22fragmentshaderhighintprecisionrangeMax%22%3A30%2C%22fragmentshadermediumintprecision%22%3A0%2C%22fragmentshadermediumintprecisionrangeMin%22%3A31%2C%22fragmentshadermediumintprecisionrangeMax%22%3A30%2C%22fragmentshaderlowintprecision%22%3A0%2C%22fragmentshaderlowintprecisionrangeMin%22%3A31%2C%22fragmentshaderlowintprecisionrangeMax%22%3A30%7D%2C%22touch%22%3A%7B%22maxTouchPoints%22%3A0%2C%22touchEvent%22%3Afalse%2C%22touchStart%22%3Afalse%7D%2C%22video%22%3A%7B%22ogg%22%3A%22probably%22%2C%22h264%22%3A%22probably%22%2C%22webm%22%3A%22probably%22%7D%2C%22audio%22%3A%7B%22ogg%22%3A%22probably%22%2C%22mp3%22%3A%22probably%22%2C%22wav%22%3A%22probably%22%2C%22m4a%22%3A%22maybe%22%7D%2C%22vendor%22%3A%22GoogleInc.%22%2C%22product%22%3A%22Gecko%22%2C%22productSub%22%3A%2220030107%22%2C%22browser%22%3A%7B%22ie%22%3Afalse%2C%22chrome%22%3Atrue%2C%22webdriver%22%3Afalse%7D%2C%22fonts%22%3A%22Batang%3BCalibri%3BCentury%3BHaettenschweiler%3BLeelawadee%3BMarlett%3BPMingLiU%3BPristina%3BSimHei%3BVrinda%22%7D%2C%22cookies%22%3A1%2C%22setTimeout%22%3A1%2C%22setInterval%22%3A1%2C%22appName%22%3A%22Netscape%22%2C%22platform%22%3A%22Win32%22%2C%22syslang%22%3A%22en-US%22%2C%22userlang%22%3A%22en-US%22%2C%22cpu%22%3A%22%22%2C%22productSub%22%3A%2220030107%22%2C%22plugins%22%3A%7B%220%22%3A%22ChromePDFPlugin%22%2C%221%22%3A%22ChromePDFViewer%22%2C%222%22%3A%22NativeClient%22%7D%2C%22mimeTypes%22%3A%7B%220%22%3A%22application%2Fpdf%22%2C%221%22%3A%22PortableDocumentFormatapplication%2Fx-google-chrome-pdf%22%2C%222%22%3A%22NativeClientExecutableapplication%2Fx-nacl%22%2C%223%22%3A%22PortableNativeClientExecutableapplication%2Fx-pnacl%22%7D%2C%22screen%22%3A%7B%22width%22%3A1366%2C%22height%22%3A768%2C%22colorDepth%22%3A24%7D%2C%22fonts%22%3A%7B%220%22%3A%22Calibri%22%2C%221%22%3A%22Cambria%22%2C%222%22%3A%22Constantia%22%2C%223%22%3A%22LucidaBright%22%2C%224%22%3A%22DejaVuSerif%22%2C%225%22%3A%22Georgia%22%2C%226%22%3A%22SegoeUI%22%2C%227%22%3A%22Candara%22%2C%228%22%3A%22DejaVuSans%22%2C%229%22%3A%22TrebuchetMS%22%2C%2210%22%3A%22Verdana%22%2C%2211%22%3A%22Consolas%22%2C%2212%22%3A%22LucidaConsole%22%2C%2213%22%3A%22LucidaSansTypewriter%22%2C%2214%22%3A%22DejaVuSansMono%22%2C%2215%22%3A%22CourierNew%22%2C%2216%22%3A%22Courier%22%7D%7D"));
	    		post.setEntity(new UrlEncodedFormEntity(params));
	        	
	    		//post.setEntity(new StringEntity("%7B%22proof%22%3A%2270%3A1528918392869%3AlRCyApC4gopDaw7k2s01%22%2C%22fp2%22%3A%7B%22userAgent%22%3A%22Mozilla%2F5.0(WindowsNT6.1%3BWin64%3Bx64)AppleWebKit%2F537.36(KHTML%2ClikeGecko)Chrome%2F67.0.3396.87Safari%2F537.36%22%2C%22language%22%3A%22en-US%22%2C%22screen%22%3A%7B%22width%22%3A1366%2C%22height%22%3A768%2C%22availHeight%22%3A728%2C%22availWidth%22%3A1366%2C%22innerHeight%22%3A150%2C%22innerWidth%22%3A1366%2C%22outerHeight%22%3A728%2C%22outerWidth%22%3A1366%7D%2C%22timezone%22%3A5.5%2C%22indexedDb%22%3Atrue%2C%22addBehavior%22%3Afalse%2C%22openDatabase%22%3Atrue%2C%22cpuClass%22%3A%22unknown%22%2C%22platform%22%3A%22Win32%22%2C%22doNotTrack%22%3A%221%22%2C%22plugins%22%3A%22ChromePDFPlugin%3A%3APortableDocumentFormat%3A%3Aapplication%2Fx-google-chrome-pdf~pdf%3BChromePDFViewer%3A%3A%3A%3Aapplication%2Fpdf~pdf%3BNativeClient%3A%3A%3A%3Aapplication%2Fx-nacl~%2Capplication%2Fx-pnacl~%22%2C%22canvas%22%3A%7B%22winding%22%3A%22yes%22%2C%22towebp%22%3Atrue%2C%22blending%22%3Atrue%2C%22img%22%3A%223f79b75d9af6c722b60838222ccf41927cbd2b08%22%7D%2C%22webGL%22%3A%7B%22img%22%3A%229debeeee077f7cfdc834330fe26be86e27393b6f%22%2C%22extensions%22%3A%22ANGLE_instanced_arrays%3BEXT_blend_minmax%3BEXT_color_buffer_half_float%3BEXT_texture_filter_anisotropic%3BWEBKIT_EXT_texture_filter_anisotropic%3BOES_element_index_uint%3BOES_standard_derivatives%3BOES_texture_float%3BOES_texture_float_linear%3BOES_texture_half_float%3BOES_texture_half_float_linear%3BOES_vertex_array_object%3BWEBGL_color_buffer_float%3BWEBGL_compressed_texture_etc1%3BWEBGL_compressed_texture_s3tc%3BWEBKIT_WEBGL_compressed_texture_s3tc%3BWEBGL_debug_renderer_info%3BWEBGL_debug_shaders%3BWEBGL_depth_texture%3BWEBKIT_WEBGL_depth_texture%3BWEBGL_draw_buffers%3BWEBGL_lose_context%3BWEBKIT_WEBGL_lose_context%22%2C%22aliasedlinewidthrange%22%3A%22%5B1%2C1%5D%22%2C%22aliasedpointsizerange%22%3A%22%5B0.125%2C8192%5D%22%2C%22alphabits%22%3A8%2C%22antialiasing%22%3A%22yes%22%2C%22bluebits%22%3A8%2C%22depthbits%22%3A24%2C%22greenbits%22%3A8%2C%22maxanisotropy%22%3A16%2C%22maxcombinedtextureimageunits%22%3A32%2C%22maxcubemaptexturesize%22%3A8192%2C%22maxfragmentuniformvectors%22%3A224%2C%22maxrenderbuffersize%22%3A8192%2C%22maxtextureimageunits%22%3A16%2C%22maxtexturesize%22%3A8192%2C%22maxvaryingvectors%22%3A32%2C%22maxvertexattribs%22%3A32%2C%22maxvertextextureimageunits%22%3A16%2C%22maxvertexuniformvectors%22%3A256%2C%22maxviewportdims%22%3A%22%5B8192%2C8192%5D%22%2C%22redbits%22%3A8%2C%22renderer%22%3A%22WebKitWebGL%22%2C%22shadinglanguageversion%22%3A%22WebGLGLSLES1.0(OpenGLESGLSLES1.0Chromium)%22%2C%22stencilbits%22%3A0%2C%22vendor%22%3A%22WebKit%22%2C%22version%22%3A%22WebGL1.0(OpenGLES2.0Chromium)%22%2C%22vertexshaderhighfloatprecision%22%3A23%2C%22vertexshaderhighfloatprecisionrangeMin%22%3A127%2C%22vertexshaderhighfloatprecisionrangeMax%22%3A127%2C%22vertexshadermediumfloatprecision%22%3A23%2C%22vertexshadermediumfloatprecisionrangeMin%22%3A127%2C%22vertexshadermediumfloatprecisionrangeMax%22%3A127%2C%22vertexshaderlowfloatprecision%22%3A23%2C%22vertexshaderlowfloatprecisionrangeMin%22%3A127%2C%22vertexshaderlowfloatprecisionrangeMax%22%3A127%2C%22fragmentshaderhighfloatprecision%22%3A23%2C%22fragmentshaderhighfloatprecisionrangeMin%22%3A127%2C%22fragmentshaderhighfloatprecisionrangeMax%22%3A127%2C%22fragmentshadermediumfloatprecision%22%3A23%2C%22fragmentshadermediumfloatprecisionrangeMin%22%3A127%2C%22fragmentshadermediumfloatprecisionrangeMax%22%3A127%2C%22fragmentshaderlowfloatprecision%22%3A23%2C%22fragmentshaderlowfloatprecisionrangeMin%22%3A127%2C%22fragmentshaderlowfloatprecisionrangeMax%22%3A127%2C%22vertexshaderhighintprecision%22%3A0%2C%22vertexshaderhighintprecisionrangeMin%22%3A31%2C%22vertexshaderhighintprecisionrangeMax%22%3A30%2C%22vertexshadermediumintprecision%22%3A0%2C%22vertexshadermediumintprecisionrangeMin%22%3A31%2C%22vertexshadermediumintprecisionrangeMax%22%3A30%2C%22vertexshaderlowintprecision%22%3A0%2C%22vertexshaderlowintprecisionrangeMin%22%3A31%2C%22vertexshaderlowintprecisionrangeMax%22%3A30%2C%22fragmentshaderhighintprecision%22%3A0%2C%22fragmentshaderhighintprecisionrangeMin%22%3A31%2C%22fragmentshaderhighintprecisionrangeMax%22%3A30%2C%22fragmentshadermediumintprecision%22%3A0%2C%22fragmentshadermediumintprecisionrangeMin%22%3A31%2C%22fragmentshadermediumintprecisionrangeMax%22%3A30%2C%22fragmentshaderlowintprecision%22%3A0%2C%22fragmentshaderlowintprecisionrangeMin%22%3A31%2C%22fragmentshaderlowintprecisionrangeMax%22%3A30%7D%2C%22touch%22%3A%7B%22maxTouchPoints%22%3A0%2C%22touchEvent%22%3Afalse%2C%22touchStart%22%3Afalse%7D%2C%22video%22%3A%7B%22ogg%22%3A%22probably%22%2C%22h264%22%3A%22probably%22%2C%22webm%22%3A%22probably%22%7D%2C%22audio%22%3A%7B%22ogg%22%3A%22probably%22%2C%22mp3%22%3A%22probably%22%2C%22wav%22%3A%22probably%22%2C%22m4a%22%3A%22maybe%22%7D%2C%22vendor%22%3A%22GoogleInc.%22%2C%22product%22%3A%22Gecko%22%2C%22productSub%22%3A%2220030107%22%2C%22browser%22%3A%7B%22ie%22%3Afalse%2C%22chrome%22%3Atrue%2C%22webdriver%22%3Afalse%7D%2C%22fonts%22%3A%22Batang%3BCalibri%3BCentury%3BHaettenschweiler%3BLeelawadee%3BMarlett%3BPMingLiU%3BPristina%3BSimHei%3BVrinda%22%7D%2C%22cookies%22%3A1%2C%22setTimeout%22%3A1%2C%22setInterval%22%3A1%2C%22appName%22%3A%22Netscape%22%2C%22platform%22%3A%22Win32%22%2C%22syslang%22%3A%22en-US%22%2C%22userlang%22%3A%22en-US%22%2C%22cpu%22%3A%22%22%2C%22productSub%22%3A%2220030107%22%2C%22plugins%22%3A%7B%220%22%3A%22ChromePDFPlugin%22%2C%221%22%3A%22ChromePDFViewer%22%2C%222%22%3A%22NativeClient%22%7D%2C%22mimeTypes%22%3A%7B%220%22%3A%22application%2Fpdf%22%2C%221%22%3A%22PortableDocumentFormatapplication%2Fx-google-chrome-pdf%22%2C%222%22%3A%22NativeClientExecutableapplication%2Fx-nacl%22%2C%223%22%3A%22PortableNativeClientExecutableapplication%2Fx-pnacl%22%7D%2C%22screen%22%3A%7B%22width%22%3A1366%2C%22height%22%3A768%2C%22colorDepth%22%3A24%7D%2C%22fonts%22%3A%7B%220%22%3A%22Calibri%22%2C%221%22%3A%22Cambria%22%2C%222%22%3A%22Constantia%22%2C%223%22%3A%22LucidaBright%22%2C%224%22%3A%22DejaVuSerif%22%2C%225%22%3A%22Georgia%22%2C%226%22%3A%22SegoeUI%22%2C%227%22%3A%22Candara%22%2C%228%22%3A%22DejaVuSans%22%2C%229%22%3A%22TrebuchetMS%22%2C%2210%22%3A%22Verdana%22%2C%2211%22%3A%22Consolas%22%2C%2212%22%3A%22LucidaConsole%22%2C%2213%22%3A%22LucidaSansTypewriter%22%2C%2214%22%3A%22DejaVuSansMono%22%2C%2215%22%3A%22CourierNew%22%2C%2216%22%3A%22Courier%22%7D%7D", "UTF8"));
	    		
	        	content = "";
		        try{
		    		response = null;
		    		responseHeaders = null;
		    		try{
		    			response = httpClient.execute(post);
		    			
		    			UncompressedHttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
		    			content = EntityUtils.toString(entity);
						System.out.println("DISTIL AJAX content----->"+content);
		    			
		    			responseHeaders = response.getAllHeaders();
		    			EntityUtils.consumeQuietly(response.getEntity());
		    		}catch(Exception e){
		    			e.printStackTrace();
		    		}finally{
		    			//to reuse connection we need to close response stream associated with that
		    			if(response != null)
		    				response.close();
		    		}
		    		
		    		
		    		/*i = 0;
		        	for (org.apache.http.Header header : responseHeaders) {
		        		
		        		System.out.println(header.getName() 
		        		      + " : " + header.getValue());  
		        		
		        		if(header.getName().equals("Set-Cookie")){
		        			
		        			String tempArray[] = header.getValue().split(";");
		        			
		        			if(null != tempArray && tempArray.length > 0){
		        				
		        				if(i == 0){
			        				cookies = tempArray[0];
			        			}else{
			        				cookies = cookies+";"+tempArray[0];
			        			}
		        				
		        				if(tempArray[0].contains("TLTHID")){
		        					String tempVariable = tempArray[0];
		        					tempVariable= tempVariable.replaceAll("TLTHID", "TLTSID");
		        					cookies =  cookies+";"+tempVariable;
		        				}
			        			i++;
		        			}
		        		}
		        	}*/
		    		
		    		
		        }catch(Exception e){
		        	e.printStackTrace();
		        	return null;
		        }	
		    	
	        	
	        	
	        }catch(Exception e){
	        	e.printStackTrace();
	        	return null;
	        }
	        
	        
	        
	        
	        
        	
        }catch(Exception e){
        	e.printStackTrace();
        	return null;
        }
        
        return cookies;
    }
	
		public static void main(String[] args) throws Exception {
		/*Calendar calendar = new GregorianCalendar();
    	int month = calendar.get(Calendar.MONTH);
    	int date = calendar.get(Calendar.DATE);
    	int year = calendar.get(Calendar.YEAR);
    	System.out.println(month+":"+date+":"+year);*/
    	SimpleHttpClient httpClient = HttpClientStore.createHttpClient("stubhub", "", "");
    	getCookies(httpClient, null);
			
			/*String content = "/zwxsutztwbeffxbyzcquv.js?PID=CDF4DD54-2CD5-3834-9754-662BA9F8A710";
			
			System.out.println(content.split("PID=")[1]);
			
			Pattern pattern = Pattern.compile("PID=(.*?)\"");
			Matcher matcher = pattern.matcher(content);
			String processId = "";
			
			while(matcher.find()){
				if(matcher.find()){
					processId=matcher.group();
				}
			}
			System.out.println("---"+processId);*/
			
	}
	
	
	}
