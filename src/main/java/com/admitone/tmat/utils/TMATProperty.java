package com.admitone.tmat.utils;

public class TMATProperty {
	private String zonesAdminURL;

	public String getZonesAdminURL() {
		return zonesAdminURL;
	}

	public void setZonesAdminURL(String zonesAdminURL) {
		this.zonesAdminURL = zonesAdminURL;
	}
}
