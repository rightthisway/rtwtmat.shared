package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ShortTransaction;
//import com.admitone.tmat.data.Tour;

public class ShortTransactionManager {
	
	private static ShortTransactionManager instance = null;
	
//	private final Logger log = LoggerFactory.getLogger(ShortTransactionManager.class);
	
	protected static final int DEFAULT_SHORT_TIMER_WAIT = 9 * 60 * 1000;
	
	private Map<Integer, Integer> eventIdByAdmitoneId = new HashMap<Integer, Integer>();

	
	/* Map<EventId, ShortTransactions> */
	private Map<Integer, List<ShortTransaction>> shortTransactionsByEventId = null;
	
	private ShortTransactionManager() {
		Timer timer = new Timer();
		//run every 15 minutes by default
		timer.scheduleAtFixedRate(
				new TimerTask() {
					@Override
					public void run() {
						try {
							//System.out.println(";;;;;;;;;;START TO LOAD SHORTTRANSACTION " + new Date());
//							instance.reinit();
							//System.out.println(";;;;;;;;;;FINISH TO LOAD SHORTTRANSACTION " + new Date());
						} catch (Exception e) {
							System.out.println(";;;;;;;;;;ERROR WHILE LOADING SHORTTRANSACTION " + new Date());
							e.printStackTrace();
						}
					}
				}
				, new Date(), DEFAULT_SHORT_TIMER_WAIT);
	}

	public static ShortTransactionManager getInstance() {
		if (instance == null) {
			instance = new ShortTransactionManager();
		}
		return instance;		
	}

	private Integer getEventIdByAdmitoneId(int admitoneId) {
		Integer eventId;
		if (!eventIdByAdmitoneId.containsKey(admitoneId)) {
			Event event = DAORegistry.getEventDAO().getEventByAdmitoneId(admitoneId);
			if (event == null) {
				eventId = null;
			} else {
				eventId = event.getId();
				eventIdByAdmitoneId.put(admitoneId, eventId);
			}
			
		} else {
			eventId = eventIdByAdmitoneId.get(admitoneId);
		}
		return eventId;
	}


	/**
	 * getShortsByEvent
	 * 
	 * @param eventId - Integer Id to get the transactions for
	 * @return ArrayList<ShortTransaction> - all shorts for this event
	 */
	public List<ShortTransaction> getShortsByEvent(Integer eventId) {
		List<ShortTransaction> shortTransactions = shortTransactionsByEventId.get(eventId);
		if (shortTransactions == null) {
			return new ArrayList<ShortTransaction>();
		}
		return shortTransactions;
	}

	/**
	 * getAllShortEventIds
	 * 
	 * @return Set<Integer> - all of the eventIds that have shorts
	 */
	public Set<Integer> getAllShortEventIds() {
		return new HashSet<Integer>(shortTransactionsByEventId.keySet());
	}
		
	/*public List<Integer> getTourShortEventIds(Tour tour) {
		Collection<Event> events = tour.getEvents();
		List<Integer> returnIds = new ArrayList<Integer>();
		for(Event event : events){
			if(shortTransactionsByEventId.get(event.getId()) != null){
				returnIds.add(event.getId());
			}
		}
		return returnIds;
	}*/

	protected void reinit() {
		// if it is null, that means that the replication is going on and during that time
		// all the short transactions records are removed from DB.
		// skip it for now and will update the short transactions in memory later.

		Collection<ShortTransaction> transactions = DAORegistry.getShortTransactionDAO().getAll();
		
		if (transactions == null || transactions.isEmpty()) {
			return;
		}
		shortTransactionsByEventId = new HashMap<Integer, List<ShortTransaction>>();
		Map<Integer, Integer> checkMap = new HashMap<Integer, Integer>();
		for(ShortTransaction transaction : transactions){
			Integer eventId = null;
			if(!checkMap.containsKey(transaction.getEventId())){
				eventId = getEventIdByAdmitoneId(transaction.getEventId());
				checkMap.put(transaction.getEventId(),eventId);
			}else{
				eventId= checkMap.get(transaction.getEventId());
			}
			if(eventId==null){
				continue;
			}
			List<ShortTransaction> shortTransactions = shortTransactionsByEventId.get(eventId);
			if(shortTransactions == null){
				shortTransactions = new ArrayList<ShortTransaction>();
				shortTransactionsByEventId.put(eventId, shortTransactions);
			} 
			shortTransactions.add(transaction);
		}
	}
}
