package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.ShortBroadcast;

public class ShortBroadcastManager {
	
	private static ShortBroadcastManager instance = null;
	
	private final Logger log = LoggerFactory.getLogger(ShortBroadcastManager.class);
	protected static final int DEFAULT_SHORT_BROADCAST_TIMER_WAIT = 10 * 60 * 1000;

	
	/* Map<EventId, ShortBroadcast> */
	private Map<Integer, Collection<ShortBroadcast>> shortBroadcastsByEventId = new HashMap<Integer, Collection<ShortBroadcast>>();

	private ShortBroadcastManager() {
		Timer timer = new Timer();
		//run every 15 minutes by default
		timer.scheduleAtFixedRate(
				new TimerTask() {
					public void run(){
						try {
							System.out.println(";;;;;;;;;;START TO LOAD ADMITONE SHORT BROADCAST " + new Date());
							instance.reinit();
							System.out.println(";;;;;;;;;;FINISH TO LOAD ADMITONE SHORT BROADCAST " + new Date());
						} catch (Exception e) {
							System.out.println(";;;;;;;;;;ERROR WHILE ADMITONE SHORT BROADCAST " + new Date());
							e.printStackTrace();
						}
					}
				}
				, new Date(), DEFAULT_SHORT_BROADCAST_TIMER_WAIT);
	}

	public static ShortBroadcastManager getInstance() {
		if (instance == null) {
			//instance = new ShortBroadcastManager();
		}
		return instance;
	}
	
	public void reinit() {
		shortBroadcastsByEventId = new HashMap<Integer, Collection<ShortBroadcast>>();
		
		Collection<ShortBroadcast> broadcasts = DAORegistry.getShortBroadcastDAO().getAllLinkedShortBroadcasts();
		// the table might be erased to enable the data to be copied from 01tn
		// in this case, don't update the data in memory now and update it at the next cycle
		if (broadcasts == null || broadcasts.size() == 0) {
			return;
		}
		
		for(ShortBroadcast broadcast : broadcasts){
			this.addBroadcast(broadcast);
		}			
	}		

	/**
	 * addBroadcast
	 * 
	 * This method will add the ShortBroadcat to the DB and Memory
	 * 
	 * @param price
	 * @param quantity
	 * @param event
	 * @param section
	 * @param row
	 */
	public void addBroadcast(String price, String quantity, Integer event, 
			String section, String row) {
		
		Double bPrice = new Double(price);
		Integer qty = new Integer(quantity);
		Integer eventId = new Integer(event);
		
		ShortBroadcast broadcast = new ShortBroadcast(bPrice,
				qty, eventId, section, row);
		DAORegistry.getShortBroadcastDAO().save(broadcast);
		
		this.addBroadcast(broadcast);
	}

	/**
	 * addBroadcast
	 * 
	 * This method will add the ShortBroadcat to Memory ONLY
	 * 
	 * @param shortBroadcast
	 */
	public void addBroadcast(ShortBroadcast shortBroadcast) {
		
//		log.info("adding newBroadcast: " + shortBroadcast.toString());

		if (shortBroadcast.getEvent() == null) {
			return;
		}
		
		Integer eventId = shortBroadcast.getEvent().getId();
		Collection<ShortBroadcast> shortBroadcasts = shortBroadcastsByEventId.get(eventId);
		if(shortBroadcasts == null){
			shortBroadcasts = new ArrayList<ShortBroadcast>(); 
			shortBroadcastsByEventId.put(eventId, shortBroadcasts);
		} 
		shortBroadcasts.add(shortBroadcast);		
	}

	/**
	 * removeBroadcasts
	 * 
	 * This method will remove all ShortBroadcasts for this event id
	 * from Memory ONLY
	 * 
	 * @param eventId
	 */
	public void removeBroadcasts(Integer eventId) {
		shortBroadcastsByEventId.put(eventId, new ArrayList<ShortBroadcast>());
		DAORegistry.getShortBroadcastDAO().deleteBroadcastsByEventId(eventId);
	}

	/**
	 * getShortBroadcastsByEvent
	 * 
	 * @param eventId
	 * @return ArrayList<ShortBroadcast> - list of broadcasts for this event id
	 */
	public Collection<ShortBroadcast> getShortBroadcastsByEvent(Integer eventId) {
		return shortBroadcastsByEventId.get(eventId);
	}

	/**
	 * getAllShortCastEventIds
	 * 
	 * @return Set<Integer> - all of the eventIds that have short broadcasts
	 */
	public Set<Integer> getAllShortCastEventIds() {
		return shortBroadcastsByEventId.keySet();
	}
}
