package com.admitone.tmat.utils;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.StatefulJob;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class Cleanup extends QuartzJobBean implements StatefulJob {
	public void init(){
//		System.out.println("Clean up started at:" + new Date());
//		Runtime.getRuntime().gc();
//		System.out.println("Clean up ended at:" + new Date());
	}

	@Override
	protected void executeInternal(JobExecutionContext context)
			throws JobExecutionException {
		init();
		
	}
}
