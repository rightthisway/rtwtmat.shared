package com.admitone.tmat.utils.ticketnetwork;

import java.util.Date;

import org.directwebremoting.annotations.DataTransferObject;
import org.directwebremoting.annotations.RemoteProperty;

//	<Event>
//		<EventName>Country Thunder</EventName>
//		<DisplayDate>04/01/2009 TBA</DisplayDate>
//		<Venue>Country Thunder USA - Arizona</Venue>
//		<City>Florence</City>
//		<State>AZ</State>
//		<ParentCategoryID>2</ParentCategoryID>
//		<Map/>
//		<EventID>973045</EventID>
//		<VenueID>478</VenueID>
//		<StateID>4</StateID>
//		<EventDate>2009-04-01T03:30:00.0000000-04:00</EventDate>
//		<VenueConfigurationID/>
//		<Clicks>0</Clicks>
//	</Event>

@DataTransferObject
public class TicketNetworkEvent {
	private String eventID;
	private String eventName;
	private String venueID;
	private String venue;
	private String city;
	private String stateID;
	private String state;
	private String parentCategoryID;
	private Date eventDate;
	private String displayDate;
	
	public TicketNetworkEvent(String eventID, String eventName, String venueID, String venue, String city,
			String stateID, String state, String parentCategoryID, Date eventDate, String displayDate) {
		this.eventID = eventID;
		this.eventName = eventName;
		this.venueID = venueID;
		this.venue = venue;
		this.city = city;
		this.stateID = stateID;
		this.state = state;
		this.parentCategoryID = parentCategoryID;
		this.eventDate = eventDate;
		this.displayDate = displayDate;
	}

	@RemoteProperty
	public String getEventID() {
		return eventID;
	}

	@RemoteProperty
	public String getEventName() {
		return eventName;
	}

	@RemoteProperty
	public String getVenueID() {
		return venueID;
	}

	@RemoteProperty
	public String getVenue() {
		return venue;
	}

	@RemoteProperty
	public String getCity() {
		return city;
	}

	@RemoteProperty
	public String getStateID() {
		return stateID;
	}

	@RemoteProperty
	public String getState() {
		return state;
	}

	@RemoteProperty
	public String getParentCategoryID() {
		return parentCategoryID;
	}

	@RemoteProperty
	public Date getEventDate() {
		return eventDate;
	}

	@RemoteProperty
	public String getDisplayDate() {
		return displayDate;
	}
	
	@Override
	public String toString() {
		return "Event:" 
			+ "eventID=" + eventID
			+ ", eventName=" + eventName
			+ ", venueID=" + venueID
			+ ", venue=" + venue
			+ ", city=" + city
			+ ", stateID=" + stateID
			+ ", state=" + state
			+ ", parentCategoryID=" + parentCategoryID
			+ ", eventDate=" + eventDate
			+ ", displayDate=" + displayDate;
	}
}
