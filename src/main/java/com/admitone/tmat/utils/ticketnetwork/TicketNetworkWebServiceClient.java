package com.admitone.tmat.utils.ticketnetwork;

import java.io.ByteArrayInputStream;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.enums.TicketDeliveryType;
import com.admitone.tmat.utils.TextUtil;
import com.admitone.tmat.utils.httpclient.HttpClientStore;
import com.admitone.tmat.utils.httpclient.HttpEntityHelper;
import com.admitone.tmat.utils.httpclient.SimpleHttpClient;

/**
 * Class that handles communication with the TicketNetworkDirect POS using the web service API.
 * for now, we are using the following URL:
 *     http://tnwebservicespublic-test.ticketsoftware.net/webservices/TNPubServicesXML.asmx
 *   http://tnwebservices.ticketnetwork.com/tnwebservice/v3.2/tnwebservicestringinputs.asmx
 *   
 *   http://tnwebservices.ticketnetwork.com/tnwebservice/v3.2/TNWebServiceLimited.asmx - for mercury tickets in POS
 *	
 */
public class TicketNetworkWebServiceClient {
	private static Logger logger = LoggerFactory.getLogger(TicketNetworkWebServiceClient.class);
	
	private static String webServiceHost = "tnwebservices.ticketnetwork.com";
	private static String webServiceBaseDir = "tnwebservice/v3.2";
	private static String soapActionRootUrl = "http://" + webServiceHost + "/" + webServiceBaseDir;
	private static String webServiceTicketsUrl = soapActionRootUrl + "/tnwebservicelimited.asmx";
	private static String webServiceUrl = soapActionRootUrl + "/tnwebservice.asmx";
	
	
	private static String protocol ="https";
	private static String mercuryWebServiceHost = "webservices.ticketnetwork.com";
	private static String mercuryWebServiceBaseDir = "MercuryWebServices";
	private static String mercurySoapActionRootUrl = protocol +  "://" + mercuryWebServiceHost + "/" + mercuryWebServiceBaseDir;
	private static String mercuryWebServiceUrl = protocol +  "://" + mercuryWebServiceHost + "/MercuryWebServices/MercuryService.svc";
	
	private static Map<Integer, String> countries = null;
	private static Map<Integer, Map<Integer, String>> countryStates = null;
	
	
	private static String websiteConfigIDIN = "3320";
	
	// category caching time
	private static long cachedCategoriesInvalidationTime = 6L * 60L * 60L * 1000L;
	
	// cached categories
	private static List<TicketNetworkCategory> cachedCategories;
	
	// time when the categories were cached
	private static Date lastCachedCategoriesUpdateDate;

	private static String getAllCategoriesSOAPMessage = 
			"<?xml version='1.0' encoding='utf-8'?>"
			+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>"
			+ "	<soap:Body>"
			+ "		<GetCategories xmlns='" + soapActionRootUrl + "'>"
			+ "			<websiteConfigID>%1$s</websiteConfigID>"
			+ "		</GetCategories>"
			+ "	</soap:Body>"
			+ "</soap:Envelope>";

	private static String getEventsSOAPMessage = 
			"<?xml version='1.0' encoding='utf-8'?>\n"
			+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\n"
			+ "	<soap:Body>\n"
			+ "		<GetEvents xmlns='" + soapActionRootUrl + "'>\n"
		    + "<websiteConfigID>%1$s</websiteConfigID>\n"
//		    + "<numberOfEvents>100</numberOfEvents>\n"
//		    + "<eventID></eventID>\n"
		    + "<eventName>%2$s</eventName>\n"
//		    + "<eventDate></eventDate>\n"
		    + "<beginDate>%3$s</beginDate>\n"
		    + "<endDate>%4$s</endDate>\n"
//		    + "<venueID></venueID>\n"
		    + "<venueName>%5$s</venueName>\n"
//		    + "<stateProvDesc></stateProvDesc>\n"
//		    + "<stateID></stateID>\n"
//		    + "<cityZip></cityZip>\n"
//		    + "<nearZip></nearZip>\n"
//		    + "<parentCategoryID>%5$s</parentCategoryID>\n"
//		    + "<childCategoryID>62</childCategoryID>\n"
//		    + "<grandchildCategoryID>25</grandchildCategoryID>\n"
//		    + "<performerID></performerID>\n"
//		    + "<performerName></performerName>\n"
//		    + "<noPerformers></noPerformers>\n"
//		    + "<lowPrice></lowPrice>\n"
//		    + "<highPrice></highPrice>\n"
//		    + "<modificationDate></modificationDate>\n"
//		    + "<onlyMine></onlyMine>\n"
//		    + "<whereClause></whereClause>\n"
//		    + "<orderByClause></orderByClause>\n"
//		    + "<countryID>71</countryID>"
			+ "		</GetEvents>\n"
			+ "	</soap:Body>\n"
			+ "</soap:Envelope>\n";

/*			
			+ "	<soap:Body>"
			+ "		<GetEvents xmlns='" + soapActionRootUrl + "'>"
			+ "			<websiteConfigID>%1$s</websiteConfigID>"
			+ "			<numberOfEvents></numberOfEvents>"
			+ "			<eventID></eventID>"
			+ "			<eventName></eventI>"
			+ "			<cityZip></cityZip>"
			+ "			<eventName>%2$s</eventName>"
			+ "			<eventDate></eventDate>"
			+ "			<beginDate>%3$s</beginDate>"
			+ "			<endDate>%4$s</endDate>"
			+ "			<lowPrice></lowPrice>"
			+ "			<highPrice></highPrice>"
			+ "			<venueID></venueID>"
			+ "			<stateProvDesc></stateProvDesc>"
			+ "			<stateProvID></stateProvID>"			
			+ "			<parentCatID>%5$s</parentCatID>"
			+ "			<childCatID>%6$s</childCatID>"
			+ "			<grandChildCatID>%7$s</grandChildCatID>"
			+ "			<performerID></performerID>"
			+ "			<sortColumn></sortColumn>"
			+ "			<sortDescending></sortDescending>"			
			+ "		</GetEvents>"
			+ "	</soap:Body>"
			+ "</soap:Envelope>";
*/
	private static String getTicketsSOAPMessage =
			"<?xml version='1.0' encoding='utf-8'?>"
			+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>"
			+ "	<soap:Body>"
			+ "		<GetTicketsLimited xmlns='" + soapActionRootUrl + "'>"
			+ "			<websiteConfigID>%1$s</websiteConfigID>"
//			+ "			<numberOfRecords></numberOfRecords>"
			+ "			<eventID>%2$s</eventID>"
//			+ "			<lowPrice>10</lowPrice>"
//			+ "			<highPrice>10000</highPrice>"
//			+ "			<ticketGroupID></ticketGroupID>"
//		    + " <requestedTixSplit></requestedTixSplit>"
//		    + "  <whereClause></whereClause>"
//		    + "  <orderByClause></orderByClause>"
			+ "		</GetTicketsLimited>"
			+ "	</soap:Body>"
			+ "</soap:Envelope>";
	
	private static String ticketsSOAPMessage =
		"<?xml version='1.0' encoding='utf-8'?>"
		+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>"
		+ "	<soap:Body>"
		+ "		<GetEventTickets2 xmlns='" + soapActionRootUrl + "'>"
		+ "			<websiteConfigId>%1$s</websiteConfigId>"
//		+ "			<numberOfRecords></numberOfRecords>"
		+ "			<eventId>%2$s</eventId>"
//		+ "			<lowPrice>10</lowPrice>"
//		+ "			<highPrice>10000</highPrice>"
//		+ "			<ticketGroupID></ticketGroupID>"
//	    + " <requestedTixSplit></requestedTixSplit>"
//	    + "  <whereClause></whereClause>"
//	    + "  <orderByClause></orderByClause>"
		+ "		</GetEventTickets2>"
		+ "	</soap:Body>"
		+ "</soap:Envelope>";
	
	public static List<TicketNetworkCategory> getAllCategories(SimpleHttpClient httpClient) throws Exception {
		if (lastCachedCategoriesUpdateDate != null
				&& new Date().getTime() - lastCachedCategoriesUpdateDate.getTime() < cachedCategoriesInvalidationTime) {
			return cachedCategories;
		}
		
		// POST /webservices/TNPubServicesXML.asmx HTTP/1.1
		// Host: tnwebservicespublic-test.ticketsoftware.net
		// Content-Type: text/xml; charset=utf-8
		// Content-Length: length
		// SOAPAction: "http://www.ticketnetworkdirect.com/webservices/GetAllCategories"

		String msg = String.format(getAllCategoriesSOAPMessage, websiteConfigIDIN);

		HttpPost httpPost = new HttpPost(webServiceUrl);
		httpPost.addHeader("Host", webServiceHost);		
		httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
		httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetCategories" + "\"");
		
		httpPost.setEntity(new StringEntity(msg));
		
		CloseableHttpResponse response = null;
		Document xmlDocument = null;
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				xmlDocument = documentBuilder.parse(entity.getContent());
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}

		httpPost.abort();

		List<TicketNetworkCategory> ticketNetworkCategories = new ArrayList<TicketNetworkCategory>();
		// soap:Envelope > soap:Body > soap:GetAllCategoriesResponse 
		NodeList categoryNodeList = xmlDocument.getElementsByTagName("Category");
		//		<Category>
		//			<ParentCategoryID>3</ParentCategoryID>
		//			<ParentCategoryDesc>THEATRE</ParentCategoryDesc>
		//			<ChildCategoryID>32</ChildCategoryID>
		//			<ChildCategoryDesc>OFF-BROADWAY</ChildCategoryDesc>
		//			<GrandChildCategoryID>25</GrandChildCategoryID>
		//			<GrandChildCategoryDesc>-</GrandChildCategoryDesc>
		//		</Category>		
		for (int i = 0 ; i < categoryNodeList.getLength() ; i++) {
			Node categoryNode = categoryNodeList.item(i);
//			System.out.println("CAT NODE=" + categoryNode.getNodeName());
			
			String parentCategoryID = null;
			String parentCategoryDesc = null;
			String childCategoryID = null;
			String childCategoryDesc = null;
			String grandChildCategoryID = null;
			String grandChildCategoryDesc = null;
			
			for (int j = 0 ; j < categoryNode.getChildNodes().getLength() ; j++) {
				Node node = categoryNode.getChildNodes().item(j);
				
				String nodeName = node.getNodeName();
				if (nodeName.equals("ParentCategoryID")) {
					parentCategoryID = node.getTextContent();
				} else if (nodeName.equals("ParentCategoryDescription")) {
					parentCategoryDesc = node.getTextContent();
				} else if (nodeName.equals("ChildCategoryID")) {
					childCategoryID = node.getTextContent();
				} else if (nodeName.equals("ChildCategoryDescription")) {
					childCategoryDesc = node.getTextContent();
				} else if (nodeName.equals("GrandchildCategoryID")) {
					grandChildCategoryID = node.getTextContent();
				} else if (nodeName.equals("GrandchildCategoryDescription")) {
					grandChildCategoryDesc = node.getTextContent();
				}
			}
			ticketNetworkCategories.add(
					new TicketNetworkCategory(parentCategoryID, parentCategoryDesc, childCategoryID, childCategoryDesc, grandChildCategoryID, grandChildCategoryDesc)
			);
		}		
		
		cachedCategories = ticketNetworkCategories;
		return ticketNetworkCategories;
	}

	public static List<TicketNetworkEvent> getEvents(SimpleHttpClient httpClient, String keyword, Date startDate, Date endDate, String parentCatID, String childCatID, String grandChildCatID,String location,List<Event> eventList,boolean isVenue) throws Exception {
		// POST /webservices/TNPubServicesXML.asmx HTTP/1.1
		// Host: tnwebservicespublic-test.ticketsoftware.net
		// Content-Type: text/xml; charset=utf-8
		// Content-Length: length
		// SOAPAction: "http://www.ticketnetworkdirect.com/webservices/GetEvents"

		DateFormat displayDateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat displayTimeFormat = new SimpleDateFormat("hh:mmaa");
		DateFormat displayTimeFormatNew = new SimpleDateFormat("HH:mm:ss.SSS");
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
//		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Map<String,Boolean> dateMap = new HashMap<String, Boolean>();
		Map<String,Boolean> locationMap = new HashMap<String, Boolean>();
		for(Event event:eventList){
			if(event.getDate()==null){
				dateMap.put("TBD", true);
				locationMap.put(event.getVenue().getBuilding().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getCity().replaceAll("\\s+", " ") + ":-:" + event.getVenue().getState().replaceAll("\\s+", " ") , true);
				continue;
			}
			dateMap.put(dateFormat.format(event.getDate()), true);
		}
		
		String startDateString = "";
		if (startDate != null) {
			startDateString = dateFormat.format(startDate) + "T00:00:00";
		}

		String endDateString = "";
		if (endDate != null) {
			endDateString = dateFormat.format(endDate) + "T23:59:59";
		}
		
//		System.out.println(startDateString + "->" + endDateString);
		String eventName="";
		String venueName="";
		if(isVenue){
			venueName=keyword;
		}else{
			eventName=keyword;
		}
		String msg = String.format(getEventsSOAPMessage, 
				websiteConfigIDIN, StringEscapeUtils.escapeXml(eventName), startDateString, endDateString,StringEscapeUtils.escapeXml(venueName), 
				(parentCatID == null)?"":parentCatID,
				(childCatID == null)?"":childCatID,
				(grandChildCatID == null)?"":grandChildCatID);
		
		HttpPost httpPost = new HttpPost(webServiceUrl);
		httpPost.addHeader("Host", webServiceHost);		
		httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
		httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetEvents" + "\"");
		
		httpPost.setEntity(new StringEntity(msg));
				
		CloseableHttpResponse response = null;
		Document xmlDocument = null;
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				xmlDocument = documentBuilder.parse(entity.getContent());
				EntityUtils.consumeQuietly(entity);
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
		}
		
		List<TicketNetworkEvent> ticketNetworkEvents = new ArrayList<TicketNetworkEvent>();
		// soap:Envelope > soap:Body > GetEventsResponse > GetEventsResult
		
		NodeList eventNodeList = xmlDocument.getElementsByTagName("Event");
		
		//		  <Event>
		//		    	<EventName>string</EventName>
		//		    	<DisplayDate>string</DisplayDate>
		//		   	 	<Venue>string</Venue>
		//		    	<City>string</City>
		//		    	<State>string</State>
		//		    	<ParentCategoryID>string</ParentCategoryID>
		//		    	<Map>string</Map>
		//		    	<EventID>string</EventID>
		//		    	<VenueID>string</VenueID>
		//		    	<StateID>string</StateID>
		//		    	<EventDate>dateTime</EventDate>
		//		    	<VenueConfigurationID>string</VenueConfigurationID>
		//		    	<Clicks>int</Clicks>
		//		  </Event>

		String resultEventName = null;
		String displayDate = null;
		String venue = null;
		String city = null;
		String state = null;
		String parentCategoryID = null;
		String map = null;
		String eventID = null;
		String venueID = null;
		String stateID = null;
		String eventDateString = null;
		String country=null;
		Integer countryID=null;
//		System.out.println("EVENT NODE LIST=" + eventNodeList.getLength());
		
		
	    SimpleDateFormat eventDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for (int i = 0 ; i < eventNodeList.getLength() ; i++) {
			Node eventNode = eventNodeList.item(i);

			for (int j = 0 ; j < eventNode.getChildNodes().getLength() ; j++) {
				Node node = eventNode.getChildNodes().item(j);

				String nodeName = node.getNodeName();
				if (nodeName.equals("Name")) {
					resultEventName = node.getTextContent();
				} else if (nodeName.equals("DisplayDate")) {
					displayDate = node.getTextContent();
				} else if (nodeName.equals("Venue")) {
					venue = node.getTextContent();
				} else if (nodeName.equals("City")) {
					city = node.getTextContent();
				} else if (nodeName.equals("StateProvince")) {
					state = node.getTextContent();
				} else if (nodeName.equals("ParentCategoryID")) {
					parentCategoryID = node.getTextContent();
				} else if (nodeName.equals("MapURL")) {
					map = node.getTextContent();
				} else if (nodeName.equals("ID")) {
					eventID = node.getTextContent();
				} else if (nodeName.equals("VenueID")) {
					venueID = node.getTextContent();
				} else if (nodeName.equals("StateProvinceID")) {
					stateID = node.getTextContent();
				} else if (nodeName.equals("Date")) {
					eventDateString = node.getTextContent();
				} else if (nodeName.equals("CountryID")) {
					countryID=Integer.parseInt(node.getTextContent());
					country = countries.get(Integer.parseInt(node.getTextContent()));
				}
			}
//			Disabled by Chirag on 09.25.2014. Due to below code states are getting null value.
			/*if(null != countryID){
				
				if(null != countryStates.get(countryID) && !countryStates.get(countryID).isEmpty()){
					state = countryStates.get(countryID).get(stateID);
				}
			}*/
			
			
			String venueLocation = venue + "," + city + "," + state;
			if(location!=null && !location.isEmpty() && !venueLocation.toLowerCase().contains(location.toLowerCase())){
				continue;
			}
			Date eventDate = null;
			if (eventDateString != null) {
				String[] dateTokens = displayDate.split(" ");
				if (dateTokens.length != 2) {
					eventDate = null;
				} else {
					Date date = displayDateFormat.parse(dateTokens[0]);
					
					Time origTime = new Time(0, 0, 0);  
					Time time = origTime;  
					if (!dateTokens[1].equals("TBA")) {
						try{
							time = new Time(displayTimeFormat.parse(dateTokens[1]).getTime());
						}catch(ParseException e){
							try{
								time = new Time(displayTimeFormatNew.parse(dateTokens[1]).getTime());
							}catch(ParseException px){
								e.printStackTrace();
							}
						}
					}
					
					eventDate = new Date(date.getTime() + time.getTime() - origTime.getTime());
				}
			}
			boolean flag=false;
			if(!dateMap.containsKey(dateFormat.format(eventDate))){
				for(String key:locationMap.keySet()){
					String tokens[] = key.split(":-:");
					String building = tokens[0].trim();
					String cityToken = tokens[1].trim();
					String stateToken = tokens[2].trim();
					if(cityToken.equalsIgnoreCase(city.trim())&& stateToken.equalsIgnoreCase(state.trim()) && (TextUtil.isSimilarVenue(building, venue, 1))){
						flag=true;
						break;
					}
				}
			}else{
				flag=true;
			}
//			if(dateMap.containsKey(dateFormat.format(eventDate))){
			if(flag){
				ticketNetworkEvents.add(new TicketNetworkEvent(eventID, resultEventName, venueID, venue, city, stateID, state,
						parentCategoryID, eventDate, displayDate));	
			}
			/*if(eventDate.after(startDate) && eventDate.before(endDate)){
				ticketNetworkEvents.add(new TicketNetworkEvent(eventID, resultEventName, venueID, venue, city, stateID, state,
					parentCategoryID, eventDate, displayDate));			
			}*/
		}
		return ticketNetworkEvents;
	}

	public static List<TicketNetworkTicketGroup> getTicketNetworkTicketGroups(SimpleHttpClient httpClient, String eventId) throws Exception {
		// POST /webservices/TNPubServicesXML.asmx HTTP/1.1
		// Host: tnwebservicespublic-test.ticketsoftware.net
		// Content-Type: text/xml; charset=utf-8
		// Content-Length: length
		// SOAPAction: "http://www.ticketnetworkdirect.com/webservices/GetTicketsLimited"

		try {
			Map<Integer, TicketDeliveryType> map = new HashMap<Integer, TicketDeliveryType>();
			SimpleHttpClient httpClientMercury = null;
			try {
				//httpClientMercury = HttpClientStore.createHttpClient(Site.TICKET_NETWORK_DIRECT);
				//map = getTicketNetworkMercuryTicketsMap(httpClientMercury, eventId);
			} catch(Exception e) {
				e.printStackTrace();
				System.out.println(" TND Mercury Error Error : "+eventId+".."+new Date());
				logger.error(" TND Mercury Error Error : "+eventId+".."+new Date());
				map = new HashMap<Integer, TicketDeliveryType>();
			} finally {
				if (httpClientMercury != null) {
					HttpClientStore.releaseHttpClient(httpClientMercury);
				}
			}
		String msg = String.format(ticketsSOAPMessage, websiteConfigIDIN, eventId);

		HttpPost httpPost = new HttpPost(webServiceUrl);
//		HttpPost httpPost = new HttpPost(webServiceTicketsUrl);
		httpPost.addHeader("Host", webServiceHost);		
		httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
		httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetEventTickets2" + "\"");
		
		//System.out.println("TN REQUEST SOAP: "+msg);
		
		httpPost.setEntity(new StringEntity(msg));
		
		CloseableHttpResponse response = null;
		String result = null;
		try{
			response = httpClient.execute(httpPost);
			HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//System.out.println("TN RESPONSE: "+response);
			//ensure it is fully consumed
			if(entity != null){
				result = IOUtils.toString(entity.getContent(), "UTF-8");
				EntityUtils.consumeQuietly(entity);
			}
			//System.out.println("TN RESPONSE SOAP: "+result);
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != httpPost){
				httpPost.releaseConnection();
			}
		}

//		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
//		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
//		Document xmlDocument = documentBuilder.parse(entity.getContent());
//		httpPost.abort();

		DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
		Document xmlDocument = null;
		try{
			xmlDocument = documentBuilder.parse(new ByteArrayInputStream(IOUtils.toByteArray(result)));
		}catch(SAXParseException parseException){
			xmlDocument = documentBuilder.parse(new ByteArrayInputStream(IOUtils.toByteArray(result.replaceAll("&#x0;", "null"))));
		}
		
// soap:Envelope > soap:Body > ArrayOfTicketGroupLimited > TicketGroup
		
//		<LimitedTicketGroup>
//		<EventID>1186408</EventID>
//		<FacePrice>45.00</FacePrice>
//		<HighSeat>2</HighSeat>
//		<ID>676218989</ID>
//		<IsMine>false</IsMine>
//		<LowSeat>1</LowSeat>
//		<Marked>false</Marked>
//		<Notes>MAIN LEVEL - Left Field Seats. </Notes>
//		<ParentCategoryID>0</ParentCategoryID>
//		<Rating>4</Rating>
//		<RatingDescription>Good Fulfillment</RatingDescription>
//		<RetailPrice>34.00</RetailPrice>
//		<Row>6</Row>
//		<TicketQuantity>2</TicketQuantity>
//		<ValidSplits>
//		<int>2</int>
//		</ValidSplits>
//		<TicketGroupType>Event Ticket</TicketGroupType>
//		<currencyTypeAbbr>USD</currencyTypeAbbr>
//		<convertedActualPrice>41.00</convertedActualPrice>
//		<isMercury>true</isMercury>
//		<ActualPrice>41</ActualPrice>
//		<Section>234</Section>
//		<WholesalePrice>34.00</WholesalePrice>
//		<IsVisibleInPOS>true</IsVisibleInPOS>
//		</LimitedTicketGroup>
		
		List<TicketNetworkTicketGroup> ticketGroups = new ArrayList<TicketNetworkTicketGroup>();
		
		NodeList ticketGroupNodeList = xmlDocument.getFirstChild().getFirstChild().getFirstChild().getFirstChild().getFirstChild().getChildNodes();
		for (int i = 0 ; i < ticketGroupNodeList.getLength() ; i++) {
			Node ticketGroupNode = ticketGroupNodeList.item(i);
			
			String id = null;
			int ticketQuantity = -1;
			String section = null;
			String row = null;
			String deliveryOption = null;
			Integer loSeat = null;
			Integer hiSeat = null;
			
			double wholesalePrice = 0;
			double retailPrice = 0;
			double actualPrice = 0;
			boolean isMercury = false;
			boolean isMine = false; 
			boolean isVisibleInPOS = false;
			boolean inHand = false;
			String notes = null;
			List<Integer> validSplits = new ArrayList<Integer>();

			for (int j = 0 ; j < ticketGroupNode.getChildNodes().getLength() ; j++) {
				Node node = ticketGroupNode.getChildNodes().item(j);
				
				String nodeName = node.getNodeName();
				if (nodeName.equals("ID")) {
					id = node.getTextContent();
				} else if (nodeName.equals("TicketQuantity")) {
					ticketQuantity = Integer.parseInt(node.getTextContent());
				} else if (nodeName.equals("Section")) {
					section = node.getTextContent();
				} else if (nodeName.equals("Row")) {
					row = node.getTextContent();
				} else if (nodeName.equals("WholesalePrice")) {
					wholesalePrice = Double.parseDouble(node.getTextContent());
				} else if (nodeName.equals("RetailPrice")) {
					retailPrice = Double.parseDouble(node.getTextContent());
				} else if (nodeName.equals("ActualPrice")) {
					actualPrice = Double.parseDouble(node.getTextContent());
				} else if (nodeName.equals("Notes")) {
					notes = node.getTextContent();
				} else if (nodeName.equals("isMercury")){
					isMercury = Boolean.parseBoolean(node.getTextContent());
				} else if (nodeName.equals("IsMine")){
					isMine = Boolean.parseBoolean(node.getTextContent());
				} else if (nodeName.equals("IsVisibleInPOS")){
					isVisibleInPOS = Boolean.parseBoolean(node.getTextContent());
				}else if (nodeName.equals("deliveryOptions")){
					deliveryOption = node.getTextContent();
					if(deliveryOption!=null && !deliveryOption.isEmpty() && deliveryOption.contains("ID")){
						inHand = true;
					}
				}
				else if (nodeName.equals("ValidSplits")) {
					for (int k = 0 ; k < node.getChildNodes().getLength() ; k++) {
						Node intNode = node.getChildNodes().item(k);
						if (intNode.getNodeName().equals("int")) {
							validSplits.add(Integer.parseInt(intNode.getTextContent()));
						}
					}
				} else if (nodeName.equals("LowSeat")) {
					try {
						if (node.getTextContent().equals("*")){
							loSeat=null;
						}else{
						loSeat = Integer.parseInt(node.getTextContent());
						}
					} catch (Exception e) {}
				} else if (nodeName.equals("HighSeat")) {
					try {
						if (node.getTextContent().equals("*")){
							hiSeat = null;
						}else{
						hiSeat = Integer.parseInt(node.getTextContent());
						}
					} catch (Exception e) {}
				}
			}
			
			if (ticketQuantity == -1) {
//				System.out.println("TND FAILED FOR EVENT ID=" + eventId);
//				System.out.println("CONTENT=" + content);
//				System.out.println("TICKET GROUP LENGTH=" + ticketGroupNodeList.getLength());
								
				logger.info("TND FAILED FOR EVENT ID=" + eventId);
//				logger.info("CONTENT=" + content);
				
				throw new Exception("Invalid quantity");
			}
			Integer[] validSplitsArray = new Integer[validSplits.size()];
			validSplits.toArray(validSplitsArray);
			TicketDeliveryType deliveryType = map.get(Integer.parseInt(id));
			ticketGroups.add(new TicketNetworkTicketGroup(id, ticketQuantity, section, row, wholesalePrice, retailPrice, actualPrice, isMercury, isMine, isVisibleInPOS, notes, validSplitsArray, loSeat, hiSeat,inHand,deliveryType));
		}
		return ticketGroups;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(" TND Error Error : "+eventId+".."+new Date());
			logger.error(" TND Error Error : "+eventId+".."+new Date());
			return null;
		}
		
		
	}
	
	
	public static Map<Integer,TicketDeliveryType> getTicketNetworkMercuryTicketsMap(SimpleHttpClient httpClient, String eventId) throws Exception {

		try {
//		String url ="https://webservices.ticketnetwork.com/IMercuryService/GetTickets2";
//		String ticketMethod ="GetMercuryTempLock";
//		String ticketMethod ="GetTickets2";
//		String service= "https://webservices.ticketnetwork.com/MercuryWebServices/MercuryService.svc";
		String mercuryTicketsSOAPMessage =
			 "<s:Envelope xmlns:s='http://www.w3.org/2003/05/soap-envelope' " +
				" xmlns:a='http://www.w3.org/2005/08/addressing'> " +
				" <s:Header>" +
				" <a:Action s:mustUnderstand='1'>http://webservices.ticketnetwork.com/IMercuryService/GetTickets2</a:Action>" +
				" <a:MessageID>urn:uuid:5e3ba224-dbe3-498a-9a05-ecb716c6d31d</a:MessageID>" +
				" <a:ReplyTo><a:Address>http://www.w3.org/2005/08/addressing/anonymous</a:Address></a:ReplyTo>" +
				" <a:To s:mustUnderstand='1'>https://webservices.ticketnetwork.com/MercuryWebServices/MercuryService.svc</a:To>" +
				" </s:Header>" 
			+ "<s:Body>"
			+ "		<GetTickets2 xmlns='http://webservices.ticketnetwork.com'>"
			+ "			<websiteConfigID>%1$s</websiteConfigID>"
			+ "			<eventID>%2$s</eventID>"
			+ "		</GetTickets2>"
			+ "	</s:Body>"
			+ "</s:Envelope>";
		
		Map<Integer, TicketDeliveryType> map = new HashMap<Integer, TicketDeliveryType>();
		String msg = String.format(mercuryTicketsSOAPMessage,websiteConfigIDIN, eventId);
		HttpPost httpPost = new HttpPost(mercuryWebServiceUrl);
		httpPost.addHeader("Host", mercuryWebServiceHost);		
		httpPost.addHeader("Content-Type", "application/soap+xml; charset=utf-8");		
		httpPost.addHeader("SOAPAction", "\"" + mercurySoapActionRootUrl + "/GetTickets2" + "\"");
		
		System.out.println("TN REQUEST SOAP(MERCURY): "+msg);
		httpPost.setEntity(new StringEntity(msg));
		
		if(httpClient == null) {
			System.out.println("httpClient null : "+httpClient);
			httpClient = new SimpleHttpClient(Site.TICKET_NETWORK_DIRECT);
		}
		
		HttpEntity entity = null;
		CloseableHttpResponse response = null;
		Document xmlDocument = null;
		try{
			response = httpClient.execute(httpPost);
			System.out.println("msg : "+msg);
			if(response == null) {
				System.out.println("xmlResponse : "+ response);
				return map;
			}
			System.out.println("status : "+response.getStatusLine());
			entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
			//ensure it is fully consumed
			if(entity != null){
				DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
				xmlDocument = documentBuilder.parse(entity.getContent());
				EntityUtils.consumeQuietly(entity);
			}else{
				return map;
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			//to reuse connection we need to close response stream associated with that
			if(response != null)
				response.close();
			
			if(null != httpPost){
				httpPost.releaseConnection();
			}
		}		
		
//		List<TicketNetworkTicketGroup> ticketGroups = new ArrayList<TicketNetworkTicketGroup>();
		
		NodeList ticketGroupNodeList = xmlDocument.getFirstChild().getFirstChild().getNextSibling().getFirstChild().getChildNodes();
		for (int i = 0 ; i < ticketGroupNodeList.getLength() ; i++) {
			Node ticketGroupNode = ticketGroupNodeList.item(i);
			
			String id = null;
			boolean isInstantDownLoad =false;
			boolean isETickets =false;
			for (int j = 0 ; j < ticketGroupNode.getChildNodes().getLength() ; j++) {
				Node node = ticketGroupNode.getChildNodes().item(j);
				for (int ii = 0 ; ii < node.getChildNodes().getLength() ; ii++) {
					Node childNode = node.getChildNodes().item(ii);
					String nodeName = childNode.getNodeName();
					if (nodeName.equals("b:ID")) {
						id = childNode.getTextContent();
						if(isETickets || isInstantDownLoad){
							break;
						}
					}
					if (nodeName.equals("b:ShippingMethodsAvailable")) {
						for(int cnt=0;cnt<childNode.getChildNodes().getLength();cnt++){
							Node shipping = childNode.getChildNodes().item(cnt);
							if("Email".equals(shipping.getTextContent())){
								isETickets = true;
							}else if("InstantDownload".equals(shipping.getTextContent())){
								isInstantDownLoad = true;
							}
						}
						if(id!=null){
							break;
						}
					}
				}
				if(isInstantDownLoad){
					map.put(Integer.parseInt(id), TicketDeliveryType.INSTANT);
				}else if(isETickets){
					map.put(Integer.parseInt(id), TicketDeliveryType.ETICKETS);
				}
			}
			
		}
		System.out.println("Mercury MAP Size :  "+map.size());
		return map;
		} catch(Exception e) {
			e.printStackTrace();
			throw e;
		}
	}
	
	public static Map<Integer, String> getTicketNetworkCountries() {
		
		if(countries == null || countries.isEmpty()){
			String webServiceHost = "tnwebservices.ticketnetwork.com";
			String webServiceBaseDir = "tnwebservice/v3.2";
			String soapActionRootUrl = "http://" + webServiceHost + "/" + webServiceBaseDir;
			String webServiceUrl = soapActionRootUrl + "/tnwebservice.asmx";
			String websiteConfigIDIN ="3320";
			
			//to get full venue location
			String getVenueSOAPMessage = 
				"<?xml version='1.0' encoding='utf-8'?>\n"
				+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\n"
				+ "<soap:Body>\n"
				+ 	"<GetCountries xmlns='" +soapActionRootUrl+ "'>\n"
			    + 		"<websiteConfigID>%1$s</websiteConfigID>\n"
			    + 	"</GetCountries>\n"
			    + "</soap:Body>\n"
			    + "</soap:Envelope>\n";
			
			String countryMsg = String.format(getVenueSOAPMessage, 
					websiteConfigIDIN);
			
			try{
				HttpPost httpPost = new HttpPost(webServiceUrl);
				httpPost.addHeader("Host", webServiceHost);		
				httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
				httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetCountries" + "\"");
				
				httpPost.setEntity(new StringEntity(countryMsg));
				SimpleHttpClient httpClient =  HttpClientStore.createHttpClient(Site.TICKET_NETWORK_DIRECT);	
				
				CloseableHttpResponse response = null;
				Document xmlDocument = null;
				try{
					response = httpClient.execute(httpPost);
					HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
					//ensure it is fully consumed
					if(entity != null){
						DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
						xmlDocument = documentBuilder.parse(entity.getContent());
						EntityUtils.consumeQuietly(entity);
					}
				}catch(Exception e){
					e.printStackTrace();
				}finally{
					//to reuse connection we need to close response stream associated with that
					if(response != null)
						response.close();
					
					if(null != httpPost){
						httpPost.releaseConnection();
					}
				}
				
				NodeList countryNodeList = xmlDocument.getElementsByTagName("GetCountriesResult");
				
				
				countries = new HashMap<Integer, String>();
				
				Node countriesNode = countryNodeList.item(0);
				for (int i = 0 ; i < countriesNode.getChildNodes().getLength() ; i++) {
					Node countryNode = countriesNode.getChildNodes().item(i);
					boolean isCountryDone=false;
					boolean isCountryIDDone=false;
					String country=null;
					String id=null;
					for (int j = 0 ; j < countryNode.getChildNodes().getLength() ; j++) {
						Node node = countryNode.getChildNodes().item(j);
						String nodeName = node.getNodeName();
						if (nodeName.equals("Abbreviation")) {
							country = node.getTextContent();
							isCountryDone=true;;
						}
						if (nodeName.equals("ID")) {
							id = node.getTextContent();
							isCountryIDDone=true;;
						}
						if(isCountryDone && isCountryIDDone){
							countries.put(Integer.parseInt(id), country);
							break;
						}
					}
				}
				return countries;
				
			}catch(Exception e){
				e.printStackTrace();
				return countries;
			}
		}
		return countries;
		
	}
	
 public static Map<Integer, Map<Integer, String>> getTicketNetworkStates() throws Exception{
		
		if(countryStates == null || countryStates.isEmpty()){
			String webServiceHost = "tnwebservices.ticketnetwork.com";
			String webServiceBaseDir = "tnwebservice/v3.2";
			String soapActionRootUrl = "http://" + webServiceHost + "/" + webServiceBaseDir;
			String webServiceUrl = soapActionRootUrl + "/tnwebservice.asmx";
			String websiteConfigIDIN ="3320";
			
			if(countries == null || countries.isEmpty()){
				getTicketNetworkCountries();
			}
			
			countryStates = new HashMap<Integer, Map<Integer, String>>();
			
			for (Integer countryID : countries.keySet()) {
				
				Map<Integer, String> statesMap = new HashMap<Integer, String>();
				
				//to get full venue location
				String getVenueSOAPMessage = 
					"<?xml version='1.0' encoding='utf-8'?>\n"
					+ "<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>\n"
					+ "<soap:Body>\n"
					+ 	"<GetStates xmlns='" +soapActionRootUrl+ "'>\n"
				    + 		"<websiteConfigID>%1$s</websiteConfigID>\n"
				    +		"<countryID>%2$s</countryID>\n"
				    + 	"</GetStates>\n"
				    + "</soap:Body>\n"
				    + "</soap:Envelope>\n";
				
				String countryMsg = String.format(getVenueSOAPMessage, 
						websiteConfigIDIN,countryID);
				
				try{
					HttpPost httpPost = new HttpPost(webServiceUrl);
					httpPost.addHeader("Host", webServiceHost);		
					httpPost.addHeader("Content-Type", "text/xml; charset=utf-8");		
					httpPost.addHeader("SOAPAction", "\"" + soapActionRootUrl + "/GetStates" + "\"");
					
					httpPost.setEntity(new StringEntity(countryMsg));
					SimpleHttpClient httpClient =  HttpClientStore.createHttpClient(Site.TICKET_NETWORK_DIRECT);	
					
					CloseableHttpResponse response = null;
					Document xmlDocument = null;
					try{
						response = httpClient.execute(httpPost);
						HttpEntity entity = HttpEntityHelper.getUncompressedEntity(response.getEntity());
						//ensure it is fully consumed
						if(entity != null){
							DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
							DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
							xmlDocument = documentBuilder.parse(entity.getContent());
							EntityUtils.consumeQuietly(entity);
						}
					}catch(Exception e){
						e.printStackTrace();
					}finally{
						//to reuse connection we need to close response stream associated with that
						if(response != null)
							response.close();
						
						if(null != httpPost){
							httpPost.releaseConnection();
						}
					}
					
					NodeList stateNodeList = xmlDocument.getElementsByTagName("GetStatesResult");
					
					Node statesNode = stateNodeList.item(0);
					for (int i = 0 ; i < statesNode.getChildNodes().getLength() ; i++) {
						Node stateNode = statesNode.getChildNodes().item(i);
						boolean isStateShortDescDone=false;
						boolean isStateIDDone=false;
						//boolean isStateLongDescDone=false;
						String stateShortDesc=null; //stateLongDesc=null;
						String id=null;
						for (int j = 0 ; j < stateNode.getChildNodes().getLength() ; j++) {
							Node node = stateNode.getChildNodes().item(j);
							String nodeName = node.getNodeName();
							/*if (nodeName.equals("StateProvinceLongDesc")) {
								stateLongDesc = node.getTextContent();
								isStateLongDescDone=true;;
							}*/
							
							if (nodeName.equals("StateProvinceShortDesc")) {
								stateShortDesc = node.getTextContent();
								isStateShortDescDone=true;;
							}
							if (nodeName.equals("StateProvinceID")) {
								id = node.getTextContent();
								isStateIDDone=true;;
							}
							if(isStateIDDone && isStateShortDescDone){
								statesMap.put(Integer.parseInt(id), stateShortDesc);
							}
						}
					}
				}catch(Exception e){
					e.printStackTrace();
				}
				countryStates.put(countryID, statesMap);
				
			}
			return countryStates;
			
		}
		return countryStates;
	}
	
	public static void main(String[] args) throws Exception {
		SimpleHttpClient httpClient = HttpClientStore.createHttpClient(Site.TICKET_NETWORK_DIRECT);//new SimpleHttpClient(Site.TICKET_NETWORK_DIRECT);
		try {
			getTicketNetworkTicketGroups(httpClient, "3515748");
			//getTicketNetworkMercuryTicketsMap(httpClient,"2827601");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
