package com.admitone.tmat.utils.ticketnetwork;

import com.admitone.tmat.enums.TicketDeliveryType;

//<LimitedTicketGroup>
//<EventID>1186408</EventID>
//<FacePrice>45.00</FacePrice>
//<HighSeat>2</HighSeat>
//<ID>676218989</ID>
//<IsMine>false</IsMine>
//<LowSeat>1</LowSeat>
//<Marked>false</Marked>
//<Notes>MAIN LEVEL - Left Field Seats. </Notes>
//<ParentCategoryID>0</ParentCategoryID>
//<Rating>4</Rating>
//<RatingDescription>Good Fulfillment</RatingDescription>
//<RetailPrice>34.00</RetailPrice>
//<Row>6</Row>
//<TicketQuantity>2</TicketQuantity>
//<ValidSplits>
//<int>2</int>
//</ValidSplits>
//<TicketGroupType>Event Ticket</TicketGroupType>
//<currencyTypeAbbr>USD</currencyTypeAbbr>
//<convertedActualPrice>41.00</convertedActualPrice>
//<isMercury>true</isMercury>
//<ActualPrice>41</ActualPrice>
//<Section>234</Section>
//<WholesalePrice>34.00</WholesalePrice>
//<IsVisibleInPOS>true</IsVisibleInPOS>
//</LimitedTicketGroup>

public class TicketNetworkTicketGroup {
	private String id;
	private int ticketQuantity;
	private String section;
	private String row;
	private double wholesalePrice;
	private double retailPrice;
	private double actualPrice;
	private String note;
	private Integer[] validSplits;
	private Integer loSeat;
	private Integer hiSeat;
	private boolean isMercury;
	private boolean isMine; 
	private boolean isVisibleInPOS;
	private boolean inHand;
	private TicketDeliveryType ticketDeliveryType;
	public TicketNetworkTicketGroup(String id, int ticketQuantity, String section, String row, double wholesalePrice, double retailPrice, double actualPrice, boolean isMercury, boolean isMine, boolean isVisibleInPOS, String note, Integer[] validSplits, Integer loSeat, Integer hiSeat,boolean inHand,TicketDeliveryType ticketDeliveryType) {
		this.id = id;
		this.ticketQuantity = ticketQuantity;
		this.section = section;
		this.row = row;
		this.wholesalePrice = wholesalePrice;
		this.retailPrice = retailPrice;
		this.actualPrice = actualPrice;
		this.isMercury = isMercury;
		this.isMine = isMine;
		this.isVisibleInPOS = isVisibleInPOS;
		this.note = note;
		this.validSplits = validSplits;
		this.loSeat = loSeat;
		this.hiSeat = hiSeat;
		this.inHand = inHand;
		this.ticketDeliveryType = ticketDeliveryType;
	}

	public String getId() {
		return id;
	}

	public int getTicketQuantity() {
		return ticketQuantity;
	}

	public String getSection() {
		return section;
	}

	public String getRow() {
		return row;
	}

	public double getWholesalePrice() {
		return wholesalePrice;
	}

	public double getRetailPrice() {
		return retailPrice;
	}

	public double getActualPrice() {
		return actualPrice;
	}

	public String getNote() {
		return note;
	}
	
	public boolean getIsMercury() {
		return isMercury;
	}

	public void setIsMercury(boolean isMercury) {
		this.isMercury = isMercury;
	}
	
	public boolean getIsMine() {
		return isMine;
	}

	public void setIsMine(boolean isMine) {
		this.isMine = isMine;
	}
	
	public boolean getIsVisibleInPOS(){
		return isVisibleInPOS;
	}
	
	public void setIsVisibleInPOS(boolean isVisibleInPOS){
		this.isVisibleInPOS = isVisibleInPOS;
	}
	
	public Integer[] getValidSplits() {
		return validSplits;
	}
	
	public Integer getLoSeat() {
		return loSeat;
	}

	public void setLoSeat(Integer loSeat) {
		this.loSeat = loSeat;
	}

	public Integer getHiSeat() {
		return hiSeat;
	}

	public void setHiSeat(Integer hiSeat) {
		this.hiSeat = hiSeat;
	}

	@Override
	public String toString() {
		String validSplitsString = "[" + validSplits[0];
		for (int i = 1 ; i < validSplits.length ; i++) {
			validSplitsString += "," + validSplits[i];
		}
		validSplitsString += "]";
		
		return "TicketGroup:" 
			+ "id=" + id
			+ ", ticketQuantity=" + ticketQuantity
			+ ", section=" + section
			+ ", row=" + row
			+ ", wholesalePrice=" + wholesalePrice
			+ ", retailPrice=" + retailPrice
			+ ", actualPrice=" + actualPrice
			+ ", isMercury=" + isMercury
			+ ", isMine=" + isMine
			+ ", isVisibleInPOS=" + isVisibleInPOS
			+ ", note=" + note
			+ ", validSplits=" + validSplitsString
			+ ", loSeat=" + loSeat
			+ ", hiSeat=" + hiSeat;
	}

	public boolean isInHand() {
		return inHand;
	}

	public void setInHand(boolean inHand) {
		this.inHand = inHand;
	}

	public TicketDeliveryType getTicketDeliveryType() {
		return ticketDeliveryType;
	}

	public void setTicketDeliveryType(TicketDeliveryType ticketDeliveryType) {
		this.ticketDeliveryType = ticketDeliveryType;
	}

}
