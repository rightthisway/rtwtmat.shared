package com.admitone.tmat.utils.ticketnetwork;

import org.directwebremoting.annotations.DataTransferObject;
import org.directwebremoting.annotations.RemoteProperty;

//	<Category>
//		<ParentCategoryID>3</ParentCategoryID>
//		<ParentCategoryDesc>THEATRE</ParentCategoryDesc>
//		<ChildCategoryID>32</ChildCategoryID>
//		<ChildCategoryDesc>OFF-BROADWAY</ChildCategoryDesc>
//		<GrandChildCategoryID>25</GrandChildCategoryID>
//		<GrandChildCategoryDesc>-</GrandChildCategoryDesc>
//	</Category>

@DataTransferObject
public class TicketNetworkCategory {
	private String parentCategoryID;
	private String parentCategoryDesc;
	private String childCategoryID;
	private String childCategoryDesc;
	private String grandChildCategoryID;
	private String grandChildCategoryDesc;
	
	public TicketNetworkCategory() {
	}

	public TicketNetworkCategory(String parentCategoryID, String parentCategoryDesc, String childCategoryID,
			String childCategoryDesc, String grandChildCategoryID, String grandChildCategoryDesc) {
		this.parentCategoryID = parentCategoryID;
		this.parentCategoryDesc = parentCategoryDesc;
		this.childCategoryID = childCategoryID;
		this.childCategoryDesc = childCategoryDesc;
		this.grandChildCategoryID = grandChildCategoryID;
		this.grandChildCategoryDesc = grandChildCategoryDesc;
	}

	@RemoteProperty
	public String getParentCategoryID() {
		return parentCategoryID;
	}

	@RemoteProperty
	public String getParentCategoryDesc() {
		return parentCategoryDesc;
	}

	@RemoteProperty
	public String getChildCategoryID() {
		return childCategoryID;
	}

	@RemoteProperty
	public String getChildCategoryDesc() {
		return childCategoryDesc;
	}

	@RemoteProperty
	public String getGrandChildCategoryDesc() {
		return grandChildCategoryDesc;
	}

	@RemoteProperty
	public String getGrandChildCategoryID() {
		return grandChildCategoryID;
	}		
	
	@Override
	public String toString() {
		return "Category: " 
			+  "parentCategoryID=" + parentCategoryID
			+ ", parentCategoryDesc=" + parentCategoryDesc
			+ ", childCategoryID=" + childCategoryID
			+ ", childCategoryDesc=" + childCategoryDesc
			+ ", grandChildCategoryID=" + grandChildCategoryID
			+ ", grandChildCategoryDesc=" + grandChildCategoryDesc;
	}
}