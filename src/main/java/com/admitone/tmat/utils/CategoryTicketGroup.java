package com.admitone.tmat.utils;

import java.util.Date;

import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.Ticket;

public class CategoryTicketGroup implements CategoryTicket{

	
	private Integer id;
	private String section;
	private String lastRow;
	private Integer quantity;
	private Double actualPrice;
	private Double tnPrice;
	private Double vividPrice;
	private Double tickPickPrice;
	private Double scorebigPrice;
	private String itemId; 
	private Date expectedArrivalDate;
	private Integer shippingMethodId;
	private Integer nearTermDisplayOptionId;
	private Integer eventId;
	private Integer tnExchangeEventId;
//	private Integer tnCategoryTicketGroupId;
	private Integer categoryId;
	private Ticket ticket;
	private String status;
	private Date lastUpdated;
	private Date createdDate;
// 	private Double posPurchaseOrderId;
	private Double purPrice;
	private Integer baseTicket1;
	private Integer baseTicket2;
	private Integer baseTicket3;
	private String priceHistory;
	private String ticketIdHistory;
	private String BaseTicketOneHistory;
	private String BaseTicketTwoHistory;
	private String BaseTicketThreeHistory;
	private String popPrice;
	private String popDate;
	
	
	
	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getTicketIdHistory() {
		return ticketIdHistory;
	}
	public void setTicketIdHistory(String ticketIdHistory) {
		this.ticketIdHistory = ticketIdHistory;
	}
	
	public String getBaseTicketOneHistory() {
		return BaseTicketOneHistory;
	}
	public void setBaseTicketOneHistory(String baseTicketOneHistory) {
		BaseTicketOneHistory = baseTicketOneHistory;
	}
	
	public String getBaseTicketTwoHistory() {
		return BaseTicketTwoHistory;
	}
	public void setBaseTicketTwoHistory(String baseTicketTwoHistory) {
		BaseTicketTwoHistory = baseTicketTwoHistory;
	}
	
	public String getBaseTicketThreeHistory() {
		return BaseTicketThreeHistory;
	}
	public void setBaseTicketThreeHistory(String baseTicketThreeHistory) {
		BaseTicketThreeHistory = baseTicketThreeHistory;
	}

	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	
	
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	public Date getExpectedArrivalDate() {
		return expectedArrivalDate;
	}
	public void setExpectedArrivalDate(Date expectedArrivalDate) {
		this.expectedArrivalDate = expectedArrivalDate;
	}
	
	public Integer getShippingMethodId() {
		return shippingMethodId;
	}
	public void setShippingMethodId(Integer shippingMethodId) {
		this.shippingMethodId = shippingMethodId;
	}
	
	public String getItemId() {
		return itemId;
	}
	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Integer getNearTermDisplayOptionId() {
		return nearTermDisplayOptionId;
	}
	public void setNearTermDisplayOptionId(Integer nearTermDisplayOptionId) {
		this.nearTermDisplayOptionId = nearTermDisplayOptionId;
	}
	
	public Integer getEventId() {
		return eventId;
	}
	public void setEventId(Integer eventId) {
		this.eventId = eventId;
	}
	
	public Integer getTnExchangeEventId() {
		return tnExchangeEventId;
	}
	public void setTnExchangeEventId(Integer tnExchangeEventId) {
		this.tnExchangeEventId = tnExchangeEventId;
	}
	
	/*public Integer getTnCategoryTicketGroupId() {
		return tnCategoryTicketGroupId;
	}
	public void setTnCategoryTicketGroupId(Integer tnCategoryTicketGroupId) {
		this.tnCategoryTicketGroupId = tnCategoryTicketGroupId;
	}*/
	
	
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public Ticket getTicket() {
		return ticket;
	}
	public void setTicket(Ticket ticket) {
		this.ticket = ticket;
	}
	

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	
	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getBaseTicket2() {
		return baseTicket2;
	}
	public void setBaseTicket2(Integer baseTicket2) {
		this.baseTicket2 = baseTicket2;
	}
	
	public Integer getBaseTicket3() {
		return baseTicket3;
	}
	public void setBaseTicket3(Integer baseTicket3) {
		this.baseTicket3 = baseTicket3;
	}
	
	public String getPriceHistory() {
		return priceHistory;
	}
	public void setPriceHistory(String priceHistory) {
		this.priceHistory = priceHistory;
	}
	
	/*public Double getPosPurchaseOrderId() {
		return posPurchaseOrderId;
	}
	public void setPosPurchaseOrderId(Double posPurchaseOrderId) {
		this.posPurchaseOrderId = posPurchaseOrderId;
	}*/
	
	public Double getTnPrice() {
		return tnPrice;
	}
	public void setTnPrice(Double tnPrice) {
		this.tnPrice = tnPrice;
	}
	
	public Double getVividPrice() {
		return vividPrice;
	}
	public void setVividPrice(Double vividPrice) {
		this.vividPrice = vividPrice;
	}
	
	public Double getTickPickPrice() {
		return tickPickPrice;
	}
	public void setTickPickPrice(Double tickPickPrice) {
		this.tickPickPrice = tickPickPrice;
	}
	
	public Double getScorebigPrice() {
		return scorebigPrice;
	}
	public void setScorebigPrice(Double scorebigPrice) {
		this.scorebigPrice = scorebigPrice;
	}
	
	public Integer getBaseTicket1() {
		return baseTicket1;
	}
	public void setBaseTicket1(Integer baseTicket1) {
		this.baseTicket1 = baseTicket1;
	}
	/*@Transient
	public String getPopPrice() {
		return popPrice;
	}
	public void setPopPrice(String popPrice) {
		this.popPrice = popPrice;
	}
	@Transient
	public String getPopDate() {
		return popDate;
	}
	public void setPopDate(String popDate) {
		this.popDate = popDate;
	}*/
	
	
	public String getLastRow() {
		return lastRow;
	}
	public void setLastRow(String lastRow) {
		this.lastRow = lastRow;
	}
	
	public Double getActualPrice() {
		return actualPrice;
	}
	public void setActualPrice(Double actualprice) {
		this.actualPrice = actualprice;
	}

	@Override
	public Category getCategory() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setCategory(Category category) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer getBaseTicketOne() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBaseTicketOne(Integer baseTicketOne) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer getBaseTicketTwo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBaseTicketTwo(Integer baseTicketTwo) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Double getScoreBigPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setScoreBigPrice(Double scoreBigPrice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getTickpickPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTickpickPrice(Double tickpickPrice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getPurPrice() {
		return purPrice;
	}

	@Override
	public void setPurPrice(Double purPrice) {
		this.purPrice = purPrice;
	}

	@Override
	public Boolean getIsEdited() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setIsEdited(Boolean isEdited) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Integer getBaseTicketThree() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBaseTicketThree(Integer baseTicketThree) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getBaseTicketOnePurPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBaseTicketOnePurPrice(Double baseTicketOnePurPrice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getBaseTicketTwoPurPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBaseTicketTwoPurPrice(Double baseTicketTwoPurPrice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getBaseTicketThreePurPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setBaseTicketThreePurPrice(Double baseTicketThreePurPrice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getRowRange() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setRowRange(String rowRange) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void createCategoryTicket(Ticket ticket) {
		// TODO Auto-generated method stub
		this.ticket = ticket;
//		this.section = ticket.getCategoryMapping().getLarrySection().toUpperCase();
		this.eventId = ticket.getEventId();
		this.categoryId = ticket.getCategoryMapping().getCategoryId();
		this.itemId = ticket.getItemId();
		this.lastRow = ticket.getCategoryMapping().getLastRow().toUpperCase();
		this.purPrice = ticket.getPurchasePrice();	
		this.status = "ACTIVE";		
	}

	@Override
	public Double getZoneTicketPrice() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setZoneTicketPrice(Double zoneTicketPrice) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getSectionRange() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setSectionRange(String sectionRange) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer getPriority() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setPriority(Integer priority) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Double getFloorCap() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setFloorCap(Double floorCap) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer getVenueConfigurationZoneId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setVenueConfigurationZoneId(Integer venueConfigurationZoneId) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Integer getTnBrokerId() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setTnBrokerId(Integer tnBrokerId) {
		// TODO Auto-generated method stub
		
	}
	
}
