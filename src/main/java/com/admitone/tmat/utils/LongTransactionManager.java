package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.LongTransaction;
import com.admitone.tmat.data.ShortTransaction;
//import com.admitone.tmat.data.Tour;

public class LongTransactionManager {
	
	private static LongTransactionManager instance = null;
	
	private final Logger log = LoggerFactory.getLogger(LongTransactionManager.class);
	
	protected static final int DEFAULT_SHORT_TIMER_WAIT = 9 * 60 * 1000;
	
	private Map<Integer, Integer> eventIdByAdmitoneId = new HashMap<Integer, Integer>();

	
	/* Map<EventId, LongTransactions> */
	private Map<Integer, List<LongTransaction>> longTransactionsByEventId = new HashMap<Integer, List<LongTransaction>>();
	
	private LongTransactionManager() {
		Timer timer = new Timer();
		//run every 15 minutes by default
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				try {
					//System.out.println(";;;;;;;;;;START TO LOAD LONG TRANSACTION " + new Date());
//					instance.reinit();
					//System.out.println(";;;;;;;;;;FINISH TO LOAD LONG TRANSACTION " + new Date());
				} catch (Exception e) {
					System.out.println(";;;;;;;;;;ERROR WHILE LOADING LONG TRANSACTION " + new Date());
					e.printStackTrace();
				}
			}
		},
		new Date(), DEFAULT_SHORT_TIMER_WAIT);
	}

	public static LongTransactionManager getInstance() {
		if (instance == null) {
			instance = new LongTransactionManager();
		}
		return instance;		
	}

	private Integer getEventIdByAdmitoneId(int admitoneId) {
		Integer eventId;
		if (!eventIdByAdmitoneId.containsKey(admitoneId)) {
			Event event = DAORegistry.getEventDAO().getEventByAdmitoneId(admitoneId);
			if (event == null) {
				eventId = null;
			} else {
				eventId = event.getId();
			}
			eventIdByAdmitoneId.put(admitoneId, eventId);
		} else {
			eventId = eventIdByAdmitoneId.get(admitoneId);
		}
		return eventId;
	}

	/**
	 * getLongTransactionsByEvent
	 * 
	 * @param eventId - Integer Id to get the transactions for
	 * @return ArrayList<ShortTransaction> - all shorts for this event
	 */
	public List<LongTransaction> getLongTransactionsByEvent(Integer eventId) {
		List<LongTransaction> longTransactions = longTransactionsByEventId.get(eventId);
		if (longTransactions == null) {
			return new ArrayList<LongTransaction>();
		}
		return longTransactions;
	}

	/**
	 * getAllShortEventIds
	 * 
	 * @return Set<Integer> - all of the eventIds that have shorts
	 */
	public Set<Integer> getAllLongTransactionEventIds() {
		return new HashSet<Integer>(longTransactionsByEventId.keySet());
	}
		
	/*public List<Integer> getTourLongTransactionEventIds(Tour tour) {
		Collection<Event> events = tour.getEvents();
		ArrayList<Integer> returnIds = new ArrayList<Integer>();
		for(Event event : events){
			if(longTransactionsByEventId.get(event.getId()) != null){
				returnIds.add(event.getId());
			}
		}
		return returnIds;
	}*/
	

	public Collection<ShortTransaction> getShortsByEvent(Integer eventId) {
		Collection<ShortTransaction> shortTransactions = new ArrayList<ShortTransaction>();
		
		Collection<LongTransaction> longTransactions = getLongTransactionsByEvent(eventId);
		System.out.println(">>>>>>>>> INVENTORY=" + longTransactions);
		if(longTransactions == null || longTransactions.isEmpty()){
			longTransactions = getLongTransactionsByEvent(eventId);
		} 
		if(longTransactions != null){				
			for(LongTransaction longTransaction:longTransactions){
				if(longTransaction.getCost() != null && longTransaction.getCost() > 0.00) {						
					shortTransactions.add(new ShortTransaction(longTransaction));
				}
			}
		}
		return shortTransactions;
	}

	protected void reinit() {
		// if it is null, that means that the replication is going on and during that time
		// all the short transactions records are removed from DB.
		// skip it for now and will update the short transactions in memory later.

		Collection<LongTransaction> transactions = DAORegistry.getLongTransactionDAO().getAll();
		
		if (transactions == null || transactions.isEmpty()) {
			return;
		}
		longTransactionsByEventId = new HashMap<Integer, List<LongTransaction>>();

		for(LongTransaction transaction:transactions){
			Integer eventId = getEventIdByAdmitoneId(transaction.getAdmitoneId());

			if(eventId == null) {
				continue;
			}
			
			List<LongTransaction> longTransactions = longTransactionsByEventId.get(eventId);
			if(longTransactions == null){
				longTransactions = new ArrayList<LongTransaction>();
				longTransactionsByEventId.put(eventId, longTransactions);
			} 
			longTransactions.add(transaction);
		}
	}


	
}
