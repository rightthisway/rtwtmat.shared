package com.admitone.tmat.utils;

/**
 * To copy the EI instant tickets file periodically every 15 minutes to the specified file location in dbo.property
 * 
 */
	
public class EITicketsManager {
	
		/*private static final int EI_TIX_TIMER_INTERVAL = 15 * 60 * 1000;
		
		public EITicketsManager()
		{
			Calendar calendar = new GregorianCalendar();
			int minute = calendar.get(Calendar.MINUTE);
			
			minute = (int)(Math.ceil((double)minute / 15D)) * 15;
			
			calendar.set(Calendar.MINUTE, minute);
			calendar.set(Calendar.SECOND, 0);

			Timer timer = new Timer();
			timer.scheduleAtFixedRate(
					new TimerTask(){
					public void run(){
						try{
						String fileLoc ="";
						Collection<Ticket> eiTickets= EITicketsUtil.computeEITickets();
						Collection<Ticket> eiTNDTickets = EITicketsUtil.computeCustomizedCatEITickets();
						try{
							
							Property eiTNDProperty = DAORegistry.getPropertyDAO().get("ei.tnd.tickets.fileLoc");
							fileLoc = eiTNDProperty.getValue();
							OutputStream eiTNDoutput = new FileOutputStream(fileLoc, false);
//							Collection<Ticket> eiTickets = EITicketsUtil.computeEITickets();
							EITicketsUtil.downloadEITNDTicketCsvFile(eiTickets,eiTNDoutput);
							eiTNDoutput.close();
						}catch(Exception ex){
							ex.printStackTrace();
						}
						try{
							Property property = DAORegistry.getPropertyDAO().get("eiTickets.fileLoc");
							fileLoc = property.getValue();
							OutputStream output = new FileOutputStream(fileLoc, false);
//							Collection<Ticket> eiTickets = EITicketsUtil.computeEITickets();
							EITicketsUtil.downloadEITicketCsvFile(eiTickets,output);
							output.close();
						}catch(Exception ex){
							ex.printStackTrace();
						}
						try{
							Property custProperty = DAORegistry.getPropertyDAO().get("eiCustomizedTickets.fileLoc");
							fileLoc = custProperty.getValue();
							OutputStream custOutput = new FileOutputStream(fileLoc, false);
							
							EITicketsUtil.downloadCustomizedEITicketCsvFile(eiTNDTickets, custOutput);
							custOutput.close(); 
						}catch(Exception ex){
							ex.printStackTrace();
						}
						try{
							Property custEITNDProperty = DAORegistry.getPropertyDAO().get("ei.tnd.CustomizedTickets.fileLoc");
							fileLoc = custEITNDProperty.getValue();
							OutputStream custEITNDOutput = new FileOutputStream(fileLoc, false);
							EITicketsUtil.downloadCustomizedEITNDTicketCsvFile(eiTNDTickets,custEITNDOutput);
							custEITNDOutput.close();
						}
						catch(Exception e){
							e.printStackTrace();
						}
						}catch(Exception ex){
							ex.printStackTrace();
						}
					}
					}
					, calendar.getTime(), EI_TIX_TIMER_INTERVAL);	
		}
		
		*/
}
