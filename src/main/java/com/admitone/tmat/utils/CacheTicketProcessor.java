package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.crawler.com.coherence.TicketCacheKey;
import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.pojo.TicketData;
import com.tangosol.net.CacheFactory;
import com.tangosol.net.NamedCache;

public class CacheTicketProcessor extends Thread{
	
	private NamedCache ticketCache;
	private Integer threadNumber;
	public final int MAX_RETRY = 3;
	public final int RETRY_WAIT = 2000;
	
	public CacheTicketProcessor(Integer threadNumber){
		System.out.println("In CacheTicketProcessor constructor, initializing thread " + threadNumber);
		ticketCache = CacheFactory.getCache("ticketCache");
		this.threadNumber = threadNumber;
	}
	
	public void run() {
		while(true){
			try{
				List<TicketCacheKey> keys = new ArrayList<TicketCacheKey>(ticketCache.keySet());
				if(keys != null && keys.size() > 0){
					System.out.println("The current cache size is: " + keys.size());
					
					//Collections.sort(keys); JDK1.8 Changes
					
					try {					
						keys.sort(new Comparator<TicketCacheKey>() {
					    @Override
					    public int compare(final TicketCacheKey model1, final TicketCacheKey model2) {						    	
					    	int result;
					    	if(model1.getTimeStamp() == null ) return -1;
					    	if(model2.getTimeStamp() == null ) return  1;
					    	result = model1.getTimeStamp().compareTo(model2.getTimeStamp());
							if(result == 0) {
								if(model1.getUuid() == null) return -1;
								if(model2.getUuid() == null) return  1;
								result = model1.getUuid().compareTo(model2.getUuid());
							}
						    return result;
						}
					});
					}catch(Exception ex) {
						System.out.println("Error Sorting in CacheTicketProcessor Line 41 " + ex);
					}
					
					
					for(TicketCacheKey key : keys){
						System.out.println("Thread: " + threadNumber + "; key is: " + key.getTimeStamp());
						if(ticketCache.lock(key)){
							TicketData ticketData = (TicketData)ticketCache.remove(key);
							ticketCache.unlock(key);
							if(ticketData != null){
								List<Ticket> newTickets = ticketData.getNewTickets();
								List<Ticket> updateTickets = ticketData.getUpdateTickets();
								if(newTickets != null && !newTickets.isEmpty() && updateTickets != null && !updateTickets.isEmpty()){
									List<Ticket> allTickets = new ArrayList<Ticket>();
									allTickets.addAll(newTickets);
									allTickets.addAll(updateTickets);
									System.out.println("Thread: " + threadNumber + "; Saving all tickets.. for event : " + allTickets.get(0).getEventId()+ " the site is: " + allTickets.get(0).getSiteId() + " the size of the all tickets is: " + allTickets.size());
									try{
										Date date1 = new Date();
										DAORegistry.getTicketDAO().saveOrUpdateAll(allTickets);
										Date date2 = new Date();
										long timeTaken =  date2.getTime() - date1.getTime();
										System.out.println("Thread: " + threadNumber + "; Time taken to save all tickets is: " + timeTaken);
									}catch(Exception e){
										System.out.println("Thread: " + threadNumber + "; Exception while saving all tickets");
										e.printStackTrace();
									}
								}else{
									if(newTickets != null && !newTickets.isEmpty()){
										System.out.println("Thread: " + threadNumber + "; Saving new tickets.. for event : " + newTickets.get(0).getEventId()+ " the site is: " + newTickets.get(0).getSiteId() + " the size of the new tickets is: " + newTickets.size());
										try{
											Date date1 = new Date();
											DAORegistry.getTicketDAO().saveOrUpdateAll(newTickets);
											Date date2 = new Date();
											long timeTaken =  date2.getTime() - date1.getTime();
											System.out.println("Thread: " + threadNumber + "; Time taken to save new tickets is: " + timeTaken);
										}catch(Exception e){
											System.out.println("Thread: " + threadNumber + "; Exception while saving new tickets");
											e.printStackTrace();
										}
									}
									if(updateTickets != null && !updateTickets.isEmpty()){
										System.out.println("Thread: " + threadNumber + "; Saving update tickets.. for event : " + updateTickets.get(0).getEventId()+ " the site is: " + updateTickets.get(0).getSiteId() + " the size of the update tickets is: " + updateTickets.size());
										try{
											Date date1 = new Date();
											DAORegistry.getTicketDAO().saveOrUpdateAll(updateTickets);
											Date date2 = new Date();
											long timeTaken =  date2.getTime() - date1.getTime();
											System.out.println("Thread: " + threadNumber + "; Time taken to save updated tickets is: " + timeTaken);
										}catch(Exception e){
											System.out.println("Thread: " + threadNumber + "; Exception while saving update tickets");
											e.printStackTrace();
										}
									}
								}
								TicketPersistenceTracker.getInstance().setCrawlPersisitenceStatusToPersisted(ticketData.getCrawlerId());
							}
						}else{
							continue;
						}
					}
				}else{
//					System.out.println("Thread: " + threadNumber + "; Nothing in the cache.. Going to sleep");
					Thread.sleep(5000);
				}
			}catch(Exception e){
				System.out.println("Thread: " + threadNumber + "; Error while processing data from ticket cache");
				e.printStackTrace();
			}
			
		}
	}
}
