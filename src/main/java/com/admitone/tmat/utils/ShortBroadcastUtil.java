package com.admitone.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneInventory;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ShortBroadcast;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.pojo.CastStatus;
import com.admitone.tmat.web.Constants;


public class ShortBroadcastUtil {

	/**
	 * getStatusByEvent
	 * 
	 * Will calculate and return an Event CastStatus that contains child
	 * Category statuses which contain individual statuses. All fields are
	 * calculated.
	 * 
	 * @param eventId
	 * @return CastStatus - an event CastStatus containing children
	 */
	public static CastStatus getStatusByEvent(Integer eventId, String catScheme) {
		Collection<ShortBroadcast> broadcasts = ShortBroadcastManager.getInstance().getShortBroadcastsByEvent(eventId);

		Event event = DAORegistry.getEventDAO().get(eventId);
		return getStatusByEvent(broadcasts, event, catScheme);
	}

	/**
	 * getStatusByEvent
	 * 
	 * Will calculate and return an Event CastStatus that contains child
	 * Category statuses which contain individual statuses. All fields are
	 * calculated. 
	 * 
	 * @param broadcasts - ArrayList of Broadcasts to compute
	 * @return CastStatus - an event CastStatus containing children
	 */
	public static CastStatus getStatusByEvent(Collection<ShortBroadcast>  broadcasts, Event event, String catScheme) {

		if(broadcasts == null) {
			return null;
		}
		
		Integer eventId = event.getId();
		
		Map<Integer, Collection<CastStatus>> ticketCastStatusesByCategoryId = new HashMap<Integer, Collection<CastStatus>>();
		Map<Integer, Category> categoryById = new HashMap<Integer, Category>();

		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());	
		
		/** Map of siteId to qty for entire event**/
		Map<String, Integer> quantityBySiteId = StatHelper.getTicketCountBySite(tickets);
		
		/** Map of siteId to qty by catSymbol **/
		Map<String, Map<String, Integer>> ticketCountBySiteIdAndCategorySymbol = StatHelper.getTicketCountByCategoryAndSite(tickets, catScheme);

		/** Map of siteId to qty by castId(section and row match) **/
		HashMap<Integer, Map<String, Integer>> castIdToExactMatch 
				= new HashMap<Integer, Map<String, Integer>>();
		
		
		DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");

		String eventString = event.getName();
		String venue = null;
		venue = event.getVenue().getLocation();
		String eventDate = "TBD";
		if(event.getDate()!=null){
			eventDate = formatDate.format(event.getDate());
		}
		
		Collection<AdmitoneInventory> aotickets = DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByEventId(event.getId());
		
		List<Ticket> inventoryTickets = new ArrayList<Ticket>();
		Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();

		Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
		Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
		for(Synonym synonym:synonyms){
			if(synonym.getType().equals("Theater")){
				theaterSynonymMap.put(synonym.getName(), synonym);
			}else{
				concertSynonymMap.put(synonym.getName(), synonym);
			}
		}
		Map<String,Synonym> synonymMap = null;
		if(event.getEventType().equals(TourType.THEATER)){
			synonymMap = theaterSynonymMap;
		}else if(event.getEventType().equals(TourType.CONCERT) || event.getEventType().equals(TourType.SPORT)){
			synonymMap = concertSynonymMap;
		}
		else{
			synonymMap =  new HashMap<String, Synonym>();
		}
		for(Ticket ticket:tickets){
//			if(ticket.getEvent().getEventType().equals(TourType.THEATER)){
//				if(TicketUtil.isInInventory(ticket,aotickets,theaterSynonymMap)){
//					inventoryTickets.add(ticket);
//				}
//			}else{
				if(TicketUtil.isInInventory(ticket,aotickets,synonymMap)){
					inventoryTickets.add(ticket);
				}
//			}
				
		}	
		Map<Integer, Category> catMap = new HashMap<Integer, Category>();
		if(event.getVenueCategory()!=null){
			for(Category cat:DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenue().getId(),catScheme)){
				catMap.put(cat.getId(), cat);
			}
		}
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getCategoryId(), list);
		}
		for(ShortBroadcast broadcast : broadcasts){
		
			CastStatus status = new CastStatus();
		
			Map<String, Integer> siteToExactQty 
					= StatHelper.getTicketCountByExactMatchAndSite(
					inventoryTickets, broadcast.getSection(), broadcast.getRow());
		
			castIdToExactMatch.put(broadcast.getId(), siteToExactQty);
			
			status.setCastId(broadcast.getId());
			status.setQuantity(broadcast.getQuantity().intValue());
			status.setPrice(broadcast.getPrice().doubleValue());
			status.setExposure(status.getQuantity() * status.getPrice());	
			status.setEventId(broadcast.getEvent().getId());
			status.setSection(broadcast.getSection());
			Integer categoryId =null;
//			if(event.getEventType().equals(TourType.THEATER)){
//				status.setNormalizedSection(SectionRowStripper.strip(event.getEventType(), broadcast.getSection(),theaterSynonymMap));
//				status.setGroupName("Section:" + broadcast.getNormalizedSection(theaterSynonymMap) + " Row:" + broadcast.getNormalizedRow());
//				categoryId = Categorizer.computeCategory(broadcast.getEvent().getVenueCategory(), broadcast.getNormalizedSection(theaterSynonymMap), broadcast.getNormalizedRow(), catScheme,catMap);
//			}else{
				status.setNormalizedSection(SectionRowStripper.strip(event.getEventType(), broadcast.getSection(),synonymMap));
				status.setGroupName("Section:" + broadcast.getNormalizedSection(synonymMap) + " Row:" + broadcast.getNormalizedRow());
				Category cat = Categorizer.computeCategory(broadcast.getEvent().getVenueCategory(), broadcast.getNormalizedSection(synonymMap), broadcast.getNormalizedRow(), catScheme,catMap,catMappingMap);
				if(cat!=null){
					categoryId = cat.getId();
				}
//			}
			
			status.setNormalizedRow(broadcast.getRow().replaceAll("\\W", ""));
			status.setRow(broadcast.getRow());
			status.setEventName(eventString);
			status.setVenue(venue);
			status.setDate(eventDate);
			
			
			

			if(categoryId == null){
				Category category = Categorizer.getCategoryByCatSection(broadcast.getSection(), broadcast.getRow(), event.getVenueCategory());
				if (category != null) {
					categoryId = category.getId();
					status.setCategoryId(categoryId.intValue());
					categoryById.put(categoryId, category);
				} else {
					
					//log.info("No categoryId found for event:" 
					//		+ cast.getEventId() + " section:" 
					//		+ cast.getSection() + " row:" + cast.getRow());
					status.setCategoryId(0);
					Category notSet = new Category();
					notSet.setId(0);
					notSet.setDescription("NOT SET");
					notSet.setSymbol("UNCAT");
					categoryById.put(new Integer(0), notSet);
				}
			} else {
				status.setCategoryId(categoryId.intValue());
				Category category = DAORegistry.getCategoryDAO().get(categoryId);
				categoryById.put(categoryId, category);
			}
			if(ticketCastStatusesByCategoryId.get(status.getCategoryId()) == null){
				ticketCastStatusesByCategoryId.put(status.getCategoryId(), new ArrayList<CastStatus>());
			} 
			ticketCastStatusesByCategoryId.get(status.getCategoryId()).add(status);
		}	
		
		ArrayList<CastStatus> categoryCastStatuses = new ArrayList<CastStatus>(ticketCastStatusesByCategoryId.keySet().size());
		
		CastStatus eventCastStatus = new CastStatus();

		int eventTotalQty = 0;
		double eventTotalPrice = 0;
		double eventTotalExposure = 0.00;

		String[] siteIds = Constants.getInstance().getSiteIds();
		
		HashMap<String, Integer> eventQtyBySiteId 
				= new HashMap<String, Integer>();
		
		for(String id : siteIds){
			eventQtyBySiteId.put(id, 0);
		}
		
		//for each category
		for(Integer categoryId : categoryById.keySet()) {
			CastStatus categoryCastStatus = new CastStatus();
			
			HashMap<String, Integer> catQuantityBySiteId = new HashMap<String, Integer>();
			
			for(String siteId : siteIds){	
				catQuantityBySiteId.put(siteId, 0);
			}
			
			List<CastStatus> ticketCastStatuses = new ArrayList<CastStatus>();
			
			int catTotalQty = 0;
			double catTotalPrice = 0;
			double catTotalExposure = 0.00;
			
			for(CastStatus ticketCastStatus : ticketCastStatusesByCategoryId.get(categoryId)){
				catTotalQty += ticketCastStatus.getQuantity();
				catTotalExposure += ticketCastStatus.getExposure();
				catTotalPrice += ticketCastStatus.getPrice() * ticketCastStatus.getQuantity();
			
				HashMap<String, Integer> ticketQuantityBySiteId  = new HashMap<String, Integer>();
				
				for(String siteId : siteIds) {
					Integer ticketTotalQty = castIdToExactMatch.get(ticketCastStatus.getCastId()).get(siteId);
					if (ticketTotalQty != null && ticketTotalQty > 0) {
						ticketQuantityBySiteId.put(siteId, ticketCastStatus.getQuantity());
						catQuantityBySiteId.put(siteId, ticketCastStatus.getQuantity() + catQuantityBySiteId.get(siteId));					
						eventQtyBySiteId.put(siteId, ticketCastStatus.getQuantity() + eventQtyBySiteId.get(siteId));
					}
				}
				ticketCastStatus.setQtyBySite(ticketQuantityBySiteId);
				ticketCastStatus.setTotalQtyBySite(castIdToExactMatch.get(ticketCastStatus.getCastId()));
				ticketCastStatuses.add(ticketCastStatus);	
			}
			
			categoryCastStatus.setEventId(eventId);
			categoryCastStatus.setEventName(eventString);
			categoryCastStatus.setVenue(venue);
			categoryCastStatus.setDate(eventDate);
			categoryCastStatus.setQuantity(catTotalQty);
			categoryCastStatus.setExposure(catTotalExposure);
			categoryCastStatus.setGroupName(categoryById.get(categoryId).getSymbol());
			categoryCastStatus.setCategoryId(categoryId);
			categoryCastStatus.setQtyBySite(catQuantityBySiteId);
			if (catTotalQty > 0) {
				categoryCastStatus.setPrice(catTotalPrice / catTotalQty);
			}
			
			if(ticketCountBySiteIdAndCategorySymbol != null) {
				categoryCastStatus.setTotalQtyBySite(ticketCountBySiteIdAndCategorySymbol.get(categoryById.get(categoryId).getSymbol()));
			} else {				
				Map<String, Integer> tempCatSiteQty = new HashMap<String, Integer>();
				
				for(String id : siteIds){
					tempCatSiteQty.put(id, 0);
				}
				categoryCastStatus.setTotalQtyBySite(tempCatSiteQty);
			}
			categoryCastStatus.setChildren(ticketCastStatuses);
			categoryCastStatuses.add(categoryCastStatus);

			eventTotalQty += catTotalQty;
			eventTotalExposure += catTotalExposure;
			eventTotalPrice += catTotalPrice;
		}
		orderCastStatuses(categoryCastStatuses);
	
		eventCastStatus.setEventId(eventId);
		eventCastStatus.setEventName(eventString);
		eventCastStatus.setVenue(venue);
		eventCastStatus.setDate(eventDate);		
		eventCastStatus.setQuantity(eventTotalQty);
		eventCastStatus.setExposure(eventTotalExposure);
		
		if (eventTotalQty > 0) {
			eventCastStatus.setPrice(eventTotalPrice / eventTotalQty);
		}
		
		eventCastStatus.setQtyBySite(eventQtyBySiteId);
		eventCastStatus.setTotalQtyBySite(quantityBySiteId);
		eventCastStatus.setChildren(categoryCastStatuses);
		
		return eventCastStatus;
	}

	/**
	 * getAllStatuses
	 * 
	 * Will calculate and return a Collection of Event CastStatus that 
	 * contains child Category statuses which contain individual statuses. 
	 * All fields are calculated.
	 * 
	 * @return Collection<CastStatus> - a collection of nested event CastStatuses
	 */
	public static Collection<CastStatus> getAllStatuses(String catScheme) {
		Set<Integer> eventIds = ShortBroadcastManager.getInstance().getAllShortCastEventIds();
		
		Collection<CastStatus> statuses = new ArrayList<CastStatus>();
		
		for(Integer eventId:eventIds){
			statuses.add(getStatusByEvent(eventId, catScheme));
		}
		return statuses;
	}

	/**
	 * orderCastStatuses - THIS METHOD MUTATES THE PARAMETER broadcasts
	 * 
	 * This method will mutate the parameter List statuses and 
	 * order it by:
	 * a.) Set categories before unset categories
	 * b.) Lower(older) category ids before higher category ids
	 *
	 * @param broadcasts - List of CastStatus to be ordered
	 * @return void 
	 */
	public static void orderCastStatuses(List<CastStatus> broadcasts){
		Collections.sort(broadcasts, new Comparator<CastStatus>() {
			
			public int compare(CastStatus cast1, CastStatus cast2) {
				if(cast1.getCategoryId() == 0 && cast2.getCategoryId() != 0) {
					return -1;
				} else if(cast1.getCategoryId() != 0 && cast2.getCategoryId() == 0) {
					return 1;
				} else if(cast1.getCategoryId() > cast2.getCategoryId()) {
					return 1;
				} else if(cast1.getCategoryId() < cast2.getCategoryId()) {
					return -1;
				}
				return 0;
			}
		});
	}
}
