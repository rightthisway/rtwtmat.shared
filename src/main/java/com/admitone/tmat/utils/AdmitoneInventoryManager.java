package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneInventory;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ShortTransaction;
//import com.admitone.tmat.data.Tour;

/**
 * Admitone Inventory Manager.
 * 
 *  TODO:
 *  - make direct access to database instead of putting that in memory.
 */
public class AdmitoneInventoryManager {
	
	protected static AdmitoneInventoryManager instance = null;

	private final Logger log = LoggerFactory.getLogger(AdmitoneInventoryManager.class);
	
	// every 10 mn
	protected static final int DEFAULT_INV_TIMER_WAIT = 10 * 60 * 1000;
		
	/* Map<AOneEventId, AdmitoneInventory> */
	
	// admit one event id => list of inventories
	private Map<Integer, List<AdmitoneInventory>> ticketsByAdmitoneId = new HashMap<Integer, List<AdmitoneInventory>>();

	// admit one event id + qty => list of inventories
	private Map<String, List<AdmitoneInventory>> ticketsByAdmitoneIdQty = new HashMap<String, List<AdmitoneInventory>>();
	
	// tmat event id => list of inventories
	private Map<Integer, List<AdmitoneInventory>> ticketsByEventId = new HashMap<Integer, List<AdmitoneInventory>>();
	
	// admit one event id => tmat event id
	private Map<Integer, Integer> eventByAdmitoneId = new HashMap<Integer, Integer>();
	
	// tmat event id => admitone event id
	private Map<Integer, Integer> admitoneIdByEventId = new HashMap<Integer, Integer>();
	
	// id of events which has inventory
	private HashSet<Integer> eventIdsWithInventory = new HashSet<Integer>();
	
	private AdmitoneInventoryManager() {
		Timer timer = new Timer();
		//run every 15 minutes by default
		timer.scheduleAtFixedRate(
				new TimerTask() {
					@Override
					public void run(){
						try {
							log.info(";;;;;;;;;;START TO LOAD ADMITONE INVENTORY " + new Date());
							instance.reinit();
							log.info(";;;;;;;;;;FINISH TO LOAD ADMITONE INVENTORY " + new Date());
						} catch (Exception e) {
							System.out.println(";;;;;;;;;;ERROR WHILE LOADING ADMITONEINVENTORY " + new Date());
							e.printStackTrace();
						}
					}
				}, new Date(), DEFAULT_INV_TIMER_WAIT);
	}

	public static AdmitoneInventoryManager getInstance() {
		if (instance == null) {
			instance = new AdmitoneInventoryManager();
		}
		return instance;		
	}

	/**
	 * addInventory
	 * 
	 * This method will add the parameter newInventory to Memory ONLY
	 * 
	 * @param newInventory - inventory to add
	 * @return void
	 */
	private void addInventory(AdmitoneInventory newInventory) {
		//log.info("adding Inventory: " + newInventory.toString());
		List<AdmitoneInventory> inventories = ticketsByAdmitoneId.get(newInventory.getEventId());
		
		if(inventories == null){
			inventories = new ArrayList<AdmitoneInventory>();
			ticketsByAdmitoneId.put(newInventory.getEventId(), inventories);
		} 
		
		inventories.add(newInventory);

		String key = newInventory.getEventId() + "-" + newInventory.getQuantity();

		List<AdmitoneInventory> inventories2 = ticketsByAdmitoneIdQty.get(key);
		if (inventories2 == null) {
			inventories2 = new ArrayList<AdmitoneInventory>();
			ticketsByAdmitoneIdQty.put(key, inventories2);
		}
		inventories2.add(newInventory);
		
		Integer eventId = this.getEventIdByAdmitoneId(newInventory.getEventId());


		if(eventId != null){
			eventIdsWithInventory.add(eventId);
			
			List<AdmitoneInventory> inventories3 = ticketsByEventId.get(eventId);
			if(inventories3 == null){
				inventories3 = new ArrayList<AdmitoneInventory>();
				ticketsByEventId.put(eventId, inventories3);
			} 
			inventories3.add(newInventory);
		}
	}

	/**
	 * getInventoryByAdmitoneEvent
	 * 
	 * @param eventId - Admitone Integer Id to get inventory for
	 * @return ArrayList<AdmitoneInventory> - all A1 Inventory for this event
	 */
	public List<AdmitoneInventory> getInventoryByAdmitoneEvent(Integer eventId) {
		return ticketsByAdmitoneId.get(eventId);
	}
	
	/**
	 * getInventoryByAdmitoneEvent
	 * 
	 * @param eventId - Admitone Integer Id to get inventory for
	 * @return ArrayList<AdmitoneInventory> - all A1 Inventory for this event
	 */
	public List<AdmitoneInventory> getInventoryByAdmitoneEventQuantity(Integer eventId, Integer quantity) {
		return ticketsByAdmitoneIdQty.get(eventId + "-" + quantity);
	}


	public List<AdmitoneInventory> getInventoryByEvent(Integer eventId) {
		if(admitoneIdByEventId.get(eventId) == null){
			return new ArrayList<AdmitoneInventory>();
		}
		
		List<AdmitoneInventory> result = ticketsByAdmitoneId.get(admitoneIdByEventId.get(eventId));
		if (result == null) {
			return new ArrayList<AdmitoneInventory>();
		}
		
		return result;
	}

	/**
	 * getAllInventoryEventIds
	 * 
	 * @return Set<Integer> - all of the eventIds that have AdmitoneInventory
	 */
	public Set<Integer> getAllInventoryEventIds() {
		return admitoneIdByEventId.keySet();
	}
	
	/**
	 * getAllInventoryAOneIds
	 * 
	 * @return Set<Integer> - all of the eventIds that have AdmitoneInventory
	 */
	public Set<Integer> getAllInventoryAOneIds() {
		return ticketsByAdmitoneId.keySet();
	}

	/**
	 * getAllInventoryAOneIds
	 * 
	 * @return Set<Integer> - all of the eventIds that have AdmitoneInventory AND cost is not 0.00
	 */
	public Set<Integer> getAllTransactionInventoryEventIds() {
		return eventIdsWithInventory;
	}

	/**
	 * getEventIdByAdmitoneId
	 * 
	 * @param eventId - Admitone Integer Id to get inventory for
	 * @return ArrayList<AdmitoneInventory> - all A1 Inventory for this event
	 */
	public Integer getEventIdByAdmitoneId(Integer eventId) {
		return eventByAdmitoneId.get(eventId);
	}
	
	public Integer getAdmitoneIdByEventId(Integer eventId) {
		return admitoneIdByEventId.get(eventId);
	}

	protected void reinit(){
		System.out.println("REINIT ADMITONE INVENTORY");
		Collection<Integer> admitoneIds = DAORegistry.getAdmitoneEventDAO().getAllEventIds();
		// if it is null, that means that the replication is going on and during that time
		// all the inventory records are removed from DB.
		// skip it for now and will update the inventory in memory later.
		if (admitoneIds == null || admitoneIds.isEmpty()) {
			System.out.println("Admit One Event IDS is empty");
			return;
		}
		
		Collection<AdmitoneInventory> inventory = DAORegistry.getAdmitoneInventoryDAO().getAll();
		if (inventory == null || inventory.isEmpty()) {
			System.out.println("Admit One inventory is empty");
			return;
		}
		
		ticketsByAdmitoneId = new HashMap<Integer, List<AdmitoneInventory>>();
		ticketsByAdmitoneIdQty = new HashMap<String, List<AdmitoneInventory>>();
		ticketsByEventId = new HashMap<Integer, List<AdmitoneInventory>>();
		eventByAdmitoneId = new HashMap<Integer, Integer>();
		admitoneIdByEventId = new HashMap<Integer, Integer>();
		eventIdsWithInventory = new HashSet<Integer>();

		Set<Integer> admitoneEventIds = new HashSet<Integer>(admitoneIds); 			
		
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEvents();
		for(Event event: events) {
			Integer admitoneEventId = event.getAdmitoneId();
			if(admitoneEventId != null && admitoneEventIds.contains(admitoneEventId)){
				eventByAdmitoneId.put(admitoneEventId, event.getId());
				admitoneIdByEventId.put(event.getId(), admitoneEventId);
			}
		}

		for(AdmitoneInventory inv : inventory){
			this.addInventory(inv);
		}	
		System.out.println("Finished adding inventory: " + eventByAdmitoneId);
	}
	
	/*public List<Integer> getTourAdmitoneInventoryEventIds(Tour tour) {
		Collection<Event> events = tour.getEvents();
		List<Integer> returnIds = new ArrayList<Integer>();
		for(Event event : events){
			if(eventIdsWithInventory.contains(event.getId())){
				returnIds.add(event.getId());
			}
		}
		return returnIds;
	}*/

	@Deprecated
	public Collection<ShortTransaction> getShortsByEvent(Integer eventId) {
		Collection<ShortTransaction> shortTransactions = new ArrayList<ShortTransaction>();
		
		Collection<AdmitoneInventory> inventory = getInventoryByEvent(eventId);
		System.out.println(">>>>>>>>> INVENTORY=" + inventory);
		if(inventory == null || inventory.isEmpty()){
			inventory = getInventoryByAdmitoneEvent(eventId);
		} 
		if(inventory != null){				
			for(AdmitoneInventory inv : inventory){
				if(inv.getCost() != null && inv.getCost() > 0.00) {						
					ShortTransaction longTransaction = new ShortTransaction(inv);
					shortTransactions.add(longTransaction);
				}
			}
		}
		return shortTransactions;
	}
}
