package com.admitone.tmat.utils;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.admitone.tmat.web.pojo.WebTicketRow;

public class TicketSorter {
	
	public static final int REMAINING_QUANTITY_SORT = 1; 
	public static final int CATEGORY_SORT = 2;
	public static final int SECTION_SORT = 3;
	public static final int ROW_SORT = 4;
	public static final int CURRENT_PRICE_SORT = 5; 
	public static final int BUY_IT_NOW_PRICE_SORT = 6;
	public static final int PURCHASE_PRICE = 7;
	public static final int TITLE_SORT = 8; 
	public static final int SELLER_SORT = 9; 
	public static final int LOT_SIZE_SORT = 10; 
	public static final int SOLD_QUANTITY_SORT = 11; 
	public static final int ITEM_ID_SORT = 12; 
	public static final int END_DATE_SORT = 13; 
	public static final int INSERTION_DATE_SORT = 14; 
	public static final int LAST_UPDATE_SORT = 15; 
	public static final int STATUS_SORT = 16;
	public static final int SEAT_SORT = 18;
	public static final int ACTION_INDICATOR_SORT = 19;
	public static final int E_VAL_SORT = 20;
	
	public static void sortTickets(List<WebTicketRow> tickets, final int[] sortFields, final String catScheme) {
		
	if(tickets == null){
		return;
	}
	if(tickets.isEmpty()){
		return;
	}
		Collections.sort(tickets, new Comparator<WebTicketRow>() {
			
			private int compareLists(Object[] list1,Object[] list2) {
				for(int i = 0 ; i < list1.length ; i++) {
					if (list1[i] == null) {
						if (list2[i] == null) {
							continue;
						} else {
							return -1;
						}
					} else if (list2[i] == null) {
						return 1;
					}
					
					if (list1[i] instanceof String) {
						String str1 = (list1[i] == null)?"":((String)list1[i]).trim().toLowerCase();
						String str2 = (list2[i] == null)?"":((String)list2[i]).trim().toLowerCase();
						
						int n1 = -1;
						try {
							n1 = Integer.parseInt(str1, 10);
						} catch (Exception e) {						
						}
	
						int n2 = -1;
						try {
							n2 = Integer.parseInt(str2, 10);
						} catch (Exception e) {						
						}
	
						// if the string are number, compare them as numbers
						if (n1 != -1 && n2 != -1) {
							if (n1 < n2) {
								return -1;
							} else if (n1 > n2) {
								return 1;
							}							
						} else {
							// number appears before string
							if (n1 != -1) {
								return -1;
							} else if (n2 != -1) {
								return 1;
							}
							if (!str1.equals(str2)) {
								if (str1.compareTo(str2) < 0) {
									return -1;
								} else {
									return 1;
								}
							}
						}
					} else if (list1[i] instanceof Integer) {
						int r = ((Integer)list1[i]).compareTo((Integer)list2[i]);
						if (r != 0) {
							return r;
						}
					} else if (list1[i] instanceof Double) {
						int r = ((Double)list1[i]).compareTo((Double)list2[i]);
						if (r != 0) {
							return r;
						}
					} else if (list1[i] instanceof Date) {
						int r = ((Date)list1[i]).compareTo((Date)list2[i]);
						if (r != 0) {
							return r;
						}
					}
	
				}
				return 0;
			}
			
			private int simpleCompare(WebTicketRow ticket1, WebTicketRow ticket2, int pSort, int pOrder) {
				switch(pSort) {
				case LOT_SIZE_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getLotSize()},
							new Object[] {ticket2.getLotSize()}
					);
				case REMAINING_QUANTITY_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getRemainingQuantity()},
							new Object[] {ticket2.getRemainingQuantity()}
					);
				case CATEGORY_SORT:
					return pOrder * compareLists(
//							new Integer[] {(ticket1.getCategoryMappingId() == null)?999999999:ticket1.getCategoryMappingId()},
//							new Integer[] {(ticket2.getCategoryMappingId() == null)?999999999:ticket2.getCategoryMappingId()}
							new String[] {(ticket1.getCategory() == null)?null:ticket1.getCategory().getSymbol()},
							new String[] {(ticket2.getCategory() == null)?null:ticket2.getCategory().getSymbol()}
					);
				case SECTION_SORT:
					return pOrder * compareLists(
							new String[] {ticket1.getNormalizedSection()},
							new String[] {ticket2.getNormalizedSection()}
					);
				case ROW_SORT:
					return pOrder * compareLists(
							new String[] {ticket1.getNormalizedRow()},
							new String[] {ticket2.getNormalizedRow()}
					);
				case CURRENT_PRICE_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getAdjustedCurrentPrice()},
							new Object[] {ticket2.getAdjustedCurrentPrice()}
					);
				case BUY_IT_NOW_PRICE_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getBuyItNowPrice()},
							new Object[] {ticket2.getBuyItNowPrice()}
					);
				case PURCHASE_PRICE:
					return pOrder * compareLists(
							new Object[] {ticket1.getPurchasePrice()},
							new Object[] {ticket2.getPurchasePrice()}
					);
				case SOLD_QUANTITY_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getSoldQuantity()},
							new Object[] {ticket2.getSoldQuantity()}
					);
				case SELLER_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getSeller()},
							new Object[] {ticket2.getSeller()}
					);
				case ITEM_ID_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getItemId()},
							new Object[] {ticket2.getItemId()}
					);
				case END_DATE_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getEndDate()},
							new Object[] {ticket2.getEndDate()}
					);
				case INSERTION_DATE_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getInsertionDate()},
							new Object[] {ticket2.getInsertionDate()}
					);
				case LAST_UPDATE_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getLastUpdate()},
							new Object[] {ticket2.getLastUpdate()}
					);
				case STATUS_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getTicketStatus().toString()},
							new Object[] {ticket2.getTicketStatus().toString()}
					);
				case SEAT_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getSeat()},
							new Object[] {ticket2.getSeat()}
					);
				//FIXME - To Be Implemented
	//			case ACTION_IND_SORT:
	//				return pOrder * compareLists(
	//						new Object[] {ticket1.getStartSeat()},
	//						new Object[] {ticket2.getStartSeat()}
	//				);
				case E_VAL_SORT:
					return pOrder * compareLists(
							new Object[] {ticket1.getValuationFactor()},
							new Object[] {ticket2.getValuationFactor()}
					);
				}		
				return 0;
			}
	
			public int compare(WebTicketRow ticket1, WebTicketRow ticket2) {
				// if we sort on a single fields, have some presets, so that
				// if we sort by lot size => sort by lot size, category, section, row
				// if we sort by rem qty => sort by rem qty, category, section, row
				// if we sort by category => sort by category, section, row
								
				int[] newSortFields;

				if (sortFields.length == 1) {
					int parameterField = sortFields[0]  % 100;
					int parameterOrderOffset = (((int)(sortFields[0] / 100)) == 0)?0:100;
					
					switch(parameterField) {
					case LOT_SIZE_SORT:
						newSortFields = new int[]{TicketSorter.LOT_SIZE_SORT + parameterOrderOffset, TicketSorter.CATEGORY_SORT + parameterOrderOffset, TicketSorter.SECTION_SORT + parameterOrderOffset, TicketSorter.ROW_SORT + parameterOrderOffset};
						break;
						
					case REMAINING_QUANTITY_SORT:
						newSortFields = new int[]{TicketSorter.REMAINING_QUANTITY_SORT + parameterOrderOffset, TicketSorter.CATEGORY_SORT + parameterOrderOffset, TicketSorter.SECTION_SORT + parameterOrderOffset, TicketSorter.ROW_SORT + parameterOrderOffset};
						break;
						
					case CATEGORY_SORT:
						newSortFields = new int[]{TicketSorter.CATEGORY_SORT + parameterOrderOffset, TicketSorter.SECTION_SORT + parameterOrderOffset, TicketSorter.ROW_SORT + parameterOrderOffset};
						break;
						
					case SECTION_SORT:
						newSortFields = new int[]{TicketSorter.SECTION_SORT + parameterOrderOffset, TicketSorter.ROW_SORT + parameterOrderOffset};
						break;
						
					default:
						newSortFields = sortFields;
					}
				} else {
					newSortFields = sortFields;
				}
				
				// if we sort on multiple fields
				for (int i = 0 ; i < newSortFields.length ; i++) {
					if (newSortFields[i] == 0) {
						continue;
					}
					int parameterSort = newSortFields[i] % 100;
					int parameterOrder = (((int)(newSortFields[i] / 100)) == 0)?1:-1;
					int r = simpleCompare(ticket1, ticket2, parameterSort, parameterOrder);//,ticket1.getEvent().getTourId());
					if (r != 0) {
						return r;
					}					
				}
				return 0;					
			}				
		});
	}
	
	private static Map<String, Integer> sortStringMap = new HashMap<String, Integer>();
	private static Map<Integer,String> sortNameMap = new HashMap<Integer,String>();
	
	static {
		Object[][] sortStringMappings = new Object[][] {
				{"remainingQuantity", TicketSorter.REMAINING_QUANTITY_SORT},
				{"category", TicketSorter.CATEGORY_SORT},
				{"section", TicketSorter.SECTION_SORT},
				{"row", TicketSorter.ROW_SORT},
				{"currentPrice", TicketSorter.CURRENT_PRICE_SORT},
				{"buyItNowPrice", TicketSorter.BUY_IT_NOW_PRICE_SORT},
				{"title", TicketSorter.TITLE_SORT},
				{"soldQuantity", TicketSorter.SOLD_QUANTITY_SORT},
				{"seller", TicketSorter.SELLER_SORT},
				{"lotSize", TicketSorter.LOT_SIZE_SORT},
				{"itemId", TicketSorter.ITEM_ID_SORT},
				{"endDate", TicketSorter.END_DATE_SORT},
				{"lastUpdate", TicketSorter.LAST_UPDATE_SORT},
				{"status", TicketSorter.STATUS_SORT},
				{"seat", TicketSorter.SEAT_SORT},
				{"actionIndicator", TicketSorter.ACTION_INDICATOR_SORT},
				{"eVal", TicketSorter.E_VAL_SORT}
		};
		
		for(Object[] mapping: sortStringMappings) {
			sortStringMap.put((String)mapping[0], (Integer)mapping[1]);
		}		

		for(Object[] mapping: sortStringMappings) {
			sortNameMap.put((Integer)mapping[1], (String)mapping[0]);
		}		
		
		int[] sortFields = new int[] {REMAINING_QUANTITY_SORT, CATEGORY_SORT, SECTION_SORT, ROW_SORT, CURRENT_PRICE_SORT,
				BUY_IT_NOW_PRICE_SORT, TITLE_SORT, SELLER_SORT,  LOT_SIZE_SORT, SOLD_QUANTITY_SORT, ITEM_ID_SORT,
				END_DATE_SORT, INSERTION_DATE_SORT, LAST_UPDATE_SORT, STATUS_SORT, SEAT_SORT, ACTION_INDICATOR_SORT, E_VAL_SORT};
		
		for(int field: sortFields) {
			sortStringMap.put(Integer.toString(field), field);
		}
		
	}
	
	
	public static int parseSort(String sortString) {		
		Integer r = sortStringMap.get(sortString);
		if (r == null) {
			return 0;
		}
		return r;
	}
	
	public static String getSortName(int sort) {		
		return sortNameMap.get(sort);
	}
}
