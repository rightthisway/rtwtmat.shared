package com.admitone.tmat.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneEventLocal;
import com.admitone.tmat.data.AdmitoneInventory;
import com.admitone.tmat.data.AdmitoneInventoryLocal;
import com.admitone.tmat.data.AdmitoneTicketMark;
import com.admitone.tmat.data.BaseTicket;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.DuplicateTicketMap;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.EventBookmark;
import com.admitone.tmat.data.EventPriceAdjustment;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.ebay.EbayUtil;
import com.admitone.tmat.enums.AdmitoneTicketMarkType;
import com.admitone.tmat.enums.PriceRounding;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.pojo.CheapestZone;
import com.admitone.tmat.pojo.SimpleTicket;
import com.admitone.tmat.util.service.impl.TicketGroupingManager;
import com.admitone.tmat.web.Constants;
import com.admitone.tmat.web.pojo.WebTicketRow;
import com.sun.syndication.fetcher.FetcherException;
import com.sun.syndication.io.FeedException;




/**
 * Ticket Util Class.
 */
public final class TicketUtil {
	public static final Integer MAX_QTY = new Integer(11);
	private static final Logger logger = LoggerFactory.getLogger(TicketUtil.class);
	private static final Double SIMILARITY_PRICE_THRESHOLD_PERCENT = 0.80; // not used for smart remove
//	private static final Double SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT = 0.20;   // Old markup % 
	private static final Double SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT = 0.25;   // Chagned from 18 to 25 by Chirag on Joe's Request
	
	// site ranking when comparing tickets
	private static Map<String, Integer> siteRank = new HashMap<String, Integer>();
	private static CurrencyRSSFetcher gbpFetcher = null;
	private static CurrencyRSSFetcher eurFetcher = null;
	// eventId-siteId => price adjustment
	private static Map<String, Double> percentAdjustmentMap = new HashMap<String, Double>();
	private static Map<String, PriceRounding> priceRoundingMap = new HashMap<String, PriceRounding>();
	private static String[] rankedSiteIds = {Site.AOP, Site.EIBOX, Site.EI_MARKETPLACE, Site.TICKET_NETWORK, Site.TICKET_NETWORK_DIRECT,
							Site.STUB_HUB, Site.STUBHUB_FEED, Site.TICKET_SOLUTIONS, Site.WS_TICKETS,Site.TICKET_EVOLUTION,
							Site.EBAY, Site.VIAGOGO, Site.SEATWAVE, Site.GET_ME_IN,
							Site.RAZOR_GATOR, Site.TICKET_MASTER, Site.EVENT_INVENTORY, Site.TICKETS_NOW,Site.VIVIDSEAT,Site.FLASH_SEATS, Site.TICK_PICK, Site.TICKET_CITY,Site.TICKET_LIQUIDATOR,Site.STUB_HUB_API,Site.FAN_SNAP , Site.SEATGEEK
	};

	private static String[] reversedRankedSiteIds;
	private static Pattern seatRangePattern = Pattern.compile("[0]*([0-9])+\\s+-\\s+([0]*[0-9])+");
	private static Pattern singleSeatPattern = Pattern.compile("([0]*[0-9])+");
	private static final int CREATE_GROUP_TIME_DURATION = 1000*60*5; 
	private static final int DEFAULT_MARK_UP = 0; 
	private static final int DEFAULT_SELL_QTY = 10; 
	private static final double DEFAULT_RPT_FACTOR = 20;
	private static final double DEFAULT_SHIPPING_CHARG = 4.0;
	private static final int DEFAULT_EXPOSOR = 2;
	private static  TicketGroupingManager ticketGroupingManager=new TicketGroupingManager();
	
	static {
		// 1- IF EIMP and ANY other sites….THEN scrub all dupes and display ONLY
		// EIMP
		// 2- IF no EIMP, BUT Ticket Network and ANY other sites….THEN scrub all
		// dupes and display ONLY Ticket Network
		// 3- IF no EIMP and Ticket Network, BUT Stubhub and ANY other
		// sites…then scrub all dupes and display ONLY StubHub
		// 4- IF no EIMP and Ticket Network, and StubHub, BUT Ticketsnow and ANY
		// other sites…then scrub all dupes and display ONLY Ticketsnow
		// 5- IF no EIMP and TicketNetwork, StubHub, TicketsNow, BUT Event
		// Inventory and ANY other sites….then scrub all dupes and display only
		// Event Inventory
		// 6- IF no EIMP and Ticket Network and StubHub and Ticketsnow, BUT
		// Razorgator and ANY other sites…then scrub all dupes and display ONLY
		// Razorgator
		// 7- IF no EIMP, Ticket Network, StubHub, Ticketsnow, Razorgator, BUT
		// Ticketsolutions and ANY other sites…then scrub all dupes and display
		// ONLY Ticketsolutions
		// 8- IF no EIMP, TicketNetwork, StubHub, Ticketsnow, Razorgator,
		// Ticketsolutions, BUT Western States and ANY other sites…then scrub
		// all dupes and display only Western States
		// 9- IF no EIMP, Ticket Network, Stub Hub, Ticketsnow, RazorGator,
		// Ticketsoultions, Western States, but Viagogo and ANY other site…then
		// scrub all dupes and display ONLY Viagogo
		// 10- IF no EIMP, TicketNetwork, StubHub, TicketsNow, Rzrogator,
		// Ticketsolutions, Western State, Viagogo, but Seatwave and ANY other
		// site…then scrub all dupes and display ONLY Seatwave
		// 11 – IF no EIMP, TicketNetwork, StubHub, Ticketsnow, Razorgator,
		// Ticketsolutions, Western State, Viagogo, Seatwave, but GetMeIn and
		// ANY other site…then scrub all dupes and display ONLY GetMeIn
		// 12- IF no EIMP, TicektNetwork, StubHub, Ticektsnow, Razorgator,
		// Ticketsolutions, Western States, Viagogo, Seatwave, GetMeIn, but EBay
		// and ANY other site, then scrub all dupes and display ONLY Ebay.

		// orders: EIMP > Ticket Network > StubHub > TicketsNow > RazorGator >
		// TicketsSolutions > Western States > Viagogo > SeatWave > GetMeIn >
		// eBay > FanSnap > TicketMaster
		
		int rank = 1;
		for (String siteId : rankedSiteIds) {
			siteRank.put(siteId, rank);
			rank++;
		}	

		reversedRankedSiteIds = new String[rankedSiteIds.length];
		for (int i = 0; i < rankedSiteIds.length; i++) {
			reversedRankedSiteIds[rankedSiteIds.length - 1 - i] = rankedSiteIds[i];
		}	
		
		try{
			gbpFetcher = new CurrencyRSSFetcher("http://coinmill.com/rss/GBP_USD.xml");
			eurFetcher = new CurrencyRSSFetcher("http://coinmill.com/rss/EUR_USD.xml");
		} catch (FeedException feed) {
			System.out.println("Caught FeedException");
			feed.printStackTrace();
		} catch (FetcherException fete) {
			System.out.println("Caught FetcherException");
			fete.printStackTrace();
		} catch (Exception e) {
			System.out.println("Caught Exception getting currency fetcher: " + e.getMessage());
			e.printStackTrace();
		}		
	}

	/**
	 * Get ticket duplicate key. (allows to identify a unique ticket)
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 */
	private static MessageDigest md5MessageDigest = null;	
	static {
		try {
			md5MessageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public static void flushAdjustmentMaps(){
		percentAdjustmentMap = new HashMap<String, Double>();
		priceRoundingMap = new HashMap<String, PriceRounding>();
	}

	private static String getAdjustKey(BaseTicket ticket) {
		return ticket.getEventId() + "-" + ticket.getSiteId();
	}
	
	// return web ticket rows from ticket (will set duplicate ticket field)
	// that will allow in the front end to show duplicated tickets
	public static List<WebTicketRow> getWebTicketRows(Collection<Ticket> origTickets, Collection<Ticket> nonDuplicateTickets, Boolean showDuplicates) {
		List<WebTicketRow> webTicketRows = new ArrayList<WebTicketRow>();
		
		if(origTickets == null || origTickets.isEmpty()){
			return webTicketRows;
		}

		if (!showDuplicates) {
			for (Ticket ticket: nonDuplicateTickets) {
				WebTicketRow webTicketRow = new WebTicketRow(ticket);
				Category category = ticket.getCategory();
				if(category!=null){
					webTicketRow.setCategory(category);
					webTicketRow.setCategoryId(category.getId());
					webTicketRow.setCategoryMappingId(ticket.getCategoryMappingId());
					webTicketRow.setCategorySymbol(category.getSymbol());
					
				}
				webTicketRow.setPriceByQuantity(ticket.getPriceByQuantity());
				webTicketRow.setPurchasePrice(ticket.getPurchasePrice());
				webTicketRows.add(webTicketRow);				
			}
			return webTicketRows;
		}
		
		
		Map<Integer, Ticket> tixMap = new HashMap<Integer, Ticket>();
		for (Ticket ticket: origTickets) {
			tixMap.put(ticket.getId(), ticket);
		}

		Map<Integer, Ticket> nonDupTixMap = new HashMap<Integer, Ticket>();
		for (Ticket ticket: nonDuplicateTickets) {
			nonDupTixMap.put(ticket.getId(), ticket);
		}
		
		for (Ticket ticket: origTickets) {
			WebTicketRow webTicketRow = new WebTicketRow(ticket);
			webTicketRow.setCategoryMappingId(ticket.getCategoryMappingId());
			webTicketRow.setPriceByQuantity(ticket.getPriceByQuantity());
			webTicketRow.setPurchasePrice(ticket.getPurchasePrice());
			Category category = ticket.getCategory();
			if(category!=null){
				webTicketRow.setCategory(category);
				webTicketRow.setCategoryId(category.getId());
				webTicketRow.setCategoryMappingId(ticket.getCategoryMappingId());
				webTicketRow.setCategorySymbol(category.getSymbol());
			}
			if (nonDupTixMap.get(ticket.getId()) == null) {
				webTicketRow.setRemovedDuplicateTicket(true);
				Ticket curTicket = ticket;
				if (curTicket.getDupRefTicketId() != null) {
					int maxIter = 10;
					int i = 0;
					do {
						curTicket = tixMap.get(curTicket.getDupRefTicketId());
						if (curTicket == null) {
							break;
						}
						i++;
					} while (curTicket.getDupRefTicketId() != null && i < maxIter);
					
					if (curTicket != null) {
						webTicketRow.setDupRefTicketId(curTicket.getId());
					}
				}
//				System.out.println("ticket: " + ticket + " is scrubbed by " + curTicket);
			}
			webTicketRows.add(webTicketRow);
		}
	
		return webTicketRows;
	}
	
	public static Integer getRemovedDuplicateCount(Collection<WebTicketRow> ticketRows) {
		int numDups = 0;
		
		for (WebTicketRow row: ticketRows) {
			if (row.isRemovedDuplicateTicket()) {
				numDups++;
			}
		}
		
		return numDups;
	}
	
	/**
	 * Generate the ticket duplicate key from the fields of the ticket.
	 * @return duplicate key.
	 */

	public static String getTicketDuplicateKey(Ticket ticket, RemoveDuplicatePolicy removeDuplicatePolicy) { 	
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow());
		}
	
		String admitoneTicket = "";
		if(ticket.isAdmitOneTicket()){
			admitoneTicket = "ADMT1";
		}
		String key = "" + admitoneTicket
			 	+ ticket.getTicketStatus()
		 		+ ticket.getRemainingQuantity() + "," + section + ","
				+ row;
		key=key.toLowerCase();
		
		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}


	public static String getTicketDuplicateByQuantitySectionRowPriceKey(Ticket ticket) { 	
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow());
		}
	
		String key = "" + 
		 		+ ticket.getRemainingQuantity() + "," + section + ","
				+ row;// + "," + ticket.getAdjustedCurrentPrice();

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}

	/**
	 * Return true if ticket1 is better than ticket2 by using the following rules:
	 * - cheaper price
	 * - if price are equals compare the site ranking
	 * @param ticket1
	 * @param ticket2
	 * @return
	 */
	public static boolean isBetterTicket(Ticket ticket1, Ticket ticket2) {
/*
		boolean isAdm1 = isAdmitOneTicket(ticket1);
		boolean isAdm2 = isAdmitOneTicket(ticket2);
		
		if (isAdm1 && !isAdm2) {
			return true;
		}

		if (!isAdm1 && isAdm2) {
			return false;
		}
*/
		return (ticket1.getCurrentPrice() < ticket2.getCurrentPrice()
				|| (ticket1.getCurrentPrice().equals(ticket2.getCurrentPrice()) 
						&& siteRank.get(ticket1.getSiteId()) < siteRank.get(ticket2.getSiteId())));
	}

	/**
	 * Remove duplicate tickets.
	 * @param tickets
	 * @return
	 */
	public static List<Ticket> removeDuplicateTickets(Collection<Ticket> tickets) {
		return removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
	}

	public static List<WebTicketRow> removeDuplicateWebTickets(Collection<WebTicketRow> webTickets) {
		// convert harvest to tickets
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		for (WebTicketRow webTicketRow: webTickets) {
			tickets.add(new Ticket(webTicketRow));
		}

		List<Ticket> ticketResult = removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		
		List<WebTicketRow> result = new ArrayList<WebTicketRow>();
		for (Ticket ticket: ticketResult) {
			result.add(new WebTicketRow(ticket));
		}
		
		return result;
	}

	public static List<Ticket> manualRemoveDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy initialRemoveDuplicatePolicy) {
		if (tickets == null || tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}
		
		if (!initialRemoveDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			tickets = removeDuplicateTickets(tickets, initialRemoveDuplicatePolicy);
		}
		
		Integer eventId = tickets.iterator().next().getEventId();
		Map<Integer, Long> duplicateTicketMap = new HashMap<Integer, Long>();

		//This is to make sure we don't remove more than one duplicate tickets from the the same Site 
		Map<Long, Collection<String>> keyToSiteMap = new HashMap<Long, Collection<String>>();
		
		for (DuplicateTicketMap duplicateTicket: DAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(eventId)) {
			duplicateTicketMap.put(duplicateTicket.getTicketId(), duplicateTicket.getGroupId());
		}

		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		Map<Long, Ticket> scannedTickets = new HashMap<Long, Ticket>();
		for (Ticket ticket: tickets) {
			Long key = duplicateTicketMap.get(ticket.getId()); 

			if (key == null) {
				scannedTickets.put(key, ticket);
				resultTickets.add(ticket);	
				continue;
			} else {
				Ticket duplicateTicket = scannedTickets.get(key);
				if (duplicateTicket != null && (keyToSiteMap.get(key) == null || !keyToSiteMap.get(key).contains(ticket.getSiteId()))) {
					// in case of duplicate, keep preferentially:
					// 1) cheapest ticket
					// 2) best merchant
					
					if (isBetterTicket(ticket, duplicateTicket)) {
						// removed the duplicate ticket from the list
						resultTickets.remove(duplicateTicket);
						scannedTickets.put(key, ticket);
						resultTickets.add(ticket);
						if(keyToSiteMap.get(key) == null) {
							keyToSiteMap.put(key, new ArrayList<String>());
						}
						keyToSiteMap.get(key).add(duplicateTicket.getSiteId());
					}
					// else we skip the current ticket
					if(keyToSiteMap.get(key) == null) {
						keyToSiteMap.put(key, new ArrayList<String>());
					}
					keyToSiteMap.get(key).add(ticket.getSiteId());
				} else {
                    			scannedTickets.put(key, ticket);
                    			resultTickets.add(ticket);
				}
			}
		}
		return resultTickets;
	}

	/**
	 * Remove duplicate tickets.
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 */

	public static List<Ticket> removeDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy removeDuplicatePolicy) {
		
		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			return new ArrayList<Ticket>(tickets);
		}	
		
		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.SMART)) {
			return smartRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);
		}

		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.SUPER_SMART)) {
			return superSmartRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		}

		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.MANUAL)) {
			return manualRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.SMART);
		}
		
		if (tickets == null) {
			return new ArrayList<Ticket>();
		}
		
		if (tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}		

		List<Ticket> resultTickets = new ArrayList<Ticket>();

		Map<String, ArrayList<Ticket>> scannedTickets = new HashMap<String, ArrayList<Ticket>>();
		for (Ticket ticket: tickets) {
			String key = getTicketDuplicateKey(ticket, removeDuplicatePolicy); 

			ArrayList<Ticket> duplicateTickets = scannedTickets.get(key);
			if (duplicateTickets != null) {
				// in case of duplicate, keep preferentially:
				// 1) cheapest ticket
				// 2) best merchant
				boolean foundDup = false;
				for(Ticket duplicateTicket : duplicateTickets){
					if((duplicateTicket.getSeat() == null)
							|| (ticket.getSeat() == null)
							|| duplicateTicket.getSeat().isEmpty()
							|| ticket.getSeat().isEmpty()
							|| duplicateTicket.getSeat().equals("*-*")
							|| ticket.getSeat().equals("*-*")
							|| duplicateTicket.getSeat().equals(ticket.getSeat())) {
						
						foundDup = true;
						if (isBetterTicket(ticket, duplicateTicket)) {
							// removed the duplicate ticket from the list
							resultTickets.remove(duplicateTicket);
							scannedTickets.get(key).remove(duplicateTicket);
							scannedTickets.get(key).add(ticket);
							resultTickets.add(ticket);
							break;
						}
					}
				}
				if(!foundDup) {
						scannedTickets.get(key).add(ticket);
						resultTickets.add(ticket);
				}
				// else we skip the current ticket
			} else {
				ArrayList<Ticket> ticketList = new ArrayList<Ticket>();
				ticketList.add(ticket);
				scannedTickets.put(key, ticketList);
				resultTickets.add(ticket);
			}
		}
		return resultTickets;
	}
	
	private static Collection<Ticket> filterTicketsBySite(String siteId, Collection<Ticket> tickets) {
		Collection<Ticket> result = new ArrayList<Ticket>();
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(siteId)) {
				result.add(ticket);
			}
		}
		
		return result;
	}
	
	private static Ticket getTicketWithClosestPrice(Ticket srcTicket, Collection<Ticket> tickets) {
		if (tickets == null || tickets.isEmpty()) {
			return null;
		}
		
		Ticket closestTicket = null;
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(srcTicket.getSiteId())) {
				continue;
			}
			
			Double price = srcTicket.getCurrentPrice();
			if (closestTicket == null || Math.abs(ticket.getCurrentPrice() - price) < Math.abs(closestTicket.getCurrentPrice() - price)) {
				closestTicket = ticket;
			}
		}
		
		return closestTicket;
	}
	
	private static String[] getStartEndSeatFromSeat(String seat) {
		if (seat == null || seat.trim().isEmpty()) {
			return null;
		}

		Matcher matcher = singleSeatPattern.matcher(seat);
		if (matcher.find()) {
			return new String[]{matcher.group(1), matcher.group(1)};
		} else {
			matcher = seatRangePattern.matcher(seat);
			if (matcher.find()) {
				return new String[]{matcher.group(1), matcher.group(2)};
			}
		}
		return null;
	}
	
	public static boolean areCompatibleSeats(String seat1, String seat2) {
		if (seat1 == null || seat1.trim().isEmpty()) {
			return true;
		}
		
		if (seat2 == null || seat1.trim().isEmpty()) {
			return true;
		}
		
		String[] tok1 = getStartEndSeatFromSeat(seat1);
		String[] tok2 = getStartEndSeatFromSeat(seat2);

		if (tok1[0].equals(tok2[0]) && tok2[0].equals(tok2[0])) {
			return true;
		}

		return false;
	}
	
	public static Ticket getMostLikelySameTicket(Ticket srcTicket, Collection<Ticket> tickets) {
		Ticket candidate = null;
		Double difference = 0D;
		boolean isAdm1 = srcTicket.isAdmitOneTicket();
		
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(srcTicket.getSiteId())) {
				continue;
			}
			
			if (!areCompatibleSeats(ticket.getSeat(), srcTicket.getSeat())) {
				continue;
			}
		
			if (candidate == null
				|| Math.abs((ticket.getAdjustedCurrentPrice() - srcTicket.getAdjustedCurrentPrice())) < difference) {
				boolean isAdm2 = ticket.isAdmitOneTicket();
				
				if (isAdm1 && !isAdm2 && ticket.getAdjustedCurrentPrice() > srcTicket.getAdjustedCurrentPrice()) {
					continue;
				}

				if (!isAdm1 && isAdm2 && ticket.getAdjustedCurrentPrice() < srcTicket.getAdjustedCurrentPrice()) {
					continue;
				}

				candidate = ticket;
				difference = Math.abs(ticket.getAdjustedCurrentPrice() - srcTicket.getAdjustedCurrentPrice());
			}
		}
		
		return candidate;
	}
	
	public static Ticket getTicketWithSameSeat(Ticket srcTicket, Collection<Ticket> tickets) {
		String[] seatTokens = getStartEndSeatFromSeat(srcTicket.getSeat());
		
		if (seatTokens == null) {
			return null;
		}
		
		String srcStartSeat = seatTokens[0];
		String srcEndSeat = seatTokens[1];
		
		for (Ticket ticket: tickets) {
			seatTokens = getStartEndSeatFromSeat(ticket.getSeat());
			
			if (seatTokens == null) {
				continue;
			}
			
			String startSeat = seatTokens[0];
			String endSeat = seatTokens[1];
			
			if (srcStartSeat.equals(startSeat) && srcEndSeat.equals(endSeat)) {
				return ticket;
			}
		}
		
		return null;
	}

	public static List<Ticket> smartRemoveDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy removeDuplicatePolicy) {
		Map<String, Collection<Ticket>> ticketMap = new HashMap<String, Collection<Ticket>>();
				
		for (String siteId: reversedRankedSiteIds) {
			for(Ticket ticket: filterTicketsBySite(siteId, tickets)) {
				String key = getTicketDuplicateKey(ticket, removeDuplicatePolicy);
				Collection<Ticket> ticketList = ticketMap.get(key);
				if (ticketList == null) {
					ticketList = new ArrayList<Ticket>();
					ticketMap.put(key, ticketList);
					ticketList.add(ticket);
				} else {
					Ticket sameTicket = getTicketWithSameSeat(ticket, ticketList);
					if (sameTicket != null) {
						if (isBetterTicket(ticket, sameTicket)) {
							ticketList.remove(sameTicket);
							ticketList.add(ticket);
						}
						continue;
					}
					
					Ticket closestTicket = getTicketWithClosestPrice(ticket, ticketList);
					if (closestTicket == null) {
						ticketList.add(ticket);
					} else if (ticket.getCurrentPrice() > closestTicket.getCurrentPrice()) {
						if (closestTicket.getCurrentPrice() / ticket.getCurrentPrice() < SIMILARITY_PRICE_THRESHOLD_PERCENT) {
							ticketList.add(ticket);							
						}
					} else {
						if (ticket.getCurrentPrice() / closestTicket.getCurrentPrice() > SIMILARITY_PRICE_THRESHOLD_PERCENT) {
							ticketList.remove(closestTicket);
							ticketList.add(ticket);	
						} else{
							ticketList.add(ticket);		
						}
					}
				}
			}
		}

		List<Ticket> resultTickets = new ArrayList<Ticket>();
		for (Collection<Ticket> list: ticketMap.values()) {
			resultTickets.addAll(list);
		}
		return resultTickets;
	}
	public static double getCurrencyConversion(String siteId) {
		// note: removed SEATWAVE for this because the seatwave API
		// gives prices in USD now
		if (siteId.equals(Site.GET_ME_IN)) {
			try {
				return gbpFetcher.getCurrency();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 1.0;
	}

	//FIXME: dirty code for now
	public static double getGBPConversionRate() {
		try {
			return gbpFetcher.getCurrency();
		} catch (Exception e) {
			e.printStackTrace();
			return 1.60;
		}		
	}

	//FIXME: dirty code for now
	public static double getEURConversionRate() {
		try {
			return eurFetcher.getCurrency();
		} catch (Exception e) {
			e.printStackTrace();
			return 1.45;
		}		
	}

	
	/**
	 * Compute adjusted price using the price adjustment for the event and the merchant site and
	 * the rounding policy for the merchant site.
	 * @param percentAdjustment
	 * @param priceRounding
	 * @param price
	 * @param siteId
	 * @return
	 */
	public static double computeAdjustedPrice(BaseTicket ticket) {
		Double percentAdjustment = percentAdjustmentMap.get(getAdjustKey(ticket));
		if(percentAdjustment == null){
			EventPriceAdjustment priceAdjustment = DAORegistry.getEventPriceAdjustmentDAO().get(ticket.getEventId(), ticket.getSiteId());
			if (priceAdjustment != null) {
				percentAdjustment = priceAdjustment.getPercentAdjustment();
				percentAdjustmentMap.put(getAdjustKey(ticket), percentAdjustment);
			}
		}
		PriceRounding priceRounding = priceRoundingMap.get(ticket.getSiteId());
		if (priceRounding == null) {
			Site site = DAORegistry.getSiteDAO().get(ticket.getSiteId());
			if (site != null) {
				priceRounding = site.getPriceRounding();
				priceRoundingMap.put(ticket.getSiteId(), priceRounding);
			}		
		}
		return computeAdjustedPrice(percentAdjustment, priceRounding, 
				ticket.getCurrentPrice(), ticket.getSiteId());
	}
	/**
	 * Compute adjusted price using the price adjustment for the event and the merchant site and
	 * the rounding policy for the merchant site.
	 * @param percentAdjustment
	 * @param priceRounding
	 * @param price
	 * @param siteId
	 * @return
	 */
	public static double computeAdjustedPrice(Double percentAdjustment,
			PriceRounding priceRounding, double price, String siteId) {
		double adjustedPrice = price;
		if (percentAdjustment != null) {
			adjustedPrice = adjustedPrice * (1.0 + percentAdjustment / 100.0);
		}
		
		// note: removed SEATWAVE for this because the seatwave API
		// gives prices in USD now
		
		if(siteId.equals(Site.GET_ME_IN)){
			
			Double gbpConversion = null;
			try {
				gbpConversion = gbpFetcher.getCurrency();
			} catch (FeedException feed) {
				System.out.println("Caught FeedException");
				feed.printStackTrace();
			} catch (FetcherException fete) {
				System.out.println("Caught FetcherException");
				fete.printStackTrace();
			} catch (Exception e) {
				System.out.println("Caught Exception getting curremcy fetcher: " + e.getMessage());
				e.printStackTrace();
			}
			if(gbpConversion != null) {
				adjustedPrice = adjustedPrice * gbpConversion;
			}
		}
		
		if (priceRounding == null || priceRounding.equals(PriceRounding.NONE)) {
			// no change
		} else if (priceRounding.equals(PriceRounding.UP_TO_NEAREST_DOLLAR)) {
			adjustedPrice = Math.ceil(adjustedPrice);
		} else if (priceRounding.equals(PriceRounding.DOWN_TO_NEAREST_DOLLAR)) {
			adjustedPrice = Math.floor(adjustedPrice);
		}else if (priceRounding.equals(PriceRounding.ROUND_TO_NEAREST_DOLLAR)) {			
			if((adjustedPrice-Math.floor(adjustedPrice))<0.50){
				adjustedPrice = Math.floor(adjustedPrice);
			}else{
				adjustedPrice = Math.ceil(adjustedPrice);	
			}
		}

		return adjustedPrice;
	}

	/**
	 * Remove tickets belonging to A1 from the tickets list given as parameter.
	 * @param tickets 
	 */
	public static List<Ticket> removeAdmitOneTickets(Collection<Ticket> tickets) {
		if (tickets == null) {
			return null;
		}
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		for(Ticket ticket : tickets){
			if (!ticket.isAdmitOneTicket() || !ticket.isAdmitOneNonTMTicket()) {
				resultTickets.add(ticket);
			}
		}
		
		return resultTickets;
	}

	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	public static List<Ticket> filterCategory(Integer catId, Collection<Ticket> tickets) {
		if (tickets == null) {
			return null;
		}
		if (tickets.isEmpty()) {
			return null;
		}
		VenueCategory venueCategory = tickets.iterator().next().getEvent().getVenueCategory();
		
		Category cat = DAORegistry.getCategoryDAO().get(catId);
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		catMap.put(cat.getId(), cat);
		List<CategoryMapping> list = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByCategoryId(catId);
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer,List<CategoryMapping>>();
		catMappingMap.put(catId, list);
		for(Ticket ticket : tickets){
			Integer ticketCat = ticket.getCategoryId(venueCategory,cat.getGroupName(),catMap,catMappingMap); 
			if (ticketCat != null) {
				if(ticketCat.intValue() == catId.intValue()){
					resultTickets.add(ticket);
				}
			}
		}
		
		return resultTickets;
	}

	public static String getCatScheme(List<Integer> catIds) {
		for (Integer catId: catIds) {
			if (catId > 0) { 
				return DAORegistry.getCategoryDAO().get(catId).getGroupName();
			}
		}
		return null;
	}
	
	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	public static List<Ticket> filterCategories(List<Integer> catIds, Collection<Ticket> tickets) {
		if (tickets == null) {
			return null;
		}
		
		if (tickets.isEmpty()) {
			return null;
		}
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
//		String catScheme = getCatScheme(catIds);
		for(Ticket ticket : tickets){
			Category category =ticket.getCategory();
			Integer ticketCat = category==null?null:category.getId();
//			Integer ticketCat = ticket.getCategoryId(catScheme); 
			if (ticketCat == null) {
				ticketCat = Category.UNCATEGORIZED_ID;
			} // so if we have in the filter UNCAT, we'll consider both uncat and cat

			if(catIds.contains(ticketCat)){
				resultTickets.add(ticket);
			}
		}
		
		return resultTickets;
	}

	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	public static List<Ticket> filterUncategorized(Collection<Ticket> tickets, String groupName) {
		if (tickets == null) {
			return null;
		}
		if (tickets.isEmpty()) {
			return null;
		}
		VenueCategory venueCategory = tickets.iterator().next().getEvent().getVenueCategory();
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		Collection<Category> catList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Category cat:catList){
			catMap.put(cat.getId(), cat);
		}
		List<CategoryMapping> CategoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer,List<CategoryMapping>>();
		for(CategoryMapping cm:CategoryMappingList){
			List<CategoryMapping> list = catMappingMap.get(cm.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(cm);
			catMappingMap.put(cm.getCategoryId(), list);
		}
		
		for(Ticket ticket : tickets){
			Integer ticketCat = ticket.getCategoryId(venueCategory,groupName,catMap,catMappingMap); 
			if (ticketCat == null) {
				resultTickets.add(ticket);
			}
		}
		
		return resultTickets;
	}

	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	public static List<Ticket> filterCategorized(Collection<Ticket> tickets, String groupName) {
		if (tickets == null) {
			return null;
		}
		if (tickets.isEmpty()) {
			return null;
		}
		VenueCategory venueCategory = tickets.iterator().next().getEvent().getVenueCategory();	
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		Collection<Category> catList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Category cat:catList){
			catMap.put(cat.getId(), cat);
		}
		List<CategoryMapping> CategoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer,List<CategoryMapping>>();
		for(CategoryMapping cm:CategoryMappingList){
			List<CategoryMapping> list = catMappingMap.get(cm.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(cm);
			catMappingMap.put(cm.getCategoryId(), list);
		}
		for(Ticket ticket : tickets){
			Integer ticketCat = ticket.getCategoryId(venueCategory,groupName,catMap,catMappingMap); 
			if (ticketCat != null) {
				resultTickets.add(ticket);
			}
		}
		
		return resultTickets;
	}

	/**
	 * Returns true if tickets contains an AdmitOne ticket.
	 * @param tickets 
	 */
	public static boolean containsAdmitOneTickets(Collection<Ticket> tickets) {
		if (tickets == null) {
			return false;
		}
		if (tickets.isEmpty()) {
			return false;
		}

		for(Ticket ticket : tickets){
			if (ticket.isAdmitOneTicket() || ticket.isAdmitOneNonTMTicket()) {
				return true;
			}
		}
		
		return false;
	}

	/**
	 * Returns true if tickets contains an AdmitOne ticket.
	 * @param tickets 
	 */
	public static boolean containsAdmitOneWebTickets(Collection<WebTicketRow> tickets) {
		if (tickets == null || tickets.isEmpty()) {
			return false;
		}

		for(Ticket ticket : tickets){
			if (ticket.isAdmitOneTicket()||ticket.isAdmitOneNonTMTicket()) {
				return true;
			}
		}
		
		return false;
	}

	private static String stripCharactersFromString(String str){
		if (str == null) {
	        return null;
	    }

	    StringBuffer strBuff = new StringBuffer();
	    char c;
	    
	    for (int i = 0; i < str.length() ; i++) {
	        c = str.charAt(i);
	        
	        if (Character.isDigit(c)) {
	            strBuff.append(c);
	        }
	    }
	    String retString = strBuff.toString();
	    if(retString == null || retString.length() <= 0){
	    	return str;
	    }
	    return retString;
	    
	}
	/**
	 * inSectionRange
	 * 
	 * Return true if the ticket is in the section range.
	 * @param ticket
	 * @param startSection
	 * @param endSection
	 * @return
	 */
	public static boolean inSectionRange(Ticket ticket, String startSection, String endSection) {
		/*
		 * Strip out the characters from the sections. (Both the user supplied and the ticket sections)
		 * This is done because the compare function of the categorizer only compares strings of same length. (We may need to fix that)
		 */
		
		String fullyNormalizedSection = stripCharactersFromString(ticket.getNormalizedSection());
		String normalizedStartSection = stripCharactersFromString(startSection);
		String normalizedEndSection = stripCharactersFromString(endSection);
		
		Integer cmp1 = Categorizer.compare(normalizedStartSection, fullyNormalizedSection);
		Integer cmp2 = Categorizer.compare(fullyNormalizedSection, normalizedEndSection);
		System.out.println("Ticket Info: section" + ticket.getSection() + " row :" + ticket.getRow() + " qty: " + ticket.getQuantity());
		System.out.println("cmp 1:" + cmp1 + "cmp 2:" + cmp2);
		if (cmp1 == null || cmp2 == null) {
				return false;
		}
		
		if (cmp1 <= 0 && cmp2 <= 0) {
			return true;
		}
		return false;
	}

	/**
	 * Check if a ticket belongs to a section or a section range (100-120)
	 * @param ticket
	 * @param sections
	 * @return
	 */
	public static boolean isInSections(Ticket ticket, String sections) {
		if(sections == null || sections.isEmpty()){
			return true;
		}
		
		if(ticket == null) {
			return false;
		}
		String[] sectionAndRange = sections.split(",");
		for(String section:sectionAndRange){
			if(section.contains("-")){
				String[] sectionRange = section.split("-");
				if(inSectionRange(ticket, sectionRange[0], sectionRange[1])) {
					return true;
				}
			} else {
				//if(ticket.getNormalizedSection().compareToIgnoreCase(section)==0) {
				if(ticket.getNormalizedSection().toLowerCase().contains(section.toLowerCase())==true) {
					
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * inRowRange
	 * 
	 * Return true if the ticket is in the row range.
	 * @param ticket
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	public static boolean inRowRange(Ticket ticket, String startRow, String endRow) {
		Integer cmp1 = Categorizer.compare(startRow, ticket.getNormalizedRow());
		Integer cmp2 = Categorizer.compare(ticket.getNormalizedRow(), endRow);
		if (cmp1 == null || cmp2 == null) {
				return false;
		}
		
		if (cmp1 <= 0 && cmp2 <= 0) {
			return true;
		}
		return false;
	}	
	
	/**
	 * Check if a ticket belongs to a row or a row range
	 * @param ticket
	 * @param rows
	 * @return
	 */
	public static boolean isInRows(Ticket ticket, String rows) {
		if(rows == null || rows.isEmpty()){
			return true;
		}
		
		if(ticket == null) {
			return false;
		}
		String[] rowAndRange = rows.split(",");
		for(String row:rowAndRange){
			if(row.contains("-")){
				String[] rowRange = row.split("-");
				if(inRowRange(ticket, rowRange[0], rowRange[1])) {
					return true;
				}
			} else {
				//if(ticket.getNormalizedRow().compareToIgnoreCase(row)==0) {
				if(ticket.getNormalizedRow().toLowerCase().startsWith(row.toLowerCase())==true) {
					return true;
				}
			}
		}
		return false;
	}
	

	/**
	 * Check if a ticket is is within a price range
	 * if wholesale is true, then we compare the wholesale price, otherwise we compare the online price
	 * @param ticket
	 * @param startPrice
	 * @param endPrice
	 * @param wholesale
	 * @return
	 */
	
	public static boolean isInPriceRange(Ticket ticket, Double startPrice, Double endPrice, String priceType) {
		if(startPrice == null && endPrice == null){
			return true;
		}
		if(priceType == null){
			return true;
		}
		if(priceType.equalsIgnoreCase("wholesale")){
			if(startPrice == null || endPrice == null) {
				if ((startPrice == null && endPrice.equals(ticket.getAdjustedCurrentPrice()))
					|| (endPrice == null && startPrice.equals(ticket.getAdjustedCurrentPrice()))) {
					return true;
				}
			} else {
				if((startPrice <= ticket.getAdjustedCurrentPrice() && endPrice >= ticket.getAdjustedCurrentPrice())
						|| (startPrice >= ticket.getAdjustedCurrentPrice() && endPrice <= ticket.getAdjustedCurrentPrice())) {
					return true;
				}
			}
		} else if(priceType.equalsIgnoreCase("online")) {
			if(startPrice == null || endPrice == null) {
				if ((startPrice == null && endPrice.equals(ticket.getBuyItNowPrice()))
					|| (endPrice == null && startPrice.equals(ticket.getBuyItNowPrice()))) {
					return true;
				}
			} else {
				if((startPrice <= ticket.getBuyItNowPrice() && endPrice >= ticket.getBuyItNowPrice())
						|| (startPrice >= ticket.getBuyItNowPrice() && endPrice <= ticket.getBuyItNowPrice())) {
					return true;
				}
			}
		}else if(priceType.equalsIgnoreCase("purchase")){
			
			if(ticket.getPurchasePrice() <= 0){
				return true;
			}else if(startPrice == null || endPrice == null) {
				if ((startPrice == null && endPrice.equals(ticket.getPurchasePrice()))
					|| (endPrice == null && startPrice.equals(ticket.getPurchasePrice()))) {
					return true;
				}
			} else {
				if((startPrice <= ticket.getPurchasePrice() && endPrice >= ticket.getPurchasePrice())
						|| (startPrice >= ticket.getPurchasePrice() && endPrice <= ticket.getPurchasePrice())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Return true if the ticket note contains an admitone code.
	 * @param ticketTitle
	 * @return
	 */
    private static boolean matchesCode(String ticketTitle) {
        String notePatterns = Constants.getInstance().getAdmit1NotePattern();
        if(notePatterns.contains(",")) {
	        String[] noteTokens = notePatterns.split(",");
	        for(int i = 0; i < noteTokens.length ; i++){
	                String notePattern = noteTokens[i];
	                if(ticketTitle.matches(".*" + notePattern + ".*")){
	                        return true;
	                }
	        }
	        return false;
        } else {
                return ticketTitle.matches(".*" + notePatterns + ".*");
        }
    }
    
	public static Collection<Ticket> getEventCategoriesLotsSimpleTicket(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName,VenueCategory venueCategory) {
		if(tickets == null) {
			return null;
		}

		Collection<Integer> excludedTicketIds = new HashSet<Integer>();
		if(usedTicketIds != null){
			excludedTicketIds.addAll(usedTicketIds);
		}

		tickets = TicketUtil.removeAdmitOneTickets(tickets);
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		
		Collection<Category> catList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Category cat:catList){
			catMap.put(cat.getId(), cat);
		}
		List<CategoryMapping> CategoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer,List<CategoryMapping>>();
		for(CategoryMapping cm:CategoryMappingList){
			List<CategoryMapping> list = catMappingMap.get(cm.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(cm);
			catMappingMap.put(cm.getCategoryId(), list);
		}
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		for(Ticket ticket:tickets){
			
			Integer categoryId = ticket.getCategoryId(venueCategory, groupName,catMap,catMappingMap);
			if(categoryId == null){
				categoryId = 0;
			}	
	
			// quantity is equals or if more then if sold we have at least a pair
			if (!excludedTicketIds.contains(ticket.getId()) && categoryIds.contains(categoryId)
					&& (ticket.getRemainingQuantity().equals(quantity) || ticket.getRemainingQuantity().compareTo(quantity + 2) >= 0)
//							lotSizes == null 
//							|| lotSizes.isEmpty()
//							|| (lotSizes.contains(ticket.getLotSize()) || lotSizes.contains(ticket.getRemainingQuantity()))
//						)
				) {
				resultTickets.add(ticket);
			} 
		}
				
		if (resultTickets == null || resultTickets.isEmpty()) {
			return null;
		}
		
		try {
			    resultTickets.sort(new Comparator<Ticket>() {
			    @Override
				public int compare(Ticket ticket1, Ticket ticket2) {
				return ticket1.getBuyItNowPrice().compareTo(ticket2.getBuyItNowPrice());
			}
		});
		}catch(Exception ex){			
			System.out.println("Exception sorting TicketUtils line no 1325 " + ex);			
		}
		
		return resultTickets;
    }
	
	public static Collection<Ticket> getEventCategoriesLotsSimpleTicket1(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName,VenueCategory venueCategory) {
		if(tickets == null) {
			return null;
		}

		Collection<Integer> excludedTicketIds = new HashSet<Integer>();
		if(usedTicketIds != null){
			excludedTicketIds.addAll(usedTicketIds);
		}

		tickets = TicketUtil.removeAdmitOneTickets(tickets);
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		
		Collection<Category> catList = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Category cat:catList){
			catMap.put(cat.getId(), cat);
		}
		List<CategoryMapping> CategoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer,List<CategoryMapping>>();
		for(CategoryMapping cm:CategoryMappingList){
			List<CategoryMapping> list = catMappingMap.get(cm.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(cm);
			catMappingMap.put(cm.getCategoryId(), list);
		}
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		for(Ticket ticket:tickets){
			
			Integer categoryId = ticket.getCategoryId(venueCategory,groupName,catMap,catMappingMap);
			if(categoryId == null){
				categoryId = 0;
			}	
	
			// In this we dont worry about the quantity. We get all the tickets and sort it 
			if (!excludedTicketIds.contains(ticket.getId()) && categoryIds.contains(categoryId)){
				resultTickets.add(ticket);
			} 
		}
				
		if (resultTickets == null || resultTickets.isEmpty()) {
			return null;
		}
		
		try{
		resultTickets.sort( new Comparator<Ticket>() {
			
			@Override
			public int compare(Ticket ticket1, Ticket ticket2) {
				if(ticket2.getRemainingQuantity().compareTo(ticket1.getRemainingQuantity()) > 0){
					return 1;
				}else if(ticket2.getRemainingQuantity().compareTo(ticket1.getRemainingQuantity()) == 0){
					return ticket1.getBuyItNowPrice().compareTo(ticket2.getBuyItNowPrice());					
				}else{
					return -1;
				}
				//return ticket2.getRemainingQuantity().compareTo(ticket1.getRemainingQuantity());
			}
		});
		}catch(Exception ex){
			 System.out.println("Exception in Sorting TicketUtil line no 1393 " + ex);
		}
		
		return resultTickets;
    }	

	//assumes tickets are ordered by buyItNowPrice ASC
	public static SimpleTicket getLowestEventCategoriesLotsSimpleTicket(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName,VenueCategory venueCategory) {
//		System.out.println("CIRCLES: tix=" + tickets);
//		System.out.println("CIRCLES: catId=" + categoryIds);
//		System.out.println("CIRCLES: qty=" + quantity);
//		System.out.println("CIRCLES: group=" + groupName);
		Collection<Ticket> resultTickets = getEventCategoriesLotsSimpleTicket(tickets, categoryIds, quantity, usedTicketIds, groupName,venueCategory);
		if (resultTickets == null) {
			return null;
		}
		//Because the tickets are ordered, the first ticket is the lowest priced ticket
		
		/* This is for Purchase price calculation - Begins */
		List<Ticket> newTicketList = new ArrayList<Ticket>();
		newTicketList.addAll(resultTickets);
		Map<String, ManagePurchasePrice> purchasePriceMap = new HashMap<String, ManagePurchasePrice>();
		List<ManagePurchasePrice> managePurchasePrices = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(newTicketList.get(0).getEvent().getArtistId());
		for (ManagePurchasePrice managePurchasePrice : managePurchasePrices) {
			purchasePriceMap.put(managePurchasePrice.getExchange()+"-"+managePurchasePrice.getTicketType(), managePurchasePrice);
		}
		/* This is for Purchase price calculation - Ends */
		
		return new SimpleTicket(resultTickets.iterator().next(), groupName,purchasePriceMap);
	}

	public static SimpleTicket getLowestEventCategoriesLotsSimpleTicket1(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName,VenueCategory venueCategory) {
//		System.out.println("CIRCLES: tix=" + tickets);
//		System.out.println("CIRCLES: catId=" + categoryIds);
//		System.out.println("CIRCLES: qty=" + quantity);
//		System.out.println("CIRCLES: group=" + groupName);
		Collection<Ticket> resultTickets = getEventCategoriesLotsSimpleTicket1(tickets, categoryIds, quantity, usedTicketIds, groupName,venueCategory);
		if (resultTickets == null) {
			return null;
		}
		/* This is for Purchase price calculation - Begins */
		List<Ticket> newTicketList = new ArrayList<Ticket>();
		newTicketList.addAll(resultTickets);
		Map<String, ManagePurchasePrice> purchasePriceMap = new HashMap<String, ManagePurchasePrice>();
		List<ManagePurchasePrice> managePurchasePrices = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByArtistId(newTicketList.get(0).getEvent().getArtistId());
		for (ManagePurchasePrice managePurchasePrice : managePurchasePrices) {
			purchasePriceMap.put(managePurchasePrice.getExchange()+"-"+managePurchasePrice.getTicketType(), managePurchasePrice);
		}
		/* This is for Purchase price calculation - Ends */
		//Because the tickets are ordered, the first ticket is the lowest priced ticket
		return new SimpleTicket(resultTickets.iterator().next(), groupName,purchasePriceMap);
	}
	
	/**
	 * Assign to a set of tickets their categories
	 * @param tickets
	 * @param categories
	 * @param scheme
	 */
	public static void preAssignCategoriesToTickets(Collection<Ticket> tickets, Collection<Category> categories) {
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		
		if(categories != null && !categories.isEmpty()){
			for (Category category: categories) {
				categoryMap.put(category.getId(), category);
			}
		}
	//		for (Ticket ticket: tickets) {
				tickets = Categorizer.computeCategoryForTickets(tickets, categoryMap);
	//		}
	
	}
	
	public static Collection<Ticket> preAssignCategoriesToTickets(Collection<Ticket> tickets, Collection<Category> categories,Event event) throws Exception {
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		
		if(categories != null && !categories.isEmpty()){
			for (Category category: categories) {
				categoryMap.put(category.getId(), category);
			}
		}
	//		for (Ticket ticket: tickets) {
				tickets = Categorizer.computeCategoryForTickets(tickets, categoryMap,event);
	//		}
				return tickets;
	}
	
	/**
	 * Duplicate key based on normSection, normRow, qty, wholesale 
	 * @param ticket
	 * @param removeDuplicatePolicy
	 * @return
	 */

	public static String getExactTicketDuplicateKey(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow());
		}

		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + ","
				+ row + "," + normalizeSeat(ticket.getSeat()) + "," + ticket.getCurrentPrice();

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
	/**
	 * 
	 */
	
	public static String getTicketDuplicateKeyBySectionRowAndQuantity(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection()).trim();
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow()).trim();
		}

		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + "," + row ;

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
	
	/***
	 * 
	 */
	public static String getTicketDuplicateKeyBySectionRowSeatAndQuantity(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection()).trim();
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow()).trim();
		}

		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + "," + row + "," + normalizeSeat(ticket.getSeat()) ;

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
	/**
	 * remove tickets based on Matt's logic that gives priority to exchanges which are more likely
	 * to have duplicates
	 * @param ticket
	 * @param siteIds
	 * @param toKeepSiteId
	 * @param ticketMap
	 * @return
	 */
	private static boolean removeMostLikelySameTickets(Ticket ticket, String[] siteIds, String toKeepSiteId, Map<String, Map<String, Collection<Ticket>>> ticketMap) {
		Collection<String> includedSites = new ArrayList<String>();
		for (String siteId: siteIds) {
			includedSites.add(siteId);
		}

		Ticket ticketToRemove = null;
		Ticket ticketToAdd = null;
		
		// check that tickets within price tolerance are in the specified sites
		// BUT NOT in the other sites
		// in the other sites have to find a ticket that may potentially conflict
		
//		boolean hasConflict = false;
		
		for (String siteId: rankedSiteIds) {  // Same site can not be duplicate
			if (siteId.equals(ticket.getSiteId())) {
				continue;
			}
			
			boolean isIncludedSite = includedSites.contains(siteId);
			
			if (ticketMap.get(siteId) == null) {
				if (isIncludedSite) { // not in the specified site => do nothing
					return false;	
				}
				continue;
			}
				
			String key = getTicketDuplicateKey(ticket, RemoveDuplicatePolicy.NONE);
			Collection<Ticket> tickets = ticketMap.get(siteId).get(key);
			if (tickets == null) {
				if (isIncludedSite) { // no tickets for a specified site => leave
					return false;
				} 
				continue;
			}
		
			Object[] closestTicketObj = getClosestTicket(ticket, tickets, false);
			if (closestTicketObj == null) {
				if (isIncludedSite) {
					return false;
				} 
				continue;
			}
			Double delta = (Double)closestTicketObj[0];
			Ticket closestTicket = (Ticket)closestTicketObj[1];
						
			if (Math.floor(delta*100) < SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT) {
				if (!isIncludedSite) {
					if (!seatMatch(ticket.getSeat(), closestTicket.getSeat())) {
//						hasConflict = true;
						continue;
					} else {
						return false;
					}
				} else if (siteId.equals(toKeepSiteId)){
					if (delta < 0) { // cheaper find => replacement
						ticketToAdd = closestTicket;
						ticketToRemove = ticket;
					} else if (delta.equals(0D)) {
						if(siteRank.get(ticket.getSiteId()) < siteRank.get(closestTicket.getSiteId())) {
							ticketToAdd = ticket;
							ticketToRemove = closestTicket;							
						} else {
							ticketToAdd = closestTicket;
							ticketToRemove = ticket;							
						}
					} else { // remove it
						ticketToAdd = ticket;
						ticketToRemove = closestTicket;
					}
				}
			} else {
				if (isIncludedSite) {
					return false;
				} else {
					continue;
				}
			}
		}
		
		// if we survived here, then tickets were found on the included sites but not on the others
		
		// replacement and removal
//		String key = getTicketDuplicateKey(ticket, RemoveDuplicatePolicy.NONE);
		
		// removal
		/*
		Collection<Ticket> tickets = ticketMap.get(ticketToRemove.getSiteId()).get(key);
		tickets.remove(ticketToRemove);
		*/
//		ticketToRemove.setId(null);
		ticketToRemove.setDupRefTicketId(ticketToAdd.getId());
//		System.out.println("==XXXX========> FRANCOIS removing " + ticketToRemove);
		//addition
		/*
		if (ticketToAdd != null) {
			tickets = ticketMap.get(ticketToAdd.getSiteId()).get(key);
			tickets.add(ticketToAdd);			
			System.out.println("==XXXX=======> FRANCOIS adding " + ticketToAdd);
		}
		*/
		
		return true;
	}

	private static boolean removeMostLikelySameTickets(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
		boolean result = false;
		for (String siteId: rankedSiteIds) {
			if (ticketMap.get(siteId) == null) {
				continue;
			}
			
			for (Collection<Ticket> tickets: new ArrayList<Collection<Ticket>>(ticketMap.get(siteId).values())) {
				for (Ticket ticket: new ArrayList<Ticket>(tickets)) {
					if (ticket.getDupRefTicketId() != null) { // skip dup ticket
						continue;
					} 
					if (!isSeatNull(ticket.getSeat())) {
						result |= removeMostLikelySameTickets(ticket, ticketMap);
					}
				}
			}
		}
		return result;
	}

	private static boolean removeMostLikelySameTickets(Ticket ticket, Map<String, Map<String, Collection<Ticket>>> ticketMap) {
		String siteId = ticket.getSiteId();
		
		if (siteId.equals("eimarketplace")
			|| siteId.equals("eibox")
			|| siteId.equals("eventinventory")
			|| siteId.equals("admitoneeventinventory")
			|| siteId.equals("ticketsnow") || siteId.equals("ticketevolution")) {
			return removeMostLikelySameTickets(ticket, new String[]{"stubhub", "ticketnetwork"}, "stubhub", ticketMap);
		} 

		if (siteId.equals("ticketnetwork") 
			|| siteId.equals("ticketnetworkdirect")) {
				return removeMostLikelySameTickets(ticket, new String[]{"stubhub", "eimarketplace","admitoneeventinventory","ticketsnow"}, "admitoneeventinventory",  ticketMap);
		}

		if (siteId.equals("ticketsolutions")) {
			return removeMostLikelySameTickets(ticket, new String[]{"ticketnetwork", "eimarketplace","admitoneeventinventory"}, "admitoneeventinventory",  ticketMap)
				|| removeMostLikelySameTickets(ticket, new String[]{"stubhub", "eimarketplace","admitoneeventinventory"}, "admitoneeventinventory",  ticketMap);
		}
	
		if (siteId.equals("seatwave")) {
			return removeMostLikelySameTickets(ticket, new String[]{"viagogo", "getmein"}, "viagogo",  ticketMap);
		}
		
		if (siteId.equals("viagogo")) {
			return removeMostLikelySameTickets(ticket, new String[]{"seatwave", "getmein"}, "seatwave",  ticketMap);
		}
		
		return false;
	}
	
	private static void removeSameSeatWithLowerRank(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
//		Set<String> seats = new HashSet<String>();
		Map<String, Ticket> seatMap = new HashMap<String, Ticket>();
		for (String siteId: rankedSiteIds) {
			Map<String, Collection<Ticket>> map = ticketMap.get(siteId);
			if (map == null) {
				continue;
			}
			
			for (Entry<String, Collection<Ticket>> entry: map.entrySet()) {
				Collection<Ticket> tickets = entry.getValue();
				for (Ticket ticket: tickets) {
					if (isSeatNull(ticket.getSeat())) {
						continue;
					}
					
					String seatKey = getTicketDuplicateKeyBySectionRowSeatAndQuantity(ticket);
					Ticket potentialDup = seatMap.get(seatKey); 
					if (potentialDup != null) {
						if (ticket.getCurrentPrice() >= potentialDup.getCurrentPrice()) {
							ticket.setDupRefTicketId(potentialDup.getId());
//							seatMap.put(seatKey, ticket);
						} else {
							potentialDup.setDupRefTicketId(ticket.getId());
							seatMap.put(seatKey, ticket);
						}
					} else {
						seatMap.put(seatKey, ticket);
					}
				}
			}
		}
	}
	/***/
	private static void removeDuplicateBySeactionRowPrice(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
//		Set<String> seats = new HashSet<String>();
		Map<String, List<Ticket>> seatMap = new HashMap<String, List<Ticket>>();
		List<Integer> ticketRestoreMap= new ArrayList<Integer>();
		for (String siteId: rankedSiteIds) {
			Map<String, Collection<Ticket>> map = ticketMap.get(siteId);
			if (map == null) {
				continue;
			}
			
			
			for (Entry<String, Collection<Ticket>> entry: map.entrySet()) {
				Collection<Ticket> tickets = entry.getValue();
				for (Ticket ticket: tickets) {
					if(ticket.getDupRefTicketId()!=null){
						continue;
					}
					String seatKey = getTicketDuplicateKeyBySectionRowAndQuantity(ticket);
					List<Ticket> potentialDupList = seatMap.get(seatKey); 
					if (potentialDupList != null && !potentialDupList.isEmpty()) {
						boolean flag=true;
						for(Ticket potentialDupTicket:potentialDupList){
							if(!potentialDupTicket.getSiteId().equals(ticket.getSiteId())){
								if(ticketRestoreMap.contains(potentialDupTicket.getId())){
									continue;
								}
								if(isSeatNull(potentialDupTicket.getSeat()) || isSeatNull(ticket.getSeat())){
//									System.out.println(potentialDupTicket.getCurrentPrice()+ ":" + ticket.getCurrentPrice());
//									System.out.println("Kem:" + (potentialDupTicket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT))+ ":" + ticket.getCurrentPrice());
//									System.out.println("Kem-1" + (potentialDupTicket.getCurrentPrice() + ":" + (ticket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT))));
//									System.out.println(potentialDupTicket.getCurrentPrice()<ticket.getCurrentPrice());
//									System.out.println(((potentialDupTicket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)) >= ticket.getCurrentPrice()));
									if((potentialDupTicket.getPurchasePrice().equals(ticket.getPurchasePrice())) ||
									(potentialDupTicket.getPurchasePrice()<ticket.getPurchasePrice() && 
									((potentialDupTicket.getPurchasePrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)) >= ticket.getPurchasePrice()))){
										ticket.setDupRefTicketId(potentialDupTicket.getId());
										ticketRestoreMap.add(potentialDupTicket.getId());
										flag=false;
										break;
									}else if(potentialDupTicket.getPurchasePrice()>ticket.getPurchasePrice() && 
											(potentialDupTicket.getPurchasePrice() <= (ticket.getPurchasePrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)))){
										potentialDupTicket.setDupRefTicketId(ticket.getId());
										potentialDupList.remove(potentialDupTicket);
										addInList(potentialDupList,ticket);
										flag=false;
										break;
									}
								
								}else if (potentialDupTicket.getSeat().equals(ticket.getSeat())){
									if(potentialDupTicket.getPurchasePrice()>ticket.getPurchasePrice()){
										potentialDupTicket.setDupRefTicketId(ticket.getId());
										potentialDupList.remove(potentialDupTicket);
										addInList(potentialDupList,ticket);
										flag=false;
										break;
//										seatMap.put(seatKey, ticket);
									}else{
										ticket.setDupRefTicketId(potentialDupTicket.getId());
									}
								}
							}else{
								addInList(potentialDupList,ticket);
								flag=false;
								break;
							}
						}
						if(flag){
							addInList(potentialDupList,ticket);
						}
					} else {
						potentialDupList=new ArrayList<Ticket>();
						addInList(potentialDupList,ticket);
						seatMap.put(seatKey, potentialDupList);
					}
				}
			}
			ticketRestoreMap.clear();
		}
	}
	 
	 /****/
	private static Map<String, Map<String, Collection<Ticket>>> createMapByKeyAndSite(Collection<Ticket> tickets, TicketKeyCallBack callback) {
		Map<String, Map<String, Collection<Ticket>>> map = new HashMap<String, Map<String, Collection<Ticket>>>();
		
		for (String siteId: rankedSiteIds) {
			Collection<Ticket> filteredTickets = filterTicketsBySite(siteId, tickets);
			if (filteredTickets == null || filteredTickets.isEmpty()) {
				continue;
			}
			Map<String, Collection<Ticket>> ticketMap = new HashMap<String, Collection<Ticket>>();
			map.put(siteId, ticketMap);
			Collection<Ticket> sameKeyTickets =null;
			for(Ticket ticket: filteredTickets) {
				String key = callback.compute(ticket);
				sameKeyTickets = ticketMap.get(key);
				if (sameKeyTickets == null) {
					sameKeyTickets = new ArrayList<Ticket>();
					ticketMap.put(key, sameKeyTickets);
				}
				
				sameKeyTickets.add(ticket);
			}
			if(sameKeyTickets!=null && !sameKeyTickets.isEmpty()){
				
				try {					
				
				((List<Ticket>)(sameKeyTickets)).sort( new Comparator<Ticket>() {
					@Override
					public int compare(Ticket t1, Ticket t2) {
						int cmp = t1.getAdjustedCurrentPrice().compareTo(t2.getAdjustedCurrentPrice());
						
						if (cmp != 0) {
							return cmp;
						}
						
						return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
					}
				});
			}catch(Exception ex){
				 System.out.println("Exception in Sorting TicketUtil line no 1900 " + ex);
			}
			}
		}
		return map;
	}

	public static void addInList(List<Ticket> list,Ticket ticket){
		list.add(ticket);
		try {
			list.sort( new Comparator<Ticket>() {			
			public int compare(Ticket t1, Ticket t2) {
				int cmp = t1.getAdjustedCurrentPrice().compareTo(t2.getAdjustedCurrentPrice());
				
				if (cmp != 0) {
					return cmp;
				}
				
				return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
			}
		});
		}catch(Exception ex){
			 System.out.println("Exception in Sorting TicketUtil line no 1923 " + ex);
		}
	}
	private static void markAsAdmitOneTickets(Collection<Ticket> tickets) {
		for(Ticket ticket: tickets) {
			ticket.setAdmitOneTicket(true);
			ticket.setSeller("ADMT1");
		}		
	}

	private static boolean hasAdmitOneTickets(Collection<Ticket> tickets) {
		for(Ticket ticket: tickets) {
			if (ticket.getId() != null && (ticket.isAdmitOneTicket()|| ticket.isAdmitOneNonTMTicket())) {
				return true;
			}
		}		
		return false;
	}

	private static boolean hasSeat(String seat, Collection<Ticket> tickets) {
		for(Ticket ticket: tickets) {
			if (ticket.getId() != null && seatMatch(seat, ticket.getSeat())) {
				return true;
			}
		}		
		return false;		
	}

	private static Collection<String> getSeatsFromTickets(Collection<Ticket> tickets) {
		Set<String> seats = new HashSet<String>();
		for(Ticket ticket: tickets) {
			if (ticket.getId() == null) {
				continue;
			}
			
			if (!isSeatNull(ticket.getSeat())) {
				seats.add(normalizeSeat(ticket.getSeat()));
			}
		}
		return seats;
	}
	
	private static Map<String, Collection<Ticket>> mergeTicketMap(Map<String, Map<String, Collection<Ticket>>> ticketMap, TicketMergeCallBack callback) {
		Map<String, Collection<Ticket>> result = new HashMap<String, Collection<Ticket>>();
		for (String siteId: rankedSiteIds) {
			if (ticketMap.get(siteId) == null) {
				continue;
			}
			
			for(Entry<String, Collection<Ticket>> entry: ticketMap.get(siteId).entrySet()) {
				Collection<Ticket> groupMapTickets = result.get(entry.getKey());
				
				if (groupMapTickets == null) {
					groupMapTickets = new ArrayList<Ticket>();
					result.put(entry.getKey(), groupMapTickets);
				}
				
				callback.merge(groupMapTickets, entry.getValue());
			}
		}
		return result;
	}

	public static List<Ticket> scrubExplicitlyMarkedDuplicateTickets(Collection<Ticket> tickets) {
		if (tickets == null || tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}
		
		Map<Integer, Ticket> ticketMap = new HashMap<Integer, Ticket>();
		for (Ticket ticket: tickets) {
			ticketMap.put(ticket.getId(), ticket);
		}
		
		List<Ticket> result = new ArrayList<Ticket>();
		Map<Integer, Long> groupIdByTicketId = new HashMap<Integer, Long>();
		Map<Long, List<Ticket>> ticketListByGroupId = new HashMap<Long, List<Ticket>>();
		
		// get duplicate marked tickets
		for (DuplicateTicketMap map: DAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(tickets.iterator().next().getEventId())) {
			groupIdByTicketId.put(map.getTicketId(), map.getGroupId());
			List<Ticket> ticketList = ticketListByGroupId.get(map.getGroupId());
			if (ticketList == null) {
				ticketList = new ArrayList<Ticket>();
				ticketListByGroupId.put(map.getGroupId(), ticketList);
			}
			Ticket ticket = ticketMap.get(map.getTicketId());
			if (ticket != null) {
				ticketList.add(ticket);
			}
		}
		
		// delete based on price and exchanges
		
		for (Ticket ticket: tickets) {
			Long groupId = groupIdByTicketId.get(ticket.getId());
			if (groupId != null) {
				List<Ticket> ticketList = ticketListByGroupId.get(groupId);
				if (ticketList != null) {
					try {
						ticketList.sort( new Comparator<Ticket>() {
						@Override
						public int compare(Ticket t1, Ticket t2) {
							int cmp = t1.getAdjustedCurrentPrice().compareTo(t2.getAdjustedCurrentPrice());
							
							if (cmp != 0) {
								return cmp;
							}
							
							return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
						}
					});
					}catch(Exception ex){
						 System.out.println("Exception in Sorting TicketUtil line no 2033 " + ex);
					}
					ticketListByGroupId.remove(groupId);
					// mark other tix as dupe of the best one
					int i = 0;
					Ticket bestTicket = null;
					for (Ticket dupTicket: ticketList) {
						if (i++ == 0) {
							bestTicket = dupTicket;
							result.add(bestTicket);
						} else {
							dupTicket.setDupRefTicketId(bestTicket.getId());
						}
					}
				}
			} else {
				result.add(ticket);
			}
		}
		
		return result;
	}
	
	/**
	 * Implement the duplicate ticket removal logic:
	 * 1) remove dups based on section, row and wholesale
	 * 2) identify AO tickets
	 * 3) scrub other non-AO tickets
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 */
	public static List<Ticket> superSmartRemoveDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy removeDuplicatePolicy) {
		List<Ticket> processedMarkedDupTickets = scrubExplicitlyMarkedDuplicateTickets(tickets);
		List<Ticket> result = new ArrayList<Ticket>();
		
		// group tickets by qty, section, row, price
		// siteId => (key => {tickets})
		Map<String, Map<String, Collection<Ticket>>> sameQtySecRowPriceBySiteMap
				= createMapByKeyAndSite(processedMarkedDupTickets, new TicketKeyCallBack() {
					
					public String compute(Ticket ticket) {
						return  getTicketDuplicateByQuantitySectionRowPriceKey(ticket);
					}
				});
		
		// remove same seats with lower ranking
		removeSameSeatWithLowerRank(sameQtySecRowPriceBySiteMap);
		
		// remove most likely tickets based on seats (e.g., when comparing EI: 1 with SH * and TN *)
		//removeMostLikelySameTickets(sameQtySecRowPriceBySiteMap);
		removeDuplicateBySeactionRowPrice(sameQtySecRowPriceBySiteMap);
		final Collection<Ticket> nonAOTicketContainer = new ArrayList<Ticket>();
		
		Map<String, Collection<Ticket>> ticketResultMap = mergeTicketMap(sameQtySecRowPriceBySiteMap, new TicketMergeCallBack() {
			
			public void merge(Collection<Ticket> merged, Collection<Ticket> ticketRow) {
				// simple merge
				// FIXME improve later in the case there are different seats for the same row
				// Changes by cs as added new case of different seats with same row
				/*Collection<Ticket> row = new ArrayList<Ticket>();
				if (!merged.isEmpty()) {
					List<Ticket> toMerged = new ArrayList<Ticket>(merged);
					int diff = ticketRow.size() - merged.size();
					if (diff > 0) {
						for (Ticket ticket: ticketRow) {
							if (diff-- < 0) {
								ticket.setDupRefTicketId(toMerged.get(0).getId());
							} 
							row.add(ticket);
						}
					} else {
						for (Ticket ticket: ticketRow) {
							ticket.setDupRefTicketId(toMerged.get(0).getId());
							row.add(ticket);
						}
					}
				} else {
					row.addAll(ticketRow);
				}
				
				merged.addAll(row);
				*/
				merged.addAll(ticketRow);

			}
		});
		/* cs code starts here*/
		for (Collection<Ticket> list: ticketResultMap.values()) {
				for (Ticket ticket: list) {
					if (ticket.getDupRefTicketId() == null) {
						result.add(ticket);
					}
				}
			
		}
	

		return result;
	}
	
	
	// return best match [delta, ticket1, ticket2]
	private static Object[] getClosestTicket(Collection<Ticket> tickets1, Collection<Ticket> tickets2) {
		Object[] bestMatch = new Object[3];
		
		for (Ticket ticket: tickets1) {
			Object[] res = getClosestTicket(ticket, tickets2, false);
			if (res == null) {
				continue;
			}
			Double delta = (Double)res[0];
			Ticket closestTicket = (Ticket)res[1];
			
			if (bestMatch[0] == null || Math.abs(delta) < Math.abs((Double)bestMatch[0])) {
				bestMatch[0] = delta;
				bestMatch[1] = ticket;
				bestMatch[2] = closestTicket;
			}
		}
		
		if (bestMatch[0] == null) {
			return null; // no match
		}
		return bestMatch;
	}

	// compare a ticket to a list of tickets
	// return best match [delta, ticket] or null if no match
	private static Object[] getClosestTicket(Ticket ticket, Collection<Ticket> tickets, boolean strictMatch) {
		Double ticketPrice;
		
		Ticket resultTicket = null;
		Double minDelta = 0D;
		
		if (ticket.getSiteId().equals("stubhub")) {
			ticketPrice = ticket.getPurchasePrice();
		} else {
			ticketPrice = ticket.getPurchasePrice();
		}
		
		for (Ticket curTicket: tickets) {
			if (curTicket.getDupRefTicketId() != null) { // skip already dup ticket
				continue;
			}
			// check seat compatibility => only accept if seats are STRICTLY EQUALS (*=* or A=A)
			if (strictMatch) {
				if (!normalizeSeat(ticket.getSeat()).equals(normalizeSeat(curTicket.getSeat()))) {
					continue;
				}
			} else { // otherwise check if ticket and curTicket have seats which are somehow compatible
				if (!seatMatch(ticket.getSeat(), curTicket.getSeat())) {
					continue;
				}
			}
			
			Double curTicketPrice; 
			if (curTicket.getSiteId().equals("stubhub")) {
				curTicketPrice = curTicket.getPurchasePrice();
			} else {
				curTicketPrice = curTicket.getPurchasePrice();
			}
			
			Double delta;
			if(curTicket.getSeat()!=null && ticket.getSeat()!=null){
				if(curTicket.getSeat().equalsIgnoreCase(ticket.getSeat())){
					resultTicket=curTicket;
				}
				else{
					continue;
				}
			}
			
			if (curTicketPrice.equals(ticketPrice)) {
				delta = 0D;
			} else {
				
				delta = (ticketPrice/curTicketPrice) - 1;  // Changed by Chirag : Used in Browse page..
			}
			
			if (resultTicket == null || Math.abs(delta) < Math.abs(minDelta)) {
				resultTicket = curTicket;
				minDelta = delta;
			}
		}
		
		if (resultTicket == null) {
			return null;
		}
		
		return new Object[]{minDelta, resultTicket};
	}

	public static boolean isSeatNull(String seat) {
		if (seat == null) {
			return true;
		}
		
		seat = seat.trim().toUpperCase();
		
		if (seat.isEmpty()
			|| seat.equals("*")
			|| seat.equals("-")
			|| seat.equals("*-*")
			|| seat.contains("N/A")) {
			return true;
		}
		
		return false;
	}
	
	public static String normalizeSeat(String seat) {
		if (isSeatNull(seat)) {
			return "";
		}
		if(!seat.contains("-")){
			Pattern pattern = Pattern.compile("^\\d+$");
			Matcher matcher = pattern.matcher(seat);
			if(matcher.find()){
				seat=seat+"-" + seat;
			}
		}
		return seat.replaceAll("0*(\\d+)-0*(\\d+)", "$1-$2").replaceAll("^0*(\\d+)$", "$1");
	}
		
	public static boolean seatMatch(String seat1, String seat2) {
		if (isSeatNull(seat1)
			|| isSeatNull(seat2)) {
			return true;
		}
		
		String[] tokens1 = seat1.split("-");
		String[] tokens2 = seat2.split("-");
		
		Integer startSeat1;
		Integer endSeat1;
		Integer startSeat2;
		Integer endSeat2;
		
		try {
			if (tokens1.length == 2) {
				startSeat1 = Integer.valueOf(tokens1[0].trim());
				endSeat1 = Integer.valueOf(tokens1[1].trim());
			} else {
				startSeat1 = endSeat1 = Integer.valueOf(seat1.trim());
			}

			if (tokens2.length == 2) {
				startSeat2 = Integer.valueOf(tokens2[0].trim());
				endSeat2 = Integer.valueOf(tokens2[1].trim());
			} else {
				startSeat2 = endSeat2 = Integer.valueOf(seat2.trim());
			}
			
			if (startSeat1 <= startSeat2 && endSeat1 <= endSeat2) {
				return true;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private interface TicketKeyCallBack {
		String compute(Ticket ticket);
	}

	private interface TicketMergeCallBack {
		void merge(Collection<Ticket> merged, Collection<Ticket> ticketRow);
	}

	private static AdmitoneInventory getInventory(Ticket ticket, Collection<AdmitoneInventory> inventories,Map<String,Synonym> synonymsMap) {
		TourType type = ticket.getEvent().getEventType();
		
		if (ticket == null || ticket.getSeller() == null || ticket.getSection() == null || ticket.getRow() == null || inventories == null) {
			return null;
		}
		
		for (AdmitoneInventory admitoneInventory: inventories) {
			if (admitoneInventory == null || admitoneInventory.getBroadcastSeller() == null
				|| admitoneInventory.getSection() == null || admitoneInventory.getRow() == null) {
				continue;
			}
			
			Integer quantity = admitoneInventory.getQuantity();
			String seller = admitoneInventory.getBroadcastSeller();
			String section = SectionRowStripper.strip(type, admitoneInventory.getSection().replaceAll("[-\\.]", ""),synonymsMap).trim().toUpperCase();
			String row = SectionRowStripper.strip(type, admitoneInventory.getRow().replaceAll("[-\\.]", ""),synonymsMap).trim().toUpperCase();
			String seat = admitoneInventory.getSeat().replaceAll("\\*-\\*", "").trim();
			seat = seat.equals("-")?"":seat;
			Double price = (admitoneInventory.getCurrentPrice() != null) ? admitoneInventory.getCurrentPrice(): 0D;
			String ticketSection = (ticket.getNormalizedSection() != null) ? ticket.getNormalizedSection().replaceAll("[-\\.]", "").trim(): "";
			String ticketRow = (ticket.getNormalizedRow() != null) ? ticket.getNormalizedRow().replaceAll("[-\\.]", "").trim(): "";
			String ticketSeat = (ticket.getSeat() != null) ? ticket.getSeat().replaceAll("\\*-\\*", "").trim(): "";
			Double ticketPrice = (ticket.getAdjustedCurrentPrice() != null) ? ticket.getAdjustedCurrentPrice(): 0D;
			

			if (quantity.equals(ticket.getRemainingQuantity())
				&& (seller.equals("all")
					|| seller.contains(ticket.getSiteId())
					|| ((seller.contains("eimarketplace") || seller.contains("eibox")) && (ticket.getSiteId().contains("eimarketplace") || ticket.getSiteId().contains("eibox")))
					|| ((seller.contains("ticketnetwork") || seller.contains("ticketnetworkdirect")) && (ticket.getSiteId().contains("ticketnetwork") || ticket.getSiteId().contains("ticketnetworkdirect")))
					)
				&& section.equalsIgnoreCase(ticketSection)
				&& row.equalsIgnoreCase(ticketRow)) {

				// quick fix for seatwave
				// seatwave tix are listed +10% higher
				
				if (ticket.getSiteId().equals("seatwave")) {
					if (!price.equals(1.1D * ticketPrice)) {
						continue;
					}
				}else if(!seat.equals("") && !ticketSeat.equals("") && seat.equals(ticketSeat)){
					
				}else if (!price.equals(ticketPrice)) {
					continue;
				}
				
				//Will check if seat exactly matches like *=* or number=number or nonadmitone ticket falls in the range of admitone ticket
//				if((isSeatNull(seat) && isSeatNull(ticketSeat))||(!isSeatNull(seat) && !isSeatNull(ticketSeat)
//					&& seatMatch(normalizeSeat(seat), normalizeSeat(ticket.getSeat())))) {
//					return admitoneInventory;
//				}
				if (isSeatNull(seat) || isSeatNull(ticketSeat)
					|| seatMatch(normalizeSeat(seat), normalizeSeat(ticket.getSeat()))) {
					return admitoneInventory;
				}
			}
		}
		return null;
	}

	public static boolean isInInventory(Ticket ticket, Collection<AdmitoneInventory> inventories,Map<String,Synonym> synonymsMap) {
		return getInventory(ticket, inventories,synonymsMap) != null;
	}
	
	private static Map<String, Collection<AdmitoneInventory>> createInventoryMapBySectionAndRow(Collection<AdmitoneInventory> inventories, TourType type,Map<String,Synonym> synonymMap) {
		// map section-row => {inventory}
		Map<String, Collection<AdmitoneInventory>> inventoryMap = new HashMap<String, Collection<AdmitoneInventory>>(); 
		for (AdmitoneInventory inventory: inventories) {
			String section ="";
			section = SectionRowStripper.strip(type, inventory.getSection().replaceAll("[-\\.]", ""),synonymMap).trim().toUpperCase();
			String row = SectionRowStripper.strip(type, inventory.getRow().replaceAll("[-\\.]", ""),synonymMap).trim().toUpperCase();
			String seat = inventory.getSeat()!=null?inventory.getSeat().trim():"";
			String key = section + "-" + row + "-" + seat;
			
			Collection<AdmitoneInventory> inventoryList = inventoryMap.get(key);
			if (inventoryList == null) {
				inventoryList = new ArrayList<AdmitoneInventory>();
				inventoryMap.put(key, inventoryList);
			}
			inventoryList.add(inventory);
		}
		return inventoryMap;
	}
		
	/*private static Map<String, Collection<AdmitoneInventory>> createInventoryMapBySectionAndRowIra(Collection<AdmitoneInventory> inventories, TourType type) {
		// map section-row => {inventory}
		Map<String, Collection<AdmitoneInventory>> inventoryMap = new HashMap<String, Collection<AdmitoneInventory>>(); 
		
		for (AdmitoneInventory inventory: inventories) {
			String section = SectionRowStripper.strip(type, inventory.getSection().replaceAll("[-\\.]", "")).trim().toUpperCase();
			String row = SectionRowStripper.strip(type, inventory.getRow().replaceAll("[-\\.]", "")).trim().toUpperCase();
			String qty = "";
			if(inventory.getQuantity() != null)
				qty = inventory.getQuantity().toString();
			String key = section + "-" + row + "-" + qty;
			
			Collection<AdmitoneInventory> inventoryList = inventoryMap.get(key);
			if (inventoryList == null) {
				inventoryList = new ArrayList<AdmitoneInventory>();
				inventoryMap.put(key, inventoryList);
			}
			inventoryList.add(inventory);
		}
		return inventoryMap;
	}*/
	/**
	 * Mark tickets as admit one based on:
	 * - if they are in the inventory
	 * - if the seller is ADMT1
	 * - if it has been marked explicitly as an ADMIT ONE ticket
	 * @param tickets
	 * @return
	 */
	public static void computeAdmitOneTickets(Collection<Ticket> tickets,AdmitoneEventLocal admitoneEventLocal) {
		if (tickets == null || tickets.isEmpty()) {
			return;
		}

		// handle marked and unmarked tickets
		Map<Integer, AdmitoneTicketMark> admitoneTicketMarkMap = new HashMap<Integer, AdmitoneTicketMark>();
		Ticket firstTicket = tickets.iterator().next();

		if (firstTicket.getEventId() != null) {
			for (AdmitoneTicketMark admitoneTicketMark: DAORegistry.getAdmitoneTicketMarkDAO().getAdmitOneTicketMarksByEvent(firstTicket.getEventId())) {
				admitoneTicketMarkMap.put(admitoneTicketMark.getTicketId(), admitoneTicketMark);
			}
		}	
	
		List<Ticket> sortedTickets = new ArrayList<Ticket>(tickets);
		
		try{
			sortedTickets.sort( new Comparator<Ticket>() {

			public int compare(Ticket ticket1, Ticket ticket2) {
				String seat1 = (ticket1.getSeat() == null) ? "" : ticket1.getSeat().replaceAll("\\*-\\*", "").trim();
				String seat2 = (ticket2.getSeat() == null) ? "" : ticket2.getSeat().replaceAll("\\*-\\*", "").trim();
				String seller1 = (ticket1.getSeller() == null) ? "" : ticket1.getSeller();
				String seller2 = (ticket2.getSeller() == null) ? "" : ticket2.getSeller();
				
				if (!seat1.isEmpty() && seat2.isEmpty()) {
					return -1;
				}

				if (seat1.isEmpty() && !seat2.isEmpty()) {
					return 1;
				}

				if (seller1.equals("ADMT1") && !seller2.equals("ADMT1")) {
					return -1;
				}

				if (!seller1.equals("ADMT1") && seller2.equals("ADMT1")) {
					return 1;
				}

				return 0;
			}
		});
		}catch(Exception ex){
			 System.out.println("Exception in Sorting TicketUtil line no 2466 " + ex);
		}
			
		// put the inventories with specified seats first and tickets with seller='ADMT1' first
		List<AdmitoneInventory> sortedInventories = new ArrayList<AdmitoneInventory>(AdmitoneInventoryManager.getInstance().getInventoryByEvent(firstTicket.getEventId()));
		if(admitoneEventLocal != null){
			List<AdmitoneInventoryLocal> sortedInventoriesLocal = new ArrayList<AdmitoneInventoryLocal>(AdmitoneInventoryLocalManager.getInstance().getInventoryLocalByEvent(admitoneEventLocal.getEventId()));
			if(!sortedInventoriesLocal.isEmpty()){
			
				for(AdmitoneInventoryLocal admitoneInventoryLocal : sortedInventoriesLocal){
					AdmitoneInventory admitoneInventory = new AdmitoneInventory();
					admitoneInventory.setBroadcastSeller(admitoneInventoryLocal.getBroadcastSeller());
					admitoneInventory.setBroker(admitoneInventoryLocal.getBroker());
					admitoneInventory.setCost(admitoneInventoryLocal.getCost());
					admitoneInventory.setCurrentPrice(admitoneInventoryLocal.getCurrentPrice());
					admitoneInventory.setEventId(admitoneInventoryLocal.getEventId());
					admitoneInventory.setInventoryType(admitoneInventoryLocal.getInventoryType());
					admitoneInventory.setQuantity(admitoneInventoryLocal.getQuantity());
					admitoneInventory.setRow(admitoneInventoryLocal.getRow());
					admitoneInventory.setSeat(admitoneInventoryLocal.getSeat());
					admitoneInventory.setSection(admitoneInventoryLocal.getSection());
					admitoneInventory.setTicketId(admitoneInventoryLocal.getTicketId());
					sortedInventories.add(admitoneInventory);
				}
		}
		}
//		if (sortedInventories.isEmpty()) {
//			return;
//		}
		
		try{
			sortedInventories.sort( new Comparator<AdmitoneInventory>() {
			@Override
			public int compare(AdmitoneInventory inventory1, AdmitoneInventory inventory2) {
				String seat1 = inventory1.getSeat().replaceAll("\\*-\\*", "").trim();
				String seat2 = inventory2.getSeat().replaceAll("\\*-\\*", "").trim();
				
				if (!seat1.isEmpty() && seat2.isEmpty()) {
					return -1;
				}

				if (seat1.isEmpty() && !seat2.isEmpty()) {
					return 1;
				}
				
				return 0;
			}
		});
	}catch(Exception ex){
		 System.out.println("Exception in Sorting TicketUtil line no 2516 " + ex);
	}

		TourType type = firstTicket.getEvent().getEventType();
//		List<Ticket> admitoneTickets = new ArrayList<Ticket>();
//		Map<String,Synonym> synonymsMap = null;
		Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
		Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
		Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
		for(Synonym synonym:synonyms){
			if(synonym.getType().equals("Theater")){
				theaterSynonymMap.put(synonym.getName(), synonym);
			}else{
				concertSynonymMap.put(synonym.getName(), synonym);
			}
		}
		try{
			Map<String,Synonym> synonymMap = null;
			if(type.equals(TourType.THEATER)){
				synonymMap = theaterSynonymMap;
			}else if(type.equals(TourType.CONCERT) || type.equals(TourType.SPORT)){
				synonymMap = concertSynonymMap;
			}
			else{
				synonymMap =  new HashMap<String, Synonym>();
			}
			Map<String, Collection<AdmitoneInventory>> inventoryMap = createInventoryMapBySectionAndRow(sortedInventories, type,synonymMap);
			for (String siteId: reversedRankedSiteIds) {
			 	for (Ticket ticket: filterTicketsBySite(siteId, sortedTickets)) {
					AdmitoneTicketMark admitoneTicketMark = admitoneTicketMarkMap.get(ticket.getId());
					if (admitoneTicketMark != null) {
						if (admitoneTicketMark.getType().equals(AdmitoneTicketMarkType.MARK)) {
							ticket.setAdmitOneTicket(true);
							ticket.setSeller("ADMT1");
						}
						continue;
					}
					String section ="";
					//Changed for IRA Purpose BY cs 07.17.2014
//					section = SectionRowStripper.strip(type, ticket.getSection().replaceAll("[-\\.]", ""),synonymMap).trim().toUpperCase();
					section = ticket.getNormalizedSection();
					String row = ticket.getNormalizedRow();
					String seat = ticket.getSeat()!=null?ticket.getSeat().trim():"";
					String key = section + "-" + row + "-" + seat;
					
					Collection<AdmitoneInventory> inventoryList = inventoryMap.get(key);
		
					if (inventoryList == null || inventoryList.isEmpty()) {
						continue;
					}
					AdmitoneInventory inventory = null;
					inventory = getInventory(ticket, inventoryList,synonymMap);
					if (inventory != null) {
						
						ticket.setAoCategoryTicket(inventory.getCategoryTicket());
						
						inventoryList.remove(inventory);
						ticket.setAdmitOneTicket(true);
						ticket.setSeller("ADMT1");
					}
					if(!inventoryList.isEmpty())
					{
						for (AdmitoneInventory admitoneInventory: inventoryList) 
						{
							Ticket ticketTmp = new Ticket(ticket);
							ticketTmp.setAdmitOneNonTMTicket(true);
							ticket.setAdmitOneTicket(false);
							ticketTmp.setSeller("ADMT1");
							ticketTmp.setRemainingQuantity(admitoneInventory.getQuantity());
							ticketTmp.setRow(admitoneInventory.getRow());
							ticketTmp.setSeat(admitoneInventory.getSeat());
							//ticketTmp.setSection(admitoneInventory.getSection());
							ticketTmp.setCurrentPrice(admitoneInventory.getCurrentPrice());
							ticketTmp.setSiteId(admitoneInventory.getBroadcastSeller());
							//ticketTmp.setSiteId("admitone");
							ticketTmp.setId(admitoneInventory.getTicketId());
							ticketTmp.setAoCategoryTicket(admitoneInventory.getCategoryTicket());
							
							if(!tickets.contains(ticketTmp))
								tickets.add(ticketTmp);
						}
					}
				}	
	
			}
		}
		catch(Exception e){
			logger.info("Null pointer exception");
			e.printStackTrace();
		}

		/*
		Collection<AdmitoneInventory> inventories = AdmitoneInventoryManager.getInstance().getInventoryByEvent(firstTicket.getEventId());

		Map<Integer, AdmitoneTicketMark> admitoneTicketMarkMap = new HashMap<Integer, AdmitoneTicketMark>();
		
		if (firstTicket.getEventId() != null) {
			for (AdmitoneTicketMark admitoneTicketMark: DAORegistry.getAdmitoneTicketMarkDAO().getAdmitOneTicketMarksByEvent(firstTicket.getEventId())) {
				admitoneTicketMarkMap.put(admitoneTicketMark.getTicketId(), admitoneTicketMark);
			}
		}
		
		for (Ticket ticket: tickets) {
			AdmitoneTicketMark admitoneTicketMark = admitoneTicketMarkMap.get(ticket.getId());
			
			if (admitoneTicketMark == null) {
//				// check if the seller is 'ADMT1'
//				if (ticket.getSeller().equalsIgnoreCase("ADMT1")) {
//					ticket.setAdmitOneTicket(true);
//					ticket.setSeller("ADMT1");
//				} else
				if(isInInventory(ticket, inventories)) {
					ticket.setAdmitOneTicket(true);
					ticket.setSeller("ADMT1");
				}
			} else if (admitoneTicketMark.getType().equals(AdmitoneTicketMarkType.MARK)) {
				ticket.setAdmitOneTicket(true);
				ticket.setSeller("ADMT1");
			}
		}
		*/
	}
	//Replica of computeAdmitOneTickets
	/*tmat ticket table not contain seat and other side admitone_inventory table contain seats so it is not mapping 
	  section - row - seat criteria for ao zone ticket tab for ira
	  now because of this issue change section - row - seat to section - row - qty*/
	public static void computeAdmitOneInventoryTickets(Collection<Ticket> tickets,AdmitoneEventLocal admitoneEventLocal) {
		if (tickets == null || tickets.isEmpty()) {
			return;
		}

		// handle marked and unmarked tickets
		Map<Integer, AdmitoneTicketMark> admitoneTicketMarkMap = new HashMap<Integer, AdmitoneTicketMark>();
		Ticket firstTicket = tickets.iterator().next();

		if (firstTicket.getEventId() != null) {
			for (AdmitoneTicketMark admitoneTicketMark: DAORegistry.getAdmitoneTicketMarkDAO().getAdmitOneTicketMarksByEvent(firstTicket.getEventId())) {
				admitoneTicketMarkMap.put(admitoneTicketMark.getTicketId(), admitoneTicketMark);
			}
		}	
	
		List<Ticket> sortedTickets = new ArrayList<Ticket>(tickets);
		try{
			sortedTickets.sort( new Comparator<Ticket>() {
			@Override
			public int compare(Ticket ticket1, Ticket ticket2) {
				String seat1 = (ticket1.getSeat() == null) ? "" : ticket1.getSeat().replaceAll("\\*-\\*", "").trim();
				String seat2 = (ticket2.getSeat() == null) ? "" : ticket2.getSeat().replaceAll("\\*-\\*", "").trim();
				String seller1 = (ticket1.getSeller() == null) ? "" : ticket1.getSeller();
				String seller2 = (ticket2.getSeller() == null) ? "" : ticket2.getSeller();
				
				if (!seat1.isEmpty() && seat2.isEmpty()) {
					return -1;
				}

				if (seat1.isEmpty() && !seat2.isEmpty()) {
					return 1;
				}

				if (seller1.equals("ADMT1") && !seller2.equals("ADMT1")) {
					return -1;
				}

				if (!seller1.equals("ADMT1") && seller2.equals("ADMT1")) {
					return 1;
				}

				return 0;
			}
		});
		}catch(Exception ex){
			 System.out.println("Exception in Sorting TicketUtil line no 2687 " + ex);
		}
			
		// put the inventories with specified seats first and tickets with seller='ADMT1' first
		List<AdmitoneInventory> sortedInventories = new ArrayList<AdmitoneInventory>(AdmitoneInventoryManager.getInstance().getInventoryByEvent(firstTicket.getEventId()));
		if(admitoneEventLocal != null){
			List<AdmitoneInventoryLocal> sortedInventoriesLocal = new ArrayList<AdmitoneInventoryLocal>(AdmitoneInventoryLocalManager.getInstance().getInventoryLocalByEvent(admitoneEventLocal.getEventId()));
			if(!sortedInventoriesLocal.isEmpty()){
			
				for(AdmitoneInventoryLocal admitoneInventoryLocal : sortedInventoriesLocal){
					AdmitoneInventory admitoneInventory = new AdmitoneInventory();
					admitoneInventory.setBroadcastSeller(admitoneInventoryLocal.getBroadcastSeller());
					admitoneInventory.setBroker(admitoneInventoryLocal.getBroker());
					admitoneInventory.setCost(admitoneInventoryLocal.getCost());
					admitoneInventory.setCurrentPrice(admitoneInventoryLocal.getCurrentPrice());
					admitoneInventory.setEventId(admitoneInventoryLocal.getEventId());
					admitoneInventory.setInventoryType(admitoneInventoryLocal.getInventoryType());
					admitoneInventory.setQuantity(admitoneInventoryLocal.getQuantity());
					admitoneInventory.setRow(admitoneInventoryLocal.getRow());
					admitoneInventory.setSeat(admitoneInventoryLocal.getSeat());
					admitoneInventory.setSection(admitoneInventoryLocal.getSection());
					admitoneInventory.setTicketId(admitoneInventoryLocal.getTicketId());
					sortedInventories.add(admitoneInventory);
				}
		}
		}
//		if (sortedInventories.isEmpty()) {
//			return;
//		}
		
		try{
		sortedInventories.sort( new Comparator<AdmitoneInventory>() {
			@Override
			public int compare(AdmitoneInventory inventory1, AdmitoneInventory inventory2) {
				String seat1 = inventory1.getSeat().replaceAll("\\*-\\*", "").trim();
				String seat2 = inventory2.getSeat().replaceAll("\\*-\\*", "").trim();
				
				if (!seat1.isEmpty() && seat2.isEmpty()) {
					return -1;
				}

				if (seat1.isEmpty() && !seat2.isEmpty()) {
					return 1;
				}
				
				return 0;
			}
		});
	}catch(Exception ex){
		 System.out.println("Exception in Sorting TicketUtil line no 2734 " + ex);
	}

		TourType type = firstTicket.getEvent().getEventType();
//		List<Ticket> admitoneTickets = new ArrayList<Ticket>();
//		Map<String,Synonym> synonymsMap = new HashMap<String, Synonym>();
		Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
		Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
		Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
		for(Synonym synonym:synonyms){
			if(synonym.getType().equals("Theater")){
				theaterSynonymMap.put(synonym.getName(), synonym);
			}else{
				concertSynonymMap.put(synonym.getName(), synonym);
			}
		}
		try{
			Map<String,Synonym> synonymMap = null;
			if(type.equals(TourType.THEATER)){
				synonymMap = theaterSynonymMap;
			}else if(type.equals(TourType.CONCERT) || type.equals(TourType.SPORT)){
				synonymMap = concertSynonymMap;
			}
			else{
				synonymMap =  new HashMap<String, Synonym>();
			}
			Map<String, Collection<AdmitoneInventory>> inventoryMap = createInventoryMapBySectionAndRow(sortedInventories, type,synonymMap);
			for (String siteId: reversedRankedSiteIds) {
				for (Ticket ticket: filterTicketsBySite(siteId, sortedTickets)) {
					AdmitoneTicketMark admitoneTicketMark = admitoneTicketMarkMap.get(ticket.getId());
					if (admitoneTicketMark != null) {
						if (admitoneTicketMark.getType().equals(AdmitoneTicketMarkType.MARK)) {
							ticket.setAdmitOneTicket(true);
							ticket.setSeller("ADMT1");
						}
						continue;
					}
					
					String section ="";
					//Changed for IRA Purpose BY cs 07.17.2014
//					section = SectionRowStripper.strip(type, ticket.getSection().replaceAll("[-\\.]", ""),synonymMap).trim().toUpperCase();
					section = ticket.getNormalizedSection();
					String row = ticket.getNormalizedRow();
					String qty = "";
					if(ticket.getQuantity() != null)
						qty = ticket.getQuantity().toString();
					String key = section + "-" + row + "-" + qty;
					
					Collection<AdmitoneInventory> inventoryList = inventoryMap.get(key);
		
					if (inventoryList == null || inventoryList.isEmpty()) {
						continue;
					}
									
					AdmitoneInventory inventory = null;
//					if(ticket.getEvent().getEventType().equals(TourType.THEATER)){
//						inventory = getInventory(ticket, inventoryList,theaterSynonymMap);
//					}else{
						inventory = getInventory(ticket, inventoryList,synonymMap);
//					}
					if (inventory != null) {
						inventoryList.remove(inventory);
						ticket.setAdmitOneTicket(true);
						ticket.setSeller("ADMT1");
					}
					if(!inventoryList.isEmpty())
					{
						for (AdmitoneInventory admitoneInventory: inventoryList) 
						{
							Ticket ticketTmp = new Ticket(ticket);
							ticketTmp.setAdmitOneNonTMTicket(true);
							ticket.setAdmitOneTicket(false);
							ticketTmp.setSeller("ADMT1");
							ticketTmp.setRemainingQuantity(admitoneInventory.getQuantity());
							ticketTmp.setRow(admitoneInventory.getRow());
							ticketTmp.setSeat(admitoneInventory.getSeat());
							//ticketTmp.setSection(admitoneInventory.getSection());
							ticketTmp.setCurrentPrice(admitoneInventory.getCurrentPrice());
							ticketTmp.setSiteId(admitoneInventory.getBroadcastSeller());
							//ticketTmp.setSiteId("admitone");
							ticketTmp.setId(admitoneInventory.getTicketId());
							if(!tickets.contains(ticketTmp))
								tickets.add(ticketTmp);
						}
					}
				}	
	
		}
		}
		catch(Exception e){
			logger.info("Null pointer exception");
			e.printStackTrace();
		}

		/*
		Collection<AdmitoneInventory> inventories = AdmitoneInventoryManager.getInstance().getInventoryByEvent(firstTicket.getEventId());

		Map<Integer, AdmitoneTicketMark> admitoneTicketMarkMap = new HashMap<Integer, AdmitoneTicketMark>();
		
		if (firstTicket.getEventId() != null) {
			for (AdmitoneTicketMark admitoneTicketMark: DAORegistry.getAdmitoneTicketMarkDAO().getAdmitOneTicketMarksByEvent(firstTicket.getEventId())) {
				admitoneTicketMarkMap.put(admitoneTicketMark.getTicketId(), admitoneTicketMark);
			}
		}
		
		for (Ticket ticket: tickets) {
			AdmitoneTicketMark admitoneTicketMark = admitoneTicketMarkMap.get(ticket.getId());
			
			if (admitoneTicketMark == null) {
//				// check if the seller is 'ADMT1'
//				if (ticket.getSeller().equalsIgnoreCase("ADMT1")) {
//					ticket.setAdmitOneTicket(true);
//					ticket.setSeller("ADMT1");
//				} else
				if(isInInventory(ticket, inventories)) {
					ticket.setAdmitOneTicket(true);
					ticket.setSeller("ADMT1");
				}
			} else if (admitoneTicketMark.getType().equals(AdmitoneTicketMarkType.MARK)) {
				ticket.setAdmitOneTicket(true);
				ticket.setSeller("ADMT1");
			}
		}
		*/
	}
	
	private TicketUtil() {} // prevent from instantiating this utility class
	
	
	public static void getFilteredTickets(Integer eventId, List<Integer> categoryIds, List<Integer> quantities,
			Map<String, Boolean> siteFilters, String section, String row, //String show,
			Collection<Ticket> origTickets, Collection<Ticket> filteredTickets, List<Ticket> filteredHTickets,Collection<Ticket> tickets,Collection<Ticket> hTickets ,Collection<Category> categories,
			String catScheme, boolean recurse, Double startPrice, Double endPrice, String priceType,
			RemoveDuplicatePolicy removeDuplicatePolicy, Map<String, Long> timeStats, 
			AdmitoneEventLocal admitoneEventLocal,VenueCategory venueCategory,Map<String, DefaultPurchasePrice> defaultTourPriceMap,
			Map<String, ManagePurchasePrice> purchasePriceMap,String inHandTypeStr) {
		
/*		
 		Collection<Ticket> tickets = new ArrayList<Ticket>();
		Collection<Ticket> hTickets = new ArrayList<Ticket>();
		Long loadCategoriesStartTime = System.currentTimeMillis();
		Event event = DAORegistry.getEventDAO().get(eventId);
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), catScheme);
		timeStats.put("loadCategories", System.currentTimeMillis() - loadCategoriesStartTime);
		
		Long loadTicketsStartTime = System.currentTimeMillis();
		if ("completed".equals(show)) {
			// show completed  ticket					
			tickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		} else {
			// show active ticket
			tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		}

		timeStats.put("loadTickets", System.currentTimeMillis() - loadTicketsStartTime);

		Long preAssignCategoriesStartTime = System.currentTimeMillis();		
		TicketUtil.preAssignCategoriesToTickets(tickets, categories, catScheme); 
		timeStats.put("preAssignCategories", System.currentTimeMillis() - preAssignCategoriesStartTime);
*/
		Boolean inHandType = null;
		if(inHandTypeStr!=null){
			inHandType = Boolean.parseBoolean(inHandTypeStr);
		}
		Long filterCategorizedStartTime = System.currentTimeMillis();		
		if (categoryIds.get(0).equals(Category.ALL_ID)) {
		} else if (categoryIds.get(0).equals(Category.CATEGORIZED_ID)) {
			tickets = TicketUtil.filterCategorized(tickets, catScheme);
		} else {
			tickets = TicketUtil.filterCategories(categoryIds, tickets);
		}
		timeStats.put("filterCategorized", System.currentTimeMillis() - filterCategorizedStartTime);
		
		Long filterTicketsStartTime = System.currentTimeMillis();
		if (tickets != null) {
			List<Ticket> tmpFilteredTickets = new ArrayList<Ticket>();
			for(Ticket ticket: tickets) {
				// try to put the less costly conditions first
				if (
						siteFilters.get(ticket.getSiteId())
						&& (
								quantities.isEmpty() || quantities.contains(ticket.getRemainingQuantity()) 
								|| (quantities.contains(MAX_QTY) && (ticket.getRemainingQuantity() > MAX_QTY) )
						) && (inHandType==null || inHandType.equals(ticket.isInHand()))
						&& TicketUtil.isInRows(ticket, row)
						&& TicketUtil.isInSections(ticket, section)) {
					tmpFilteredTickets.add(ticket);
				}
			}
		
			for (Ticket ticket: tmpFilteredTickets) {
				if (TicketUtil.isInPriceRange(ticket, startPrice, endPrice, priceType)) {
					origTickets.add(ticket);
				}
				Double tempPrice = ticket.getBuyItNowPrice();
//				Map<String, ManagePurchasePrice> purchasePriceMap = managePurchasePriceMap.get(tourId);
//				Long time = managePurchasePriceTimeMap.get(tourId);
//				if(purchasePriceMap==null){
//					purchasePriceMap= new HashMap<String, ManagePurchasePrice>();
//				}
//				Long now =new Date().getTime();
//				if(purchasePriceMap.get(mapKey)==null || now-time>UPDATE_FREQUENCY){
//					List<ManagePurchasePrice> list = DAORegistry.getManagePurchasePriceDAO().getAllByManagePurchasePriceByTourId(event.getTourId());
//					for(ManagePurchasePrice tourPrice:list){
//						purchasePriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
//					}
//					managePurchasePriceMap.put(tourId,purchasePriceMap);
//					managePurchasePriceTimeMap.put(tourId,now);
//				}
				
				String mapKey = ticket.getSiteId()+ "-" + (ticket.getTicketDeliveryType()==null?"REGULAR":ticket.getTicketDeliveryType());
				ManagePurchasePrice managePurchasePrice=purchasePriceMap.get(mapKey);
				String priceByQuantity="Qty : Price&#13;";
				DecimalFormat df2 = new DecimalFormat( "#,###,###,##0.00" );
				if(managePurchasePrice!=null){
					double serviceFee=managePurchasePrice.getServiceFee();
					double  shippingFee=managePurchasePrice.getShipping();
					int currencyType=managePurchasePrice.getCurrencyType();
					tempPrice=(currencyType==1?((ticket.getBuyItNowPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getBuyItNowPrice() + serviceFee + (shippingFee/ticket.getQuantity())));
					for(int i=1;i<=ticket.getQuantity();i++){
						if(EbayUtil.isPartition(i,ticket.getQuantity())){
							priceByQuantity+= i + " : " + df2.format((currencyType==1?((ticket.getBuyItNowPrice()*(1+serviceFee/100)) + (shippingFee/i)):(ticket.getBuyItNowPrice() + serviceFee + (shippingFee/i)))) + "&#13;" ;
						}
					}
				}else{
					DefaultPurchasePrice defaultPurchasePrice =  defaultTourPriceMap.get(mapKey);
					if(defaultPurchasePrice!=null){
						double serviceFee=defaultPurchasePrice.getServiceFees();
						double  shippingFee=defaultPurchasePrice.getShipping();
						int currencyType=defaultPurchasePrice.getCurrencyType();
						tempPrice=(currencyType==1?((ticket.getBuyItNowPrice()*(1+serviceFee/100)) + (shippingFee/ticket.getQuantity())):(ticket.getBuyItNowPrice() + serviceFee + (shippingFee/ticket.getQuantity())));
						for(int i=1;i<=ticket.getQuantity();i++){
							if(EbayUtil.isPartition(i,ticket.getQuantity())){
								priceByQuantity+= i + " : " + df2.format((currencyType==1?((ticket.getBuyItNowPrice()*(1+serviceFee/100)) + (shippingFee/i)):(ticket.getBuyItNowPrice() + serviceFee + (shippingFee/i)))) + "&#13;" ;
							}
						}
					}
				}
				ticket.setPriceByQuantity(priceByQuantity); 
				ticket.setPurchasePrice(tempPrice);
			}	 
			
			TicketUtil.computeAdmitOneTickets(tmpFilteredTickets, admitoneEventLocal);
			Long removeDuplicateTicketsStartTime = System.currentTimeMillis();
			tmpFilteredTickets = TicketUtil.removeDuplicateTickets(tmpFilteredTickets, removeDuplicatePolicy);
			for (Ticket ticket: tmpFilteredTickets) {
				if (TicketUtil.isInPriceRange(ticket, startPrice, endPrice, priceType)) {
					filteredTickets.add(ticket);
				}
			}
			timeStats.put("removeDuplicateTickets", System.currentTimeMillis() - removeDuplicateTicketsStartTime);
		}	
		timeStats.put("filterTickets", System.currentTimeMillis() - filterTicketsStartTime);

		Long loadHistoricalTicketsStartTime = System.currentTimeMillis();		
		if (filteredHTickets != null) {
			/*
			hTickets = DAORegistry.getHistoricalTicketDAO().getAllTicketsByEvent(eventId);
			Collection<Ticket> tempHTickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
			if(tempHTickets != null) {
				hTickets.addAll(tempHTickets);
			}

			TicketUtil.preAssignCategoriesToTickets(hTickets, categories, catScheme);
			*/
			if (categoryIds.get(0).equals(Category.ALL_ID)) {
			} else if (categoryIds.get(0).equals(Category.CATEGORIZED_ID)) {
				hTickets = TicketUtil.filterCategorized(hTickets, catScheme);
			} else if (categoryIds.get(0).equals(Category.UNCATEGORIZED_ID)) {
				hTickets = TicketUtil.filterUncategorized(hTickets, catScheme);
			} else {
				hTickets = TicketUtil.filterCategories(categoryIds, hTickets);
			}
		}
		timeStats.put("loadHistoricalTickets", System.currentTimeMillis() - loadHistoricalTicketsStartTime);

		Long filterHistoricalTicketsStartTime = System.currentTimeMillis();
		if(filteredHTickets != null && hTickets != null) {
			List<Ticket> tmpFilteredHTickets = new ArrayList<Ticket>();
			for(Ticket ticket: hTickets) {
				if ((quantities.isEmpty() || quantities.contains(ticket.getRemainingQuantity()) 
						|| (quantities.contains(MAX_QTY) && (ticket.getRemainingQuantity() > MAX_QTY) ))
						&& siteFilters.get(ticket.getSiteId())
						&& (row == null || row.isEmpty() || (ticket.getRow() != null && ticket.getRow().toLowerCase().contains(row)))
						&& TicketUtil.isInSections(ticket, section)) {
					tmpFilteredHTickets.add(ticket);
				}
			}
			Long removeDuplicateHistoricalTicketsStartTime = System.currentTimeMillis();
			
			tmpFilteredHTickets = TicketUtil.removeDuplicateTickets(tmpFilteredHTickets, removeDuplicatePolicy);
			for (Ticket ticket: tmpFilteredHTickets) {
				if (TicketUtil.isInPriceRange(ticket, startPrice, endPrice, priceType)) {
					filteredHTickets.add(ticket);
				}
			}
			timeStats.put("removeDuplicateHistoricalTickets", System.currentTimeMillis() - removeDuplicateHistoricalTicketsStartTime);
		}
		timeStats.put("filterHistoricalTickets", System.currentTimeMillis() - filterHistoricalTicketsStartTime);		
	}
	
	public static void getFilteredTicketsForChart(Integer eventId, List<Integer> categoryIds, List<Integer> quantities,List<Ticket> allTicketsChart,String catScheme){
		Collection<Ticket> activeTickets = new ArrayList<Ticket>();
		Collection<Ticket> inActiveTickets = new ArrayList<Ticket>();
		//get particular event
		Event event = DAORegistry.getEventDAO().get(eventId);
		//get All categories for particular event
//		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), catScheme);
		Collection<Category> categories = null;
		if(event.getVenueCategory()!=null){
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
		}else{
			categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
		}
		//get All active Tickets of this event
		activeTickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);	
		inActiveTickets = DAORegistry.getTicketDAO().getAllCompletedTicketsByEvent(eventId);
		activeTickets.addAll(inActiveTickets);
		TicketUtil.preAssignCategoriesToTickets(activeTickets, categories);
		activeTickets = TicketUtil.filterCategories(categoryIds, activeTickets);
		if(activeTickets != null && !activeTickets.isEmpty()){
			allTicketsChart.addAll(activeTickets);	
		}
		
	}	
	public static List<Integer> getMatchingQuantities(int quantity) {
		List<Integer> result = new ArrayList<Integer>();
		
		result.add(quantity);
		
		for (int q = quantity + 2; q < MAX_QTY; q++) {
			result.add(q);
		}
		
		return result;
	}
	
	public String[] getRankedSiteIds() {
		return rankedSiteIds;
	}
	/*public static void createGroup(Integer eventId,ServletContext context,HttpSession session,Collection<Ticket> nonAOTickets){
		try{
			    context.setAttribute(eventId+"isRunning", Boolean.TRUE);
    			System.out.println("Locked : " + eventId);
    			Long startTime = System.currentTimeMillis();
    			EbayInventoryEvent event= DAORegistry.getEbayInventoryEventDAO().getEbayInventoryEventByEventId(eventId);
    			int count=0;
    			while(event==null && count < 2){
    				Thread.sleep(5000);
    				event = DAORegistry.getEbayInventoryEventDAO().getEbayInventoryEventByEventId(eventId);
    				count ++;
    			}
    			
    			DAORegistry.getEbayInventoryDAO().deleteEbayInventoryByEventId(eventId);
    			DAORegistry.getEbayInventoryGroupDAO().deleteByEventId(eventId);
    			
    			if(event==null){
    				event = new EbayInventoryEvent();
    				event.setNotes(null);
    				event.setExposure(DEFAULT_EXPOSOR);
    				event.setId(eventId);
    				event.setCreationDate(new Date());
    				event.setRptFactor(DEFAULT_RPT_FACTOR);
    				//event.setShipping(DEFAULT_SHIPPING_CHARG);
    				//event.setSellQuantity(DEFAULT_SELL_QTY);
    				
    				//event.setMarkup(DEFAULT_MARK_UP);
    				DAORegistry.getEbayInventoryEventDAO().saveOrUpdate(event);
    				
    			}
        			System.out.println("Start Time of categorizer:"+startTime);
      			 ///ticketGroupingManager.initializeTicketGroupingForEvent(eventId);
        			Collection<EbayInventoryGroup> ebayInventoryGroupList = new ArrayList<EbayInventoryGroup>();
        			ebayInventoryGroupList = ticketGroupingManager.groupTicketsForEvent(event,session.getId(),nonAOTickets);
        			
        			Collection<EbayInventory> inventoryList = new ArrayList<EbayInventory>();
        			if(ebayInventoryGroupList!= null){
        				for(EbayInventoryGroup ebayInGroup : ebayInventoryGroupList){
            				ebayInGroup.setStatus(EbayInventoryStatus.ACTIVE);
            				DAORegistry.getEbayInventoryGroupDAO().saveOrUpdate(ebayInGroup);
            				Collection<EbayInventory> list = new ArrayList<EbayInventory>();
            				for (EbayInventory inventory: ebayInGroup.getEbayInventories()) {
            						inventory.setGroupId(ebayInGroup.getId());            						
            						list.add(inventory);
            						ebayInGroup.setEbayInventories(list);
            				}
            				inventoryList.addAll(list);
            			}
        				DAORegistry.getEbayInventoryDAO().saveAll(inventoryList);
        			}
        			
        		
        		 context.setAttribute(eventId+"groups",ebayInventoryGroupList);
    			 context.setAttribute(eventId+"isRunning", Boolean.FALSE);
    			 context.setAttribute("lastUpdateTime-"+eventId,new Date().getTime());
    			 System.out.println("Released : " + eventId);
    		 	System.out.println("Time took for grouping and saving the groups in seconds" + (System.currentTimeMillis() - startTime) / 1000);
//    		
			
			
		}catch(Exception e){
			 context.setAttribute(eventId+"isRunning", Boolean.FALSE);
			 System.out.println("Released : " + eventId);
			System.err.println(e.fillInStackTrace());
		}	
    	
	}*/
	
	
	/*public static List<EbayInventoryGroup> calculateGroup(Integer eventId,HttpSession session,List<Integer>quantities,Double startPrice,Double endPrice,Collection<Ticket> nonAOTickets){
//		Boolean result = Boolean.TRUE;
		ServletContext context = session.getServletContext();
		try{
			Boolean temp=(Boolean)context.getAttribute(session+"isRunning");
    		while(temp!=null && temp){
    			Thread.sleep(5000);
    			temp=(Boolean)context.getAttribute(session+"isRunning");
    		}
//		System.out.println("Locking :" + tmatEventId);			
		Long time = context.getAttribute("lastUpdateTime-"+eventId)!=null ? (Long)context.getAttribute("lastUpdateTime-"+eventId):null;
		Collection<EbayInventoryGroup> groups = null;
		Collection<Category> zoneCategories = new ArrayList<Category>();
		Collection<Integer> zoneCategoryIds = new ArrayList<Integer>();
		List<String> zoneCatGroups = new ArrayList<String>();
		groups =  (List<EbayInventoryGroup> ) context.getAttribute(eventId+"groups");
		if(time == null || (new Date().getTime()-time>=CREATE_GROUP_TIME_DURATION) || (groups==null ||groups.isEmpty())){
			createGroup(eventId,context,session,nonAOTickets);	
			context.setAttribute("lastUpdateTime-"+eventId,new Date().getTime());
			groups = DAORegistry.getEbayInventoryGroupDAO().getAllEbayInventoryGroupsByEventId(eventId);

		} else {
			  context.setAttribute(eventId+"isRunning", Boolean.FALSE);			 
		}
		 groups = (List<EbayInventoryGroup> ) context.getAttribute(eventId+"groups");
		 if(groups==null || groups.isEmpty()){
			 return null;
		 }else{
			 Iterator<EbayInventoryGroup> iter =groups.iterator();
			 session.setAttribute("tempZoneCatScheme", iter.next().getCategory());
		 }
		
		List<EbayInventoryGroup> newGroups = new ArrayList<EbayInventoryGroup>();
			for (EbayInventoryGroup eig : groups) {
		       if(quantities!=null && !quantities.isEmpty() && !quantities.contains(eig.getQuantity())){		    		
		    	   
		    	   continue;
		       }
		     
		       if(eig.getEbayInventories()==null || eig.getEbayInventories().isEmpty()){
		    	   continue;
		       }
		    	 Double total = eig.getEbayInventoryPrice() * eig.getQuantity();
		         
		      
		      if((endPrice!=null && total>endPrice )|| (startPrice!=null && total<startPrice)){
		    	  //groups.remove(eig);
		    	  continue;
		      }
		      if(!zoneCatGroups.contains(eig.getCategory())){		    	  
		    	  zoneCatGroups.add(eig.getCategory());
		      }
		      
		      if(!zoneCategoryIds.contains(eig.getEbayInventory1().getTicket().getCategory().getId())){
		    	  zoneCategories.add(eig.getEbayInventory1().getTicket().getCategory()); 
		    	  zoneCategoryIds.add(eig.getEbayInventory1().getTicket().getCategory().getId());
		      }
		    		      
		      newGroups.add(eig); 
			}
			
			if(zoneCatGroups!=null && !zoneCatGroups.isEmpty()){
				Collections.sort(zoneCatGroups);
			}
			
			context.setAttribute(eventId+"isRunning", Boolean.FALSE);
			session.setAttribute("zoneCategories", zoneCategories);
			session.setAttribute("tempZoneCatGroups", zoneCatGroups);
			
			
			
			StringBuffer zoneCategorySymbols = new StringBuffer();
			if(zoneCategories!=null && !zoneCategories.isEmpty()){
				for(Category zoneCat:zoneCategories){
					zoneCategorySymbols.append(","+zoneCat);
				}				
				session.setAttribute("tempZoneCategorySymbols", zoneCategorySymbols.substring(1).toString());
				
			}else{
				session.setAttribute("tempZoneCategorySymbols", "");
				
			}
						
			return newGroups;
			
		
	}catch (Exception e) {
		e.printStackTrace();
		  context.setAttribute(eventId+"isRunning", Boolean.FALSE);
		 return null ;
	}
}*/
	public static void preAssignCategoriesToTickets(Collection<Ticket> tickets,Event event, Collection<Category> categories, String scheme) {
		Long startTime = System.currentTimeMillis();
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		
		for (Category category: categories) {
			categoryMap.put(category.getId(), category);
		}	
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getCategoryId(), list);
		}
		for (Ticket ticket: tickets) {
			
			Integer catId = null;
			Category cat = Categorizer.computeCategory(event.getVenueCategory(),ticket.getNormalizedSection(),ticket.getNormalizedRow(),ticket.getNormalizedSeat(),categoryMap,catMappingMap);
			if(cat!=null){
				catId = cat.getId();
			}else{
				catId= 0;
			}
			logger.info("event id is: " + ticket.getEventId() + "computing the categories for the ticket :"+ticket.getId()+" cat for the same :"+catId);
			logger.info("The category object is: " + categoryMap.get(catId));
			ticket.setCategory(categoryMap.get(catId));
		}
		
		System.out.println("Time Taken For preAssignCategoriesToTickets() =" + (System.currentTimeMillis() - startTime) / 1000);
	}
	

	
	
	/**
	 * orderCatMaps - THIS METHOD MUTATES THE PARAMETER catMaps
	 * 
	 * This method will mutate the parameter List statuses and 
	 * order it by:
	 * a.) Worse(newer/lower numbered) category ids before better category ids
	 *
	 * @param catMaps - List of CatMaps to be ordered
	 * @return void 
	 */
	public static void orderCatMaps(List<CategoryMapping> catMaps){
		
		try {
			catMaps.sort( new Comparator<CategoryMapping>() {
			@Override
			public int compare(CategoryMapping catMap1, CategoryMapping catMap2) {
				if(catMap1.getCategoryId() < catMap2.getCategoryId()) {
					return 1;
				} else if(catMap1.getCategoryId() > catMap2.getCategoryId()) {
					return -1;
				}
				return 0;
			}
		});
		}catch(Exception ex){
			 System.out.println("Exception in Sorting TicketUtil line no 3299 " + ex);
		}
		// sort = "external";
	}
	
public static ArrayList<Ticket> viewCaterizedCheapestTickets(Collection<Ticket> filteredTickets){
		
		HashMap<Integer, Ticket> tempTicketMap = new HashMap<Integer, Ticket>();
		Ticket tempTicketArray = new Ticket();
		
		for(Ticket tickets : filteredTickets){
			tempTicketArray = tickets;
			if(tickets.getCategory() != null && tickets.getCategory().getSymbol() != null && tickets.getQuantity() != null){
				for(Ticket tempTickets : filteredTickets){
					if((tempTickets.getCategory() != null && tempTickets.getCategory().getSymbol() != null && tempTickets.getQuantity() != null) && 
							(tempTickets.getCategory().getSymbol().equalsIgnoreCase(tempTicketArray.getCategory().getSymbol())) && 
							(tempTickets.getQuantity().equals(tempTicketArray.getQuantity())) && (tempTickets.getBuyItNowPrice() <= tempTicketArray.getBuyItNowPrice())){
						tempTicketArray = tempTickets;
					}
					
				}
				if(!tempTicketMap.containsKey(tempTicketArray.getId())){
					tempTicketMap.put(tempTicketArray.getId(), tempTicketArray);
				}				
			}
		}
		ArrayList<Ticket> returnedList = new ArrayList<Ticket>(tempTicketMap.values());
		return returnedList;
		
	}
	
	public static ArrayList<Ticket> excludeCaterizedCheapestTickets(Collection<Ticket> filteredTickets){
		
		HashMap<Integer, Ticket> tempTicketMap = new HashMap<Integer, Ticket>();
		Ticket tempTicketArray = new Ticket();
		
		for(Ticket tickets : filteredTickets){
			tempTicketArray = tickets;
			if(tickets.getCategory() != null && tickets.getCategory().getSymbol() != null && tickets.getQuantity() != null){
				for(Ticket tempTickets : filteredTickets){
					if((tempTickets.getCategory() != null && tempTickets.getCategory().getSymbol() != null && tempTickets.getQuantity() != null) && 
							(tempTickets.getCategory().getSymbol().equalsIgnoreCase(tempTicketArray.getCategory().getSymbol())) && 
							(tempTickets.getQuantity().equals(tempTicketArray.getQuantity())) && (tempTickets.getBuyItNowPrice() <= tempTicketArray.getBuyItNowPrice())){
						tempTicketArray = tempTickets;
					}
					
				}
				if(!tempTicketMap.containsKey(tempTicketArray.getId())){
					tempTicketMap.put(tempTicketArray.getId(), tempTicketArray);
				}				
			}
		}
		ArrayList<Ticket> returnedList = new ArrayList<Ticket>(tempTicketMap.values());
		ArrayList<Ticket> excludeCheapestTicketList = new ArrayList<Ticket>();
		boolean flag = true;
		for(Ticket tickets : filteredTickets){
			for(Ticket FilteredTicket : returnedList){
				
				if(tickets.getId().equals(FilteredTicket.getId())){
					flag = false;
				}
			}
			if(flag){
				excludeCheapestTicketList.add(tickets);
			}
			flag = true;
		}
		
		return excludeCheapestTicketList;
		
	}
	
	
	
	public static ArrayList<CheapestZone> getCaterizedCheapestTickets(ArrayList<Ticket> tickets, EventBookmark event){
			
		boolean cnt=false;
		double highPrice=0;
		String symbol=null;
		Map<Integer,Ticket> finalMap = new HashMap<Integer, Ticket>();
		List<Ticket> ticketList = null;
		
		for(Ticket ticket: tickets){
			if(ticket.getCategory() == null){
				continue;
			}
			
			if(finalMap != null && !finalMap.isEmpty()){
				for(Ticket t:finalMap.values()){
					if(ticket.getCategory().getSymbol().equals(t.getCategory().getSymbol())){
						cnt= true;
						break;
					}	
				}
			}
			if(cnt){
				cnt=false;
				continue;
			}
			ticketList = new ArrayList<Ticket>();
			if(ticket.getCategory().getSymbol() != null){
				symbol = ticket.getCategory().getSymbol();
				for(Ticket tic:tickets){
					if(tic.getCategory() != null && tic.getCategory().getSymbol() != null){
						if(ticket.getId().equals(tic.getId())){
							continue;
						}
						if(symbol.equalsIgnoreCase(tic.getCategory().getSymbol())){
							ticketList.add(tic);
						}
					}
					
				}
				
				try {
				ticketList.sort( new Comparator<Ticket>() {
					@Override
					public int compare(Ticket t1,Ticket t2){
						return t1.getPurchasePrice().compareTo(t2.getPurchasePrice());
					}
				});
			}catch(Exception ex){
				 System.out.println("Exception in Sorting TicketUtil line no 3428 " + ex);
			}
				
				if(!finalMap.containsKey(ticketList.get(0).getId())){
					finalMap.put(ticketList.get(0).getId(),ticketList.get(0));
				}
				
				double pPrice =ticketList.get(0).getPurchasePrice();
				highPrice = pPrice + ((pPrice*20)/100);
					
				for(Ticket tix:ticketList){
					if(tix.getPurchasePrice() >= highPrice){
						if(!finalMap.containsKey(tix.getId())){
							finalMap.put(tix.getId(),tix);
							break;
						}
							
						
					}
				}
			}
				
		}
		/*for(Ticket t : finalMap.values()){
			System.out.println("cat="+t.getCategory().getSymbol()+"====quntity===="+t.getQuantity()+"====price===="+t.getPurchasePrice());
		}
		System.out.println("******************************************");*/
		
		List<Ticket> finalList = new ArrayList<Ticket>(finalMap.values());
		List<CheapestZone> categoryList = new ArrayList<CheapestZone>();
		List<CheapestZone> eventList = new ArrayList<CheapestZone>();
		
		try {
		finalList.sort( new Comparator<Ticket>() {
			
			public int compare(Ticket t1,Ticket t2){
				return t1.getCategory().getSymbol().compareTo(t2.getCategory().getSymbol());
			}
		});
		}catch(Exception ex){
		 System.out.println("Exception in Sorting TicketUtil line no 3461 " + ex);
		}
		
		int i=0;
		double diff=0;
		int size = finalList.size();
		boolean isTwo =false;
		while(i < size-2){
			CheapestZone zone = new CheapestZone();
			List<CheapestZone> tic = new ArrayList<CheapestZone>();
			if(finalList.get(i).getCategory().getSymbol().equalsIgnoreCase
					(finalList.get(i+1).getCategory().getSymbol())){
				if(finalList.get(i).getPurchasePrice() < finalList.get(i+1).getPurchasePrice()){
					zone.setQuantity(finalList.get(i).getQuantity());
					zone.setSection(finalList.get(i).getSection());
					zone.setRow(finalList.get(i).getRow());
					zone.setOnlinePrice(finalList.get(i).getBuyItNowPrice());
					zone.setPurchasePrice(finalList.get(i).getPurchasePrice());
					zone.setSeller(finalList.get(i).getSeller());
					zone.setSiteId(finalList.get(i).getSiteId());
					zone.setNextPurchasePrice(finalList.get(i+1).getPurchasePrice());
					zone.setNextSection(finalList.get(i+1).getSection());
					zone.setNextRow(finalList.get(i+1).getRow());
					zone.setNextQuantity(finalList.get(i+1).getQuantity());
					diff = finalList.get(i+1).getPurchasePrice()-finalList.get(i).getPurchasePrice();
					zone.setDifference((diff/finalList.get(i).getPurchasePrice())*100);
					isTwo = true;
					
				}else{
					zone.setQuantity(finalList.get(i+1).getQuantity());
					zone.setSection(finalList.get(i+1).getSection());
					zone.setRow(finalList.get(i+1).getRow());
					zone.setOnlinePrice(finalList.get(i+1).getBuyItNowPrice());
					zone.setPurchasePrice(finalList.get(i+1).getPurchasePrice());
					zone.setSeller(finalList.get(i+1).getSeller());
					zone.setSiteId(finalList.get(i+1).getSiteId());
					zone.setNextPurchasePrice(finalList.get(i).getPurchasePrice());
					zone.setNextSection(finalList.get(i).getSection());
					zone.setNextRow(finalList.get(i).getRow());
					zone.setNextQuantity(finalList.get(i).getQuantity());
					diff = finalList.get(i).getPurchasePrice()-finalList.get(i+1).getPurchasePrice();
					zone.setDifference((diff/finalList.get(i+1).getPurchasePrice())*100);
					isTwo = true;
				}
			}else{
				zone.setQuantity(finalList.get(i).getQuantity());
				zone.setSection(finalList.get(i).getSection());
				zone.setRow(finalList.get(i).getRow());
				zone.setOnlinePrice(finalList.get(i).getBuyItNowPrice());
				zone.setPurchasePrice(finalList.get(i).getPurchasePrice());
				zone.setSeller(finalList.get(i).getSeller());
				zone.setSiteId(finalList.get(i).getSiteId());
			}
			tic.add(zone);
			CheapestZone catZone = new CheapestZone();
			catZone.setCategoryId(finalList.get(i).getCategory().getId());
			catZone.setCategory(finalList.get(i).getCategory().getDescription());
			catZone.setChildren(tic);
			categoryList.add(catZone);
			if(isTwo){
				i = i + 2;
				isTwo = false;
			}else{
				i = i + 1;
			}
		}
		CheapestZone zone = new CheapestZone();
		if(finalList.get(size-2).getCategory().getSymbol().equalsIgnoreCase
				(finalList.get(size-1).getCategory().getSymbol())){
			if(finalList.get(size-2).getPurchasePrice() < finalList.get(size-1).getPurchasePrice()){
				zone.setQuantity(finalList.get(size-2).getQuantity());
				zone.setSection(finalList.get(size-2).getSection());
				zone.setRow(finalList.get(size-2).getRow());
				zone.setOnlinePrice(finalList.get(size-2).getBuyItNowPrice());
				zone.setPurchasePrice(finalList.get(size-2).getPurchasePrice());
				zone.setSeller(finalList.get(size-2).getSeller());
				zone.setSiteId(finalList.get(size-2).getSiteId());
				zone.setNextPurchasePrice(finalList.get(size-1).getPurchasePrice());
				zone.setNextSection(finalList.get(size-1).getSection());
				zone.setNextRow(finalList.get(size-1).getRow());
				zone.setNextQuantity(finalList.get(size-1).getQuantity());
				diff = finalList.get(size-1).getPurchasePrice()-finalList.get(size-2).getPurchasePrice();
				zone.setDifference((diff/finalList.get(i).getPurchasePrice())*100);
			}else{
				zone.setQuantity(finalList.get(size-1).getQuantity());
				zone.setSection(finalList.get(size-1).getSection());
				zone.setRow(finalList.get(size-1).getRow());
				zone.setOnlinePrice(finalList.get(size-1).getBuyItNowPrice());
				zone.setPurchasePrice(finalList.get(size-1).getPurchasePrice());
				zone.setSeller(finalList.get(size-1).getSeller());
				zone.setSiteId(finalList.get(size-1).getSiteId());
				zone.setNextPurchasePrice(finalList.get(size-2).getPurchasePrice());
				zone.setNextSection(finalList.get(size-2).getSection());
				zone.setNextRow(finalList.get(size-2).getRow());
				zone.setNextQuantity(finalList.get(size-2).getQuantity());
				diff = finalList.get(size-2).getPurchasePrice()-finalList.get(size-1).getPurchasePrice();
				zone.setDifference((diff/finalList.get(i+1).getPurchasePrice())*100);
			}
		}else{
			zone.setQuantity(finalList.get(size-2).getQuantity());
			zone.setSection(finalList.get(size-2).getSection());
			zone.setRow(finalList.get(size-2).getRow());
			zone.setOnlinePrice(finalList.get(size-2).getBuyItNowPrice());
			zone.setPurchasePrice(finalList.get(size-2).getPurchasePrice());
			zone.setSeller(finalList.get(size-2).getSeller());
			zone.setSiteId(finalList.get(size-2).getSiteId());
		}
		List<CheapestZone> tic = new ArrayList<CheapestZone>();
		tic.add(zone);
		CheapestZone catZone = new CheapestZone();
		catZone.setCategoryId(finalList.get(i).getCategory().getId());
		catZone.setCategory(finalList.get(i).getCategory().getDescription());
		catZone.setChildren(tic);
		categoryList.add(catZone);
		CheapestZone eventZone = new CheapestZone();
		eventZone.setEventId(event.getEvent().getId());
		eventZone.setEventName(event.getEvent().getName());
		eventZone.setEventDate(event.getEvent().getFormatedDate());
		eventZone.setVenue(event.getEvent().getVenue().getBuilding());
		eventList.add(eventZone);
		eventZone.setChildren(categoryList);
	
		return (ArrayList<CheapestZone>) eventList;
		
	}
	
	public static Map<Integer,List<Ticket>> getTicketMapByZones(Collection<Ticket> tickets,Collection<Category> categories){

		Map<Integer,List<Ticket>> ticketByZoneMap = new TreeMap<Integer, List<Ticket>>();
		preAssignCategoriesToTickets(tickets, categories);
		for(Ticket ticket:tickets){
			Category cat = ticket.getCategory();
			if(cat==null){
				List<Ticket> tix = ticketByZoneMap.get(0);
				if(tix==null){
					tix = new ArrayList<Ticket>();
				}
				tix.add(ticket);
				ticketByZoneMap.put(0, tix);
			}else{
				List<Ticket> tix = ticketByZoneMap.get(cat.getId());
				if(tix==null){
					tix = new ArrayList<Ticket>();
				}
				tix.add(ticket);
				ticketByZoneMap.put(cat.getId(), tix);
			}
		}
		return ticketByZoneMap;
	}
	
	public static Collection<Ticket> uncategorizedTickets(Collection<Ticket> tickets,Event event, Collection<Category> categories){
		Long startTime = System.currentTimeMillis();
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		
		for (Category category: categories) {
			categoryMap.put(category.getId(), category);
		}
//		for (Ticket ticket: tickets) {
		
			//Call method for - computing categories of each tickets
			try {
				tickets = Categorizer.unCategorizedTickets(tickets, categoryMap,event);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
//		}
		
		System.out.println("Time Taken For preAssignCategoriesToTickets() =" + (System.currentTimeMillis() - startTime) / 1000);
		return tickets;
	}
	
	public static void getPurchasePrice(Ticket t,Map<String, DefaultPurchasePrice> defaultPurchasePriceMap,Integer lotSize,Map<String, ManagePurchasePrice> tourPriceMap){
		Double tempPrice = t.getCurrentPrice();
		if (lotSize==null || lotSize==0){
			lotSize = t.getQuantity();
		}
//		Event event = t.getEvent();
//		Map<String, ManageTourPrice> tourPriceMap = new HashMap<String, ManageTourPrice>();
//		Long time = manageTourPriceTimeMap.get(tour.getTmatTourId());
//		if(tourPriceMap==null){
//			tourPriceMap= new HashMap<String, ManageTourPrice>();
//		}
		String mapKey = t.getSiteId()+ "-" + (t.getTicketDeliveryType()==null?"REGULAR":t.getTicketDeliveryType());
//		Long now =new Date().getTime();
//		if(tourPriceMap.get(mapKey)==null || now-time>60*60*1000){
//			System.out.println("DB called for manage price stated @ " + new Date());
//			for(ManageTourPrice tourPrice:manageTourPricelist){
//				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
//			}
//			System.out.println("DB called for manage price ended @ " + new Date());
//			manageTourPriceMap.put(tour.getTmatTourId(),tourPriceMap);
//			manageTourPriceTimeMap.put(tour.getTmatTourId(),now);
//		}
		ManagePurchasePrice managePurchasePrice=tourPriceMap.get(mapKey);
		
		if(managePurchasePrice!=null){
			double serviceFee=managePurchasePrice.getServiceFee();
			double  shippingFee=managePurchasePrice.getShipping();
			int currencyType=managePurchasePrice.getCurrencyType();
			tempPrice=(currencyType==1?((lotSize * tempPrice*(1+serviceFee/100)) + shippingFee)/lotSize:((lotSize * tempPrice) + serviceFee + shippingFee)/lotSize);
		}else{
			DefaultPurchasePrice defaultPurchasePrice = defaultPurchasePriceMap.get(mapKey);
			if(defaultPurchasePrice!=null){
				double serviceFee=defaultPurchasePrice.getServiceFees();
				double  shippingFee=defaultPurchasePrice.getShipping();
				int currencyType=defaultPurchasePrice.getCurrencyType();
				tempPrice=(currencyType==1?((lotSize * tempPrice*(1+serviceFee/100)) + shippingFee)/lotSize:((lotSize * tempPrice) + serviceFee + shippingFee)/lotSize);
			}
		}
		t.setPurchasePrice(tempPrice);
	}
public static void main(String[] args) {
	String text = "The A Book of mormon";
	text = text.replaceFirst("^The ", "").replaceFirst("^A ", "");
	//text = text.replaceFirst("^A ", "");
	System.out.println("Text :"+text);
}	
}

	