package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Collection;

import com.admitone.tmat.crawler.InterruptedTicketListingCrawlException;
import com.admitone.tmat.indexer.TicketHitIndexer;
import com.admitone.tmat.ticketfetcher.TicketHit;

public class ComparatorTicketHitIndexer extends TicketHitIndexer {
	private Collection<TicketHit> fetchedTicketHits;
	private Integer previewLimitSize;
	
	public ComparatorTicketHitIndexer(Integer previewLimitSize) {
		this.previewLimitSize = previewLimitSize;
		reset();
	}

	@Override
	public int indexTicketHit(TicketHit ticketHit) {
		if (previewLimitSize != null && fetchedTicketHits.size() >= previewLimitSize) {
			throw new InterruptedTicketListingCrawlException();
		}
		fetchedTicketHits.add(ticketHit);
		return NEW_ITEM;
	}
	
	public Collection<TicketHit> getFetchedTicketHits() {
		return fetchedTicketHits;
	}
	
	public void reset() {
		fetchedTicketHits = new ArrayList<TicketHit>();	
	}

}
