package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.EventStatus;

public class LatestEventCache {

	private volatile Date lastUpdate = null;
	private volatile List<Event> latestEvents = null;
	Timer timer = new Timer();

	public void refreshLatestEventsEntry() {
		// /* CS1
		 /* if(lastUpdate != null){
		 
			//Every 4 hours max
			if ((tempDate.getTime() - lastUpdate.getTime()) < 14400000) {
				return;
			}
		}
//		CS1 */
		setLastUpdate(new Date());
		
		timer.schedule(new TimerTask() {

			@Override
			public void run() {
				try {
					updateLatestEventsCacheEntry();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}	
		}, 0);
	}

	public void updateLatestEventsCacheEntry() {

		// next 10 upcoming events that have A1 inventory
		Date startDate = new Date();

		List<Event> tempEvents = new ArrayList<Event>(DAORegistry.getEventDAO().getAllEventsByDates(startDate, null, EventStatus.ACTIVE));
		List<Event> events = new ArrayList<Event>();
		for(Event event : tempEvents){
			Collection<Ticket> tempTickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(event.getId());
			TicketUtil.computeAdmitOneTickets(tempTickets,null);
			if(TicketUtil.containsAdmitOneTickets(tempTickets)){
				events.add(event);
				if (events.size() >= 10) {
					break;
				}
			}
		}
		
		latestEvents = events;
	}

	public List<Event> getLatestEvents() {
		return latestEvents;
	}

	/**
	 * @return the yesterday's date if lastUpdate is null else function will return lastUpdate. 
	 */
	public Date getLastUpdate() {
		if(lastUpdate== null){
			Calendar tempDate= Calendar.getInstance();
			tempDate.add(Calendar.DAY_OF_YEAR, -1);
			lastUpdate=tempDate.getTime();
		}
		return lastUpdate;
	}

	/**
	 * @param lastUpdate the lastUpdate to set
	 */
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}	
}

