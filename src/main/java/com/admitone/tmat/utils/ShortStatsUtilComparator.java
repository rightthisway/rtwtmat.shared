package com.admitone.tmat.utils;

import java.util.Comparator;

import com.admitone.tmat.pojo.ShortStatus;

public class ShortStatsUtilComparator implements Comparator<ShortStatus>{

	private String sortBy,sortingType;
	
	public ShortStatsUtilComparator(String sortParam,String sortingTypeParam){
		sortBy = sortParam;
		sortingType = sortingTypeParam;
	}
	
	public int compare(ShortStatus o1, ShortStatus o2) {
		
		if(sortBy.equalsIgnoreCase("Event Name")){
			if(sortingType.equals("asc")){
				if(o1.getDescription() != null && o2.getDescription() != null){
					return o1.getDescription().compareTo(o2.getDescription());
				}else{
					if(o1.getDescription() == null)
						return 1;
					else
						return -1;
				}	
			}else{
				if(o1.getDescription() != null && o2.getDescription() != null){
					return o2.getDescription().compareTo(o1.getDescription());
				}else{
					if(o1.getDescription() == null)
						return 1;
					else
						return -1;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Event Date")){
			if(sortingType.equals("asc")){
				if(o1.getDate() != null && o2.getDate() != null){
					return o1.getDate().compareTo(o2.getDate());
				}else{
					if(o1.getDate() == null)
						return 1;
					else
						return -1;
				}
			}else{
				if(o1.getDate() != null && o2.getDate() != null){
					return o2.getDate().compareTo(o1.getDate());
				}else{
					if(o1.getDate() == null)
						return 1;
					else
						return -1;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Event Time")){
			if(sortingType.equals("asc")){
				if(o1.getTime() != null && o2.getTime() != null){
					return o1.getTime().compareTo(o2.getTime());
				}else{
					if(o1.getTime() == null)
						return 1;
					else
						return -1;
				}					
			}else{
				if(o1.getTime() != null && o2.getTime() != null){
					return o2.getTime().compareTo(o1.getTime());
				}else{
					if(o1.getTime() == null)
						return 1;
					else
						return -1;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Venue")){
			if(sortingType.equals("asc")){
				if(o1.getVenue() != null && o2.getVenue() != null){
					return o1.getVenue().compareTo(o2.getVenue());
				}else{
					if(o1.getVenue() == null)
						return 1;
					else
						return -1;
				}
			}else{
				if(o1.getVenue() != null && o2.getVenue() != null){
					return o2.getVenue().compareTo(o1.getVenue());
				}else{
					if(o1.getVenue() == null)
						return 1;
					else
						return -1;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Qty")){
			if(sortingType.equals("asc")){
				return o1.getQtySold() - o2.getQtySold();
			}else{
				return o2.getQtySold() - o1.getQtySold();
			}			
		}
		else if(sortBy.equalsIgnoreCase("Cost/Revenue")){
			if(sortingType.equals("asc")){
				if(o1.getPriceSold() < o2.getPriceSold()){
					return -1;
				}else if(o1.getPriceSold() > o2.getPriceSold()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getPriceSold() < o2.getPriceSold()){
					return 1;
				}else if(o1.getPriceSold() > o2.getPriceSold()){
					return -1;
				}else{
					return 0;
				}
			}
							
		}
		else if(sortBy.equalsIgnoreCase("Cost")){
			if(sortingType.equals("asc")){
				if(o1.getPriceSold() < o2.getPriceSold()){
					return -1;
				}else if(o1.getPriceSold() > o2.getPriceSold()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getPriceSold() < o2.getPriceSold()){
					return 1;
				}else if(o1.getPriceSold() > o2.getPriceSold()){
					return -1;
				}else{
					return 0;
				}
			}
							
		}
		else if(sortBy.equalsIgnoreCase("Revenue")){
			if(sortingType.equals("asc")){
				if(o1.getPriceSold() < o2.getPriceSold()){
					return -1;
				}else if(o1.getPriceSold() > o2.getPriceSold()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getPriceSold() < o2.getPriceSold()){
					return 1;
				}else if(o1.getPriceSold() > o2.getPriceSold()){
					return -1;
				}else{
					return 0;
				}
			}
							
		}
		else if(sortBy.equalsIgnoreCase("Exposure")){
			if(sortingType.equals("asc")){
				if(o1.getExposure() < o2.getExposure()){
					return -1;
				}else if(o1.getExposure() > o2.getExposure()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getExposure() < o2.getExposure()){
					return 1;
				}else if(o1.getExposure() > o2.getExposure()){
					return -1;
				}else{
					return 0;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("PGM")){
			if(sortingType.equals("asc")){
				if(o1.getProjGrossMargin() < o2.getProjGrossMargin()){
					return -1;
				}else if(o1.getProjGrossMargin() > o2.getProjGrossMargin()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getProjGrossMargin() < o2.getProjGrossMargin()){
					return 1;
				}else if(o1.getProjGrossMargin() > o2.getProjGrossMargin()){
					return -1;
				}else{
					return 0;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Total Market Price")){
			if(sortingType.equals("asc")){
				if(o1.getTotalMinPrice() < o2.getTotalMinPrice()){
					return -1;
				}else if(o1.getTotalMinPrice() > o2.getTotalMinPrice()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getTotalMinPrice() < o2.getTotalMinPrice()){
					return 1;
				}else if(o1.getTotalMinPrice() > o2.getTotalMinPrice()){
					return -1;
				}else{
					return 0;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Market Price")){
			if(sortingType.equals("asc")){
				if(o1.getMinPrice() < o2.getMinPrice()){
					return -1;
				}else if(o1.getMinPrice() > o2.getMinPrice()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getMinPrice() < o2.getMinPrice()){
					return 1;
				}else if(o1.getMinPrice() > o2.getMinPrice()){
					return -1;
				}else{
					return 0;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Market Mover")){
			if(sortingType.equals("asc")){
				if(o1.getMarketMover() < o2.getMarketMover()){
					return -1;
				}else if(o1.getMarketMover() > o2.getMarketMover()){
					return 1;
				}else{
					return 0;
				}
			}else{
				if(o1.getMarketMover() < o2.getMarketMover()){
					return 1;
				}else if(o1.getMarketMover() > o2.getMarketMover()){
					return -1;
				}else{
					return 0;
				}
			}			
		}
		else if(sortBy.equalsIgnoreCase("Section")){
			return 0;
		}
		else if(sortBy.equalsIgnoreCase("Row")){
			return 0;
		}
		else if(sortBy.equalsIgnoreCase("Invoice")){
			return 0;
		}
		else if(sortBy.equalsIgnoreCase("Customer")){
			return 0;
		}
		
		return 0;
	}

}
