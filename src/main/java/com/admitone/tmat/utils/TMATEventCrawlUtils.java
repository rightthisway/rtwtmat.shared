package com.admitone.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.NewPOSEvents;

public class TMATEventCrawlUtils {
	
	public static StringBuffer getLatestNewPosEvents(String fromDateTimeParam) {
		StringBuffer stringBuffer = new StringBuffer();		
		try {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null == fromDateTimeParam) {
				Date date = new Date();
				date.setDate(date.getDate()-1);
				fromDateTimeParam = df.format(date)+" 00:00:00";	
			}
			
			List<NewPOSEvents> list = DAORegistry.getNewPOSEventDAO().getNewPOSEventsByDateStr(fromDateTimeParam);

			stringBuffer.append("EventId,Event Created DateAndTime,EventName,EventDate,EventTime,Venue,City,State,Parent Category,Child Category,GrandChild Category \n");
			/*df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat tf = new SimpleDateFormat("hh:mm aa");
			DateFormat dateTimeformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");*/
			
			for(NewPOSEvents event:list){
				stringBuffer.append(event.getId()  + "," + event.getCreatedDateStr() + "," + event.getName().replaceAll(",", "-") + "," + event.getDateStr()+ "," + event.getTimeStr() + "," + 
				event.getVenue().replaceAll(",", "-") +","+ event.getCity().replaceAll(",", "-") +","+ event.getState().replaceAll(",", "-") +","+ 
				event.getParentCategory().replaceAll(",", "-")+","+ event.getChildCategory().replaceAll(",", "-")+","+ event.getGrandChildCategory().replaceAll(",", "-")+"\n");
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stringBuffer;
	}

}
