package com.admitone.tmat.utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.Stat;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.enums.RemoveDuplicatePolicy;
import com.admitone.tmat.enums.TicketStatus;
import com.admitone.tmat.web.Constants;

/**
 * Helper class to compute ticket stats 
 */
public class StatHelper {
	public static final Integer CACHE_TIMEOUT = 60000;
	private static Map<String, CacheEntry> cacheMap = new HashMap<String, CacheEntry>();
	private static final Integer MAX_QTY = new Integer(11);
	
	/**
	 * getTicketCountByCategoryAndSite
	 * 
	 * Returns a map of category symbol to siteId and cat/siteId-quantity.
	 * If there are uncategorized tickets the categorySymbol is 'UNCAT'
	 * 
	 * @param tickets
	 * @return Map<catSymbol, Map<siteId, count>>
	 */
	public static Map<String, Map<String, Integer>> getTicketCountByCategoryAndSite(Collection<Ticket> tickets, String catScheme) {
		//
		// Initialize Site Stat By Category
		//
		if (tickets == null || tickets.size() == 0) {
			return null;
		}

		Event event = tickets.iterator().next().getEvent();
		
		// categoryId => category
		Map<Integer, Category> categoryMapById = new HashMap<Integer, Category>();
		
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
		
		Map<String, Map<String, Integer>> siteStatByCategory = new HashMap<String, Map<String,Integer>>();
		for (Category category: categories) {
			categoryMapById.put(category.getId(), category);
			
			Map<String, Integer> statBySite = new HashMap<String, Integer>();
			for (String siteId: Constants.getInstance().getSiteIds()) {
				statBySite.put(siteId, 0);
			}
			siteStatByCategory.put(category.getSymbol(), statBySite);
		}
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getCategoryId(), list);
		}
		// for the uncategorized
		Map<String, Integer> statBySite = new HashMap<String, Integer>();
		for (String siteId: Constants.getInstance().getSiteIds()) {
			statBySite.put(siteId, 0);
		}
		siteStatByCategory.put("UNCAT", statBySite);
//		Event event =null;
		for(Ticket ticket: tickets) {
			String cat;
			// if cat does not belong to the list of categories for the tour of the event,
			// put the ticket into the UNCAT category
			if(event==null){
				event = ticket.getEvent();
			}
			if (ticket.getCategory(event.getVenueCategory(),catScheme,categoryMapById,catMappingMap) == null) {
				cat = "UNCAT";
			} else {
				Category category = ticket.getCategory();
				if (category == null || !siteStatByCategory.containsKey(category.getSymbol())) {
					cat = "UNCAT";					
				} else {
					cat = category.getSymbol();
				}
			}
			
			siteStatByCategory.get(cat).put(ticket.getSiteId(), siteStatByCategory.get(cat).get(ticket.getSiteId()) + ticket.getRemainingQuantity());
		}
		return siteStatByCategory;
	}
	
	public static Map<String, Map<String, Integer>> getTicketEntryCountByCategoryAndQuantity(Collection<Ticket> tickets, String catScheme) {
		//
		// Initialize Quantity Stat By Category
		//
		if (tickets == null || tickets.size() == 0) {
			return null;
		}
		Event event = tickets.iterator().next().getEvent();
		Map<String, Map<String, Integer>> quantityStatByCategory = new HashMap<String, Map<String,Integer>>();
		Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catScheme);
		Map<Integer,Category> catMap = new HashMap<Integer, Category>();
		for(Category category: categories) {
			Map<String, Integer> statByQuantity = new HashMap<String, Integer>();
			for (int qty = 1 ; qty <= 11 ; qty++) {
				String qtyString;
				if (qty <= 10) {
					qtyString = Integer.toString(qty);
				} else {
					qtyString = "11+";
				}
				statByQuantity.put(qtyString, 0);
			}
			quantityStatByCategory.put(category.getSymbol(), statByQuantity);
			catMap.put(category.getId(), category);
		}
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getCategoryId(), list);
		}
		// for the uncategorized
		Map<String, Integer> statByQuantity = new HashMap<String, Integer>();
		for (int qty = 1 ; qty <= 11 ; qty++) {
			String qtyString;
			if (qty <= 10) {
				qtyString = Integer.toString(qty);
			} else {
				qtyString = "11+";
			}
			statByQuantity.put(qtyString, 0);
		}
		quantityStatByCategory.put("UNCAT", statByQuantity);
//		Event event = null;
		for(Ticket ticket: tickets) {
//			if(event==null){
//				event = ticket.getEvent();
//			}
			String cat;
			// if cat does not belong to the list of categories for the tour of the event,
			// put the ticket into the UNCAT category
			if (ticket.getCategory(event.getVenueCategory(),catScheme,catMap,catMappingMap) == null || !quantityStatByCategory.containsKey(ticket.getCategory().getSymbol())) {
				cat = "UNCAT";
			} else {
				cat = ticket.getCategory().getSymbol();
			}
			
			String qty;
			if (ticket.getRemainingQuantity() <= 10) {
				qty = Integer.toString(ticket.getRemainingQuantity());
			} else {
				qty = "11+";
			}

			quantityStatByCategory.get(cat).put(qty, quantityStatByCategory.get(cat).get(qty) + 1);
		}
		return quantityStatByCategory;
	}
	
	public static Map<String, Map<String, Integer>> getTicketEntryCountBySiteAndQuantity(Collection<Ticket> tickets) {
		//
		// Initialize Quantity Stat By SiteId
		//

		if (tickets == null || tickets.size() == 0) {
			return null;
		}

		Map<String, Map<String, Integer>> quantityStatBySiteId = new HashMap<String, Map<String,Integer>>();				
		for(String siteId: Constants.getInstance().getSiteIds()) {
			Map<String, Integer> statByQuantity = new HashMap<String, Integer>();
			for (int qty = 1 ; qty <= 11 ; qty++) {
				String qtyString;
				if (qty <= 10) {
					qtyString = Integer.toString(qty);
				} else {
					qtyString = "11+";
				}
				statByQuantity.put(qtyString, 0);
			}
			quantityStatBySiteId.put(siteId, statByQuantity);
		}				
		try{
		for(Ticket ticket: tickets) {
			String qty;
			if (ticket.getRemainingQuantity() <= 10) {
				qty = Integer.toString(ticket.getRemainingQuantity());
			} else {
				qty = "11+";
			}
			quantityStatBySiteId.get(ticket.getSiteId()).put(qty, quantityStatBySiteId.get(ticket.getSiteId()).get(qty) + 1);
		}
		}
		catch(NullPointerException npe){}
		catch(Exception e){
			e.printStackTrace();
		}
		return quantityStatBySiteId;
	}
	
	/**
	 * getTicketCountByExactMatchAndSite
	 * 
	 * Tickets that exactly match section and row.
	 * 
	 * @param tickets
	 * @param section
	 * @param row
	 * @return Map<siteId, count>
	 */
	public static Map<String, Integer> getTicketCountByExactMatchAndSite(Collection<Ticket> tickets, String section, String row) {
		
		Map<String, Integer> siteToQty = new HashMap<String, Integer>();
		
		for (String siteId: Constants.getInstance().getSiteIds()) {
			siteToQty.put(siteId, new Integer(0));
		}
		
		if(tickets != null && !tickets.isEmpty()){
			for(Ticket ticket : tickets) {
				String ticketSection = ticket.getNormalizedSection();
				if(ticketSection == null){
					ticketSection = ticket.getSection();
				}
				if(ticketSection != null && row != null && ticketSection.equals(section) 
						&& ticket.getNormalizedRow().equals(row)){
					Integer currQty = siteToQty.get(ticket.getSiteId());
					Integer newQty = new Integer(currQty.intValue() + ticket.getRemainingQuantity().intValue());
					siteToQty.put(ticket.getSiteId(), newQty);
				}
			}
		}
		return siteToQty;
	}
	
	/**
	 * getTicketCountBySite
	 * 
	 * @param tickets
	 * @param section
	 * @param row
	 * @return Map<siteId, count>
	 */
	public static Map<String, Integer> getTicketCountBySite(Collection<Ticket> tickets) {
		
		Map<String, Integer> siteToQty = new HashMap<String, Integer>();
		
		for (String siteId: Constants.getInstance().getSiteIds()) {
			siteToQty.put(siteId, new Integer(0));
		}
		
		if(tickets != null){
			for(Ticket ticket : tickets) {
				Integer currQty = siteToQty.get(ticket.getSiteId());
				Integer newQty = new Integer(currQty.intValue() + ticket.getRemainingQuantity().intValue());
				siteToQty.put(ticket.getSiteId(), newQty);
			}
		}
		return siteToQty;
	}

	@SuppressWarnings("unchecked")
	public static Stat getEventCategoryStat(Integer eventId, Integer categoryId, Date date) {
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllTicketsByEventAndDate(eventId, date);

		tickets = TicketUtil.filterCategory(categoryId, tickets);
		Collection<Ticket> htickets = DAORegistry.getHistoricalTicketDAO().getAllTicketsByEventAndDate(eventId, date);

		tickets = mergeTickets(tickets, htickets);
			
		if (tickets.size() == 0) {
			return null;
		}
		Map<String,ManagePurchasePrice> purchasePriceMap= null;
		Stat stat = getStat(tickets,purchasePriceMap);
		stat.setDate(date);
		return stat;
	}

	@SuppressWarnings("unchecked")
	public static Stat getEventCategorySiteQuantityLotStat(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, RemoveDuplicatePolicy removeDuplicatePolicy, Date date, String catScheme) {
		return getEventCategorySiteQuantityLotStat(eventId, categoryId, siteIds, quantity, lotSize, null, null, date, removeDuplicatePolicy, true, catScheme);
	}

	@SuppressWarnings("unchecked")
	public static Stat getEventCategorySiteQuantityLotStatActive(Integer eventId, Integer categoryId, String section, String row, String[] siteIds, Integer quantity, 
			Integer lotSize, RemoveDuplicatePolicy removeDuplicatePolicy, String catScheme,Map<String, ManagePurchasePrice> purchasePriceMap) {
		return getEventCategorySiteQuantityLotStat(eventId, categoryId, siteIds, quantity, lotSize, section, row, null, removeDuplicatePolicy, true, true, catScheme,purchasePriceMap);
	}
 
	@SuppressWarnings("unchecked")
	public static Stat getEventCategorySellerQuantityLotStatActive(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, RemoveDuplicatePolicy removeDuplicatePolicy, String catScheme) {
		return getEventCategorySiteQuantityLotStat(eventId, categoryId, siteIds, quantity, lotSize, null, null, null, removeDuplicatePolicy, true, catScheme);
	}

	@SuppressWarnings("unchecked")
	public static Stat getEventCategorySiteQuantityLotStat(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, Date date, RemoveDuplicatePolicy removeDuplicatePolicy, boolean keepOnlyActive, String catScheme) {
		return getEventCategorySiteQuantityLotStat(eventId, categoryId, siteIds, quantity, lotSize, section, row, date, removeDuplicatePolicy, keepOnlyActive, false, catScheme,null);
	}
	@SuppressWarnings("unchecked")
	public static Stat getEventCategorySiteQuantityLotStat(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, Date date, RemoveDuplicatePolicy removeDuplicatePolicy, 
			boolean keepOnlyActive, boolean removeA1Tix, String catScheme,Map<String, ManagePurchasePrice> purchasePriceMap) {
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryId, siteIds, quantity, lotSize, section, row, date, date, removeDuplicatePolicy, keepOnlyActive, removeA1Tix, catScheme);
		if (tickets == null) {
			return null;
		}
		
		if (!keepOnlyActive) {
			Collection<Ticket> htickets = DAORegistry.getHistoricalTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryId, siteIds, quantity, lotSize, section, row, date, date, removeDuplicatePolicy, removeA1Tix, catScheme);
			if (htickets != null) {
				tickets = new ArrayList<Ticket>();
				tickets.addAll(htickets);
			}
		}
		
		
		Stat stat = getStat(tickets,purchasePriceMap);
		if (stat != null){
			stat.setDate(date);
		}
		return stat;
	}

	/*
	 * @param numDays
	 * 
	 * @return Collection of Stat that has an agregate stat of all lotSizes and sellers for each day 
	 */
	public static Collection<Stat> getEventCategoryStats(Integer eventId, Integer categoryId, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		return getEventCategorySiteQuantityLotStats(eventId, categoryId, null, new Integer(0), null, null, null, removeDuplicatePolicy, startDate, numDays, catScheme);
	}	

	public static Collection<Stat> getEventCategorySiteQuantityLotStats(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		return getEventCategorySiteQuantityLotStats(eventId, categoryId, siteIds, quantity, lotSize, null, null, removeDuplicatePolicy, startDate, numDays, catScheme);
	}
	public static Collection<Stat> getEventCategorySiteQuantityLotStats(Date startDate, int numDays, Collection<Ticket> tickets, String catScheme) {
		return getEventCategorySiteQuantityLotStats(startDate,numDays,tickets,catScheme,null);
	}
	public static Collection<Stat> getEventCategorySiteQuantityLotStats(Date startDate, int numDays, Collection<Ticket> tickets, String catScheme,Calendar recentUpdateDate) {
		Calendar dayCalendar = new GregorianCalendar();
		dayCalendar.setTime(startDate);
		dayCalendar.add(Calendar.DATE, numDays);
		
		Collection<Stat> stats = new ArrayList<Stat>();
		
		if (tickets == null) {
			return null;
		}
		
		Collection<Ticket> dayTickets = new ArrayList<Ticket>();
		for (int i = 0; i < numDays; i++) {
			dayTickets.clear();
			for (Ticket ticket: tickets) {
				Calendar insertionCalendar = new GregorianCalendar();
				insertionCalendar.setTime(ticket.getInsertionDate());
				Calendar updateCalendar = new GregorianCalendar();
				updateCalendar.setTime(ticket.getLastUpdate());
/*
				if (i == 0) {
					// francois: last day - only consider active tickets
					if (ticket.getTicketStatus() != null && ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
						dayTickets.add(ticket);
					}
				} else if (compareDates(tixCalIns, cal) <= 0 && compareDates(cal, tixCalLast) <= 0) {
					// francois: other days - consider sold and active tickets

					// added an extra test to test if ticket.getTicketStatus() is not null. Sometimes it happens, don't
					// really know why...
					 //if (ticket.getTicketStatus() != null && ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
					 //}
					 
					// francois: not sure why there was this test added. commented it
					 dayTickets.add(ticket);
				}
				*/
				//System.out.println("COMPARE 1: " + compareDates(insertionCalendar, dayCalendar) + " COMPARE 2: "  + compareDates(dayCalendar, updateCalendar) + " TICKETSTATUS: " + ticket.getTicketStatus());
				// frederic: made the filtering in getStat instead, otherwise the inTicketCount and outTicketCount are
				// inconsistent.
				if(recentUpdateDate ==null){					
				
					if (!ticket.getTicketStatus().equals(TicketStatus.ACTIVE) ||  (compareDates(insertionCalendar, dayCalendar) <= 0 && compareDates(dayCalendar, updateCalendar) <= 0)) {
						//	if(!ticket.getTicketStatus().equals(TicketStatus.ACTIVE)){
							//	System.out.println("Adding Completed Ticket");
						//	}
							 dayTickets.add(ticket);					
						}
				}else{
					if (!ticket.getTicketStatus().equals(TicketStatus.ACTIVE) ||  (compareDates(insertionCalendar, dayCalendar) <= 0 && compareDates(dayCalendar, updateCalendar) <= 0)
							|| (compareDates(dayCalendar, recentUpdateDate)>0)) {
						//	if(!ticket.getTicketStatus().equals(TicketStatus.ACTIVE)){
							//	System.out.println("Adding Completed Ticket");
						//	}
							 dayTickets.add(ticket);					
						}
				}
				
			
			
			}
				
			Stat stat = getStat(dayTickets, dayCalendar.getTime(), i == 0,null);
			if(stat != null){
				stat.setDate(dayCalendar.getTime());
				if (!dayTickets.isEmpty()) {
					stats.add(stat);
				}
			}
			dayCalendar.add(Calendar.DATE, -1);
		}
		return stats;		
	}	

	// for all sellers, set it to null
	public static Collection<Stat> getEventCategorySiteQuantityLotStats(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		String key = computeCacheKey(eventId, categoryId, siteIds, quantity, lotSize, section, row, removeDuplicatePolicy, startDate, numDays, catScheme);
		Collection<Stat> cacheStats = getCacheStats(key);
		if (cacheStats != null) {
			return cacheStats;
		}
		
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryId, siteIds, quantity, lotSize,
				   section, row, null, null, removeDuplicatePolicy, false, false, catScheme);
		Collection<Ticket> htickets = DAORegistry.getTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryId, siteIds, quantity, lotSize,
				   section, row, null, null, removeDuplicatePolicy, false, false, catScheme);
		tickets = mergeTickets(tickets, htickets);
		
		return getEventCategorySiteQuantityLotStats(startDate, numDays, tickets, catScheme);
	}

	// for all sellers, set it to null
	public static Collection<Stat> getEventCategorySiteQuantityLotStats(Integer eventId, Integer categoryId, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		String key = computeCacheKey(eventId, categoryId, siteIds, quantities, lotSize, section, row, removeDuplicatePolicy, startDate, numDays, catScheme);
		Collection<Stat> cacheStats = getCacheStats(key);
		if (cacheStats != null) {
			return cacheStats;
		}
		String tempSection = section;
		if(section.contains("~")){
			tempSection = null;
		}
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryId, siteIds, new ArrayList<Integer>(), lotSize,
				   section, row, null, null, removeDuplicatePolicy, false, false, catScheme);

		Collection<Ticket> htickets = DAORegistry.getHistoricalTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryId, siteIds, new ArrayList<Integer>(), lotSize,
				   section, row, null, null, removeDuplicatePolicy, false, false, catScheme);
		
		tickets = mergeTickets(tickets, htickets);
		
		Collection<Ticket> filteredTickets = new ArrayList<Ticket>();
		if(tickets != null && !tickets.isEmpty()){
			for(Ticket ticket : tickets){
				if(section.contains("~")){
					String[] sectionRange = section.split("~");
					if(TicketUtil.inSectionRange(ticket, sectionRange[0], sectionRange[1])
							&& (quantities.isEmpty() || quantities.contains(ticket.getRemainingQuantity()) 
							|| (quantities.contains(MAX_QTY) && (ticket.getRemainingQuantity().compareTo(MAX_QTY) == 1) ))){
						filteredTickets.add(ticket);
					}
				} else {
					if(quantities.isEmpty() || quantities.contains(ticket.getRemainingQuantity()) 
							|| (quantities.contains(MAX_QTY) && (ticket.getRemainingQuantity().compareTo(MAX_QTY) == 1) )) {
						filteredTickets.add(ticket);
					}
					
				}
			}
		}
		return getEventCategorySiteQuantityLotStats(startDate, numDays, filteredTickets, catScheme);
	}

	// for all sellers, set it to null
	public static Collection<Stat> getEventCategorySiteQuantityLotStats(Integer eventId, List<Integer> categoryIds, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		String key = computeCacheKey(eventId, categoryIds, siteIds, quantities, lotSize, section, row, removeDuplicatePolicy, startDate, numDays, catScheme);
		Collection<Stat> cacheStats = getCacheStats(key);
		if (cacheStats != null) {
			return cacheStats;
		}
		
		Collection<Ticket> tickets = DAORegistry.getTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryIds, siteIds, new ArrayList<Integer>(), lotSize,
				   section, row, null, null, removeDuplicatePolicy, false, false, catScheme);

		Collection<Ticket> htickets = DAORegistry.getHistoricalTicketDAO().getEventCategorySiteQuantityLotTickets(eventId, categoryIds, siteIds, new ArrayList<Integer>(), lotSize,
				   section, row, null, null, removeDuplicatePolicy, false, false, catScheme);
		tickets = mergeTickets(tickets, htickets);
		Collection<Ticket> filteredTickets = new ArrayList<Ticket>();
		if(tickets != null && !tickets.isEmpty()){
			for(Ticket ticket : tickets){
				if(quantities.isEmpty() || quantities.contains(ticket.getRemainingQuantity()) 
						|| (quantities.contains(MAX_QTY) && (ticket.getRemainingQuantity().compareTo(MAX_QTY) == 1) )) {
					filteredTickets.add(ticket);
				}
			}
		}

		return getEventCategorySiteQuantityLotStats(startDate, numDays, filteredTickets, catScheme);
	}

	public static Stat getStat(Collection<Ticket> tickets, Date day) {
		return getStat(tickets, day, false,null);
	}
	
	public static Stat getStat(Collection<Ticket> tickets,Map<String, ManagePurchasePrice> purchasePriceMap) {
		return getStat(tickets, null, false,purchasePriceMap);
	}

	// date format: yyyy-mm-dd
	public static Stat getStat(Collection<Ticket> tickets, Date day, boolean todayStat,Map<String, ManagePurchasePrice> purchasePriceMap) {

		//System.out.println("getting stat for day: " + day + " with tickets size: " + tickets.size());

		Stat stat = new Stat();
//		System.out.println("Getting Stat: ");
		Integer endTicketCount = 0;
		
		int inTicketCount = 0;
		int outTicketCount = 0;
		
		Integer totalTickets = 0;
		Integer minQuantity = Integer.MAX_VALUE;
		Integer maxQuantity = Integer.MIN_VALUE;
		Double minPrice1 = Double.MAX_VALUE;			
		Double minPrice2 = Double.MAX_VALUE;
		Double minPrice = Double.MAX_VALUE;
		Double maxPrice = Double.MIN_VALUE;
		Double marketClosed = Double.MAX_VALUE;
		Double totalPrice = 0D;
		Integer ticketId = null;
		
		if (tickets.size() == 0) {
			return null;
		}
		
		Calendar dayCalendar = new GregorianCalendar();
		if (day != null) {
			dayCalendar.setTime(day);
		}

		for (Ticket ticket: tickets) {
		//	System.out.println("getting stat for day: " + day + " with tickets status: " + ticket.getTicketStatus());
//			System.out.println(ticket.getLastUpdate());
			Double thisTicketPrice = ticket.getBuyItNowPrice();
			if(thisTicketPrice == null){
				thisTicketPrice = ticket.getAdjustedCurrentPrice();
			}
			
			/* Purchase price calculation from online price- Begins*/
			if(null != purchasePriceMap && !purchasePriceMap.isEmpty()){
				ManagePurchasePrice managePurchasePrice =purchasePriceMap.get(ticket.getSiteId()+"-"+(null == ticket.getTicketType()?"REGULAR":ticket.getTicketType()));
				if( null != managePurchasePrice && managePurchasePrice.getCurrencyType().equals(1)){
					thisTicketPrice =(thisTicketPrice *(1+Double.valueOf(managePurchasePrice.getServiceFee())/100)) +
					(Double.valueOf(managePurchasePrice.getShipping()) /ticket.getRemainingQuantity());
				}else if(null != managePurchasePrice){
					thisTicketPrice =(thisTicketPrice + Double.valueOf(managePurchasePrice.getServiceFee())) +
					(Double.valueOf(managePurchasePrice.getShipping()) /ticket.getRemainingQuantity());
				}
			}
			/* Purchase price calculation from online price- Ends*/
			
			
			Calendar ticketInsertionCalendar = new GregorianCalendar();
			ticketInsertionCalendar.setTime(ticket.getInsertionDate());

			Calendar ticketUpdatedCalendar = new GregorianCalendar();
			ticketUpdatedCalendar.setTime(ticket.getLastUpdate());

			if (!todayStat || ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {

				//if the ticket was inserted before this day and not sold before
				if (compareDates(dayCalendar, ticketInsertionCalendar) >= 0 
						&& (ticket.getTicketStatus().equals(TicketStatus.ACTIVE) || compareDates(dayCalendar, ticketUpdatedCalendar) <= 0 )) {
					
					if(ticket.getTicketStatus().equals(TicketStatus.ACTIVE)){
						marketClosed = Math.min(marketClosed, thisTicketPrice);
					}
					endTicketCount += ticket.getRemainingQuantity();
					minQuantity = Math.min(minQuantity, ticket.getRemainingQuantity());
					maxQuantity = Math.max(maxQuantity, ticket.getRemainingQuantity());
					
					//now maxPrice is minPrice2 for History Tab					
					Double previousMinPrice1 = new Double(minPrice1);
					Double previousMinPrice2 = new Double(minPrice2);
					minPrice1 = Math.min(minPrice1, thisTicketPrice);
//					System.out.println("# "+thisTicketPrice+" #");
					if(!minPrice1.equals(previousMinPrice1)){
						minPrice2 =  previousMinPrice1;
						
					}else{
						minPrice2=   Math.min(thisTicketPrice, previousMinPrice2);
					} 
					minPrice = Math.min(minPrice, thisTicketPrice);
					maxPrice = Math.max(maxPrice, thisTicketPrice);
					totalPrice += ticket.getRemainingQuantity() * thisTicketPrice;
					totalTickets += ticket.getRemainingQuantity();
				
					if(minPrice.compareTo(thisTicketPrice) == 0){
						ticketId = ticket.getId();
					}
				}
			}
			
			
			if (compareDates(dayCalendar, ticketInsertionCalendar) == 0) {
				inTicketCount += ticket.getRemainingQuantity();
			}

			if (!ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
				if (compareDates(dayCalendar, ticketUpdatedCalendar) == 0) {
				//	System.out.println("Adding sold ticket with qty: " + ticket.getRemainingQuantity());
					outTicketCount += ticket.getRemainingQuantity();
					if (!todayStat) {
						endTicketCount -= ticket.getRemainingQuantity();
					}
				}
			}
		}
		
		if(endTicketCount == 0) {
			return null;
		}
		
		Double average = totalPrice / endTicketCount;

		// get median
		int currentQty = 0;
		Double median = 0D;
//		Double lastPrice = 0D;
		Double variance = 0D;
		String section = "";
		List<Ticket> list= new ArrayList<Ticket>();
		for (Ticket ticket: tickets) {
			
			section = ticket.getSection();
			
			Double thisTicketPrice = ticket.getBuyItNowPrice();
			if(thisTicketPrice == null){
				thisTicketPrice = ticket.getAdjustedCurrentPrice();
			}
			
			if (todayStat && !ticket.getTicketStatus().equals(TicketStatus.ACTIVE)) {
				continue;
			}
			
			list.add(ticket);
//			if (currentQty == endTicketCount / 2 && endTicketCount % 2 == 0) {
//				median = (thisTicketPrice + lastPrice) / 2;
//				break;
//			} else if (currentQty <= endTicketCount / 2 && endTicketCount / 2 < currentQty + ticket.getQuantity()) {
//				median = thisTicketPrice;
//				break;
//			}

			variance += Math.pow(thisTicketPrice - average, 2);

			currentQty += ticket.getQuantity();
//			lastPrice = thisTicketPrice;
		}
		// We are calculating median based on ticket group instead of ticket 
		
		try {			
			list.sort(new Comparator<Ticket>() {				
			@Override
			public int compare(Ticket o1, Ticket o2) {
				return o1.getBuyItNowPrice().compareTo(o2.getBuyItNowPrice());
			}
		});
		}catch(Exception ex) {
			System.out.println("Exception in sorting  StatHelper line no 687 " + ex);
		}
		int size=list.size();
		
		if(size!=0){
			if(size%2==0){
				median=list.get((size/2)-1).getBuyItNowPrice() + list.get((size/2)).getBuyItNowPrice();
			}else{
				median=list.get((size/2)).getBuyItNowPrice();
			}
		}

		variance /= endTicketCount;
		Double deviation = Math.sqrt(variance);
		
		if(minPrice2.equals(Double.MAX_VALUE)){
			minPrice2 = 0D;
		}
		if(marketClosed.equals(Double.MAX_VALUE)){
			marketClosed = 0D;
		}
		stat.setSection(section);
		stat.setAverage(average);
		stat.setStartTicketCount(endTicketCount + outTicketCount - inTicketCount);
		stat.setEndTicketCount(endTicketCount);		
		stat.setMinQuantity(minQuantity);
		stat.setMaxQuantity(maxQuantity);
		stat.setMinPrice(minPrice);
		stat.setMaxPrice(maxPrice);
		stat.setMinPrice1(minPrice1);
		stat.setMinPrice2(minPrice2);
		stat.setMarketClosed(marketClosed);
		stat.setMedian(median);
		stat.setVariance(variance);
		stat.setDeviation(deviation);
		stat.setTicketId(ticketId);
		stat.setInTicketCount(inTicketCount);
		stat.setOutTicketCount(outTicketCount);
		stat.setTotalTicketCount(totalTickets);

		return stat;
	}

	private static String computeCacheKey(Integer eventId, Integer categoryId, String[] siteIds, Integer quantity, Integer lotSize, String section, String row, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		String siteIdKeys = "";
		for (String site: siteIds) {
			siteIdKeys += siteIds + "-";
		}
		return eventId + "-" + categoryId + "-" +  siteIdKeys + "-" +  quantity + "-" + lotSize + "-" + section + row + "-" + removeDuplicatePolicy + "-" +  startDate +  "-" + numDays;		
	}
	
	private static String computeCacheKey(Integer eventId, Integer categoryId, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		String siteIdKeys = "";
		for (String site: siteIds) {
			siteIdKeys += siteIds + "-";
		}
		String qtyKeys = "";
		for (Integer qty: quantities) {
			qtyKeys += qty + "-";
		}
		return eventId + "-" + categoryId + "-" +  siteIdKeys + "-" +  qtyKeys + lotSize + "-" + section + row + "-" + removeDuplicatePolicy + "-" +  startDate +  "-" + numDays;		
	}
	
	private static String computeCacheKey(Integer eventId, List<Integer> categoryIds, String[] siteIds, List<Integer> quantities, Integer lotSize, String section, String row, RemoveDuplicatePolicy removeDuplicatePolicy, Date startDate, int numDays, String catScheme) {
		String siteIdKeys = "";
		for (String site: siteIds) {
			siteIdKeys += siteIds + "-";
		}
		String qtyKeys = "";
		for (Integer qty: quantities) {
			qtyKeys += qty + "-";
		}
		String catIdKeys = "";
		for (Integer catId: categoryIds) {
			catIdKeys += catId + "-";
		}
		return eventId + "-" + catIdKeys + "-" +  siteIdKeys + "-" +  qtyKeys + lotSize + "-" + section + row + "-" + removeDuplicatePolicy + "-" +  startDate +  "-" + numDays;		
	}
	
	private static Collection<Stat> getCacheStats(String key) {
		cleanCache();
		CacheEntry entry = cacheMap.get(key);
		if (entry == null) {
			return null;
		}
		
		return entry.getStats();
	}
	
	private static void putCache(String key, Collection<Stat> stats) {
		cacheMap.put(key, new CacheEntry(stats));
	}
	
	private static void cleanCache() {
		Date now = new Date();
		Collection<String> keys = new ArrayList<String>();
		for (Entry<String, CacheEntry> entry: cacheMap.entrySet()) {
			if (now.getTime() - entry.getValue().getDate().getTime() > CACHE_TIMEOUT) {
				keys.add(entry.getKey());
			}
		}
		
		for (String key: keys) {
			cacheMap.remove(key);
		}
	}
	
	public static class CacheEntry {
		Date date = new Date();
		Collection<Stat> stats;
		
		public CacheEntry(Collection<Stat> stats) {
			this.stats = stats;
		}
		
		public Date getDate() {
			return date;
		}
		
		public Collection<Stat> getStats() {
			return stats;
		}
	}

	private static int compareDates(Calendar cal1, Calendar cal2) {
		if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) {
			return -1;
		} else if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) {
			return 1;
		}

		if (cal1.get(Calendar.MONTH) < cal2.get(Calendar.MONTH)) {
			return -1;
		} else if (cal1.get(Calendar.MONTH) > cal2.get(Calendar.MONTH)) {
			return 1;
		}

		if (cal1.get(Calendar.DATE) < cal2.get(Calendar.DATE)) {
			return -1;
		} else if (cal1.get(Calendar.DATE) > cal2.get(Calendar.DATE)) {
			return 1;
		}
		
		return 0;
	}

	private static Collection<Ticket> mergeTickets(Collection<Ticket> tickets1, Collection<Ticket> tickets2) {
		Collection<Ticket> result = new ArrayList<Ticket>();
		
		if (tickets1 == null) {
			return tickets2;
		}
		
		if (tickets2 == null) {
			return tickets1;
		}
		
		result.addAll(tickets1);
		result.addAll(tickets2);
		return result;
	}
	
//	private Collection<Ticket> convertToTickets(Collection<HistoricalTicket> historicalTickets) {
//		
//	}
}

