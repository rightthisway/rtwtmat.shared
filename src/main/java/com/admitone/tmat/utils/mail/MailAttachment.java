package com.admitone.tmat.utils.mail;

public class MailAttachment {
	private byte[] content;
	private String mimeType;
	private String fileName;
	
	public MailAttachment(byte[] content, String mimeType, String fileName) {
		this.content = content;
		this.mimeType = mimeType;
		this.fileName = fileName;
	}
	
	public byte[] getContent() {
		return content;
	}

	public String getMimeType() {
		return mimeType;
	}

	public String getFileName() {
		return fileName;
	}	 
}