package com.admitone.tmat.utils.mail;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.ui.velocity.VelocityEngineUtils;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Property;
import com.tangosol.coherence.component.util.Collections;

public final class MailManager extends Thread{
	public final int MAX_RETRY = 3;
	public final int RETRY_WAIT = 2000;

	private static final int DEFAULT_TIMEOUT = 30000;	

	private String staticUrl;
	
	// mail in the queue
	private List<Mail> mails = new ArrayList<Mail>();;
	
	private VelocityEngine velocityEngine;

	public MailManager() {
		start();
	}
	
	public void run() {
		while(true) {
			try {
				Mail mail = null;

				synchronized (this) {					
					if (mails.size() > 0) {
						mail = mails.remove(0);
					}
				}
				
				if (mail == null) {
					Thread.sleep(5000);
					continue;
				}
				
				// we  pulled out the property from the db every time because the email properties can change
				// at any time
				// FIXME: cache the properties

				String smtpHost = DAORegistry.getPropertyDAO().get("smtp.host").getValue().trim();
				int smtpPort = Integer.parseInt(DAORegistry.getPropertyDAO().get("smtp.port").getValue().trim());
				String smtpSecureConnection = DAORegistry.getPropertyDAO().get("smtp.secure.connection").getValue();
				Boolean smtpAuth = Boolean.parseBoolean(DAORegistry.getPropertyDAO().get("smtp.auth").getValue());
				
				String username = null;
				Property smtpUserNameProperty = DAORegistry.getPropertyDAO().get("smtp.username");
				if (smtpUserNameProperty != null) {
					username = DAORegistry.getPropertyDAO().get("smtp.username").getValue();
				}
				
				String password = null;
				Property smtpUserPasswordProperty = DAORegistry.getPropertyDAO().get("smtp.password");
				if (smtpUserPasswordProperty != null) {
					password = DAORegistry.getPropertyDAO().get("smtp.password").getValue();
					if (password.isEmpty()) {
						password = null;
					}
				}
				
				String emailFrom = DAORegistry.getPropertyDAO().get("smtp.from").getValue();
				
				sendMailNow(smtpHost, smtpPort, "smtp", smtpSecureConnection, smtpAuth, username, password, 
						mail.getFromName(), emailFrom, 
						mail.getToAddress(), mail.getCcAddress(), mail.getBccAddress(), mail.getSubject(), mail.getResource(), mail.getMap(), mail.getMimeType(), mail.getAttachments());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	

	public void sendMailNow(String smtpHost, int smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType) throws Exception {
		sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, toAddress, subject, resource, map, mimeType, null);
	}
	
	private Address[] getAddresses(String addr) throws Exception {
		String[] recipients = addr.split(",");
		List<Address> addressList = new ArrayList<Address>();
		
		for (String recipient: recipients) {
			if (!recipient.isEmpty()) {
				addressList.add(new InternetAddress(recipient));
			}
		}
		return (Address[])Collections.toArray(addressList, new Address[addressList.size()]);
	}

	public void sendMailNow(String smtpHost, int smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments) throws Exception {
		sendMailNow(smtpHost, smtpPort, transportProtocol, smtpSecureConnection, smtpAuth, username, password, fromName, emailFrom, toAddress, null, null, subject, resource, map, mimeType, attachments);
	}
	
	public void sendMailNow(String smtpHost, Integer smtpPort, String transportProtocol,
			String smtpSecureConnection,
			Boolean smtpAuth, String username, String password,
			String fromName,
			String emailFrom,
			final String toAddress,
			final String ccAddress,
			final String bccAddress,
			final String subject, String resource, Map<String, Object> map, String mimeType,
			MailAttachment[] attachments) throws Exception {
		
//		System.out.println("TRYING TO SEND MAIL " + resource);
		Properties props = new Properties();
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
//		props.put("mail.transport.protocol", "smtp");
		
		
//		props.put("mail.smtp.auth", "true");

		if (smtpSecureConnection.equalsIgnoreCase("tls")) {
			props.put("mail.smtp.starttls.enable", "true");
		}
		if (smtpSecureConnection.equalsIgnoreCase("ssl")) {
			props.put("mail.smtp.ssl.enable", "true");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
			props.put("mail.smtp.socketFactory.port", smtpPort.toString());
		}
		
//		props.put("mail.smtp.connectiontimeout", DEFAULT_TIMEOUT);
//		props.put("mail.smtp.timeout", DEFAULT_TIMEOUT);
		
//		//props.put("mail.smtp.socketFactory.port", DAORegistry.getPropertyDAO().get("smtp.port").getValue());
//		props.put("mail.smtp.socketFactory.port", smtpPort);
		
//		props.put("mail.smtp.socketFactory.fallback", "false");
//		props.put("mail.smtp.quitwait", "false");			
		
		
		
		Session session;
		if (smtpAuth) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MailAuthenticator(username, password);
			session = Session.getInstance(props, auth);
			
		} else {
			props.put("mail.smtp.auth", "false");
			Authenticator auth = new MailAuthenticator(username, null);
			session = Session.getInstance(props, auth);
		}

		MimeMessage message = new MimeMessage(session);

		String fullFromAddress = null;
		if (fromName != null && !fromName.isEmpty() && emailFrom !=null && !emailFrom.isEmpty()) {
			fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";			
		} else if(emailFrom !=null && !emailFrom.isEmpty()){
			fullFromAddress = emailFrom;			
		}else{
			fullFromAddress = fromName;
		}

		Address fromAddr = new InternetAddress(fullFromAddress);

		message.setSubject(subject);
		message.setFrom(fromAddr);
		message.setReplyTo(new Address[] {new InternetAddress(fullFromAddress)});

		if (toAddress != null) {
			message.setRecipients(Message.RecipientType.TO, getAddresses(toAddress));
		}
		if (ccAddress != null) {
			message.addRecipients(Message.RecipientType.CC, getAddresses(ccAddress));	
		}
		if (bccAddress != null) {
			message.addRecipients(Message.RecipientType.BCC, getAddresses(bccAddress));
		}

		Map<String, Object> model = new HashMap<String, Object>();
		model.put("staticUrl", staticUrl);
		model.putAll(map);
		String text = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "templates/" + resource, model);
//		System.out.println(text);
		message.setSubject(subject);
		
		MimeBodyPart mimeBodyPart = new MimeBodyPart();
		mimeBodyPart.setContent(text, mimeType);
		
		//MimeMultipart mimeMultiPart = new MimeMultipart("related");
		MimeMultipart mimeMultiPart = new MimeMultipart("alternative");
		mimeMultiPart.addBodyPart(mimeBodyPart);
		
		if (attachments != null) {
			for(MailAttachment attachment: attachments) {
				MimeBodyPart attachmentBodyPart = new MimeBodyPart();
//				attachmentBodyPart.setContent(new String(Base64.encodeBase64(attachment.getContent())), mimeType);
				attachmentBodyPart.setDataHandler(new DataHandler(new ByteDataSource(attachment.getContent(), attachment.getFileName(), attachment.getMimeType())));
								 
				attachmentBodyPart.setHeader("Content-Transfer-Encoding", "base64");
				attachmentBodyPart.setFileName(attachment.getFileName());
				//attachmentBodyPart.setDisposition(MimeBodyPart.ATTACHMENT);
				attachmentBodyPart.setContentID("<" + attachment.getFileName() + ">");
				mimeMultiPart.addBodyPart(attachmentBodyPart);
			}
		}

		message.setContent(mimeMultiPart);

		message.setSentDate(new Date());
		
		Transport transport = session.getTransport("smtp");
		transport.connect(smtpHost, smtpPort, username, password);
		Transport.send(message);
	}

	
	public void sendMail(final String fromName, final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType) {
		sendMail(fromName, toAddress, null, null, subject, resource, map, mimeType, null);
	}

	public void sendMail(final String fromName, final String toAddress, final String subject, String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
		sendMail(fromName, toAddress, null, null, subject, resource, map, mimeType, attachments);		
	}

	public void sendMail(final String fromName, final String toAddress, final String ccAddress, final String bccAddress, final String subject, String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
		synchronized (this) {
			mails.add(new Mail(
					fromName,
					toAddress, ccAddress, bccAddress, subject, resource, map, mimeType, attachments
			));
		}
	}

	public void sendMail(final String fromName, final String[] toAddress, final String subject, String resource, Map<String, Object> map, String mimeType) {
		sendMail(fromName, toAddress, subject, resource, map, mimeType, null);
	}

	public void sendMail(final String fromName, final String[] toAddress, final String subject, String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
		synchronized (this) {
			for(String address: toAddress) {
				address = address.trim();
				if (address.length() == 0) {
					continue;
				}
				sendMail(fromName, address, null, null, subject, resource, map, mimeType, attachments);
			}
		}
	}

	static class MailAuthenticator extends Authenticator {
		private final String username;

		private final String password;

		public MailAuthenticator(String username, String password) {
			this.username = username;
			this.password = password;
		}

		public PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(username, 	password);
		}
	}

	public void setStaticUrl(String staticUrl) {
		this.staticUrl = staticUrl;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}
	
	class Mail {
		private String fromName;
		private String toAddress;
		private String ccAddress;
		private String bccAddress;
		private String subject;
		private String resource;
		private Map<String, Object> map;
		private String mimeType;
		private MailAttachment[] attachments;
		
		public Mail(String fromName, String toAddress, String ccAddress, String bccAddress, String subject,  String resource, Map<String, Object> map, String mimeType, MailAttachment[] attachments) {
			this.fromName = fromName;
			this.toAddress = toAddress;
			this.ccAddress = ccAddress;
			this.bccAddress = bccAddress;
			this.subject = subject;
			this.resource = resource;
			this.map = map;
			this.mimeType = mimeType;
			this.attachments = attachments;
		}

		public String getSubject() {
			return subject;
		}

		public String getResource() {
			return resource;
		}

		public Map<String, Object> getMap() {
			return map;
		}

		public String getMimeType() {
			return mimeType;
		}

		public String getToAddress() {
			return toAddress;
		}

		public String getCcAddress() {
			return ccAddress;
		}

		public String getBccAddress() {
			return bccAddress;
		}

		public String getFromName() {
			return fromName;
		}

		public MailAttachment[] getAttachments() {
			return attachments;
		}

	}
	
	public static void main(String[] args) throws Exception {


		MailManager manager = new MailManager();

		manager.sendMailNow("smtp.gmail.com",465, "ssl",
				"ssl",true, "rightthiswayllc@gmail.com", "ticket#1441","Chirag Shah","cshah@rightthisway.com","cshah@rightthisway.com",
				null,null,"Test", "mail-venue-map.txt",new HashMap(), "text/html",null);

		
		final String username = "rightthiswayllc@gmail.com";
		final String password = "ticket#1441";

		Properties props = new Properties();
		Boolean smtpAuth = true;
//		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.ssl.enable", "true");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "465");
		
		
		String smtpHost ="smtp.gmail.com";
		Integer smtpPort = 465;
		String smtpSecureConnection ="ssl";
		props.put("mail.smtp.host", smtpHost);
		props.put("mail.smtp.port", smtpPort);
		
		
//		props.put("mail.smtp.auth", "true");

		if (smtpSecureConnection.equalsIgnoreCase("tls")) {
			props.put("mail.smtp.starttls.enable", "true");
		}
		if (smtpSecureConnection.equalsIgnoreCase("ssl")) {
			props.put("mail.smtp.ssl.enable", "true");
			props.put("mail.smtp.socketFactory.port", smtpPort.toString());
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		}


		Session session;
		if (smtpAuth) {
			props.put("mail.smtp.auth", "true");
			Authenticator auth = new MailAuthenticator(username, password);
			session = Session.getInstance(props, auth);
			
		} else {
			props.put("mail.smtp.auth", "false");
			Authenticator auth = new MailAuthenticator(username, null);
			session = Session.getInstance(props, auth);
		}
		try {
			String fromName ="Chirag Shah";
			String emailFrom ="cshah@rightthisway.com";
			MimeMessage message = new MimeMessage(session);

			String fullFromAddress = null;
			if (fromName != null && !fromName.isEmpty() && emailFrom !=null && !emailFrom.isEmpty()) {
				fullFromAddress = "\"" + fromName + "\" <" + emailFrom + ">";			
			} else if(emailFrom !=null && !emailFrom.isEmpty()){
				fullFromAddress = emailFrom;			
			}else{
				fullFromAddress = fromName;
			}

			Address fromAddr = new InternetAddress(fullFromAddress);

//			Message message = new MimeMessage(session);
//			InternetAddress from = new InternetAddress("Chirag Shah<cshah@rightthisway.com>");
			message.setFrom(fromAddr);
			message.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse("cshah@rightthisway.com"));
			message.setSubject("Testing Subject Sess From Addr");
			message.setText("Dear Mail Crawler,"
				+ "\n\n No spam to my email, please!");
			System.out.println(props);
			Transport.send(message);

			System.out.println("Done");

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}
	
}


class ByteDataSource implements DataSource {
	private InputStream inputStream;
	private OutputStream outputStream;
	private String mimeType;
	private String name;
	
	public ByteDataSource(byte[] content, String name, String mimeType) {
		this.inputStream = new ByteArrayInputStream(content);
		this.outputStream = new ByteArrayOutputStream();
		this.mimeType = mimeType;
		this.name = name;
	}

	public String getContentType() {
		return mimeType;
	}

	public InputStream getInputStream() throws IOException {
		return inputStream;
	}

	public String getName() {
		return name;
	}

	public OutputStream getOutputStream() throws IOException {
		return outputStream;
	}	
}