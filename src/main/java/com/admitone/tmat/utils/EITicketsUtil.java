package com.admitone.tmat.utils;

import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.EIInstantEvent;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.Property;
import com.admitone.tmat.data.Ticket;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.EventStatus;

/**
 * a. Computing the final set of EIMP 'fan' tickets
 * b. Writing the EIMP 'fan' tickets to CSV file
 */

public class EITicketsUtil {
	
private EITicketsUtil() {}
	
	public static void downloadEITicketCsvFile(HttpServletResponse response) throws ServletException,IOException{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Content-disposition",
				"attachment; filename=EITickets_" + today + ".csv");
		Collection<Ticket> eiTickets = computeEITickets();
		downloadEITicketCsvFile(eiTickets, response.getOutputStream()); 
	}
	
	public static void downloadCustomizedEITicketCsvFile(HttpServletResponse response) throws ServletException,IOException{
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			String today = dateFormat.format(new Date());
		response.setHeader("Content-Type", "text/plain");
		response.setHeader("Content-disposition",
				"attachment; filename=CustomizedEITickets_" + today + ".csv");
		Collection<Ticket> eiTickets = computeCustomizedCatEITickets();
		downloadCustomizedEITicketCsvFile(eiTickets,response.getOutputStream());
	}
	
	public static  void downloadCustomizedEITicketCsvFile(Collection<Ticket> eiTickets ,OutputStream outputStream) throws IOException{
//		Collection<Ticket> eiTickets = computeCustomizedCatEITickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		for (Ticket eiTicket: eiTickets) {
//			Event event = eiTicket.getEvent();

			String timeStr;
			if (eiTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(eiTicket.getEventTime());
			}
	
				String seatFrom = "";
				String seatThru = "";
				String price = numberFormat.format(eiTicket.getCurrentPrice());
				String notes = null;
			if (eiTicket.getRemainingQuantity()>=3){
				Property notesProperty = DAORegistry.getPropertyDAO().get("eiTickets.notes");
				notes = notesProperty.getValue();
			}else{
				notes="";
			}
			
			String finalRow = "";
			if (eiTicket.getRow()!=null){
			finalRow = getMaskedRow(eiTicket.getRow().toUpperCase());
			}
			
			String resultRow = "y" + ","
						+ eiTicket.getEventName() + "," 
						+ eiTicket.getVenue() + "," 
						+ dateFormat.format(eiTicket.getEventDate()) + ","
						+ timeStr.trim() + ","
						+ eiTicket.getRemainingQuantity() + ","
						+ eiTicket.getNormalizedSection() + ","
					//	+ eiTicket.getRow() + ","
						+ finalRow + ","
						+ seatFrom + ","
						+ seatThru + ","
						+ notes +","
						+ price + "," 
						+ eiTicket.getId()
						+ "\n";
			outputStream.write(resultRow.getBytes());
		}
	}
	
	public static  void downloadCustomizedEITNDTicketCsvFile(Collection<Ticket> eiTickets ,OutputStream outputStream) throws IOException{
//		Collection<Ticket> eiTickets = computeCustomizedCatEITickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		StringBuffer resultRow= new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,TicketGroupTypeID,TicketGroupStockTypeID,ShippingMethodSpecialID,ShowNearTermOptionID,InHandDate,TicketGroupObstructionDescriptionID\n");
		int i=0;
		Date now = new Date();
		Property notesProperty = DAORegistry.getPropertyDAO().get("eiTickets.notes");
		for (Ticket eiTicket: eiTickets) {
//			Event event = eiTicket.getEvent();
			i++;
			String timeStr;
			if (eiTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(eiTicket.getEventTime());
			}
	
				String seatFrom = "";
				String seatThru = "";
				String price = numberFormat.format(eiTicket.getCurrentPrice());
				String notes = null;
			if (eiTicket.getRemainingQuantity()>=3){
				notes = notesProperty.getValue();
			}else{
				notes="";
			}
			
			String finalRow = "";
			if (eiTicket.getRow()!=null){
			finalRow = getMaskedRow(eiTicket.getRow().toUpperCase());
			}
			
			int emailOption=1;
			long hrs=(eiTicket.getEventDate().getTime()- now.getTime()) /(1000*60*60);
			if(hrs > 50)
				emailOption=0;
			
			resultRow.append("y,");
			resultRow.append(eiTicket.getEventName() + ","); 
			resultRow.append(eiTicket.getVenue() + "," );
			resultRow.append(dateFormat.format(eiTicket.getEventDate()) + ",");
			resultRow.append(timeStr.trim() + ",");
			resultRow.append(eiTicket.getRemainingQuantity() + ",");
			resultRow.append(eiTicket.getNormalizedSection() + ",");
		//	+ eiTicket.getRow() + ","
			resultRow.append(finalRow + ",");
			resultRow.append(seatFrom + ",");
			resultRow.append(seatThru + ",");
			resultRow.append(notes +",");
			resultRow.append(price + ","); 
			resultRow.append(eiTicket.getId()+ ",");
			resultRow.append("1,"); //TicketGroupTypeID 
			resultRow.append("2,"); //TicketGroupStockTypeID 
			resultRow.append("1,"); //ShippingMethodSpecialID  : emailOption
			resultRow.append("0,"); //ShowNearTermOptionID 
//			resultRow.append(dateFormat.format(now) +","); //InHandDate
			resultRow.append("Y,");
			resultRow.append("0 \n"); //TicketGroupObstructionDescriptionID 
			if(i>200){
				i=0;
				outputStream.write(resultRow.toString().getBytes());
				resultRow=null;
				resultRow= new StringBuffer("");
			}
			
		}
		outputStream.write(resultRow.toString().getBytes());
		resultRow=null;
	}
	
	public static  void downloadEITicketCsvFile(Collection<Ticket> eiTickets, OutputStream outputStream) throws IOException{
//		Collection<Ticket> eiTickets = computeEITickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		outputStream.write("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId\n".getBytes());
		for (Ticket eiTicket: eiTickets) {
			String timeStr;
			if (eiTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(eiTicket.getEventTime());
			}
		
				String seatFrom = "";
				String seatThru = "";
				
			String price = numberFormat.format(eiTicket.getCurrentPrice());
			
			String notes = null;
			if (eiTicket.getRemainingQuantity()>=3){
				Property notesProperty = DAORegistry.getPropertyDAO().get("eiTickets.notes");
				notes = notesProperty.getValue();
			}else{
				notes="";
			}
			
			String finalRow = "";
			if (eiTicket.getRow()!=null){
			finalRow = getMaskedRow(eiTicket.getRow().toUpperCase());
			}
			
			String resultRow = "y" + ","
						+ eiTicket.getEventName() + "," 
						+ eiTicket.getVenue() + "," 
						+ dateFormat.format(eiTicket.getEventDate()) + ","
						+ timeStr.trim() + ","
						+ eiTicket.getRemainingQuantity() + ","
						+ eiTicket.getNormalizedSection() + ","
					//	+ eiTicket.getRow() + ","
						+ finalRow + ","
						+ seatFrom + ","
						+ seatThru + ","
						+ notes +","
						+ price + "," 
						+ eiTicket.getId()
						+ "\n";
			outputStream.write(resultRow.getBytes());
		}
	}
	public static  void downloadEITNDTicketCsvFile(Collection<Ticket> eiTickets, OutputStream outputStream) throws IOException{
//		Collection<Ticket> eiTickets = computeEITickets();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat timeFormat = new SimpleDateFormat("h:mma");
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setGroupingUsed(false);
		numberFormat.setMinimumFractionDigits(2);
		numberFormat.setMaximumFractionDigits(2);
		StringBuffer resultRow= new StringBuffer("Edit,Event,Venue,EventDate,EventTime,Quantity,Section,Row,SeatFrom,SeatThru,Notes,Cost,TicketId,TicketGroupTypeID,TicketGroupStockTypeID,ShippingMethodSpecialID,ShowNearTermOptionID,InHandDate,TicketGroupObstructionDescriptionID\n");
//		outputStream.write();
		Date now=new Date();
		Property notesProperty = DAORegistry.getPropertyDAO().get("eiTickets.notes");
		int i=0;
		for (Ticket eiTicket: eiTickets) {
			i++;
			String timeStr;
			if (eiTicket.getEventTime() == null) {
				timeStr = "TBD";
			} else {
				timeStr = timeFormat.format(eiTicket.getEventTime());
			}
		
				String seatFrom = "";
				String seatThru = "";
				
			String price = numberFormat.format(eiTicket.getCurrentPrice());
			
			String notes = null;
			if (eiTicket.getRemainingQuantity()>=3){
				notes = notesProperty.getValue();
			}else{
				notes="";
			}
			
			String finalRow = "";
			if (eiTicket.getRow()!=null){
			finalRow = getMaskedRow(eiTicket.getRow().toUpperCase());
			}
			int emailOption=1;
			long hrs=(eiTicket.getEventDate().getTime()- now.getTime()) /(1000*60*60);
			if(hrs > 50)
				emailOption=0;
			
			resultRow.append("y,");
			resultRow.append(eiTicket.getEventName() + ","); 
			resultRow.append(eiTicket.getVenue() + "," );
			resultRow.append(dateFormat.format(eiTicket.getEventDate()) + ",");
			resultRow.append(timeStr.trim() + ",");
			resultRow.append(eiTicket.getRemainingQuantity() + ",");
			resultRow.append(eiTicket.getNormalizedSection() + ",");
					//	+ eiTicket.getRow() + ","
			resultRow.append(finalRow + ",");
			resultRow.append(seatFrom + ",");
			resultRow.append(seatThru + ",");
			resultRow.append(notes +",");
			resultRow.append(price + "," );
			resultRow.append(eiTicket.getId()+ ",");
			resultRow.append(",");
			resultRow.append(",");
			resultRow.append(emailOption +",");
			resultRow.append(2 +",");
			resultRow.append(dateFormat.format(now) +",");
			resultRow.append("\n");
			if(i>200){
				i=0;
				outputStream.write(resultRow.toString().getBytes());
				resultRow=null;
				resultRow= new StringBuffer("");  // dose delete() free memory ? 
			}
			
		}
		outputStream.write(resultRow.toString().getBytes());
		resultRow=null;
	}
	
	

	/*Only non duplicate, non AO, categorized EI tickets are considered*/
	public static Collection<Ticket> computeEITickets() {
		Collection<Ticket> filteredEITickets = new ArrayList<Ticket>();
		Collection<Ticket> filteredUniqueEITickets = new ArrayList<Ticket>();
		Property property = DAORegistry.getPropertyDAO().get("eiTickets.eventIds");
		String eventIds = property.getValue();
		Integer eventId = 0;
		double markup;
		double varPrice = 5 ;
		for (String eventIdStr: eventIds.split(",")) {
			if (eventIdStr.trim().isEmpty()) {
				continue;
			}
			try{
			eventId = Integer.valueOf(eventIdStr.trim());
			}catch (NumberFormatException nfe){}
			Event event = DAORegistry.getEventDAO().get(eventId);
			if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)&&eventId!=0){
				if (event.getDate().getTime()- new Date().getTime()>60L*60L*60L*1000L){
			/*Integer tourId = event.getTourId();
			if (tourId==16341){
				markup = 1.25;
			}else if (tourId==16570){
				markup = 1.27;
			}else if (tourId==16319){
				markup = 1.26;
			}else if (tourId==6700){
				markup = 1.24;
			}else{*/
				markup = 1.2;
			/*}*/
			Collection<Ticket> eiTickets = null;//DAORegistry.getEiTicketsQueryManager().getEITickets(eventId);
			String catScheme = "";
			if(event.getVenueCategory()!=null){
				catScheme = event.getVenueCategory().getCategoryGroup();	
			}
			List<String> catSchemes = new ArrayList<String>();
			catSchemes.add(catScheme);
		//	Collection<Ticket> tickets = DAORegistry.getTicketDAO().getAllActiveTicketsByEvent(eventId);
		//	List<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
		//	tmpFilteredTickets = TicketUtil.removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		//	Collection<AdmitoneInventory> aotickets = DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByEventId(eventId);
		//	if (catScheme!=null){
			for (String catscheme : catSchemes) {
				VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catscheme);
				if(venueCategory==null){
					continue;
				}
				Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(venueCategory.getId());
				Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
				for (Category category : categories) {
					categoryMap.put(category.getId(), category);
				}
				Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
				Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(venueCategory.getId());
				for(CategoryMapping mapping:categoryMappingList){
					List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
					if(list==null){
						list = new ArrayList<CategoryMapping>();
					}
					list.add(mapping);
					catMappingMap.put(mapping.getCategoryId(), list);
				}
				
		//		for (Ticket ticket:tmpFilteredTickets){
				if (eiTickets!=null){
				for (Ticket eiTicket : eiTickets) {
		//				if(!TicketUtil.isInInventory(eiTicket, aotickets)){
								int quantity = eiTicket.getRemainingQuantity();
				//				double varPrice = 5 ;
								double price = ((eiTicket.getCurrentPrice()*markup)+varPrice);
								eiTicket.setCurrentPrice(price);
								if(quantity*price>100 && price<20000){
									Category cat = Categorizer.computeCategory(event.getVenueCategory(), eiTicket.getNormalizedSection(),eiTicket.getRow(), catscheme,categoryMap,catMappingMap);	
									if (cat != null) {
										filteredEITickets.add(eiTicket);	
											
										}
									}
								}
							}
							}
				//		}
				}	
				}
				}
	//	System.out.println("EI tix filtered coll size" +filteredEITickets.size());
		filteredUniqueEITickets = getUniqueValues (filteredEITickets);
		return filteredUniqueEITickets;
	//	return filteredEITickets;
	}
	
	public static Collection<Ticket> computeCustomizedEITickets(Integer eventId) {
		Collection<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
			EIInstantEvent instantEvent = null;//DAORegistry.getEiinstantEventDAO().get(eventId);
			Event event = DAORegistry.getEventDAO().get(eventId);
			
			Integer expiryTime = instantEvent.getExpiryTime();
		
			long expiryEventTime  = expiryTime * 60L * 60L * 1000L ;
			
			if(event!=null && event.getEventStatus().equals(EventStatus.ACTIVE)){
				Date date = event.getDate();
				Date time = null;
				
				if (event.getTime()==null){
					time = event.getDate();
				}else{
					time = event.getTime();
				}
				Calendar calDate = new GregorianCalendar();
				Calendar calTime = new GregorianCalendar();
				calDate.setTime(date);
				calTime.setTime(time);
			
				Calendar caldatetime = new GregorianCalendar();
				caldatetime.set(Calendar.YEAR, calDate.get(Calendar.YEAR));
				caldatetime.set(Calendar.MONTH, calDate.get(Calendar.MONTH));
				caldatetime.set(Calendar.DATE, calDate.get(Calendar.DATE));
				caldatetime.set(Calendar.HOUR_OF_DAY, calTime.get(Calendar.HOUR_OF_DAY));
				caldatetime.set(Calendar.MINUTE, calTime.get(Calendar.MINUTE));
				caldatetime.set(Calendar.SECOND, calTime.get(Calendar.SECOND));
				Date eventDateTime = caldatetime.getTime();
				long difference = eventDateTime.getTime() - new Date().getTime();
				
				if (difference>expiryEventTime){	
				Double markup = instantEvent.getMarkupPercent();
				Double minThreshold = instantEvent.getMinThreshold();
				Double maxThreshold = instantEvent.getMaxThreshold();
				Double salesPercent = instantEvent.getSalesPercent();
				Double shippingFee = instantEvent.getShippingFee();
				
			Collection<Ticket> eiTickets = null;//DAORegistry.getEiTicketsQueryManager().getEITickets(eventId);		
			List<String> catSchemes = new ArrayList<String>();//= Categorizer.getCategoryGroupsByEvent(eventId);
			if(event.getVenueCategory()!=null){
				String catScheme = event.getVenueCategory().getCategoryGroup();
				catSchemes.add(catScheme);
			}
	
			for (String catscheme : catSchemes) {
//				Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategories(event.getTourId(), catscheme);
				Collection<Category> categories = null;
				/*if(event.getVenueCategory()!=null){
					categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
				}else{
					categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueIdAndCategoryGroup(event.getVenueId(), catscheme);
				}*/
				VenueCategory venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catscheme);
				if(venueCategory==null){
					continue;
				}
				categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
				Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
				for (Category category : categories) {
					categoryMap.put(category.getId(), category);
				}
				Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
				Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
				for(CategoryMapping mapping:categoryMappingList){
					List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
					if(list==null){
						list = new ArrayList<CategoryMapping>();
					}
					list.add(mapping);
					catMappingMap.put(mapping.getCategoryId(), list);
				}
				for (Ticket eiTicket : eiTickets) {
					double markupPrice = 0, salesPrice = 0,  totalPrice = 0;
								int quantity = eiTicket.getRemainingQuantity();
	
								double price = eiTicket.getCurrentPrice();
								if (markup!= null){
									markupPrice = (price * ((100 + markup) / 100))-price;
								}
								if (salesPercent != null){
									salesPrice  =(price * (100 +salesPercent) /100 ) - price;
								}
							
									totalPrice = price + markupPrice + salesPrice + shippingFee ; 
						
								eiTicket.setCurrentPrice(totalPrice);
								if(quantity*totalPrice>minThreshold && totalPrice<maxThreshold){
									Category cat = Categorizer.computeCategory(venueCategory, eiTicket.getNormalizedSection(),eiTicket.getRow(), catscheme,categoryMap,catMappingMap);
									if(cat!=null){
										eiTicket.setCategoryId(cat.getId());
										eiTicket.setCategory(categoryMap.get(cat.getId()));
										tmpFilteredTickets.add(eiTicket);
										}
									}
								}
							}
						}
					}
			else if (!event.getEventStatus().equals(EventStatus.ACTIVE)){
//				Removed by chirag on 02.06.2015 for tour removal logic.. write it as per new requirement
//					EIEventManager.deleteInstantEvents(eventId);
				
					}
	return tmpFilteredTickets;
	}                        
	
	
	public static Collection<Ticket> computeCustomizedCatEITickets(){
		Collection<Ticket> filteredCatTickets = new ArrayList<Ticket>();
		Collection<Ticket> tmpFilteredTickets =  new ArrayList<Ticket>();
		Collection<Ticket> eiFilteredTickets = new ArrayList<Ticket>();
		Collection<Ticket> filteredUniqueEITickets = new ArrayList<Ticket>();
		Collection<EIInstantEvent> instantEvents = null;//DAORegistry.getEiinstantEventDAO().getAll();
		for (EIInstantEvent instantEvent:instantEvents){
				Integer eventId = instantEvent.getEventId();
				tmpFilteredTickets = computeCustomizedEITickets(eventId);
				filteredCatTickets = getCatEITickets(tmpFilteredTickets);				
				eiFilteredTickets.addAll(filteredCatTickets);
			}
		filteredUniqueEITickets = getUniqueValues (eiFilteredTickets);
		return filteredUniqueEITickets;
	}
	
	public static Collection<Ticket> getCatEITickets(Collection<Ticket> tmpFilteredTickets){
		Map<Integer, List<Ticket>> catTickets = new HashMap<Integer, List<Ticket>>();
		Collection<Ticket> filteredInstantTickets = new ArrayList<Ticket>();
				for (Ticket instantTicket:tmpFilteredTickets){
					List<Ticket> list = (List<Ticket>) catTickets.get(instantTicket.getCategory().getId());
					if (list == null) {
					list = new ArrayList<Ticket>();
						catTickets.put(instantTicket.getCategory().getId(), list);
				}
					list.add(instantTicket);
				}	
				filteredInstantTickets = orderCatTickets(catTickets);		
			return filteredInstantTickets;
	}
	
	
	public static Collection<Ticket> orderCatTickets(Map<Integer, List<Ticket>> catTicketList){
		Collection<Ticket> orderedTickets = new ArrayList<Ticket>();
		List<Ticket> ticketsByCat = new ArrayList<Ticket>();
		for (Integer categoryId: catTicketList.keySet()){
			ticketsByCat= catTicketList.get(categoryId);
				
			try {
			//Collections.sort(ticketsByCat, new Comparator<Ticket>(){
				ticketsByCat.sort( new Comparator<Ticket>(){
					@Override
					public int compare(Ticket t1, Ticket t2){
						if (t1.getCurrentPrice().compareTo(t2.getCurrentPrice())>0){
							return -1;
						} else if (t1.getCurrentPrice().compareTo(t2.getCurrentPrice())<0){
							return 1;
						}else 
							return 0;
					}
				});
			}catch(Exception ex) {
				System.out.println("Exception in EITictetsUtil line no 575 " + ex);
			}
			
				if (ticketsByCat.size()>1){
					ticketsByCat.remove(ticketsByCat.size()-1);
					orderedTickets.addAll(ticketsByCat);
				}else {
					orderedTickets.addAll(ticketsByCat);
				}
		}
		return orderedTickets;
	}
	
/* To get unique tickets when there are multiple cat schemes*/	
public static Collection<Ticket> getUniqueValues(Collection<Ticket> filteredEITickets){
    return (Collection<Ticket>)Union(filteredEITickets, filteredEITickets);
}

public static Collection<Ticket> Union(Collection<Ticket> coll1, Collection<Ticket> coll2){
    Set<Ticket> union = new HashSet<Ticket>(coll1);
    union.addAll(new HashSet<Ticket>(coll2));
    return new ArrayList<Ticket>(union);
}

/* To increment the value of row by 1(1->2 or A->B)*/
public static String getMaskedRow(String row){
	String finalRow = null;
	String intRow = "\\d+";
//	String alphaRow = "([A-Z]+|[a-z]+)";
//	String intStrRegex = "([0-9]{1}[A-Z]{1}|[A-Z]{1}[0-9]{1})";
	String repeatChar = "(\\w)\\1+";
	
	if (row.length()>2){
		finalRow = row;
	}else {
		if (Pattern.matches(intRow, row)){
			try{
			finalRow = (Integer.parseInt(row) + 1) + "";
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		else if (row.length() ==1){
			char c = row.charAt(0);
			if (Character.toUpperCase(c)!='Z'){
				c ++;
			}else {
				c = 'Z';
			}
			finalRow = String.valueOf(c);
		}
		else if ((row.length()==2) && (Pattern.matches(repeatChar, row))){
				char[] c = row.toCharArray();
				if (Character.toUpperCase(c[0])!='Z'){
					c[0] ++;
				}else {
					c[0] = 'Z';
				}
				c[1] = c[0];
				finalRow = String.valueOf(c);
			}
		else 
			finalRow = row;
		}
	return finalRow;
	}

}

