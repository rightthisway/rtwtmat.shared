package com.admitone.tmat.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Hex;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.AdmitoneInventory;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.DefaultPurchasePrice;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ManagePurchasePrice;
import com.admitone.tmat.data.Site;
import com.admitone.tmat.data.Ticket;


/**
 * Ticket Util Class.
 */
public final class AutoPricingTicketUtil {
	public static final Integer MAX_QTY = new Integer(11);
//	private static final Logger logger = LoggerFactory.getLogger(TicketUtil.class);
	private static final Double SIMILARITY_PRICE_THRESHOLD_PERCENT = 0.80; // not used for smart remove
	private static final Double SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT = 0.25;   // Chagned by Chirag on Matt's Request
	
	// site ranking when comparing tickets
	private static Map<String, Integer> siteRank = new HashMap<String, Integer>();
//	private static CurrencyRSSFetcher gbpFetcher = null;
//	private static CurrencyRSSFetcher eurFetcher = null;
//	// eventId-siteId => price adjustment
//	private static Map<String, Double> percentAdjustmentMap = new HashMap<String, Double>();
//	private static Map<String, PriceRounding> priceRoundingMap = new HashMap<String, PriceRounding>();
	private static String[] rankedSiteIds = {Site.AOP, Site.EIBOX, Site.EI_MARKETPLACE, Site.TICKET_NETWORK, Site.TICKET_NETWORK_DIRECT,
		Site.STUB_HUB, Site.STUBHUB_FEED, Site.TICKET_SOLUTIONS, Site.WS_TICKETS,Site.TICKET_EVOLUTION,
		Site.EBAY, Site.VIAGOGO, Site.SEATWAVE, Site.GET_ME_IN,
		Site.RAZOR_GATOR, Site.TICKET_MASTER, Site.EVENT_INVENTORY, Site.TICKETS_NOW, Site.FAN_SNAP 
};


	private static String[] reversedRankedSiteIds;
	private static Pattern seatRangePattern = Pattern.compile("[0]*([0-9])+\\s+-\\s+([0]*[0-9])+");
	private static Pattern singleSeatPattern = Pattern.compile("([0]*[0-9])+");
	
	private static Set<String> larryQuantitySeatTickets = new HashSet<String>();
	
	static {
		// 1- IF EIMP and ANY other sites�.THEN scrub all dupes and display ONLY
		// EIMP
		// 2- IF no EIMP, BUT Ticket Network and ANY other sites�.THEN scrub all
		// dupes and display ONLY Ticket Network
		// 3- IF no EIMP and Ticket Network, BUT Stubhub and ANY other
		// sites�then scrub all dupes and display ONLY StubHub
		// 4- IF no EIMP and Ticket Network, and StubHub, BUT Ticketsnow and ANY
		// other sites�then scrub all dupes and display ONLY Ticketsnow
		// 5- IF no EIMP and TicketNetwork, StubHub, TicketsNow, BUT Event
		// Inventory and ANY other sites�.then scrub all dupes and display only
		// Event Inventory
		// 6- IF no EIMP and Ticket Network and StubHub and Ticketsnow, BUT
		// Razorgator and ANY other sites�then scrub all dupes and display ONLY
		// Razorgator
		// 7- IF no EIMP, Ticket Network, StubHub, Ticketsnow, Razorgator, BUT
		// Ticketsolutions and ANY other sites�then scrub all dupes and display
		// ONLY Ticketsolutions
		// 8- IF no EIMP, TicketNetwork, StubHub, Ticketsnow, Razorgator,
		// Ticketsolutions, BUT Western States and ANY other sites�then scrub
		// all dupes and display only Western States
		// 9- IF no EIMP, Ticket Network, Stub Hub, Ticketsnow, RazorGator,
		// Ticketsoultions, Western States, but Viagogo and ANY other site�then
		// scrub all dupes and display ONLY Viagogo
		// 10- IF no EIMP, TicketNetwork, StubHub, TicketsNow, Rzrogator,
		// Ticketsolutions, Western State, Viagogo, but Seatwave and ANY other
		// site�then scrub all dupes and display ONLY Seatwave
		// 11 � IF no EIMP, TicketNetwork, StubHub, Ticketsnow, Razorgator,
		// Ticketsolutions, Western State, Viagogo, Seatwave, but GetMeIn and
		// ANY other site�then scrub all dupes and display ONLY GetMeIn
		// 12- IF no EIMP, TicektNetwork, StubHub, Ticektsnow, Razorgator,
		// Ticketsolutions, Western States, Viagogo, Seatwave, GetMeIn, but EBay
		// and ANY other site, then scrub all dupes and display ONLY Ebay.

		// orders: EIMP > Ticket Network > StubHub > TicketsNow > RazorGator >
		// TicketsSolutions > Western States > Viagogo > SeatWave > GetMeIn >
		// eBay > FanSnap > TicketMaster
		
		int rank = 1;
		for (String siteId : rankedSiteIds) {
			siteRank.put(siteId, rank);
			rank++;
		}	

		reversedRankedSiteIds = new String[rankedSiteIds.length];
		for (int i = 0; i < rankedSiteIds.length; i++) {
			reversedRankedSiteIds[rankedSiteIds.length - 1 - i] = rankedSiteIds[i];
		}	
		
		/*try{
			gbpFetcher = new CurrencyRSSFetcher("http://coinmill.com/rss/GBP_USD.xml");
			eurFetcher = new CurrencyRSSFetcher("http://coinmill.com/rss/EUR_USD.xml");
		} catch (FeedException feed) {
			System.out.println("Caught FeedException");
			feed.printStackTrace();
		} catch (FetcherException fete) {
			System.out.println("Caught FetcherException");
			fete.printStackTrace();
		} catch (Exception e) {
			System.out.println("Caught Exception getting currency fetcher: " + e.getMessage());
			e.printStackTrace();
		}	*/	
		
		larryQuantitySeatTickets.add("1-1001");
		larryQuantitySeatTickets.add("2-2001-2002");
		larryQuantitySeatTickets.add("3-3001-3003");
		larryQuantitySeatTickets.add("4-4001-4004");
		larryQuantitySeatTickets.add("5-5001-5005");
		larryQuantitySeatTickets.add("6-6001-6006");
		larryQuantitySeatTickets.add("7-7001-7007");
		larryQuantitySeatTickets.add("8-8001-8008");
		larryQuantitySeatTickets.add("9-9001-9009");
		
	}

	/**
	 * Get ticket duplicate key. (allows to identify a unique ticket)
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 */
	private static MessageDigest md5MessageDigest = null;	
	static {
		try {
			md5MessageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	/*public static void flushAdjustmentMaps(){
		percentAdjustmentMap = new HashMap<String, Double>();
		priceRoundingMap = new HashMap<String, PriceRounding>();
	}*/

	/*private static String getAdjustKey(Ticket ticket) {
		return ticket.getEventId() + "-" + ticket.getSiteId();
	}*/
	
	public static Map<String ,Map<String,CategoryMapping>> getSectionBasedAlternateRows(Event event) throws Exception {
		
		Map<String ,Map<String,CategoryMapping>> alternateRowMap = new HashMap<String, Map<String,CategoryMapping>>();
		
		Map<String,List<CategoryMapping>> csvCatMap = Categorizer.getCategoryMappingByVenueCategoryId(event);
		List<String> keyList = new ArrayList<String>(csvCatMap.keySet());
		
		for (String key : keyList) {
		
			if(key.equals("-")) {
				continue;
			}
			List<CategoryMapping> catmappingList = csvCatMap.get(key);
		
			for (CategoryMapping mapping : catmappingList) {
				
				if(mapping.getAlternateRow() != null && !mapping.getAlternateRow().equals("")) {
					String alternateRow = mapping.getAlternateRow().replaceAll("\\s+", " ").toLowerCase();
					try {
						List<String> rowList = null;
						String[] strArr = alternateRow.split("-");
						if(strArr.length == 2) {
							//section = mapping.getStartSection().replaceAll("\\s+", " ").toLowerCase();
							Map<String,CategoryMapping> catmappingMap = alternateRowMap.get(key);
							
							
							if(catmappingMap == null) {
								catmappingMap = new HashMap<String, CategoryMapping>();
							}
							CategoryMapping catmapping = catmappingMap.get(alternateRow);
							
							if(catmapping == null) {
								rowList = computeValidRowsByAlternateRow(strArr);
							} else {
								rowList = catmapping.getRowList();
							}
							
							if(rowList == null || rowList.isEmpty()) {
								continue;
							}
							mapping.setRowList(rowList);
//							CategoryMapping tempCatMapping = new CategoryMapping(mapping);
//							catmappingMap.put(alternateRow, tempCatMapping);
							catmappingMap.put(alternateRow, mapping);
							alternateRowMap.put(key, catmappingMap);
							
						}
						
					} catch(Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return alternateRowMap;
			
	}
	
	
	public static List<String> computeValidRowsByAlternateRow(String[] strArr) throws Exception {
		String intRegex = "[0-9]+";
		String strRegex = "[a-z]+";
		List<String> rowList = new ArrayList<String>(); 
		try {
			
			String firstVal = strArr[0];
			String secondVal = strArr[1];
			boolean isFirstValInt = false,isSecondValInt = false;;
			
			if(firstVal.matches(intRegex)) {
				isFirstValInt = true;
			} else if(!firstVal.matches(strRegex)) {
				return null;
			}
			
			if(secondVal.matches(intRegex)) {
				isSecondValInt = true;
			} else if(!secondVal.matches(strRegex)) {
				return null;
			}
			
			if(isFirstValInt && isSecondValInt) {//Both values are Number
				int startValue = Integer.parseInt(firstVal);
				int endValue =  Integer.parseInt(secondVal);
				int count=0;	
				for(int i=endValue;i>=startValue;i--) {
					count++;
					if(count<=10) {
						rowList.add(""+i);
					}
				}
				
			} else if (!isFirstValInt && !isSecondValInt) {//Both values are Character
				char startChar = firstVal.charAt(0);
				char endChar = secondVal.charAt(0);
				char tempChar;
				
				boolean isSameLength = firstVal.length() == secondVal.length();
				
				int length = secondVal.length();
				int count=0;
				
				for(int i=endChar;i>=97;i--) {
					
					if(count<10) {
						if(isSameLength && i<startChar) {
							break;
						}
						if(i == 105 && i!=endChar && i!=startChar) {//Checking i value
							continue;
						}
						
						count++;
						tempChar = (char)i;
						String str="";
						for(int j=0;j<length;j++) {
							str = str+tempChar;
						}
						rowList.add(str);
					}
				}
				if(!isSameLength && count <= 10) {
					length = firstVal.length();
					for(int i=startChar; count<10; i++) {
						if(i>122) {
							break;
						}
						if(i == 105 && i!=endChar && i!=startChar) {//Checking i value
							continue;
						}
						
						count++;
						tempChar = (char)i;
						String str="";
						for(int j=0;j<length;j++) {
							str = str+tempChar;
						}
						rowList.add(str);
					}	
				}
				
			} else if(isFirstValInt && !isSecondValInt) {//First value int and second value character
				char endChar = secondVal.charAt(0);
				char tempChar;
				
				int length = secondVal.length();
				int count=0;
				
				for(int i=endChar;i>=97;i--) {
					
					if(count<10) {
						if(i<97) {
							break;
						}
						if(i == 105 && i!=endChar) {//Checking i value
							continue;
						}
						
						count++;
						tempChar = (char)i;
						String str="";
						for(int j=0;j<length;j++) {
							str = str+tempChar;
						}
						rowList.add(str);
					}
				}
				
				if(count <= 10) {
					int startNum = Integer.valueOf(firstVal);
					for(int i=startNum; count<10; i++) {
						count++;
						rowList.add(""+i);
					}	
				}
				
			} else if(!isFirstValInt && isSecondValInt) {//Firset value character and second value int
				char startChar = firstVal.charAt(0);
				char tempChar;
				
				//int length = endLetter.length();
				int count=0;
				int endNum = Integer.valueOf(secondVal);

				for(int i=endNum;i>=1;i--) {
					if(count<10) {
						count++;
						rowList.add(""+i);
					}
				}
				
				if(count <= 10) {
					int length = firstVal.length();
					for(int i=startChar; count<10; i++) {
						if(i>122) {
							break;
						}
						if(i == 105 && i!=startChar) {//Checking i value
							continue;
						}
						count++;
						tempChar = (char)i;
						String str="";
						for(int j=0;j<length;j++) {
							str = str+tempChar;
						}
						rowList.add(str);
					}	
				}
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return rowList;
	}
	/**
	 * Generate the ticket duplicate key from the fields of the ticket.
	 * @return duplicate key.
	 */

	public static String getTicketDuplicateKey(Ticket ticket) { 	
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getRow());
		}
	
		/*String admitoneTicket = "";
		if(ticket.isAdmitOneTicket()){
			admitoneTicket = "ADMT1";
		}*/
		String key = "" //+ admitoneTicket
			 	+ ticket.getTicketStatus()
		 		+ ticket.getRemainingQuantity() + "," + section + ","
				+ row;

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}


	public static String getTicketDuplicateByQuantitySectionRowPriceKey(Ticket ticket) { 	
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow());
		}
	
		String key = "" + 
		 		+ ticket.getRemainingQuantity() + "," + section + ","
				+ row;// + "," + ticket.getPurchasePrice();

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}

	/**
	 * Return true if ticket1 is better than ticket2 by using the following rules:
	 * - cheaper price
	 * - if price are equals compare the site ranking
	 * @param ticket1
	 * @param ticket2
	 * @return
	 */
	public static boolean isBetterTicket(Ticket ticket1, Ticket ticket2) {
/*
		boolean isAdm1 = isAdmitOneTicket(ticket1);
		boolean isAdm2 = isAdmitOneTicket(ticket2);
		
		if (isAdm1 && !isAdm2) {
			return true;
		}

		if (!isAdm1 && isAdm2) {
			return false;
		}
*/
		return (ticket1.getPurchasePrice() < ticket2.getPurchasePrice()
				|| (ticket1.getPurchasePrice().equals(ticket2.getPurchasePrice()) 
						&& siteRank.get(ticket1.getSiteId()) < siteRank.get(ticket2.getSiteId())));
	}

	/**
	 * Remove duplicate tickets.
	 * @param tickets
	 * @return
	 */
	/*public static List<Ticket> removeDuplicateTickets(Collection<Ticket> tickets) {
		return removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
	}*/

	/*public static List<WebTicketRow> removeDuplicateWebTickets(Collection<WebTicketRow> webTickets) {
		// convert harvest to tickets
		Collection<Ticket> tickets = new ArrayList<Ticket>();
		for (WebTicketRow webTicketRow: webTickets) {
			tickets.add(new Ticket(webTicketRow));
		}

		List<Ticket> ticketResult = removeDuplicateTickets(tickets, RemoveDuplicatePolicy.SUPER_SMART);
		
		List<WebTicketRow> result = new ArrayList<WebTicketRow>();
		for (Ticket ticket: ticketResult) {
			result.add(new WebTicketRow(ticket));
		}
		
		return result;
	}
*/
/*	public static List<Ticket> manualRemoveDuplicateTickets(Collection<Ticket> tickets, RemoveDuplicatePolicy initialRemoveDuplicatePolicy) {
		if (tickets == null || tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}
		
		if (!initialRemoveDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			tickets = removeDuplicateTickets(tickets, initialRemoveDuplicatePolicy);
		}
		
		Integer eventId = tickets.iterator().next().getEventId();
		Map<Integer, Long> duplicateTicketMap = new HashMap<Integer, Long>();

		//This is to make sure we don't remove more than one duplicate tickets from the the same Site 
		Map<Long, Collection<String>> keyToSiteMap = new HashMap<Long, Collection<String>>();
		
		for (DuplicateTicketMap duplicateTicket: TMATDAORegistry.getDuplicateTicketMapDAO().getDuplicateTicketMap(eventId)) {
			duplicateTicketMap.put(duplicateTicket.getTicketId(), duplicateTicket.getGroupId());
		}

		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		Map<Long, Ticket> scannedTickets = new HashMap<Long, Ticket>();
		for (Ticket ticket: tickets) {
			Long key = duplicateTicketMap.get(ticket.getId()); 

			if (key == null) {
				scannedTickets.put(key, ticket);
				resultTickets.add(ticket);	
				continue;
			} else {
				Ticket duplicateTicket = scannedTickets.get(key);
				if (duplicateTicket != null && (keyToSiteMap.get(key) == null || !keyToSiteMap.get(key).contains(ticket.getSiteId()))) {
					// in case of duplicate, keep preferentially:
					// 1) cheapest ticket
					// 2) best merchant
					
					if (isBetterTicket(ticket, duplicateTicket)) {
						// removed the duplicate ticket from the list
						resultTickets.remove(duplicateTicket);
						scannedTickets.put(key, ticket);
						resultTickets.add(ticket);
						if(keyToSiteMap.get(key) == null) {
							keyToSiteMap.put(key, new ArrayList<String>());
						}
						keyToSiteMap.get(key).add(duplicateTicket.getSiteId());
					}
					// else we skip the current ticket
					if(keyToSiteMap.get(key) == null) {
						keyToSiteMap.put(key, new ArrayList<String>());
					}
					keyToSiteMap.get(key).add(ticket.getSiteId());
				}else{
        			scannedTickets.put(key, ticket);
        			resultTickets.add(ticket);
				}
			}
		}
		return resultTickets;
	}
*/
	/**
	 * Remove duplicate tickets.
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 * @throws SQLException 
	 */

	public static List<Ticket> removeDuplicateTickets(Collection<Ticket> tickets) throws SQLException {
		
		/*if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.NONE)) {
			return new ArrayList<Ticket>(tickets);
		}	
		
		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.SMART)) {
			return smartRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.REMOVE_NON_WORD_CHARACTERS);
		}
*/
//		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.SUPER_SMART)) {
			return superSmartRemoveDuplicateTickets(tickets);
//		}
/*
		if (removeDuplicatePolicy.equals(RemoveDuplicatePolicy.MANUAL)) {
			return manualRemoveDuplicateTickets(tickets, RemoveDuplicatePolicy.SMART);
		}*/
		
		/*if (tickets == null) {
			return new ArrayList<Ticket>();
		}
		
		if (tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}		

		List<Ticket> resultTickets = new ArrayList<Ticket>();

		Map<String, ArrayList<Ticket>> scannedTickets = new HashMap<String, ArrayList<Ticket>>();
		for (Ticket ticket: tickets) {
			String key = getTicketDuplicateKey(ticket); 

			ArrayList<Ticket> duplicateTickets = scannedTickets.get(key);
			if (duplicateTickets != null) {
				// in case of duplicate, keep preferentially:
				// 1) cheapest ticket
				// 2) best merchant
				boolean foundDup = false;
				for(Ticket duplicateTicket : duplicateTickets){
					if((duplicateTicket.getSeat() == null)
							|| (ticket.getSeat() == null)
							|| duplicateTicket.getSeat().isEmpty()
							|| ticket.getSeat().isEmpty()
							|| duplicateTicket.getSeat().equals("*-*")
							|| ticket.getSeat().equals("*-*")
							|| duplicateTicket.getSeat().equals(ticket.getSeat())) {
						
						foundDup = true;
						if (isBetterTicket(ticket, duplicateTicket)) {
							// removed the duplicate ticket from the list
							resultTickets.remove(duplicateTicket);
							scannedTickets.get(key).remove(duplicateTicket);
							scannedTickets.get(key).add(ticket);
							resultTickets.add(ticket);
							break;
						}
					}
				}
				if(!foundDup) {
						scannedTickets.get(key).add(ticket);
						resultTickets.add(ticket);
				}
				// else we skip the current ticket
			} else {
				ArrayList<Ticket> ticketList = new ArrayList<Ticket>();
				ticketList.add(ticket);
				scannedTickets.put(key, ticketList);
				resultTickets.add(ticket);
			}
		}
		return resultTickets;*/
	}
	
	private static Collection<Ticket> filterTicketsBySite(String siteId, Collection<Ticket> tickets) {
		Collection<Ticket> result = new ArrayList<Ticket>();
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(siteId)) {
				result.add(ticket);
			}
		}
		
		return result;
	}
	
	private static Ticket getTicketWithClosestPrice(Ticket srcTicket, Collection<Ticket> tickets) {
		if (tickets == null || tickets.isEmpty()) {
			return null;
		}
		
		Ticket closestTicket = null;
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(srcTicket.getSiteId())) {
				continue;
			}
			
			Double price = srcTicket.getPurchasePrice();
			if (closestTicket == null || Math.abs(ticket.getPurchasePrice() - price) < Math.abs(closestTicket.getPurchasePrice() - price)) {
				closestTicket = ticket;
			}
		}
		
		return closestTicket;
	}
	
	private static String[] getStartEndSeatFromSeat(String seat) {
		if (seat == null || seat.trim().isEmpty()) {
			return null;
		}

		Matcher matcher = singleSeatPattern.matcher(seat);
		if (matcher.find()) {
			return new String[]{matcher.group(1), matcher.group(1)};
		} else {
			matcher = seatRangePattern.matcher(seat);
			if (matcher.find()) {
				return new String[]{matcher.group(1), matcher.group(2)};
			}
		}
		return null;
	}
	
	public static boolean areCompatibleSeats(String seat1, String seat2) {
		if (seat1 == null || seat1.trim().isEmpty()) {
			return true;
		}
		
		if (seat2 == null || seat1.trim().isEmpty()) {
			return true;
		}
		
		String[] tok1 = getStartEndSeatFromSeat(seat1);
		String[] tok2 = getStartEndSeatFromSeat(seat2);

		if (tok1[0].equals(tok2[0]) && tok2[0].equals(tok2[0])) {
			return true;
		}

		return false;
	}
	
	/*public static Ticket getMostLikelySameTicket(Ticket srcTicket, Collection<Ticket> tickets) {
		Ticket candidate = null;
		Double difference = 0D;
		boolean isAdm1 = srcTicket.isAdmitOneTicket();
		
		for (Ticket ticket: tickets) {
			if (ticket.getSiteId().equals(srcTicket.getSiteId())) {
				continue;
			}
			
			if (!areCompatibleSeats(ticket.getSeat(), srcTicket.getSeat())) {
				continue;
			}
		
			if (candidate == null
				|| Math.abs((ticket.getPurchasePrice() - srcTicket.getPurchasePrice())) < difference) {
				boolean isAdm2 = ticket.isAdmitOneTicket();
				
				if (isAdm1 && !isAdm2 && ticket.getPurchasePrice() > srcTicket.getPurchasePrice()) {
					continue;
				}

				if (!isAdm1 && isAdm2 && ticket.getPurchasePrice() < srcTicket.getPurchasePrice()) {
					continue;
				}

				candidate = ticket;
				difference = Math.abs(ticket.getPurchasePrice() - srcTicket.getPurchasePrice());
			}
		}
		
		return candidate;
	}*/
	
	public static Ticket getTicketWithSameSeat(Ticket srcTicket, Collection<Ticket> tickets) {
		String[] seatTokens = getStartEndSeatFromSeat(srcTicket.getSeat());
		
		if (seatTokens == null) {
			return null;
		}
		
		String srcStartSeat = seatTokens[0];
		String srcEndSeat = seatTokens[1];
		
		for (Ticket ticket: tickets) {
			seatTokens = getStartEndSeatFromSeat(ticket.getSeat());
			
			if (seatTokens == null) {
				continue;
			}
			
			String startSeat = seatTokens[0];
			String endSeat = seatTokens[1];
			
			if (srcStartSeat.equals(startSeat) && srcEndSeat.equals(endSeat)) {
				return ticket;
			}
		}
		
		return null;
	}

	public static List<Ticket> smartRemoveDuplicateTickets(Collection<Ticket> tickets) {
		Map<String, Collection<Ticket>> ticketMap = new HashMap<String, Collection<Ticket>>();
				
		for (String siteId: reversedRankedSiteIds) {
			for(Ticket ticket: filterTicketsBySite(siteId, tickets)) {
				String key = getTicketDuplicateKey(ticket);
				Collection<Ticket> ticketList = ticketMap.get(key);
				if (ticketList == null) {
					ticketList = new ArrayList<Ticket>();
					ticketMap.put(key, ticketList);
					ticketList.add(ticket);
				} else {
					Ticket sameTicket = getTicketWithSameSeat(ticket, ticketList);
					if (sameTicket != null) {
						if (isBetterTicket(ticket, sameTicket)) {
							ticketList.remove(sameTicket);
							ticketList.add(ticket);
						}
						continue;
					}
					
					Ticket closestTicket = getTicketWithClosestPrice(ticket, ticketList);
					if (closestTicket == null) {
						ticketList.add(ticket);
					} else if (ticket.getPurchasePrice() > closestTicket.getPurchasePrice()) {
						if (closestTicket.getPurchasePrice() / ticket.getPurchasePrice() < SIMILARITY_PRICE_THRESHOLD_PERCENT) {
							ticketList.add(ticket);							
						}
					} else {
						if (ticket.getPurchasePrice() / closestTicket.getPurchasePrice() > SIMILARITY_PRICE_THRESHOLD_PERCENT) {
							ticketList.remove(closestTicket);
							ticketList.add(ticket);	
						} else{
							ticketList.add(ticket);		
						}
					}
				}
			}
		}

		List<Ticket> resultTickets = new ArrayList<Ticket>();
		for (Collection<Ticket> list: ticketMap.values()) {
			resultTickets.addAll(list);
		}
		return resultTickets;
	}
	/*public static double getCurrencyConversion(String siteId) {
		// note: removed SEATWAVE for this because the seatwave API
		// gives prices in USD now
		if (siteId.equals(Site.GET_ME_IN)) {
			try {
				return gbpFetcher.getCurrency();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 1.0;
	}

	//FIXME: dirty code for now
	public static double getGBPConversionRate() {
		try {
			return gbpFetcher.getCurrency();
		} catch (Exception e) {
			e.printStackTrace();
			return 1.60;
		}		
	}*/

	//FIXME: dirty code for now
	/*public static double getEURConversionRate() {
		try {
			return eurFetcher.getCurrency();
		} catch (Exception e) {
			e.printStackTrace();
			return 1.45;
		}		
	}*/

	
	/**
	 * Compute adjusted price using the price adjustment for the event and the merchant site and
	 * the rounding policy for the merchant site.
	 * @param percentAdjustment
	 * @param priceRounding
	 * @param price
	 * @param siteId
	 * @return
	 */
	public static double computeAdjustedPrice(Ticket ticket) {
	/*	Double percentAdjustment = percentAdjustmentMap.get(getAdjustKey(ticket));
		if(percentAdjustment == null){
			EventPriceAdjustment priceAdjustment = TMATDAORegistry.getEventPriceAdjustmentDAO().get(ticket.getEventId(), ticket.getSiteId());
			if (priceAdjustment != null) {
				percentAdjustment = priceAdjustment.getPercentAdjustment();
				percentAdjustmentMap.put(getAdjustKey(ticket), percentAdjustment);
			}
		}
		PriceRounding priceRounding = priceRoundingMap.get(ticket.getSiteId());
		if (priceRounding == null) {
			Site site = TMATDAORegistry.getSiteDAO().get(ticket.getSiteId());
			if (site != null) {
				priceRounding = site.getPriceRounding();
				priceRoundingMap.put(ticket.getSiteId(), priceRounding);
			}		
		}
		return computeAdjustedPrice(percentAdjustment, priceRounding, 
				ticket.getCurrentPrice(), ticket.getSiteId());
				*/
//		return ticket.getPurchasePrice();
		return ticket.getBuyItNowPrice();
	}
	/**
	 * Compute adjusted price using the price adjustment for the event and the merchant site and
	 * the rounding policy for the merchant site.
	 * @param percentAdjustment
	 * @param priceRounding
	 * @param price
	 * @param siteId
	 * @return
	 */
	/*public static double computeAdjustedPrice(Double percentAdjustment,
			PriceRounding priceRounding, double price, String siteId) {
		double adjustedPrice = price;
		if (percentAdjustment != null) {
			adjustedPrice = adjustedPrice * (1.0 + percentAdjustment / 100.0);
		}
		
		// note: removed SEATWAVE for this because the seatwave API
		// gives prices in USD now
		
		if(siteId.equals(Site.GET_ME_IN)){
			
			Double gbpConversion = null;
			try {
				gbpConversion = gbpFetcher.getCurrency();
			} catch (FeedException feed) {
				System.out.println("Caught FeedException");
				feed.printStackTrace();
			} catch (FetcherException fete) {
				System.out.println("Caught FetcherException");
				fete.printStackTrace();
			} catch (Exception e) {
				System.out.println("Caught Exception getting curremcy fetcher: " + e.getMessage());
				e.printStackTrace();
			}
			if(gbpConversion != null) {
				adjustedPrice = adjustedPrice * gbpConversion;
			}
		}
		
		if (priceRounding == null || priceRounding.equals(PriceRounding.NONE)) {
			// no change
		} else if (priceRounding.equals(PriceRounding.UP_TO_NEAREST_DOLLAR)) {
			adjustedPrice = Math.ceil(adjustedPrice);
		} else if (priceRounding.equals(PriceRounding.DOWN_TO_NEAREST_DOLLAR)) {
			adjustedPrice = Math.floor(adjustedPrice);
		}

		return adjustedPrice;
	}*/

	public static List<Ticket> removeAdmitoneTickets(Collection<Ticket> nonAOTickets,Event event,Integer ticketNetworkExchagneId,boolean allowSectionRange){
		Collection<AdmitoneInventory> admitoneTickets = null;
		try {
			admitoneTickets = DAORegistry.getAdmitoneInventoryDAO().getAllInventoryByAdmitOneEventId(ticketNetworkExchagneId);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Map<String,AdmitoneInventory> admitoneTicketMap = new HashMap<String,AdmitoneInventory>();
		if(admitoneTickets!=null){
			for(AdmitoneInventory admitoneTicket:admitoneTickets){
//				String seat = admitoneTicket.getSeat()==null?"":admitoneTicket.getSeat();
				String section = admitoneTicket.getSection()==null?"":admitoneTicket.getSection().substring(0,admitoneTicket.getSection().length()-1).toLowerCase();
				String row = admitoneTicket.getRow()==null?"":admitoneTicket.getRow().substring(0,admitoneTicket.getRow().length()-1).toLowerCase();
				String key = admitoneTicket.getQuantity() + "-" + section + "-" + row;
				admitoneTicketMap.put(key,admitoneTicket);
			}
		}
		List<Ticket> result= new ArrayList<Ticket>();
		for(Ticket ticket:nonAOTickets){
			String seat = ticket.getSeat()==null?"":ticket.getSeat();
			String section = ticket.getSection()==null?"":ticket.getSection().toLowerCase();
			String row = ticket.getRow()==null?"":ticket.getRow().toLowerCase();
			String key = ticket.getQuantity() + "-" + section + "-" + row;
			String qtySeatKey = ticket.getQuantity() + "-" + seat;
			if((!allowSectionRange && (section.contains("-") || section.contains(" or ") || row.contains("-") || row.contains(" or "))) || 
					larryQuantitySeatTickets.contains(qtySeatKey)) {
				
				continue;
			}
			
			AdmitoneInventory admitoneTicket = admitoneTicketMap.get(key);
			if(admitoneTicket !=null){
				String admitoneSeat = admitoneTicket.getSeat();
				if(seat!=null && admitoneSeat!=null && seat.equals(admitoneSeat)){
					continue;
				}else if(admitoneTicket.getCurrentPrice().equals(ticket.getCurrentPrice())){
					continue;
				}
			}
			result.add(ticket);
		}
		return result;
	}

	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	/*public static List<Ticket> filterCategory(Integer catId, Collection<Ticket> tickets) {
		if (tickets == null) {
			return null;
		}
		if (tickets.isEmpty()) {
			return null;
		}
		Category cat = ZonesDAORegistry.getCategoryDAO().get(catId);
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		for(Ticket ticket : tickets){
			Integer ticketCat = ticket.getCategoryId(cat.getGroupName()); 
			if (ticketCat != null) {
				if(ticketCat.intValue() == catId.intValue()){
					resultTickets.add(ticket);
				}
			}
		}
		
		return resultTickets;
	}*/

	/*public static String getCatScheme(List<Integer> catIds) {
		for (Integer catId: catIds) {
			if (catId > 0) { 
				return ZonesDAORegistry.getCategoryDAO().get(catId).getGroupName();
			}
		}
		return null;
	}*/
	
	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	/*public static List<Ticket> filterCategories(List<Integer> catIds, Collection<Ticket> tickets) {
		if (tickets == null) {
			return null;
		}
		
		if (tickets.isEmpty()) {
			return null;
		}
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		String catScheme = getCatScheme(catIds);
		for(Ticket ticket : tickets){
			Integer ticketCat = ticket.getCategoryId(catScheme); 
			if (ticketCat == null) {
				ticketCat = Category.UNCATEGORIZED_ID;
			} // so if we have in the filter UNCAT, we'll consider both uncat and cat

			if(catIds.contains(ticketCat)){
				resultTickets.add(ticket);
			}
		}
		
		return resultTickets;
	}*/

	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	/*public static List<Ticket> filterUncategorized(Collection<Ticket> tickets, String groupName) {
		if (tickets == null) {
			return null;
		}
		if (tickets.isEmpty()) {
			return null;
		}
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		for(Ticket ticket : tickets){
			Integer ticketCat = ticket.getCategoryId(groupName); 
			if (ticketCat == null) {
				resultTickets.add(ticket);
			}
		}
		
		return resultTickets;
	}*/

	/**
	 * Remove tickets not belonging the given category from the tickets list given as parameter.
	 * @param tickets 
	 */
	/*public static List<Ticket> filterCategorized(Collection<Ticket> tickets, String groupName) {
		if (tickets == null) {
			return null;
		}
		if (tickets.isEmpty()) {
			return null;
		}
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		
		for(Ticket ticket : tickets){
			Integer ticketCat = ticket.getCategoryId(groupName); 
			if (ticketCat != null) {
				resultTickets.add(ticket);
			}
		}
		
		return resultTickets;
	}*/

	/**
	 * Returns true if tickets contains an AdmitOne ticket.
	 * @param tickets 
	 */
	/*public static boolean containsAdmitOneTickets(Collection<Ticket> tickets) {
		if (tickets == null) {
			return false;
		}
		if (tickets.isEmpty()) {
			return false;
		}

		for(Ticket ticket : tickets){
			if (ticket.isAdmitOneTicket()) {
				return true;
			}
		}
		
		return false;
	}*/

	/**
	 * Returns true if tickets contains an AdmitOne ticket.
	 * @param tickets 
	 */
	/*public static boolean containsAdmitOneWebTickets(Collection<WebTicketRow> tickets) {
		if (tickets == null || tickets.isEmpty()) {
			return false;
		}

		for(Ticket ticket : tickets){
			if (ticket.isAdmitOneTicket()) {
				return true;
			}
		}
		
		return false;
	}
*/
	private static String stripCharactersFromString(String str){
		if (str == null) {
	        return null;
	    }

	    StringBuffer strBuff = new StringBuffer();
	    char c;
	    
	    for (int i = 0; i < str.length() ; i++) {
	        c = str.charAt(i);
	        
	        if (Character.isDigit(c)) {
	            strBuff.append(c);
	        }
	    }
	    String retString = strBuff.toString();
	    if(retString == null || retString.length() <= 0){
	    	return str;
	    }
	    return retString;
	    
	}
	/**
	 * inSectionRange
	 * 
	 * Return true if the ticket is in the section range.
	 * @param ticket
	 * @param startSection
	 * @param endSection
	 * @return
	 */
	/*public static boolean inSectionRange(Ticket ticket, String startSection, String endSection) {
		
		 * Strip out the characters from the sections. (Both the user supplied and the ticket sections)
		 * This is done because the compare function of the categorizer only compares strings of same length. (We may need to fix that)
		 
		
		String fullyNormalizedSection = stripCharactersFromString(ticket.getNormalizedSection());
		String normalizedStartSection = stripCharactersFromString(startSection);
		String normalizedEndSection = stripCharactersFromString(endSection);
		
		Integer cmp1 = Categorizer.compare(normalizedStartSection, fullyNormalizedSection);
		Integer cmp2 = Categorizer.compare(fullyNormalizedSection, normalizedEndSection);
		System.out.println("Ticket Info: section" + ticket.getSection() + " row :" + ticket.getRow() + " qty: " + ticket.getQuantity());
		System.out.println("cmp 1:" + cmp1 + "cmp 2:" + cmp2);
		if (cmp1 == null || cmp2 == null) {
				return false;
		}
		
		if (cmp1 <= 0 && cmp2 <= 0) {
			return true;
		}
		return false;
	}*/

	/**
	 * Check if a ticket belongs to a section or a section range (100-120)
	 * @param ticket
	 * @param sections
	 * @return
	 */
	/*public static boolean isInSections(Ticket ticket, String sections) {
		if(sections == null || sections.isEmpty()){
			return true;
		}
		
		if(ticket == null) {
			return false;
		}
		String[] sectionAndRange = sections.split(",");
		for(String section:sectionAndRange){
			if(section.contains("-")){
				String[] sectionRange = section.split("-");
				if(inSectionRange(ticket, sectionRange[0], sectionRange[1])) {
					return true;
				}
			} else {
				if(ticket.getNormalizedSection().compareToIgnoreCase(section)==0) {
					return true;
				}
			}
		}
		return false;
	}*/
	
	/**
	 * inRowRange
	 * 
	 * Return true if the ticket is in the row range.
	 * @param ticket
	 * @param startRow
	 * @param endRow
	 * @return
	 */
	/*public static boolean inRowRange(Ticket ticket, String startRow, String endRow) {
		Integer cmp1 = Categorizer.compare(startRow, ticket.getNormalizedRow());
		Integer cmp2 = Categorizer.compare(ticket.getNormalizedRow(), endRow);
		if (cmp1 == null || cmp2 == null) {
				return false;
		}
		
		if (cmp1 <= 0 && cmp2 <= 0) {
			return true;
		}
		return false;
	}	
	
	*//**
	 * Check if a ticket belongs to a row or a row range
	 * @param ticket
	 * @param rows
	 * @return
	 *//*
	public static boolean isInRows(Ticket ticket, String rows) {
		if(rows == null || rows.isEmpty()){
			return true;
		}
		
		if(ticket == null) {
			return false;
		}
		String[] rowAndRange = rows.split(",");
		for(String row:rowAndRange){
			if(row.contains("-")){
				String[] rowRange = row.split("-");
				if(inRowRange(ticket, rowRange[0], rowRange[1])) {
					return true;
				}
			} else {
				if(ticket.getNormalizedRow().compareToIgnoreCase(row)==0) {
					return true;
				}
			}
		}
		return false;
	}*/
	

	/**
	 * Check if a ticket is is within a price range
	 * if wholesale is true, then we compare the wholesale price, otherwise we compare the online price
	 * @param ticket
	 * @param startPrice
	 * @param endPrice
	 * @param wholesale
	 * @return
	 */
	
	public static boolean isInPriceRange(Ticket ticket, Double startPrice, Double endPrice, boolean wholesale) {
		if(startPrice == null && endPrice == null){
			return true;
		}
		if(wholesale){
			if(startPrice == null || endPrice == null) {
				if ((startPrice == null && endPrice.equals(ticket.getCurrentPrice()))
					|| (endPrice == null && startPrice.equals(ticket.getCurrentPrice()))) {
					return true;
				}
			} else {
				if((startPrice <= ticket.getCurrentPrice() && endPrice >= ticket.getCurrentPrice())
						|| (startPrice >= ticket.getCurrentPrice() && endPrice <= ticket.getCurrentPrice())) {
					return true;
				}
			}
		} else {
			if(startPrice == null || endPrice == null) {
				if ((startPrice == null && endPrice.equals(ticket.getPurchasePrice()))
					|| (endPrice == null && startPrice.equals(ticket.getPurchasePrice()))) {
					return true;
				}
			} else {
				if((startPrice <= ticket.getPurchasePrice() && endPrice >= ticket.getPurchasePrice())
						|| (startPrice >= ticket.getPurchasePrice() && endPrice <= ticket.getPurchasePrice())) {
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Return true if the ticket note contains an admitone code.
	 * @param ticketTitle
	 * @return
	 */
    /*private static boolean matchesCode(String ticketTitle) {
        String notePatterns = Constants.getInstance().getAdmit1NotePattern();
        if(notePatterns.contains(",")) {
	        String[] noteTokens = notePatterns.split(",");
	        for(int i = 0; i < noteTokens.length ; i++){
	                String notePattern = noteTokens[i];
	                if(ticketTitle.matches(".*" + notePattern + ".*")){
	                        return true;
	                }
	        }
	        return false;
        } else {
                return ticketTitle.matches(".*" + notePatterns + ".*");
        }
    }*/
    
	/*public static Collection<Ticket> getEventCategoriesLotsSimpleTicket(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName) {
		if(tickets == null) {
			return null;
		}

		Collection<Integer> excludedTicketIds = new HashSet<Integer>();
		if(usedTicketIds != null){
			excludedTicketIds.addAll(usedTicketIds);
		}

		tickets = TicketUtil.removeAdmitOneTickets(tickets);
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		for(Ticket ticket:tickets){
			
			Integer categoryId = ticket.getCategoryId(groupName);
			if(categoryId == null){
				categoryId = 0;
			}	
	
			// quantity is equals or if more then if sold we have at least a pair
			if (!excludedTicketIds.contains(ticket.getId()) && categoryIds.contains(categoryId)
					&& (ticket.getRemainingQuantity().equals(quantity) || ticket.getRemainingQuantity().compareTo(quantity + 2) >= 0)
//							lotSizes == null 
//							|| lotSizes.isEmpty()
//							|| (lotSizes.contains(ticket.getLotSize()) || lotSizes.contains(ticket.getRemainingQuantity()))
//						)
				) {
				resultTickets.add(ticket);
			} 
		}
				
		if (resultTickets == null || resultTickets.isEmpty()) {
			return null;
		}
		
		Collections.sort(resultTickets, new Comparator<Ticket>() {

			public int compare(Ticket ticket1, Ticket ticket2) {
				return ticket1.getPurchasePrice().compareTo(ticket2.getPurchasePrice());
			}
		});
		
		return resultTickets;
    }
	
	public static Collection<Ticket> getEventCategoriesLotsSimpleTicket1(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName) {
		if(tickets == null) {
			return null;
		}

		Collection<Integer> excludedTicketIds = new HashSet<Integer>();
		if(usedTicketIds != null){
			excludedTicketIds.addAll(usedTicketIds);
		}

		tickets = TicketUtil.removeAdmitOneTickets(tickets);
		tickets = TicketUtil.removeDuplicateTickets(tickets);
		
		List<Ticket> resultTickets = new ArrayList<Ticket>();
		for(Ticket ticket:tickets){
			
			Integer categoryId = ticket.getCategoryId(groupName);
			if(categoryId == null){
				categoryId = 0;
			}	
	
			// In this we dont worry about the quantity. We get all the tickets and sort it 
			if (!excludedTicketIds.contains(ticket.getId()) && categoryIds.contains(categoryId)){
				resultTickets.add(ticket);
			} 
		}
				
		if (resultTickets == null || resultTickets.isEmpty()) {
			return null;
		}
		
		Collections.sort(resultTickets, new Comparator<Ticket>() {

			public int compare(Ticket ticket1, Ticket ticket2) {
				if(ticket2.getRemainingQuantity().compareTo(ticket1.getRemainingQuantity()) > 0){
					return 1;
				}else if(ticket2.getRemainingQuantity().compareTo(ticket1.getRemainingQuantity()) == 0){
					return ticket1.getPurchasePrice().compareTo(ticket2.getPurchasePrice());					
				}else{
					return -1;
				}
				//return ticket2.getRemainingQuantity().compareTo(ticket1.getRemainingQuantity());
			}
		});
		
		return resultTickets;
    }*/	

	//assumes tickets are ordered by buyItNowPrice ASC
	/*public static SimpleTicket getLowestEventCategoriesLotsSimpleTicket(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName) {
		System.out.println("CIRCLES: tix=" + tickets);
		System.out.println("CIRCLES: catId=" + categoryIds);
		System.out.println("CIRCLES: qty=" + quantity);
		System.out.println("CIRCLES: group=" + groupName);
		Collection<Ticket> resultTickets = getEventCategoriesLotsSimpleTicket(tickets, categoryIds, quantity, usedTicketIds, groupName);
		if (resultTickets == null) {
			return null;
		}
		//Because the tickets are ordered, the first ticket is the lowest priced ticket
		return new SimpleTicket(resultTickets.iterator().next(), groupName);
	}
*/
	/*public static SimpleTicket getLowestEventCategoriesLotsSimpleTicket1(Collection<Ticket> tickets, Collection<Integer> categoryIds, Integer quantity, Collection<Integer> usedTicketIds, String groupName) {
		System.out.println("CIRCLES: tix=" + tickets);
		System.out.println("CIRCLES: catId=" + categoryIds);
		System.out.println("CIRCLES: qty=" + quantity);
		System.out.println("CIRCLES: group=" + groupName);
		Collection<Ticket> resultTickets = getEventCategoriesLotsSimpleTicket1(tickets, categoryIds, quantity, usedTicketIds, groupName);
		if (resultTickets == null) {
			return null;
		}
		//Because the tickets are ordered, the first ticket is the lowest priced ticket
		return new SimpleTicket(resultTickets.iterator().next(), groupName);
	}
	*/
	/**
	 * Assign to a set of tickets their categories
	 * @param tickets
	 * @param categories
	 * @param scheme
	 * @throws SQLException 
	 */
	public static Collection<Ticket> preAssignCategoriesToTickets(Collection<Ticket> tickets,Event event, Collection<Category> categories) throws Exception {
		Long startTime = System.currentTimeMillis();
		Map<Integer, Category> categoryMap = new HashMap<Integer, Category>();
		
		for (Category category: categories) {
			categoryMap.put(category.getId(), category);
		}
//		for (Ticket ticket: tickets) {
		
			//Call method for - computing categories of each tickets
			tickets = Categorizer.computeCategoryForTickets(tickets, categoryMap,event);
//		}
		
		System.out.println("Time Taken For preAssignCategoriesToTickets() =" + (System.currentTimeMillis() - startTime) / 1000);
		return tickets;
	}
	
	/**
	 * Duplicate key based on normSection, normRow, qty, wholesale 
	 * @param ticket
	 * @param removeDuplicatePolicy
	 * @return
	 */

	public static String getExactTicketDuplicateKey(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection());
		}
	
		String row;
		if (ticket.getRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getRow());
		}

		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + ","
				+ row + "," + normalizeSeat(ticket.getSeat()) + "," + ticket.getCurrentPrice();

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}

	
	/***
	 * 
	 */
	public static String getTicketDuplicateKeyBySectionRowSeatAndQuantity(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection()).trim();
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow()).trim();
		}
		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + "," + row + "," + normalizeSeat(ticket.getSeat()) ;

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
	/**
	 * remove tickets based on Matt's logic that gives priority to exchanges which are more likely
	 * to have duplicates
	 * @param ticket
	 * @param siteIds
	 * @param toKeepSiteId
	 * @param ticketMap
	 * @return
	 */
	private static boolean removeMostLikelySameTickets(Ticket ticket, String[] siteIds, String toKeepSiteId, Map<String, Map<String, Collection<Ticket>>> ticketMap) {
		Collection<String> includedSites = new ArrayList<String>();
		for (String siteId: siteIds) {
			includedSites.add(siteId);
		}

		Ticket ticketToRemove = null;
		Ticket ticketToAdd = null;
		
		// check that tickets within price tolerance are in the specified sites
		// BUT NOT in the other sites
		// in the other sites have to find a ticket that may potentially conflict
		
		boolean hasConflict = false;
		
		for (String siteId: rankedSiteIds) {
			if (siteId.equals(ticket.getSiteId())) {
				continue;
			}
			
			boolean isIncludedSite = includedSites.contains(siteId);
			
			if (ticketMap.get(siteId) == null) {
				if (isIncludedSite) { // not in the specified site => do nothing
					return false;	
				}
				continue;
			}
				
			String key = getTicketDuplicateKey(ticket);
			Collection<Ticket> tickets = ticketMap.get(siteId).get(key);
			if (tickets == null) {
				if (isIncludedSite) { // no tickets for a specified site => leave
					return false;
				} 
				continue;
			}
		
			Object[] closestTicketObj = getClosestTicket(ticket, tickets, false);
			if (closestTicketObj == null) {
				if (isIncludedSite) {
					return false;
				} 
				continue;
			}
			Double delta = (Double)closestTicketObj[0];
			Ticket closestTicket = (Ticket)closestTicketObj[1];
						
			if (Math.abs(delta) < SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT) {
				if (!isIncludedSite) {
					if (!seatMatch(ticket.getSeat(), closestTicket.getSeat())) {
						hasConflict = true;
						continue;
					} else {
						return false;
					}
				} else if (siteId.equals(toKeepSiteId)){
					if (delta < 0) { // cheaper find => replacement
						ticketToAdd = closestTicket;
						ticketToRemove = ticket;
					} else if (delta.equals(0D)) {
						if(siteRank.get(ticket.getSiteId()) < siteRank.get(closestTicket.getSiteId())) {
							ticketToAdd = ticket;
							ticketToRemove = closestTicket;							
						} else {
							ticketToAdd = closestTicket;
							ticketToRemove = ticket;							
						}
					} else { // remove it
						ticketToAdd = ticket;
						ticketToRemove = closestTicket;
					}
				}
			} else {
				if (isIncludedSite) {
					return false;
				} else {
					continue;
				}
			}
		}
		
		// if we survived here, then tickets were found on the included sites but not on the others
		
		// replacement and removal
//		String key = getTicketDuplicateKey(ticket, RemoveDuplicatePolicy.NONE);
		
		// removal
		/*
		Collection<Ticket> tickets = ticketMap.get(ticketToRemove.getSiteId()).get(key);
		tickets.remove(ticketToRemove);
		*/
//		ticketToRemove.setId(null);
		ticketToRemove.setDupRefTicketId(ticketToAdd.getId());
//		System.out.println("==XXXX========> FRANCOIS removing " + ticketToRemove);
		//addition
		/*
		if (ticketToAdd != null) {
			tickets = ticketMap.get(ticketToAdd.getSiteId()).get(key);
			tickets.add(ticketToAdd);			
			System.out.println("==XXXX=======> FRANCOIS adding " + ticketToAdd);
		}
		*/
		
		return true;
	}

	private static boolean removeMostLikelySameTickets(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
		boolean result = false;
		for (String siteId: new String[] {"eimarketplace", "eibox", "eventinventory", "ticketsnow", "ticketnetwork",
										  "ticketnetworkdirect", "ticketsolutions", "seatwave", "viagogo"}) {
			if (ticketMap.get(siteId) == null) {
				continue;
			}
			
			for (Collection<Ticket> tickets: new ArrayList<Collection<Ticket>>(ticketMap.get(siteId).values())) {
				for (Ticket ticket: new ArrayList<Ticket>(tickets)) {
					if (ticket.getDupRefTicketId() != null) { // skip dup ticket
						continue;
					} 
					if (!isSeatNull(ticket.getSeat())) {
						result |= removeMostLikelySameTickets(ticket, ticketMap);
					}
				}
			}
		}
		return result;
	}

	private static boolean removeMostLikelySameTickets(Ticket ticket, Map<String, Map<String, Collection<Ticket>>> ticketMap) {
		String siteId = ticket.getSiteId();
		
		if (siteId.equals("eimarketplace")
			|| siteId.equals("eibox")
			|| siteId.equals("eventinventory")
			|| siteId.equals("ticketsnow")) {
			return removeMostLikelySameTickets(ticket, new String[]{"stubhub", "ticketnetwork"}, "stubhub", ticketMap);
		} 

		if (siteId.equals("ticketnetwork") 
			|| siteId.equals("ticketnetworkdirect")) {
				return removeMostLikelySameTickets(ticket, new String[]{"stubhub", "eimarketplace"}, "eimarketplace",  ticketMap);
		}

		if (siteId.equals("ticketsolutions")) {
			return removeMostLikelySameTickets(ticket, new String[]{"ticketnetwork", "eimarketplace"}, "eimarketplace",  ticketMap)
				|| removeMostLikelySameTickets(ticket, new String[]{"stubhub", "eimarketplace"}, "eimarketplace",  ticketMap);
		}
	
		if (siteId.equals("seatwave")) {
			return removeMostLikelySameTickets(ticket, new String[]{"viagogo", "getmein"}, "viagogo",  ticketMap);
		}
		
		if (siteId.equals("viagogo")) {
			return removeMostLikelySameTickets(ticket, new String[]{"seatwave", "getmein"}, "seatwave",  ticketMap);
		}
		
		return false;
	}
	
	private static void removeSameSeatWithLowerRank(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
//		Set<String> seats = new HashSet<String>();
		Map<String, Ticket> seatMap = new HashMap<String, Ticket>();
		for (String siteId: rankedSiteIds) {
			Map<String, Collection<Ticket>> map = ticketMap.get(siteId);
			if (map == null) {
				continue;
			}
			
			for (Entry<String, Collection<Ticket>> entry: map.entrySet()) {
				Collection<Ticket> tickets = entry.getValue();
				for (Ticket ticket: tickets) {
					if (isSeatNull(ticket.getSeat())) {
						continue;
					}
					
					String seatKey = getTicketDuplicateKeyBySectionRowSeatAndQuantity(ticket);
					Ticket potentialDup = seatMap.get(seatKey); 
					if (potentialDup != null) {
						if (ticket.getCurrentPrice() >= potentialDup.getCurrentPrice()) {
							ticket.setDupRefTicketId(potentialDup.getId());
//							seatMap.put(seatKey, ticket);
						} else {
							potentialDup.setDupRefTicketId(ticket.getId());
							seatMap.put(seatKey, ticket);
						}
					} else {
						seatMap.put(seatKey, ticket);
					}
				}
			}
		}
	}
	
	 /****/
	private static Map<String, Map<String, Collection<Ticket>>> createMapByKeyAndSite(Collection<Ticket> tickets, TicketKeyCallBack callback) {
		Map<String, Map<String, Collection<Ticket>>> map = new HashMap<String, Map<String, Collection<Ticket>>>();
		
		for (String siteId: rankedSiteIds) {
			Collection<Ticket> filteredTickets = filterTicketsBySite(siteId, tickets);
			if (filteredTickets == null || filteredTickets.isEmpty()) {
				continue;
			}
			Map<String, Collection<Ticket>> ticketMap = new HashMap<String, Collection<Ticket>>();
			map.put(siteId, ticketMap);
			Collection<Ticket> sameKeyTickets =null;
			for(Ticket ticket: filteredTickets) {
				String key = callback.compute(ticket);
				sameKeyTickets = ticketMap.get(key);
				if (sameKeyTickets == null) {
					sameKeyTickets = new ArrayList<Ticket>();
					ticketMap.put(key, sameKeyTickets);
				}
				
				sameKeyTickets.add(ticket);
			}
			if(sameKeyTickets!=null && !sameKeyTickets.isEmpty()){
				
				//Collections.sort((List<Ticket>)(sameKeyTickets), new Comparator<Ticket>() {
				
				try {
				((List<Ticket>)(sameKeyTickets)).sort( new Comparator<Ticket>() {
					
					@Override
					public int compare(Ticket t1, Ticket t2) {
						int cmp = t1.getPurchasePrice().compareTo(t2.getPurchasePrice());
						
						if (cmp != 0) {
							return cmp;
						}
						
						return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
					}
				});
				}catch(Exception ex) {
					
					System.out.println("Exception in AutoPricingTicketUtil line 1769 " + ex);
				}
			}
		}
		return map;
	}

	/*private static void markAsAdmitOneTickets(Collection<Ticket> tickets) {
		for(Ticket ticket: tickets) {
			ticket.setAdmitOneTicket(true);
			ticket.setSeller("ADMT1");
		}		
	}*/

	/*private static boolean hasAdmitOneTickets(Collection<Ticket> tickets) {
		for(Ticket ticket: tickets) {
			if (ticket.getId() != null && ticket.isAdmitOneTicket()) {
				return true;
			}
		}		
		return false;
	}

	private static boolean hasSeat(String seat, Collection<Ticket> tickets) {
		for(Ticket ticket: tickets) {
			if (ticket.getId() != null && seatMatch(seat, ticket.getSeat())) {
				return true;
			}
		}		
		return false;		
	}
*/
	private static Collection<String> getSeatsFromTickets(Collection<Ticket> tickets) {
		Set<String> seats = new HashSet<String>();
		for(Ticket ticket: tickets) {
			if (ticket.getId() == null) {
				continue;
			}
			
			if (!isSeatNull(ticket.getSeat())) {
				seats.add(normalizeSeat(ticket.getSeat()));
			}
		}
		return seats;
	}
	
	private static Map<String, Collection<Ticket>> mergeTicketMap(Map<String, Map<String, Collection<Ticket>>> ticketMap, TicketMergeCallBack callback) {
		Map<String, Collection<Ticket>> result = new HashMap<String, Collection<Ticket>>();
		for (String siteId: rankedSiteIds) {
			if (ticketMap.get(siteId) == null) {
				continue;
			}
			
			for(Entry<String, Collection<Ticket>> entry: ticketMap.get(siteId).entrySet()) {
				Collection<Ticket> groupMapTickets = result.get(entry.getKey());
				
				if (groupMapTickets == null) {
					groupMapTickets = new ArrayList<Ticket>();
					result.put(entry.getKey(), groupMapTickets);
				}
				
				callback.merge(groupMapTickets, entry.getValue());
			}
		}
		return result;
	}

	public static List<Ticket> scrubExplicitlyMarkedDuplicateTickets(Collection<Ticket> tickets) throws SQLException {
		if (tickets == null || tickets.isEmpty()) {
			return new ArrayList<Ticket>();
		}
		
		Map<Integer, Ticket> ticketMap = new HashMap<Integer, Ticket>();
		for (Ticket ticket: tickets) {
			ticketMap.put(ticket.getId(), ticket);
		}
		
		List<Ticket> result = new ArrayList<Ticket>();
		Map<Integer, Long> groupIdByTicketId = new HashMap<Integer, Long>();
		Map<Long, List<Ticket>> ticketListByGroupId = new HashMap<Long, List<Ticket>>();
		
		// get duplicate marked tickets
	/*	for (DuplicateTicketMap map: DAORegistry.getDuplicateTicketMapDAO().getAllDuplicateTicketByEventId(tickets.iterator().next().getEventId())) {
			groupIdByTicketId.put(map.getTicketId(), map.getGroupId());
			List<Ticket> ticketList = ticketListByGroupId.get(map.getGroupId());
			if (ticketList == null) {
				ticketList = new ArrayList<Ticket>();
				ticketListByGroupId.put(map.getGroupId(), ticketList);
			}
			Ticket ticket = ticketMap.get(map.getTicketId());
			if (ticket != null) {
				ticketList.add(ticket);
			}
		}
		*/
		// delete based on price and exchanges
		
		for (Ticket ticket: tickets) {
			Long groupId = groupIdByTicketId.get(ticket.getId());
			if (groupId != null) {
				List<Ticket> ticketList = ticketListByGroupId.get(groupId);
				if (ticketList != null) {
					// Collections.sort(ticketList, new Comparator<Ticket>() {
					
					try {
					ticketList.sort(new Comparator<Ticket>() {
						@Override
						public int compare(Ticket t1, Ticket t2) {
							int cmp = t1.getPurchasePrice().compareTo(t2.getPurchasePrice());
							
							if (cmp != 0) {
								return cmp;
							}
							
							return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
						}
					});
					}catch(Exception ex) {
						System.out.println("Exception in Sorting AutoPricingTicketUtil at line 1897 " + ex);
					}
					ticketListByGroupId.remove(groupId);
					// mark other tix as dupe of the best one
					int i = 0;
					Ticket bestTicket = null;
					for (Ticket dupTicket: ticketList) {
						if (i++ == 0) {
							bestTicket = dupTicket;
							result.add(bestTicket);
						} else {
							dupTicket.setDupRefTicketId(bestTicket.getId());
						}
					}
				}
			} else {
				result.add(ticket);
			}
		}
		
		return result;
	}
	
	/**
	 * Implement the duplicate ticket removal logic:
	 * 1) remove dups based on section, row and wholesale
	 * 2) identify AO tickets
	 * 3) scrub other non-AO tickets
	 * @param tickets
	 * @param removeDuplicatePolicy
	 * @return
	 */
	public static List<Ticket> superSmartRemoveDuplicateTickets(Collection<Ticket> tickets) throws SQLException {
		List<Ticket> processedMarkedDupTickets = scrubExplicitlyMarkedDuplicateTickets(tickets);
		List<Ticket> result = new ArrayList<Ticket>();
		
		// group tickets by qty, section, row, price
		// siteId => (key => {tickets})
		Map<String, Map<String, Collection<Ticket>>> sameQtySecRowPriceBySiteMap
				= createMapByKeyAndSite(processedMarkedDupTickets, new TicketKeyCallBack() {
					
					public String compute(Ticket ticket) {
						return  getTicketDuplicateByQuantitySectionRowPriceKey(ticket);
					}
				});
		
		// remove same seats with lower ranking
		removeSameSeatWithLowerRank(sameQtySecRowPriceBySiteMap);
		
		// remove most likely tickets based on seats (e.g., when comparing EI: 1 with SH * and TN *)
		//removeMostLikelySameTickets(sameQtySecRowPriceBySiteMap);
		removeDuplicateBySeactionRowPrice(sameQtySecRowPriceBySiteMap);
		final Collection<Ticket> nonAOTicketContainer = new ArrayList<Ticket>();
		
		Map<String, Collection<Ticket>> ticketResultMap = mergeTicketMap(sameQtySecRowPriceBySiteMap, new TicketMergeCallBack() {
			
			public void merge(Collection<Ticket> merged, Collection<Ticket> ticketRow) {
				// simple merge
				// FIXME improve later in the case there are different seats for the same row
				// Changes by cs as added new case of different seats with same row
				/*Collection<Ticket> row = new ArrayList<Ticket>();
				if (!merged.isEmpty()) {
					List<Ticket> toMerged = new ArrayList<Ticket>(merged);
					int diff = ticketRow.size() - merged.size();
					if (diff > 0) {
						for (Ticket ticket: ticketRow) {
							if (diff-- < 0) {
								ticket.setDupRefTicketId(toMerged.get(0).getId());
							} 
							row.add(ticket);
						}
					} else {
						for (Ticket ticket: ticketRow) {
							ticket.setDupRefTicketId(toMerged.get(0).getId());
							row.add(ticket);
						}
					}
				} else {
					row.addAll(ticketRow);
				}
				
				merged.addAll(row);
				*/
				merged.addAll(ticketRow);

			}
		});
		/* cs code starts here*/
		for (Collection<Ticket> list: ticketResultMap.values()) {
				for (Ticket ticket: list) {
					if (ticket.getDupRefTicketId() == null) {
						result.add(ticket);
					}
				}
			
		}
	

		return result;
	}
	
	
	// return best match [delta, ticket1, ticket2]
	/*private static Object[] getClosestTicket(Collection<Ticket> tickets1, Collection<Ticket> tickets2) {
		Object[] bestMatch = new Object[3];
		
		for (Ticket ticket: tickets1) {
			Object[] res = getClosestTicket(ticket, tickets2, false);
			if (res == null) {
				continue;
			}
			Double delta = (Double)res[0];
			Ticket closestTicket = (Ticket)res[1];
			
			if (bestMatch[0] == null || Math.abs(delta) < Math.abs((Double)bestMatch[0])) {
				bestMatch[0] = delta;
				bestMatch[1] = ticket;
				bestMatch[2] = closestTicket;
			}
		}
		
		if (bestMatch[0] == null) {
			return null; // no match
		}
		return bestMatch;
	}*/

	// compare a ticket to a list of tickets
	// return best match [delta, ticket] or null if no match
	private static Object[] getClosestTicket(Ticket ticket, Collection<Ticket> tickets, boolean strictMatch) {
		Double ticketPrice;
		
		Ticket resultTicket = null;
		Double minDelta = 0D;
		
		if (ticket.getSiteId().equals("stubhub")) {
			ticketPrice = ticket.getPurchasePrice();
		} else {
			ticketPrice = ticket.getCurrentPrice();
		}
		
		for (Ticket curTicket: tickets) {
			if (curTicket.getDupRefTicketId() != null) { // skip already dup ticket
				continue;
			}
			// check seat compatibility => only accept if seats are STRICTLY EQUALS (*=* or A=A)
			if (strictMatch) {
				if (!normalizeSeat(ticket.getSeat()).equals(normalizeSeat(curTicket.getSeat()))) {
					continue;
				}
			} else { // otherwise check if ticket and curTicket have seats which are somehow compatible
				if (!seatMatch(ticket.getSeat(), curTicket.getSeat())) {
					continue;
				}
			}
			
			Double curTicketPrice; 
			if (curTicket.getSiteId().equals("stubhub")) {
				curTicketPrice = curTicket.getPurchasePrice();
			} else {
				curTicketPrice = curTicket.getCurrentPrice();
			}
			
			Double delta;
	
			if (curTicketPrice.equals(ticketPrice)) {
				delta = 0D;
			} else {
				if (ticket.getSiteId().equalsIgnoreCase("stubhub")) {
					if (curTicket.getCurrentPrice() < ticket.getPurchasePrice()) {
						delta = (curTicketPrice - ticketPrice) / ticketPrice;	
					} else {
						continue;
					}
				} else {
					delta = (curTicketPrice - ticketPrice) / ticketPrice;
				}
			}
			
			if (resultTicket == null || Math.abs(delta) < Math.abs(minDelta)) {
				resultTicket = curTicket;
				minDelta = delta;
			}
		}
		
		if (resultTicket == null) {
			return null;
		}
		
		return new Object[]{minDelta, resultTicket};
	}

	public static boolean isSeatNull(String seat) {
		if (seat == null) {
			return true;
		}
		
		seat = seat.trim().toUpperCase();
		
		if (seat.isEmpty()
			|| seat.equals("*")
			|| seat.equals("-")
			|| seat.equals("*-*")
			|| seat.contains("N/A")) {
			return true;
		}
		
		return false;
	}
	
	public static String normalizeSeat(String seat) {
		if (isSeatNull(seat)) {
			return "";
		}
		if(!seat.contains("-")){
			Pattern pattern = Pattern.compile("^\\d+$");
			Matcher matcher = pattern.matcher(seat);
			if(matcher.find()){
				seat=seat+"-" + seat;
			}
		}
		return seat.replaceAll("0*(\\d+)-0*(\\d+)", "$1-$2").replaceAll("^0*(\\d+)$", "$1");
	}
		
	public static boolean seatMatch(String seat1, String seat2) {
		if (isSeatNull(seat1)
			|| isSeatNull(seat2)) {
			return true;
		}
		
		String[] tokens1 = seat1.split("-");
		String[] tokens2 = seat2.split("-");
		
		Integer startSeat1;
		Integer endSeat1;
		Integer startSeat2;
		Integer endSeat2;
		
		try {
			if (tokens1.length == 2) {
				startSeat1 = Integer.valueOf(tokens1[0].trim());
				endSeat1 = Integer.valueOf(tokens1[1].trim());
			} else {
				startSeat1 = endSeat1 = Integer.valueOf(seat1.trim());
			}

			if (tokens2.length == 2) {
				startSeat2 = Integer.valueOf(tokens2[0].trim());
				endSeat2 = Integer.valueOf(tokens2[1].trim());
			} else {
				startSeat2 = endSeat2 = Integer.valueOf(seat2.trim());
			}
			
			if (startSeat1 <= startSeat2 && endSeat1 <= endSeat2) {
				return true;
			}
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
	
	private interface TicketKeyCallBack {
		String compute(Ticket ticket);
	}

	private interface TicketMergeCallBack {
		void merge(Collection<Ticket> merged, Collection<Ticket> ticketRow);
	}
/*
	private static AdmitoneInventory getInventory(Ticket ticket, Collection<AdmitoneInventory> inventories) {
		TourType type = ticket.getEvent().getEventType();
		
		if (ticket == null || ticket.getSeller() == null || ticket.getSection() == null || ticket.getRow() == null || inventories == null) {
			return null;
		}
		
		for (AdmitoneInventory admitoneInventory: inventories) {
			if (admitoneInventory == null || admitoneInventory.getBroadcastSeller() == null
				|| admitoneInventory.getSection() == null || admitoneInventory.getRow() == null) {
				continue;
			}
			
			Integer quantity = admitoneInventory.getQuantity();
			String seller = admitoneInventory.getBroadcastSeller();
			String section = SectionRowStripper.strip(type, admitoneInventory.getSection().replaceAll("[-\\.]", "")).trim().toUpperCase();
			String row = SectionRowStripper.strip(type, admitoneInventory.getRow().replaceAll("[-\\.]", "")).trim().toUpperCase();
			String seat = admitoneInventory.getSeat().replaceAll("\\*-\\*", "").trim();
			Double price = (admitoneInventory.getCurrentPrice() != null) ? admitoneInventory.getCurrentPrice(): 0D;
			String ticketSection = (ticket.getNormalizedSection() != null) ? ticket.getNormalizedSection().replaceAll("[-\\.]", "").trim(): "";
			String ticketRow = (ticket.getNormalizedRow() != null) ? ticket.getNormalizedRow().replaceAll("[-\\.]", "").trim(): "";
			String ticketSeat = (ticket.getSeat() != null) ? ticket.getSeat().replaceAll("\\*-\\*", "").trim(): "";
			Double ticketPrice = (ticket.getPurchasePrice() != null) ? ticket.getPurchasePrice(): 0D;
			

			if (quantity.equals(ticket.getRemainingQuantity())
				&& (seller.equals("all")
					|| seller.contains(ticket.getSiteId())
					|| ((seller.contains("eimarketplace") || seller.contains("eibox")) && (ticket.getSiteId().contains("eimarketplace") || ticket.getSiteId().contains("eibox")))
					|| ((seller.contains("ticketnetwork") || seller.contains("ticketnetworkdirect")) && (ticket.getSiteId().contains("ticketnetwork") || ticket.getSiteId().contains("ticketnetworkdirect")))
					)
				&& section.equalsIgnoreCase(ticketSection)
				&& row.equalsIgnoreCase(ticketRow)) {

				// quick fix for seatwave
				// seatwave tix are listed +10% higher
				
				if (ticket.getSiteId().equals("seatwave")) {
					if (!price.equals(1.1D * ticketPrice)) {
						continue;
					}
				} else if (!price.equals(ticketPrice)) {
					continue;
				}
				
				//Will check if seat exactly matches like *=* or number=number or nonadmitone ticket falls in the range of admitone ticket
//				if((isSeatNull(seat) && isSeatNull(ticketSeat))||(!isSeatNull(seat) && !isSeatNull(ticketSeat)
//					&& seatMatch(normalizeSeat(seat), normalizeSeat(ticket.getSeat())))) {
//					return admitoneInventory;
//				}
				if (isSeatNull(seat) || isSeatNull(ticketSeat)
					|| seatMatch(normalizeSeat(seat), normalizeSeat(ticket.getSeat()))) {
					return admitoneInventory;
				}
			}
		}
		return null;
	}

	public static boolean isInInventory(Ticket ticket, Collection<AdmitoneInventory> inventories) {
		return getInventory(ticket, inventories) != null;
	}
*/	
	/*private static Map<String, Collection<AdmitoneInventory>> createInventoryMapBySectionAndRow(Collection<AdmitoneInventory> inventories, TourType type) {
		// map section-row => {inventory}
		Map<String, Collection<AdmitoneInventory>> inventoryMap = new HashMap<String, Collection<AdmitoneInventory>>(); 
		
		for (AdmitoneInventory inventory: inventories) {
			String section = SectionRowStripper.strip(type, inventory.getSection().replaceAll("[-\\.]", "")).trim().toUpperCase();
			String row = SectionRowStripper.strip(type, inventory.getRow().replaceAll("[-\\.]", "")).trim().toUpperCase();
			String key = section + "-" + row;
			
			Collection<AdmitoneInventory> inventoryList = inventoryMap.get(key);
			if (inventoryList == null) {
				inventoryList = new ArrayList<AdmitoneInventory>();
				inventoryMap.put(key, inventoryList);
			}
			inventoryList.add(inventory);
		}
		return inventoryMap;
	}
	*/

	private AutoPricingTicketUtil() {} // prevent from instantiating this utility class
	private static void removeDuplicateBySeactionRowPrice(Map<String, Map<String, Collection<Ticket>>> ticketMap) {
//		Set<String> seats = new HashSet<String>();
		Map<String, List<Ticket>> seatMap = new HashMap<String, List<Ticket>>();
		List<Integer> ticketRestoreMap= new ArrayList<Integer>();
		for (String siteId: rankedSiteIds) {
			Map<String, Collection<Ticket>> map = ticketMap.get(siteId);
			if (map == null) {
				continue;
			}
			
			
			for (Entry<String, Collection<Ticket>> entry: map.entrySet()) {
				Collection<Ticket> tickets = entry.getValue();
				for (Ticket ticket: tickets) {
					if(ticket.getDupRefTicketId()!=null){
						continue;
					}
					String seatKey = getTicketDuplicateKeyBySectionRowAndQuantity(ticket);
					List<Ticket> potentialDupList = seatMap.get(seatKey); 
					if (potentialDupList != null && !potentialDupList.isEmpty()) {
						boolean flag=true;
						for(Ticket potentialDupTicket:potentialDupList){
							if(!potentialDupTicket.getSiteId().equals(ticket.getSiteId())){
								if(ticketRestoreMap.contains(potentialDupTicket.getId())){
									continue;
								}
								if(isSeatNull(potentialDupTicket.getSeat()) || isSeatNull(ticket.getSeat())){
//									System.out.println(potentialDupTicket.getCurrentPrice()+ ":" + ticket.getCurrentPrice());
//									System.out.println("Kem:" + (potentialDupTicket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT))+ ":" + ticket.getCurrentPrice());
//									System.out.println("Kem-1" + (potentialDupTicket.getCurrentPrice() + ":" + (ticket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT))));
//									System.out.println(potentialDupTicket.getCurrentPrice()<ticket.getCurrentPrice());
//									System.out.println(((potentialDupTicket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)) >= ticket.getCurrentPrice()));
									if((potentialDupTicket.getCurrentPrice().equals(ticket.getCurrentPrice())) ||
									(potentialDupTicket.getCurrentPrice()<ticket.getCurrentPrice() && 
									((potentialDupTicket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)) >= ticket.getCurrentPrice()))){
										ticket.setDupRefTicketId(potentialDupTicket.getId());
										ticketRestoreMap.add(potentialDupTicket.getId());
										flag=false;
										break;
									}else if(potentialDupTicket.getCurrentPrice()>ticket.getCurrentPrice() && 
											(potentialDupTicket.getCurrentPrice() <= (ticket.getCurrentPrice()* (1 + SIMILARITY_PRICE_DIFF_THRESHOLD_PERCENT)))){
										potentialDupTicket.setDupRefTicketId(ticket.getId());
										potentialDupList.remove(potentialDupTicket);
										addInList(potentialDupList,ticket);
										flag=false;
										break;
									}
								
								}else if (potentialDupTicket.getSeat().equals(ticket.getSeat())){
									if(potentialDupTicket.getCurrentPrice()>ticket.getCurrentPrice()){
										potentialDupTicket.setDupRefTicketId(ticket.getId());
										potentialDupList.remove(potentialDupTicket);
										addInList(potentialDupList,ticket);
										flag=false;
										break;
//										seatMap.put(seatKey, ticket);
									}else{
										ticket.setDupRefTicketId(potentialDupTicket.getId());
									}
								}
							}else{
								addInList(potentialDupList,ticket);
								flag=false;
								break;
							}
						}
						if(flag){
							addInList(potentialDupList,ticket);
						}
					} else {
						potentialDupList=new ArrayList<Ticket>();
						addInList(potentialDupList,ticket);
						seatMap.put(seatKey, potentialDupList);
					}
				}
			}
			ticketRestoreMap.clear();
		}
	}
	public static String getTicketDuplicateKeyBySectionRowAndQuantity(Ticket ticket) {
		String section;
		if (ticket.getNormalizedSection() == null) {
			section = "";
		} else {
			section = TextUtil.removeNonWordCharacters(ticket.getNormalizedSection()).trim();
		}
	
		String row;
		if (ticket.getNormalizedRow() == null) {
			row = "";
		} else {
			row = TextUtil.removeNonWordCharacters(ticket.getNormalizedRow()).trim();
		}

		String key = ""
			 	+ ticket.getTicketStatus()
		 		+ "," + ticket.getRemainingQuantity() + "," + section + "," + row ;

		String duplicateKeyHash;
		
		synchronized (md5MessageDigest) {			
			duplicateKeyHash = new String(Hex.encodeHex(md5MessageDigest.digest(key.getBytes())));
		}

		return duplicateKeyHash;
	}
		
	public static void addInList(List<Ticket> list,Ticket ticket){
		list.add(ticket);
		
		try {
			
			//Collections.sort(list, new Comparator<Ticket>() {
			
			list.sort(new Comparator<Ticket>() {
			@Override
			public int compare(Ticket t1, Ticket t2) {
				int cmp = t1.getPurchasePrice().compareTo(t2.getPurchasePrice());
				
				if (cmp != 0) {
					return cmp;
				}
				
				return siteRank.get(t1.getSiteId()).compareTo(siteRank.get(t1.getSiteId()));
			}
		});
		}catch(Exception ex) {
			System.out.println("Exception in sorting AutoPricingTicketUtil line no 2381 " + ex);
		}
	}
	/**
	 * Tell if a quantity can be a partition of a bigger quantity.
	 * For instance a ticket of quantity 5 can be subdivided in 1, 2, 3, 5
	 * @param quantity
	 * @param totalQuantity
	 * @return
	 */
	public static Integer[] getPartition(int quantity) {
		
		if(quantity == 1){
			return new Integer[]{1};
		}else if(quantity == 2){
			return new Integer[]{2};
		}else if(quantity == 3) {
			return new Integer[]{1,3};
		}else if(quantity == 4) {
			return new Integer[]{2,4};
		}else if(quantity == 5) {
			return new Integer[]{1,2,3,5};
		}else if(quantity == 6) {
			return new Integer[]{1,2,4,6};
		}else if(quantity == 7) {
			return new Integer[]{1,2,3,4,5,7};
		}else if(quantity == 8) {
			return new Integer[]{1,2,3,4,5,6,8};
		}else if(quantity == 9) {
			return new Integer[]{1,2,3,4,5,6,7,9};
		}else if(quantity == 10) {
			return new Integer[]{1,2,3,4,5,6,7,8,10};
		}else if(quantity == 11) {
			return new Integer[]{1,2,3,4,5,6,7,8,9,11};
		}else{
			return new Integer[]{1,2,3,4,5,6,7,8,9,10,12};
		}
	}
	
	 /*public static Collection<Ticket> removeLockedTickets(Collection<Ticket> tickets,Integer eventId){
//		 Map<Integer,LockedTicket> map = LockedTicket.getLockedTicketSessionMap();
		 List<LockedTicket> list = ZonesDAORegistry.getLockedTicketDAO().getLockedTicketByEventId(eventId);
		 Collection<Ticket> ticketList= new ArrayList<Ticket>();
		if(list !=null && !list.isEmpty()){
			for(Ticket ticket:tickets){
				 boolean flag=false;
				 for(LockedTicket lockedTicket:list){
					 if(ticket.getItemId().equals(lockedTicket.getItemId())){
						 flag=true;
						 break;
					 }
				 }
				 if(!flag){
					 ticketList.add(ticket);
				 }
			 }
			 return ticketList;	
		}
		return tickets;
		 
	 }*/
	public static List<Integer> getMatchingQuantities(int quantity) {
		List<Integer> result = new ArrayList<Integer>();
		
		result.add(quantity);
		
		for (int q = quantity + 2; q < MAX_QTY; q++) {
			result.add(q);
		}
		
		return result;
	}
	
	public String[] getRankedSiteIds() {
		return rankedSiteIds;
	}
	
	public static void getPurchasePrice(Ticket t,Map<String, DefaultPurchasePrice> defaultPurchasePriceMap,Integer lotSize,Map<String, ManagePurchasePrice> tourPriceMap){
		Double tempPrice = t.getCurrentPrice();
		if (lotSize==null || lotSize==0){
			lotSize = t.getQuantity();
		}
//		Event event = t.getEvent();
//		Map<String, ManageTourPrice> tourPriceMap = new HashMap<String, ManageTourPrice>();
//		Long time = manageTourPriceTimeMap.get(tour.getTmatTourId());
//		if(tourPriceMap==null){
//			tourPriceMap= new HashMap<String, ManageTourPrice>();
//		}
		String mapKey = t.getSiteId()+ "-" + (t.getTicketDeliveryType()==null?"REGULAR":t.getTicketDeliveryType());
//		Long now =new Date().getTime();
//		if(tourPriceMap.get(mapKey)==null || now-time>60*60*1000){
//			System.out.println("DB called for manage price stated @ " + new Date());
//			for(ManageTourPrice tourPrice:manageTourPricelist){
//				tourPriceMap.put(tourPrice.getExchange()+"-" + tourPrice.getTicketType(),tourPrice);
//			}
//			System.out.println("DB called for manage price ended @ " + new Date());
//			manageTourPriceMap.put(tour.getTmatTourId(),tourPriceMap);
//			manageTourPriceTimeMap.put(tour.getTmatTourId(),now);
//		}
		ManagePurchasePrice managePurchasePrice=tourPriceMap.get(mapKey);
		
		if(managePurchasePrice!=null){
			double serviceFee=managePurchasePrice.getServiceFee();
			double  shippingFee=managePurchasePrice.getShipping();
			int currencyType=managePurchasePrice.getCurrencyType();
			tempPrice=(currencyType==1?((lotSize * tempPrice*(1+serviceFee/100)) + shippingFee)/lotSize:((lotSize * tempPrice) + serviceFee + shippingFee)/lotSize);
		}else{
			DefaultPurchasePrice defaultPurchasePrice = defaultPurchasePriceMap.get(mapKey);
			if(defaultPurchasePrice!=null){
				double serviceFee=defaultPurchasePrice.getServiceFees();
				double  shippingFee=defaultPurchasePrice.getShipping();
				int currencyType=defaultPurchasePrice.getCurrencyType();
				tempPrice=(currencyType==1?((lotSize * tempPrice*(1+serviceFee/100)) + shippingFee)/lotSize:((lotSize * tempPrice) + serviceFee + shippingFee)/lotSize);
			}
		}
		t.setPurchasePrice(tempPrice);
	}
	/*public static Ticket getTicketForSection(Set<Ticket> sectionTickets,Double rptFactor) throws Exception {
		Set<Ticket> sectionTicketsForQty4 = new TreeSet<Ticket>();
		Boolean twoEntryFlag = true;
		for (Ticket ticket : sectionTickets) {
			if(ticket.getRemainingQuantity() >=4) {
				sectionTicketsForQty4.add(ticket);
			}
		}
		Ticket ticket = null;
		if(!sectionTicketsForQty4.isEmpty()) {
			ticket = TicketUtil.getCheapestTicketForSection(sectionTicketsForQty4, rptFactor);	
		}
		if(null == ticket) {
			twoEntryFlag = false;
			ticket = getCheapestTicketForSection(sectionTickets, rptFactor);
		}
		
		return ticket;
	}*/
	
	public static Comparator<Ticket> ticketSortingComparator = new Comparator<Ticket>() {

		public int compare(Ticket ticket1, Ticket ticket2) {
			int cmp = ticket1.getPurchasePrice().compareTo(
					ticket2.getPurchasePrice());
			if (cmp < 0) {
				return -1;
			}
			if (cmp > 0) {
				return 1;
			}
			// if same price get the one with less quantity first
			int cmp2 = ticket1.getQuantity().compareTo(
					ticket2.getQuantity());
			if (cmp2 < 0) {
				return -1;
			}

			if (cmp2 > 0) {
				return 1;
			}
			return ticket1.getId().compareTo(ticket2.getId());
	    }};
	    
	public static void getCheapestTicketByQty(Set<Ticket> sectionTickets,ExchangeEvent exEvent,
			int quantity,int sectionEligibleMinEntries,CategoryTicket categoryTicket) throws Exception {
		
		
		Set<Ticket> sectionTicketsForQty = new TreeSet<Ticket>(new Comparator<Ticket>() {
			public int compare(Ticket ticket1, Ticket ticket2) {
				int cmp = ticket1.getPurchasePrice().compareTo(
						ticket2.getPurchasePrice());
				if (cmp < 0) {
					return -1;
				}
				if (cmp > 0) {
					return 1;
				}
				// if same price get the one with less quantity first
				int cmp2 = ticket1.getQuantity().compareTo(
						ticket2.getQuantity());
				if (cmp2 < 0) {
					return -1;
				}

				if (cmp2 > 0) {
					return 1;
				}
				return ticket1.getId().compareTo(ticket2.getId());
			}	

		});
		
		
		for (Ticket ticket : sectionTickets) {
			for(int qty : getPartition(ticket.getQuantity())){
				if(quantity==qty){
					sectionTicketsForQty.add(ticket);
					break;
				}
			}
		}
		
		if(!sectionTicketsForQty.isEmpty() && sectionTicketsForQty.size() >= sectionEligibleMinEntries) {
			
			getSafeCheapestTicket(sectionTicketsForQty, exEvent,categoryTicket);
		}
//		return categoryTicket;
	}
	
	
	
	private static void getSafeCheapestTicket(Set<Ticket> sortedTickets,
			ExchangeEvent exchangeEvent,CategoryTicket categoryTicket){
		
		Integer baseTicketOne,baseTicketTwo, baseTicketThree;
		Double baseTicketOnePurPrice,baseTicketTwoPurPrice, baseTicketThreePurPrice;
//		LastRowMiniCatsCategoryTicket categoryTicket = null;
		
		Integer exposureAmt = Integer.parseInt(exchangeEvent.getExposure().split("-")[0]);
		
		//Exposure hot coded to 1-OXP for allow section range events section range sections.
		/*if(isAllowRangeSection) {
			exposureAmt = 1;
		}*/
//		String exposuretype = exposure.split("-")[1];

		Iterator<Ticket> iterator = sortedTickets.iterator();
		Iterator<Ticket> currentTicketIterator = sortedTickets.iterator();
		if(!iterator.hasNext()){
			return ;
		}
		Ticket currentTicket=null;
		Ticket nextTicket=null;
			currentTicket= iterator.next();
			currentTicketIterator.next();
		int count=0;
		for(Ticket ticket=currentTicket;;ticket=currentTicketIterator.next()){
			try{
				baseTicketOne = 0;
				baseTicketTwo = 0;
				baseTicketThree=0;
				baseTicketOnePurPrice = 0.0;
				baseTicketTwoPurPrice = 0.0;
				baseTicketThreePurPrice =0.0;
				
				Map<Integer, Ticket> map = new HashMap<Integer, Ticket>();
				count++;
				int ii=0;
				Iterator<Ticket> nextTicketIterator = sortedTickets.iterator();
				while(nextTicketIterator.hasNext() && ii<count){
					nextTicketIterator.next();
					ii++;
				}
				if(nextTicketIterator.hasNext()){
					nextTicket=nextTicketIterator.next();
				}else if(1!=exposureAmt){
					return ;
				}
				
				boolean flag=false;
				if(1==exposureAmt){
					categoryTicket.createCategoryTicket(ticket);
					categoryTicket.setBaseTicketOne(baseTicketOne);
					categoryTicket.setBaseTicketTwo(baseTicketTwo);
					categoryTicket.setBaseTicketThree(baseTicketThree);
					categoryTicket.setBaseTicketOnePurPrice(baseTicketOnePurPrice);
					categoryTicket.setBaseTicketTwoPurPrice(baseTicketTwoPurPrice);
					categoryTicket.setBaseTicketThreePurPrice(baseTicketThreePurPrice);
					break;
				}else{
					int j=1;
					for(int i=exposureAmt ;i>1  && iterator != null;i--) {
						if(ticket.getPurchasePrice()*(1+exchangeEvent.getRptFactor()/100)>=(nextTicket.getPurchasePrice())){
							if(j==1) {
								baseTicketOne = nextTicket.getId();
								baseTicketOnePurPrice = nextTicket.getPurchasePrice();
							} else if(j==2) {
								baseTicketTwo = nextTicket.getId();
								baseTicketTwoPurPrice = nextTicket.getPurchasePrice();
							}
							else if(j==3) {
								baseTicketThree = nextTicket.getId();
								baseTicketThreePurPrice = nextTicket.getPurchasePrice();
							}
							
							j++;
							map.put(1 + exposureAmt-i, ticket);
							
							if(j==exposureAmt){
								break;
							}
							if(nextTicketIterator.hasNext() ){
								nextTicket=nextTicketIterator.next();
							}else{
								flag=true;
								break;
							}
						}else{
							flag=true;
							break;
						}
					}
					if(!flag){
						categoryTicket.createCategoryTicket(map.get(1));
						categoryTicket.setBaseTicketOne(baseTicketOne);
						categoryTicket.setBaseTicketTwo(baseTicketTwo);
						categoryTicket.setBaseTicketThree(baseTicketThree);
						categoryTicket.setBaseTicketOnePurPrice(baseTicketOnePurPrice);
						categoryTicket.setBaseTicketTwoPurPrice(baseTicketTwoPurPrice);
						categoryTicket.setBaseTicketThreePurPrice(baseTicketThreePurPrice);
						break;
					}
				}
			}catch (Exception e) {
				System.out.println( exchangeEvent.getEvent().getId() + ":" +  e.fillInStackTrace());
			}
			if(!currentTicketIterator.hasNext()){
				break;
			}
		}
//		return null;
	}
	
	
	public static List<ExchangeEvent> sortExchangeEvents(List<ExchangeEvent> eventCategories){
		
		List<ExchangeEvent> finalEventList = new ArrayList<ExchangeEvent>();
		
		try{
			
			Collections.sort(eventCategories,new Comparator<ExchangeEvent>() {
				
				public int compare(ExchangeEvent tgcatEventOne, ExchangeEvent tgcatEventTwo) {
					try {	
					if(null == tgcatEventOne) {
						return 1;
					} 
					if(null == tgcatEventTwo) {
						return -1;
					}
					
					if(null == tgcatEventOne.getEvent()) {
						return 1;
					} 
					if(null == tgcatEventTwo.getEvent()) {
						return -1;
					} 
					
					if(null == tgcatEventOne.getEvent().getLocalDate()) {
						return 1;
					} 
					
					if(null == tgcatEventTwo.getEvent().getLocalDate()) {
						return -1;
					} 
					return tgcatEventOne.getEvent().getLocalDate().compareTo(tgcatEventTwo.getEvent().getLocalDate());
					
					} catch(Exception e) {
						//e.printStackTrace();
						return -1;
					}
				}
			});
			
		finalEventList.addAll(eventCategories);

		
		return finalEventList;
	}catch(Exception e){
		e.printStackTrace();
	}
	return eventCategories;
	}
	
	public static String getRowRangeForSectionRangeTickets(String key,Map<String,CategoryMapping> rowRangeMap)   {
		String rowRange = "";
		
		try {
			String[] strKeyArr = key.split("-");
			
			if(strKeyArr.length == 2) {
				boolean isInt=false;
				Integer intStartSection =0;
				Integer intEndSection =0; 
				try {
					intStartSection= Integer.parseInt(strKeyArr[0]);
					intEndSection= Integer.parseInt(strKeyArr[1]);
					isInt=true;
				}catch(Exception e) {
					
				}
				
				if(isInt) {
					for (String strSection : rowRangeMap.keySet()) {
						if(strSection != null) {
							String[] strArr = strSection.split("-");
							if(strArr.length == 2) {
								try {
									Integer startValue = Integer.parseInt(strArr[0]);
									Integer endValue = Integer.parseInt(strArr[1]);
									
									if(startValue <= intStartSection && intEndSection <= endValue) {
										CategoryMapping map = rowRangeMap.get(strSection);
										if(map != null && map.getRowRange() != null) {
											rowRange = map.getRowRange();
											break;
										}
									}
									
								}catch(Exception e) {
									
								}
							}
						}
					}
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		return rowRange;
	}
	
	/*public static Map<String, Set<Ticket>> getSectionRangeTicketsFromUniqueSections(Map<String, Set<Ticket>> sectionTicketMap) {
		String intRegex = "[0-9]+";
		String alphabetRegex = "[a-z]";

		List<String> keyList = new ArrayList<String>(sectionTicketMap.keySet());
		Map<String, Set<Ticket>> finalMap = new HashMap<String, Set<Ticket>>();
		
		for (String sectionRange : keyList) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(sectionRange);
			
			if(sectionRange.contains("-")) {
				try {
					Ticket sectionTicket = sectionTickets.iterator().next();
					String[] sectionRangeArr = sectionRange.split("-");
					String startSection = sectionRangeArr[0];
					String endsection = sectionRangeArr[1];
					Boolean isIntFlag = false,isStringFlag = false;
					Integer startIntValue = null,endIntValue=null,tempIntValue=null;
					
					
					if(startSection.matches(intRegex) && endsection.matches(intRegex)) {
						isIntFlag = true;
						startIntValue = Integer.parseInt(startSection);
						endIntValue = Integer.parseInt(endsection);
							
					} else if(startSection.toLowerCase().matches(alphabetRegex) && endsection.toLowerCase().matches(alphabetRegex)) {
						isStringFlag = true;
						startIntValue = (int) startSection.toLowerCase().charAt(0);
						endIntValue = (int) endsection.toLowerCase().charAt(0);
						
					} else {
						continue;
					}
					
					for (String section : keyList) {
						if(!section.contains("-")) {
							boolean isValidSection = false;
							try {
								if(isIntFlag && section.matches(intRegex)) {
									tempIntValue = Integer.parseInt(section);
									
									if(startIntValue <= tempIntValue && tempIntValue <= endIntValue) {
										isValidSection = true;
										//sectionTickets.addAll(sectionTicketMap.get(section));
									}
									
								} else if(isStringFlag && section.matches(alphabetRegex)) {
									tempIntValue = (int)section.toLowerCase().charAt(0);
									
									if(startIntValue <= tempIntValue && tempIntValue <= endIntValue) {
										isValidSection = true;
										//sectionTickets.addAll(sectionTicketMap.get(section));
									}
								}
								
								if(isValidSection) {
									for (Ticket tix : sectionTicketMap.get(section)) {
										Ticket tempTix = new Ticket(tix);
										tempTix.setNormalizedSection(sectionTicket.getNormalizedSection());
										sectionTickets.add(tempTix);
									}
								}
								
							} catch(Exception e){
								//e.printStackTrace();
							}
						}
					}
					
				} catch(Exception  e) {
					//e.printStackTrace();
				}
			}
			finalMap.put(sectionRange, sectionTickets);
		}
		
		return finalMap;
	}*/
	
	public static void main(String[] args) throws Exception {
		
		String str = " ";
		System.out.println("....."+str.toLowerCase());
		
		String tempSec = "101.--";
		String tempRow = "1-25-";
		int qty = 1;
		
		
		String section = (tempSec==null || tempSec.equals(""))?"":tempSec.replaceAll("[-]$", "").toLowerCase();
	    String row = (tempRow==null || tempRow.equals(""))?"":tempRow.replaceAll("[-]$", "").toLowerCase();
		
		/*String section = tempSec==null?"":tempSec.substring(0,tempSec.length()-1).toLowerCase();
		String row = tempRow==null?"":tempRow.substring(0,tempRow.length()-1).toLowerCase();*/
		String key = qty + "-" + section + "-" + row;
		
		System.out.println("key.......1...."+key);
		
		
		 tempSec = "101";
		 tempRow = "1-25";
		 qty = 1;
		
		 section = tempSec==null?"":tempSec.toLowerCase();//.substring(0,tempSec.length()-1)
		row = tempRow==null?"":tempRow.toLowerCase();//.substring(0,tempRow.length()-1)
		 key = qty + "-" + section + "-" + row;
		
		System.out.println("key.......1...."+key);
		
		/*List<Ticket> ticketList = DAORegistry.getTicketDAO().getAllActiveTicketByEventId(1000087597);
		
		ticketList = removeDuplicateTickets(ticketList);
		
		Collection<Ticket> finalList = removeAdmitoneTickets(ticketList, null, null, true);
		
		ticketList.removeAll(finalList);
		
		System.out.println("duplicat list..."+ticketList.size()+"..fina..."+finalList.size());
		
		for (Ticket ticket : ticketList) {
			System.out.println("id.."+ticket.getId()+"..sec..."+ticket.getSection()+"..row.."+ticket.getRow()+"..qty.."+ticket.getQuantity());
		}
		System.out.println("::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::");
		
		for (Ticket ticket : finalList) {
			System.out.println("id.."+ticket.getId()+"..sec..."+ticket.getSection()+"..row.."+ticket.getRow()+"..qty.."+ticket.getQuantity());
		}*/
		
		
	}
	
	
	public static Map<String, Set<Ticket>> getSectionRangeTicketsFromUniqueSections(Map<String, Set<Ticket>> sectionTicketMap) {
		String intRegex = "[0-9]+";
		String alphabetRegex = "[a-z]";

		List<String> keyList = new ArrayList<String>(sectionTicketMap.keySet());
		Map<String, Set<Ticket>> finalMap = new HashMap<String, Set<Ticket>>();
		
		for (String sectionRange : keyList) {
			Set<Ticket> sectionTickets = sectionTicketMap.get(sectionRange);
			
			if(sectionRange.contains("-")) {
				try {
					Ticket sectionTicket = sectionTickets.iterator().next();
					String[] sectionRangeArr = sectionRange.split("-");
					String startSection = sectionRangeArr[0];
					String endsection = sectionRangeArr[1];
					Boolean isIntFlag = false,isStringFlag = false;
					Integer startIntValue = null,endIntValue=null,tempIntValue=null;
					
					
					if(startSection.matches(intRegex) && endsection.matches(intRegex)) {
						isIntFlag = true;
						startIntValue = Integer.parseInt(startSection);
						endIntValue = Integer.parseInt(endsection);
							
					} else if(startSection.toLowerCase().matches(alphabetRegex) && endsection.toLowerCase().matches(alphabetRegex)) {
						isStringFlag = true;
						startIntValue = (int) startSection.toLowerCase().charAt(0);
						endIntValue = (int) endsection.toLowerCase().charAt(0);
						
					} else {
						continue;
					}
					
					for (String section : keyList) {
						if(!section.contains("-")) {
							boolean isValidSection = false;
							try {
								if(isIntFlag && section.matches(intRegex)) {
									tempIntValue = Integer.parseInt(section);
									
									if(startIntValue <= tempIntValue && tempIntValue <= endIntValue) {
										isValidSection = true;
										//sectionTickets.addAll(sectionTicketMap.get(section));
									}
									
								} else if(isStringFlag && section.matches(alphabetRegex)) {
									tempIntValue = (int)section.toLowerCase().charAt(0);
									
									if(startIntValue <= tempIntValue && tempIntValue <= endIntValue) {
										isValidSection = true;
										//sectionTickets.addAll(sectionTicketMap.get(section));
									}
								}
								
								if(isValidSection) {
									for (Ticket tix : sectionTicketMap.get(section)) {
										Ticket tempTix = new Ticket(tix);
										tempTix.setNormalizedSection(sectionTicket.getNormalizedSection());
										sectionTickets.add(tempTix);
									}
								}
								
							} catch(Exception e){
								//e.printStackTrace();
							}
						}
					}
					
				} catch(Exception  e) {
					//e.printStackTrace();
				}
			}
			finalMap.put(sectionRange, sectionTickets);
		}
		
		return finalMap;
	}
}