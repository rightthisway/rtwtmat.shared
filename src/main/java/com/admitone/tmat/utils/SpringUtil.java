package com.admitone.tmat.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.admitone.tmat.crawler.TicketListingCrawler;
import com.admitone.tmat.ebay.EbayManager;
import com.admitone.tmat.ebay.ListEasyManager;
import com.admitone.tmat.utils.mail.MailManager;

public final class SpringUtil implements ApplicationContextAware {
	private static ApplicationContext context;

	public static Object getBean(String name) {
		return context.getBean(name);
	}
	
	public static MailManager getMailManager() {
		return (MailManager)getBean("mailManager");
	}

	public static PreferenceManager getPreferenceManager() {
		return (PreferenceManager)getBean("preferenceManager");
	}

	public static ListEasyManager getListEasyManager() {
		return (ListEasyManager)getBean("listEasyManager");
	}

	public static EbayManager getEbayManager() {
		return (EbayManager)getBean("ebayManager");
	}

	public static TicketListingCrawler getTicketListingCrawler() {
		if (context == null) {
			return null;
		}
		return (TicketListingCrawler)getBean("ticketListingCrawler");
	}

	public void setApplicationContext(ApplicationContext context)
			throws BeansException {
		SpringUtil.context = context;
	}	
}