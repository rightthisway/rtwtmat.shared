package com.admitone.tmat.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.NewPOSEvents;

public class POSEventUtil {
	
	public static StringBuffer getLatestNewPosEvents(String fromDateTimeParam,List<NewPOSEvents> list) {
		StringBuffer stringBuffer = new StringBuffer();		
		try {
			
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			
			if(null == fromDateTimeParam) {
				Date date = new Date();
				date.setDate(date.getDate()-1);
				fromDateTimeParam = df.format(date)+" 00:00:00";	
			}
			
			if(null == list) {
				list = DAORegistry.getQueryManagerDAO().getNewPosEventsFromStub(fromDateTimeParam);
				//List<NewPOSEvents> list = DAORegistry.getNewPOSEventDAO().getNewPOSEventsByDateStr(fromDateTimeParam);
			}
			
			stringBuffer.append("EventId,Event Created DateAndTime,TMAT ArtistName,EventName,EventDate,Venue,EventTime,City,State,TMAT ParentCategory,TN Parent Category,Child Category,GrandChild Category,Event Exist in TMAT,VenueCategory \n");
			/*df = new SimpleDateFormat("MM/dd/yyyy");
			DateFormat tf = new SimpleDateFormat("hh:mm aa");
			DateFormat dateTimeformat = new SimpleDateFormat("MM/dd/yyyy hh:mm aa");*/
			
			if(null != list) {
				for(NewPOSEvents event:list){					
					stringBuffer.append(event.getId()  + "," + event.getCreatedDateStr() + "," + event.getTmatArtistName().replaceAll(",", "-") + "," + event.getName().replaceAll(",", "-") + "," + event.getDateStr() + "," + 
					event.getVenue().replaceAll(",", "-")+ "," + event.getTimeStr() +","+ event.getCity().replaceAll(",", "-") +","+ event.getState().replaceAll(",", "-") +","+ 
					event.getTmatParentCategory().replaceAll(",", "-")+","+event.getParentCategory().replaceAll(",", "-")+","+ event.getChildCategory().replaceAll(",", "-")+","+ event.getGrandChildCategory().replaceAll(",", "-")+","+ event.getEventExistinTMAT().replaceAll(",", "-")+","+event.getCategoryGroup().replaceAll(",", "-")+"\n");
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return stringBuffer;
	}

}
