package com.admitone.tmat.utils;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import com.admitone.tmat.dao.DAORegistry;

/**
 * Its only purpose is to initialize Hibernate so it creates the tables
 */
public class DBInitializer {
        public static void main(String[] args) {
        	ApplicationContext context = new FileSystemXmlApplicationContext("target/tmat-1/WEB-INF/spring-dao-database.xml");
        	DAORegistry.getEventDAO().get(1);
        	System.exit(0);
        }
}