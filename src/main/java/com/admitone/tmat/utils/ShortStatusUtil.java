package com.admitone.tmat.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.admitone.tmat.dao.DAORegistry;
import com.admitone.tmat.data.Category;
import com.admitone.tmat.data.CategoryMapping;
import com.admitone.tmat.data.Event;
import com.admitone.tmat.data.ExcludeTicketMap;
import com.admitone.tmat.data.ShortTransaction;
import com.admitone.tmat.data.Synonym;
import com.admitone.tmat.data.Ticket;
//import com.admitone.tmat.data.Tour;
import com.admitone.tmat.data.UserAlert;
import com.admitone.tmat.data.VenueCategory;
import com.admitone.tmat.enums.EventStatus;
import com.admitone.tmat.enums.TourType;
import com.admitone.tmat.enums.UserAlertStatus;
import com.admitone.tmat.pojo.ShortStatus;
import com.admitone.tmat.pojo.SimpleTicket;


public class ShortStatusUtil {
	
	//These go to eleven.
	private static final int MAX_QTY = 20;

	// For the matching quantities, the idea is to avoid quantities for 
	// which after fulfilling the transaction, there is only a single ticket left.
	// So we avoid quantity n + 1 (but only if n < 10).
	//1 = 1,3,4,5,6,7,8,9,10,11
	//2 = 2,4,5,6,7,8,9,10,11
	//3 = 3,5,6,7,8,9,10,11
	//4 = 4,6,7,8,9,10,11
	//5 = 5,7,8,9,10,11
	//6 = 6,8,9,10,11
	//7 = 7,9,10,11
	//8 = 8,10,11
	//9 = 9,11
	//10 = 10,11
	//11 = 11
	private static Map<Integer, List<Integer>> matchingQuantities; 
	static {
		matchingQuantities = new HashMap<Integer, List<Integer>>();
		matchingQuantities.put(0, new ArrayList<Integer>());
		for(int i = 1; i <= MAX_QTY ; i++) {
			List<Integer> tmpQtys = new ArrayList<Integer>();
			for(int qty = i ; qty <= MAX_QTY ; qty++){
				if(qty != (i + 1)){
					tmpQtys.add(qty);
				}
			}
			matchingQuantities.put(i, tmpQtys);
		}
	}

	/**
	 * getStatusByEvent
	 * 
	 * Will calculate and return an Event ShortStatus that contains child
	 * Category statuses which contain individual statuses. All fields are
	 * calculated.
	 * 
	 * @param eventId - Integer
	 * @return ShortStatus - single event status
	 */
	public static ShortStatus getStatusByEvent(Event event, String filter,String catType,String eventType,
			String childEventType,String grandChildEventType,String userName,Double minPGM,Double maxPGM) {
		if(event == null){
			return null;
		}	
		if(grandChildEventType!=null && grandChildEventType.trim().length() >0 &&
				!event.getArtist().getGrandChildTourCategory().getId().equals(Integer.valueOf(grandChildEventType))){
			return null;
		}
		if(childEventType!=null && childEventType.trim().length() >0
				&& !event.getArtist().getGrandChildTourCategory().getChildTourCategory().getId().equals(Integer.valueOf(childEventType))){
			return null;
		}
		if(eventType!=null && eventType.trim().length() >0
				&& !eventType.equalsIgnoreCase(event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName())){
			return null;
		}
		ArrayList<ShortTransaction> transactions = new ArrayList<ShortTransaction>();
		if(filter == null || filter.isEmpty() || filter.equals("short")){
			transactions.addAll(ShortTransactionManager.getInstance().getShortsByEvent(event.getId()));
		}
		if(filter == null || filter.isEmpty() || filter.equals("inventory")) {
			transactions.addAll(LongTransactionManager.getInstance().getShortsByEvent(event.getId()));
		}
//		if(filter == null || filter.isEmpty() || filter.equals("uncat")) {
//			transactions.addAll(ShortTransactionManager.getInstance().getShortsByEvent(event.getId()));
//			transactions.addAll(LongTransaction6Manager.getInstance().getShortsByEvent(event.getId()));
//		}
//		Event event = DAORegistry.getEventDAO().get(eventId);
		return getStatusByEvent(transactions, event, filter,catType,userName,minPGM,maxPGM);
	}
	
	public static ShortStatus getSaleStatusByEvent(Event event, String catScheme, String filter,String catType,String eventType) {
		if(event == null){
			return null;
		}	
		if(eventType!=null && !eventType.equalsIgnoreCase(event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName())){
			return null;
		}
		ArrayList<ShortTransaction> transactions = new ArrayList<ShortTransaction>();
		if(filter == null || filter.isEmpty() || filter.equals("short")){
			transactions.addAll(ShortTransactionManager.getInstance().getShortsByEvent(event.getId()));
		}
		if(filter == null || filter.isEmpty() || filter.equals("inventory")) {
			transactions.addAll(LongTransactionManager.getInstance().getShortsByEvent(event.getId()));
		}
//		if(filter == null || filter.isEmpty() || filter.equals("uncat")) {
//			transactions.addAll(ShortTransactionManager.getInstance().getShortsByEvent(event.getId()));
//			transactions.addAll(LongTransaction6Manager.getInstance().getShortsByEvent(event.getId()));
//		}
//		Event event = DAORegistry.getEventDAO().get(eventId);
		return getSaleStatusByEvent(transactions, event, catScheme, filter,catType);
	}

	/**
	 * getStatusByEvent
	 * 
	 * Will calculate and return an Event ShortStatus that contains child
	 * Category statuses which contain individual statuses. All fields are
	 * calculated.
	 * 
	 * @param transactions - ArrayList<ShortTransaction> for an event
	 * @return ShortStatus - single event status
	 */
	public static ShortStatus getStatusByEvent(ArrayList<ShortTransaction> transactions, Event event, String filter,String catType,String userName,Double minPGM,Double maxPGM) {
		Integer eventId = event.getId();
		String venue = null;
		String catScheme = "";
		DateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat formatTime = new SimpleDateFormat("hh:mm aa");
		/* Map<transId, transaction> for determining covers */
		
		// Map<CategoryId, Map<TransactionId, ShortStatus>
		Map<Integer, Map<Integer, ShortStatus>> ticketGroupStatusByCategoryId = new HashMap<Integer, Map<Integer, ShortStatus>>();

		// Map<RemainingQty, List<TicketId>
		Map<Integer, List<Integer>> ticketIdByRemainingQty = new HashMap<Integer, List<Integer>>();

		
		// If catScheme in parameter is invalid, take the first cat scheme for this event
//		List<String> schemes = Categorizer.getCategoryGroupsByEvent(eventId);
//		if(!schemes.contains(catScheme) && !schemes.isEmpty()){
//			catScheme = schemes.iterator().next();
//		}
		Map<Integer, Category> catMap = new HashMap<Integer, Category>();
		Map<String, Category> catSymbolMap = new HashMap<String, Category>();
		if(event.getVenueCategory()!=null){
			catScheme = event.getVenueCategory().getCategoryGroup();
			for(Category cat:DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId())){
				catMap.put(cat.getId(), cat);
				catSymbolMap.put(cat.getSymbol(), cat);
			}
		}
		Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
		Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategoryId());
		for(CategoryMapping mapping:categoryMappingList){
			List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
			if(list==null){
				list = new ArrayList<CategoryMapping>();
			}
			list.add(mapping);
			catMappingMap.put(mapping.getCategoryId(), list);
		}
		if(transactions != null) {
		
			//order transaction by putting in order:
			// short tx before covers, larger lotSize, lower price 
			orderTransactions(transactions);
			
			// loop through all of the shorting transactions
			for(ShortTransaction transaction : transactions){
//				String normalizedSection = SectionRowStripper.strip(event.getEventType(), transaction.getSection()); // Old logic
				String normalizedSection = transaction.getSection();  // New logic for group of section..
				String lastSectionChar = normalizedSection.substring(normalizedSection.length()-1);
				if(lastSectionChar.equals("-")){
					normalizedSection = normalizedSection.substring(0,normalizedSection.length()-1);
				}
				String row = transaction.getRow();				
				String normalizedRow =transaction.getRow();
				String lastChar =row.substring(row.length()-1);
				if(lastChar.equals("-")){
					normalizedRow= row.substring(0,row.length()-1);
				}else{
					normalizedRow = row;	
				}
				Integer categoryId = null;
				Category cat = Categorizer.computeCategory(event.getVenueCategory(), normalizedSection, normalizedRow, catScheme,catMap,catMappingMap);
				if(cat == null){
					cat = Categorizer.getCategoryByCatSection(transaction.getSection(), transaction.getRow(), event.getVenueCategory());
					categoryId = (cat == null)?0: cat.getId();
				}else{
					categoryId = cat.getId();
				}
				
				if("UNCAT".equalsIgnoreCase(catType) && categoryId != 0){
					continue;
				}
				if("CAT".equalsIgnoreCase(catType) && categoryId == 0){
					continue;
				}
				ShortStatus ticketGroupStatus = new ShortStatus();
				ticketGroupStatus.setTransactionId(transaction.getId());
				ticketGroupStatus.setQtySold(transaction.getQuantity());
				ticketGroupStatus.setPriceSold(transaction.getPrice());
				ticketGroupStatus.setExposure(ticketGroupStatus.getQtySold() * ticketGroupStatus.getPriceSold());
				ticketGroupStatus.setSection(transaction.getSection());
				ticketGroupStatus.setNormalizedSection(normalizedSection);
				ticketGroupStatus.setRow(transaction.getRow());
				ticketGroupStatus.setNormalizedRow(normalizedRow);
				ticketGroupStatus.setInvoice(transaction.getInvoice());
				ticketGroupStatus.setCustomer(transaction.getCustomer());
				if(transaction.getCustomer() != null 
						&& (transaction.getCustomer().equals("ADMT1") || transaction.getCustomer().equals("ADMT! - cat"))){
					ticketGroupStatus.setLongTransaction(true);
				}
				// if the section of a short ticket is listed as ZONE A, then categorize it and list the ticket under category 'A' 
				if(transaction.getSection() != null){
					transaction.getSection().toUpperCase();
				}
				if(categoryId == 0 && transaction.getSection().contains("ZONE")){
					if(!catType.equalsIgnoreCase("uncat")){
						Collection<Category> categories = null;
						if(event.getVenueCategory()!=null){
							categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
						}
						String section = transaction.getSection();
						int index = section.indexOf("ZONE");
						section= "Category "+section.substring(index+4).trim();
						if(categories!=null && !categories.isEmpty()){
							for(Category category:categories){
								if(category.getDescription().equalsIgnoreCase(section)){
									ticketGroupStatus.setCategory(category.getDescription());
									categoryId=category.getId();
									break;
								}
							}
						}
					}
					else{
						continue;
					}
					
				}
				else if (categoryId == 0) {
					ticketGroupStatus.setProjGrossMargin(0.00);
					ticketGroupStatus.setMinPrice(-1.00);
					ticketGroupStatus.setCategory("UNCAT");
				} else {
					Category category = catMap.get(categoryId);
					if (category == null) {
						ticketGroupStatus.setProjGrossMargin(0.00);
						ticketGroupStatus.setMinPrice(-1.00);
						ticketGroupStatus.setCategory("UNCAT");						
					} else {
						ticketGroupStatus.setCategory(category.getDescription());
					}
				}
				ticketGroupStatus.setCategoryId(categoryId);

				String transactionString = "";
				if(ticketGroupStatus.getLongTransaction()){
					transactionString = " Long ";
				} else {
					transactionString = " Short ";
				}
				ticketGroupStatus.setDescription(transactionString);
//				ticketGroupStatus.setDate(formatDate.format(event.getDate()));
				ticketGroupStatus.setEventId(eventId);
				
				UserAlert userAlert =getShortUserAlert(ticketGroupStatus,userName);
				
				if(null != userAlert){					
					
					if(null != userAlert.getPriceLow()){
						ticketGroupStatus.setAlertMinPrice(userAlert.getPriceLow().toString());
					}
					if(null != userAlert.getPriceHigh()){
						ticketGroupStatus.setAlertMaxPrice(userAlert.getPriceHigh().toString());
					}
					
					ticketGroupStatus.setShortAlertId(userAlert.getId());
				}
				
				
				Integer transactionKey = transaction.getId();
				
				Map<Integer, ShortStatus> transactionById = ticketGroupStatusByCategoryId.get(categoryId);
				if(transactionById == null) {
					transactionById = new HashMap<Integer, ShortStatus>();
					ticketGroupStatusByCategoryId.put(categoryId, transactionById);
				}
				transactionById.put(transactionKey, ticketGroupStatus);
				
			}	
		}

		List<Ticket> tickets = new ArrayList<Ticket>();
		Collection<Ticket> rawTickets = DAORegistry.getTicketDAO().getAllActiveRegularTicketsByEvent(eventId);
		TicketUtil.computeAdmitOneTickets(rawTickets,null);
		if (rawTickets != null) {
			// exclude admitone tickets
			for(Ticket ticket: rawTickets) {
				if(!ticket.isAdmitOneTicket()) {
					tickets.add(ticket);
				}
			}
		}
		
		

//		String dateString = "";
		int eventSumQtySold = 0;
		double eventSumPriceSold = 0.00;
		double eventSumBenefit = 0.00;
		double eventSumRevenue = 0.00;
		double eventSumMinPrice = 0.00;
		double eventSumTotalMinPrice = 0.00;
		double eventSumExposure = 0.00;
		
//		List<Integer> categoryIds = Categorizer.getCategoriesByEvent(event.getVenueCategory());
		List<Integer> categoryIds = new ArrayList<Integer>(catMap.keySet());
		if(("UNCAT".equalsIgnoreCase(catType) || "".equals(catType))&& ticketGroupStatusByCategoryId.get(0)!=null){
			categoryIds.add(0);
		}
		Collections.sort(categoryIds);

		List<ShortStatus> categoryStatuses = new ArrayList<ShortStatus>();
		
		Integer shortCatCount = 0;
		Integer longCatCount = 0;
		//for each category beginning from lowest id to highest id
		int catCount=0;
		for(Integer categoryId : categoryIds) {		
			Map<Integer, ShortStatus> statuses = ticketGroupStatusByCategoryId.get(categoryId);
			if(statuses == null) {
				continue;
			}
			
			Category category;
			
			if (categoryId == 0) {
				category = new Category(0, "UNCAT", "UNCAT", "", catScheme);
			} else {
				category = catMap.get(categoryId);
				if (category == null) {
					continue;
				}
			}
						
			
			
			double catSumBenefit = 0.0;
			double catSumRevenue = 0.0;
			double catSumPriceSold = 0.0;
			int catSumQtySold = 0;
			double catSumExposure = 0.0;
			double catSumMinPrice = 0.0;
			double catSumTotalMinPrice = 0.0;
			
			String categoryName = "";
			List<Integer> equalCats;
			if(category.getEqualCats() == null){
				//use the old method
				equalCats = new ArrayList<Integer>();
				equalCats.add(categoryId);
			} else if(category.getEqualCats().equals("*")) {					
				//use the all better cats
				//contains this cat
				equalCats = categoryIds.subList(0, categoryIds.indexOf(categoryId) + 1);
			} else {
				//use only mapped cats
				equalCats = new ArrayList<Integer>();

//				System.out.println("THE CAT OBJECT IS " + category.getEqualCats());
				String[] symbols = category.getEqualCats().split("\\s");
				if(event.getVenueCategory()!=null){
					for(String symbol: symbols) {
						Category dbCategory = catSymbolMap.get(symbol);
						if(dbCategory == null) {
							System.out.println("Found Bad Equal CAT! VenueCategory: " + event.getVenueCategory().getId() + " Symbol: " + symbol);
							continue;
						}
						equalCats.add(dbCategory.getId());
					}
				}
			}

			List<ShortStatus> ticketGroupStatuses = new ArrayList<ShortStatus>(statuses.values());
			Integer shortTicketGroupCount = 0;
			Integer longTicketGroupCount = 0;
			List<ShortStatus> notInRangeticketGroupStatuses = new ArrayList<ShortStatus>();
			for(ShortStatus ticketGroupStatus : ticketGroupStatuses){

				if(categoryName == null || categoryName.equals("")){
//					dateString = ticketGroupStatus.getDate();
					categoryName = ticketGroupStatus.getCategory();
				}
				
				if(ticketGroupStatus.getCategoryId() != 0 && ticketGroupStatus.getQtySold() > 0 && category.getEqualCats() != null) {					
					//this method mutates minPrice, siteId, and itemId of status
					getMinPrice(tickets, ticketIdByRemainingQty, ticketGroupStatus, equalCats, catScheme,event.getVenueCategory());
					
					if(ticketGroupStatus.getMinPrice() > 0) {
						double projGM = 0.00;
						if(!ticketGroupStatus.getLongTransaction()){
							projGM = (ticketGroupStatus.getPriceSold() - ticketGroupStatus.getMinPrice()) / ticketGroupStatus.getPriceSold();
							if((minPGM!=null && projGM<=minPGM) || 
									(maxPGM!=null && projGM>=maxPGM)){
								notInRangeticketGroupStatuses.add(ticketGroupStatus); 
								continue; // Remove status by category id but make sure you dont remove similar status for diff event 
							}             // if only one status remove entire event    ?? How (count status if status count is 0 return null event
							
							shortTicketGroupCount++;
							shortCatCount++;
						} else {
							projGM = (ticketGroupStatus.getMinPrice() - ticketGroupStatus.getPriceSold()) / ticketGroupStatus.getMinPrice();
							if((minPGM!=null && projGM<=minPGM) || 
									(maxPGM!=null && projGM>=maxPGM)){
								notInRangeticketGroupStatuses.add(ticketGroupStatus);
								continue;
							}
							longTicketGroupCount++;
							longCatCount++;
						}
						ticketGroupStatus.setTotalMinPrice(ticketGroupStatus.getQtySold() * ticketGroupStatus.getMinPrice());
						catSumTotalMinPrice += ticketGroupStatus.getTotalMinPrice();
						ticketGroupStatus.setProjGrossMargin(projGM);

						if (!ticketGroupStatus.getLongTransaction()) {
							catSumBenefit += ticketGroupStatus.getQtySold() * (ticketGroupStatus.getPriceSold() - ticketGroupStatus.getMinPrice());
							catSumRevenue += ticketGroupStatus.getQtySold() * ticketGroupStatus.getPriceSold();
						} else {
							catSumBenefit += ticketGroupStatus.getQtySold() * (ticketGroupStatus.getMinPrice() - ticketGroupStatus.getPriceSold());
							catSumRevenue += ticketGroupStatus.getQtySold() * ticketGroupStatus.getMinPrice();					
						}
					}else {
						if(minPGM != null || maxPGM!=null){
							if(!(minPGM !=null && minPGM <= 0) 
									&& !(maxPGM !=null && 0 <= maxPGM)){
								notInRangeticketGroupStatuses.add(ticketGroupStatus);
								continue;
							}
						}
						ticketGroupStatus.setProjGrossMargin(0.00);
					}
				}else if(minPGM != null || maxPGM!=null){
					if(!((minPGM !=null && minPGM <= 0) 
							& (maxPGM !=null && 0 <= maxPGM))){
						notInRangeticketGroupStatuses.add(ticketGroupStatus);
						continue;
					}
				}
				if(ticketGroupStatus.getMinPrice() > 0) {
					catSumMinPrice += ticketGroupStatus.getQtySold() * ticketGroupStatus.getMinPrice();
					catSumPriceSold += ticketGroupStatus.getQtySold() * ticketGroupStatus.getPriceSold();
					catSumQtySold += ticketGroupStatus.getQtySold();
					catSumExposure += ticketGroupStatus.getExposure();
				}
				
			}
			if(notInRangeticketGroupStatuses.size() == ticketGroupStatuses.size()){
				continue;
			}else{
				ticketGroupStatuses.removeAll(notInRangeticketGroupStatuses);
			}
			/*
			 * If count count of short and long tickets is > 0 
			 * or if both are 0 then the cat group is set to neutral (0)
			 *
			 * If count of short > 0 and long tickets == 0 
			 * then the cat group is set to short (-1)
			 * 
			 * If count of long > 0 and short ==0
			 * then the cat group is set to long (1)
			 * 
			 */
			catCount++;
			ShortStatus categoryStatus = new ShortStatus();
			
			if(shortTicketGroupCount > 0 && longTicketGroupCount > 0){
				categoryStatus.setLongShortNeutral(0);
			}else if(shortTicketGroupCount == 0 && longTicketGroupCount == 0){
				categoryStatus.setLongShortNeutral(0);
			}else if(shortTicketGroupCount == 0 && longTicketGroupCount > 0){
				categoryStatus.setLongShortNeutral(1);
			}else if(shortTicketGroupCount > 0 && longTicketGroupCount == 0){
				categoryStatus.setLongShortNeutral(-1);
			}else{
				categoryStatus.setLongShortNeutral(0);
			}
			
			categoryStatus.setPriceSold(catSumPriceSold / catSumQtySold);
			
			if (catSumRevenue != 0) {
				categoryStatus.setProjGrossMargin(catSumBenefit / catSumRevenue);
			}
			
			categoryStatus.setEventId(eventId);
			categoryStatus.setDescription(categoryName);
			categoryStatus.setCategoryId(categoryId);
			categoryStatus.setCategory(categoryName);
			categoryStatus.setQtySold(catSumQtySold);
			categoryStatus.setExposure(catSumExposure);
			if (catSumQtySold > 0) {
				categoryStatus.setMinPrice(catSumMinPrice / catSumQtySold);
			}
			categoryStatus.setTotalMinPrice(catSumTotalMinPrice);				
			
			categoryStatus.setChildren(ticketGroupStatuses);
			categoryStatuses.add(categoryStatus);
			
			eventSumPriceSold += catSumPriceSold;
 			eventSumQtySold += categoryStatus.getQtySold();
			eventSumExposure += categoryStatus.getExposure();
			if (catSumMinPrice > 0) {
				eventSumMinPrice += catSumMinPrice;
				eventSumTotalMinPrice += catSumTotalMinPrice;
			}
			eventSumBenefit += catSumBenefit;
			eventSumRevenue += catSumRevenue;
			
//			return eventStatus;
		
		}
		if(catCount==0){
			return null;
		}
		ShortStatus eventStatus = new ShortStatus();
		eventStatus.setEventId(eventId);
		//eventStatus.setDescription(event.getName() + ", " + formatDate.format(event.getDate()));
		eventStatus.setDescription(event.getName());
		venue=event.getVenue().getLocation();
		eventStatus.setVenue(venue);
		eventStatus.setDate(formatDate.format(event.getDate()));
		if(event.getTime()!=null){
			eventStatus.setTime(formatTime.format(event.getTime()));
		}
//		eventStatus.setDate(dateString);
		eventStatus.setQtySold(eventSumQtySold);
		eventStatus.setExposure(eventSumExposure);

		if (eventSumRevenue != 0) {
			eventStatus.setProjGrossMargin(eventSumBenefit / eventSumRevenue);
		}

		if(eventSumQtySold > 0) {
			eventStatus.setPriceSold(eventSumPriceSold / eventSumQtySold);
			eventStatus.setMinPrice(eventSumMinPrice / eventSumQtySold);
		}
		eventStatus.setTotalMinPrice(eventSumTotalMinPrice);

		if(shortCatCount > 0 && longCatCount > 0){
			eventStatus.setLongShortNeutral(0);
		}else if(shortCatCount == 0 && longCatCount == 0){
			eventStatus.setLongShortNeutral(0);
		}else if(shortCatCount == 0 && longCatCount > 0){
			eventStatus.setLongShortNeutral(1);
		}else if(shortCatCount > 0 && longCatCount == 0){
			eventStatus.setLongShortNeutral(-1);
		}else{
			eventStatus.setLongShortNeutral(0);
		}
		orderCatStatuses(categoryStatuses);
		
		System.out.println("ordered child list: " + categoryStatuses);
		
		eventStatus.setChildren(categoryStatuses);
		return eventStatus;
	}
	
	
	public static UserAlert getShortUserAlert(ShortStatus shortStatus,String userName){
		
		String qty  = String.valueOf(shortStatus.getQtySold());
		String category = shortStatus.getCategory();
		
		if(null == userName || null ==qty){
			return null;
		}
		
		if(null != category && category.trim().length() >0){
			String[] catArray = category.split(" ");
			if(null != catArray && catArray.length >0){
				for (String catObj : catArray) {
					if(catObj.contains("Category")){
						continue;
					}
					category = catObj;
				}
			}
			if(null != category && category.equals("") ){
				category = null;
			}
		}
		Collection<UserAlert> userAlertList = DAORegistry.getUserAlertDAO().getAlertsByUsernameAndEventIdAndShortDetails(userName, shortStatus.getEventId(),
				 qty,category, UserAlertStatus.ACTIVE);
		
		UserAlert userAlert = null;
		if(null != userAlertList && !userAlertList.isEmpty()){
			userAlert = userAlertList.iterator().next();
		}	
		return  userAlert;
	}
	
	//same as getStatusByEvent method but here we calculate data point for 90,60,30,14,7,4,1 days	
	public static ShortStatus getSaleStatusByEvent(ArrayList<ShortTransaction> transactions, Event event, String catScheme, String filter,String catType) {
		Integer eventId = event.getId();
		String venue = null;
		DateFormat formatDate = new SimpleDateFormat("yyyy/MM/dd");
		DateFormat formatTime = new SimpleDateFormat("hh:mm aa");
		/* Map<transId, transaction> for determining covers */
				
		// Map<CategoryId, Map<TransactionId, ShortStatus>
		Map<Integer, Map<Integer, ShortStatus>> ticketGroupStatusByCategoryId = new HashMap<Integer, Map<Integer, ShortStatus>>();

		// Map<RemainingQty, List<TicketId>
		Map<Integer, List<Integer>> ticketIdByRemainingQty = new HashMap<Integer, List<Integer>>();
		VenueCategory venueCategory = null;
		if(event.getVenueCategory()==null){
			venueCategory = DAORegistry.getVenueCategoryDAO().getVenueCategoryByVenueAndCategoryGroup(event.getVenueId(), catScheme);
		}
		Calendar toDate = Calendar.getInstance();
		toDate.setTime(toDate.getTime());
		Calendar fromDate = Calendar.getInstance();
		fromDate.setTime(event.getLocalDate());
		
		int dateDifference = 0;
		while(toDate.before(fromDate)){
			toDate.add(Calendar.DAY_OF_MONTH, 1);
			dateDifference++;
		}
		
		Map<String,Synonym> theaterSynonymMap = new HashMap<String, Synonym>();
		Map<String,Synonym> concertSynonymMap = new HashMap<String, Synonym>();
		Collection<Synonym> synonyms = DAORegistry.getSynonymDAO().getAll();
		for(Synonym synonym:synonyms){
			if(synonym.getType().equals("Theater")){
				theaterSynonymMap.put(synonym.getName(), synonym);
			}else{
				concertSynonymMap.put(synonym.getName(), synonym);
			}
		}
		
		Map<String,Synonym> synonymMap = null;
		if(event.getEventType().equals(TourType.THEATER)){
			synonymMap = theaterSynonymMap;
		}else if(event.getEventType().equals(TourType.CONCERT) || event.getEventType().equals(TourType.SPORT)){
			synonymMap = concertSynonymMap;
		}
		else{
			synonymMap =  new HashMap<String, Synonym>();
		}
		// If catScheme in parameter is invalid, take the first cat scheme for this event
//		List<String> schemes = Categorizer.getCategoryGroupsByEvent(eventId);
//		if(!schemes.contains(catScheme) && !schemes.isEmpty()){
//			catScheme = schemes.iterator().next();
//		}
		if(event.getVenueCategory()!=null){
			catScheme = event.getVenueCategory().getCategoryGroup();
		}
		if(transactions != null) {
		
			//order transaction by putting in order:
			// short tx before covers, larger lotSize, lower price 
			orderTransactions(transactions);
			Map<Integer, Category> catMap = new HashMap<Integer, Category>();
			if(event.getVenueCategory()!=null){
				catScheme = event.getVenueCategory().getCategoryGroup();
				for(Category cat:DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId())){
					catMap.put(cat.getId(), cat);
				}
			}
			Map<Integer,List<CategoryMapping>> catMappingMap = new HashMap<Integer, List<CategoryMapping>>();
			Collection<CategoryMapping> categoryMappingList = DAORegistry.getCategoryMappingDAO().getAllCategoryMappingsByVenueCategoryId(event.getVenueCategory().getId());
			for(CategoryMapping mapping:categoryMappingList){
				List<CategoryMapping> list =catMappingMap.get(mapping.getCategoryId());
				if(list==null){
					list = new ArrayList<CategoryMapping>();
				}
				list.add(mapping);
				catMappingMap.put(mapping.getCategoryId(), list);
			}
			// loop through all of the shorting transactions
			for(ShortTransaction transaction : transactions){
				String normalizedSection = "";
				normalizedSection = SectionRowStripper.strip(event.getEventType(), transaction.getSection(),synonymMap);
				String lastSectionChar = normalizedSection.substring(normalizedSection.length()-1);
				if(lastSectionChar.equals("-")){
					normalizedSection = normalizedSection.substring(0,normalizedSection.length()-1);
				}				
				String normalizedRow ="";
				String row = transaction.getRow();
				String lastChar =row.substring(row.length()-1);
				if(lastChar.equals("-")){
					row = row.substring(0,row.length()-1);
				}
				normalizedRow = transaction.getRow().replaceAll("\\W+", "");   //remove this and below line once .csv uses %minus% in rows range... this line is to support old code(ex. AE-->A-E) 
				Integer categoryId = null;
				Category cat = Categorizer.computeCategory(event.getVenueCategory(), normalizedSection, normalizedRow, catScheme,catMap,catMappingMap);
				if(cat ==null && row.contains("-")){ // this line is to support new code(ex. A%minus%E-->A-E)
					cat = Categorizer.computeCategory(event.getVenueCategory(), normalizedSection, row, catScheme,catMap,catMappingMap);
					if(cat!=null){
						categoryId = cat.getId();
					}
				}
				boolean delFlag=false;
				if(categoryId == null){
					Category category = Categorizer.getCategoryByCatSection(transaction.getSection(), transaction.getRow(),  venueCategory);
					categoryId = (category == null)?0: category.getId();
					if(categoryId!=0){
						delFlag=true;
					}
				}
				if(delFlag){
					System.out.println(eventId + ":" + normalizedSection + ":" + normalizedRow + ":" + categoryId);
				}
				if("UNCAT".equalsIgnoreCase(catType) && categoryId != 0){
					continue;
				}
				if("CAT".equalsIgnoreCase(catType) && categoryId == 0){
					continue;
				}
				ShortStatus ticketGroupStatus = new ShortStatus();
				ticketGroupStatus.setTransactionId(transaction.getId());
				ticketGroupStatus.setQtySold(transaction.getQuantity());
				ticketGroupStatus.setPriceSold(transaction.getPrice());
				ticketGroupStatus.setExposure(ticketGroupStatus.getQtySold() * ticketGroupStatus.getPriceSold());
				ticketGroupStatus.setSection(transaction.getSection());
				ticketGroupStatus.setNormalizedSection(normalizedSection);
				ticketGroupStatus.setRow(transaction.getRow());
				ticketGroupStatus.setNormalizedRow(normalizedRow);
				ticketGroupStatus.setInvoice(transaction.getInvoice());
				ticketGroupStatus.setCustomer(transaction.getCustomer());
				//ticketGroupStatus.setSellingPrice(transaction.getSelling_price());
				//ticketGroupStatus.setInvoiceDate(transaction.getInvoiceDate());
				//ticketGroupStatus.setPoDate(transaction.getPoDate());
				ticketGroupStatus.setDataPointDiff(dateDifference);
				//ticketGroupStatus.setBuyingPrice(transaction.getBuyingPrice());
				
				if(transaction.getCustomer() != null 
						&& (transaction.getCustomer().equals("ADMT1") || transaction.getCustomer().equals("ADMT! - cat"))){
					ticketGroupStatus.setLongTransaction(true);
				}
				// if the section of a short ticket is listed as ZONE A, then categorize it and list the ticket under category 'A' 
				if(transaction.getSection() != null)
					transaction.getSection().toUpperCase();
				if(categoryId == 0 && transaction.getSection().contains("ZONE"))
				{
					if(!catType.equalsIgnoreCase("uncat"))
					{
						Collection<Category> categories = DAORegistry.getCategoryDAO().getAllCategoriesByVenueCategoryId(event.getVenueCategory().getId());
						String section = transaction.getSection();
						int index = section.indexOf("ZONE");
						section= "Category "+section.substring(index+4).trim();
						for(Category category:categories)
						{
							if(category.getDescription().equalsIgnoreCase(section))
							{
								ticketGroupStatus.setCategory(category.getDescription());
								categoryId=category.getId();
								break;
							}
						}
					}
					else{
						continue;
					}
					
				}
				else if (categoryId == 0) {
					ticketGroupStatus.setProjGrossMargin(0.00);
					ticketGroupStatus.setMinPrice(-1.00);
					ticketGroupStatus.setCategory("UNCAT");
				} else {
					Category category = DAORegistry.getCategoryDAO().get(categoryId);
					if (category == null) {
						ticketGroupStatus.setProjGrossMargin(0.00);
						ticketGroupStatus.setMinPrice(-1.00);
						ticketGroupStatus.setCategory("UNCAT");						
					} else {
						ticketGroupStatus.setCategory(category.getDescription());
					}
				}
				ticketGroupStatus.setCategoryId(categoryId);

				String transactionString = "";
				if(ticketGroupStatus.getLongTransaction()){
					transactionString = " Long ";
				} else {
					transactionString = " Short ";
				}
				ticketGroupStatus.setDescription(transactionString);
//				ticketGroupStatus.setDate(formatDate.format(event.getDate()));
				ticketGroupStatus.setEventId(eventId);
				
				Integer transactionKey = transaction.getId();
				
				Map<Integer, ShortStatus> transactionById = ticketGroupStatusByCategoryId.get(categoryId);
				if(transactionById == null) {
					transactionById = new HashMap<Integer, ShortStatus>();
					ticketGroupStatusByCategoryId.put(categoryId, transactionById);
				}
				transactionById.put(transactionKey, ticketGroupStatus);
				
			}	
		}

		List<Ticket> tickets = new ArrayList<Ticket>();
		Collection<Ticket> rawTickets = DAORegistry.getTicketDAO().getAllActiveRegularTicketsByEvent(eventId);
		TicketUtil.computeAdmitOneTickets(rawTickets,null);
		if (rawTickets != null) {
			// exclude admitone tickets
			for(Ticket ticket: rawTickets) {
				if(!ticket.isAdmitOneTicket()) {
					tickets.add(ticket);
				}
			}
		}
		
		ShortStatus eventStatus = new ShortStatus();

//		String dateString = "";
		int eventSumQtySold = 0;
		double eventSumPriceSold = 0.00;
		double eventSumBenefit = 0.00;
		double eventSumRevenue = 0.00;
		double eventSumMinPrice = 0.00;
		double eventSumTotalMinPrice = 0.00;
		double eventSumExposure = 0.00;
		
		List<Integer> categoryIds = Categorizer.getCategoriesByEvent(event.getVenueCategory());
		if(("UNCAT".equalsIgnoreCase(catType) || "".equals(catType))&& ticketGroupStatusByCategoryId.get(0)!=null){
			categoryIds.add(0);
		}
		Collections.sort(categoryIds);

		List<ShortStatus> categoryStatuses = new ArrayList<ShortStatus>();
		
		Integer shortCatCount = 0;
		Integer longCatCount = 0;
		//for each category beginning from lowest id to highest id
		for(Integer categoryId : categoryIds) {		
			Map<Integer, ShortStatus> statuses = ticketGroupStatusByCategoryId.get(categoryId);
			if(statuses == null) {
				continue;
			}
			
			Category category;
			
			if (categoryId == 0) {
				category = new Category(0, "UNCAT", "UNCAT", "", catScheme);
			} else {
				category = DAORegistry.getCategoryDAO().get(categoryId);
				if (category == null) {
					continue;
				}
			}
						
			ShortStatus categoryStatus = new ShortStatus();
			
			double catSumBenefit = 0.0;
			double catSumRevenue = 0.0;
			double catSumPriceSold = 0.0;
			int catSumQtySold = 0;
			double catSumExposure = 0.0;
			double catSumMinPrice = 0.0;
			double catSumTotalMinPrice = 0.0;
			
			String categoryName = "";
			List<Integer> equalCats;
			if(category.getEqualCats() == null){
				//use the old method
				equalCats = new ArrayList<Integer>();
				equalCats.add(categoryId);
			} else if(category.getEqualCats().equals("*")) {					
				//use the all better cats
				//contains this cat
				equalCats = categoryIds.subList(0, categoryIds.indexOf(categoryId) + 1);
			} else {
				//use only mapped cats
				equalCats = new ArrayList<Integer>();

				System.out.println("THE CAT OBJECT IS " + category.getEqualCats());
				String[] symbols = category.getEqualCats().split("\\s");
				for(String symbol: symbols) {
					Category dbCategory = DAORegistry.getCategoryDAO().getCategoryByVenueCategoryIdAndCategorySymbol(event.getVenueCategory().getId(),symbol);
					if(dbCategory == null) {
						System.out.println("Found Bad Equal CAT! VenueCategory: " + event.getVenueCategory().getId() + " Symbol: " + symbol);
						continue;
					}
					equalCats.add(dbCategory.getId());
				}
			} 	

			List<ShortStatus> ticketGroupStatuses = new ArrayList<ShortStatus>(statuses.values());
			Integer shortTicketGroupCount = 0;
			Integer longTicketGroupCount = 0;
			for(ShortStatus ticketGroupStatus : ticketGroupStatuses){

				if(categoryName == null || categoryName.equals("")){
//					dateString = ticketGroupStatus.getDate();
					categoryName = ticketGroupStatus.getCategory();
				}
				
				if(ticketGroupStatus.getCategoryId() != 0 && ticketGroupStatus.getQtySold() > 0 && category.getEqualCats() != null) {					
					//this method mutates minPrice, siteId, and itemId of status
					getMinPrice(tickets, ticketIdByRemainingQty, ticketGroupStatus, equalCats, catScheme,venueCategory);
					
					if(ticketGroupStatus.getMinPrice() > 0) {
						ticketGroupStatus.setTotalMinPrice(ticketGroupStatus.getQtySold() * ticketGroupStatus.getMinPrice());
						catSumTotalMinPrice += ticketGroupStatus.getTotalMinPrice();
						double projGM = 0.00;
						if(!ticketGroupStatus.getLongTransaction()){
							projGM = (ticketGroupStatus.getPriceSold() - ticketGroupStatus.getMinPrice()) / ticketGroupStatus.getPriceSold();
							shortTicketGroupCount++;
							shortCatCount++;
						} else {
							projGM = (ticketGroupStatus.getMinPrice() - ticketGroupStatus.getPriceSold()) / ticketGroupStatus.getMinPrice();
							longTicketGroupCount++;
							longCatCount++;
						}
						ticketGroupStatus.setProjGrossMargin(projGM);

						if (!ticketGroupStatus.getLongTransaction()) {
							catSumBenefit += ticketGroupStatus.getQtySold() * (ticketGroupStatus.getPriceSold() - ticketGroupStatus.getMinPrice());
							catSumRevenue += ticketGroupStatus.getQtySold() * ticketGroupStatus.getPriceSold();
						} else {
							catSumBenefit += ticketGroupStatus.getQtySold() * (ticketGroupStatus.getMinPrice() - ticketGroupStatus.getPriceSold());
							catSumRevenue += ticketGroupStatus.getQtySold() * ticketGroupStatus.getMinPrice();					
						}
					} else {
						ticketGroupStatus.setProjGrossMargin(0.00);
					}
				}

				if(ticketGroupStatus.getMinPrice() > 0) {
					catSumMinPrice += ticketGroupStatus.getQtySold() * ticketGroupStatus.getMinPrice();
				}
				
				catSumPriceSold += ticketGroupStatus.getQtySold() * ticketGroupStatus.getPriceSold();
				catSumQtySold += ticketGroupStatus.getQtySold();
				catSumExposure += ticketGroupStatus.getExposure();
			}
			
			/*
			 * If count count of short and long tickets is > 0 
			 * or if both are 0 then the cat group is set to neutral (0)
			 *
			 * If count of short > 0 and long tickets == 0 
			 * then the cat group is set to short (-1)
			 * 
			 * If count of long > 0 and short ==0
			 * then the cat group is set to long (1)
			 * 
			 */
			
			if(shortTicketGroupCount > 0 && longTicketGroupCount > 0){
				categoryStatus.setLongShortNeutral(0);
			}else if(shortTicketGroupCount == 0 && longTicketGroupCount == 0){
				categoryStatus.setLongShortNeutral(0);
			}else if(shortTicketGroupCount == 0 && longTicketGroupCount > 0){
				categoryStatus.setLongShortNeutral(1);
			}else if(shortTicketGroupCount > 0 && longTicketGroupCount == 0){
				categoryStatus.setLongShortNeutral(-1);
			}else{
				categoryStatus.setLongShortNeutral(0);
			}
			
			categoryStatus.setPriceSold(catSumPriceSold / catSumQtySold);
			
			if (catSumRevenue != 0) {
				categoryStatus.setProjGrossMargin(catSumBenefit / catSumRevenue);
			}
			
			categoryStatus.setEventId(eventId);
			categoryStatus.setDescription(categoryName);
			categoryStatus.setCategoryId(categoryId);
			categoryStatus.setCategory(categoryName);
			categoryStatus.setQtySold(catSumQtySold);
			categoryStatus.setExposure(catSumExposure);
			if (catSumQtySold > 0) {
				categoryStatus.setMinPrice(catSumMinPrice / catSumQtySold);
			}
			categoryStatus.setTotalMinPrice(catSumTotalMinPrice);				
			
			categoryStatus.setChildren(ticketGroupStatuses);
			categoryStatuses.add(categoryStatus);
			
			eventSumPriceSold += catSumPriceSold;
 			eventSumQtySold += categoryStatus.getQtySold();
			eventSumExposure += categoryStatus.getExposure();
			if (catSumMinPrice > 0) {
				eventSumMinPrice += catSumMinPrice;
				eventSumTotalMinPrice += catSumTotalMinPrice;
			}
			eventSumBenefit += catSumBenefit;
			eventSumRevenue += catSumRevenue;
			
			eventStatus.setEventId(eventId);
			//eventStatus.setDescription(event.getName() + ", " + formatDate.format(event.getDate()));
			eventStatus.setDescription(event.getName());
			venue=event.getVenue().getLocation();
			eventStatus.setVenue(venue);
			eventStatus.setDate(formatDate.format(event.getDate()));
			if(event.getTime()!=null){
				eventStatus.setTime(formatTime.format(event.getTime()));
			}
//			eventStatus.setDate(dateString);
			eventStatus.setQtySold(eventSumQtySold);
			eventStatus.setExposure(eventSumExposure);

			if (eventSumRevenue != 0) {
				eventStatus.setProjGrossMargin(eventSumBenefit / eventSumRevenue);
			}

			if(eventSumQtySold > 0) {
				eventStatus.setPriceSold(eventSumPriceSold / eventSumQtySold);
				eventStatus.setMinPrice(eventSumMinPrice / eventSumQtySold);
			}
			eventStatus.setTotalMinPrice(eventSumTotalMinPrice);

			if(shortCatCount > 0 && longCatCount > 0){
				eventStatus.setLongShortNeutral(0);
			}else if(shortCatCount == 0 && longCatCount == 0){
				eventStatus.setLongShortNeutral(0);
			}else if(shortCatCount == 0 && longCatCount > 0){
				eventStatus.setLongShortNeutral(1);
			}else if(shortCatCount > 0 && longCatCount == 0){
				eventStatus.setLongShortNeutral(-1);
			}else{
				eventStatus.setLongShortNeutral(0);
			}
			orderCatStatuses(categoryStatuses);
			
			System.out.println("ordered child list: " + categoryStatuses);
			
			eventStatus.setChildren(categoryStatuses);
			
			
		
		}
				
		Calendar from = Calendar.getInstance();
		from.setTime(event.getLocalDate());
		double date90= 0,date60= 0,date30= 0,date14= 0,date7= 0,date4= 0,date1 = 0;
//		int dateDiff90= 0,dateDiff60= 0,dateDiff30= 0, dateDiff14= 0,dateDiff7= 0,dateDiff4= 0,dateDiff1 = 0;
//		int i = 0;
		
		if("UNCAT".equalsIgnoreCase(catType)){
			ArrayList<Ticket> unCatTicketList = (ArrayList<Ticket>) DAORegistry.getTicketDAO().getUncategorizedActiveTicketsByEvent(event.getId(), catScheme);
			ArrayList<Ticket> tempTicketList = new ArrayList<Ticket>();
			ArrayList<Ticket> sortableList = new ArrayList<Ticket>();
			ArrayList<ShortStatus> tempShortTicket = new ArrayList<ShortStatus>();
			ArrayList<ShortStatus> tempShortTicketChild = new ArrayList<ShortStatus>();
			if(eventStatus != null){
				if(eventStatus.getChildren() != null){
				for(ShortStatus ticketsDetail : eventStatus.getChildren()){
					if(ticketsDetail.getChildren() != null){
					for(ShortStatus ticketDetails: ticketsDetail.getChildren()){
					if(unCatTicketList != null){
						for(Ticket ticket : unCatTicketList){
							if(ticket.getQuantity().equals(ticketDetails.getQtySold())){
								tempTicketList.add(ticket);
							}
						}
					}
					if(tempTicketList != null && !tempTicketList.isEmpty()){
						for(Ticket list : tempTicketList){
							if(list.getPriceHistory() != null && !list.getPriceHistory().isEmpty()){
								String[] priceList = list.getPriceHistory().split(",");
								for(String pricelist : priceList){
									if(pricelist != null && !priceList.equals("")){
										String[] finalPriceList = pricelist.split(":");
										
										Ticket ticketOb = new Ticket();
										try {
											if(finalPriceList.length > 1){
												if(finalPriceList[0] != null && !finalPriceList[0].isEmpty())
													ticketOb.setLastUpdate(new SimpleDateFormat("MM-dd-yy").parse(finalPriceList[0]));
												if(finalPriceList[1] != null && !finalPriceList[1].isEmpty())
													ticketOb.setBuyItNowPrice(Double.valueOf(finalPriceList[1]));
											}											
										} catch (ParseException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										sortableList.add(ticketOb);
									}
								}
							}
						}
						tempTicketList.clear();
						/*Collections.sort(sortableList, new Comparator<Ticket>() {

							public int compare(Ticket arg0, Ticket arg1) {
								
								if(arg0.getLastUpdate().after(arg1.getLastUpdate())){
									return 1;
								}
								
								return 0;
							}
						});*/
												
						for(Ticket ticketSortedList : sortableList){
							if(ticketSortedList.getLastUpdate() != null && !ticketSortedList.getLastUpdate().equals("")){
							Calendar to = Calendar.getInstance();
							to.setTime(ticketSortedList.getLastUpdate());
							
							int dateDiff =0;
							while(to.before(from)){
								to.add(Calendar.DAY_OF_MONTH, 1);
								dateDiff++;
							}
							if(dateDiff <= 90){
								if(dateDiff == 90){
									date90 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 60){									
									date60 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 30){
									date30 = ticketSortedList.getBuyItNowPrice();									
								}else if(dateDiff == 14){
									date14 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 7){
									date7 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 4){
									date4 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 1){
									date1 = ticketSortedList.getBuyItNowPrice();
								}
							}
						}
						}
						sortableList.clear();
						
					}
					
					ticketDetails.setPriceAtDate90(date90);
					ticketDetails.setPriceAtDate60(date60);
					ticketDetails.setPriceAtDate30(date30);
					ticketDetails.setPriceAtDate14(date14);
					ticketDetails.setPriceAtDate7(date7);
					ticketDetails.setPriceAtDate4(date4);
					ticketDetails.setPriceAtDate1(date1);
					
					tempShortTicket.add(ticketDetails);					
				}
					ticketsDetail.setChildren(tempShortTicket);
					tempShortTicketChild.add(ticketsDetail);					
				}				
				}
				if(tempShortTicketChild != null && !tempShortTicketChild.isEmpty())
					eventStatus.setChildren(tempShortTicketChild);				
			}
			}			
						
		}else if("CAT".equalsIgnoreCase(catType)){
			ArrayList<Ticket> catTicketList = (ArrayList<Ticket>) DAORegistry.getTicketDAO().getCategorizedActiveTicketsByEvent(event.getId(), catScheme);
			ArrayList<Ticket> tempTicketList = new ArrayList<Ticket>();
			ArrayList<Ticket> sortableList = new ArrayList<Ticket>();
			ArrayList<ShortStatus> tempShortTicket = new ArrayList<ShortStatus>();
			ArrayList<ShortStatus> tempShortTicketChild = new ArrayList<ShortStatus>();
			if(eventStatus != null){
				if(eventStatus.getChildren() != null){
				for(ShortStatus ticketsDetail : eventStatus.getChildren()){
					if(ticketsDetail.getChildren() != null){
					for(ShortStatus ticketDetails: ticketsDetail.getChildren()){
					if(catTicketList != null){
						for(Ticket ticket : catTicketList){
							if(ticketDetails.getCategory().equalsIgnoreCase(ticket.getCategory().getDescription()) && ticket.getQuantity().equals(ticketDetails.getQtySold())){
								tempTicketList.add(ticket);
							}
						}
					}
					if(tempTicketList != null && !tempTicketList.isEmpty()){
						for(Ticket list : tempTicketList){
							if(list.getPriceHistory() != null && !list.getPriceHistory().isEmpty()){
								String[] priceList = list.getPriceHistory().split(",");
								for(String pricelist : priceList){
									if(pricelist != null && !priceList.equals("")){
										String[] finalPriceList = pricelist.split(":");
										
										Ticket ticketOb = new Ticket();
										try {
											if(finalPriceList.length > 1){
												if(finalPriceList[0] != null && !finalPriceList[0].isEmpty())
													ticketOb.setLastUpdate(new SimpleDateFormat("MM-dd-yy").parse(finalPriceList[0]));
												if(finalPriceList[1] != null && !finalPriceList[1].isEmpty())
													ticketOb.setBuyItNowPrice(Double.valueOf(finalPriceList[1]));
											}											
										} catch (ParseException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
										sortableList.add(ticketOb);
									}
								}
							}
						}
						tempTicketList.clear();
						/*Collections.sort(sortableList, new Comparator<Ticket>() {

							public int compare(Ticket arg0, Ticket arg1) {
								
								if(arg0.getLastUpdate().after(arg1.getLastUpdate())){
									return 1;
								}
								
								return 0;
							}
						});*/
												
						for(Ticket ticketSortedList : sortableList){
							if(ticketSortedList.getLastUpdate() != null && !ticketSortedList.getLastUpdate().equals("")){
							Calendar to = Calendar.getInstance();
							to.setTime(ticketSortedList.getLastUpdate());
							
							int dateDiff =0;
							while(to.before(from)){
								to.add(Calendar.DAY_OF_MONTH, 1);
								dateDiff++;
							}
							
							if(dateDiff <= 90){
								if(dateDiff == 90){
									date90 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 60){									
									date60 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 30){
									date30 = ticketSortedList.getBuyItNowPrice();									
								}else if(dateDiff == 14){
									date14 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 7){
									date7 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 4){
									date4 = ticketSortedList.getBuyItNowPrice();
								}else if(dateDiff == 1){
									date1 = ticketSortedList.getBuyItNowPrice();
								}
							}
						}
						}
						sortableList.clear();
						
					}
					
					ticketDetails.setPriceAtDate90(date90);
					ticketDetails.setPriceAtDate60(date60);
					ticketDetails.setPriceAtDate30(date30);
					ticketDetails.setPriceAtDate14(date14);
					ticketDetails.setPriceAtDate7(date7);
					ticketDetails.setPriceAtDate4(date4);
					ticketDetails.setPriceAtDate1(date1);
					
					tempShortTicket.add(ticketDetails);					
				}
					ticketsDetail.setChildren(tempShortTicket);
					tempShortTicketChild.add(ticketsDetail);
				}		
				}
				eventStatus.setChildren(tempShortTicketChild);
			}
			}
			
		}else{
			ArrayList<Ticket> catTicketList = (ArrayList<Ticket>) DAORegistry.getTicketDAO().getCategorizedActiveTicketsByEvent(event.getId(), catScheme);
			ArrayList<Ticket> unCatTicketList = (ArrayList<Ticket>) DAORegistry.getTicketDAO().getUncategorizedActiveTicketsByEvent(event.getId(), catScheme);
			ArrayList<ShortStatus> tempShortTicketChild = new ArrayList<ShortStatus>();
			ArrayList<Ticket> tempTicketList = new ArrayList<Ticket>();
			ArrayList<Ticket> sortableList = new ArrayList<Ticket>();
			ArrayList<ShortStatus> tempShortTicket = new ArrayList<ShortStatus>();
			if(eventStatus != null){
				if(eventStatus.getChildren() != null){
				for(ShortStatus ticketsDetail : eventStatus.getChildren()){
					if(ticketsDetail.getChildren() != null){
					for(ShortStatus ticketDetails: ticketsDetail.getChildren()){
						if(ticketDetails.getCategory() != null){												
							
							if(catTicketList != null){
								for(Ticket ticket : catTicketList){
									if(ticketDetails.getCategory().equalsIgnoreCase(ticket.getCategory().getDescription()) && ticket.getQuantity().equals(ticketDetails.getQtySold())){
										tempTicketList.add(ticket);
									}
								}
							}
							if(tempTicketList != null && !tempTicketList.isEmpty()){
								for(Ticket list : tempTicketList){
									if(list.getPriceHistory() != null && !list.getPriceHistory().isEmpty()){
										String[] priceList = list.getPriceHistory().split(",");
										for(String pricelist : priceList){
											if(pricelist != null && !priceList.equals("")){
												String[] finalPriceList = pricelist.split(":");
												
												Ticket ticketOb = new Ticket();
												try {
													if(finalPriceList.length > 1){
														if(finalPriceList[0] != null && !finalPriceList[0].isEmpty())
															ticketOb.setLastUpdate(new SimpleDateFormat("MM-dd-yy").parse(finalPriceList[0]));
														if(finalPriceList[1] != null && !finalPriceList[1].isEmpty())
															ticketOb.setBuyItNowPrice(Double.valueOf(finalPriceList[1]));
													}											
												} catch (ParseException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												sortableList.add(ticketOb);
											}
										}
									}
								}
								tempTicketList.clear();
								/*Collections.sort(sortableList, new Comparator<Ticket>() {

									public int compare(Ticket arg0, Ticket arg1) {
										
										if(arg0.getLastUpdate().after(arg1.getLastUpdate())){
											return 1;
										}
										
										return 0;
									}
								});*/
														
								for(Ticket ticketSortedList : sortableList){
									if(ticketSortedList.getLastUpdate() != null && !ticketSortedList.getLastUpdate().equals("")){
									Calendar to = Calendar.getInstance();
									to.setTime(ticketSortedList.getLastUpdate());
									
									int dateDiff =0;
									while(to.before(from)){
										to.add(Calendar.DAY_OF_MONTH, 1);
										dateDiff++;
									}
									
									if(dateDiff <= 90){
										if(dateDiff == 90){
											date90 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 60){									
											date60 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 30){
											date30 = ticketSortedList.getBuyItNowPrice();									
										}else if(dateDiff == 14){
											date14 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 7){
											date7 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 4){
											date4 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 1){
											date1 = ticketSortedList.getBuyItNowPrice();
										}
									}
								}
								}
								sortableList.clear();
								
							}
							
							ticketDetails.setPriceAtDate90(date90);
							ticketDetails.setPriceAtDate60(date60);
							ticketDetails.setPriceAtDate30(date30);
							ticketDetails.setPriceAtDate14(date14);
							ticketDetails.setPriceAtDate7(date7);
							ticketDetails.setPriceAtDate4(date4);
							ticketDetails.setPriceAtDate1(date1);
							
							tempShortTicket.add(ticketDetails);				
						
							
						}else{
							
							if(unCatTicketList != null){
								for(Ticket ticket : unCatTicketList){
									if(ticket.getQuantity().equals(ticketDetails.getQtySold())){
										tempTicketList.add(ticket);
									}
								}
							}
							if(tempTicketList != null && !tempTicketList.isEmpty()){
								for(Ticket list : tempTicketList){
									if(list.getPriceHistory() != null && !list.getPriceHistory().isEmpty()){
										String[] priceList = list.getPriceHistory().split(",");
										for(String pricelist : priceList){
											if(pricelist != null && !priceList.equals("")){
												String[] finalPriceList = pricelist.split(":");
												
												Ticket ticketOb = new Ticket();
												try {
													if(finalPriceList.length > 1){
														if(finalPriceList[0] != null && !finalPriceList[0].isEmpty())
															ticketOb.setLastUpdate(new SimpleDateFormat("MM-dd-yy").parse(finalPriceList[0]));
														if(finalPriceList[1] != null && !finalPriceList[1].isEmpty())
															ticketOb.setBuyItNowPrice(Double.valueOf(finalPriceList[1]));
													}											
												} catch (ParseException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												sortableList.add(ticketOb);
											}
										}
									}
								}
								tempTicketList.clear();
								/*Collections.sort(sortableList, new Comparator<Ticket>() {

									public int compare(Ticket arg0, Ticket arg1) {
										
										if(arg0.getLastUpdate().after(arg1.getLastUpdate())){
											return 1;
										}
										
										return 0;
									}
								});*/
														
								for(Ticket ticketSortedList : sortableList){
									if(ticketSortedList.getLastUpdate() != null && !ticketSortedList.getLastUpdate().equals("")){
									Calendar to = Calendar.getInstance();
									to.setTime(ticketSortedList.getLastUpdate());
									
									int dateDiff =0;
									while(to.before(from)){
										to.add(Calendar.DAY_OF_MONTH, 1);
										dateDiff++;
									}
									
									if(dateDiff <= 90){
										if(dateDiff == 90){
											date90 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 60){									
											date60 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 30){
											date30 = ticketSortedList.getBuyItNowPrice();									
										}else if(dateDiff == 14){
											date14 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 7){
											date7 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 4){
											date4 = ticketSortedList.getBuyItNowPrice();
										}else if(dateDiff == 1){
											date1 = ticketSortedList.getBuyItNowPrice();
										}
									}
								}
								}
								sortableList.clear();
								
							}
							
							ticketDetails.setPriceAtDate90(date90);
							ticketDetails.setPriceAtDate60(date60);
							ticketDetails.setPriceAtDate30(date30);
							ticketDetails.setPriceAtDate14(date14);
							ticketDetails.setPriceAtDate7(date7);
							ticketDetails.setPriceAtDate4(date4);
							ticketDetails.setPriceAtDate1(date1);
							
							tempShortTicket.add(ticketDetails);				
						
							
						}			
					}		
						ticketsDetail.setChildren(tempShortTicket);
						tempShortTicketChild.add(ticketsDetail);
					}					
				}
				eventStatus.setChildren(tempShortTicketChild);
			}
			
		}	
		}
		
		
		return eventStatus;
	}
	
	

	/**
	 * getAllStatuses
	 * 
	 * Will calculate and return a nested Collection of Event ShortStatuses.
	 * Each Event ShortStatus will contain a child Collection of Category 
	 * ShortStatuses. Also, each Category status will contain a child 
	 * Collection of Individual ShortStatuses.
	 * 
	 * @return Collection<ShortStatus> - nested Collection of event statuses
	 */
	public static Collection<ShortStatus> getAllEventStatuses(String catScheme, String filter,String catType,String eventType,Double minPGM,Double maxPGM) {
		
		Set<Integer> eventIds = new HashSet<Integer>(); 
		
		if(filter == null || filter.isEmpty() || filter.equals("short")){
			eventIds.addAll(ShortTransactionManager.getInstance().getAllShortEventIds());
		}
		
		if(filter == null || filter.isEmpty() || filter.equals("inventory")){
			eventIds.addAll(LongTransactionManager.getInstance().getAllLongTransactionEventIds());			
		}
		
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
		
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		
		for(Integer eventId: eventIds){
			Event event=DAORegistry.getEventDAO().get(eventId);
			ShortStatus status = getStatusByEvent(event, filter,catType,eventType,null,null,null,minPGM,maxPGM);
			if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
				totQtySold = totQtySold + status.getQtySold();
				//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
				totPriceSold = totPriceSold + status.getPriceSold();
				totMinPrice = totMinPrice + status.getMinPrice();
				totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
				totExposure = totExposure + status.getExposure();
				statuses.add(status);
			}
		}
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		sortEvents(statuses);
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice);
		st.setTotalMinPrice(totTotalMinPrice);
		st.setExposure(totExposure);
		st.setDescription("TOTAL");
		//statuses.add(st);
		return statuses;
	}
	
	
	/**
	 * getAllStatuses
	 * 
	 * Will calculate and return a nested Collection of Event ShortStatuses.
	 * Each Event ShortStatus will contain a child Collection of Category 
	 * ShortStatuses. Also, each Category status will contain a child 
	 * Collection of Individual ShortStatuses. 
	 * With date filters
	 * 
	 * @return Collection<ShortStatus> - nested Collection of event statuses
	 */
	public static Collection<ShortStatus> getAllEventStatuses(String catScheme, String filter,String catType, Date dateRange1, Date dateRange2,String eventType,Double minPGM,Double maxPGM) {
		
		Set<Integer> eventIds = new HashSet<Integer>(); 
		
		if(filter == null || filter.isEmpty() || filter.equals("short")){
			eventIds.addAll(ShortTransactionManager.getInstance().getAllShortEventIds());
		}
		
		if(filter == null || filter.isEmpty() || filter.equals("inventory")){
			eventIds.addAll(LongTransactionManager.getInstance().getAllLongTransactionEventIds());			
		}
		
//		if(filter == null || filter.isEmpty() || filter.equals("uncat")){
//			eventIds.addAll(LongTransactionManager.getInstance().getAllLongTransactionEventIds());
//			eventIds.addAll(ShortTransactionManager.getInstance().getAllShortEventIds());
//		}
		
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
		
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		
		for(Integer eventId: eventIds){
			Event event = DAORegistry.getEventDAO().get(eventId);
			if(event.getDate().after(dateRange1) && event.getDate().before(dateRange2) && (eventType==null || event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName().equalsIgnoreCase(eventType))){
				ShortStatus status = getStatusByEvent(event, filter,catType,eventType,null,null,null,minPGM,maxPGM);
				if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
					totQtySold = totQtySold + status.getQtySold();
					//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
					totPriceSold = totPriceSold + status.getPriceSold();
					totMinPrice = totMinPrice + status.getMinPrice();
					totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
					totExposure = totExposure + status.getExposure();
					statuses.add(status);
				}
			}
		}
		sortEvents(statuses);
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice);
		st.setTotalMinPrice(totTotalMinPrice);
		st.setExposure(totExposure);
		st.setDescription("TOTAL");
		//statuses.add(st);
		return statuses;
	}
	//same as getAllEventStatuses method but here we calculate data point for 90,60,30,14,7,4,1 days	
	public static Collection<ShortStatus> getAllSaleEventStatuses(String catScheme, String filter,String catType, Date dateRange1, Date dateRange2,String eventType) {
		
		Set<Integer> eventIds = new HashSet<Integer>(); 
		
		if(filter == null || filter.isEmpty() || filter.equals("short")){
			eventIds.addAll(ShortTransactionManager.getInstance().getAllShortEventIds());
		}
		
		if(filter == null || filter.isEmpty() || filter.equals("inventory")){
			eventIds.addAll(LongTransactionManager.getInstance().getAllLongTransactionEventIds());			
		}
		
//		if(filter == null || filter.isEmpty() || filter.equals("uncat")){
//			eventIds.addAll(LongTransactionManager.getInstance().getAllLongTransactionEventIds());
//			eventIds.addAll(ShortTransactionManager.getInstance().getAllShortEventIds());
//		}
		
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
		
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		
		for(Integer eventId: eventIds){
			Event event = DAORegistry.getEventDAO().get(eventId);
			if(event.getDate().after(dateRange1) && event.getDate().before(dateRange2) && (eventType==null || event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName().equalsIgnoreCase(eventType))){
				ShortStatus status = getSaleStatusByEvent(event, catScheme, filter,catType,eventType);
				if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
					totQtySold = totQtySold + status.getQtySold();
					//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
					totPriceSold = totPriceSold + status.getPriceSold();
					totMinPrice = totMinPrice + status.getMinPrice();
					totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
					totExposure = totExposure + status.getExposure();
					statuses.add(status);
				}
			}
		}
		sortEvents(statuses);
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice);
		st.setTotalMinPrice(totTotalMinPrice);
		st.setExposure(totExposure);
		st.setDescription("TOTAL");
		//statuses.add(st);
		return statuses;
	}
		
	public static synchronized List<Event> getFilteredEvents(Integer venueId,Integer eventId,Integer artistId, String filter,
			String catType, Date dateRange1, Date dateRange2,String eventType,String childEventType,String grandChildEventType) {
		Set<Integer> eventIds = new HashSet<Integer>(); 
		Set<Integer> finalEventIds = new HashSet<Integer>();
		List<Event> events = new ArrayList<Event>();
//		Map<Integer,Event> tourEventMap = new HashMap<Integer,Event>();
//		Map<Integer,Event> venueEventMap = new HashMap<Integer,Event>();
		if(filter == null || filter.isEmpty() || filter.equals("short")){
			eventIds.addAll(ShortTransactionManager.getInstance().getAllShortEventIds());
		}
		
		if(filter == null || filter.isEmpty() || filter.equals("inventory")){
			eventIds.addAll(LongTransactionManager.getInstance().getAllLongTransactionEventIds());			
		}
		Map<Integer,Event> eventMap = new HashMap<Integer,Event>();
		
		if(eventId != null){
			if(!eventIds.contains(eventId)){
				return events;
			}else{
				eventIds.clear();
				Event event = DAORegistry.getEventDAO().get(eventId);
				eventMap.put(eventId,event);
				finalEventIds.add(eventId);
			}
		}else if(artistId != null){
			Collection<Event>  tourEvents = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
			for(Event tourEvent:tourEvents){
				if(eventIds.contains(tourEvent.getId())){
					eventMap.put(tourEvent.getId(), tourEvent);
					finalEventIds.add(tourEvent.getId());
				}
			}
		}else if(venueId != null){
			
			Collection<Event>  venueEvents = DAORegistry.getEventDAO().getAllEventsByVenueAndStatus(venueId,EventStatus.ACTIVE);
			for(Event venueEvent:venueEvents){
				if(eventIds.contains(venueEvent.getId())){
					eventMap.put(venueEvent.getId(), venueEvent);
					finalEventIds.add(venueEvent.getId());
				}
			}
			
		}else{
			Collection<Event>  eventList = DAORegistry.getEventDAO().getAllEventsByDates(dateRange1,dateRange2,EventStatus.ACTIVE);
			for(Event event:eventList){
					eventMap.put(event.getId(), event);
			}
			finalEventIds.addAll(eventIds);
		}
		eventIds.clear();
		eventIds=null;
		for(Integer tempEventId: finalEventIds){
			Event event = eventMap.get(tempEventId);
			if(event != null){
				if((eventId !=null || artistId !=null  || venueId !=null || dateRange1==null || (event.getDate()!=null && event.getDate().after(dateRange1))) 
						&& (eventId !=null || artistId !=null || venueId !=null || dateRange2==null || (event.getDate()!=null && event.getDate().before(dateRange2))) 
						&& (grandChildEventType==null || 
								(grandChildEventType.trim().length() >0 && event.getArtist().getGrandChildTourCategory() != null  &&
									event.getArtist().getGrandChildTourCategory().getName() != null &&
									event.getArtist().getGrandChildTourCategory().getId().equals(Integer.valueOf(grandChildEventType)))
									&& (childEventType==null || 
											(childEventType.trim().length() >0 && event.getArtist().getGrandChildTourCategory() != null && 
												event.getArtist().getGrandChildTourCategory().getChildTourCategory() != null && 
												event.getArtist().getGrandChildTourCategory().getChildTourCategory().getId().equals(Integer.valueOf(childEventType))))
												&& (eventType==null || 
														(event.getArtist().getGrandChildTourCategory() != null && 
															event.getArtist().getGrandChildTourCategory().getChildTourCategory() != null && 
															event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory() != null && 
															event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName() != null &&
															event.getArtist().getGrandChildTourCategory().getChildTourCategory().getTourCategory().getName().equalsIgnoreCase(eventType))))){
					events.add(event);
				}
			}
		}
		return events;
	}
	public static Collection<ShortStatus> getAllEventStatusesByEvents(Collection<Event> events, String filter,String catType,
			String eventType,String childEventType,String grandChildEventType,String userName,Double minPGM,Double maxPGM){
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		double marketMover = 0.0D;
		int priceSoldCount = 0,exposureCount = 0,totalPriceCount = 0,priceCount = 0,moverCount = 0;
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
		for(Event event: events){
			ShortStatus status = getStatusByEvent(event, filter,catType,eventType,childEventType,grandChildEventType,userName,minPGM,maxPGM);
			if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
				totQtySold = totQtySold + status.getQtySold();
				//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
				totPriceSold = totPriceSold + status.getPriceSold();				
				totMinPrice = totMinPrice + status.getMinPrice();
				if(status.getMinPrice() > 0.00)
					priceCount += 1;
				totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
				if(status.getTotalMinPrice() > 0.00)
					totalPriceCount += 1;
				totExposure = totExposure + status.getExposure();
				if(status.getExposure() > 0.00)
					exposureCount += 1;
				marketMover = marketMover + status.getMarketMover();
				if(status.getMarketMover() > 0.00)
					moverCount += 1;
				if(status.getPriceSold() > 0.00)
					priceSoldCount += 1;
				
				statuses.add(status);
			}
		}

		sortEvents(statuses);
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice/(priceCount > 0 ? priceCount : 1 ));
		st.setTotalMinPrice(totTotalMinPrice/(totalPriceCount > 0 ? totalPriceCount : 1));
		st.setExposure(totExposure/ (exposureCount > 0 ? exposureCount : 1) );
		st.setTotalMarketMover(marketMover/(moverCount > 0 ? moverCount : 1));
		st.setPriceSold(totPriceSold/(priceSoldCount > 0 ? priceSoldCount : 1));
		
		st.setDescription("AVERAGE");
		statuses.add(st);
		
		ShortStatus st1 = new ShortStatus();
		st1.setQtySold(totQtySold);
		st1.setProjGrossMargin(totProjGrossMargin);
		st1.setPriceSold(totPriceSold);
		st1.setMinPrice(totMinPrice);
		st1.setTotalMinPrice(totTotalMinPrice);
		st1.setExposure(totExposure);
		st1.setTotalMarketMover(marketMover);
		st1.setDescription("TOTAL");
		statuses.add(st1);
		
		return statuses;
	}
	/*public static Collection<ShortStatus> getAllEventStatuses(Integer tourId, String catScheme, String filter,String catType,String eventType,Double minPGM,Double maxPGM) {
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
		Tour tour = DAORegistry.getTourDAO().get(tourId);
		if (tour == null) {
			return statuses;
		}
				
		Collection<Event> events = tour.getOrderedEvents();
		if (events == null) {
			return statuses;
		}
		
		//The below variables are used to calculate the sum.
		
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		
		for(Event event: events){
			ShortStatus status = getStatusByEvent(event, filter,catType,eventType,null,null,null,minPGM,maxPGM);
			if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
				totQtySold = totQtySold + status.getQtySold();
				//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
				totPriceSold = totPriceSold + status.getPriceSold();
				totMinPrice = totMinPrice + status.getMinPrice();
				totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
				totExposure = totExposure + status.getExposure();
				statuses.add(status);
			}
		}
		//Create a short status object and set it values to the sum. Set its description as TOTAL
		//Calculate the tot Projected Gross Margin 
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		sortEvents(statuses);
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice);
		st.setTotalMinPrice(totTotalMinPrice);
		st.setExposure(totExposure);
		st.setDescription("TOTAL");
		statuses.add(st);		
		return statuses;		
	}*/
	//same as getAllEventStatuses method but here we calculate data point for 90,60,30,14,7,4,1 days	
	public static Collection<ShortStatus> getAllSaleEventStatuses(Integer artistId, String catScheme, String filter,String catType,String eventType) {
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
						
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		if (events == null) {
			return statuses;
		}
		
		//The below variables are used to calculate the sum.
		
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		
		for(Event event: events){
			ShortStatus status = getSaleStatusByEvent(event, catScheme, filter,catType,eventType);
			if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
				totQtySold = totQtySold + status.getQtySold();
				//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
				totPriceSold = totPriceSold + status.getPriceSold();
				totMinPrice = totMinPrice + status.getMinPrice();
				totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
				totExposure = totExposure + status.getExposure();
				statuses.add(status);
			}
		}
		//Create a short status object and set it values to the sum. Set its description as TOTAL
		//Calculate the tot Projected Gross Margin 
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		sortEvents(statuses);
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice);
		st.setTotalMinPrice(totTotalMinPrice);
		st.setExposure(totExposure);
		st.setDescription("TOTAL");
		statuses.add(st);		
		return statuses;		
	}

	//method with date range
	public static Collection<ShortStatus> getAllEventStatuses(Integer artistId, String catScheme, String filter,String catType, Date dateRange1, Date dateRange2,String eventType,Double minPGM,Double maxPGM) {
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
				
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		if (events == null) {
			return statuses;
		}
		
		//The below variables are used to calculate the sum. 
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		
		for(Event event: events){
			if(event.getDate().after(dateRange1) & event.getDate().before(dateRange2)){
				ShortStatus status = getStatusByEvent(event, filter,catType,eventType,null,null,null,minPGM,maxPGM);
				if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
					totQtySold = totQtySold + status.getQtySold();
					//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
					totPriceSold = totPriceSold + status.getPriceSold();
					totMinPrice = totMinPrice + status.getMinPrice();
					totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
					totExposure = totExposure + status.getExposure();
					statuses.add(status);
				}
			}
		}
		//Create a short status object and set it values to the sum. Set its description as TOTAL
		//Calculate the tot Projected Gross Margin 
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice);
		st.setTotalMinPrice(totTotalMinPrice);
		st.setExposure(totExposure);
		st.setDescription("TOTAL");
		statuses.add(st);		
		return statuses;		
	}
	//same as getAllEventStatuses method but here we calculate data point for 90,60,30,14,7,4,1 days	
	public static Collection<ShortStatus> getAllSaleEventStatuses(Integer artistId, String catScheme, String filter,String catType, Date dateRange1, Date dateRange2,String eventType) {
		Collection<ShortStatus> statuses = new ArrayList<ShortStatus>();
		/*Tour tour = DAORegistry.getTourDAO().get(tourId);
		if (tour == null) {
			return statuses;
		}
		*/		
		Collection<Event> events = DAORegistry.getEventDAO().getAllActiveEventsByArtistId(artistId);
		if (events == null) {
			return statuses;
		}
		
		//The below variables are used to calculate the sum. 
		int totQtySold = 0;
		double totProjGrossMargin = 0.0D;
		double totPriceSold = 0.0D;
		double totMinPrice = 0.0D;
		double totTotalMinPrice = 0.0D;
		double totExposure = 0.0D;
		
		for(Event event: events){
			if(event.getDate().after(dateRange1) & event.getDate().before(dateRange2)){
				ShortStatus status = getSaleStatusByEvent(event, catScheme, filter,catType,eventType);
				if(status != null && status.getChildren() != null && !status.getChildren().isEmpty()){
					totQtySold = totQtySold + status.getQtySold();
					//totProjGrossMargin = totProjGrossMargin + status.getProjGrossMargin();
					totPriceSold = totPriceSold + status.getPriceSold();
					totMinPrice = totMinPrice + status.getMinPrice();
					totTotalMinPrice = totTotalMinPrice + status.getTotalMinPrice();
					totExposure = totExposure + status.getExposure();
					statuses.add(status);
				}
			}
		}
		//Create a short status object and set it values to the sum. Set its description as TOTAL
		//Calculate the tot Projected Gross Margin 
		System.out.println("The Total Exposure : " + totExposure);
		System.out.println("The TotalMinPrice : " + totTotalMinPrice);
		System.out.println("The Min Price  : " + totMinPrice);
		totProjGrossMargin = (totExposure - totTotalMinPrice)/totExposure;
		ShortStatus st = new ShortStatus();
		st.setQtySold(totQtySold);
		st.setProjGrossMargin(totProjGrossMargin);
		st.setPriceSold(totPriceSold);
		st.setMinPrice(totMinPrice);
		st.setTotalMinPrice(totTotalMinPrice);
		st.setExposure(totExposure);
		st.setDescription("TOTAL");
		statuses.add(st);		
		return statuses;		
	}


	/*
	 * remainingQtyByTicketId is an empty map this is going to be filled
	 * if null then it won't be considered
	 */
	public static Collection<Integer> getExcludedTicketIds(Map<Integer, List<Integer>> ticketIdsByRemainingQty, ShortStatus ticketGroupStatus, Map<Integer, Integer> remainingQtyByTicketId) {
		// holds the remaining quantity of tickets which already matches a previous shorting
		// transaction

		Collection<Integer> excludedTicketIds = new ArrayList<Integer>();
		for(Integer remainingQty: ticketIdsByRemainingQty.keySet()) {					
			// If the remaining Qty is less than the lotSize of this short,
			// then add it to the tickets to be excluded.
			if(remainingQty < ticketGroupStatus.getQtySold()) {
				excludedTicketIds.addAll(ticketIdsByRemainingQty.get(remainingQty));
			} else {
				if (remainingQtyByTicketId != null) {
					for(Integer ticketId : ticketIdsByRemainingQty.get(remainingQty)){
						remainingQtyByTicketId.put(ticketId, remainingQty);
					}
				}
			}
		}
		
		Integer transactionType = ticketGroupStatus.getLongTransaction()? ExcludeTicketMap.TYPE_LONG:ExcludeTicketMap.TYPE_SHORT;
		Collection<Integer> userExcludedTicketIds = getExcludedTickets(ticketGroupStatus.getEventId(), ticketGroupStatus.getCategoryId(), ticketGroupStatus.getTransactionId(), transactionType);
		if(userExcludedTicketIds != null){
			excludedTicketIds.addAll(userExcludedTicketIds);
		}		
		
		return excludedTicketIds;
	}

	/**
	 * getMinPrice - THIS METHOD MUTATES THE PARAMETER ShortStatus status 
	 * 				 AND HashMap remainingQtyToTixIdMap
	 * 
	 * This method will find the minimum priced ticket available to this short,
	 * taking into account the other shorts filled all ready, and possible
	 * higher categories that can be used to fill this short. It is expected
	 * that the ShortStatuses were ordered prior to calling this method so
	 * that higher categories and larger lotSizes are filled first.
	 * 
	 * @param ticketIdsByRemainingQty - Map used to keep track of tickets 
	 * 		      that have been used for other shorts
	 * @param ticketGroupStatus - The short status requiring a minPrice lookup
	 * @param equivalentCats - The categories that can be used to fill 
	 * 		      this short
	 * 
	 * @return void
	 */
	private static void getMinPrice(Collection<Ticket> tickets,
			Map<Integer, List<Integer>> ticketIdsByRemainingQty, 
			ShortStatus ticketGroupStatus, Collection<Integer> equivalentCats, String catScheme,VenueCategory venueCategory) {
		
		Map<Integer, Integer> remainingQtyByTicketId = new HashMap<Integer, Integer>();
		Collection<Integer> excludedTicketIds = getExcludedTicketIds(ticketIdsByRemainingQty, ticketGroupStatus, remainingQtyByTicketId);
	
		SimpleTicket lowestTicket = TicketUtil.getLowestEventCategoriesLotsSimpleTicket(
				tickets, equivalentCats, ticketGroupStatus.getQtySold(),
				excludedTicketIds, catScheme,venueCategory);
		
					
		//if there are no tickets, check if there are tickets with lower quantities and get their minimum price.
		if(lowestTicket == null) {
			
			SimpleTicket lowestTicket1 = null;
			//TODO: Try to find the next largest ticket size. 
			/*SimpleTicket lowestTicket1 = TicketUtil.getLowestEventCategoriesLotsSimpleTicket1(
					tickets, equivalentCats, ticketGroupStatus.getQtySold(),
					excludedTicketIds, catScheme);*/
			if(lowestTicket1 == null){
				ticketGroupStatus.setMinPrice(-1.00);
				return;
			}/*else{
				lowestTicket = lowestTicket1;
			}*/
		}
		
		Integer ticketId = lowestTicket.getTicketId();

		ticketGroupStatus.setMinPrice(lowestTicket.getPrice());
		ticketGroupStatus.setSiteId(lowestTicket.getSiteId());
		ticketGroupStatus.setTicketId(ticketId);
		ticketGroupStatus.setTicketSection(lowestTicket.getSection());
		ticketGroupStatus.setTicketRow(lowestTicket.getRow());
		
		// update the ticketIdsByRemainingQty to substract qty of the matching
		// lowest ticket
		Integer oldQty;
		if(remainingQtyByTicketId.containsKey(ticketId)){
			oldQty = remainingQtyByTicketId.get(ticketId);
			ticketIdsByRemainingQty.get(oldQty).remove(ticketId);
		} else {
			oldQty = lowestTicket.getRemainingQty();
		}
		int newQty = oldQty - ticketGroupStatus.getQtySold();

		List<Integer> ticketIds = ticketIdsByRemainingQty.get(newQty);
		if(ticketIds == null){
			ticketIds = new ArrayList<Integer>();
			ticketIdsByRemainingQty.put(newQty, ticketIds);
		}
		ticketIds.add(ticketId);
	}
	
	/**
	 * orderTransactions - THIS METHOD MUTATES THE PARAMETER transactions
	 * 
	 * This method will mutate the parameter ArrayList transactions and 
	 * order it by:
	 * a.) Short transactions before covers
	 * b.) Larger lotSize before smaller lotSize
	 * c.) Lower Price before higher price
	 *
	 * @param transactions - List of ShortTransactions to be ordered
	 * @return void 
	 */
	public static void orderTransactions(ArrayList<ShortTransaction> transactions){
		Collections.sort(transactions, new Comparator<ShortTransaction>() {
			
			public int compare(ShortTransaction trans1, ShortTransaction trans2) {
				if(trans1.getCoverId() == null && trans2.getCoverId() != null) {
					return -1;
				} else if(trans1.getCoverId() != null && trans2.getCoverId() == null) {
					return 1;
				} else if(trans1.getQuantity() > trans2.getQuantity()) {
					return -1;
				} else if(trans1.getQuantity() < trans2.getQuantity()) {
					return 1;
				} else if(trans1.getPrice() < trans2.getPrice()) {
					return -1;
				} else if(trans1.getPrice() > trans2.getPrice()) {
					return 1;
				}
				return 0;
			}
		});
	}
	
	/**
	 * orderCatStatuses - THIS METHOD MUTATES THE PARAMETER statuses
	 * 
	 * This method will mutate the parameter List statuses and 
	 * order it by:
	 * a.) Set categories before unset categories
	 * b.) Lower(older) category ids before higher category ids
	 *
	 * @param statuses - List of ShortStatus to be ordered
	 * @return void 
	 */
	public static void orderCatStatuses(List<ShortStatus> statuses){
		Collections.sort(statuses, new Comparator<ShortStatus>() {
			
			public int compare(ShortStatus status1, ShortStatus status2) {
				if((status1.getCategoryId() == 0 && status2.getCategoryId() != 0)
						|| status1.getCategoryId() < status2.getCategoryId()) {
					return -1;
				} else if((status1.getCategoryId() != 0 && status2.getCategoryId() == 0)
						|| status1.getCategoryId() > status2.getCategoryId()) {
					return 1;
				}
				return 0;
			}
		});
	}
	
	/**
	 * orderEventStatuses - THIS METHOD MUTATES THE PARAMETER statuses
	 * 
	 * This method will mutate the parameter List statuses and 
	 * order it by:
	 * a.) Set dates before unset dates
	 * b.) Lower(older) category ids before higher category ids
	 *
	 * @param statuses - List of ShortStatus to be ordered
	 * @return void 
	 */
	public static void orderEventStatuses(List<ShortStatus> statuses){
		Collections.sort(statuses, new Comparator<ShortStatus>() {
			
			public int compare(ShortStatus status1, ShortStatus status2) {
				if((status1.getDate() == null) && (status2.getDate() == null)){
					return 0;
				}
				if(status1.getDate().isEmpty() && status2.getDate().isEmpty()){
					return 0;
				}
				if((status1.getDate() == null) && (status2.getDate() != null)) {
					return 1;
				} else if((status1.getDate() != null) && (status2.getDate() == null)) {
					return -1;
				} else if(!status1.getDate().isEmpty() && status2.getDate().isEmpty()) {
					return -1;
				} else if(status1.getDate().isEmpty() && !status2.getDate().isEmpty()) {
					return 1;
				} else if(new Date(status1.getDate()).after(new Date(status2.getDate()))) {
					return 1;
				} else if(new Date(status1.getDate()).before(new Date(status2.getDate()))) {
					return -1;
				}
				return 0;
			}
		});
		// sort = "external";
	}


	public static Collection<Integer> getExcludedTickets(Integer eventId, Integer categoryId, Integer transactionId, Integer transactionType){
		Collection<ExcludeTicketMap> ticketMaps = DAORegistry.getExcludeTicketMapDAO().getAllMapsByEventId(eventId);
		if (ticketMaps == null) {
			return null;
		}

		List<Integer> result = new ArrayList<Integer>();
		for(ExcludeTicketMap ticketMap : ticketMaps){
			//if exclusion applies at the event level
			// or at category level or at the inventory level exclude id
			
			if (ticketMap.getScope().equals(ExcludeTicketMap.SCOPE_EVENT)
				|| (ticketMap.getScope().equals(ExcludeTicketMap.SCOPE_CAT) && ticketMap.getCategoryId().equals(categoryId))
				|| (ticketMap.getScope().equals(ExcludeTicketMap.SCOPE_TX) && ticketMap.getTransactionId().equals(transactionId) && ticketMap.getTransactionType().equals(transactionType))) {
				result.add(ticketMap.getTicketId());
			}
		}
		
		if(result.isEmpty()){
			return null;
		}
		
		return result;
	}
	private static void sortEvents(Collection<ShortStatus> statuses)
	{
		Collections.sort((List<ShortStatus>)statuses, new Comparator<ShortStatus>() {
			
			public int compare(ShortStatus status1, ShortStatus status2) {
				
					if((status1.getDate() == null) && (status2.getDate() == null)){
						return 0;
					}
					if(status1.getDate().isEmpty() && status2.getDate().isEmpty()){
						return 0;
					}
					if((status1.getDate() == null) && (status2.getDate() != null)) {
						return 1;
					} else if((status1.getDate() != null) && (status2.getDate() == null)) {
						return -1;
					} else if(!status1.getDate().isEmpty() && status2.getDate().isEmpty()) {
						return -1;
					} else if(status1.getDate().isEmpty() && !status2.getDate().isEmpty()) {
						return 1;
					} else if(new Date(status1.getDate()).after(new Date(status2.getDate()))) {
						return 1;
					} else if(new Date(status1.getDate()).before(new Date(status2.getDate()))) {
						return -1;
					}	
				  return 0;
				}
				
			
		});
	}
}
