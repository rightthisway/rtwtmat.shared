package com.admitone.tmat.common;

public class SharedProperty {
	
	private String adminUrl;
	private String browseUrl;
	private String analyticsUrl;
	private String browseLiteUrl;
	
	public String getAdminUrl() {
		return adminUrl;
	}
	public void setAdminUrl(String adminUrl) {
		this.adminUrl = adminUrl;
	}
	public String getBrowseUrl() {
		return browseUrl;
	}
	public void setBrowseUrl(String browseUrl) {
		this.browseUrl = browseUrl;
	}
	public String getAnalyticsUrl() {
		return analyticsUrl;
	}
	public void setAnalyticsUrl(String analyticsUrl) {
		this.analyticsUrl = analyticsUrl;
	}
	public String getBrowseLiteUrl() {
		return browseLiteUrl;
	}
	public void setBrowseLiteUrl(String browseLiteUrl) {
		this.browseLiteUrl = browseLiteUrl;
	}
	
}
